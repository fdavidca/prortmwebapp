package com.proambiente.webapp.test;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.proambiente.modelo.Color;
import com.proambiente.modelo.dto.EquipoDTO;
import com.proambiente.webapp.dao.AbstractFacade;
import com.proambiente.webapp.dao.ColorFacade;

@RunWith(Arquillian.class)
public class ColorFacadeTest {
	
	@Inject
	ColorFacade colorFacade;
	
	
	@Deployment
	public static WebArchive createDeployment(){		
		
		File[] files = Maven.resolver().loadPomFromFile("pom.xml").resolve("joda-time:joda-time:2.3").withTransitivity().asFile();		
		return ShrinkWrap.create(WebArchive.class,"test.war").addAsWebInfResource("test-ds.xml")
				.addAsLibraries(files)
				.addClass(AbstractFacade.class).addPackage(Color.class.getPackage()).addClass(ColorFacade.class).addClass(EquipoDTO.class)
				.addAsResource("META-INF/test-persistence.xml","META-INF/persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE,"beans.xml");
	}
	
	@Test
	public void testEncontrarColor(){
		try{
			Color color = colorFacade.find(1); 
			System.out.println("37j : " + color);
		}catch(Exception exc){
			exc.printStackTrace();
			Assert.fail("Error consultar analizador por id");
		}
				
	}
	
	@Test
	public void testListarColores(){
		try{
			List<Color> listaColores = colorFacade.findRange(new int[] {4,120});
			System.out.println("Lista Colores encontrado: " + listaColores.size());
		}catch(Exception exc){
			exc.printStackTrace();
			Assert.fail("error consultando lista");
		}
	}
	
	@Test
	public void testGuardarColor() {
		try {
			
			Color color = new Color();
			color.setColorId(8956321);
			color.setNombre("KIRA MUNGITA");
			colorFacade.edit(color);
			
		}catch(Exception exc) {
			exc.printStackTrace();
			Assert.fail("error guardando color");
		}
	}

}
