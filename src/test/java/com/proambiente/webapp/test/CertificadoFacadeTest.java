package com.proambiente.webapp.test;

import java.io.File;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.proambiente.modelo.Certificado;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.dto.EquipoDTO;
import com.proambiente.webapp.dao.AbstractFacade;
import com.proambiente.webapp.dao.CertificadoFacade;
import com.proambiente.webapp.dao.RevisionFacade;

@RunWith(Arquillian.class)
public class CertificadoFacadeTest {
	
	@Inject
	CertificadoFacade certificadoFacade;
	
	@Inject
	RevisionFacade revisionFacade;
	
	@Deployment
	public static WebArchive createDeployment(){		
		
		File[] files = Maven.resolver().loadPomFromFile("pom.xml").resolve("joda-time:joda-time:2.3").withTransitivity().asFile();		
		return ShrinkWrap.create(WebArchive.class,"test.war").addAsWebInfResource("test-ds.xml")
				.addAsLibraries(files)
				.addClass(AbstractFacade.class).addPackage(Certificado.class.getPackage()).addClass(RevisionFacade.class).addClass(CertificadoFacade.class).addClass(EquipoDTO.class)
				.addAsResource("META-INF/test-persistence.xml","META-INF/persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE,"beans.xml");
	}
	
	@Test
	public void testEncontrarCertificado(){
		try{
			Certificado certificado = certificadoFacade.find(1); 
			System.out.println("37j : " + certificado);
		}catch(Exception exc){
			exc.printStackTrace();
			Assert.fail("Error consultar analizador por id");
		}
				
	}
	
	@Test
	public void testListarCertificados(){
		try{
			List<Certificado> listaCertificados = certificadoFacade.findRange(new int[] {4,120});
			System.out.println("Lista Certificados encontrado: " + listaCertificados.size());
		}catch(Exception exc){
			exc.printStackTrace();
			Assert.fail("error consultando alerta");
		}
	}
	
	@Test
	public void testGuardarCertificado() {
		try {
			
			Certificado certificado = new Certificado();
			certificado.setAnulado(true);
			certificado.setConsecutivoRunt("485125");
			certificado.setConsecutivoSustrato("7852369");
			certificado.setFechaAnulacion(new Timestamp(new Date().getTime()));
			certificado.setFechaExpedicion(new Timestamp(new Date().getTime()));
			certificado.setFechaImpresion(new Timestamp(new Date().getTime()));
			certificado.setFechaVencimiento(new Timestamp(new Date().getTime()));
			certificado.setMotivoAnulacion("");
			Revision referenceRevision =revisionFacade.find(10);
			certificado.setRevision(referenceRevision);
			certificadoFacade.edit(certificado);
			
		}catch(Exception exc) {
			exc.printStackTrace();
			Assert.fail("error guardando certificado");
		}
	}

}
