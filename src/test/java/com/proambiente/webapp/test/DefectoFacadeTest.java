package com.proambiente.webapp.test;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.proambiente.modelo.Defecto;
import com.proambiente.modelo.dto.DefectoDTO;

import com.proambiente.webapp.dao.AbstractFacade;
import com.proambiente.webapp.dao.DefectoFacade;

@RunWith(Arquillian.class)
public class DefectoFacadeTest {
	
	@Inject
	DefectoFacade defectoFacade;
	
	
	
	@Deployment
	public static WebArchive createDeployment(){		
		
		File[] files = Maven.resolver().loadPomFromFile("pom.xml").resolve("joda-time:joda-time:2.3").withTransitivity().asFile();		
		return ShrinkWrap.create(WebArchive.class,"test.war").addAsWebInfResource("test-ds.xml")
				.addAsLibraries(files)
				.addClass(AbstractFacade.class).addPackage(Defecto.class.getPackage()).addClass(DefectoFacade.class).addClass(DefectoDTO.class)
				.addAsResource("META-INF/test-persistence.xml","META-INF/persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE,"beans.xml");
	}
	
	@Test
	public void testEncontrarDefecto(){
		try{
			Defecto defectoAsociado = defectoFacade.find(1); 
			System.out.println("119j : " +defectoAsociado);
		}catch(Exception exc){
			exc.printStackTrace();
			Assert.fail("Error consultar entidad");
		}
				
	}
	
	@Test
	public void testListarDefectos(){
		try{
			List<Defecto> listaDefectos = defectoFacade.findRange(new int[] {0,10});
			System.out.println("Lista encontradas" + listaDefectos.size());
		}catch(Exception exc){
			exc.printStackTrace();
			Assert.fail("error consultando lista ");
		}
	}
	
	

}
