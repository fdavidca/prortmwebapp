package com.proambiente.webapp.test.formulario;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.File;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.proambiente.modelo.ClaseVehiculo;
import com.proambiente.modelo.Color;
import com.proambiente.modelo.LineaVehiculo;
import com.proambiente.modelo.Marca;
import com.proambiente.modelo.Propietario;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Servicio;
import com.proambiente.modelo.TipoCombustible;
import com.proambiente.modelo.TipoVehiculo;
import com.proambiente.modelo.Usuario;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.modelo.dto.DefectoDTO;
import com.proambiente.modelo.dto.EquipoDTO;
import com.proambiente.modelo.dto.MedidaDTO;
import com.proambiente.modelo.dto.PruebaDTO;
import com.proambiente.modelo.dto.RevisionDTO;
import com.proambiente.webapp.dao.ClaseVehiculoFacade;
import com.proambiente.webapp.dao.ColorFacade;
import com.proambiente.webapp.dao.MarcaFacade;
import com.proambiente.webapp.dao.PropietarioFacade;
import com.proambiente.webapp.dao.RevisionFacade;
import com.proambiente.webapp.dao.ServicioFacade;
import com.proambiente.webapp.dao.TipoCombustibleFacade;
import com.proambiente.webapp.dao.TipoVehiculoFacade;
import com.proambiente.webapp.dao.UsuarioFacade;
import com.proambiente.webapp.dao.VehiculoFacade;
import com.proambiente.webapp.service.FechaService;
import com.proambiente.webapp.service.InspeccionService;
import com.proambiente.webapp.service.PasswordHash;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.util.UtilErrores;

/**
 * Hace test de todos los metodos de CONSULTA en 
 * la clas RevisionBean
 * @author FABIAN
 *
 */



@RunWith(Arquillian.class)
public class IngresarDatosRevisionTest {
	
	@Inject
	UsuarioFacade usuarioDAO;
	
	@Inject
	RevisionFacade revisionDAO;
	
	@Inject
	MarcaFacade marcaDAO;
	
	@Inject
	ColorFacade colorDAO;
	
	@Inject
	VehiculoFacade vehiculoDAO;
	
	@Inject
	PropietarioFacade propietarioDAO;
	
	@Inject
	RevisionService revisionService;
	
	@Inject
	TipoCombustibleFacade tipoCombustibleDAO;
	
	@Inject
	TipoVehiculoFacade tipoVehiculoDAO;
	
	@Inject
	ServicioFacade servicioDAO;
	
	@Inject
	ClaseVehiculoFacade claseDAO;
	
	
	
	@Deployment
	public static WebArchive createDeployment(){		
		
		File[] files = Maven.resolver().loadPomFromFile("pom.xml").resolve("joda-time:joda-time:2.3").withTransitivity().asFile();	
		return ShrinkWrap.create(WebArchive.class,"test.war").addAsWebInfResource("test-ds.xml")
				.addAsLibraries(files)
				.addPackage(Color.class.getPackage())
				.addPackage(RevisionFacade.class.getPackage())
				.addClass(PruebaDTO.class).addClass(RevisionDTO.class).addClass(DefectoDTO.class)
				.addClass(MedidaDTO.class).addClass(EquipoDTO.class)
				.addClass(RevisionService.class)
				.addClass(InspeccionService.class)
				.addClass(FechaService.class)
				.addClass(PasswordHash.class)
				.addClass(UtilErrores.class)				
				.addAsResource("META-INF/test-persistence.xml","META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE,"beans.xml");
	}
	
	
	/**
	 * 
	 */
	@Test
	public void testEncontrarMarcaPorNombre(){
		try{
			List<Marca> marcas = marcaDAO.findMarcasByNombre("CHEVROLET");
			for(Marca m:marcas)
				System.out.println("Nombre de marca: " + m.getNombre());
			assertNotNull(marcas);
		}catch(Exception exc){
			exc.printStackTrace();
			fail("Prueba fallida para metodo  testEncontrarMarcaPorNombre");
		}
	}
	
	
	
	/**
	 * 
	 */
	@Test
	public void testBuscarColoresPorNombre(){
		try{
			List<Color> colores = colorDAO.findColoresByNombre("VERDE");
			for(int i = 0; i < 10; i++){
				Color c = colores.get(i);
				System.out.println("Nombre de color: " + c.getNombre());
			}
			assertNotNull(colores);			
		}catch(Exception exc){
			exc.printStackTrace();
			fail("Error en test metodo buscarColoresPorNombre");
		}
	}
	
	@Test
	public void testConsultarLineasDeMarca(){
		try{
		Marca m = marcaDAO.find(3);
		assertNotNull(m);
		System.out.println("Buscando lineas para marca : " + m.getNombre());
		List<LineaVehiculo> lineas = marcaDAO.findLineaVehiculosByMarca(m);
		for(int i = 0; i < 5 ; i ++){
			LineaVehiculo linea = lineas.get(i);
			System.out.println("Nombre de Linea :" + linea.getNombre());
		}
		assertNotNull(lineas);
		}catch(Exception exc){
			exc.printStackTrace();
			fail("Error en prueba de ConsultarLineasDeMarca");
		}
	}
	
	@Test
	public void testBuscarVehiculoPorPlaca(){
		try{
			Vehiculo vehiculo = vehiculoDAO.findVehicuoByPlaca("OTTOV1");
			System.out.println("Vehiculo cargado : " + vehiculo.getPlaca());
			assertNotNull(vehiculo.getVehiculoId());			
		}catch(Exception exc){
			exc.printStackTrace();
			fail("Prueba fallida metodo testBuscarVehiculoPorPlaca");			
		}
	}
	
	
	
	
	/**
	 * Debe estar creado el propietario correspondiente
	 */
	@Test
	public void buscarPropietarioPorIdentificacion(){
		try{
			Propietario propietario = propietarioDAO.find(4192845l);//ojo la id que espera es de tipo Long
			System.out.println("Nombres de propietario:" +  propietario.getNombres() );
			assertNotNull(propietario);
		}catch(Exception exc){
			exc.printStackTrace();
			fail("Prueba fallida metodo buscarPropietarioPorIdentificacion");
		}
	}
	
	@Test
	public void testRegistrarRevisionSinPropietario(){
		try{
			Vehiculo vehiculo = new Vehiculo();
			vehiculo.setPlaca("AAA245");			
			vehiculo.setModelo(2002);
			TipoCombustible tipoCombustibleGasolina = tipoCombustibleDAO.find(1);
			vehiculo.setTipoCombustible(tipoCombustibleGasolina);//consultar tipo combustible 
			TipoVehiculo tipoPesado = tipoVehiculoDAO.find(3);
			vehiculo.setTipoVehiculo(tipoPesado);
			Servicio servicioPArticular = servicioDAO.find(3);
			vehiculo.setServicio(servicioPArticular);
			ClaseVehiculo claseAutomovil = claseDAO.find(1);
			vehiculo.setClaseVehiculo(claseAutomovil);
			vehiculo.setConversionGnv("NO");
			vehiculo.setCilindraje(12000);
			vehiculo.setFechaSoat(new Timestamp(new Date().getTime()));
			vehiculo.setDiseno("CONVENCIONAL");
			
			//esta seria la informacion minima
			Revision revision = new Revision();
			revision.setVehiculo(vehiculo);
			revision.setNumeroInspecciones(1);
			
			Usuario usuarioPorDefecto = usuarioDAO.find(11);
			vehiculo.setUsuario(usuarioPorDefecto);
			revision.setUsuario(usuarioPorDefecto);
			
			revisionService.registrarRevisionSinPropietario(revision, vehiculo, usuarioPorDefecto);
			
		}catch(Exception exc){
			exc.printStackTrace();
			fail("Falla tratando de registrar revision sin propietario");
		}
		
	}
	
	
}
