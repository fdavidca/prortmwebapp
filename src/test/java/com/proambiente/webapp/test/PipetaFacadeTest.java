package com.proambiente.webapp.test;

import java.io.File;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.proambiente.modelo.Marca;
import com.proambiente.modelo.Pipeta;
import com.proambiente.modelo.TipoMuestra;
import com.proambiente.modelo.dto.EquipoDTO;
import com.proambiente.webapp.dao.AbstractFacade;
import com.proambiente.webapp.dao.PipetaFacade;

@RunWith(Arquillian.class)
public class PipetaFacadeTest {
	
	@Inject
	PipetaFacade pipetaFacade;
	
	
	
	
	@Deployment
	public static WebArchive createDeployment(){		
		
		File[] files = Maven.resolver().loadPomFromFile("pom.xml").resolve("joda-time:joda-time:2.3").withTransitivity().asFile();		
		return ShrinkWrap.create(WebArchive.class,"test.war").addAsWebInfResource("test-ds.xml")
				.addAsLibraries(files)
				.addClass(AbstractFacade.class).addPackage(Pipeta.class.getPackage())
				.addClass(PipetaFacade.class).addClass(EquipoDTO.class)
				.addAsResource("META-INF/test-persistence.xml","META-INF/persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE,"beans.xml");
	}
	
	@Test
	public void testEncontrarPipeta(){
		try{
			Pipeta pipeta = pipetaFacade.find(1); 
			System.out.println("37j : " + pipeta);
		}catch(Exception exc){
			exc.printStackTrace();
			Assert.fail("Error consultar entidad por id");
		}
				
	}
	
	@Test
	public void testListarPipetas(){
		try{
			List<Pipeta> listaPipetas = pipetaFacade.findRange(new int[] {4,120});
			System.out.println("Lista  encontrada: " + listaPipetas.size());
		}catch(Exception exc){
			exc.printStackTrace();
			Assert.fail("error consultando lista");
		}
	}
	
	@Test
	public void testGuardarPipeta() {
		try {
			
			Pipeta pipeta = new Pipeta();
			pipeta.setAgotada(false);
			pipeta.setAnulada(false);
			pipeta.setFabricante("CRYOGAS");
			pipeta.setFechaAgotada(new Date());
			pipeta.setFechaCreacion(new Date());
			pipeta.setLaboratorioEmisorCertificado("EUROMETRIC");
			pipeta.setNumeroCertificado("45623-8954");
			pipeta.setNumeroCilindro("54623-2");
			pipeta.setNumeroLote("85632");
			pipeta.setTipoMuestra(TipoMuestra.MEDIA);
			pipeta.setValorCO(1.0);
			pipeta.setValorCO2(6.0);
			pipeta.setValorHC(300);
			pipeta.setValorO2(0.0);
			
			
			pipetaFacade.edit(pipeta);
			
		}catch(Exception exc) {
			exc.printStackTrace();
			Assert.fail("error guardando entidad");
		}
	}

}
