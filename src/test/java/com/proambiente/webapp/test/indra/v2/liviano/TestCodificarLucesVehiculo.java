package com.proambiente.webapp.test.indra.v2.liviano;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.proambiente.modelo.Medida;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.TipoMedida;
import com.proambiente.modelo.Usuario;
import com.proambiente.webapp.dao.UsuarioFacade;
import com.proambiente.webapp.dao.VehiculoFacade;
import com.proambiente.webapp.service.PruebasService;
import com.proambiente.webapp.service.indra.CodificadorFurLucesIndraV2;
import com.proambiente.webapp.util.ConstantesMedidasLuces;

/**
 * Hace test de todos los metodos de CONSULTA (por lo pronto) en la clas
 * RevisionBean
 * 
 * @author FABIAN
 *
 */

@RunWith(MockitoJUnitRunner.class)
public class TestCodificarLucesVehiculo {

	private static final Logger logger = Logger.getLogger(TestCodificarLucesVehiculo.class.getName());

	@Inject
	UsuarioFacade usuarioDAO;





	@Inject
	VehiculoFacade vehiculoDAO;




	@Mock
	PruebasService pruebaService;



	private CodificadorFurLucesIndraV2 codificador = new CodificadorFurLucesIndraV2();
	

	
	public Usuario configurarUsuario() {
		Usuario usuario = Mockito.mock(Usuario.class);
		Mockito.when(usuario.getCedula()).thenReturn(41956l);
		Mockito.when(usuario.getUsuarioId()).thenReturn(13);
		Mockito.when(usuario.getNombres()).thenReturn("Proambiente 1");
		Mockito.when(usuario.getApellidos()).thenReturn("Servicio");
		return usuario;
	}
	
	public void configurarPruebaService(Prueba p) {
		
		List<Medida> medidasLuces = new ArrayList<>();
		Medida m1 = crearMedida(2.4,ConstantesMedidasLuces.INCLINACION_BAJA_DERECHA,89588);
		medidasLuces.add(m1);
		
		Medida m2 = crearMedida(1.7,ConstantesMedidasLuces.INCLINACION_BAJA_IZQUIERDA,89589);
		medidasLuces.add(m2);
		
		Medida m3 = crearMedida(18.9,ConstantesMedidasLuces.INTENSIDAD_BAJA_DERECHA,89590);
		medidasLuces.add(m3);
		
		Medida m4 = crearMedida(6.8,ConstantesMedidasLuces.INTENSIDAD_ALTA_DERECHA,89591);
		medidasLuces.add(m4);
		
		Medida m5 = crearMedida(10.9,ConstantesMedidasLuces.INTENSIDAD_BAJA_IZQUIERDA,89592);
		medidasLuces.add(m5);
		
		Medida m6 = crearMedida(12.5,ConstantesMedidasLuces.INTENSIDAD_ALTA_IZQUIERDA,89593);
		medidasLuces.add(m6);
		
		Medida m7 = crearMedida(41.4,ConstantesMedidasLuces.SUMA_LUCES,89594);
		medidasLuces.add(m7);
		
		Medida m8 = crearMedida(0.0,ConstantesMedidasLuces.LUCES_BAJAS_ALTAS_EXPL_SIMULTANEAS,89595);
		medidasLuces.add(m8);
		
		Medida m9 = crearMedida(6.1,ConstantesMedidasLuces.INTENSIDAD_EXPLORADORA_DERECHA,89596);
		medidasLuces.add(m9);
		
		Medida m10 = crearMedida(5.5,ConstantesMedidasLuces.INTENSIDAD_EXPLORADORA_IZQUIERDA,89597);
		medidasLuces.add(m10);
		
		Mockito.when(pruebaService.obtenerMedidas(p)).thenReturn(medidasLuces);
	}
	
	private TipoMedida crearTipoMedida(Integer tipoMedidaId) {
		TipoMedida tm = new TipoMedida();
		tm.setTipoMedidaId(tipoMedidaId);
		return tm;
	}
	
	private Medida crearMedida(double valor,int tipoMedidaId,int medidaId) {
		
		Medida m1 = new Medida();
		m1.setTipoMedida(crearTipoMedida(tipoMedidaId));
		m1.setValor(valor);
		m1.setMedidaId(medidaId);
		
		return m1;
		
		
	}
	
	
	@Test
	public void testGenerarInformacionLuces() {
		try {
			StringBuilder sb = new StringBuilder();
			Prueba prueba = new Prueba();
			prueba.setPruebaId(14604);

			Usuario usuario = configurarUsuario();
			configurarPruebaService(prueba);

			List<Medida> medidas = pruebaService.obtenerMedidas(prueba);			
			codificador.codificarResultadoLucesVehiculo(medidas, usuario, sb);

			String cadenaEsperada = "Proambiente 1 Servicio;7852368;9,4;;;;;;;;2,6;;;;;;;51,2;;;;;;;;;;;;;;;|";
			String cadenaGenerada = sb.toString();

			logger.log(Level.INFO, sb.toString());

			logger.log(Level.INFO, cadenaEsperada);

			Boolean cadenasIguales = cadenaEsperada.equals(cadenaGenerada);
			
			int numeroSeparadores = StringUtils.countMatches(sb.toString(),";");
			
			Assert.assertTrue(numeroSeparadores==32);

			Assert.assertTrue(cadenasIguales);

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error test", e);
			Assert.fail("Error");
		}
	}


	
	
	

}
