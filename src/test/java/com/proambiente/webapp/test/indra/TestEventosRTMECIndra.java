package com.proambiente.webapp.test.indra;

import static org.junit.Assert.fail;

import java.io.File;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.proambiente.indra.cliente.Sicov;
import com.proambiente.indra.cliente.SicovSoap;
import com.proambiente.modelo.Color;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.dto.ResultadoOttoMotocicletasDTO;
import com.proambiente.webapp.dao.EquipoFacade;
import com.proambiente.webapp.dao.FotoFacade;
import com.proambiente.webapp.dao.InformacionCdaFacade;
import com.proambiente.webapp.dao.PruebaFacade;
import com.proambiente.webapp.service.AlertasDeHoyService;
import com.proambiente.webapp.service.CertificadoService;
import com.proambiente.webapp.service.DefectoService;
import com.proambiente.webapp.service.InspeccionService;
import com.proambiente.webapp.service.PasswordHash;
import com.proambiente.webapp.service.PruebasService;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.service.FechaService;
import com.proambiente.webapp.service.VehiculoService;
import com.proambiente.webapp.service.builderdto.RevisionDTOConstructor;
import com.proambiente.webapp.service.generadordto.GeneradorDTOResultados;
import com.proambiente.webapp.service.indra.CodificadorEventosIndra;
import com.proambiente.webapp.service.indra.CodificadorFurIndra;
import com.proambiente.webapp.service.indra.IndraEncryptor;
import com.proambiente.webapp.service.indra.ManejadorCodificarIndra;
import com.proambiente.webapp.service.indra.OperacionesSicovIndraV2;
import com.proambiente.webapp.service.indra.UtilIndraEquipos;
import com.proambiente.webapp.service.indra.WebServiceIndraProducer;
import com.proambiente.webapp.service.notification.NotificationPrimariaService;
import com.proambiente.webapp.service.sicov.EtapaRevision;
import com.proambiente.webapp.service.sicov.GeneradorInfoEventoRTMEC;
import com.proambiente.webapp.service.sicov.OperacionesSicov;
import com.proambiente.webapp.service.sicov.OperadorSicovDummy;
import com.proambiente.webapp.service.util.UtilInformesDefectos;
import com.proambiente.webapp.service.util.UtilInformesMedidas;
import com.proambiente.webapp.util.ConstantesDefectosTaximetro;
import com.proambiente.webapp.util.ConstantesIndra;
import com.proambiente.webapp.util.ConstantesMedidasAlineacion;
import com.proambiente.webapp.util.ConstantesMedidasFrenos;
import com.proambiente.webapp.util.ConstantesMedidasGases;
import com.proambiente.webapp.util.ConstantesMedidasSonometria;
import com.proambiente.webapp.util.ConstantesMedidasSuspension;
import com.proambiente.webapp.util.ConstantesMedidasTaximetro;
import com.proambiente.webapp.util.ConstantesTiposCombustible;
import com.proambiente.webapp.util.ConstantesTiposEquipo;
import com.proambiente.webapp.util.ConstantesTiposPrueba;
import com.proambiente.webapp.util.ConstantesTiposVehiculo;
import com.proambiente.webapp.service.PinNoEncontradoException;
import com.proambiente.webapp.service.UtilInformacionProfundidadLabrado;

/**
 * Hace test de todos los metodos de CONSULTA (por lo pronto) en la clas
 * RevisionBean
 * 
 * @author FABIAN
 *
 */

@RunWith(Arquillian.class)
public class TestEventosRTMECIndra {

	private static final Logger logger = Logger.getLogger(TestEventosRTMECIndra.class.getName());

	

	@Inject
	RevisionService revisionService;

	@Inject
	PruebasService pruebaService;
	
	
	@Inject
	PruebaFacade pruebaDAO;
	
	@Inject
	EquipoFacade equipoService;

	@Inject
	VehiculoService vehiculoService;
	
	@Inject
	UtilIndraEquipos utilIndraEquipos;

	@Inject
	GeneradorInfoEventoRTMEC generadorEventos;
	
	@Inject
	OperacionesSicovIndraV2 operadorIndra;
	
	private static final String iv = "sicovcontacindra";
	private static final String key = "v239pShjXXXXXXXXXXXXXXXXXXXXXXXX";

	Sicov sicov = new Sicov();
	SicovSoap sicovSoap = sicov.getSicovSoap();

	@Deployment
	public static WebArchive createDeployment() {

		File[] files = Maven.resolver().loadPomFromFile("pom.xml").importRuntimeDependencies().resolve()
				.withTransitivity().asFile();
		return ShrinkWrap.create(WebArchive.class, "test.war").addAsWebInfResource("test-ds.xml").addAsLibraries(files)

				.addPackage(Color.class.getPackage())
				.addPackage(ResultadoOttoMotocicletasDTO.class.getPackage())
				.addPackage("com.proambiente.webapp.dao")
				.addClass(EquipoFacade.class)
				.addClass(PruebasService.class)
				.addClass(VehiculoService.class)
				.addClass(UtilIndraEquipos.class)
				.addClass(ConstantesTiposPrueba.class)
				.addClass(ConstantesTiposEquipo.class)
				.addClass(ConstantesIndra.class)
				.addClass(GeneradorInfoEventoRTMEC.class)
				.addClass(RevisionDTOConstructor.class)
				.addClass(OperacionesSicov.class)
				.addClass(OperadorSicovDummy.class)
				.addClass(EtapaRevision.class)
				.addClass(PasswordHash.class)
				.addClass(ManejadorCodificarIndra.class)
				.addClass(WebServiceIndraProducer.class)
				.addClass(OperacionesSicovIndraV2.class)
				.addClass(CodificadorEventosIndra.class)
				.addClass(CodificadorFurIndra.class)
				.addClass(UtilInformesMedidas.class)
				.addClass(UtilInformesDefectos.class)
				.addClass(GeneradorDTOResultados.class)
				.addClass(NotificationPrimariaService.class)
				.addClass(AlertasDeHoyService.class)
				.addClass(FotoFacade.class)
				.addClass(DefectoService.class)
				.addClass(FechaService.class)
				.addClass(PinNoEncontradoException.class)
				.addClass(UtilInformacionProfundidadLabrado.class)
				.addClass(IndraEncryptor.class)
				.addClass(CertificadoService.class)
				.addClass(InformacionCdaFacade.class)
				.addClass(ConstantesMedidasGases.class).addClass(ConstantesMedidasFrenos.class)
				.addClass(ConstantesMedidasAlineacion.class).addClass(ConstantesMedidasSuspension.class)
				.addClass(ConstantesMedidasSonometria.class).addClass(ConstantesMedidasTaximetro.class)
				.addClass(ConstantesDefectosTaximetro.class).addClass(ConstantesTiposPrueba.class)
				.addClass(ConstantesTiposVehiculo.class).addClass(ConstantesTiposCombustible.class)
				.addClass(RevisionService.class)//estas son minimas para que el test se ejecute
				.addClass(InspeccionService.class)//estas son minimas para que el test se ejecute
				.addPackage(Sicov.class.getPackage())
				.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	
	@Test
	public void testEventoRTMEC(){
		try{
			Prueba prueba = pruebaDAO.obtenerPruebaAnalizador(793);
			List<String> serieEquipos = equipoService.consultarEquiposPorPruebaId(814);
			generadorEventos.setOperacionesSicov(operadorIndra);
			generadorEventos.generarInfoEventoRTMEC(prueba,serieEquipos);
		}catch(Exception exc){
			logger.log(Level.SEVERE, " Error", exc);
			fail("Error ");
		}
	}
}
