package com.proambiente.webapp.test.indra;

import static org.junit.Assert.fail;

import java.io.File;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.proambiente.indra.cliente.Sicov;
import com.proambiente.modelo.Color;
import com.proambiente.modelo.Defecto;
import com.proambiente.modelo.Foto;
import com.proambiente.modelo.Medida;
import com.proambiente.modelo.Propietario;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Usuario;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.modelo.Vehiculo_;
import com.proambiente.modelo.dto.ResultadoOttoMotocicletasDTO;
import com.proambiente.webapp.dao.ColorFacade;
import com.proambiente.webapp.dao.FotoFacade;
import com.proambiente.webapp.dao.MarcaFacade;
import com.proambiente.webapp.dao.PropietarioFacade;
import com.proambiente.webapp.dao.RevisionFacade;
import com.proambiente.webapp.dao.UsuarioFacade;
import com.proambiente.webapp.dao.VehiculoFacade;
import com.proambiente.webapp.service.DefectoService;
import com.proambiente.webapp.service.FechaService;
import com.proambiente.webapp.service.InspeccionService;
import com.proambiente.webapp.service.PasswordHash;
import com.proambiente.webapp.service.PinNoEncontradoException;
import com.proambiente.webapp.service.PruebasService;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.service.UtilInformacionProfundidadLabrado;
import com.proambiente.webapp.service.generadordto.GeneradorDTOResultados;
import com.proambiente.webapp.service.indra.CodificadorFurIndra;
import com.proambiente.webapp.service.indra.CodificadorResultadoTaximetro;
import com.proambiente.webapp.service.indra.UtilConvertirServicioSuperintendencia;
import com.proambiente.webapp.service.indra.UtilTipoMotor;
import com.proambiente.webapp.service.sicov.UtilEncontrarPruebas;
import com.proambiente.webapp.service.util.UtilInformesDefectos;
import com.proambiente.webapp.service.util.UtilInformesMedidas;
import com.proambiente.webapp.util.ConstantesDefectosTaximetro;
import com.proambiente.webapp.util.ConstantesMedidasAlineacion;
import com.proambiente.webapp.util.ConstantesMedidasFrenos;
import com.proambiente.webapp.util.ConstantesMedidasGases;
import com.proambiente.webapp.util.ConstantesMedidasSonometria;
import com.proambiente.webapp.util.ConstantesMedidasSuspension;
import com.proambiente.webapp.util.ConstantesMedidasTaximetro;

/**
 * Hace test de todos los metodos de CONSULTA (por lo pronto) en la clas
 * RevisionBean
 * 
 * @author FABIAN
 *
 */

@RunWith(Arquillian.class)
public class TestInfoFurIndra {

	private static final Logger logger = Logger.getLogger(TestInfoFurIndra.class.getName());

	@Inject
	UsuarioFacade usuarioDAO;

	@Inject
	RevisionFacade revisionDAO;

	@Inject
	FotoFacade fotoDAO;

	@Inject
	VehiculoFacade vehiculoDAO;

	@Inject
	PropietarioFacade propietarioDAO;

	@Inject
	RevisionService revisionService;

	@Inject
	PruebasService pruebaService;

	@Inject
	DefectoService defectoService;

	private CodificadorFurIndra codificador = new CodificadorFurIndra();
	

	

	@Deployment
	public static WebArchive createDeployment() {

		File[] files = Maven.resolver().loadPomFromFile("pom.xml").importRuntimeDependencies().resolve()
				.withTransitivity().asFile();
		return ShrinkWrap.create(WebArchive.class, "test.war").addAsWebInfResource("test-ds.xml").addAsLibraries(files)

				.addPackage(Color.class.getPackage()).addPackage(ResultadoOttoMotocicletasDTO.class.getPackage())
				.addPackage("com.proambiente.webapp.dao").addClass(MarcaFacade.class).addClass(ColorFacade.class)
				.addClass(VehiculoFacade.class).addClass(PruebasService.class).addClass(PropietarioFacade.class)
				.addClass(InspeccionService.class).addClass(RevisionService.class).addClass(DefectoService.class)
				.addClass(FechaService.class)
				.addClass(PinNoEncontradoException.class)
				.addClass(UtilInformacionProfundidadLabrado.class)
				.addClass(PasswordHash.class).addClass(CodificadorFurIndra.class).addClass(UtilInformesMedidas.class)
				.addClass(UtilInformesDefectos.class).addClass(ConstantesMedidasGases.class)
				.addClass(UtilConvertirServicioSuperintendencia.class).addClass(UtilTipoMotor.class)
				.addClass(ConstantesMedidasFrenos.class).addClass(ConstantesMedidasAlineacion.class)
				.addClass(ConstantesMedidasSuspension.class).addClass(ConstantesMedidasSonometria.class)
				.addClass(ConstantesMedidasTaximetro.class).addClass(ConstantesDefectosTaximetro.class)
				.addClass(GeneradorDTOResultados.class).addClass(CodificadorResultadoTaximetro.class)
				.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	/**
	 * 
	 */
	@Test
	public void testGenerarInforPropietarioIndra() {
		try {

			Propietario propietario = propietarioDAO.find(4192845l);
			StringBuilder sb = new StringBuilder();
			codificador.generarInformacionPropietario(sb, propietario);
			String cadenaEsperada = "FABIAN CAMARGO;C;4192845;FATIMA;7850744;MEDELLIN;ANTIOQUIA|";
			Assert.assertEquals(cadenaEsperada, sb.toString());

		} catch (Exception exc) {
			exc.printStackTrace();
			fail("Prueba fallida para metodo  testGenerarInfoPropietarioIndra");
		}
	}

	@Test
	public void testGenerarInformacionVehiculo() {
		try {
			Revision revision = revisionService.cargarRevision(48);
			String cadenaEsperada = "Proambiente 1 Servicio;7852368;OTTOV2;COLOMBIA;1;4;CHEVROLET;ASTRA ;2000;"
					+ "YHNYHNYHN;2016-11-16 00:00:00;VERDE;1;YHNUJMQAZASDFQWERT;QAZWSXEDC;4;1600;78526;5;FALSE;"
					+ "FALSE;FALSE;2017-01-12 10:08:48;2|";
//			String cadenaEsperada = "Jairo Emilio prieto;" + 
//					"4564564534;ABC123;Colombia;1;1;RENAULT;LOGAN;2005;45454545;2016-05-12 12:56:23;PLATA" + 
//					"ESCUNA;1;GHJ12313123;DAS12;1;1800;9600;4;true;true;true;2016-05-12 12:00:00;1|";
			StringBuilder sb = new StringBuilder();
			Usuario usuario = usuarioDAO.find(11);
			Vehiculo vehiculo = vehiculoDAO.findVehiculoLoadGraph(revision.getVehiculo().getVehiculoId(),
					Vehiculo_.pais);
			Assert.assertNotNull(vehiculo);
			codificador.generarInformacionVehiculo(sb, revision.getVehiculo(), revision, usuario,false);
			logger.log(Level.INFO, "Generada " + sb.toString().length());
			logger.log(Level.INFO, "Esperada " + cadenaEsperada.length());
			int separadores = StringUtils.countMatches(cadenaEsperada,";");
			logger.log(Level.INFO,"Numero de separadores " + separadores);
			int numeroSeparadores = StringUtils.countMatches(sb.toString(), ";");
			Assert.assertEquals(numeroSeparadores,23);
			
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error", e);
			fail("Prueba fallida para metodo");
		}
	}

	@Test
	public void testGenerarInformationFotoOk() {
		try {
			Prueba prueba = new Prueba();
			prueba.setPruebaId(533);
			Foto foto = fotoDAO.consultarFotoPorPrueba(prueba);
			StringBuilder sb = new StringBuilder();
			codificador.codificarFoto(sb, foto, "OTTOV2", prueba.isAbortada());
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error", e);
			Assert.fail("Error");
		}
	}
	
	
	@Test
	public void testGenerarInformationFotoAbortada() {
		try {
			Prueba prueba = new Prueba();
			prueba.setPruebaId(686);
			prueba.setAbortada(true);
			Foto foto = fotoDAO.consultarFotoPorPrueba(prueba);
			StringBuilder sb = new StringBuilder();
			codificador.codificarFoto(sb, foto, "OTTOV2", prueba.isAbortada());			
			logger.log(Level.INFO, "Foto abortada:" + sb.toString());
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error", e);
			Assert.fail("Error");
		}
	}
	
	@Test
	public void testGenerarInformationFotoNoAbortada() {
		try {
			Prueba prueba = new Prueba();
			prueba.setPruebaId(686);
			prueba.setAbortada(false);
			Foto foto = fotoDAO.consultarFotoPorPrueba(prueba);
			StringBuilder sb = new StringBuilder();
			codificador.codificarFoto(sb, foto, "OTTOV2", prueba.isAbortada());			
			Assert.fail("Debe aparecer error si la prueba no es abortada");
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error", e);			
		}
	}

	@Test
	public void testGenerarInformacionGasesOtto() {
		try {
			StringBuilder sb = new StringBuilder();
			Prueba prueba = new Prueba();
			prueba.setPruebaId(394);
			Usuario usuario = usuarioDAO.find(11);
			List<Medida> medidas = pruebaService.obtenerMedidas(prueba);
			String cadenaEsperada = "Proambiente 1 Servicio;7852368;66;830;401;3,77;13,3;1,65;66;2400;401;3,77;13,3;1,65;FALSE|FALSE;FALSE;FALSE;FALSE;FALSE;FALSE;FALSE;FALSE|";
			List<Defecto> defectos = new ArrayList<>();
			codificador.codificadorInformacionGasesOttoVehiculo(sb, medidas, defectos, usuario, false);
			logger.info(sb.toString());
			logger.info(cadenaEsperada);
			Assert.assertEquals(cadenaEsperada.length(), sb.toString().length());

		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Error", exc);
			Assert.fail("Error");
		}
	}
	
	@Test
	public void testGenerarInformacionGasesOttoAbortada(){
		try{
			StringBuilder sb = new StringBuilder();
			Prueba prueba = new Prueba();
			prueba.setPruebaId(691);
			Usuario usuario = usuarioDAO.find(11);
			List<Medida> medidas = pruebaService.obtenerMedidas(prueba);
			String cadenaEsperada = "Proambiente 1 Servicio;7852368;;;;;;;;;;;;;FALSE|FALSE;FALSE;FALSE;FALSE;FALSE;FALSE;FALSE;FALSE|";
			List<Defecto> defectos = new ArrayList<>();
			codificador.codificadorInformacionGasesOttoVehiculo(sb, medidas, defectos, usuario, false);
			logger.info(sb.toString());
			logger.info(cadenaEsperada);
			Assert.assertEquals(cadenaEsperada.length(), sb.toString().length());
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error", exc);
			Assert.fail("Error");
		}
	}
	
	@Test
	public void testGenerarInformacionGasesOttoDefectos(){
		try{
			StringBuilder sb = new StringBuilder();
			Prueba prueba = new Prueba();
			prueba.setPruebaId(699);
			Usuario usuario = usuarioDAO.find(11);
			List<Medida> medidas = pruebaService.obtenerMedidas(prueba);
			String cadenaEspecificacion = "JAIRO EMILIO PRIETO; 456456452;1;2;3;4;5;6;7;8;9;10;11;12;TRUE|FALSE;FALSE;FALSE;FALSE;FALSE;FALSE;FALSE;FALSE";
			String cadenaEsperada = "Proambiente 1 Servicio;7852368;;;;;;;;;;;;;FALSE|FALSE;FALSE;FALSE;TRUE;FALSE;FALSE;FALSE;FALSE|";
			List<Defecto> defectos = pruebaService.obtenerDefectos(prueba);
			codificador.codificadorInformacionGasesOttoVehiculo(sb, medidas, defectos, usuario, false);
			logger.info(sb.toString());
			logger.info(cadenaEsperada);
			Assert.assertEquals(cadenaEsperada.length(), sb.toString().length());
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error", exc);
			Assert.fail("Error");
		}
	}

	@Test
	public void testGenerarInformacionDiesel() {
		try {
			StringBuilder sb = new StringBuilder();
			Prueba prueba = new Prueba();
			prueba.setPruebaId(196);

			Usuario usuario = usuarioDAO.find(11);

			List<Medida> medidas = pruebaService.obtenerMedidas(prueba);
			String cadenaEsperada = "Proambiente 1 Servicio;7852368;19;5900;28,0;28,8;28,8;27,9;28,5|FALSE;FALSE;FALSE;FALSE;FALSE;FALSE;FALSE;FALSE;FALSE|";
			List<Defecto> defectos = new ArrayList<>();
			codificador.codificarInformacionGasesDiesel(sb, medidas, defectos, usuario);
			String cadenaGenerada = sb.toString();
			logger.info(cadenaGenerada);
			logger.info(cadenaEsperada);

			Boolean longitudCadenasIguales = cadenaGenerada.equals(cadenaEsperada);
			Assert.assertTrue(longitudCadenasIguales);

		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Error", exc);
			Assert.fail("Error");
		}
	}

	@Test
	public void testGenerarInformacionLucesOk() {
		try {
			StringBuilder sb = new StringBuilder();
			Prueba prueba = new Prueba();
			prueba.setPruebaId(388);
			String cadenaEsperada = "Proambiente 1 Servicio;7852368;1,0;2,0;0,4;3,6;40,0|";
			Usuario usuario = usuarioDAO.find(11);

			List<Medida> medidas = pruebaService.obtenerMedidas(prueba);
			codificador.codificarResultadoLuces(sb, medidas, usuario,false);

			String cadenaGenerada = sb.toString();

			logger.log(Level.INFO, cadenaGenerada);

			logger.log(Level.INFO, cadenaEsperada);

			Boolean cadenasIguales = cadenaEsperada.equals(cadenaGenerada);

			Assert.assertTrue(cadenasIguales);

		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Error testGenerarInformacionLuces", exc);
			Assert.fail("Error testGenerarInformacionLuces");
		}
	}

	@Test
	public void testInformacionLucesAbortada() {
		try {
			StringBuilder sb = new StringBuilder();
			Prueba prueba = new Prueba();
			prueba.setPruebaId(685);
			String cadenaEsperada = "Proambiente 1 Servicio;7852368;;;;;|";
			Usuario usuario = usuarioDAO.find(11);

			List<Medida> medidas = pruebaService.obtenerMedidas(prueba);
			codificador.codificarResultadoLuces(sb, medidas, usuario,false);

			String cadenaGenerada = sb.toString();

			logger.log(Level.INFO, cadenaGenerada);

			logger.log(Level.INFO, cadenaEsperada);

			Boolean cadenasIguales = cadenaEsperada.equals(cadenaGenerada);

			Assert.assertTrue(cadenasIguales);

		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Error testGenerarInformacionLuces", exc);
			Assert.fail("Error testGenerarInformacionLuces");
		}
	}
	
	
	@Test
	public void testResultadoSonometroAbortada() {
		try {
			StringBuilder sb = new StringBuilder();
			Prueba prueba = new Prueba();
			prueba.setPruebaId(690);

			Usuario usuario = usuarioDAO.find(11);

			List<Medida> medidas = pruebaService.obtenerMedidas(prueba);
			codificador.codificarResultadoSonometria(sb, medidas, usuario);
			String esperada = "Proambiente 1 Servicio;7852368;|";
			String generada = sb.toString();

			logger.log(Level.INFO, generada);

			logger.log(Level.INFO, esperada);

			Boolean cadenasIguales = generada.equals(esperada);

			Assert.assertTrue(cadenasIguales);

		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Error testResultadoSonometro", exc);
			Assert.fail("Error testResultadoSonometro");
		}
	}
	

	@Test
	public void testStringBuilder() {
		try {
			String miNombre = "Fabian David Camargo; Amaya 2.8";
			StringBuilder sb = new StringBuilder();
			DecimalFormat df = new DecimalFormat();
			df.applyPattern("0.0");
			DecimalFormatSymbols ds = DecimalFormatSymbols.getInstance();
			ds.setDecimalSeparator('.');
			df.setDecimalFormatSymbols(ds);		
			Double dos = new Double(2.8d);
			sb.append("Fabian ").append("David ").append("Camargo; ").append("Amaya ").append(df.format(dos));
			boolean cadenasIguales = miNombre.equals(sb.toString());
			Assert.assertTrue(cadenasIguales);

		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Error", exc);
			Assert.fail("No se pudieron comparar las cadenas");
		}
	}

	@Test
	public void testGenerarInformacionGasesMotos() {
		try {
			StringBuilder sb = new StringBuilder();
			Prueba prueba = new Prueba();
			prueba.setPruebaId(361);

			Usuario usuario = usuarioDAO.find(11);

			List<Medida> medidas = pruebaService.obtenerMedidas(prueba);
			List<Defecto> defectos = new ArrayList<>();
			codificador.codificarInformacionGasesMotocicleta(sb, medidas, defectos, usuario, false);

			String cadenaEsperada = "Proambiente 1 Servicio;7852368;87;1582;4;4,08;12,0;1,65|FALSE;FALSE;FALSE;FALSE;FALSE;FALSE;FALSE|";
			String cadenaGenerada = sb.toString();

			logger.log(Level.INFO, sb.toString());

			logger.log(Level.INFO, cadenaEsperada);

			Boolean cadenasIguales = cadenaEsperada.equals(cadenaGenerada);

			Assert.assertTrue(cadenasIguales);

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error test", e);
			Assert.fail("Error");
		}
	}
	
	@Test
	public void testGenerarInformacionGasesMotosAbortada() {
		try {
			StringBuilder sb = new StringBuilder();
			Prueba prueba = new Prueba();
			prueba.setPruebaId(705);

			Usuario usuario = usuarioDAO.find(11);

			List<Medida> medidas = pruebaService.obtenerMedidas(prueba);
			List<Defecto> defectos = new ArrayList<>();
			codificador.codificarInformacionGasesMotocicleta(sb, medidas, defectos, usuario, false);

			String cadenaEsperada = "Proambiente 1 Servicio;7852368;;;;;;|FALSE;FALSE;FALSE;FALSE;FALSE;FALSE;FALSE|";
			String cadenaGenerada = sb.toString();

			logger.log(Level.INFO, sb.toString());

			logger.log(Level.INFO, cadenaEsperada);

			Boolean cadenasIguales = cadenaEsperada.equals(cadenaGenerada);

			Assert.assertTrue(cadenasIguales);

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error test", e);
			Assert.fail("Error");
		}
	}

	
	@Test
	public void testGenerarInformacionGasesMotosDefecto() {
		try {
			StringBuilder sb = new StringBuilder();
			Prueba prueba = new Prueba();
			prueba.setPruebaId(705);

			Usuario usuario = usuarioDAO.find(11);

			List<Medida> medidas = pruebaService.obtenerMedidas(prueba);
			List<Defecto> defectos = new ArrayList<>();
			codificador.codificarInformacionGasesMotocicleta(sb, medidas, defectos, usuario, false);

			String cadenaEsperada = "Proambiente 1 Servicio;7852368;;;;;;|FALSE;FALSE;FALSE;FALSE;FALSE;FALSE;FALSE|";
			String cadenaGenerada = sb.toString();

			logger.log(Level.INFO, sb.toString());

			logger.log(Level.INFO, cadenaEsperada);

			Boolean cadenasIguales = cadenaEsperada.equals(cadenaGenerada);

			Assert.assertTrue(cadenasIguales);

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error test", e);
			Assert.fail("Error");
		}
	}
	
	@Test
	public void testGenerarInformacionFAS() {
		try {
			StringBuilder sb = new StringBuilder();
			Usuario usuario = usuarioDAO.find(11);

			Prueba pruebaFrenos = new Prueba();
			pruebaFrenos.setPruebaId(391);

			Prueba pruebaAlineacion = new Prueba();
			pruebaAlineacion.setPruebaId(390);

			Prueba pruebaSuspension = new Prueba();
			pruebaSuspension.setPruebaId(392);

			List<Medida> medidasFrenos = pruebaService.obtenerMedidas(pruebaFrenos);
			List<Medida> medidasAlineacion = pruebaService.obtenerMedidas(pruebaAlineacion);
			List<Medida> medidasSuspension = pruebaService.obtenerMedidas(pruebaSuspension);

			List<Medida> todasLasMedidas = new ArrayList<>();
			todasLasMedidas.addAll(medidasFrenos);
			todasLasMedidas.addAll(medidasSuspension);
			todasLasMedidas.addAll(medidasAlineacion);

			String cadenaEsperada = "Proambiente 1 Servicio;7852368;2;34,242;16,632;20,000;30,000;;;;;800;700;;;;;1000;1000;;;;;3156;2136;;;;;2656;2274;;;;;11,0;-11,0;;;;;39,0;39,0;39,0;39,0|";
			codificador.codificarMedidasFas(sb, todasLasMedidas, usuario, 2);

			String cadenaGenerada = sb.toString();

			logger.log(Level.INFO, cadenaGenerada);

			logger.log(Level.INFO, cadenaEsperada);

			Boolean cadenasIguales = cadenaGenerada.equals(cadenaEsperada);

			Assert.assertTrue(cadenasIguales);

		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Error testGenerarInformacionFAS", exc);
			Assert.fail("Error testGenerarInformacionFAS");
		}
	}
	
	@Test
	public void testGenerarInformacionFASAbortada() {
		try {
			StringBuilder sb = new StringBuilder();
			Usuario usuario = usuarioDAO.find(11);

			Prueba pruebaFrenos = new Prueba();
			pruebaFrenos.setPruebaId(688);

			Prueba pruebaAlineacion = new Prueba();
			pruebaAlineacion.setPruebaId(687);

			Prueba pruebaSuspension = new Prueba();
			pruebaSuspension.setPruebaId(689);

			List<Medida> medidasFrenos = pruebaService.obtenerMedidas(pruebaFrenos);
			List<Medida> medidasAlineacion = pruebaService.obtenerMedidas(pruebaAlineacion);
			List<Medida> medidasSuspension = pruebaService.obtenerMedidas(pruebaSuspension);

			List<Medida> todasLasMedidas = new ArrayList<>();
			todasLasMedidas.addAll(medidasFrenos);
			todasLasMedidas.addAll(medidasSuspension);
			todasLasMedidas.addAll(medidasAlineacion);

			String cadenaEsperada = "Proambiente 1 Servicio;7852368;2;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;|";
			codificador.codificarMedidasFas(sb, todasLasMedidas, usuario, 2);

			String cadenaGenerada = sb.toString();

			logger.log(Level.INFO, cadenaGenerada);

			logger.log(Level.INFO, cadenaEsperada);

			Boolean cadenasIguales = cadenaGenerada.equals(cadenaEsperada);

			Assert.assertTrue(cadenasIguales);

		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Error testGenerarInformacionFAS", exc);
			Assert.fail("Error testGenerarInformacionFAS");
		}
	}

	@Test
	public void testResultadoSonometro() {
		try {
			StringBuilder sb = new StringBuilder();
			Prueba prueba = new Prueba();
			prueba.setPruebaId(393);

			Usuario usuario = usuarioDAO.find(11);

			List<Medida> medidas = pruebaService.obtenerMedidas(prueba);
			codificador.codificarResultadoSonometria(sb, medidas, usuario);
			String esperada = "Proambiente 1 Servicio;7852368;95,2|";
			String generada = sb.toString();

			logger.log(Level.INFO, generada);

			logger.log(Level.INFO, esperada);

			Boolean cadenasIguales = generada.equals(esperada);

			Assert.assertTrue(cadenasIguales);

		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Error testResultadoSonometro", exc);
			Assert.fail("Error testResultadoSonometro");
		}
	}

	@Test
	public void testResultadoTaximetro() {
		try {
			StringBuilder sb = new StringBuilder();
			Prueba prueba = new Prueba();
			prueba.setPruebaId(478);
			
			CodificadorResultadoTaximetro codificadorTaxi = new CodificadorResultadoTaximetro();

			Usuario usuario = usuarioDAO.find(11);

			List<Medida> medidas = pruebaService.obtenerMedidas(prueba);
			List<Defecto> defectos = new ArrayList<>();
			String refLlanta = "R15";
			codificadorTaxi.codificarResultadoTaximetro(sb, medidas, defectos, usuario, refLlanta, Boolean.TRUE);
			String esperada = "Proambiente 1 Servicio;7852368;TRUE;FALSE;FALSE;R15;-7,0;-29,4|";
			String generada = sb.toString();
			logger.log(Level.INFO, generada);

			logger.log(Level.INFO, esperada);

			Boolean cadenasIguales = generada.equals(esperada);

			Assert.assertTrue(cadenasIguales);

		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Error testResultadoTaximetro", exc);
			Assert.fail("Error testResultadoTaximetro");
		}
	}

	@Test
	public void testCodificarDefectos() {
		try {
			StringBuilder sb = new StringBuilder();
			List<Integer> defectos = defectoService.obtenerTodosLosDefectosRevision(48);
			Usuario usuario = usuarioDAO.find(11);

			// codificador.codificarDatosDefectos(sb, defectos, usuario);
			System.out.println("Codificar Defectos");
			logger.log(Level.INFO, sb.toString());
		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Error", exc);
			Assert.fail("Error testCodificarDefectos");
		}
	}

	@Test
	public void testCodificarInfoRunt() {
		try {
			StringBuilder sb = new StringBuilder();
			Revision revision = revisionService.cargarRevision(48);
			codificador.codificarEstructuraDatosRunt(sb, revision, null, "15");
			System.out.println("Informacion Certificado");
			logger.log(Level.INFO, sb.toString());
		} catch (Exception exc) {
			logger.log(Level.SEVERE, "error testCodificarInfoRunt", exc);
			Assert.fail("Error testCodificarInfoRunt");
		}
	}

}
