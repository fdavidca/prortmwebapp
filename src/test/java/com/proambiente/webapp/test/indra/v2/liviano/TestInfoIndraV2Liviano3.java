package com.proambiente.webapp.test.indra.v2.liviano;

import static org.junit.Assert.fail;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.proambiente.modelo.Color;
import com.proambiente.modelo.dto.ResultadoOttoMotocicletasDTO;
import com.proambiente.webapp.dao.ColorFacade;
import com.proambiente.webapp.dao.FotoFacade;
import com.proambiente.webapp.dao.InformacionCdaFacade;
import com.proambiente.webapp.dao.MarcaFacade;
import com.proambiente.webapp.dao.PropietarioFacade;
import com.proambiente.webapp.dao.RevisionFacade;
import com.proambiente.webapp.dao.UsuarioFacade;
import com.proambiente.webapp.dao.VehiculoFacade;
import com.proambiente.webapp.service.CargarAnalizadorService;
import com.proambiente.webapp.service.CertificadoService;
import com.proambiente.webapp.service.CodigosDefectoNuevaResolucionService;
import com.proambiente.webapp.service.DefectoService;
import com.proambiente.webapp.service.EvaluarRevisionService;
import com.proambiente.webapp.service.FechaService;
import com.proambiente.webapp.service.InspeccionService;
import com.proambiente.webapp.service.PasswordHash;
import com.proambiente.webapp.service.PinNoEncontradoException;
import com.proambiente.webapp.service.PruebasService;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.service.UtilInformacionProfundidadLabrado;
import com.proambiente.webapp.service.VehiculoService;
import com.proambiente.webapp.service.generadordto.GeneradorDTOResultadoLuces;
import com.proambiente.webapp.service.generadordto.GeneradorDTOResultados;
import com.proambiente.webapp.service.indra.CodificadorFurIndra;
import com.proambiente.webapp.service.indra.ManejadorCodificadorIndraV2;
import com.proambiente.webapp.service.indra.ManejadorCodificarIndra;
import com.proambiente.webapp.service.util.UtilInformesDefectos;
import com.proambiente.webapp.service.util.UtilInformesMedidas;
import com.proambiente.webapp.util.ConstantesDefectosTaximetro;
import com.proambiente.webapp.util.ConstantesMedidasAlineacion;
import com.proambiente.webapp.util.ConstantesMedidasFrenos;
import com.proambiente.webapp.util.ConstantesMedidasGases;
import com.proambiente.webapp.util.ConstantesMedidasSonometria;
import com.proambiente.webapp.util.ConstantesMedidasSuspension;
import com.proambiente.webapp.util.ConstantesMedidasTaximetro;
import com.proambiente.webapp.util.ConstantesNombreSoftware;
import com.proambiente.webapp.util.ConstantesTiposCombustible;
import com.proambiente.webapp.util.ConstantesTiposPrueba;
import com.proambiente.webapp.util.ConstantesTiposVehiculo;
import com.proambiente.webapp.service.sicov.OperacionesSicov;
import com.proambiente.webapp.service.sicov.OperacionesSicov.Envio;
import com.proambiente.webapp.service.sicov.UtilEncontrarPruebas;
import com.proambiente.webapp.service.util.UtilTipoVehiculo;
import com.proambiente.webapp.service.util.UtilTipoCombustible;

	

/**
 * Hace test de todos los metodos de CONSULTA (por lo pronto) en 
 * la clas RevisionBean
 * @author FABIAN
 *
 */



@RunWith(Arquillian.class)
public class TestInfoIndraV2Liviano3 {
	
	
	private static final Logger logger = Logger.getLogger(TestInfoIndraV2Liviano3.class.getName());
	
	@Inject
	UsuarioFacade usuarioDAO;
	
	@Inject
	RevisionFacade revisionDAO;
	
	@Inject
	FotoFacade fotoDAO;
	
	@Inject
	VehiculoFacade vehiculoDAO;
	
	@Inject
	PropietarioFacade propietarioDAO;
	
	@Inject
	RevisionService revisionService;
	
	@Inject
	PruebasService pruebaService;
	
	@Inject
	DefectoService defectoService;
	
	@Inject
	InformacionCdaFacade informacionCdaFacade;
	
	@Inject
	CargarAnalizadorService cargarAnalizadorService;
	


	@Inject
	ManejadorCodificadorIndraV2 manejadorOperaciones;
	
	
	
	@Deployment
	public static WebArchive createDeployment(){		
		
		File[] files = Maven.resolver().loadPomFromFile("pom.xml").importRuntimeDependencies().resolve().withTransitivity().asFile();		
		return ShrinkWrap.create(WebArchive.class,"test.war").addAsWebInfResource("test-ds.xml")
				.addAsLibraries(files)
				
				.addPackage(Color.class.getPackage())
				.addPackage(ResultadoOttoMotocicletasDTO.class.getPackage())
				.addPackage("com.proambiente.webapp.dao")
				.addPackage("com.proambiente.webapp.service.indra")
				.addPackage("com.proambiente.webapp.service.util")
				.addClass(CodigosDefectoNuevaResolucionService.class)
			    .addClass(MarcaFacade.class)
			    .addClass(ColorFacade.class)
			    .addClass(VehiculoFacade.class)
			    .addClass(CargarAnalizadorService.class)
			    .addClass(VehiculoService.class)
			    .addClass(PruebasService.class)
			    .addClass(PropietarioFacade.class)
				.addClass(InspeccionService.class)
				.addClass(RevisionService.class)
				.addClass(EvaluarRevisionService.class)
				.addClass(DefectoService.class)
				.addClass(FechaService.class)
				.addClass(UtilEncontrarPruebas.class)
				.addClass(PinNoEncontradoException.class)
				.addClass(UtilInformacionProfundidadLabrado.class)
				.addClass(CertificadoService.class)
				.addClass(PasswordHash.class)
				.addClass(CodificadorFurIndra.class)
				.addClass(ManejadorCodificarIndra.class)
				.addClass(UtilInformesMedidas.class)
				.addClass(UtilInformesDefectos.class)
				.addClass(UtilTipoVehiculo.class)
				.addClass(UtilTipoCombustible.class)
			    .addClass(UtilInformesDefectos.class)
			    .addClass(ConstantesMedidasGases.class)
				.addClass(ConstantesMedidasFrenos.class)
				.addClass(ConstantesMedidasAlineacion.class)
				.addClass(ConstantesMedidasSuspension.class)
				.addClass(ConstantesMedidasSonometria.class)
				.addClass(ConstantesMedidasTaximetro.class)
				.addClass(ConstantesDefectosTaximetro.class)
				.addClass(ConstantesTiposPrueba.class)
				.addClass(ConstantesTiposVehiculo.class)
				.addClass(ConstantesTiposCombustible.class)
				.addClass(GeneradorDTOResultados.class)
				.addClass(ConstantesNombreSoftware.class)
				.addClass(OperacionesSicov.class)
				.addClass(GeneradorDTOResultadoLuces.class)
				.addPackage("com.proambiente.webapp.service.indra")
				.addPackage("com.proambiente.webapp.service.generadordto")
				.addPackage("com.proambiente.webapp.util.dto")
				.addAsResource("META-INF/test-persistence.xml","META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE,"beans.xml");
	}
	
	
	/**
	 * 
	 */
	@Test
	public void testcodificarFURRevisionReprobada(){
		try{
			
			String stringResultado = manejadorOperaciones.codificarFurRevision(78328,Envio.PRIMER_ENVIO);
			logger.log(Level.INFO, "Resultado" + stringResultado);
			System.out.println("Resultado: " + stringResultado);
			
		}catch(Exception exc){
			exc.printStackTrace();
			fail("Prueba fallida para metodo  testGenerarInfoPropietarioIndra");
		}
	}
	

	
	
}
