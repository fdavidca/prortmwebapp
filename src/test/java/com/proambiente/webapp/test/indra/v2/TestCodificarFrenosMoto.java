package com.proambiente.webapp.test.indra.v2;

import static org.junit.Assert.fail;

import java.io.File;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.proambiente.indra.cliente.Sicov;
import com.proambiente.modelo.Color;
import com.proambiente.modelo.Defecto;
import com.proambiente.modelo.Foto;
import com.proambiente.modelo.Medida;
import com.proambiente.modelo.Propietario;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Usuario;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.modelo.Vehiculo_;
import com.proambiente.modelo.dto.ResultadoFasDTO;
import com.proambiente.modelo.dto.ResultadoOttoMotocicletasDTO;
import com.proambiente.webapp.dao.ColorFacade;
import com.proambiente.webapp.dao.FotoFacade;
import com.proambiente.webapp.dao.MarcaFacade;
import com.proambiente.webapp.dao.PropietarioFacade;
import com.proambiente.webapp.dao.RevisionFacade;
import com.proambiente.webapp.dao.UsuarioFacade;
import com.proambiente.webapp.dao.VehiculoFacade;
import com.proambiente.webapp.service.DefectoService;
import com.proambiente.webapp.service.FechaService;
import com.proambiente.webapp.service.InspeccionService;
import com.proambiente.webapp.service.PasswordHash;
import com.proambiente.webapp.service.PinNoEncontradoException;
import com.proambiente.webapp.service.PruebasService;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.service.UtilInformacionProfundidadLabrado;
import com.proambiente.webapp.service.generadordto.GeneradorDTOResultados;
import com.proambiente.webapp.service.indra.CodificadorFurIndra;
import com.proambiente.webapp.service.indra.CodificadorFurIndraFASV2;
import com.proambiente.webapp.service.indra.CodificadorFurIndraV2;
import com.proambiente.webapp.service.indra.UtilConvertirServicioSuperintendencia;
import com.proambiente.webapp.service.indra.UtilTipoMotor;
import com.proambiente.webapp.service.sicov.UtilEncontrarPruebas;
import com.proambiente.webapp.service.util.UtilInformesDefectos;
import com.proambiente.webapp.service.util.UtilInformesMedidas;
import com.proambiente.webapp.util.ConstantesDefectosTaximetro;
import com.proambiente.webapp.util.ConstantesMedidasAlineacion;
import com.proambiente.webapp.util.ConstantesMedidasFrenos;
import com.proambiente.webapp.util.ConstantesMedidasGases;
import com.proambiente.webapp.util.ConstantesMedidasSonometria;
import com.proambiente.webapp.util.ConstantesMedidasSuspension;
import com.proambiente.webapp.util.ConstantesMedidasTaximetro;
import com.proambiente.webapp.util.dto.ResultadoOttoMotocicletasInformeDTO;

/**
 * Hace test de todos los metodos de CONSULTA (por lo pronto) en la clas
 * RevisionBean
 * 
 * @author FABIAN
 *
 */

@RunWith(Arquillian.class)
public class TestCodificarFrenosMoto {

	private static final Logger logger = Logger.getLogger(TestCodificarFrenosMoto.class.getName());

	@Inject
	UsuarioFacade usuarioDAO;

	@Inject
	RevisionFacade revisionDAO;

	@Inject
	FotoFacade fotoDAO;

	@Inject
	VehiculoFacade vehiculoDAO;

	@Inject
	PropietarioFacade propietarioDAO;

	@Inject
	RevisionService revisionService;

	@Inject
	PruebasService pruebaService;

	@Inject
	DefectoService defectoService;

	private CodificadorFurIndraFASV2 codificador = new CodificadorFurIndraFASV2();
	

	

	@Deployment
	public static WebArchive createDeployment() {

		File[] files = Maven.resolver().loadPomFromFile("pom.xml").importRuntimeDependencies().resolve()
				.withTransitivity().asFile();
		return ShrinkWrap.create(WebArchive.class, "test.war").addAsWebInfResource("test-ds.xml").addAsLibraries(files)

				.addPackage(Color.class.getPackage()).addPackage(ResultadoOttoMotocicletasDTO.class.getPackage())
				.addPackage("com.proambiente.webapp.dao").addClass(MarcaFacade.class).addClass(ColorFacade.class)
				.addClass(VehiculoFacade.class).addClass(PruebasService.class).addClass(PropietarioFacade.class)
				.addClass(InspeccionService.class).addClass(RevisionService.class).addClass(DefectoService.class)
				.addClass(FechaService.class)
				.addClass(PinNoEncontradoException.class)
				.addClass(UtilInformacionProfundidadLabrado.class)
				.addClass(PasswordHash.class).addClass(CodificadorFurIndra.class).addClass(UtilInformesMedidas.class)
				.addClass(UtilInformesDefectos.class).addClass(ConstantesMedidasGases.class)
				.addClass(UtilConvertirServicioSuperintendencia.class).addClass(UtilTipoMotor.class)
				.addClass(ConstantesMedidasFrenos.class).addClass(ConstantesMedidasAlineacion.class)
				.addClass(ConstantesMedidasSuspension.class).addClass(ConstantesMedidasSonometria.class)
				.addClass(ConstantesMedidasTaximetro.class).addClass(ConstantesDefectosTaximetro.class)
				.addClass(CodificadorFurIndraFASV2.class).addClass(ResultadoFasDTO.class)
				.addClass(GeneradorDTOResultados.class)
				.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	
	@Test
	public void testGenerarInformacionFrenosMotosAprobada() {
		try {
			StringBuilder sb = new StringBuilder();
			Prueba prueba = new Prueba();
			prueba.setPruebaId(1611);

			Usuario usuario = usuarioDAO.find(11);

			List<Medida> medidas = pruebaService.obtenerMedidas(prueba);			
			codificador.codificarResultadoFrenos(medidas, sb, usuario, 2);

			String cadenaEsperada = "Proambiente 1 Servicio;" + 
					"7852368;2;70,281;;;;;;;;;;;;;;528;574;;;;;637;931;;;;;;;;;;;;;;;;;;;;;;;;|";
			String cadenaGenerada = sb.toString();

			logger.log(Level.INFO, sb.toString());

			logger.log(Level.INFO, cadenaEsperada);

			Boolean cadenasIguales = cadenaEsperada.equals(cadenaGenerada);
			
			int numeroSeparadores = StringUtils.countMatches(sb.toString(),";");
			
			Assert.assertTrue(numeroSeparadores==48);

			Assert.assertTrue(cadenasIguales);

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error test", e);
			Assert.fail("Error");
		}
	}


	@Test
	public void testGenerarInformacionGasesMotosReprobada() {
		try {
			StringBuilder sb = new StringBuilder();
			Prueba prueba = new Prueba();
			prueba.setPruebaId(1619);

			Usuario usuario = usuarioDAO.find(11);

			List<Medida> medidas = pruebaService.obtenerMedidas(prueba);			
			codificador.codificarResultadoFrenos( medidas,sb,  usuario, 2);
			
			String cadenaEsperada = "Proambiente 1 Servicio;" + 
					"7852368;2;15,110;;;;;;;;;;;;;;215;170;;;;;1078;1470;;;;;;;;;;;;;;;;;;;;;;;;|";
			String cadenaGenerada = sb.toString();

			logger.log(Level.INFO, sb.toString());

			logger.log(Level.INFO, cadenaEsperada);

			Boolean cadenasIguales = cadenaEsperada.equals(cadenaGenerada);

			int numeroSeparadores = StringUtils.countMatches(sb.toString(),";");
			
			Assert.assertTrue(numeroSeparadores==48);
			
			Assert.assertTrue(cadenasIguales);

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error test", e);
			Assert.fail("Error");
		}
	}
	

}
