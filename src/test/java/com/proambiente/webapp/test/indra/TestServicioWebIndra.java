package com.proambiente.webapp.test.indra;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.File;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.proambiente.indra.cliente.FURRespuesta;
import com.proambiente.indra.cliente.Sicov;
import com.proambiente.indra.cliente.SicovSoap;
import com.proambiente.modelo.Color;
import com.proambiente.modelo.dto.ResultadoOttoMotocicletasDTO;
import com.proambiente.webapp.dao.ColorFacade;
import com.proambiente.webapp.dao.FotoFacade;
import com.proambiente.webapp.dao.MarcaFacade;
import com.proambiente.webapp.dao.PropietarioFacade;
import com.proambiente.webapp.dao.RevisionFacade;
import com.proambiente.webapp.dao.UsuarioFacade;
import com.proambiente.webapp.dao.VehiculoFacade;
import com.proambiente.webapp.service.CertificadoService;
import com.proambiente.webapp.service.DefectoService;
import com.proambiente.webapp.service.FechaService;
import com.proambiente.webapp.service.InspeccionService;
import com.proambiente.webapp.service.PasswordHash;
import com.proambiente.webapp.service.PruebasService;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.service.UtilInformacionProfundidadLabrado;
import com.proambiente.webapp.service.VehiculoService;
import com.proambiente.webapp.service.generadordto.GeneradorDTOResultados;
import com.proambiente.webapp.service.indra.CodificadorFurIndra;
import com.proambiente.webapp.service.indra.IndraEncryptor;
import com.proambiente.webapp.service.indra.ManejadorCodificarIndra;
import com.proambiente.webapp.service.indra.OperacionesSicovIndraV2;
import com.proambiente.webapp.service.sicov.OperacionesSicov;
import com.proambiente.webapp.service.sicov.OperacionesSicov.Envio;
import com.proambiente.webapp.service.util.UtilInformesDefectos;
import com.proambiente.webapp.service.util.UtilInformesMedidas;
import com.proambiente.webapp.util.ConstantesDefectosTaximetro;
import com.proambiente.webapp.util.ConstantesMedidasAlineacion;
import com.proambiente.webapp.util.ConstantesMedidasFrenos;
import com.proambiente.webapp.util.ConstantesMedidasGases;
import com.proambiente.webapp.util.ConstantesMedidasSonometria;
import com.proambiente.webapp.util.ConstantesMedidasSuspension;
import com.proambiente.webapp.util.ConstantesMedidasTaximetro;
import com.proambiente.webapp.util.ConstantesTiposCombustible;
import com.proambiente.webapp.util.ConstantesTiposPrueba;
import com.proambiente.webapp.util.ConstantesTiposVehiculo;



/**
 * Hace test de todos los metodos de CONSULTA (por lo pronto) en la clas
 * RevisionBean
 * 
 * @author FABIAN
 *
 */

@RunWith(Arquillian.class)
public class TestServicioWebIndra {

	private static final Logger logger = Logger.getLogger(TestServicioWebIndra.class.getName());

	@Inject
	UsuarioFacade usuarioDAO;

	@Inject
	RevisionFacade revisionDAO;

	@Inject
	FotoFacade fotoDAO;

	@Inject
	VehiculoFacade vehiculoDAO;

	@Inject
	PropietarioFacade propietarioDAO;

	@Inject
	RevisionService revisionService;

	@Inject
	PruebasService pruebaService;

	@Inject
	DefectoService defectoService;

	@Inject
	ManejadorCodificarIndra manejadorOperaciones;
	
	

	private static final String iv = "sicovcontacindra";
	private static final String key = "v239pShjXXXXXXXXXXXXXXXXXXXXXXXX";

	
	
	Sicov sicov; 	
	SicovSoap sicovSoap; 
	
	@Before
	public void inicializar() {
		
		try {		
		sicov = new Sicov();
		sicovSoap = sicov.getSicovSoap();
		}catch(Exception exc) {
			exc.printStackTrace();
		}
	}

	@Deployment
	public static WebArchive createDeployment() {

		File[] files = Maven.resolver().loadPomFromFile("pom.xml").importRuntimeDependencies().resolve()
				.withTransitivity().asFile();
		return ShrinkWrap.create(WebArchive.class, "test.war").addAsWebInfResource("test-ds.xml").addAsLibraries(files)

				.addPackage(Color.class.getPackage()).addPackage(ResultadoOttoMotocicletasDTO.class.getPackage())
				.addPackage("com.proambiente.webapp.dao").addClass(MarcaFacade.class).addClass(ColorFacade.class)
				.addClass(VehiculoFacade.class).addClass(VehiculoService.class).addClass(PruebasService.class)
				.addClass(PropietarioFacade.class).addClass(InspeccionService.class).addClass(FechaService.class)
				.addClass(RevisionService.class)
				.addClass(DefectoService.class).addClass(CertificadoService.class).addClass(PasswordHash.class)
				.addClass(CodificadorFurIndra.class).addClass(ManejadorCodificarIndra.class)
				.addClass(UtilInformesMedidas.class).addClass(UtilInformesDefectos.class)
				.addClass(ConstantesMedidasGases.class).addClass(ConstantesMedidasFrenos.class)
				.addClass(ConstantesMedidasAlineacion.class).addClass(ConstantesMedidasSuspension.class)
				.addClass(ConstantesMedidasSonometria.class).addClass(ConstantesMedidasTaximetro.class)
				.addClass(ConstantesDefectosTaximetro.class).addClass(ConstantesTiposPrueba.class)
				.addClass(ConstantesTiposVehiculo.class).addClass(ConstantesTiposCombustible.class)
				.addClass(GeneradorDTOResultados.class).addClass(IndraEncryptor.class)
				.addClass(UtilInformacionProfundidadLabrado.class)
				.addPackage(Sicov.class.getPackage()).addClass(OperacionesSicov.class)
				.addPackage("com.proambiente.webapp.service.indra")
				.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	@Test
	public void testRegistrarFur(){
		try{
			URL url = new URL("http://192.168.0.21/sicov.asmx?wsdl");
			Sicov sicov = new Sicov(url);
			SicovSoap sicovSoap = sicov.getSicovSoap();
			Integer[] revisiones = {115,116,117,118,122};
			for(int i = revisiones.length -1; i >= 0; --i){
				Integer revisionId = revisiones[i];
				String revisionCodificada = manejadorOperaciones.codificarFURRevision(revisionId,Envio.PRIMER_ENVIO);				
				logger.log(Level.INFO, "Cod " + revisionCodificada);
				String cadenaEncriptada = IndraEncryptor.encrypt(OperacionesSicovIndraV2.key, OperacionesSicovIndraV2.iv, revisionCodificada);
				FURRespuesta rta =sicovSoap.enviarFurSicov(cadenaEncriptada);
				logger.log(Level.INFO, "" + i + rta.toString());
				//assertEquals(rta.getCodRespuesta(), 1);
			}
				
		}catch(Exception exc){
			logger.log(Level.SEVERE, "error", exc);
			fail();
		}
	}
	
	@Test
	public void testRegistrarFurIndividual(){ 
		try{
			Integer revisionId = 118;
			String revisionCodificada = manejadorOperaciones.codificarFURRevision(revisionId,Envio.PRIMER_ENVIO);		
			String cadenaEncriptada = IndraEncryptor.encrypt(OperacionesSicovIndraV2.key, OperacionesSicovIndraV2.iv,revisionCodificada);
			//String cadenaDecriptada = IndraEncryptor.decrypt(key, iv, cadenaEncriptada);
			
			logger.log(Level.INFO,"Codificada " + revisionCodificada);
			logger.log(Level.INFO,"Encriptada " + cadenaEncriptada);
			//logger.log(Level.INFO, "Cadena decriptada " + cadenaDecriptada);			
			FURRespuesta rta =sicovSoap.enviarFurSicov(cadenaEncriptada);
			logger.log(Level.INFO, "Respuesta " + rta.toString());
			assertEquals(rta.getCodRespuesta(), 1);
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error", exc);
			fail();
		}
	}	
	
}

