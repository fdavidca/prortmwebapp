package com.proambiente.webapp.test;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.proambiente.modelo.Pais;
import com.proambiente.modelo.Permisible;
import com.proambiente.modelo.dto.EquipoDTO;
import com.proambiente.webapp.dao.AbstractFacade;
import com.proambiente.webapp.dao.PaisFacade;
import com.proambiente.webapp.dao.PermisibleFacade;

@RunWith(Arquillian.class)
public class PermisibleFacadeTest {

	@Inject
	PermisibleFacade permisibleFacade;

	@Deployment
	public static WebArchive createDeployment() {

		File[] files = Maven.resolver().loadPomFromFile("pom.xml").resolve("joda-time:joda-time:2.3").withTransitivity()
				.asFile();
		return ShrinkWrap.create(WebArchive.class, "test.war").addAsWebInfResource("test-ds.xml").addAsLibraries(files)
				.addClass(AbstractFacade.class).addPackage(Permisible.class.getPackage()).addClass(PermisibleFacade.class)
				.addClass(EquipoDTO.class).addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	@Test
	public void testEncontrarPermisible() {
		try {
			Permisible permisible = permisibleFacade.find(1);
			System.out.println("119j : " + permisible);
		} catch (Exception exc) {
			exc.printStackTrace();
			Assert.fail("Error consultar cambio password por id");
		}

	}

	@Test
	public void testListarPermisibles() {
		try {
			List<Permisible> listaPermisibles = permisibleFacade.findRange(new int[] { 0, 10 });
			System.out.println("Paises encontradas" + listaPermisibles.size());
		} catch (Exception exc) {
			exc.printStackTrace();
			Assert.fail("error consultando lista ");
		}
	}

}
