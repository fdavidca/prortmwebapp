package com.proambiente.webapp.test;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.proambiente.modelo.Aseguradora;
import com.proambiente.modelo.dto.EquipoDTO;
import com.proambiente.webapp.dao.AbstractFacade;
import com.proambiente.webapp.dao.AseguradoraFacade;

@RunWith(Arquillian.class)
public class AseguradoraFacadeTest {
	
	@Inject
	AseguradoraFacade aseguradoraFacade;
	
	@Deployment
	public static WebArchive createDeployment(){		
		
		File[] files = Maven.resolver().loadPomFromFile("pom.xml").resolve("joda-time:joda-time:2.3").withTransitivity().asFile();		
		return ShrinkWrap.create(WebArchive.class,"test.war").addAsWebInfResource("test-ds.xml")
				.addAsLibraries(files)
				.addClass(AbstractFacade.class).addPackage(Aseguradora.class.getPackage()).addClass(AseguradoraFacade.class).addClass(EquipoDTO.class)
				.addAsResource("META-INF/test-persistence.xml","META-INF/persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE,"beans.xml");
	}
	
	@Test
	public void testEncontrarAseguradora(){
		try{
			Aseguradora aseguradora = aseguradoraFacade.find(1); 
			System.out.println("19j : " + aseguradora);
		}catch(Exception exc){
			exc.printStackTrace();
			Assert.fail("Error consultar aseguradora por id");
		}
				
	}
	
	@Test
	public void testListarAseguradoras(){
		try{
			List<Aseguradora> listaAseguradora = aseguradoraFacade.findAll();
			System.out.println("Aseguradora encontrada: " + listaAseguradora.size());
		}catch(Exception exc){
			exc.printStackTrace();
			Assert.fail("error consultando aseguradora");
		}
	}
	
	@Test
	public void testGuardarAseguradora() {
		try {
			
			Aseguradora aseguradora = new Aseguradora();		
			
			aseguradora.setCodigo("codigotest");
			aseguradora.setNombre("ASEGURADORA DE PRUEBA");
			aseguradoraFacade.edit(aseguradora);
			
		}catch(Exception exc) {
			exc.printStackTrace();
			Assert.fail("error guardando aSEGURADORA");
		}
	}

}
