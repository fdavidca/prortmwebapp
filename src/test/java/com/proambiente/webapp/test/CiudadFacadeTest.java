package com.proambiente.webapp.test;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.proambiente.modelo.Ciudad;
import com.proambiente.modelo.dto.EquipoDTO;
import com.proambiente.webapp.dao.AbstractFacade;
import com.proambiente.webapp.dao.CiudadFacade;

@RunWith(Arquillian.class)
public class CiudadFacadeTest {
	
	@Inject
	CiudadFacade ciudadFacade;
	
	
	
	@Deployment
	public static WebArchive createDeployment(){		
		
		File[] files = Maven.resolver().loadPomFromFile("pom.xml").resolve("joda-time:joda-time:2.3").withTransitivity().asFile();		
		return ShrinkWrap.create(WebArchive.class,"test.war").addAsWebInfResource("test-ds.xml")
				.addAsLibraries(files)
				.addClass(AbstractFacade.class).addPackage(Ciudad.class.getPackage()).addClass(CiudadFacade.class).addClass(EquipoDTO.class)
				.addAsResource("META-INF/test-persistence.xml","META-INF/persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE,"beans.xml");
	}
	
	@Test
	public void testEncontrarCiudad(){
		try{
			Ciudad ciudad =ciudadFacade.find(1); 
			System.out.println("119j : " +ciudad);
		}catch(Exception exc){
			exc.printStackTrace();
			Assert.fail("Error consultar cambio password por id");
		}
				
	}
	
	@Test
	public void testListarCiudades(){
		try{
			List<Ciudad> listaCiudades = ciudadFacade.findRange(new int[] {0,10});
			System.out.println("Ciudades encontradas" + listaCiudades.size());
		}catch(Exception exc){
			exc.printStackTrace();
			Assert.fail("error consultando lista ciudades");
		}
	}
	
	

}
