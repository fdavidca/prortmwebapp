package com.proambiente.webapp.test.ci2service;

import static org.junit.Assert.fail;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.tempuri.Formulario;

import com.proambiente.modelo.Color;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.dto.ResultadoOttoMotocicletasDTO;
import com.proambiente.webapp.dao.ColorFacade;
import com.proambiente.webapp.dao.MarcaFacade;
import com.proambiente.webapp.dao.MedidaFacade;
import com.proambiente.webapp.dao.PermisibleFacade;
import com.proambiente.webapp.dao.PropietarioFacade;
import com.proambiente.webapp.dao.PruebaFacade;
import com.proambiente.webapp.dao.VehiculoFacade;
import com.proambiente.webapp.service.CertificadoService;
import com.proambiente.webapp.service.DefectoService;
import com.proambiente.webapp.service.FechaService;
import com.proambiente.webapp.service.FurCi2Codificador;
import com.proambiente.webapp.service.InspeccionService;
import com.proambiente.webapp.service.PasswordHash;
import com.proambiente.webapp.service.PruebasService;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.service.generadordto.GeneradorDTOPermisibles;
import com.proambiente.webapp.service.generadordto.GeneradorDTOResultados;
import com.proambiente.webapp.service.generadordto.MaximoMinimoPermisibleDTO;
import com.proambiente.webapp.service.indra.CodificadorFurIndra;
import com.proambiente.webapp.service.sicov.UtilEncontrarPruebas;
import com.proambiente.webapp.service.util.UtilInformesDefectos;
import com.proambiente.webapp.service.util.UtilInformesMedidas;
import com.proambiente.webapp.service.util.UtilTipoVehiculo;
import com.proambiente.webapp.util.ConstantesDefectosTaximetro;
import com.proambiente.webapp.util.ConstantesMedidasAlineacion;
import com.proambiente.webapp.util.ConstantesMedidasFrenos;
import com.proambiente.webapp.util.ConstantesMedidasGases;
import com.proambiente.webapp.util.ConstantesMedidasSonometria;
import com.proambiente.webapp.util.ConstantesMedidasSuspension;
import com.proambiente.webapp.util.ConstantesMedidasTaximetro;
import com.proambiente.webapp.util.ConstantesTiposCombustible;
import com.proambiente.webapp.util.ConstantesTiposVehiculo;

@RunWith(Arquillian.class)
public class TestCodificadorFurCi2FAS {
	
	@Inject
	FurCi2Codificador furCi2Codificador;
	
	@Inject
	PermisibleFacade permisibleService;
	
	@Inject
	RevisionService revisionService;
	
	@Inject
	PruebasService pruebaService;
	
	@Inject
	PruebaFacade pruebaFacade;
	
	@Inject
	DefectoService defectoService;
	
	private static final Logger logger = Logger.getLogger(TestCodificadorFurCi2FAS.class.getName());
	
	@Deployment
	public static WebArchive createDeployment() {

		File[] files = Maven.resolver().loadPomFromFile("pom.xml").importRuntimeDependencies().resolve()
				.withTransitivity().asFile();
		return ShrinkWrap.create(WebArchive.class, "test.war").addAsWebInfResource("test-ds.xml").addAsLibraries(files)

				.addPackage(Color.class.getPackage()).addPackage(ResultadoOttoMotocicletasDTO.class.getPackage())
				.addPackage("com.proambiente.webapp.dao").addClass(MarcaFacade.class).addClass(ColorFacade.class)
				.addClass(VehiculoFacade.class).addClass(PruebasService.class).addClass(PropietarioFacade.class)
				.addClass(InspeccionService.class).addClass(RevisionService.class).addClass(DefectoService.class)
				.addClass(PermisibleFacade.class).addClass(MedidaFacade.class).addClass(CertificadoService.class)
				.addClass(FechaService.class)
				.addClass(GeneradorDTOPermisibles.class).addClass(UtilInformesMedidas.class).addClass(MaximoMinimoPermisibleDTO.class)
				.addClass(PasswordHash.class).addClass(CodificadorFurIndra.class).addClass(UtilInformesMedidas.class)
				.addClass(UtilInformesDefectos.class).addClass(ConstantesMedidasGases.class).addClass(ConstantesTiposVehiculo.class)
				.addClass(ConstantesMedidasFrenos.class).addClass(ConstantesMedidasAlineacion.class).addClass(ConstantesTiposCombustible.class)
				.addClass(ConstantesMedidasSuspension.class).addClass(ConstantesMedidasSonometria.class)
				.addClass(ConstantesMedidasTaximetro.class).addClass(ConstantesDefectosTaximetro.class)
				.addClass(GeneradorDTOResultados.class).addClass(Formulario.class)
				.addClass(FurCi2Codificador.class).addClass(UtilEncontrarPruebas.class).addClass(UtilTipoVehiculo.class)
				.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
	}
	
	
	
	
	@Test
	public void testCodificarMedidasFASVehiculo(){
		try{
			
			Revision revision = revisionService.cargarRevision(48);
			List<Prueba> pruebas = pruebaService.pruebasRevision(48);			
			Formulario formulario = new Formulario();		
			furCi2Codificador.llenarSeccionDesviacion(pruebas, formulario, revision);
			furCi2Codificador.llenarSeccionFrenos(pruebas, formulario, revision);
			furCi2Codificador.llenarSeccionSuspension(48, pruebas, formulario, revision);
			
			Assert.assertEquals(formulario.getP7DelDerVal(),"39.0");
			Assert.assertEquals(formulario.getP7DelIzqVal(),"39.0");
			Assert.assertEquals(formulario.getP7TraDerVal(),"39.0");
			Assert.assertEquals(formulario.getP7TraIzqVal(),"39.0");
			Assert.assertEquals(formulario.getP7Min(),"40");
			
			Assert.assertEquals("11.0", formulario.getP9Ej1());
			Assert.assertEquals("-11.0", formulario.getP9Ej2());
			Assert.assertNotNull(formulario.getP9Ej3());
			Assert.assertNotNull(formulario.getP9Ej4());
			Assert.assertNotNull(formulario.getP9Ej5());
			Assert.assertNotNull(formulario.getP9Max());
			
			//FRENOS
			Assert.assertEquals("34.2", formulario.getP8EfiTot() );
			Assert.assertEquals("18",formulario.getP8EfiAuxMin());
			Assert.assertEquals("50", formulario.getP8EfiTotMin());
			Assert.assertEquals("16.6", formulario.getP8EfiAux() );
			
			Assert.assertEquals("800", formulario.getP8Ej1IzqFue());
			Assert.assertEquals("1000",formulario.getP8Ej1DerFue());
			Assert.assertEquals("700", formulario.getP8Ej2IzqFue());
			Assert.assertEquals("1000", formulario.getP8Ej2DerFue());
			//Peso
			Assert.assertEquals("2656", formulario.getP8Ej1IzqPes());
			Assert.assertEquals("2274", formulario.getP8Ej2IzqPes());
			Assert.assertEquals("3156", formulario.getP8Ej1DerPes());
			Assert.assertEquals("2136", formulario.getP8Ej2DerPes());
			//desequilibrio
			Assert.assertEquals("20.0", formulario.getP8Ej1Des());
			Assert.assertEquals("30.0", formulario.getP8Ej2Des());
			Assert.assertNotNull(formulario.getP8Ej1Max());
			Assert.assertNotNull(formulario.getP8Ej2Max());
			Assert.assertNotNull(formulario.getP8Ej3Max());
			Assert.assertNotNull(formulario.getP8Ej4Max());
			Assert.assertNotNull(formulario.getP8Ej5Max());
			
			
			
		}catch(Exception exc){
			fail("prueba fallida testCodificarMedidasGasesMoto");
			logger.log(Level.SEVERE, "Error", exc);
			
		}
	}
	
	
	@Test
	public void testFasMotos(){
		try{
			
			Revision revision = revisionService.cargarRevision(44);
			List<Prueba> pruebas = pruebaService.pruebasRevision(44);			
			Formulario formulario = new Formulario();		
			furCi2Codificador.llenarSeccionDesviacion(pruebas, formulario, revision);
			furCi2Codificador.llenarSeccionFrenos(pruebas, formulario, revision);
			furCi2Codificador.llenarSeccionSuspension(44, pruebas, formulario, revision);
			
			Assert.assertEquals(formulario.getP7DelDerVal(),"");
			Assert.assertEquals(formulario.getP7DelIzqVal(),"");
			Assert.assertEquals(formulario.getP7TraDerVal(),"");
			Assert.assertEquals(formulario.getP7TraIzqVal(),"");
			Assert.assertEquals(formulario.getP7Min(),"");
			
			Assert.assertEquals("", formulario.getP9Ej1());
			Assert.assertEquals("", formulario.getP9Ej2());
			Assert.assertNotNull(formulario.getP9Ej3());
			Assert.assertNotNull(formulario.getP9Ej4());
			Assert.assertNotNull(formulario.getP9Ej5());
			Assert.assertNotNull(formulario.getP9Max());
			
			//FRENOS
			Assert.assertEquals("53.6", formulario.getP8EfiTot() );
			Assert.assertEquals("",formulario.getP8EfiAuxMin());
			Assert.assertEquals("30", formulario.getP8EfiTotMin());
			Assert.assertEquals("", formulario.getP8EfiAux() );
			
			Assert.assertEquals("", formulario.getP8Ej1IzqFue());
			Assert.assertEquals("261",formulario.getP8Ej1DerFue());
			Assert.assertEquals("", formulario.getP8Ej2IzqFue());
			Assert.assertEquals("711", formulario.getP8Ej2DerFue());
			//Peso
			Assert.assertEquals("", formulario.getP8Ej1IzqPes());
			Assert.assertEquals("", formulario.getP8Ej2IzqPes());
			Assert.assertEquals("686", formulario.getP8Ej1DerPes());
			Assert.assertEquals("1127", formulario.getP8Ej2DerPes());
			//desequilibrio
			Assert.assertEquals("", formulario.getP8Ej1Des());
			Assert.assertEquals("", formulario.getP8Ej2Des());
			Assert.assertNotNull(formulario.getP8Ej1Max());
			Assert.assertNotNull(formulario.getP8Ej2Max());
			Assert.assertNotNull(formulario.getP8Ej3Max());
			Assert.assertNotNull(formulario.getP8Ej4Max());
			Assert.assertNotNull(formulario.getP8Ej5Max());
			
			
			
			
		}catch(Exception exc){
			fail("prueba fallida testFasMotos");
			logger.log(Level.SEVERE, "Error", exc);
		}
	}
}
