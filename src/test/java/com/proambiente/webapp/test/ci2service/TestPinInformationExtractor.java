package com.proambiente.webapp.test.ci2service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.Assert;
import org.junit.Test;

import com.proambiente.webapp.service.ci2.InformacionPin;
import com.proambiente.webapp.service.ci2.PinInformationExtractor;



public class TestPinInformationExtractor {
	
	
	private static final Logger logger = Logger.getLogger(TestPinInformationExtractor.class.getName());
	
	PinInformationExtractor extractorInformacionPin = new 	PinInformationExtractor();
	
	@Test
	public void testExtraerInformacion(){
		try{
			
			String cadenaText = "PIN@1318021154200330291050|TD@Cedula de Ciudadania|DOC@80722047";
			InformacionPin infoPin = 	extractorInformacionPin.extraerInformacionPin(cadenaText);
			Assert.assertEquals(infoPin.getPin(),"1318021154200330291050");
			Assert.assertEquals(infoPin.getTipoDocumento(),"Cedula de Ciudadania");
			Assert.assertEquals("80722047",infoPin.getDocumento() );
			
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error textExtraerInformacion", exc);
			Assert.fail("Error en extraerInformacion");
		}
	}

}
