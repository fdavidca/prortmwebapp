package com.proambiente.webapp.test.ci2service;

import static org.junit.Assert.fail;

import java.io.File;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.tempuri.Formulario;

import com.proambiente.modelo.Color;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.dto.ResultadoOttoMotocicletasDTO;
import com.proambiente.webapp.dao.ColorFacade;
import com.proambiente.webapp.dao.MarcaFacade;
import com.proambiente.webapp.dao.MedidaFacade;
import com.proambiente.webapp.dao.PermisibleFacade;
import com.proambiente.webapp.dao.PropietarioFacade;
import com.proambiente.webapp.dao.PruebaFacade;
import com.proambiente.webapp.dao.VehiculoFacade;
import com.proambiente.webapp.service.CertificadoService;
import com.proambiente.webapp.service.DefectoService;
import com.proambiente.webapp.service.FechaService;
import com.proambiente.webapp.service.FurCi2Codificador;
import com.proambiente.webapp.service.InspeccionService;
import com.proambiente.webapp.service.PasswordHash;
import com.proambiente.webapp.service.PruebasService;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.service.UtilInformacionProfundidadLabrado;
import com.proambiente.webapp.service.generadordto.GeneradorDTOPermisibles;
import com.proambiente.webapp.service.generadordto.GeneradorDTOResultados;
import com.proambiente.webapp.service.generadordto.MaximoMinimoPermisibleDTO;
import com.proambiente.webapp.service.indra.CodificadorFurIndra;
import com.proambiente.webapp.service.sicov.UtilEncontrarPruebas;
import com.proambiente.webapp.service.util.UtilInformesDefectos;
import com.proambiente.webapp.service.util.UtilInformesMedidas;
import com.proambiente.webapp.service.util.UtilTipoVehiculo;
import com.proambiente.webapp.util.ConstantesDefectosTaximetro;
import com.proambiente.webapp.util.ConstantesMedidasAlineacion;
import com.proambiente.webapp.util.ConstantesMedidasFrenos;
import com.proambiente.webapp.util.ConstantesMedidasGases;
import com.proambiente.webapp.util.ConstantesMedidasSonometria;
import com.proambiente.webapp.util.ConstantesMedidasSuspension;
import com.proambiente.webapp.util.ConstantesMedidasTaximetro;
import com.proambiente.webapp.util.ConstantesTiposCombustible;
import com.proambiente.webapp.util.ConstantesTiposVehiculo;

@RunWith(Arquillian.class)
public class TestCodificadorFurCi2Reinspecciones {
	
	@Inject
	FurCi2Codificador furCi2Codificador;
	
	@Inject
	PermisibleFacade permisibleService;
	
	@Inject
	RevisionService revisionService;
	
	@Inject
	PruebasService pruebaService;
	
	@Inject
	PruebaFacade pruebaFacade;
	
	@Inject
	DefectoService defectoService;
	
	@Inject
	InspeccionService inspeccionService;
	
	private static final Logger logger = Logger.getLogger(TestCodificadorFurCi2Reinspecciones.class.getName());
	
	@Deployment
	public static WebArchive createDeployment() {

		File[] files = Maven.resolver().loadPomFromFile("pom.xml").importRuntimeDependencies().resolve()
				.withTransitivity().asFile();
		return ShrinkWrap.create(WebArchive.class, "test.war").addAsWebInfResource("test-ds.xml").addAsLibraries(files)

				.addPackage(Color.class.getPackage()).addPackage(ResultadoOttoMotocicletasDTO.class.getPackage())
				.addPackage("com.proambiente.webapp.dao").addClass(MarcaFacade.class).addClass(ColorFacade.class)
				.addClass(VehiculoFacade.class).addClass(PruebasService.class).addClass(PropietarioFacade.class)
				.addClass(InspeccionService.class).addClass(RevisionService.class).addClass(DefectoService.class)
				.addClass(PermisibleFacade.class).addClass(MedidaFacade.class).addClass(CertificadoService.class)
				.addClass(FechaService.class)
				.addClass(GeneradorDTOPermisibles.class).addClass(UtilInformesMedidas.class).addClass(MaximoMinimoPermisibleDTO.class)
				.addClass(PasswordHash.class).addClass(CodificadorFurIndra.class).addClass(UtilInformesMedidas.class)
				.addClass(UtilInformesDefectos.class).addClass(ConstantesMedidasGases.class).addClass(ConstantesTiposVehiculo.class)
				.addClass(ConstantesMedidasFrenos.class).addClass(ConstantesMedidasAlineacion.class).addClass(ConstantesTiposCombustible.class)
				.addClass(ConstantesMedidasSuspension.class).addClass(ConstantesMedidasSonometria.class)
				.addClass(ConstantesMedidasTaximetro.class).addClass(ConstantesDefectosTaximetro.class)
				.addClass(GeneradorDTOResultados.class).addClass(Formulario.class).addClass(UtilInformacionProfundidadLabrado.class)
				.addClass(FurCi2Codificador.class).addClass(UtilEncontrarPruebas.class).addClass(UtilTipoVehiculo.class)
				.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
	}
	
	
	
	
	@Test
	public void testGenerarFormularioSegundaRevision(){
		try{
			
			Revision revision = revisionService.cargarRevision(178);
			Formulario formulario = furCi2Codificador.generarFormularioPrimeraEtapa(178);
			System.out.println(" Formulario Revision " + formulario);
			
		}catch(Exception exc){
			fail("prueba fallida testCodificarMedidasGasesMoto");
			logger.log(Level.SEVERE, "Error", exc);
			
		}
	}
	
	@Test
	public void testGenerarFormularioReinspeccion(){
		try{
			
			
			Formulario formulario = furCi2Codificador.generarFormularioPrimeraEtapaInspeccion( 48 );
			System.out.println(" Formulario generado  " + formulario );
			Formulario formulario2 = furCi2Codificador.generarFormularioPrimeraEtapaInspeccion( 49);
			System.out.println(" Formulario generado  " + formulario2 );
		}catch(Exception exc){
			fail("prueba fallida testCodificarMedidasGasesMoto");
			logger.log(Level.SEVERE, "Error", exc);
			
		}
	}	
	
	
	@Test
	public void testGenerarFormularioInspeccion(){
		try{
			
			Revision revision = revisionService.cargarRevision(178);
			Formulario formulario = furCi2Codificador.generarFormularioPrimeraEtapa(178);
			
		}catch(Exception exc){
			fail("prueba fallida testGenerarFormularioInspeccion");
			logger.log(Level.SEVERE, "Error", exc);
			
		}
	}	
	
}
