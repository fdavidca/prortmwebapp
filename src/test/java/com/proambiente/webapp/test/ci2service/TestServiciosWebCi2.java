package com.proambiente.webapp.test.ci2service;

import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.ws.Response;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.tempuri.Datos;
import org.tempuri.DatosSoap;
import org.tempuri.EchoResponse;
import org.tempuri.Formulario;
import org.tempuri.Pin;
import org.tempuri.Resultado;



public class TestServiciosWebCi2 {
	private static final Logger logger = Logger.getLogger(TestServiciosWebCi2.class.getName());
	
	Datos datos;
	DatosSoap datosSoap;
	
	@Before()
	public void setup(){
		try{
			URL url = new URL("http://192.168.1.44:8080/MockCi2/datos?wsdl");
			datos = new Datos(url);
			datosSoap = datos.getDatosSoap();
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error creando Datos", exc);
		}		
	}
	
	
	
	
	
	private static final String nombreUsuario = "proambientalCarrosymotos";
	private static final String pwd = "82b21f";
	
	
	@Test
	public void testEchoAsync(){
		try{
			Response<EchoResponse> echoResponse = datosSoap.echoAsync("Proambiente test");
			EchoResponse response = echoResponse.get();
			String echoResult = response.getEchoResult();
			Assert.assertNotNull(echoResult);			
			logger.log(Level.INFO, "Echo respuesta asincrona:" + echoResult);			
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error en testEchoAsync", exc);			
			Assert.fail("testEchoAsync fallo");
		}
	}
	
	
	@Test
	public void testEcho(){
		try{
			String echo = datosSoap.echo("Proambiente test");			
			logger.log(Level.INFO, "Echo recibido: " + echo);
			Assert.assertNotNull(echo);
		}catch(Exception exc){
			Assert.fail("Error consultando eco de web service");
		}		
	}
	
	@Test
	public void testConsultarPinPlaca(){		
		try{			
			Pin pin = new Pin();
			pin.setUsuario(nombreUsuario);
			pin.setClave(pwd);
			pin.setPPlaca("ZBF80C");
			Resultado resultado = datosSoap.consultaPinPlaca(pin);
			logger.log(Level.INFO,"Resultado: " + resultado.toString());
			Assert.assertNotNull(resultado);		
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error consultando pin placa", exc);
			Assert.fail("Error en testConsultarPinPlaca");
		}
	}
	
	@Test
	public void testConsultarPin(){
		try{
			Pin pin = new Pin();
			pin.setUsuario(nombreUsuario);
			pin.setClave(pwd);
			pin.setPPin("ABCDEFGHIJKLMN");
			Resultado resultado = datosSoap.consultaPin(pin);
			logger.log(Level.INFO, "Resultado :" + resultado.toString());
			Assert.assertNotNull(resultado);
			
		}catch(Exception exc){
			
			logger.log(Level.SEVERE, "Error consultando pin",exc);
			Assert.fail("Error en consultaPin");
		}
	}
	
	@Test
	public void testIngresarFormulario(){
		try{
			Formulario formulario = new Formulario();
			formulario.setClave(pwd);
			formulario.setUsuario(nombreUsuario);
			formulario.setP5DerInc("ABCDEFGHJKL");
			Resultado resultado = datosSoap.ingresarFur(formulario);
			logger.log(Level.INFO, "Resultado :" + resultado.toString());
			Assert.assertNotNull(resultado);
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error en ingresarFur", exc);
			Assert.fail("Error en ingresarFormulario");
		}
	}
	
	
	

}
