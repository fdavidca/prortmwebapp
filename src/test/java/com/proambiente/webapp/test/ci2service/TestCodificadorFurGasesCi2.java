package com.proambiente.webapp.test.ci2service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.tempuri.Formulario;

import com.proambiente.modelo.Color;
import com.proambiente.modelo.Defecto;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.dto.ResultadoOttoMotocicletasDTO;
import com.proambiente.webapp.dao.ColorFacade;
import com.proambiente.webapp.dao.MarcaFacade;
import com.proambiente.webapp.dao.MedidaFacade;
import com.proambiente.webapp.dao.PermisibleFacade;
import com.proambiente.webapp.dao.PropietarioFacade;
import com.proambiente.webapp.dao.PruebaFacade;
import com.proambiente.webapp.dao.VehiculoFacade;
import com.proambiente.webapp.service.CertificadoService;
import com.proambiente.webapp.service.DefectoService;
import com.proambiente.webapp.service.FechaService;
import com.proambiente.webapp.service.FurCi2Codificador;
import com.proambiente.webapp.service.InspeccionService;
import com.proambiente.webapp.service.PasswordHash;
import com.proambiente.webapp.service.PruebasService;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.service.generadordto.GeneradorDTOPermisibles;
import com.proambiente.webapp.service.generadordto.GeneradorDTOResultados;
import com.proambiente.webapp.service.generadordto.MaximoMinimoPermisibleDTO;
import com.proambiente.webapp.service.indra.CodificadorFurIndra;
import com.proambiente.webapp.service.sicov.UtilEncontrarPruebas;
import com.proambiente.webapp.service.util.UtilInformesDefectos;
import com.proambiente.webapp.service.util.UtilInformesMedidas;
import com.proambiente.webapp.service.util.UtilTipoVehiculo;
import com.proambiente.webapp.util.ConstantesDefectosTaximetro;
import com.proambiente.webapp.util.ConstantesMedidasAlineacion;
import com.proambiente.webapp.util.ConstantesMedidasFrenos;
import com.proambiente.webapp.util.ConstantesMedidasGases;
import com.proambiente.webapp.util.ConstantesMedidasSonometria;
import com.proambiente.webapp.util.ConstantesMedidasSuspension;
import com.proambiente.webapp.util.ConstantesMedidasTaximetro;
import com.proambiente.webapp.util.ConstantesTiposCombustible;
import com.proambiente.webapp.util.ConstantesTiposVehiculo;

@RunWith(Arquillian.class)
public class TestCodificadorFurGasesCi2 {
	
	@Inject
	FurCi2Codificador furCi2Codificador;
	
	@Inject
	PermisibleFacade permisibleService;
	
	@Inject
	RevisionService revisionService;
	
	@Inject
	PruebasService pruebaService;
	
	@Inject
	PruebaFacade pruebaFacade;
	
	@Inject
	DefectoService defectoService;
	
	private static final Logger logger = Logger.getLogger(TestCodificadorFurGasesCi2.class.getName());
	
	@Deployment
	public static WebArchive createDeployment() {

		File[] files = Maven.resolver().loadPomFromFile("pom.xml").importRuntimeDependencies().resolve()
				.withTransitivity().asFile();
		return ShrinkWrap.create(WebArchive.class, "test.war").addAsWebInfResource("test-ds.xml").addAsLibraries(files)

				.addPackage(Color.class.getPackage()).addPackage(ResultadoOttoMotocicletasDTO.class.getPackage())
				.addPackage("com.proambiente.webapp.dao").addClass(MarcaFacade.class).addClass(ColorFacade.class)
				.addClass(VehiculoFacade.class).addClass(PruebasService.class).addClass(PropietarioFacade.class)
				.addClass(InspeccionService.class).addClass(RevisionService.class).addClass(DefectoService.class)
				.addClass(PermisibleFacade.class).addClass(MedidaFacade.class).addClass(CertificadoService.class)
				.addClass(FechaService.class)
				.addClass(GeneradorDTOPermisibles.class).addClass(UtilInformesMedidas.class).addClass(MaximoMinimoPermisibleDTO.class)
				.addClass(PasswordHash.class).addClass(CodificadorFurIndra.class).addClass(UtilInformesMedidas.class)
				.addClass(UtilInformesDefectos.class).addClass(ConstantesMedidasGases.class).addClass(ConstantesTiposVehiculo.class)
				.addClass(ConstantesMedidasFrenos.class).addClass(ConstantesMedidasAlineacion.class).addClass(ConstantesTiposCombustible.class)
				.addClass(ConstantesMedidasSuspension.class).addClass(ConstantesMedidasSonometria.class)
				.addClass(ConstantesMedidasTaximetro.class).addClass(ConstantesDefectosTaximetro.class)
				.addClass(GeneradorDTOResultados.class).addClass(Formulario.class)
				.addClass(FurCi2Codificador.class).addClass(UtilEncontrarPruebas.class).addClass(UtilTipoVehiculo.class)
				.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
	}
	
	
	/**
	 * Prueba de fur para vehiculo otto completa
	 */
	@Test
	public void generarFur(){
		try{
			
			Formulario formulario = furCi2Codificador.generarFormularioPrimeraEtapa(48);
			assertNotNull(formulario.getUsuario());
			assertNotNull(formulario.getClave());
			assertNotNull(formulario.getPPin());
			assertNotNull(formulario.getPFurAso());
			//CDA
			assertNotNull(formulario.getPCda());
			assertNotNull(formulario.getPNit());
			assertNotNull(formulario.getP2Dir());
			assertNotNull(formulario.getPDiv());
			assertNotNull(formulario.getPCiu());
			assertNotNull(formulario.getPTel());
			//PROPIETARIO
			assertNotNull(formulario.getP1FecPru());
			assertNotNull(formulario.getP2NomRaz());
			assertEquals("1",formulario.getP2DocTip());
			assertNotNull(formulario.getP2Doc());
			//VEHICULO
			assertEquals("OTTOV2",formulario.getP3Plac());
			assertNotNull(formulario.getP3Mar());
			assertNotNull(formulario.getP3Lin());
			assertNotNull(formulario.getP3Cla());
			assertNotNull(formulario.getP3Mod());
			assertNotNull(formulario.getP3Cil());
			assertNotNull(formulario.getP3Ser());
			assertNotNull(formulario.getP3Vin());
			assertNotNull(formulario.getP3Mot());
			assertNotNull(formulario.getP3Mot());
			assertNotNull(formulario.getP3Lic());
			assertEquals("GASOLINA",formulario.getP3Com());
			assertNotNull(formulario.getP3Col());
			assertNotNull(formulario.getP3Nac());
			assertNotNull(formulario.getP3FecLic());
			assertNotNull(formulario.getP3TipMot());
			assertNotNull(formulario.getP3Kil());
			assertNotNull(formulario.getP3Sil());
			assertNotNull(formulario.getP3VidPol());
			assertNotNull(formulario.getP3Bli());
			
			//RUIDO
			
			assertNotNull(formulario.getP4RuiMax());
			assertNotNull(formulario.getP4RuiVal());
			assertNotNull(formulario.getPR03());
			assertNotNull(formulario.getPR04());
			
			//LUCES
			assertNotNull(formulario.getP5DerInt());
			assertNotNull(formulario.getP5DerMin());
			assertNotNull(formulario.getP5DerRan());
			assertNotNull(formulario.getP5DerInc());
			assertNotNull(formulario.getP5IzqInt());
			assertNotNull(formulario.getP5IzqMin());
			assertNotNull(formulario.getP5IzqInq());
			assertNotNull(formulario.getP5IzqRan());
			assertNotNull(formulario.getP6Int());
			assertNotNull(formulario.getP6Max());
			
			//SUSPENSION
			assertNotNull(formulario.getP7DelDerVal());
			assertNotNull(formulario.getP7DelIzqVal());
			assertNotNull(formulario.getP7TraDerVal());
			assertNotNull(formulario.getP7TraIzqVal());
			assertNotNull(formulario.getP7Min());
			//FRENOS
			assertNotNull(formulario.getP8EfiTot());
			assertNotNull(formulario.getP8EfiTotMin());
			assertNotNull(formulario.getP8Ej1IzqFue());
			assertNotNull(formulario.getP8Ej1IzqPes());
			assertNotNull(formulario.getP8Ej1DerFue());
			assertNotNull(formulario.getP8Ej1DerPes());
			assertNotNull(formulario.getP8Ej1Des());
			assertNotNull(formulario.getP8Ej2IzqFue());
			assertNotNull(formulario.getP8Ej2IzqPes());
			assertNotNull(formulario.getP8Ej2DerFue());
			assertNotNull(formulario.getP8Ej2DerPes());
			assertNotNull(formulario.getP8Ej2Des());
			assertNotNull(formulario.getP8EfiAux());
			assertNotNull(formulario.getP8EfiAuxMin());
			//DESVIACION
			assertNotNull(formulario.getP9Ej1());
			assertNotNull(formulario.getP9Ej2());
			assertNotNull(formulario.getP9Ej3());
			assertNotNull(formulario.getP9Ej4());
			assertNotNull(formulario.getP9Ej5());
			assertNotNull(formulario.getP10Max());
			//TAXIMETRO
			assertNotNull(formulario.getP10RefComLla());
			assertNotNull(formulario.getP10ErrDis());
			assertNotNull(formulario.getP10ErrTie());
			assertNotNull(formulario.getP10Max());
			//OTTO VEHICULO
			assertNotNull(formulario.getP11CoRalVal());
			assertNotNull(formulario.getP11CoRalNor());
			assertNotNull(formulario.getP11Co2RalVal());
			assertNotNull(formulario.getP11Co2RalNor());
			assertNotNull(formulario.getP11O2RalVal());
			assertNotNull(formulario.getP11O2RalNor());
			assertNotNull(formulario.getP11HcRalVal());
			assertNotNull(formulario.getP11HcRalNor());
			
			assertNotNull(formulario.getP11CoCruVal());
			assertNotNull(formulario.getP11CoCruNor());
			assertNotNull(formulario.getP11Co2CruVal());
			assertNotNull(formulario.getP11Co2CruNor());
			assertNotNull(formulario.getP11O2CruVal());
			assertNotNull(formulario.getP11O2CruNor());
			assertNotNull(formulario.getP11HcCruVal());
			assertNotNull(formulario.getP11HcCruNor());
			
			assertNotNull(formulario.getPG17());
			
			assertNotNull(formulario.getP11TemRal());
			assertNotNull(formulario.getP11RpmRal());
			assertNotNull(formulario.getP11TemCru());
			assertNotNull(formulario.getP11RpmCru());
			
			assertNotNull(formulario.getP11TemRal());
			assertNotNull(formulario.getP11RpmRal());
			assertNotNull(formulario.getP11TemCru());
			assertNotNull(formulario.getP11RpmCru());
			
			assertNotNull(formulario.getP11NoRalVal());
			assertNotNull(formulario.getP11NoRalNor());
			assertNotNull(formulario.getP11NoCruVal());
			assertNotNull(formulario.getP11NoCruNor());
			
			
		}catch(Exception exc){
			fail("Prueba fallida para metodo");
			logger.log(Level.SEVERE, "", exc);
		}
	}
	
	
	@Test
	public void testCodificarMedidasDiesel(){
		try{
			Revision revision = revisionService.cargarRevision(26);
			Prueba prueba = pruebaFacade.find(196);
			Formulario formulario = new Formulario();
			List<Prueba> pruebas = new ArrayList<>();
			pruebas.add(prueba);
			furCi2Codificador.llenarSeccionGases(pruebas, formulario, revision);
			
			Assert.assertEquals("5900", formulario.getP11BRpm());
			Assert.assertEquals("19", formulario.getP11BTem());
			Assert.assertEquals("28.0", formulario.getP11BCi1());
			Assert.assertEquals("28.8", formulario.getP11BCi2());
			Assert.assertEquals("28.8", formulario.getP11BCi3());
			Assert.assertEquals("27.9", formulario.getP11BCi4());
			Assert.assertEquals("28.5", formulario.getP11BResVal());
			
			Assert.assertNotNull(formulario.getP11HcCruVal());
			Assert.assertNotNull(formulario.getP11NoCruVal());
			Assert.assertNotNull(formulario.getP11O2CruVal());
			
		}catch(Exception exc){
			fail("prueba fallida testCodificarMedidasGasesMoto");
			logger.log(Level.SEVERE, "Error", exc);
			
		}
	}
	
	@Test
	public void testCodificarDefectosInspeccionVisualOttoVehiculos(){
		try{
			Revision revision = revisionService.cargarRevision(79);
			Prueba prueba = pruebaFacade.find(699);
			List<Prueba> pruebas = new ArrayList<>();
			pruebas.add(prueba);
			Formulario formulario = new Formulario();
			
			furCi2Codificador.llenarSeccionGases(pruebas, formulario, revision);
			Assert.assertEquals("", formulario.getP11BRpm());
			Assert.assertEquals("", formulario.getP11BTem());
			Assert.assertEquals("", formulario.getP11BCi1());
			Assert.assertEquals("", formulario.getP11BCi2());
			Assert.assertEquals("", formulario.getP11BCi3());
			Assert.assertEquals("", formulario.getP11BCi4());
			Assert.assertEquals("", formulario.getP11BResVal());
			
			
			Assert.assertNotNull(formulario.getPO10());
			Assert.assertNotNull(formulario.getP11BCi3());
			Assert.assertNotNull(formulario.getP11HcCruNor());
			Assert.assertNotNull(formulario.getP11NoCruVal());
			
			Assert.assertEquals("", formulario.getP11BRpm());
			Assert.assertEquals("", formulario.getP11BTem());
			Assert.assertEquals("", formulario.getP11BCi1());
			Assert.assertEquals("", formulario.getP11BCi2());
			Assert.assertEquals("", formulario.getP11BCi3());
			Assert.assertEquals("", formulario.getP11BCi4());
			Assert.assertEquals("", formulario.getP11BResVal());
			List<Defecto> defectosInspeccionMecanizada = defectoService.obtenerDefectosInspeccionMecanizada(79);
			// cargar defectos inspeccion mecanizada
			furCi2Codificador.ponerDefectosInspeccionMecanizada(79, formulario,defectosInspeccionMecanizada);
			Assert.assertTrue(formulario.getPCCod().contains("40103073"));
			
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error en testCodificarDefectosInspeccionVisual", exc);
			fail("Prueba fallida testCodificarMedidasGasesOttoVehiculo");
			
			
		}
	}
	
	@Test
	public void testCodificarDefectoVisualOttoMotocicletas(){
		try{
			Revision revision = revisionService.cargarRevision(80);
			Prueba prueba = pruebaFacade.find(705);
			List<Prueba> pruebas = new ArrayList<>();
			pruebas.add(prueba);
			Formulario formulario = new Formulario();
			
			furCi2Codificador.llenarSeccionGases(pruebas, formulario, revision);
			
			Assert.assertEquals("", formulario.getP11BRpm());
			Assert.assertEquals("", formulario.getP11BTem());
			Assert.assertEquals("", formulario.getP11BCi1());
			Assert.assertEquals("", formulario.getP11BCi2());
			Assert.assertEquals("", formulario.getP11BCi3());
			Assert.assertEquals("", formulario.getP11BCi4());
			Assert.assertEquals("", formulario.getP11BResVal());
			
			
			
			Assert.assertNotNull(formulario.getPO10());
			Assert.assertNotNull(formulario.getP11BCi3());
			Assert.assertNotNull(formulario.getP11HcCruNor());
			Assert.assertNotNull(formulario.getP11NoCruVal());
			Assert.assertNotNull(formulario.getP11NoRalNor());
			Assert.assertNotNull(formulario.getP11Co2RalNor());
		
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error testCodificarDefectoVisualOttoMotocicletas", exc);
		}
	}
	
	
	@Test
	public void testCodificarMedidasGasesOttoVehiculo(){
		try{
			
			Revision revision = revisionService.cargarRevision(48);
			Prueba prueba = pruebaFacade.find(394);
			List<Prueba> pruebas = new ArrayList<>();
			pruebas.add(prueba);
			
			Formulario formulario = new Formulario();
			furCi2Codificador.llenarSeccionGases(pruebas, formulario, revision);
			Assert.assertEquals("830", formulario.getP11RpmRal());
			Assert.assertEquals("66", formulario.getP11TemRal());
			Assert.assertEquals("401", formulario.getP11HcRalVal());
			Assert.assertEquals("3.77", formulario.getP11CoRalVal());
			Assert.assertEquals("13.3", formulario.getP11Co2RalVal());
			Assert.assertEquals("1.65", formulario.getP11O2RalVal());
			Assert.assertEquals("66", formulario.getP11TemCru());
			Assert.assertEquals("2400", formulario.getP11RpmCru());
			Assert.assertEquals("401", formulario.getP11HcCruVal());
			Assert.assertEquals("3.77", formulario.getP11CoCruVal());
			Assert.assertEquals("13.3", formulario.getP11Co2CruVal());
			Assert.assertEquals("1.65", formulario.getP11O2CruVal());
			
		}catch(Exception exc){
			fail("Prueba fallida testCodificarMedidasGasesOttoVehiculo");
			logger.log(Level.SEVERE, "Error testCodificarMedidasGasesOttoVehiculo", exc);
		}
	}
	
	@Test
	public void testCodificarMedidasGasesMotos(){
		try{
			
			Revision revision = revisionService.cargarRevision(44);
			Prueba prueba = pruebaFacade.find(361);
			Formulario formulario = new Formulario();
			List<Prueba> pruebas = new ArrayList<>();
			pruebas.add(prueba);
			furCi2Codificador.llenarSeccionGases(pruebas, formulario, revision);
			Assert.assertEquals("1582", formulario.getP11RpmRal());
			Assert.assertEquals("87", formulario.getP11TemRal());
			Assert.assertEquals("4", formulario.getP11HcRalVal());
			Assert.assertEquals("4.08", formulario.getP11CoRalVal());
			Assert.assertEquals("12.0", formulario.getP11Co2RalVal());
			Assert.assertEquals("1.65", formulario.getP11O2RalVal());
			
			Assert.assertNotNull(formulario.getPO10());
			Assert.assertNotNull(formulario.getP11BCi3());
			Assert.assertNotNull(formulario.getP11HcCruNor());
			Assert.assertNotNull(formulario.getP11NoCruVal());
			
		}catch(Exception exc){
			fail("prueba fallida testCodificarMedidasGasesMoto");
			logger.log(Level.SEVERE, "Error", exc);
			
		}
	}
}
