package com.proambiente.webapp.test.service;

import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.proambiente.modelo.Defecto;
import com.proambiente.modelo.Inspeccion;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Usuario;
import com.proambiente.modelo.dto.RevisionDTO;
import com.proambiente.webapp.dao.InspeccionFacade;
import com.proambiente.webapp.service.FechaService;
import com.proambiente.webapp.service.InspeccionService;
import com.proambiente.webapp.service.PasswordHash;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.service.VehiculoService;
import com.proambiente.webapp.service.builderdto.RevisionDTOConstructor;

@RunWith(Arquillian.class)
public class InspeccionServiceTest {
	
	
	private static final Logger logger = Logger.getLogger(InspeccionServiceTest.class.getName());
	
	@Inject
	InspeccionFacade inspeccionFacade;
	
	@Inject
	InspeccionService inspeccionService;
	
	@Inject
	RevisionService revisionService;
	
	@Inject
	FechaService fechaService;
	
	@Deployment
	public static WebArchive createDeployment(){		
		
		File[] files = Maven.resolver().loadPomFromFile("pom.xml").resolve("joda-time:joda-time:2.3").withTransitivity().asFile();		
		return ShrinkWrap.create(WebArchive.class,"test.war").addAsWebInfResource("test-ds.xml")
				.addAsLibraries(files)
				.addPackage(Defecto.class.getPackage())
			    .addPackage("com.proambiente.webapp.dao")			    
				.addClass(VehiculoService.class)				
				.addClass(PasswordHash.class)//estas son minimas
				.addClass(RevisionService.class)//estas son minimas para que el test se ejecute
				.addClass(InspeccionService.class)//estas son minimas para que el test se ejecute
				.addClass(FechaService.class)
				.addPackage(RevisionDTO.class.getPackage())
				.addAsResource("META-INF/test-persistence.xml","META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE,"beans.xml");
		
		
	}
	
	
	@Test
	public void testCargarPruebasInspeccion(){
		try{
			
			LocalDate inicio = LocalDate.of(2024,02,03);
			LocalDate fin = LocalDate.of(2024,02,04);
			
			Date fechaInicial = Date.from(inicio.atStartOfDay(ZoneId.systemDefault()).toInstant());
			Date fechaFinal = Date.from(fin.atStartOfDay(ZoneId.systemDefault()).toInstant());
			long cuentas = inspeccionService.contarPruebasOttoMotocicletasInspeccion(fechaInicial, fechaFinal);
			System.out.println("Pruebas total:" + cuentas);
			List<Prueba> pruebas = inspeccionService.consultarPruebasOttoMotocicletasPaginado(fechaInicial, fechaFinal, 0, 300);
			pruebas.forEach(p->{
				System.out.println(p.getPruebaId());
			});
			assertNotNull(pruebas);
			Assert.assertTrue(!pruebas.isEmpty());
		}catch(Exception exc){			
			exc.printStackTrace();
			Assert.fail("Prueba falida para metodo testCargarRevision");
		}
	}
	
	/**
	 * Test cargar inspecciones revision por revision id
	 * revision 170 fecha 20188226
	 */
	@Test
	public void testCargarInspeccionesPorRevisionId(){
		try{
			List<Inspeccion> inspecciones = inspeccionService.obtenerInspeccionesPorRevisionId(170);
			assertNotNull(inspecciones);
			Assert.assertTrue(!inspecciones.isEmpty());
		}catch(Exception exc){			
			exc.printStackTrace();
			Assert.fail("Prueba falida para metodo testCargarRevision");
		}
	}
	
	/**
	 * Test cargar inspecciones por placa 
	 * placa usada OTTOV7
	 */
	@Test
	public void testCargarInspeccionesPorPlaca() {
		try{
			List<Inspeccion> inspecciones = inspeccionService.inspeccionesPorPlaca("OTTOV7");
			assertNotNull(inspecciones);
			Assert.assertTrue(!inspecciones.isEmpty());
		}catch(Exception exc){			
			exc.printStackTrace();
			Assert.fail("Prueba falida para metodo testCargarRevision");
		}

	}
	
	
	/**
	 * Test cargar inspeccion por id, datos de tests asociados a revision 170
	 * 
	 */
	@Test
	public void testCargarInspeccionesPorId() {
		try{
			Inspeccion inspeccion = inspeccionService.cargarInspeccion(44);
			assertNotNull(inspeccion);
			Assert.assertTrue( inspeccion.getInspeccionId() > 0);
			
		}catch(Exception exc){			
			exc.printStackTrace();
			Assert.fail("Prueba falida para metodo testCargarRevision");
		}

	}
	
	@Test
	public void testPruebaSonometroPorInspeccion() {
		try{

			Prueba prueba = inspeccionService.pruebaSonometroInspeccion(44);
			assertNotNull(prueba);
			Assert.assertTrue( prueba.getPruebaId() > 0);
			
		}catch(Exception exc){			
			exc.printStackTrace();
			Assert.fail("Prueba falida para metodo testCargarRevision");
		}

	}
	
	
	public void testCargarRevisionPorFechaSiguienteInspeccion() {
		try {
			
			LocalDateTime ldtInicio = LocalDateTime.of(2018, 02, 26, 00, 00);
			LocalDateTime ldtFin = LocalDateTime.of( 2018, 02, 27, 00, 00);
			
			Date fechaInicio = Date.from(ldtInicio.atZone(ZoneId.systemDefault()).toInstant());
			Date fechaFin = Date.from(ldtFin.atZone(ZoneId.systemDefault()).toInstant());
			
			List<Revision> revisiones = inspeccionService.revisionInspeccionPorFechaSiguiente(fechaInicio, fechaFin,false);
			assertNotNull(revisiones);
			Assert.assertTrue(!revisiones.isEmpty());
			
		}catch(Exception exc) {
			exc.printStackTrace();
			Assert.fail("Prueba falida para metodo testCargarRevisionPorFechaSiguienteInspeccion");
		}
	}
	
	
}
