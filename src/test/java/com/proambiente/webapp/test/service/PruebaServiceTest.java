package com.proambiente.webapp.test.service;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.dto.PruebaDTO;
import com.proambiente.webapp.dao.AbstractFacade;
import com.proambiente.webapp.dao.PruebaFacade;
import com.proambiente.webapp.service.PruebasService;

@RunWith(Arquillian.class)
public class PruebaServiceTest {
	
	@Inject
	PruebaFacade pruebaFacade;
	
	@Inject
	PruebasService pruebaService;
	
	@Deployment
	public static WebArchive createDeployment(){		
		//Puede generar un out of memory exception debido a la dependencia circular
		File[] files = Maven.resolver().loadPomFromFile("pom.xml").resolve("joda-time:joda-time:2.3").withTransitivity().asFile();	
		return ShrinkWrap.create(WebArchive.class,"test.war").addAsWebInfResource("test-ds.xml")
				.addAsLibraries(files)
				.addClass(AbstractFacade.class)
				.addPackage(Prueba.class.getPackage())
				.addClass(PruebaFacade.class)
				.addClass(PruebaDTO.class)
				.addClass(PruebasService.class)		
				.addAsResource("META-INF/test-persistence.xml","META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE,"beans.xml");
	}
	
	
	/**
	 * Debe probarse sobre la base de datos de prueba con la revision
	 * para el vehiculo OTTOV1 creado
	 */
	@Test
	public void testCargarPrueba(){
			Prueba prueba = pruebaFacade.find(186);
			prueba.getEquipos();
			Assert.assertNotNull(prueba);
	}
	
	@Test
	public void testCargarPruebaDTO(){
		try{
			PruebaDTO pruebaDTO = pruebaService.consultarInfoPruebaDTO(186);
			Assert.assertNotNull(pruebaDTO);
		}catch(Exception exc){
			Assert.fail("Error testCargarPruebaDTO");
		}
	}
	
	@Test
	public void testCargarMedidas(){
		try{
			Prueba prueba = new Prueba();
			prueba.setPruebaId(394);
			pruebaService.obtenerMedidas(prueba);
		}catch(Exception exc){
			Assert.fail();
		}
	}
	
	@Test
	public void testCargarPruebas(){
		try{
			List<Prueba> pruebas = pruebaService.pruebasRevision(48);
			Assert.assertNotNull(pruebas);
		}catch(Exception exc){
			Assert.fail();
		}
	}
	
		
}
