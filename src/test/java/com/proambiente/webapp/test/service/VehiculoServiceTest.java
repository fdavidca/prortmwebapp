package com.proambiente.webapp.test.service;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.proambiente.modelo.Defecto;
import com.proambiente.modelo.dto.RevisionDTO;
import com.proambiente.webapp.service.FechaService;
import com.proambiente.webapp.service.InspeccionService;
import com.proambiente.webapp.service.PasswordHash;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.service.VehiculoService;

@RunWith(Arquillian.class)
public class VehiculoServiceTest {
	
	
	private static final Logger logger = Logger.getLogger(VehiculoServiceTest.class.getName());
	
	@Inject
	VehiculoService vehiculoService;
	
	
	@Deployment
	public static WebArchive createDeployment(){		
		//Puede generar un out of memory exception debido a la dependencia circular
		File[] files = Maven.resolver().loadPomFromFile("pom.xml").resolve("joda-time:joda-time:2.3").withTransitivity().asFile();
		
		
		return ShrinkWrap.create(WebArchive.class,"test.war").addAsWebInfResource("test-ds.xml")
				.addAsLibraries(files)
				.addPackage(Defecto.class.getPackage())
			    .addPackage("com.proambiente.webapp.dao")			    
				.addClass(VehiculoService.class)				
				.addClass(PasswordHash.class)//estas son minimas
				.addClass(RevisionService.class)//estas son minimas para que el test se ejecute
				.addClass(InspeccionService.class)//estas son minimas para que el test se ejecute
				.addClass(FechaService.class)
				.addPackage(RevisionDTO.class.getPackage())
				.addAsResource("META-INF/test-persistence.xml","META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE,"beans.xml");
	}
	
	@Test
	public void testPlacaPorPruebaId(){
		try{
			String placa = vehiculoService.placaDesdePruebaId(5);
			Assert.assertNotNull(placa);
		}catch(Exception exc){
			Assert.fail();
			logger.log(Level.SEVERE, " Error tesPlacaPorPruebaId", exc);
			
		}
	}
}
