package com.proambiente.webapp.test.service;

import java.io.File;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.proambiente.modelo.Analizador;
import com.proambiente.modelo.Aseguradora;
import com.proambiente.modelo.CategoriaDefecto;
import com.proambiente.modelo.CausaAborto;
import com.proambiente.modelo.Certificado;
import com.proambiente.modelo.Ciudad;
import com.proambiente.modelo.ClaseVehiculo;
import com.proambiente.modelo.Color;
import com.proambiente.modelo.Defecto;
import com.proambiente.modelo.DefectoAsociado;
import com.proambiente.modelo.Departamento;
import com.proambiente.modelo.Equipo;
import com.proambiente.modelo.EstadoDefecto;
import com.proambiente.modelo.FiltroDensidadNeutra;
import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.Inspeccion;
import com.proambiente.modelo.LineaVehiculo;
import com.proambiente.modelo.Llanta;
import com.proambiente.modelo.Marca;
import com.proambiente.modelo.Medida;
import com.proambiente.modelo.ModoMostrarNumPasajeros;
import com.proambiente.modelo.ModoReporte;
import com.proambiente.modelo.OperadorSicov;
import com.proambiente.modelo.Pais;
import com.proambiente.modelo.Permisible;
import com.proambiente.modelo.Pista;
import com.proambiente.modelo.Propietario;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.PruebaFuga;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Rol;
import com.proambiente.modelo.Servicio;
import com.proambiente.modelo.ServicioEspecial;
import com.proambiente.modelo.SubCategoriaDefecto;
import com.proambiente.modelo.TipoCarroceria;
import com.proambiente.modelo.TipoCombustible;
import com.proambiente.modelo.TipoDocumentoIdentidad;
import com.proambiente.modelo.TipoEquipo;
import com.proambiente.modelo.TipoMedida;
import com.proambiente.modelo.TipoPista;
import com.proambiente.modelo.TipoPrueba;
import com.proambiente.modelo.TipoVehiculo;
import com.proambiente.modelo.TipoVerificacion;
import com.proambiente.modelo.Usuario;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.modelo.VerificacionGasolina;
import com.proambiente.modelo.VerificacionLinealidad;
import com.proambiente.webapp.dao.AbstractFacade;
import com.proambiente.webapp.dao.CertificadoFacade;
import com.proambiente.webapp.dao.InformacionCdaFacade;
import com.proambiente.webapp.service.CertificadoService;
import com.proambiente.webapp.service.FechaService;
import com.proambiente.webapp.service.PasswordHash;

@RunWith(Arquillian.class)
public class CertificadoServiceTest {
	
	


	
	@Inject
	CertificadoService certificadoService;
	
	@Inject
	FechaService fechaService;
	
	@Deployment
	public static WebArchive createDeployment(){		
		//Puede generar un out of memory exception debido a la dependencia circular
		File[] files = Maven.resolver().loadPomFromFile("pom.xml").resolve("joda-time:joda-time:2.3").withTransitivity().asFile();		
		
		
		return ShrinkWrap.create(WebArchive.class,"test.war").addAsWebInfResource("test-ds.xml")
				.addAsLibraries(files)
				.addClass(Aseguradora.class)
				.addClass(Analizador.class)
				.addClass(Certificado.class)
				.addClass(Revision.class)
				.addClass(Vehiculo.class)
				.addClass(DefectoAsociado.class)
				.addClass(CategoriaDefecto.class)
				.addClass(SubCategoriaDefecto.class)
				.addClass(EstadoDefecto.class)
				.addClass(Propietario.class)
				.addClass(Usuario.class)
				.addClass(Equipo.class)
				.addClass(TipoEquipo.class)
				.addClass(Ciudad.class)
				.addClass(Pais.class)
				.addClass(Color.class)
				.addClass(OperadorSicov.class)
				.addClass(Defecto.class)
				.addClass(ServicioEspecial.class)
				.addClass(Llanta.class)
				.addClass(Prueba.class)
				.addClass(ModoReporte.class)
				.addClass(ModoMostrarNumPasajeros.class)
				.addClass(TipoPrueba.class)
				.addClass(TipoMedida.class)
				.addClass(TipoCarroceria.class)
				.addClass(Rol.class)
				.addClass(Servicio.class)
				.addClass(ClaseVehiculo.class)
				.addClass(LineaVehiculo.class)
				.addClass(TipoCombustible.class)
				.addClass(TipoVehiculo.class)
				.addClass(Marca.class)
				.addClass(Inspeccion.class)
				.addClass(Departamento.class)
				.addClass(Permisible.class)
				.addClass(FiltroDensidadNeutra.class)
				.addClass(TipoDocumentoIdentidad.class)
				.addClass(Defecto.class)
				.addClass(Medida.class)
				.addClass(CausaAborto.class)
				.addClass(InformacionCda.class)
				.addClass(Pista.class)
			    .addClass(FechaService.class)
				.addClass(PasswordHash.class)
				.addClass(CertificadoService.class)				
				.addClass(AbstractFacade.class)
				.addClass(InformacionCdaFacade.class)
				.addClass(CertificadoFacade.class)
				.addClass(VerificacionLinealidad.class)
				.addClass(VerificacionGasolina.class)
				.addClass(PruebaFuga.class)
				.addClass(TipoVerificacion.class)
				.addClass(TipoPista.class)
				.addAsResource("META-INF/test-persistence.xml","META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE,"beans.xml");
	}
	
	
	/**
	 * Debe probarse sobre la base de datos de prueba con la revision
	 * para el vehiculo OTTOV1 creado
	 */
	@Test
	public void testConsecutivoSustrato(){
				certificadoService.consultarCertificadoConsecutivoSustratoExistente("44515720");
	}
	
	
	@Test
	public void testConsecutivoRunt(){
				certificadoService.consultarCertificadoConsecutivoRuntExistente("142706766");
	}
	
	
	
}
