package com.proambiente.webapp.test.service;

import static org.junit.Assert.fail;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.proambiente.modelo.Defecto;
import com.proambiente.modelo.dto.EquipoDTO;
import com.proambiente.webapp.dao.EquipoFacade;
import com.proambiente.webapp.service.FechaService;
import com.proambiente.webapp.service.InspeccionService;
import com.proambiente.webapp.service.PasswordHash;
import com.proambiente.webapp.service.PinCi2Service;
import com.proambiente.webapp.service.RevisionService;

@RunWith(Arquillian.class)
public class PinCi2ServiceTest {
	
	
	private static final Logger logger = Logger.getLogger(PinCi2ServiceTest.class.getName());
	
	@Inject
	PinCi2Service pinCi2Service;
	
		
	@Deployment
	public static WebArchive createDeployment(){		
		
		File[] files = Maven.resolver().loadPomFromFile("pom.xml").importRuntimeDependencies().resolve().withTransitivity().asFile();		
		return ShrinkWrap.create(WebArchive.class,"test.war").addAsWebInfResource("test-ds.xml")
				.addAsLibraries(files)
				.addPackage(Defecto.class.getPackage())
				.addClass(EquipoDTO.class)
			    .addPackage("com.proambiente.webapp.dao")				
				.addClass(EquipoFacade.class)
				.addClass(PasswordHash.class)//estas son minimas
				.addClass(RevisionService.class)//estas son minimas para que el test se ejecute
				.addClass(InspeccionService.class)//estas son minimas para que el test se ejecute
				.addClass(PinCi2Service.class)
				.addClass(FechaService.class)
				.addAsResource("META-INF/test-persistence.xml","META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE,"beans.xml");
	}
	
	
	@Test
	public void testActualizarPinRevision(){
		try{
			pinCi2Service.registrarPinUtilizado("34444334234432", 100);
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error testConsultarEquiposPorIds", exc);
			fail("Error testConsultarEquiposPorIds");
		}
	}
	
	
	
	
	
	
}
