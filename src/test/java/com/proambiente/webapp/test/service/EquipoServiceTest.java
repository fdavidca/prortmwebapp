package com.proambiente.webapp.test.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.proambiente.modelo.Defecto;
import com.proambiente.modelo.dto.EquipoDTO;
import com.proambiente.webapp.dao.EquipoFacade;
import com.proambiente.webapp.service.FechaService;
import com.proambiente.webapp.service.InspeccionService;
import com.proambiente.webapp.service.PasswordHash;
import com.proambiente.webapp.service.RevisionService;

@RunWith(Arquillian.class)
public class EquipoServiceTest {
	
	
	private static final Logger logger = Logger.getLogger(EquipoServiceTest.class.getName());
	
	@Inject
	EquipoFacade equipoFacade;
	
		
	@Deployment
	public static WebArchive createDeployment(){		
		
		File[] files = Maven.resolver().loadPomFromFile("pom.xml").importRuntimeDependencies().resolve().withTransitivity().asFile();		
		return ShrinkWrap.create(WebArchive.class,"test.war").addAsWebInfResource("test-ds.xml")
				.addAsLibraries(files)
				.addClass(FechaService.class)
				.addPackage(Defecto.class.getPackage())
				.addClass(EquipoDTO.class)			    				
				.addClass(EquipoFacade.class)				
				.addClass(PasswordHash.class)//estas son minimas
				.addClass(RevisionService.class)//estas son minimas para que el test se ejecute
				.addClass(InspeccionService.class)//estas son minimas para que el test se ejecute
				.addAsResource("META-INF/test-persistence.xml","META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE,"beans.xml");
	}
	
	
	@Test
	public void testConsultarEquiposPorIds(){
		try{
			List<String> ids = new ArrayList<>();
			ids.add("LX1111");
			ids.add("TS234");
			ids.add("VQS8888");
			List<EquipoDTO> equipos  = equipoFacade.consultarEquiposPorIds(ids);
			assertNotNull(equipos);
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error testConsultarEquiposPorIds", exc);
			fail("Error testConsultarEquiposPorIds");
		}
	}
	
	@Test
	public void testConsultarEquiposPorPrueba(){
		try{
			List<String> serieEquipos = equipoFacade.consultarEquiposPorPruebaId(814);
			assertNotNull(serieEquipos);
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error testConsultarEquiposPorIds", exc);
			fail("Error testConsultarEquiposPorIds");
		}
	}
	
	
	
	
}
