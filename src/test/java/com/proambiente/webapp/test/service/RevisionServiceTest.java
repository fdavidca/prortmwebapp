package com.proambiente.webapp.test.service;

import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Revision_;
import com.proambiente.modelo.Usuario;
import com.proambiente.modelo.dto.PruebaDTO;
import com.proambiente.modelo.dto.RevisionDTO;
import com.proambiente.webapp.dao.InspeccionFacade;
import com.proambiente.webapp.service.FechaService;
import com.proambiente.webapp.service.InspeccionService;
import com.proambiente.webapp.service.PasswordHash;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.service.builderdto.RevisionDTOConstructor;

@RunWith(Arquillian.class)
public class RevisionServiceTest {
	
	
	private static final Logger logger = Logger.getLogger(RevisionServiceTest.class.getName());
	
	@Inject
	InspeccionFacade inspeccionFacade;
	
	@Inject
	InspeccionService inspeccionService;
	
	@Inject
	RevisionService revisionService;
	
	@Inject
	FechaService fechaService;
	
	@Deployment
	public static WebArchive createDeployment(){		
		
		File[] files = Maven.resolver().loadPomFromFile("pom.xml").resolve("joda-time:joda-time:2.3").withTransitivity().asFile();		
		return ShrinkWrap.create(WebArchive.class,"test.war").addAsWebInfResource("test-ds.xml")
				.addAsLibraries(files)
				.addPackage(Usuario.class.getPackage())
			    .addPackage("com.proambiente.webapp.dao")
			    .addClass(FechaService.class)
				.addClass(InspeccionService.class)
				.addClass(RevisionService.class)
				.addClass(PasswordHash.class)
				.addClass(RevisionDTOConstructor.class)
				.addPackage(com.proambiente.modelo.dto.builder.RevisionDTOBuilder.class.getPackage())
				.addPackage(RevisionDTO.class.getPackage())
				.addAsResource("META-INF/test-persistence.xml","META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE,"beans.xml");
	}
	
	
	/**
	 * Debe probarse sobre la base de datos CDA BIOCAR 2024 
	 * 
	 * 
	 */
	@Test
	public void testBuscarPorPlaca(){
				logger.log(Level.INFO, "Buscar por placa iniciando");
				assertNotNull(inspeccionFacade);
				assertNotNull(inspeccionService);
				assertNotNull(revisionService);
				Revision revision = revisionService.buscarRevisionPorPlaca("HAS396");
				assertNotNull(revision);
	}
	
	@Test
	public void testBuscarPorRevisionPorTipoPrueba(){
		try{
			Prueba prueba = revisionService.buscarPruebaPorTipoPorRevision(1, 1);
			assertNotNull(prueba);
			
		}catch(Exception exc){
			Assert.fail();
		}
	}
	
	@Test
	public void testBuscarPrueasPorRevisionIds(){
		try{
			Integer[] revisiones = {1};
			List<PruebaDTO> pruebas = revisionService. pruebasDTOPorIdRevisiones(Arrays.asList(revisiones));
						
		}catch(Exception exc){
			Assert.fail();
		}
	}
	
	
	/**
	 * Metodo para probar que la consulta de las revisiones por fecha funcione
	 * correctamente, no debe lanzar excepcion
	 */
	@Test
	public void testConsultarRevisionesRegistradas(){
		
		logger.log(Level.INFO, "Iniciando prueba test consultar revisiones registradas");
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		String fechaInicialStr = "1-01-2024";
		String fechaFinalStr = "1-02-2024";
		try{
			Date fechaInicial = formatter.parse(fechaInicialStr);
			Date fechaFinal = formatter.parse(fechaFinalStr);
			List<Revision> revisiones = revisionService.consultarRevisionesRegistradas(fechaInicial, fechaFinal,false,Optional.empty());
			Assert.assertNotNull(revisiones);
		}catch(ParseException e){
			Assert.fail("Fechas mal escritas no se pudieron procesar");
		}catch(Exception exc){
			
			Assert.fail("Error consultando revisiones por fecha");
		}
	}
	
	/**
	 * Metodo para probar que se pueda consultar una prueba no finalizada por tipo
	 */
	@Test
	public void testbuscarPruebaNoFinalizada(){
		
		try {
			revisionService.buscarPruebaNoFinalizada("OTTOV1", 8);
		} catch (Exception e) {			
			e.printStackTrace();
			Assert.fail("Error en test buscarPruebaNoFinalizada");
		}
	}
	
	/**
	 * Consulta todas las revisiones por placa para un vehiculo
	 */
	@Test
	public void testBuscarTodasRevisionesPorPlaca(){
		try{
			logger.log(Level.INFO, "Ejecutando todas revisiones por placa");
			revisionService.buscarTodasRevisionesPorPlaca("INR561");
		}catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test fallido para metodo buscarTodadRevisionesPorPlaca");
		}
	}
	
	
	/**
	 * Consulta las revisiones y las inspecciones entre las fechas,
	 * prueba que el metodo no lance excepción 
	 */
	@Test
	public void testConsultarRevisionesInspecciones(){
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		String fechaInicialStr = "1-08-2024";
		String fechaFinalStr = "2-08-2024";
		try{
			Date fechaInicial = formatter.parse(fechaInicialStr);
			Date fechaFinal = formatter.parse(fechaFinalStr);
			List<Revision> revisiones = revisionService.consultarRevisionesInspecciones(fechaInicial, fechaFinal,false,Optional.empty());
			Assert.assertNotNull(revisiones);
		}catch(ParseException e){
			Assert.fail("Fechas mal escritas no se pudieron procesar");
		}catch(Exception exc){
			
			Assert.fail("Error consultando revisiones e inspecciones por fecha");
		}
	}
	
	
	@Test
	public void testConsultarRevisionesDTO(){		
		try{			
			Revision r = revisionService.cargarRevisionDTO(1, Revision_.aprobada,Revision_.revisionId,Revision_.fechaCreacionRevision);
			Assert.assertNotNull(r.getRevisionId());
		}catch(Exception exc){			
			Assert.fail("Error consultando revisiones e inspecciones por fecha");
		}
	}
	
	
	/**
	 * Test que prueba que se cargue la revision  en la base de datos de prueba
	 * 
	 */
	@Test
	public void testCargarRevision(){
		try{
			Revision revision = revisionService.cargarRevision(2253);
			assertNotNull(revision);			
		}catch(Exception exc){			
			exc.printStackTrace();
			Assert.fail("Prueba falida para metodo testCargarRevision");
		}
	}
	
	@Test
	public void testRegistrarPrimerEnvio() {
		try {
			 revisionService.registrarPrimerEnvio(1);
		}catch(Exception exc) {
			Assert.fail("Prueba falida para metodo testCargarRevision");
		}
	}
	
	@Test
	public void testRegistrarSegundoEnvio() {
		try {
			 revisionService.registrarSegundoEnvio(1);
		}catch(Exception exc) {
			Assert.fail("Prueba falida para metodo testCargarRevision");
		}
	}
	
	
}
