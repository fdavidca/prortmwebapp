package com.proambiente.webapp.test.service;

import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.proambiente.modelo.Defecto;
import com.proambiente.modelo.DefectoAsociado;
import com.proambiente.modelo.EstadoDefecto;
import com.proambiente.modelo.dto.RevisionDTO;
import com.proambiente.webapp.dao.DefectoFacade;
import com.proambiente.webapp.service.DefectoService;
import com.proambiente.webapp.service.FechaService;
import com.proambiente.webapp.service.InspeccionService;
import com.proambiente.webapp.service.PasswordHash;
import com.proambiente.webapp.service.RevisionService;

@RunWith(Arquillian.class)
public class DefectoServiceTest {
	
	@Inject
	DefectoFacade defectoDAO;
	
	@Inject
	DefectoService defectoService;
	
	
	
	@Deployment
	public static WebArchive createDeployment(){		
		//Puede generar un out of memory exception debido a la dependencia circular
		File[] files = Maven.resolver().loadPomFromFile("pom.xml").resolve("joda-time:joda-time:2.3").withTransitivity().asFile();			
		return ShrinkWrap.create(WebArchive.class,"test.war").addAsWebInfResource("test-ds.xml")
				.addAsLibraries(files)
				.addPackage(Defecto.class.getPackage())
			    .addPackage("com.proambiente.webapp.dao")			    
				.addClass(DefectoService.class)
				.addClass(DefectoFacade.class)
				.addClass(PasswordHash.class)//estas son minimas
				.addClass(RevisionService.class)//estas son minimas para que el test se ejecute
				.addClass(InspeccionService.class)//estas son minimas para que el test se ejecute
				.addClass(FechaService.class)
				.addPackage(RevisionDTO.class.getPackage())
				.addAsResource("META-INF/test-persistence.xml","META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE,"beans.xml");
	}
	
	/**
	 * Consulta todas las revisiones por placa para un vehiculo
	 */
	@Test
	public void testObtenerDefectoPorId(){
		try{
			Defecto defecto = defectoService.obtenerDefectoPorId( 80000 );
			assertNotNull(defecto);
		}catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test fallido para metodo obtenerDefectoPorId ");
		}
	}	
	
	@Test
	public void testObtenerDefectosInspeccionSensorial(){
		try{
			List<DefectoAsociado> defectos = defectoService.obtenerDefectosInspeccionSensorial(48);
			assertNotNull(defectos);
			for(DefectoAsociado d : defectos){
				System.out.println(d.getDefecto().getTipoDefecto() + " - " + d.getDefecto().getDefectoId() + " - " + d.getDefecto().getCodigoDefecto() );
			}
		}catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test fallido para metodo obtenerDefectosInspeccionSensorial");
		}
	}
	
	@Test
	public void testObtenerDefectosInspeccionMecanizada(){
		try{
			List<Defecto> defectos = defectoService.obtenerDefectosInspeccionMecanizada(48);
			assertNotNull(defectos);
			for(Defecto d : defectos){
				System.out.println(d.getTipoDefecto() + " - " + d.getDefectoId() + " - " + d.getCodigoDefecto() );
			}
		}catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test fallido para metodo obtenerDefectosInspeccionMecanizada");
		}
	}
	
	@Test
	public void testObtenerDefectosInspeccionMecanicazadaReinspeccion(){
		try{
			List<Defecto> defectos = defectoService.obtenerDefectosInspeccionMecanizadaReinspeccion(17367,EstadoDefecto.DETECTADO);
			assertNotNull(defectos);
			for(Defecto d : defectos){
				System.out.println(d.getTipoDefecto() + " - " + d.getDefectoId() + " - " + d.getCodigoDefecto() );
			}
		}catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test fallido para metodo obtenerDefectosInspeccionMecanizada");
		}
	}
	
	@Test
	public void testObtenerDefectosInspeccionVisualReinspeccion(){
		try{
			List<Defecto> defectos = defectoService.obtenerDefectosInspeccionVisualReinspeccion(17367,EstadoDefecto.DETECTADO);
			assertNotNull(defectos);
			for(Defecto d : defectos){
				System.out.println(d.getTipoDefecto() + " - " + d.getDefectoId() + " - " + d.getCodigoDefecto() );
			}
		}catch(Exception e){
			e.printStackTrace();
			Assert.fail("Test fallido para metodo obtenerDefectosInspeccionMecanizada");
		}
	}
}
