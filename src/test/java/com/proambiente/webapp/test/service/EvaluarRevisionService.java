package com.proambiente.webapp.test.service;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.proambiente.modelo.Usuario;
import com.proambiente.webapp.dao.InspeccionFacade;
import com.proambiente.webapp.service.InspeccionService;
import com.proambiente.webapp.service.PasswordHash;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.service.builderdto.RevisionDTOConstructor;

@RunWith(Arquillian.class)
public class EvaluarRevisionService {
	
	
	private static final Logger logger = Logger.getLogger(EvaluarRevisionService.class.getName());
	
	@Inject
	InspeccionFacade inspeccionFacade;
	
	@Inject
	InspeccionService inspeccionService;
	
	@Inject
	RevisionService revisionService;
	
	@Deployment
	public static WebArchive createDeployment(){		
		
		File[] files = Maven.resolver().loadPomFromFile("pom.xml").resolve("joda-time:joda-time:2.3").withTransitivity().asFile();			
		return ShrinkWrap.create(WebArchive.class,"test.war").addAsWebInfResource("test-ds.xml")
				.addAsLibraries(files)
				.addPackage(Usuario.class.getPackage())
			    .addPackage("com.proambiente.webapp.dao")			    
				.addClass(InspeccionService.class)
				.addClass(RevisionService.class)
				.addClass(PasswordHash.class)
				.addClass(RevisionDTOConstructor.class)
				.addAsResource("META-INF/test-persistence.xml","META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE,"beans.xml");
	}	
	
	/**
	 * Test que prueba que se cargue la revision numero 6 en la base de datos de prueba
	 * 
	 */
	@Test
	public void testEvaluarRevisionReprobadaLiviano(){
		try{
			String evaluacion = revisionService.evaluarRevision(48);
			Assert.assertEquals("reprobada",evaluacion);		
		}catch(Exception exc){			
			exc.printStackTrace();
			Assert.fail("Prueba falida para metodo testCargarRevision");
		}
	}
	
	
	@Test
	public void testEvaluarRevisionAprobadaLiviano(){
		try{
			String evaluacion = revisionService.evaluarRevision(83);
			Assert.assertEquals("aprobada",evaluacion);
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Evaluacion incorrecta ", exc);
		}
	}
	
	
	
	@Test
	public void testEvaluarRevisionNoFinalizadaLiviano(){
		try{
			String evaluacion = revisionService.evaluarRevision(3);
			Assert.assertEquals("no_finalizada", evaluacion);
		}catch(Exception exc){
			logger.log(Level.SEVERE,"Evaluacion incorrecta",exc);
		}
	}
	
	@Test
	public void testEvaluarRevisionAprobadaMotocicleta(){
		try{
			String evaluacion = revisionService.evaluarRevision(44);
			Assert.assertEquals("aprobada",evaluacion);
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Evaluacion incorrecta motocicleta", exc);
		}
	}
	
	@Test
	public void testEvaluarRevisionReprobadaMotocicleta(){
		try{
			String evaluacion = revisionService.evaluarRevision(89);
			Assert.assertEquals("reprobada", evaluacion);
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Evaluacion incorrecta", exc);
		}
	}
	
	@Test
	public void testEvaluarRevisionNoFinalizadaMotocicleta(){
		try{
			String evaluacion = revisionService.evaluarRevision(3);
			Assert.assertEquals("no_finalizada",evaluacion);
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Evaluacion incorrecta", exc);
		}
	}
	
	@Test
	public void testEvaluarRevisionReprobadaPesado(){
		try{
			String evaluacion = revisionService.evaluarRevision(87);
			Assert.assertEquals("reprobada", evaluacion);
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Evaluacion incorrecta", exc);
		}
	}
	
	@Test
	public void testEvaluarRevisionAprobadaPesado(){
		try{
			String evaluacion = revisionService.evaluarRevision(28);
			Assert.assertEquals("aprobada",evaluacion);
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error evaluacion", exc);
		}
	}
	
	@Test
	public void testEvaluarRevisionNoFinalizadaPesado(){
		try{
			String evaluacion = revisionService.evaluarRevision(16);
			Assert.assertEquals("no_finalizada", evaluacion);
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error evaluacion", exc);
		}
	}
	
	@Test
	public void testEvaluarPreventivaAprobadaPesado(){
		try{
			String evaluacion = revisionService.evaluarRevision(30);
			Assert.assertEquals("aprobada", evaluacion);
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error evaluacion", exc);
		}
	}
	
	@Test
	public void testEvaluarPreventivaAprobadaLiviano(){
		try{
			String evaluacion = revisionService.evaluarRevision(50);
			Assert.assertEquals("aprobada",evaluacion);
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error evaluacion", exc);
		}
	}
	
	
	
	
	@Test
	public void testEvaluarPreventivaAprobadaMotocicleta(){
		try{
			String evaluacion = revisionService.evaluarRevision(66);
			Assert.assertEquals("aprobada",evaluacion);
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error evaluacion", exc);
		}
	}
	
	@Test
	public void testEvaluarPreventivaReprobadaPesado(){
		try{
			String evaluacion = revisionService.evaluarRevision(31);
			Assert.assertEquals("reprobada",evaluacion);
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error evaluacion", exc);
		}
	}
	
	@Test
	public void testEvaluarPreventivaReprobadaLiviano(){
		try{
			String evaluacion = revisionService.evaluarRevision(49);
			Assert.assertEquals("reprobada", evaluacion);
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error evaluacion", exc);
		}
	}
	
	@Test
	public void testEvaluarPreventivaReprobadaMoto(){
		try{
			String evaluacion = revisionService.evaluarRevision(45);
			Assert.assertEquals("reprobada", evaluacion);
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error evaluacion", exc);
		}
	}
	
	@Test
	public void testEvaluarPreventivaNoFinalizadaMoto(){
		try{
			String evaluacion = revisionService.evaluarRevision(59);
			Assert.assertEquals("no_finalizada", evaluacion);
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error evaluacion", exc);
		}
	}
	
	@Test
	public void testEvaluarPreventivaNoFinalizadaLivianio(){
		try{
			String evaluacion = revisionService.evaluarRevision(51);
			Assert.assertEquals("no_finalizada", evaluacion);
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error evaluacion", exc);
		}
	}
	
	@Test
	public void testEvaluarPreventivaNoFinalizadaPesado(){
		try{
			String evaluacion = revisionService.evaluarRevision(34);
			Assert.assertEquals("no_finalizada", evaluacion);
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error evaluacion", exc);
		}
	}
	
}
