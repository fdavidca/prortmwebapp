package com.proambiente.webapp.test.service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.Assert;
import org.junit.Test;

import com.proambiente.webapp.service.indra.IndraEncryptor;

public class TestIndraEncryptor {
	
	
	private static final Logger logger = Logger.getLogger(TestIndraEncryptor.class.getName());
	
	private static final String	iv="sicovcontacindra" ;
	private static final String key="v239pShjXXXXXXXXXXXXXXXXXXXXXXXX";
	private static final String CADENA_ENCRIPTADA = "DqpO3ppvtk35r5SdI8aw8wZyWJkX6h6gp8MmEXsXzlo=";
	

	IndraEncryptor indraEncriptor = new IndraEncryptor();
	
	@Test
	public void testEncriptar(){
		try{
		String cadenaPrueba = "Lorem ipsum dolor sit amet";
		
		String cadenaEncriptada = indraEncriptor.encrypt(key, iv, cadenaPrueba);
		
		String cadenaDecriptada = indraEncriptor.decrypt(key, iv, cadenaEncriptada);
		
		logger.log(Level.INFO, "decriptada: "+ cadenaDecriptada);
		
		//Assert.assertEquals(cadenaDecriptada,cadenaPrueba);
		Assert.assertEquals(cadenaEncriptada, CADENA_ENCRIPTADA);
		
		logger.log(Level.INFO, "Cadena Encriptada " + cadenaEncriptada);
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error ", exc);
			Assert.fail();
		}
		
	}
	
	
}
