package com.proambiente.webapp.test;

import java.io.File;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.proambiente.modelo.Bitacora;
import com.proambiente.modelo.FiltroDensidadNeutra;
import com.proambiente.modelo.dto.EquipoDTO;
import com.proambiente.webapp.dao.AbstractFacade;
import com.proambiente.webapp.dao.BitacoraFacade;
import com.proambiente.webapp.dao.FiltroDensidadNeutraFacade;

@RunWith(Arquillian.class)
public class FiltroDensidadNeutraFacadeTest {
	
	@Inject
	FiltroDensidadNeutraFacade filtroDensidadNeutraFacade;
	
	@Deployment
	public static WebArchive createDeployment(){		
		
		File[] files = Maven.resolver().loadPomFromFile("pom.xml").resolve("joda-time:joda-time:2.3").withTransitivity().asFile();		
		return ShrinkWrap.create(WebArchive.class,"test.war").addAsWebInfResource("test-ds.xml")
				.addAsLibraries(files)
				.addClass(AbstractFacade.class).addPackage(FiltroDensidadNeutra.class.getPackage()).addClass(FiltroDensidadNeutraFacade.class).addClass(EquipoDTO.class)
				.addAsResource("META-INF/test-persistence.xml","META-INF/persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE,"beans.xml");
	}
	
	@Test
	public void testEncontrarFiltroDensidadNeutra(){
		try{
			FiltroDensidadNeutra filtroDensidadNeutra = filtroDensidadNeutraFacade.find(1); 
			System.out.println("119j : " + filtroDensidadNeutra);
		}catch(Exception exc){
			exc.printStackTrace();
			Assert.fail("Error consultar filtro densidad por id");
		}
				
	}
	
	@Test
	public void testListarFiltroDensidadNeutra(){
		try{
			List<FiltroDensidadNeutra> lista = filtroDensidadNeutraFacade.findRange(new int[] {0,10});
			System.out.println("Bitacoras encontrados" + lista.size());
		}catch(Exception exc){
			exc.printStackTrace();
			Assert.fail("error consultando bitacora");
		}
	}
	
	@Test
	public void testGuardarFiltroDensidadNeutra() {
		try {
			
			FiltroDensidadNeutra filtro = new FiltroDensidadNeutra();
			
			filtro.setCalibradoPor("EUROMETRIC");
			filtro.setFechaCalibracion(new Timestamp(new Date().getTime()));
			filtro.setLaboratorioEmisorCertificado("phenix");
			filtro.setMarca("CAPELEC");
			filtro.setModelo("A1-23");
			filtro.setSerie("125489");
			filtro.setValor(new BigDecimal(70.5));
			filtroDensidadNeutraFacade.edit(filtro);
			
		}catch(Exception exc) {
			exc.printStackTrace();
			Assert.fail("error guardando entidad");
		}
	}

}
