package com.proambiente.webapp.test;

import java.io.File;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.proambiente.modelo.Bitacora;
import com.proambiente.modelo.dto.EquipoDTO;
import com.proambiente.webapp.dao.AbstractFacade;
import com.proambiente.webapp.dao.BitacoraFacade;

@RunWith(Arquillian.class)
public class BitacoraFacadeTest {
	
	@Inject
	BitacoraFacade bitacoraFacade;
	
	@Deployment
	public static WebArchive createDeployment(){		
		
		File[] files = Maven.resolver().loadPomFromFile("pom.xml").resolve("joda-time:joda-time:2.3").withTransitivity().asFile();		
		return ShrinkWrap.create(WebArchive.class,"test.war").addAsWebInfResource("test-ds.xml")
				.addAsLibraries(files)
				.addClass(AbstractFacade.class).addPackage(Bitacora.class.getPackage()).addClass(BitacoraFacade.class).addClass(EquipoDTO.class)
				.addAsResource("META-INF/test-persistence.xml","META-INF/persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE,"beans.xml");
	}
	
	@Test
	public void testEncontrarBitacora(){
		try{
			Bitacora bitacora = bitacoraFacade.find(1); 
			System.out.println("119j : " + bitacora);
		}catch(Exception exc){
			exc.printStackTrace();
			Assert.fail("Error consultar bitacora por id");
		}
				
	}
	
	@Test
	public void testListarBitacora(){
		try{
			List<Bitacora> listaAlerta = bitacoraFacade.findRange(new int[] {0,10});
			System.out.println("Bitacoras encontrados" + listaAlerta.size());
		}catch(Exception exc){
			exc.printStackTrace();
			Assert.fail("error consultando bitacora");
		}
	}
	
	@Test
	public void testGuardarBitacora() {
		try {
			
			Bitacora bitacora = new Bitacora();
			
			bitacora.setAccionesRealizadas("-------");
			bitacora.setDescripcion("Descripcion");
			bitacora.setDireccionIp("192.168.0.151");
			bitacora.setTipoEvento("GASES");
			bitacora.setFechaEvento( new Timestamp(new Date().getTime()));
			bitacora.setFechaRegistro( new Timestamp(new Date().getTime()));
			bitacoraFacade.edit(bitacora);
			
		}catch(Exception exc) {
			exc.printStackTrace();
			Assert.fail("error guardando bitacora");
		}
	}

}
