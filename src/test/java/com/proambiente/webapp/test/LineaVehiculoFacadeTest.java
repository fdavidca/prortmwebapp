package com.proambiente.webapp.test;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.proambiente.modelo.LineaVehiculo;
import com.proambiente.modelo.Marca;
import com.proambiente.modelo.dto.EquipoDTO;
import com.proambiente.webapp.dao.AbstractFacade;
import com.proambiente.webapp.dao.LineaVehiculoFacade;
import com.proambiente.webapp.dao.MarcaFacade;

@RunWith(Arquillian.class)
public class LineaVehiculoFacadeTest {
	
	@Inject
	LineaVehiculoFacade lineaVehiculoFacade;
	
	@Inject
	MarcaFacade marcaFacade;
	
	
	@Deployment
	public static WebArchive createDeployment(){		
		
		File[] files = Maven.resolver().loadPomFromFile("pom.xml").resolve("joda-time:joda-time:2.3").withTransitivity().asFile();		
		return ShrinkWrap.create(WebArchive.class,"test.war").addAsWebInfResource("test-ds.xml")
				.addAsLibraries(files)
				.addClass(AbstractFacade.class).addPackage(LineaVehiculo.class.getPackage())
				.addClass(LineaVehiculoFacade.class).addClass(MarcaFacade.class).addClass(EquipoDTO.class)
				.addAsResource("META-INF/test-persistence.xml","META-INF/persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE,"beans.xml");
	}
	
	@Test
	public void testEncontrarLineaVehiculo(){
		try{
			LineaVehiculo lineaVehiculo = lineaVehiculoFacade.find(1); 
			System.out.println("37j : " + lineaVehiculo);
		}catch(Exception exc){
			exc.printStackTrace();
			Assert.fail("Error consultar entidad por id");
		}
				
	}
	
	@Test
	public void testListarLineas(){
		try{
			List<LineaVehiculo> listaLineas = lineaVehiculoFacade.findRange(new int[] {4,120});
			System.out.println("Lista  encontrada: " + listaLineas.size());
		}catch(Exception exc){
			exc.printStackTrace();
			Assert.fail("error consultando lista");
		}
	}
	
	@Test
	public void testGuardarLinea() {
		try {
			Marca marca = marcaFacade.find(1);
			LineaVehiculo lineaVehiculo = new LineaVehiculo();
			lineaVehiculo.setLineaVehiculoId(8956851);
			lineaVehiculo.setCodigoLinea(2122);
			lineaVehiculo.setNombre("KIFA MERAFA");
			lineaVehiculo.setMarca(marca);
			lineaVehiculoFacade.edit(lineaVehiculo);
			
		}catch(Exception exc) {
			exc.printStackTrace();
			Assert.fail("error guardando entidad");
		}
	}

}
