package com.proambiente.webapp.test.registrarusuarioresponsable;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.proambiente.modelo.Equipo;
import com.proambiente.modelo.Inspeccion;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.TipoVehiculo;
import com.proambiente.modelo.Usuario;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.modelo.dto.RevisionDTO;
import com.proambiente.webapp.dao.AbstractFacade;
import com.proambiente.webapp.dao.InformacionCdaFacade;
import com.proambiente.webapp.dao.InspeccionFacade;
import com.proambiente.webapp.dao.RevisionFacade;
import com.proambiente.webapp.dao.UsuarioFacade;
import com.proambiente.webapp.service.AsignarUsuarioResponsableService;
import com.proambiente.webapp.service.FechaService;
import com.proambiente.webapp.service.InspeccionService;
import com.proambiente.webapp.service.PasswordHash;
import com.proambiente.webapp.service.RevisionService;


@RunWith(Arquillian.class)
public class AsignarUsuarioResponsableServiceTest {
	
	
	private static final Logger logger = Logger.getLogger(AsignarUsuarioResponsableServiceTest.class.getName());
	
	@Inject
	AsignarUsuarioResponsableService asignarUsuarioResposableService;
	


	
	@Inject
	RevisionService revisionService;
	
	@Inject
	UsuarioFacade usuarioFacade;
	

	
	@Deployment
	public static WebArchive createDeployment(){		
		
		File[] files = Maven.resolver().loadPomFromFile("pom.xml").resolve("joda-time:joda-time:2.3").withTransitivity().asFile();		
		return ShrinkWrap.create(WebArchive.class,"test.war").addAsWebInfResource("test-ds.xml")
				.addAsLibraries(files)
				.addClass(Usuario.class)
				.addClass(Revision.class)
				.addPackage(Vehiculo.class.getPackage())
				.addPackage(RevisionDTO.class.getPackage())
				.addClass(Inspeccion.class)
			    .addPackage(InformacionCdaFacade.class.getPackage())
			    .addClass(AbstractFacade.class)
			    .addClass(RevisionFacade.class)
			    .addClass(InspeccionFacade.class)
			    .addClass(InspeccionService.class)
			    .addClass(FechaService.class)
				.addClass(RevisionService.class)
				.addClass(PasswordHash.class)
				.addClass(AsignarUsuarioResponsableService.class)				
				.addAsResource("META-INF/test-persistence.xml","META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE,"beans.xml");
	}
	
	
	/**
	 * Debe probarse sobre la base de datos de prueba con la revision
	 * para el vehiculo OTTOV1 creado
	 */
	@Test
	public void testCambiarUsuarioResponsable(){
				logger.log(Level.INFO, "Buscar por placa iniciando");
				assertNotNull(revisionService);
				Revision revision = revisionService.buscarRevisionPorPlaca("OTTOV1");
				Usuario usuario = usuarioFacade.find(revision.getUsuarioResponsableId());				
				Usuario usuarioNuevo = usuarioFacade.find(12);//usuarios por defecto				
				asignarUsuarioResposableService.asignarUsuarioResponsable(revision.getRevisionId(), usuarioNuevo.getUsuarioId());
				assertFalse(usuario.getUsuarioId().equals( usuarioNuevo.getUsuarioId()) );
				assertNotNull(revision.getUsuarioResponsableId());		
	}
	
	
	
}
