package com.proambiente.webapp.test.utilfechavencimiento;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import com.proambiente.modelo.Rol;
import com.proambiente.modelo.Usuario;
import com.proambiente.webapp.service.util.UtilVencimientoRevision;

public class TestPasswordCaducado {

	UtilVencimientoRevision utilVencimientoRevision = new UtilVencimientoRevision();

	public TestPasswordCaducado() {
	}
/*
 * genera un usuario con un mes de fecha de caducidad
 */
	public Usuario generarUsuarioAdmin(LocalDate localDate) {
		Usuario usuario = new Usuario();

		Date fechaCaducaPassword = java.util.Date.from( // Convert from modern java.time class to troublesome old legacy
														// class. DO NOT DO THIS unless you must, to inter operate with
														// old code not yet updated for java.time.
				localDate // `LocalDate` class represents a date-only, without time-of-day and without
							// time zone nor offset-from-UTC.
						.atStartOfDay( // Let java.time determine the first moment of the day on that date in that
										// zone. Never assume the day starts at 00:00:00.
								ZoneId.systemDefault() // Specify time zone using proper name in `continent/region`
														// format, never 3-4 letter pseudo-zones such as “PST”, “CST”,
														// “IST”.
						) // Produce a `ZonedDateTime` object.
						.toInstant() // Extract an `Instant` object, a moment always in UTC.
		);
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fechaCaducaPassword);
		calendar.add(Calendar.MONTH,1);
		usuario.setFechaExpiracionContrasenia(new Timestamp(calendar.getTime().getTime()));
		
		
		usuario.setRols(generarRolAdministrador());
		return usuario;
	}
	
	public Usuario generarUsuarioOperario(LocalDate localDate) {
		Usuario usuario = new Usuario();

		Date fechaCaducaPassword = java.util.Date.from( // Convert from modern java.time class to troublesome old legacy
														// class. DO NOT DO THIS unless you must, to inter operate with
														// old code not yet updated for java.time.
				localDate // `LocalDate` class represents a date-only, without time-of-day and without
							// time zone nor offset-from-UTC.
						.atStartOfDay( // Let java.time determine the first moment of the day on that date in that
										// zone. Never assume the day starts at 00:00:00.
								ZoneId.systemDefault() // Specify time zone using proper name in `continent/region`
														// format, never 3-4 letter pseudo-zones such as “PST”, “CST”,
														// “IST”.
						) // Produce a `ZonedDateTime` object.
						.toInstant() // Extract an `Instant` object, a moment always in UTC.
		);
		
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fechaCaducaPassword);
		calendar.add(Calendar.MONTH,1);
		usuario.setFechaExpiracionContrasenia(new Timestamp(calendar.getTime().getTime()));
		usuario.setRols(generarRolOperario());
		return usuario;
	}

	public List<Rol> generarRolAdministrador() {

		Rol rol = new Rol();
		rol.setNombreRol("ADMINISTRADOR");
		List<Rol> roles = new ArrayList<>();
		roles.add(rol);
		return roles;

	}
	
	public List<Rol> generarRolOperario() {

		Rol rol = new Rol();
		rol.setNombreRol("OPERARIO");
		List<Rol> roles = new ArrayList<>();
		roles.add(rol);
		return roles;

	}

	public boolean checkPasswordExpired(Usuario usuario, Calendar calendarFechaHoy) {

		if (usuario != null) {
			Boolean admin = false;
			for (Rol r : usuario.getRols()) {
				if (r.getNombreRol().equalsIgnoreCase("ADMINISTRADOR")) {
					admin = true;
				}
			}
			if (!admin) {
				long tiempoFechaExpiracion = usuario.getFechaExpiracionContrasenia().getTime();
				Date dateFechaExpiracion = new Date(tiempoFechaExpiracion);
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(dateFechaExpiracion);

				return calendarFechaHoy.compareTo(calendar) >= 0;
			} else {
				long tiempoFechaExpiracion = usuario.getFechaExpiracionContrasenia().getTime();
				Date dateFechaExpiracion = new Date(tiempoFechaExpiracion);
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(dateFechaExpiracion);
				calendar.add(Calendar.DAY_OF_YEAR,-15);
				return calendarFechaHoy.compareTo(calendar) >= 0; 
			}
		}
		return false;

	}

	@Test
	public void testPasswordCaducado() {
		LocalDate localDate = LocalDate.now();
		Usuario usuario = generarUsuarioAdmin(localDate);
		Date date =  Date.from( LocalDate.now().atStartOfDay().plusDays(16).atZone( ZoneId.systemDefault()).toInstant() );
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		Boolean expirado = checkPasswordExpired(usuario, calendar);
		assertTrue(expirado);

	}

	@Test
	public void testPasswordNoCaducado() {
		LocalDate localDate =LocalDate.now();
		Usuario usuario = generarUsuarioOperario(localDate);
		Date date =  Date.from( LocalDate.now().atStartOfDay().plusDays(16).atZone( ZoneId.systemDefault()).toInstant() );
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		Boolean expirado = checkPasswordExpired(usuario, calendar);
		assertFalse(expirado);

	}

}
