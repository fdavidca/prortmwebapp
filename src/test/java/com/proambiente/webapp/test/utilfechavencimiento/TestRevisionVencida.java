package com.proambiente.webapp.test.utilfechavencimiento;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import org.junit.Test;
import static org.junit.Assert.*;

import com.proambiente.webapp.service.util.UtilVencimientoRevision;

public class TestRevisionVencida {

	 UtilVencimientoRevision utilVencimientoRevision = new UtilVencimientoRevision();
	    
	    
	    
	    public TestRevisionVencida() {
	    }
	    
	    @Test
	    public void testRevisionVencida(){
	        LocalDateTime fechaRevisionDt = LocalDateTime.of(2018, 04, 12, 16, 35);
	                
	        Date fechaRevision = Date.from(fechaRevisionDt.atZone(ZoneId.systemDefault()).toInstant());
	        Date ahora = new Date();
	        boolean esVencida = utilVencimientoRevision.esRevisionVencida(fechaRevision, ahora);
	        assertTrue(esVencida);
	    }
	    
	    @Test
	    public void testRevisionNoVencida(){
	        LocalDateTime fechaRevisionDt = LocalDateTime.of(2018, 04, 12, 16, 35);
	        LocalDateTime fechaReinspeccionDt = LocalDateTime.of(2018,04,17,17,00);
	        
	        Date fechaRevision = Date.from(fechaRevisionDt.atZone(ZoneId.systemDefault()).toInstant());
	        Date fechaReinspeccion = Date.from(fechaReinspeccionDt.atZone(ZoneId.systemDefault()).toInstant());
	        boolean esVencida = utilVencimientoRevision.esRevisionVencida(fechaRevision, fechaReinspeccion);
	        assertFalse(esVencida);
	    }
	    
	    @Test
	    public void testRevisionNoVencidaDiaExacto(){
	        LocalDateTime fechaRevisionDt = LocalDateTime.of(2018, 04, 12, 16, 35);
	        LocalDateTime fechaReinspeccionDt = LocalDateTime.of(2018,04,26,17,00);
	        
	        Date fechaRevision = Date.from(fechaRevisionDt.atZone(ZoneId.systemDefault()).toInstant());
	        Date fechaReinspeccion = Date.from(fechaReinspeccionDt.atZone(ZoneId.systemDefault()).toInstant());
	        boolean esVencida = utilVencimientoRevision.esRevisionVencida(fechaRevision, fechaReinspeccion);
	        assertFalse(esVencida);
	        
	    }
	    
	    @Test
	    public void testExtremo(){
	        LocalDateTime fechaRevisionDt = LocalDateTime.of(2018, 04, 12, 23, 59);
	        LocalDateTime fechaReinspeccionDt = LocalDateTime.of(2018,04,26,23,59);
	        
	        Date fechaRevision = Date.from(fechaRevisionDt.atZone(ZoneId.systemDefault()).toInstant());
	        Date fechaReinspeccion = Date.from(fechaReinspeccionDt.atZone(ZoneId.systemDefault()).toInstant());
	        boolean esVencida = utilVencimientoRevision.esRevisionVencida(fechaRevision, fechaReinspeccion);
	        assertFalse(esVencida);
	        
	    }
	    
	    @Test
	    public void testExtremoVencida(){
	        LocalDateTime fechaRevisionDt = LocalDateTime.of(2018, 04, 12, 23, 59);
	        LocalDateTime fechaReinspeccionDt = LocalDateTime.of(2018,04,28,00,00);
	        
	        Date fechaRevision = Date.from(fechaRevisionDt.atZone(ZoneId.systemDefault()).toInstant());
	        Date fechaReinspeccion = Date.from(fechaReinspeccionDt.atZone(ZoneId.systemDefault()).toInstant());
	        boolean esVencida = utilVencimientoRevision.esRevisionVencida(fechaRevision, fechaReinspeccion);
	        assertTrue(esVencida);
	        
	    }
	    
	    @Test
	    public void testEspecificacion(){
	        LocalDateTime fechaRevisionDt = LocalDateTime.of(2018, 04, 01, 07, 00);
	        LocalDateTime fechaReinspeccionDt = LocalDateTime.of(2018,04,16,00,00);
	        
	        Date fechaRevision = Date.from(fechaRevisionDt.atZone(ZoneId.systemDefault()).toInstant());
	        Date fechaReinspeccion = Date.from(fechaReinspeccionDt.atZone(ZoneId.systemDefault()).toInstant());
	        boolean esVencida = utilVencimientoRevision.esRevisionVencida(fechaRevision, fechaReinspeccion);
	        assertFalse(esVencida);
	        
	    }		
	
}
