package com.proambiente.webapp.test.utilfechavencimiento;

import java.util.Date;
import java.time.LocalDateTime;
import java.time.ZoneId;

import org.junit.Test;
import static org.junit.Assert.assertTrue;

import com.proambiente.webapp.service.util.UtilVencimientoRevision;

public class TestCalcularFechaVencimiento {

	UtilVencimientoRevision utilVencimientoRevision = new UtilVencimientoRevision();

	public TestCalcularFechaVencimiento() {
	}

	@Test
	public void testCalcularFechaVencimiento() {
		LocalDateTime fechaRevisionDt = LocalDateTime.of(2018, 04, 01, 07, 00);
		LocalDateTime fechaEsperadaVencimiento = LocalDateTime.of(2018, 04, 16, 07, 00);
		Date fechaRevision = Date.from(fechaRevisionDt.atZone(ZoneId.systemDefault()).toInstant());
		LocalDateTime fechaCalculada = utilVencimientoRevision.calcularFechaVencimientoPlazo(fechaRevision);
		assertTrue(fechaCalculada.isEqual(fechaEsperadaVencimiento));

	}

}
