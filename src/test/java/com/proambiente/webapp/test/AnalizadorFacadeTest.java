package com.proambiente.webapp.test;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.proambiente.modelo.Analizador;
import com.proambiente.modelo.dto.EquipoDTO;
import com.proambiente.webapp.dao.AbstractFacade;
import com.proambiente.webapp.dao.AnalizadorFacade;

@RunWith(Arquillian.class)
public class AnalizadorFacadeTest {
	
	@Inject
	AnalizadorFacade analizadorFacade;
	
	@Deployment
	public static WebArchive createDeployment(){		
		
		File[] files = Maven.resolver().loadPomFromFile("pom.xml").resolve("joda-time:joda-time:2.3").withTransitivity().asFile();		
		return ShrinkWrap.create(WebArchive.class,"test.war").addAsWebInfResource("test-ds.xml")
				.addAsLibraries(files)
				.addClass(AbstractFacade.class).addPackage(Analizador.class.getPackage()).addClass(AnalizadorFacade.class).addClass(EquipoDTO.class)
				.addAsResource("META-INF/test-persistence.xml","META-INF/persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE,"beans.xml");
	}
	
	@Test
	public void testEncontrarAnalizador(){
		try{
			Analizador analizador = analizadorFacade.find("3004"); 
			System.out.println("37j : " + analizador);
		}catch(Exception exc){
			exc.printStackTrace();
			Assert.fail("Error consultar analizador por id");
		}
				
	}
	
	@Test
	public void testListarAnalizadores(){
		try{
			List<Analizador> listaAnalizador = analizadorFacade.findAll();
			System.out.println("Analizador encontrado: " + listaAnalizador.size());
		}catch(Exception exc){
			exc.printStackTrace();
			Assert.fail("error consultando alerta");
		}
	}
	
	@Test
	public void testGuardarAlerta() {
		try {
			
			Analizador analizador = new Analizador();
			analizador.setBloqueoAutocero(false);
			analizador.setBloqueoAutoridadAmbiental(false);
			analizador.setBloqueoResiduos(false);
			analizador.setFabricante("sensors");
			analizador.setLtoe(364);
			analizador.setMarca("opus" );
			analizador.setModelo(" CAP 3300 ");
			analizador.setModeloBancoGases("AMB II");
			analizador.setPef(0.5);
			analizador.setSerial("3004");
			analizador.setSerialAnalizador("4856214");
			analizador.setSerialBancoGases("12589");
			analizador.setTipo("ANALIZADOR");
			analizadorFacade.edit(analizador);
			
		}catch(Exception exc) {
			exc.printStackTrace();
			Assert.fail("error guardando alerta");
		}
	}

}
