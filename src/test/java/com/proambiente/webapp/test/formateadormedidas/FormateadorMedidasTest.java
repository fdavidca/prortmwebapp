package com.proambiente.webapp.test.formateadormedidas;

import org.junit.Test;

import com.proambiente.modelo.Medida;
import com.proambiente.webapp.service.util.FormateadorMedidasResolucion;

public class FormateadorMedidasTest {

	@Test
	public void testFormatearHumedad() {
		
		Double humedad = 60.5;
		
		Medida m = new Medida();
		m.setValor(humedad);
		
		FormateadorMedidasResolucion formateador = new FormateadorMedidasResolucion();
		String medidaFormateada = formateador.formatearMedida(m);
		
		System.out.println("Medida formateada :" + medidaFormateada );
		
		
		
	}
	
	@Test
	public void testFormatearTemperatura() {
		
		Double temperatura = 9.5;
		
		Medida m = new Medida();
		m.setValor(temperatura);
		
		FormateadorMedidasResolucion formateador = new FormateadorMedidasResolucion();
		String medidaFormateada = formateador.formatearMedida(m);
		
		System.out.println("Medida formateada :" + medidaFormateada );
		
		
		
	}
	
	
	@Test
	public void testFormatearOxigeno() {
		
		Double oxigeno =6.21;
		
		Medida m = new Medida();
		m.setValor(oxigeno);
		
		FormateadorMedidasResolucion formateador = new FormateadorMedidasResolucion();
		String medidaFormateada = formateador.formatearMedida(m);
		
		System.out.println("Medida formateada :" + medidaFormateada );
		
		
		
	}
	
	@Test
	public void testFormatearDesviacion() {
		
		Double oxigeno =-3.1;
		
		Medida m = new Medida();
		m.setValor(oxigeno);
		
		FormateadorMedidasResolucion formateador = new FormateadorMedidasResolucion();
		String medidaFormateada = formateador.formatearMedida(m);
		
		System.out.println("Medida formateada :" + medidaFormateada );
		
		
		
	}
	
	@Test
	public void testFormatearError() {
		
		Double oxigeno =-13.1;
		
		Medida m = new Medida();
		m.setValor(oxigeno);
		
		FormateadorMedidasResolucion formateador = new FormateadorMedidasResolucion();
		String medidaFormateada = formateador.formatearMedida(m);
		
		System.out.println("Medida formateada :" + medidaFormateada );
		
		
		
	}
	
	
	@Test
	public void testFormatearCero() {
		
		Double oxigeno =-13.1;
		
		Medida m = new Medida();
		m.setValor(oxigeno);
		
		FormateadorMedidasResolucion formateador = new FormateadorMedidasResolucion();
		String medidaFormateada = formateador.formatearMedida(m);
		
		System.out.println("Medida formateada :" + medidaFormateada );
		
		
		
	}
	
}
