package com.proambiente.webapp.test.formateadormedidas;

import org.junit.Test;

import com.proambiente.modelo.Medida;
import com.proambiente.webapp.service.util.FormateadorMedidasInterface;
import com.proambiente.webapp.service.util.FormateadorMedidasResolucion;
import com.proambiente.webapp.service.util.FormateadorMedidasResolucionRedondeado;

import org.junit.Assert;

public class FormateadorMedidasResolucionRedondeadoTest {

	@Test
	public void testFormatearHumedad() {
		
		Double humedad = 60.5;
		
		Medida m = new Medida();
		m.setValor(humedad);
		
		FormateadorMedidasInterface formateador = new FormateadorMedidasResolucionRedondeado();
		String medidaFormateada = formateador.formatearMedida(m);
		
		System.out.println("Medida formateada :" + medidaFormateada );
		Assert.assertEquals("formateado","60.5", medidaFormateada);
		
		
		
	}
	
	@Test
	public void testFormatearTemperatura() {
		
		Double temperatura = 9.5;
		
		Medida m = new Medida();
		m.setValor(temperatura);
		
		FormateadorMedidasInterface formateador = new FormateadorMedidasResolucionRedondeado();
		String medidaFormateada = formateador.formatearMedida(m);
		
		System.out.println("Medida formateada :" + medidaFormateada );
		
		Assert.assertEquals("formateado","9.50", medidaFormateada);
		
	}
	
	
	@Test
	public void testFormatearOxigeno() {
		
		Double oxigeno =17.771;
		
		Medida m = new Medida();
		m.setValor(oxigeno);
		
		FormateadorMedidasInterface formateador = new FormateadorMedidasResolucionRedondeado();
		String medidaFormateada = formateador.formatearMedida(m);
		
		Assert.assertEquals("formateado","17.8", medidaFormateada);
		
		
	}
	
	@Test
	public void testFormatearDesviacion() {
		
		Double oxigeno =-3.1;
		
		Medida m = new Medida();
		m.setValor(oxigeno);
		
		FormateadorMedidasInterface formateador = new FormateadorMedidasResolucionRedondeado();
		String medidaFormateada = formateador.formatearMedida(m);
		
		System.out.println("Medida formateada :" + medidaFormateada );
		
		Assert.assertEquals("formateado","-3.10", medidaFormateada);
		
	}
	
	@Test
	public void testFormatearError() {
		
		Double oxigeno =-13.1;
		
		Medida m = new Medida();
		m.setValor(oxigeno);
		
		FormateadorMedidasInterface formateador = new FormateadorMedidasResolucionRedondeado();
		String medidaFormateada = formateador.formatearMedida(m);
		
		System.out.println("Medida formateada :" + medidaFormateada );
		Assert.assertEquals("formateado","-13.1", medidaFormateada);
		
		
	}
	
	
	@Test
	public void testFormatearCero() {
		
		Double oxigeno = 123.0;
		
		Medida m = new Medida();
		m.setValor(oxigeno);
		
		FormateadorMedidasInterface formateador = new FormateadorMedidasResolucionRedondeado();
		String medidaFormateada = formateador.formatearMedida(m);
		
		System.out.println("Medida formateada :" + medidaFormateada );
		
		Assert.assertEquals("formateado","123", medidaFormateada);
		
	}
	
}
