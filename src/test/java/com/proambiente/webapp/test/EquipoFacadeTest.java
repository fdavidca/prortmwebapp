package com.proambiente.webapp.test;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.proambiente.modelo.Equipo;
import com.proambiente.modelo.dto.EquipoDTO;
import com.proambiente.webapp.dao.AbstractFacade;
import com.proambiente.webapp.dao.EquipoFacade;

import org.junit.Assert;

@RunWith(Arquillian.class)
public class EquipoFacadeTest {
	
	@Inject
	EquipoFacade equipoFacade;
	
	@Deployment
	public static WebArchive createDeployment(){		
		
		File[] files = Maven.resolver().loadPomFromFile("pom.xml").resolve("joda-time:joda-time:2.3").withTransitivity().asFile();		
		return ShrinkWrap.create(WebArchive.class,"test.war").addAsWebInfResource("test-ds.xml")
				.addAsLibraries(files)
				.addClass(AbstractFacade.class).addPackage(Equipo.class.getPackage()).addClass(EquipoFacade.class).addClass(EquipoDTO.class)
				.addAsResource("META-INF/test-persistence.xml","META-INF/persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE,"beans.xml");
	}
	
	@Test
	public void testTipoEquipo(){
		try{
			Equipo equipo = equipoFacade.find("ASDFGQWERT"); 
			System.out.println("25j : " + equipo);
		}catch(Exception exc){
			exc.printStackTrace();
			Assert.fail("Error al consultar equipo");
		}
				
	}
	
	@Test
	public void testListaEquipo(){
		try{
			List<Equipo> listaEquipos = equipoFacade.consultarEquipoPorTipo(1);
			System.out.println("Equipos encontrados" + listaEquipos.size());
		}catch(Exception exc){
			exc.printStackTrace();
			Assert.fail("error consultando equipos por tipo");
		}
	}

}
