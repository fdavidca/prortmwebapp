package com.proambiente.webapp.test;

import java.io.File;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.proambiente.modelo.Alerta;
import com.proambiente.modelo.NivelAlerta;
import com.proambiente.modelo.dto.EquipoDTO;
import com.proambiente.webapp.dao.AbstractFacade;
import com.proambiente.webapp.dao.AlertaFacade;

@RunWith(Arquillian.class)
public class AlertaFacadeTest {
	
	@Inject
	AlertaFacade alertaFacade;
	
	@Deployment
	public static WebArchive createDeployment(){		
		
		File[] files = Maven.resolver().loadPomFromFile("pom.xml").resolve("joda-time:joda-time:2.3").withTransitivity().asFile();		
		return ShrinkWrap.create(WebArchive.class,"test.war").addAsWebInfResource("test-ds.xml")
				.addAsLibraries(files)
				.addClass(AbstractFacade.class).addPackage(Alerta.class.getPackage()).addClass(AlertaFacade.class).addClass(EquipoDTO.class)
				.addAsResource("META-INF/test-persistence.xml","META-INF/persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE,"beans.xml");
	}
	
	@Test
	public void testEncontrarAlerta(){
		try{
			Alerta alerta = alertaFacade.find(1); 
			System.out.println("25j : " + alerta);
		}catch(Exception exc){
			exc.printStackTrace();
			Assert.fail("Error consultar alerta por id");
		}
				
	}
	
	@Test
	public void testListarAlertas(){
		try{
			List<Alerta> listaAlerta = alertaFacade.findRange(new int[] {0,10});
			System.out.println("Equipos encontrados" + listaAlerta.size());
		}catch(Exception exc){
			exc.printStackTrace();
			Assert.fail("error consultando alerta");
		}
	}
	
	@Test
	public void testGuardarAlerta() {
		try {
			
			Alerta alerta = new Alerta();
			
			alerta.setDescripcion(" Alerta de prueba");
			alerta.setNivel(NivelAlerta.ERROR);
			alerta.setFecha(new Timestamp(new Date().getTime()));
			alerta.setReconocida(false);
			alertaFacade.edit(alerta);
			
		}catch(Exception exc) {
			exc.printStackTrace();
			Assert.fail("error guardando alerta");
		}
	}

}
