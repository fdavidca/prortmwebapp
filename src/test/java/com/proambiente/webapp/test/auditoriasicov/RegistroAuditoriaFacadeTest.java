package com.proambiente.webapp.test.auditoriasicov;

import java.io.File;
import java.util.Date;
import java.sql.Timestamp;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.proambiente.modelo.Equipo;
import com.proambiente.modelo.EventoPrueba;
import com.proambiente.modelo.RegistroAuditoria;
import com.proambiente.modelo.TipoOperacion;
import com.proambiente.webapp.dao.AbstractFacade;
import com.proambiente.webapp.dao.AuditoriaSicovFacade;

@RunWith(Arquillian.class)
public class RegistroAuditoriaFacadeTest {
	
	@Inject
	AuditoriaSicovFacade auditoriaSicovFacade;
	
	@Deployment
	public static WebArchive createDeployment(){		
		
		File[] files = Maven.resolver().loadPomFromFile("pom.xml").resolve("joda-time:joda-time:2.3").withTransitivity().asFile();		
		return ShrinkWrap.create(WebArchive.class,"test.war").addAsWebInfResource("test-ds.xml")
				.addAsLibraries(files)
				.addClass(AbstractFacade.class).addPackage(Equipo.class.getPackage()).addClass(AuditoriaSicovFacade.class)
				.addAsResource("META-INF/test-persistence.xml","META-INF/persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE,"beans.xml");
	}
	
	@Test
	public void testFindRegistro(){
		try{
			auditoriaSicovFacade.find(1);
			System.out.println("Ok ");
		}catch(Exception exc){
			exc.printStackTrace();
			Assert.fail("Error al consultar equipo");
		}
				
	}
	
	@Test
	public void testGuardarRegistro(){
		try{
			RegistroAuditoria registro = new RegistroAuditoria();
			registro.setIdRevision(11);
			registro.setSerialEquipoMedicion("123");
			registro.setIpEquipoMedicion("168.1.1.1");
			registro.setFechaRegistroBd( new Timestamp( new Date().getTime() ) );
			registro.setFechaEvento( new Timestamp(new Date().getTime() ) );
			registro.setTipoOperacion(TipoOperacion.INSERCION);
			registro.setEventoPrueba(EventoPrueba.VISUAL);
			registro.setCodigoProveedor(649);
			registro.setIdRuntCda(5);
			registro.setTrama("Este debe ser un json");
			registro.setUsuario("Fabian David Camargo Amaya");
			registro.setIdentificacionUsuario("4192845");
			registro.setObservacion("Esta es una observacion");
			auditoriaSicovFacade.create(registro);
		}catch(Exception exc){
			exc.printStackTrace();
			Assert.fail("error consultando equipos por tipo");
		}
	}

}
