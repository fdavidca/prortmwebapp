package com.proambiente.webapp.test;

import java.io.File;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.proambiente.modelo.Aseguradora;
import com.proambiente.modelo.EventoPrueba;
import com.proambiente.modelo.RegistroAuditoria;
import com.proambiente.modelo.TipoOperacion;
import com.proambiente.modelo.dto.EquipoDTO;
import com.proambiente.webapp.dao.AbstractFacade;
import com.proambiente.webapp.dao.AseguradoraFacade;
import com.proambiente.webapp.dao.AuditoriaSicovFacade;

@RunWith(Arquillian.class)
public class AuditoriaSicovFacadeTest {
	
	@Inject
	AuditoriaSicovFacade auditoriaSicovFacade;
	
	@Deployment
	public static WebArchive createDeployment(){		
		
		File[] files = Maven.resolver().loadPomFromFile("pom.xml").resolve("joda-time:joda-time:2.3").withTransitivity().asFile();		
		return ShrinkWrap.create(WebArchive.class,"test.war").addAsWebInfResource("test-ds.xml")
				.addAsLibraries(files)
				.addClass(AbstractFacade.class).addPackage(RegistroAuditoria.class.getPackage()).addClass(AuditoriaSicovFacade.class).addClass(EquipoDTO.class)
				.addAsResource("META-INF/test-persistence.xml","META-INF/persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE,"beans.xml");
	}
	
	@Test
	public void testEncontrarAseguradora(){
		try{
			RegistroAuditoria registroAuditoria = auditoriaSicovFacade.find(1); 
			System.out.println("19j : " + registroAuditoria);
		}catch(Exception exc){
			exc.printStackTrace();
			Assert.fail("Error consultar aseguradora por id");
		}
				
	}
	
	@Test
	public void testListarAseguradoras(){
		try{
			List<RegistroAuditoria> listaRegistroAuditoria = auditoriaSicovFacade.findRange( new int[]{1,25});
			System.out.println("Lista registro auditoria: " + listaRegistroAuditoria.size());
		}catch(Exception exc){
			exc.printStackTrace();
			Assert.fail("error consultando lista registro auditoria");
		}
	}
	
	@Test
	public void testGuardarRegistroAuditoria() {
		try {
			
			RegistroAuditoria registroAuditoria = new RegistroAuditoria();
			registroAuditoria.setCodigoProveedor(95);
			registroAuditoria.setEventoPrueba(EventoPrueba.ALINEACION);
			registroAuditoria.setFechaEvento(new Timestamp(new Date().getTime()));
			registroAuditoria.setFechaRegistroBd(new Timestamp(new Date().getTime()));
			registroAuditoria.setIdAuditoriaSicov(1);
			registroAuditoria.setIdentificacionUsuario("13");
			registroAuditoria.setIdRevision(10);
			registroAuditoria.setIdRuntCda(452365);
			registroAuditoria.setIpEquipoMedicion("3004");
			registroAuditoria.setObservacion("Registro de prueba");
			registroAuditoria.setPlaca("PRU111");
			registroAuditoria.setTipoOperacion(TipoOperacion.ACTUALIZACION);
			registroAuditoria.setTrama("-*69855");
			registroAuditoria.setUsuario("Jhon Doe");
			
			auditoriaSicovFacade.edit(registroAuditoria);
			
		}catch(Exception exc) {
			exc.printStackTrace();
			Assert.fail("error guardando Registro Auditoria");
		}
	}

}
