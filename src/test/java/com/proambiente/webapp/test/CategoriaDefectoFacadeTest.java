package com.proambiente.webapp.test;

import java.io.File;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.proambiente.modelo.CambioPassword;
import com.proambiente.modelo.CategoriaDefecto;
import com.proambiente.modelo.Usuario;
import com.proambiente.modelo.dto.EquipoDTO;
import com.proambiente.webapp.dao.AbstractFacade;
import com.proambiente.webapp.dao.CambioPasswordFacade;
import com.proambiente.webapp.dao.CategoriaDefectoFacade;
import com.proambiente.webapp.dao.UsuarioFacade;

@RunWith(Arquillian.class)
public class CategoriaDefectoFacadeTest {
	
	@Inject
	CategoriaDefectoFacade categoriaDefectoFacade;
	
	
	
	@Deployment
	public static WebArchive createDeployment(){		
		
		File[] files = Maven.resolver().loadPomFromFile("pom.xml").resolve("joda-time:joda-time:2.3").withTransitivity().asFile();		
		return ShrinkWrap.create(WebArchive.class,"test.war").addAsWebInfResource("test-ds.xml")
				.addAsLibraries(files)
				.addClass(AbstractFacade.class).addPackage(CambioPassword.class.getPackage()).addClass(CategoriaDefectoFacadeTest.class).addClass(EquipoDTO.class)
				.addAsResource("META-INF/test-persistence.xml","META-INF/persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE,"beans.xml");
	}
	
	@Test
	public void testEncontrarCategoriaDefecto(){
		try{
			CategoriaDefecto categoriaDefecto = categoriaDefectoFacade.find(1); 
			System.out.println("119j : " + categoriaDefecto);
		}catch(Exception exc){
			exc.printStackTrace();
			Assert.fail("Error consultar cambio password por id");
		}
				
	}
	
	@Test
	public void testListarCategoriasDefecto(){
		try{
			List<CategoriaDefecto> listaCategoriaDefectos = categoriaDefectoFacade.findRange(new int[] {0,10});
			System.out.println("Categoria defecto encontrados" + listaCategoriaDefectos.size());
		}catch(Exception exc){
			exc.printStackTrace();
			Assert.fail("error consultando cambio password");
		}
	}
	
	

}
