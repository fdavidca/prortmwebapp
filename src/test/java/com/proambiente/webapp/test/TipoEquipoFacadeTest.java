package com.proambiente.webapp.test;

import static org.junit.Assert.assertNotNull;

import java.io.File;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.proambiente.modelo.TipoEquipo;
import com.proambiente.webapp.dao.AbstractFacade;
import com.proambiente.webapp.dao.TipoEquipoFacade;

@RunWith(Arquillian.class)
public class TipoEquipoFacadeTest {
	
	@Inject
	TipoEquipoFacade tipoEquipoFacade;
	
	@Deployment
	public static WebArchive createDeployment(){		
		//Puede generar un out of memory exception debido a la dependencia circular
		File[] files = Maven.resolver().loadPomFromFile("pom.xml").resolve("joda-time:joda-time:2.3").withTransitivity().asFile();		
		return ShrinkWrap.create(WebArchive.class,"test.war").addAsWebInfResource("test-ds.xml")
				.addAsLibraries(files)
				.addClass(AbstractFacade.class).addPackage(TipoEquipo.class.getPackage()).addClass(TipoEquipoFacade.class)
				.addAsResource("META-INF/test-persistence.xml","META-INF/persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE,"beans.xml");
	}
	
	@Test
	public void testTipoEquipo(){
		TipoEquipo tipoEquipo = tipoEquipoFacade.find(1); 
		System.out.println("1a4 : " + tipoEquipo);
		assertNotNull(tipoEquipo);		
	}

}
