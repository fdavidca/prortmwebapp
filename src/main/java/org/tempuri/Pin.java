
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para pin complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="pin">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="usuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="clave" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_pin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_tipo_rtm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_placa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "pin", propOrder = {
    "usuario",
    "clave",
    "pPin",
    "pTipoRtm",
    "pPlaca"
})
public class Pin {

    protected String usuario;
    protected String clave;
    @XmlElement(name = "p_pin")
    protected String pPin;
    @XmlElement(name = "p_tipo_rtm")
    protected String pTipoRtm;
    @XmlElement(name = "p_placa")
    protected String pPlaca;

    /**
     * Obtiene el valor de la propiedad usuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * Define el valor de la propiedad usuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuario(String value) {
        this.usuario = value;
    }

    /**
     * Obtiene el valor de la propiedad clave.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClave() {
        return clave;
    }

    /**
     * Define el valor de la propiedad clave.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClave(String value) {
        this.clave = value;
    }

    /**
     * Obtiene el valor de la propiedad pPin.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPPin() {
        return pPin;
    }

    /**
     * Define el valor de la propiedad pPin.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPPin(String value) {
        this.pPin = value;
    }

    /**
     * Obtiene el valor de la propiedad pTipoRtm.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPTipoRtm() {
        return pTipoRtm;
    }

    /**
     * Define el valor de la propiedad pTipoRtm.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPTipoRtm(String value) {
        this.pTipoRtm = value;
    }

    /**
     * Obtiene el valor de la propiedad pPlaca.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPPlaca() {
        return pPlaca;
    }

    /**
     * Define el valor de la propiedad pPlaca.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPPlaca(String value) {
        this.pPlaca = value;
    }

}
