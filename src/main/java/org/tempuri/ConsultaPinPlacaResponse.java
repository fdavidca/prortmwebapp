
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="consulta_pin_placaResult" type="{http://tempuri.org/}resultado" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultaPinPlacaResult"
})
@XmlRootElement(name = "consulta_pin_placaResponse")
public class ConsultaPinPlacaResponse {

    @XmlElement(name = "consulta_pin_placaResult")
    protected Resultado consultaPinPlacaResult;

    /**
     * Obtiene el valor de la propiedad consultaPinPlacaResult.
     * 
     * @return
     *     possible object is
     *     {@link Resultado }
     *     
     */
    public Resultado getConsultaPinPlacaResult() {
        return consultaPinPlacaResult;
    }

    /**
     * Define el valor de la propiedad consultaPinPlacaResult.
     * 
     * @param value
     *     allowed object is
     *     {@link Resultado }
     *     
     */
    public void setConsultaPinPlacaResult(Resultado value) {
        this.consultaPinPlacaResult = value;
    }

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ConsultaPinPlacaResponse [consultaPinPlacaResult=");
		builder.append(consultaPinPlacaResult);
		builder.append("]");
		return builder.toString();
	}

    
}
