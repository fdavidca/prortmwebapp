
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ingresar_fur_v2Result" type="{http://tempuri.org/}resultado" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "ingresarFurV2Result"
})
@XmlRootElement(name = "ingresar_fur_v2Response")
public class IngresarFurV2Response {

    @XmlElement(name = "ingresar_fur_v2Result")
    protected Resultado ingresarFurV2Result;

    /**
     * Obtiene el valor de la propiedad ingresarFurV2Result.
     * 
     * @return
     *     possible object is
     *     {@link Resultado }
     *     
     */
    public Resultado getIngresarFurV2Result() {
        return ingresarFurV2Result;
    }

    /**
     * Define el valor de la propiedad ingresarFurV2Result.
     * 
     * @param value
     *     allowed object is
     *     {@link Resultado }
     *     
     */
    public void setIngresarFurV2Result(Resultado value) {
        this.ingresarFurV2Result = value;
    }

}
