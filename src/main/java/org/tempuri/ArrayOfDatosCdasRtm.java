
package org.tempuri;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ArrayOfDatosCdasRtm complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfDatosCdasRtm">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DatosCdasRtm" type="{http://tempuri.org/}DatosCdasRtm" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfDatosCdasRtm", propOrder = {
    "datosCdasRtm"
})
public class ArrayOfDatosCdasRtm {

    @XmlElement(name = "DatosCdasRtm", nillable = true)
    protected List<DatosCdasRtm> datosCdasRtm;

    /**
     * Gets the value of the datosCdasRtm property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the datosCdasRtm property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDatosCdasRtm().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DatosCdasRtm }
     * 
     * 
     */
    public List<DatosCdasRtm> getDatosCdasRtm() {
        if (datosCdasRtm == null) {
            datosCdasRtm = new ArrayList<DatosCdasRtm>();
        }
        return this.datosCdasRtm;
    }

}
