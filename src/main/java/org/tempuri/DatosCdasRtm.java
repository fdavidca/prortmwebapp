
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DatosCdasRtm complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DatosCdasRtm">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idCda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DatosCdasRtm", propOrder = {
    "idCda",
    "cda"
})
public class DatosCdasRtm {

    protected String idCda;
    protected String cda;

    /**
     * Obtiene el valor de la propiedad idCda.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdCda() {
        return idCda;
    }

    /**
     * Define el valor de la propiedad idCda.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdCda(String value) {
        this.idCda = value;
    }

    /**
     * Obtiene el valor de la propiedad cda.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCda() {
        return cda;
    }

    /**
     * Define el valor de la propiedad cda.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCda(String value) {
        this.cda = value;
    }

}
