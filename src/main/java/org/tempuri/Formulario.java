
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para formulario complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="formulario">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="usuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="clave" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_pin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_fur_aso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_cda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_nit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_dir" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_div" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_ciu" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_tel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_1_fec_pru" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_2_nom_raz" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_2_doc_tip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_2_doc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_2_dir" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_2_tel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_2_ciu" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_2_dep" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_3_plac" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_3_mar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_3_lin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_3_cla" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_3_mod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_3_cil" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_3_ser" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_3_vin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_3_mot" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_3_lic" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_3_com" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_3_col" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_3_nac" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_3_fec_lic" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_3_tip_mot" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_3_kil" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_3_sil" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_3_vid_pol" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_3_bli" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_4_rui_val" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_4_rui_max" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_r03" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_r04" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_5_der_int" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_5_der_min" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_5_der_inc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_5_der_ran" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_5_izq_int" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_5_izq_min" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_5_izq_inq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_5_izq_ran" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_6_int" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_6_max" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_7_del_der_val" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_7_del_izq_val" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_7_tra_der_val" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_7_tra_izq_val" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_7_min" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_8_efi_tot" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_8_efi_tot_min" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_8_ej1_izq_fue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_8_ej1_izq_pes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_8_ej1_der_fue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_8_ej1_der_pes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_8_ej1_des" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_8_ej1_max" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_8_ej2_izq_fue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_8_ej2_izq_pes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_8_ej2_der_fue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_8_ej2_der_pes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_8_ej2_des" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_8_ej2_max" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_8_efi_aux" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_8_efi_aux_min" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_8_ej3_izq_fue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_8_ej3_izq_pes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_8_ej3_der_fue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_8_ej3_der_pes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_8_ej3_des" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_8_ej3_max" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_8_ej4_izq_fue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_8_ej4_izq_pes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_8_ej4_der_fue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_8_ej4_der_pes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_8_ej4_des" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_8_ej4_max" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_8_ej5_izq_fue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_8_ej5_izq_pes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_8_ej5_der_fue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_8_ej5_der_pes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_8_ej5_des" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_8_ej5_max" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_9_ej1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_9_ej2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_9_ej3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_9_ej4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_9_ej5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_9_max" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_10_ref_com_lla" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_10_err_dis" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_10_err_tie" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_10_max" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_11_co_ral_val" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_11_co_ral_nor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_11_co2_ral_val" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_11_co2_ral_nor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_11_o2_ral_val" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_11_o2_ral_nor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_11_hc_ral_val" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_11_hc_ral_nor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_11_co_cru_val" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_11_co_cru_nor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_11_co2_cru_val" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_11_co2_cru_nor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_11_o2_cru_val" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_11_o2_cru_nor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_11_hc_cru_val" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_11_hc_cru_nor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_g17" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_11_tem_ral" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_11_rpm_ral" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_11_tem_cru" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_11_rpm_cru" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_11_no_ral_val" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_11_no_ral_nor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_11_no_cru_val" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_11_no_cru_nor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_o01" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_11_b_ci1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_11_b_ci2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_11_b_ci3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_11_b_ci4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_11_b_res_val" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_11_b_res_nor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_11_b_tem" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_11_b_rpm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_o10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_tw01" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_v01" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_v02" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_v03" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_v04" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_v05" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_v06" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_v07" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_v08" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_v09" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_v10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_v11" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_v12" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_c_cod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_c_des" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_c_gru" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_c_tip_def_a" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_c_tip_def_b" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_c_tip_def_a_tot" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_c_tip_def_b_tot" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_d_cod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_d_des" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_d_gru" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_d_tip_def_a" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_d_tip_def_b" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_d_tip_def_a_tot" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_d_tip_def_b_tot" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_d1_cod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_d1_des" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_d1_gru" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_d1_tip_def_a" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_d1_tip_def_b" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_d1_tip_def_a_tot" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_d1_tip_def_b_tot" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_e_con_run" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_e_apr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_e1_apr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_f_com_obs" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_h_nom_ope_rea_rev_tec" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_g_nom_fir_dir_tec" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_causa_rechazo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="p_foto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "formulario", propOrder = {
    "usuario",
    "clave",
    "pPin",
    "pFurAso",
    "pCda",
    "pNit",
    "pDir",
    "pDiv",
    "pCiu",
    "pTel",
    "p1FecPru",
    "p2NomRaz",
    "p2DocTip",
    "p2Doc",
    "p2Dir",
    "p2Tel",
    "p2Ciu",
    "p2Dep",
    "p3Plac",
    "p3Mar",
    "p3Lin",
    "p3Cla",
    "p3Mod",
    "p3Cil",
    "p3Ser",
    "p3Vin",
    "p3Mot",
    "p3Lic",
    "p3Com",
    "p3Col",
    "p3Nac",
    "p3FecLic",
    "p3TipMot",
    "p3Kil",
    "p3Sil",
    "p3VidPol",
    "p3Bli",
    "p4RuiVal",
    "p4RuiMax",
    "pr03",
    "pr04",
    "p5DerInt",
    "p5DerMin",
    "p5DerInc",
    "p5DerRan",
    "p5IzqInt",
    "p5IzqMin",
    "p5IzqInq",
    "p5IzqRan",
    "p6Int",
    "p6Max",
    "p7DelDerVal",
    "p7DelIzqVal",
    "p7TraDerVal",
    "p7TraIzqVal",
    "p7Min",
    "p8EfiTot",
    "p8EfiTotMin",
    "p8Ej1IzqFue",
    "p8Ej1IzqPes",
    "p8Ej1DerFue",
    "p8Ej1DerPes",
    "p8Ej1Des",
    "p8Ej1Max",
    "p8Ej2IzqFue",
    "p8Ej2IzqPes",
    "p8Ej2DerFue",
    "p8Ej2DerPes",
    "p8Ej2Des",
    "p8Ej2Max",
    "p8EfiAux",
    "p8EfiAuxMin",
    "p8Ej3IzqFue",
    "p8Ej3IzqPes",
    "p8Ej3DerFue",
    "p8Ej3DerPes",
    "p8Ej3Des",
    "p8Ej3Max",
    "p8Ej4IzqFue",
    "p8Ej4IzqPes",
    "p8Ej4DerFue",
    "p8Ej4DerPes",
    "p8Ej4Des",
    "p8Ej4Max",
    "p8Ej5IzqFue",
    "p8Ej5IzqPes",
    "p8Ej5DerFue",
    "p8Ej5DerPes",
    "p8Ej5Des",
    "p8Ej5Max",
    "p9Ej1",
    "p9Ej2",
    "p9Ej3",
    "p9Ej4",
    "p9Ej5",
    "p9Max",
    "p10RefComLla",
    "p10ErrDis",
    "p10ErrTie",
    "p10Max",
    "p11CoRalVal",
    "p11CoRalNor",
    "p11Co2RalVal",
    "p11Co2RalNor",
    "p11O2RalVal",
    "p11O2RalNor",
    "p11HcRalVal",
    "p11HcRalNor",
    "p11CoCruVal",
    "p11CoCruNor",
    "p11Co2CruVal",
    "p11Co2CruNor",
    "p11O2CruVal",
    "p11O2CruNor",
    "p11HcCruVal",
    "p11HcCruNor",
    "pg17",
    "p11TemRal",
    "p11RpmRal",
    "p11TemCru",
    "p11RpmCru",
    "p11NoRalVal",
    "p11NoRalNor",
    "p11NoCruVal",
    "p11NoCruNor",
    "po01",
    "p11BCi1",
    "p11BCi2",
    "p11BCi3",
    "p11BCi4",
    "p11BResVal",
    "p11BResNor",
    "p11BTem",
    "p11BRpm",
    "po10",
    "pTw01",
    "pv01",
    "pv02",
    "pv03",
    "pv04",
    "pv05",
    "pv06",
    "pv07",
    "pv08",
    "pv09",
    "pv10",
    "pv11",
    "pv12",
    "pcCod",
    "pcDes",
    "pcGru",
    "pcTipDefA",
    "pcTipDefB",
    "pcTipDefATot",
    "pcTipDefBTot",
    "pdCod",
    "pdDes",
    "pdGru",
    "pdTipDefA",
    "pdTipDefB",
    "pdTipDefATot",
    "pdTipDefBTot",
    "pd1Cod",
    "pd1Des",
    "pd1Gru",
    "pd1TipDefA",
    "pd1TipDefB",
    "pd1TipDefATot",
    "pd1TipDefBTot",
    "peConRun",
    "peApr",
    "pe1Apr",
    "pfComObs",
    "phNomOpeReaRevTec",
    "pgNomFirDirTec",
    "pCausaRechazo",
    "pFoto"
})
public class Formulario {

    protected String usuario;
    protected String clave;
    @XmlElement(name = "p_pin")
    protected String pPin;
    @XmlElement(name = "p_fur_aso")
    protected String pFurAso;
    @XmlElement(name = "p_cda")
    protected String pCda;
    @XmlElement(name = "p_nit")
    protected String pNit;
    @XmlElement(name = "p_dir")
    protected String pDir;
    @XmlElement(name = "p_div")
    protected String pDiv;
    @XmlElement(name = "p_ciu")
    protected String pCiu;
    @XmlElement(name = "p_tel")
    protected String pTel;
    @XmlElement(name = "p_1_fec_pru")
    protected String p1FecPru;
    @XmlElement(name = "p_2_nom_raz")
    protected String p2NomRaz;
    @XmlElement(name = "p_2_doc_tip")
    protected String p2DocTip;
    @XmlElement(name = "p_2_doc")
    protected String p2Doc;
    @XmlElement(name = "p_2_dir")
    protected String p2Dir;
    @XmlElement(name = "p_2_tel")
    protected String p2Tel;
    @XmlElement(name = "p_2_ciu")
    protected String p2Ciu;
    @XmlElement(name = "p_2_dep")
    protected String p2Dep;
    @XmlElement(name = "p_3_plac")
    protected String p3Plac;
    @XmlElement(name = "p_3_mar")
    protected String p3Mar;
    @XmlElement(name = "p_3_lin")
    protected String p3Lin;
    @XmlElement(name = "p_3_cla")
    protected String p3Cla;
    @XmlElement(name = "p_3_mod")
    protected String p3Mod;
    @XmlElement(name = "p_3_cil")
    protected String p3Cil;
    @XmlElement(name = "p_3_ser")
    protected String p3Ser;
    @XmlElement(name = "p_3_vin")
    protected String p3Vin;
    @XmlElement(name = "p_3_mot")
    protected String p3Mot;
    @XmlElement(name = "p_3_lic")
    protected String p3Lic;
    @XmlElement(name = "p_3_com")
    protected String p3Com;
    @XmlElement(name = "p_3_col")
    protected String p3Col;
    @XmlElement(name = "p_3_nac")
    protected String p3Nac;
    @XmlElement(name = "p_3_fec_lic")
    protected String p3FecLic;
    @XmlElement(name = "p_3_tip_mot")
    protected String p3TipMot;
    @XmlElement(name = "p_3_kil")
    protected String p3Kil;
    @XmlElement(name = "p_3_sil")
    protected String p3Sil;
    @XmlElement(name = "p_3_vid_pol")
    protected String p3VidPol;
    @XmlElement(name = "p_3_bli")
    protected String p3Bli;
    @XmlElement(name = "p_4_rui_val")
    protected String p4RuiVal;
    @XmlElement(name = "p_4_rui_max")
    protected String p4RuiMax;
    @XmlElement(name = "p_r03")
    protected String pr03;
    @XmlElement(name = "p_r04")
    protected String pr04;
    @XmlElement(name = "p_5_der_int")
    protected String p5DerInt;
    @XmlElement(name = "p_5_der_min")
    protected String p5DerMin;
    @XmlElement(name = "p_5_der_inc")
    protected String p5DerInc;
    @XmlElement(name = "p_5_der_ran")
    protected String p5DerRan;
    @XmlElement(name = "p_5_izq_int")
    protected String p5IzqInt;
    @XmlElement(name = "p_5_izq_min")
    protected String p5IzqMin;
    @XmlElement(name = "p_5_izq_inq")
    protected String p5IzqInq;
    @XmlElement(name = "p_5_izq_ran")
    protected String p5IzqRan;
    @XmlElement(name = "p_6_int")
    protected String p6Int;
    @XmlElement(name = "p_6_max")
    protected String p6Max;
    @XmlElement(name = "p_7_del_der_val")
    protected String p7DelDerVal;
    @XmlElement(name = "p_7_del_izq_val")
    protected String p7DelIzqVal;
    @XmlElement(name = "p_7_tra_der_val")
    protected String p7TraDerVal;
    @XmlElement(name = "p_7_tra_izq_val")
    protected String p7TraIzqVal;
    @XmlElement(name = "p_7_min")
    protected String p7Min;
    @XmlElement(name = "p_8_efi_tot")
    protected String p8EfiTot;
    @XmlElement(name = "p_8_efi_tot_min")
    protected String p8EfiTotMin;
    @XmlElement(name = "p_8_ej1_izq_fue")
    protected String p8Ej1IzqFue;
    @XmlElement(name = "p_8_ej1_izq_pes")
    protected String p8Ej1IzqPes;
    @XmlElement(name = "p_8_ej1_der_fue")
    protected String p8Ej1DerFue;
    @XmlElement(name = "p_8_ej1_der_pes")
    protected String p8Ej1DerPes;
    @XmlElement(name = "p_8_ej1_des")
    protected String p8Ej1Des;
    @XmlElement(name = "p_8_ej1_max")
    protected String p8Ej1Max;
    @XmlElement(name = "p_8_ej2_izq_fue")
    protected String p8Ej2IzqFue;
    @XmlElement(name = "p_8_ej2_izq_pes")
    protected String p8Ej2IzqPes;
    @XmlElement(name = "p_8_ej2_der_fue")
    protected String p8Ej2DerFue;
    @XmlElement(name = "p_8_ej2_der_pes")
    protected String p8Ej2DerPes;
    @XmlElement(name = "p_8_ej2_des")
    protected String p8Ej2Des;
    @XmlElement(name = "p_8_ej2_max")
    protected String p8Ej2Max;
    @XmlElement(name = "p_8_efi_aux")
    protected String p8EfiAux;
    @XmlElement(name = "p_8_efi_aux_min")
    protected String p8EfiAuxMin;
    @XmlElement(name = "p_8_ej3_izq_fue")
    protected String p8Ej3IzqFue;
    @XmlElement(name = "p_8_ej3_izq_pes")
    protected String p8Ej3IzqPes;
    @XmlElement(name = "p_8_ej3_der_fue")
    protected String p8Ej3DerFue;
    @XmlElement(name = "p_8_ej3_der_pes")
    protected String p8Ej3DerPes;
    @XmlElement(name = "p_8_ej3_des")
    protected String p8Ej3Des;
    @XmlElement(name = "p_8_ej3_max")
    protected String p8Ej3Max;
    @XmlElement(name = "p_8_ej4_izq_fue")
    protected String p8Ej4IzqFue;
    @XmlElement(name = "p_8_ej4_izq_pes")
    protected String p8Ej4IzqPes;
    @XmlElement(name = "p_8_ej4_der_fue")
    protected String p8Ej4DerFue;
    @XmlElement(name = "p_8_ej4_der_pes")
    protected String p8Ej4DerPes;
    @XmlElement(name = "p_8_ej4_des")
    protected String p8Ej4Des;
    @XmlElement(name = "p_8_ej4_max")
    protected String p8Ej4Max;
    @XmlElement(name = "p_8_ej5_izq_fue")
    protected String p8Ej5IzqFue;
    @XmlElement(name = "p_8_ej5_izq_pes")
    protected String p8Ej5IzqPes;
    @XmlElement(name = "p_8_ej5_der_fue")
    protected String p8Ej5DerFue;
    @XmlElement(name = "p_8_ej5_der_pes")
    protected String p8Ej5DerPes;
    @XmlElement(name = "p_8_ej5_des")
    protected String p8Ej5Des;
    @XmlElement(name = "p_8_ej5_max")
    protected String p8Ej5Max;
    @XmlElement(name = "p_9_ej1")
    protected String p9Ej1;
    @XmlElement(name = "p_9_ej2")
    protected String p9Ej2;
    @XmlElement(name = "p_9_ej3")
    protected String p9Ej3;
    @XmlElement(name = "p_9_ej4")
    protected String p9Ej4;
    @XmlElement(name = "p_9_ej5")
    protected String p9Ej5;
    @XmlElement(name = "p_9_max")
    protected String p9Max;
    @XmlElement(name = "p_10_ref_com_lla")
    protected String p10RefComLla;
    @XmlElement(name = "p_10_err_dis")
    protected String p10ErrDis;
    @XmlElement(name = "p_10_err_tie")
    protected String p10ErrTie;
    @XmlElement(name = "p_10_max")
    protected String p10Max;
    @XmlElement(name = "p_11_co_ral_val")
    protected String p11CoRalVal;
    @XmlElement(name = "p_11_co_ral_nor")
    protected String p11CoRalNor;
    @XmlElement(name = "p_11_co2_ral_val")
    protected String p11Co2RalVal;
    @XmlElement(name = "p_11_co2_ral_nor")
    protected String p11Co2RalNor;
    @XmlElement(name = "p_11_o2_ral_val")
    protected String p11O2RalVal;
    @XmlElement(name = "p_11_o2_ral_nor")
    protected String p11O2RalNor;
    @XmlElement(name = "p_11_hc_ral_val")
    protected String p11HcRalVal;
    @XmlElement(name = "p_11_hc_ral_nor")
    protected String p11HcRalNor;
    @XmlElement(name = "p_11_co_cru_val")
    protected String p11CoCruVal;
    @XmlElement(name = "p_11_co_cru_nor")
    protected String p11CoCruNor;
    @XmlElement(name = "p_11_co2_cru_val")
    protected String p11Co2CruVal;
    @XmlElement(name = "p_11_co2_cru_nor")
    protected String p11Co2CruNor;
    @XmlElement(name = "p_11_o2_cru_val")
    protected String p11O2CruVal;
    @XmlElement(name = "p_11_o2_cru_nor")
    protected String p11O2CruNor;
    @XmlElement(name = "p_11_hc_cru_val")
    protected String p11HcCruVal;
    @XmlElement(name = "p_11_hc_cru_nor")
    protected String p11HcCruNor;
    @XmlElement(name = "p_g17")
    protected String pg17;
    @XmlElement(name = "p_11_tem_ral")
    protected String p11TemRal;
    @XmlElement(name = "p_11_rpm_ral")
    protected String p11RpmRal;
    @XmlElement(name = "p_11_tem_cru")
    protected String p11TemCru;
    @XmlElement(name = "p_11_rpm_cru")
    protected String p11RpmCru;
    @XmlElement(name = "p_11_no_ral_val")
    protected String p11NoRalVal;
    @XmlElement(name = "p_11_no_ral_nor")
    protected String p11NoRalNor;
    @XmlElement(name = "p_11_no_cru_val")
    protected String p11NoCruVal;
    @XmlElement(name = "p_11_no_cru_nor")
    protected String p11NoCruNor;
    @XmlElement(name = "p_o01")
    protected String po01;
    @XmlElement(name = "p_11_b_ci1")
    protected String p11BCi1;
    @XmlElement(name = "p_11_b_ci2")
    protected String p11BCi2;
    @XmlElement(name = "p_11_b_ci3")
    protected String p11BCi3;
    @XmlElement(name = "p_11_b_ci4")
    protected String p11BCi4;
    @XmlElement(name = "p_11_b_res_val")
    protected String p11BResVal;
    @XmlElement(name = "p_11_b_res_nor")
    protected String p11BResNor;
    @XmlElement(name = "p_11_b_tem")
    protected String p11BTem;
    @XmlElement(name = "p_11_b_rpm")
    protected String p11BRpm;
    @XmlElement(name = "p_o10")
    protected String po10;
    @XmlElement(name = "p_tw01")
    protected String pTw01;
    @XmlElement(name = "p_v01")
    protected String pv01;
    @XmlElement(name = "p_v02")
    protected String pv02;
    @XmlElement(name = "p_v03")
    protected String pv03;
    @XmlElement(name = "p_v04")
    protected String pv04;
    @XmlElement(name = "p_v05")
    protected String pv05;
    @XmlElement(name = "p_v06")
    protected String pv06;
    @XmlElement(name = "p_v07")
    protected String pv07;
    @XmlElement(name = "p_v08")
    protected String pv08;
    @XmlElement(name = "p_v09")
    protected String pv09;
    @XmlElement(name = "p_v10")
    protected String pv10;
    @XmlElement(name = "p_v11")
    protected String pv11;
    @XmlElement(name = "p_v12")
    protected String pv12;
    @XmlElement(name = "p_c_cod")
    protected String pcCod;
    @XmlElement(name = "p_c_des")
    protected String pcDes;
    @XmlElement(name = "p_c_gru")
    protected String pcGru;
    @XmlElement(name = "p_c_tip_def_a")
    protected String pcTipDefA;
    @XmlElement(name = "p_c_tip_def_b")
    protected String pcTipDefB;
    @XmlElement(name = "p_c_tip_def_a_tot")
    protected String pcTipDefATot;
    @XmlElement(name = "p_c_tip_def_b_tot")
    protected String pcTipDefBTot;
    @XmlElement(name = "p_d_cod")
    protected String pdCod;
    @XmlElement(name = "p_d_des")
    protected String pdDes;
    @XmlElement(name = "p_d_gru")
    protected String pdGru;
    @XmlElement(name = "p_d_tip_def_a")
    protected String pdTipDefA;
    @XmlElement(name = "p_d_tip_def_b")
    protected String pdTipDefB;
    @XmlElement(name = "p_d_tip_def_a_tot")
    protected String pdTipDefATot;
    @XmlElement(name = "p_d_tip_def_b_tot")
    protected String pdTipDefBTot;
    @XmlElement(name = "p_d1_cod")
    protected String pd1Cod;
    @XmlElement(name = "p_d1_des")
    protected String pd1Des;
    @XmlElement(name = "p_d1_gru")
    protected String pd1Gru;
    @XmlElement(name = "p_d1_tip_def_a")
    protected String pd1TipDefA;
    @XmlElement(name = "p_d1_tip_def_b")
    protected String pd1TipDefB;
    @XmlElement(name = "p_d1_tip_def_a_tot")
    protected String pd1TipDefATot;
    @XmlElement(name = "p_d1_tip_def_b_tot")
    protected String pd1TipDefBTot;
    @XmlElement(name = "p_e_con_run")
    protected String peConRun;
    @XmlElement(name = "p_e_apr")
    protected String peApr;
    @XmlElement(name = "p_e1_apr")
    protected String pe1Apr;
    @XmlElement(name = "p_f_com_obs")
    protected String pfComObs;
    @XmlElement(name = "p_h_nom_ope_rea_rev_tec")
    protected String phNomOpeReaRevTec;
    @XmlElement(name = "p_g_nom_fir_dir_tec")
    protected String pgNomFirDirTec;
    @XmlElement(name = "p_causa_rechazo")
    protected String pCausaRechazo;
    @XmlElement(name = "p_foto")
    protected String pFoto;

    /**
     * Obtiene el valor de la propiedad usuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * Define el valor de la propiedad usuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuario(String value) {
        this.usuario = value;
    }

    /**
     * Obtiene el valor de la propiedad clave.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClave() {
        return clave;
    }

    /**
     * Define el valor de la propiedad clave.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClave(String value) {
        this.clave = value;
    }

    /**
     * Obtiene el valor de la propiedad pPin.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPPin() {
        return pPin;
    }

    /**
     * Define el valor de la propiedad pPin.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPPin(String value) {
        this.pPin = value;
    }

    /**
     * Obtiene el valor de la propiedad pFurAso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPFurAso() {
        return pFurAso;
    }

    /**
     * Define el valor de la propiedad pFurAso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPFurAso(String value) {
        this.pFurAso = value;
    }

    /**
     * Obtiene el valor de la propiedad pCda.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPCda() {
        return pCda;
    }

    /**
     * Define el valor de la propiedad pCda.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPCda(String value) {
        this.pCda = value;
    }

    /**
     * Obtiene el valor de la propiedad pNit.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPNit() {
        return pNit;
    }

    /**
     * Define el valor de la propiedad pNit.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPNit(String value) {
        this.pNit = value;
    }

    /**
     * Obtiene el valor de la propiedad pDir.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPDir() {
        return pDir;
    }

    /**
     * Define el valor de la propiedad pDir.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPDir(String value) {
        this.pDir = value;
    }

    /**
     * Obtiene el valor de la propiedad pDiv.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPDiv() {
        return pDiv;
    }

    /**
     * Define el valor de la propiedad pDiv.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPDiv(String value) {
        this.pDiv = value;
    }

    /**
     * Obtiene el valor de la propiedad pCiu.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPCiu() {
        return pCiu;
    }

    /**
     * Define el valor de la propiedad pCiu.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPCiu(String value) {
        this.pCiu = value;
    }

    /**
     * Obtiene el valor de la propiedad pTel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPTel() {
        return pTel;
    }

    /**
     * Define el valor de la propiedad pTel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPTel(String value) {
        this.pTel = value;
    }

    /**
     * Obtiene el valor de la propiedad p1FecPru.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP1FecPru() {
        return p1FecPru;
    }

    /**
     * Define el valor de la propiedad p1FecPru.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP1FecPru(String value) {
        this.p1FecPru = value;
    }

    /**
     * Obtiene el valor de la propiedad p2NomRaz.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP2NomRaz() {
        return p2NomRaz;
    }

    /**
     * Define el valor de la propiedad p2NomRaz.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP2NomRaz(String value) {
        this.p2NomRaz = value;
    }

    /**
     * Obtiene el valor de la propiedad p2DocTip.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP2DocTip() {
        return p2DocTip;
    }

    /**
     * Define el valor de la propiedad p2DocTip.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP2DocTip(String value) {
        this.p2DocTip = value;
    }

    /**
     * Obtiene el valor de la propiedad p2Doc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP2Doc() {
        return p2Doc;
    }

    /**
     * Define el valor de la propiedad p2Doc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP2Doc(String value) {
        this.p2Doc = value;
    }

    /**
     * Obtiene el valor de la propiedad p2Dir.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP2Dir() {
        return p2Dir;
    }

    /**
     * Define el valor de la propiedad p2Dir.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP2Dir(String value) {
        this.p2Dir = value;
    }

    /**
     * Obtiene el valor de la propiedad p2Tel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP2Tel() {
        return p2Tel;
    }

    /**
     * Define el valor de la propiedad p2Tel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP2Tel(String value) {
        this.p2Tel = value;
    }

    /**
     * Obtiene el valor de la propiedad p2Ciu.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP2Ciu() {
        return p2Ciu;
    }

    /**
     * Define el valor de la propiedad p2Ciu.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP2Ciu(String value) {
        this.p2Ciu = value;
    }

    /**
     * Obtiene el valor de la propiedad p2Dep.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP2Dep() {
        return p2Dep;
    }

    /**
     * Define el valor de la propiedad p2Dep.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP2Dep(String value) {
        this.p2Dep = value;
    }

    /**
     * Obtiene el valor de la propiedad p3Plac.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP3Plac() {
        return p3Plac;
    }

    /**
     * Define el valor de la propiedad p3Plac.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP3Plac(String value) {
        this.p3Plac = value;
    }

    /**
     * Obtiene el valor de la propiedad p3Mar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP3Mar() {
        return p3Mar;
    }

    /**
     * Define el valor de la propiedad p3Mar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP3Mar(String value) {
        this.p3Mar = value;
    }

    /**
     * Obtiene el valor de la propiedad p3Lin.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP3Lin() {
        return p3Lin;
    }

    /**
     * Define el valor de la propiedad p3Lin.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP3Lin(String value) {
        this.p3Lin = value;
    }

    /**
     * Obtiene el valor de la propiedad p3Cla.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP3Cla() {
        return p3Cla;
    }

    /**
     * Define el valor de la propiedad p3Cla.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP3Cla(String value) {
        this.p3Cla = value;
    }

    /**
     * Obtiene el valor de la propiedad p3Mod.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP3Mod() {
        return p3Mod;
    }

    /**
     * Define el valor de la propiedad p3Mod.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP3Mod(String value) {
        this.p3Mod = value;
    }

    /**
     * Obtiene el valor de la propiedad p3Cil.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP3Cil() {
        return p3Cil;
    }

    /**
     * Define el valor de la propiedad p3Cil.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP3Cil(String value) {
        this.p3Cil = value;
    }

    /**
     * Obtiene el valor de la propiedad p3Ser.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP3Ser() {
        return p3Ser;
    }

    /**
     * Define el valor de la propiedad p3Ser.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP3Ser(String value) {
        this.p3Ser = value;
    }

    /**
     * Obtiene el valor de la propiedad p3Vin.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP3Vin() {
        return p3Vin;
    }

    /**
     * Define el valor de la propiedad p3Vin.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP3Vin(String value) {
        this.p3Vin = value;
    }

    /**
     * Obtiene el valor de la propiedad p3Mot.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP3Mot() {
        return p3Mot;
    }

    /**
     * Define el valor de la propiedad p3Mot.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP3Mot(String value) {
        this.p3Mot = value;
    }

    /**
     * Obtiene el valor de la propiedad p3Lic.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP3Lic() {
        return p3Lic;
    }

    /**
     * Define el valor de la propiedad p3Lic.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP3Lic(String value) {
        this.p3Lic = value;
    }

    /**
     * Obtiene el valor de la propiedad p3Com.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP3Com() {
        return p3Com;
    }

    /**
     * Define el valor de la propiedad p3Com.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP3Com(String value) {
        this.p3Com = value;
    }

    /**
     * Obtiene el valor de la propiedad p3Col.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP3Col() {
        return p3Col;
    }

    /**
     * Define el valor de la propiedad p3Col.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP3Col(String value) {
        this.p3Col = value;
    }

    /**
     * Obtiene el valor de la propiedad p3Nac.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP3Nac() {
        return p3Nac;
    }

    /**
     * Define el valor de la propiedad p3Nac.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP3Nac(String value) {
        this.p3Nac = value;
    }

    /**
     * Obtiene el valor de la propiedad p3FecLic.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP3FecLic() {
        return p3FecLic;
    }

    /**
     * Define el valor de la propiedad p3FecLic.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP3FecLic(String value) {
        this.p3FecLic = value;
    }

    /**
     * Obtiene el valor de la propiedad p3TipMot.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP3TipMot() {
        return p3TipMot;
    }

    /**
     * Define el valor de la propiedad p3TipMot.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP3TipMot(String value) {
        this.p3TipMot = value;
    }

    /**
     * Obtiene el valor de la propiedad p3Kil.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP3Kil() {
        return p3Kil;
    }

    /**
     * Define el valor de la propiedad p3Kil.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP3Kil(String value) {
        this.p3Kil = value;
    }

    /**
     * Obtiene el valor de la propiedad p3Sil.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP3Sil() {
        return p3Sil;
    }

    /**
     * Define el valor de la propiedad p3Sil.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP3Sil(String value) {
        this.p3Sil = value;
    }

    /**
     * Obtiene el valor de la propiedad p3VidPol.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP3VidPol() {
        return p3VidPol;
    }

    /**
     * Define el valor de la propiedad p3VidPol.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP3VidPol(String value) {
        this.p3VidPol = value;
    }

    /**
     * Obtiene el valor de la propiedad p3Bli.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP3Bli() {
        return p3Bli;
    }

    /**
     * Define el valor de la propiedad p3Bli.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP3Bli(String value) {
        this.p3Bli = value;
    }

    /**
     * Obtiene el valor de la propiedad p4RuiVal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP4RuiVal() {
        return p4RuiVal;
    }

    /**
     * Define el valor de la propiedad p4RuiVal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP4RuiVal(String value) {
        this.p4RuiVal = value;
    }

    /**
     * Obtiene el valor de la propiedad p4RuiMax.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP4RuiMax() {
        return p4RuiMax;
    }

    /**
     * Define el valor de la propiedad p4RuiMax.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP4RuiMax(String value) {
        this.p4RuiMax = value;
    }

    /**
     * Obtiene el valor de la propiedad pr03.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPR03() {
        return pr03;
    }

    /**
     * Define el valor de la propiedad pr03.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPR03(String value) {
        this.pr03 = value;
    }

    /**
     * Obtiene el valor de la propiedad pr04.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPR04() {
        return pr04;
    }

    /**
     * Define el valor de la propiedad pr04.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPR04(String value) {
        this.pr04 = value;
    }

    /**
     * Obtiene el valor de la propiedad p5DerInt.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP5DerInt() {
        return p5DerInt;
    }

    /**
     * Define el valor de la propiedad p5DerInt.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP5DerInt(String value) {
        this.p5DerInt = value;
    }

    /**
     * Obtiene el valor de la propiedad p5DerMin.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP5DerMin() {
        return p5DerMin;
    }

    /**
     * Define el valor de la propiedad p5DerMin.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP5DerMin(String value) {
        this.p5DerMin = value;
    }

    /**
     * Obtiene el valor de la propiedad p5DerInc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP5DerInc() {
        return p5DerInc;
    }

    /**
     * Define el valor de la propiedad p5DerInc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP5DerInc(String value) {
        this.p5DerInc = value;
    }

    /**
     * Obtiene el valor de la propiedad p5DerRan.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP5DerRan() {
        return p5DerRan;
    }

    /**
     * Define el valor de la propiedad p5DerRan.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP5DerRan(String value) {
        this.p5DerRan = value;
    }

    /**
     * Obtiene el valor de la propiedad p5IzqInt.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP5IzqInt() {
        return p5IzqInt;
    }

    /**
     * Define el valor de la propiedad p5IzqInt.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP5IzqInt(String value) {
        this.p5IzqInt = value;
    }

    /**
     * Obtiene el valor de la propiedad p5IzqMin.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP5IzqMin() {
        return p5IzqMin;
    }

    /**
     * Define el valor de la propiedad p5IzqMin.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP5IzqMin(String value) {
        this.p5IzqMin = value;
    }

    /**
     * Obtiene el valor de la propiedad p5IzqInq.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP5IzqInq() {
        return p5IzqInq;
    }

    /**
     * Define el valor de la propiedad p5IzqInq.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP5IzqInq(String value) {
        this.p5IzqInq = value;
    }

    /**
     * Obtiene el valor de la propiedad p5IzqRan.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP5IzqRan() {
        return p5IzqRan;
    }

    /**
     * Define el valor de la propiedad p5IzqRan.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP5IzqRan(String value) {
        this.p5IzqRan = value;
    }

    /**
     * Obtiene el valor de la propiedad p6Int.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP6Int() {
        return p6Int;
    }

    /**
     * Define el valor de la propiedad p6Int.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP6Int(String value) {
        this.p6Int = value;
    }

    /**
     * Obtiene el valor de la propiedad p6Max.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP6Max() {
        return p6Max;
    }

    /**
     * Define el valor de la propiedad p6Max.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP6Max(String value) {
        this.p6Max = value;
    }

    /**
     * Obtiene el valor de la propiedad p7DelDerVal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP7DelDerVal() {
        return p7DelDerVal;
    }

    /**
     * Define el valor de la propiedad p7DelDerVal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP7DelDerVal(String value) {
        this.p7DelDerVal = value;
    }

    /**
     * Obtiene el valor de la propiedad p7DelIzqVal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP7DelIzqVal() {
        return p7DelIzqVal;
    }

    /**
     * Define el valor de la propiedad p7DelIzqVal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP7DelIzqVal(String value) {
        this.p7DelIzqVal = value;
    }

    /**
     * Obtiene el valor de la propiedad p7TraDerVal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP7TraDerVal() {
        return p7TraDerVal;
    }

    /**
     * Define el valor de la propiedad p7TraDerVal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP7TraDerVal(String value) {
        this.p7TraDerVal = value;
    }

    /**
     * Obtiene el valor de la propiedad p7TraIzqVal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP7TraIzqVal() {
        return p7TraIzqVal;
    }

    /**
     * Define el valor de la propiedad p7TraIzqVal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP7TraIzqVal(String value) {
        this.p7TraIzqVal = value;
    }

    /**
     * Obtiene el valor de la propiedad p7Min.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP7Min() {
        return p7Min;
    }

    /**
     * Define el valor de la propiedad p7Min.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP7Min(String value) {
        this.p7Min = value;
    }

    /**
     * Obtiene el valor de la propiedad p8EfiTot.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP8EfiTot() {
        return p8EfiTot;
    }

    /**
     * Define el valor de la propiedad p8EfiTot.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP8EfiTot(String value) {
        this.p8EfiTot = value;
    }

    /**
     * Obtiene el valor de la propiedad p8EfiTotMin.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP8EfiTotMin() {
        return p8EfiTotMin;
    }

    /**
     * Define el valor de la propiedad p8EfiTotMin.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP8EfiTotMin(String value) {
        this.p8EfiTotMin = value;
    }

    /**
     * Obtiene el valor de la propiedad p8Ej1IzqFue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP8Ej1IzqFue() {
        return p8Ej1IzqFue;
    }

    /**
     * Define el valor de la propiedad p8Ej1IzqFue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP8Ej1IzqFue(String value) {
        this.p8Ej1IzqFue = value;
    }

    /**
     * Obtiene el valor de la propiedad p8Ej1IzqPes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP8Ej1IzqPes() {
        return p8Ej1IzqPes;
    }

    /**
     * Define el valor de la propiedad p8Ej1IzqPes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP8Ej1IzqPes(String value) {
        this.p8Ej1IzqPes = value;
    }

    /**
     * Obtiene el valor de la propiedad p8Ej1DerFue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP8Ej1DerFue() {
        return p8Ej1DerFue;
    }

    /**
     * Define el valor de la propiedad p8Ej1DerFue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP8Ej1DerFue(String value) {
        this.p8Ej1DerFue = value;
    }

    /**
     * Obtiene el valor de la propiedad p8Ej1DerPes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP8Ej1DerPes() {
        return p8Ej1DerPes;
    }

    /**
     * Define el valor de la propiedad p8Ej1DerPes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP8Ej1DerPes(String value) {
        this.p8Ej1DerPes = value;
    }

    /**
     * Obtiene el valor de la propiedad p8Ej1Des.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP8Ej1Des() {
        return p8Ej1Des;
    }

    /**
     * Define el valor de la propiedad p8Ej1Des.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP8Ej1Des(String value) {
        this.p8Ej1Des = value;
    }

    /**
     * Obtiene el valor de la propiedad p8Ej1Max.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP8Ej1Max() {
        return p8Ej1Max;
    }

    /**
     * Define el valor de la propiedad p8Ej1Max.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP8Ej1Max(String value) {
        this.p8Ej1Max = value;
    }

    /**
     * Obtiene el valor de la propiedad p8Ej2IzqFue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP8Ej2IzqFue() {
        return p8Ej2IzqFue;
    }

    /**
     * Define el valor de la propiedad p8Ej2IzqFue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP8Ej2IzqFue(String value) {
        this.p8Ej2IzqFue = value;
    }

    /**
     * Obtiene el valor de la propiedad p8Ej2IzqPes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP8Ej2IzqPes() {
        return p8Ej2IzqPes;
    }

    /**
     * Define el valor de la propiedad p8Ej2IzqPes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP8Ej2IzqPes(String value) {
        this.p8Ej2IzqPes = value;
    }

    /**
     * Obtiene el valor de la propiedad p8Ej2DerFue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP8Ej2DerFue() {
        return p8Ej2DerFue;
    }

    /**
     * Define el valor de la propiedad p8Ej2DerFue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP8Ej2DerFue(String value) {
        this.p8Ej2DerFue = value;
    }

    /**
     * Obtiene el valor de la propiedad p8Ej2DerPes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP8Ej2DerPes() {
        return p8Ej2DerPes;
    }

    /**
     * Define el valor de la propiedad p8Ej2DerPes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP8Ej2DerPes(String value) {
        this.p8Ej2DerPes = value;
    }

    /**
     * Obtiene el valor de la propiedad p8Ej2Des.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP8Ej2Des() {
        return p8Ej2Des;
    }

    /**
     * Define el valor de la propiedad p8Ej2Des.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP8Ej2Des(String value) {
        this.p8Ej2Des = value;
    }

    /**
     * Obtiene el valor de la propiedad p8Ej2Max.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP8Ej2Max() {
        return p8Ej2Max;
    }

    /**
     * Define el valor de la propiedad p8Ej2Max.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP8Ej2Max(String value) {
        this.p8Ej2Max = value;
    }

    /**
     * Obtiene el valor de la propiedad p8EfiAux.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP8EfiAux() {
        return p8EfiAux;
    }

    /**
     * Define el valor de la propiedad p8EfiAux.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP8EfiAux(String value) {
        this.p8EfiAux = value;
    }

    /**
     * Obtiene el valor de la propiedad p8EfiAuxMin.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP8EfiAuxMin() {
        return p8EfiAuxMin;
    }

    /**
     * Define el valor de la propiedad p8EfiAuxMin.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP8EfiAuxMin(String value) {
        this.p8EfiAuxMin = value;
    }

    /**
     * Obtiene el valor de la propiedad p8Ej3IzqFue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP8Ej3IzqFue() {
        return p8Ej3IzqFue;
    }

    /**
     * Define el valor de la propiedad p8Ej3IzqFue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP8Ej3IzqFue(String value) {
        this.p8Ej3IzqFue = value;
    }

    /**
     * Obtiene el valor de la propiedad p8Ej3IzqPes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP8Ej3IzqPes() {
        return p8Ej3IzqPes;
    }

    /**
     * Define el valor de la propiedad p8Ej3IzqPes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP8Ej3IzqPes(String value) {
        this.p8Ej3IzqPes = value;
    }

    /**
     * Obtiene el valor de la propiedad p8Ej3DerFue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP8Ej3DerFue() {
        return p8Ej3DerFue;
    }

    /**
     * Define el valor de la propiedad p8Ej3DerFue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP8Ej3DerFue(String value) {
        this.p8Ej3DerFue = value;
    }

    /**
     * Obtiene el valor de la propiedad p8Ej3DerPes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP8Ej3DerPes() {
        return p8Ej3DerPes;
    }

    /**
     * Define el valor de la propiedad p8Ej3DerPes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP8Ej3DerPes(String value) {
        this.p8Ej3DerPes = value;
    }

    /**
     * Obtiene el valor de la propiedad p8Ej3Des.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP8Ej3Des() {
        return p8Ej3Des;
    }

    /**
     * Define el valor de la propiedad p8Ej3Des.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP8Ej3Des(String value) {
        this.p8Ej3Des = value;
    }

    /**
     * Obtiene el valor de la propiedad p8Ej3Max.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP8Ej3Max() {
        return p8Ej3Max;
    }

    /**
     * Define el valor de la propiedad p8Ej3Max.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP8Ej3Max(String value) {
        this.p8Ej3Max = value;
    }

    /**
     * Obtiene el valor de la propiedad p8Ej4IzqFue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP8Ej4IzqFue() {
        return p8Ej4IzqFue;
    }

    /**
     * Define el valor de la propiedad p8Ej4IzqFue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP8Ej4IzqFue(String value) {
        this.p8Ej4IzqFue = value;
    }

    /**
     * Obtiene el valor de la propiedad p8Ej4IzqPes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP8Ej4IzqPes() {
        return p8Ej4IzqPes;
    }

    /**
     * Define el valor de la propiedad p8Ej4IzqPes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP8Ej4IzqPes(String value) {
        this.p8Ej4IzqPes = value;
    }

    /**
     * Obtiene el valor de la propiedad p8Ej4DerFue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP8Ej4DerFue() {
        return p8Ej4DerFue;
    }

    /**
     * Define el valor de la propiedad p8Ej4DerFue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP8Ej4DerFue(String value) {
        this.p8Ej4DerFue = value;
    }

    /**
     * Obtiene el valor de la propiedad p8Ej4DerPes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP8Ej4DerPes() {
        return p8Ej4DerPes;
    }

    /**
     * Define el valor de la propiedad p8Ej4DerPes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP8Ej4DerPes(String value) {
        this.p8Ej4DerPes = value;
    }

    /**
     * Obtiene el valor de la propiedad p8Ej4Des.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP8Ej4Des() {
        return p8Ej4Des;
    }

    /**
     * Define el valor de la propiedad p8Ej4Des.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP8Ej4Des(String value) {
        this.p8Ej4Des = value;
    }

    /**
     * Obtiene el valor de la propiedad p8Ej4Max.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP8Ej4Max() {
        return p8Ej4Max;
    }

    /**
     * Define el valor de la propiedad p8Ej4Max.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP8Ej4Max(String value) {
        this.p8Ej4Max = value;
    }

    /**
     * Obtiene el valor de la propiedad p8Ej5IzqFue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP8Ej5IzqFue() {
        return p8Ej5IzqFue;
    }

    /**
     * Define el valor de la propiedad p8Ej5IzqFue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP8Ej5IzqFue(String value) {
        this.p8Ej5IzqFue = value;
    }

    /**
     * Obtiene el valor de la propiedad p8Ej5IzqPes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP8Ej5IzqPes() {
        return p8Ej5IzqPes;
    }

    /**
     * Define el valor de la propiedad p8Ej5IzqPes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP8Ej5IzqPes(String value) {
        this.p8Ej5IzqPes = value;
    }

    /**
     * Obtiene el valor de la propiedad p8Ej5DerFue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP8Ej5DerFue() {
        return p8Ej5DerFue;
    }

    /**
     * Define el valor de la propiedad p8Ej5DerFue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP8Ej5DerFue(String value) {
        this.p8Ej5DerFue = value;
    }

    /**
     * Obtiene el valor de la propiedad p8Ej5DerPes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP8Ej5DerPes() {
        return p8Ej5DerPes;
    }

    /**
     * Define el valor de la propiedad p8Ej5DerPes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP8Ej5DerPes(String value) {
        this.p8Ej5DerPes = value;
    }

    /**
     * Obtiene el valor de la propiedad p8Ej5Des.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP8Ej5Des() {
        return p8Ej5Des;
    }

    /**
     * Define el valor de la propiedad p8Ej5Des.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP8Ej5Des(String value) {
        this.p8Ej5Des = value;
    }

    /**
     * Obtiene el valor de la propiedad p8Ej5Max.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP8Ej5Max() {
        return p8Ej5Max;
    }

    /**
     * Define el valor de la propiedad p8Ej5Max.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP8Ej5Max(String value) {
        this.p8Ej5Max = value;
    }

    /**
     * Obtiene el valor de la propiedad p9Ej1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP9Ej1() {
        return p9Ej1;
    }

    /**
     * Define el valor de la propiedad p9Ej1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP9Ej1(String value) {
        this.p9Ej1 = value;
    }

    /**
     * Obtiene el valor de la propiedad p9Ej2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP9Ej2() {
        return p9Ej2;
    }

    /**
     * Define el valor de la propiedad p9Ej2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP9Ej2(String value) {
        this.p9Ej2 = value;
    }

    /**
     * Obtiene el valor de la propiedad p9Ej3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP9Ej3() {
        return p9Ej3;
    }

    /**
     * Define el valor de la propiedad p9Ej3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP9Ej3(String value) {
        this.p9Ej3 = value;
    }

    /**
     * Obtiene el valor de la propiedad p9Ej4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP9Ej4() {
        return p9Ej4;
    }

    /**
     * Define el valor de la propiedad p9Ej4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP9Ej4(String value) {
        this.p9Ej4 = value;
    }

    /**
     * Obtiene el valor de la propiedad p9Ej5.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP9Ej5() {
        return p9Ej5;
    }

    /**
     * Define el valor de la propiedad p9Ej5.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP9Ej5(String value) {
        this.p9Ej5 = value;
    }

    /**
     * Obtiene el valor de la propiedad p9Max.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP9Max() {
        return p9Max;
    }

    /**
     * Define el valor de la propiedad p9Max.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP9Max(String value) {
        this.p9Max = value;
    }

    /**
     * Obtiene el valor de la propiedad p10RefComLla.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP10RefComLla() {
        return p10RefComLla;
    }

    /**
     * Define el valor de la propiedad p10RefComLla.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP10RefComLla(String value) {
        this.p10RefComLla = value;
    }

    /**
     * Obtiene el valor de la propiedad p10ErrDis.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP10ErrDis() {
        return p10ErrDis;
    }

    /**
     * Define el valor de la propiedad p10ErrDis.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP10ErrDis(String value) {
        this.p10ErrDis = value;
    }

    /**
     * Obtiene el valor de la propiedad p10ErrTie.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP10ErrTie() {
        return p10ErrTie;
    }

    /**
     * Define el valor de la propiedad p10ErrTie.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP10ErrTie(String value) {
        this.p10ErrTie = value;
    }

    /**
     * Obtiene el valor de la propiedad p10Max.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP10Max() {
        return p10Max;
    }

    /**
     * Define el valor de la propiedad p10Max.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP10Max(String value) {
        this.p10Max = value;
    }

    /**
     * Obtiene el valor de la propiedad p11CoRalVal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP11CoRalVal() {
        return p11CoRalVal;
    }

    /**
     * Define el valor de la propiedad p11CoRalVal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP11CoRalVal(String value) {
        this.p11CoRalVal = value;
    }

    /**
     * Obtiene el valor de la propiedad p11CoRalNor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP11CoRalNor() {
        return p11CoRalNor;
    }

    /**
     * Define el valor de la propiedad p11CoRalNor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP11CoRalNor(String value) {
        this.p11CoRalNor = value;
    }

    /**
     * Obtiene el valor de la propiedad p11Co2RalVal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP11Co2RalVal() {
        return p11Co2RalVal;
    }

    /**
     * Define el valor de la propiedad p11Co2RalVal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP11Co2RalVal(String value) {
        this.p11Co2RalVal = value;
    }

    /**
     * Obtiene el valor de la propiedad p11Co2RalNor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP11Co2RalNor() {
        return p11Co2RalNor;
    }

    /**
     * Define el valor de la propiedad p11Co2RalNor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP11Co2RalNor(String value) {
        this.p11Co2RalNor = value;
    }

    /**
     * Obtiene el valor de la propiedad p11O2RalVal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP11O2RalVal() {
        return p11O2RalVal;
    }

    /**
     * Define el valor de la propiedad p11O2RalVal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP11O2RalVal(String value) {
        this.p11O2RalVal = value;
    }

    /**
     * Obtiene el valor de la propiedad p11O2RalNor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP11O2RalNor() {
        return p11O2RalNor;
    }

    /**
     * Define el valor de la propiedad p11O2RalNor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP11O2RalNor(String value) {
        this.p11O2RalNor = value;
    }

    /**
     * Obtiene el valor de la propiedad p11HcRalVal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP11HcRalVal() {
        return p11HcRalVal;
    }

    /**
     * Define el valor de la propiedad p11HcRalVal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP11HcRalVal(String value) {
        this.p11HcRalVal = value;
    }

    /**
     * Obtiene el valor de la propiedad p11HcRalNor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP11HcRalNor() {
        return p11HcRalNor;
    }

    /**
     * Define el valor de la propiedad p11HcRalNor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP11HcRalNor(String value) {
        this.p11HcRalNor = value;
    }

    /**
     * Obtiene el valor de la propiedad p11CoCruVal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP11CoCruVal() {
        return p11CoCruVal;
    }

    /**
     * Define el valor de la propiedad p11CoCruVal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP11CoCruVal(String value) {
        this.p11CoCruVal = value;
    }

    /**
     * Obtiene el valor de la propiedad p11CoCruNor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP11CoCruNor() {
        return p11CoCruNor;
    }

    /**
     * Define el valor de la propiedad p11CoCruNor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP11CoCruNor(String value) {
        this.p11CoCruNor = value;
    }

    /**
     * Obtiene el valor de la propiedad p11Co2CruVal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP11Co2CruVal() {
        return p11Co2CruVal;
    }

    /**
     * Define el valor de la propiedad p11Co2CruVal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP11Co2CruVal(String value) {
        this.p11Co2CruVal = value;
    }

    /**
     * Obtiene el valor de la propiedad p11Co2CruNor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP11Co2CruNor() {
        return p11Co2CruNor;
    }

    /**
     * Define el valor de la propiedad p11Co2CruNor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP11Co2CruNor(String value) {
        this.p11Co2CruNor = value;
    }

    /**
     * Obtiene el valor de la propiedad p11O2CruVal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP11O2CruVal() {
        return p11O2CruVal;
    }

    /**
     * Define el valor de la propiedad p11O2CruVal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP11O2CruVal(String value) {
        this.p11O2CruVal = value;
    }

    /**
     * Obtiene el valor de la propiedad p11O2CruNor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP11O2CruNor() {
        return p11O2CruNor;
    }

    /**
     * Define el valor de la propiedad p11O2CruNor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP11O2CruNor(String value) {
        this.p11O2CruNor = value;
    }

    /**
     * Obtiene el valor de la propiedad p11HcCruVal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP11HcCruVal() {
        return p11HcCruVal;
    }

    /**
     * Define el valor de la propiedad p11HcCruVal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP11HcCruVal(String value) {
        this.p11HcCruVal = value;
    }

    /**
     * Obtiene el valor de la propiedad p11HcCruNor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP11HcCruNor() {
        return p11HcCruNor;
    }

    /**
     * Define el valor de la propiedad p11HcCruNor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP11HcCruNor(String value) {
        this.p11HcCruNor = value;
    }

    /**
     * Obtiene el valor de la propiedad pg17.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPG17() {
        return pg17;
    }

    /**
     * Define el valor de la propiedad pg17.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPG17(String value) {
        this.pg17 = value;
    }

    /**
     * Obtiene el valor de la propiedad p11TemRal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP11TemRal() {
        return p11TemRal;
    }

    /**
     * Define el valor de la propiedad p11TemRal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP11TemRal(String value) {
        this.p11TemRal = value;
    }

    /**
     * Obtiene el valor de la propiedad p11RpmRal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP11RpmRal() {
        return p11RpmRal;
    }

    /**
     * Define el valor de la propiedad p11RpmRal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP11RpmRal(String value) {
        this.p11RpmRal = value;
    }

    /**
     * Obtiene el valor de la propiedad p11TemCru.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP11TemCru() {
        return p11TemCru;
    }

    /**
     * Define el valor de la propiedad p11TemCru.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP11TemCru(String value) {
        this.p11TemCru = value;
    }

    /**
     * Obtiene el valor de la propiedad p11RpmCru.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP11RpmCru() {
        return p11RpmCru;
    }

    /**
     * Define el valor de la propiedad p11RpmCru.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP11RpmCru(String value) {
        this.p11RpmCru = value;
    }

    /**
     * Obtiene el valor de la propiedad p11NoRalVal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP11NoRalVal() {
        return p11NoRalVal;
    }

    /**
     * Define el valor de la propiedad p11NoRalVal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP11NoRalVal(String value) {
        this.p11NoRalVal = value;
    }

    /**
     * Obtiene el valor de la propiedad p11NoRalNor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP11NoRalNor() {
        return p11NoRalNor;
    }

    /**
     * Define el valor de la propiedad p11NoRalNor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP11NoRalNor(String value) {
        this.p11NoRalNor = value;
    }

    /**
     * Obtiene el valor de la propiedad p11NoCruVal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP11NoCruVal() {
        return p11NoCruVal;
    }

    /**
     * Define el valor de la propiedad p11NoCruVal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP11NoCruVal(String value) {
        this.p11NoCruVal = value;
    }

    /**
     * Obtiene el valor de la propiedad p11NoCruNor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP11NoCruNor() {
        return p11NoCruNor;
    }

    /**
     * Define el valor de la propiedad p11NoCruNor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP11NoCruNor(String value) {
        this.p11NoCruNor = value;
    }

    /**
     * Obtiene el valor de la propiedad po01.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPO01() {
        return po01;
    }

    /**
     * Define el valor de la propiedad po01.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPO01(String value) {
        this.po01 = value;
    }

    /**
     * Obtiene el valor de la propiedad p11BCi1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP11BCi1() {
        return p11BCi1;
    }

    /**
     * Define el valor de la propiedad p11BCi1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP11BCi1(String value) {
        this.p11BCi1 = value;
    }

    /**
     * Obtiene el valor de la propiedad p11BCi2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP11BCi2() {
        return p11BCi2;
    }

    /**
     * Define el valor de la propiedad p11BCi2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP11BCi2(String value) {
        this.p11BCi2 = value;
    }

    /**
     * Obtiene el valor de la propiedad p11BCi3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP11BCi3() {
        return p11BCi3;
    }

    /**
     * Define el valor de la propiedad p11BCi3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP11BCi3(String value) {
        this.p11BCi3 = value;
    }

    /**
     * Obtiene el valor de la propiedad p11BCi4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP11BCi4() {
        return p11BCi4;
    }

    /**
     * Define el valor de la propiedad p11BCi4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP11BCi4(String value) {
        this.p11BCi4 = value;
    }

    /**
     * Obtiene el valor de la propiedad p11BResVal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP11BResVal() {
        return p11BResVal;
    }

    /**
     * Define el valor de la propiedad p11BResVal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP11BResVal(String value) {
        this.p11BResVal = value;
    }

    /**
     * Obtiene el valor de la propiedad p11BResNor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP11BResNor() {
        return p11BResNor;
    }

    /**
     * Define el valor de la propiedad p11BResNor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP11BResNor(String value) {
        this.p11BResNor = value;
    }

    /**
     * Obtiene el valor de la propiedad p11BTem.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP11BTem() {
        return p11BTem;
    }

    /**
     * Define el valor de la propiedad p11BTem.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP11BTem(String value) {
        this.p11BTem = value;
    }

    /**
     * Obtiene el valor de la propiedad p11BRpm.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP11BRpm() {
        return p11BRpm;
    }

    /**
     * Define el valor de la propiedad p11BRpm.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP11BRpm(String value) {
        this.p11BRpm = value;
    }

    /**
     * Obtiene el valor de la propiedad po10.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPO10() {
        return po10;
    }

    /**
     * Define el valor de la propiedad po10.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPO10(String value) {
        this.po10 = value;
    }

    /**
     * Obtiene el valor de la propiedad pTw01.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPTw01() {
        return pTw01;
    }

    /**
     * Define el valor de la propiedad pTw01.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPTw01(String value) {
        this.pTw01 = value;
    }

    /**
     * Obtiene el valor de la propiedad pv01.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPV01() {
        return pv01;
    }

    /**
     * Define el valor de la propiedad pv01.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPV01(String value) {
        this.pv01 = value;
    }

    /**
     * Obtiene el valor de la propiedad pv02.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPV02() {
        return pv02;
    }

    /**
     * Define el valor de la propiedad pv02.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPV02(String value) {
        this.pv02 = value;
    }

    /**
     * Obtiene el valor de la propiedad pv03.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPV03() {
        return pv03;
    }

    /**
     * Define el valor de la propiedad pv03.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPV03(String value) {
        this.pv03 = value;
    }

    /**
     * Obtiene el valor de la propiedad pv04.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPV04() {
        return pv04;
    }

    /**
     * Define el valor de la propiedad pv04.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPV04(String value) {
        this.pv04 = value;
    }

    /**
     * Obtiene el valor de la propiedad pv05.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPV05() {
        return pv05;
    }

    /**
     * Define el valor de la propiedad pv05.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPV05(String value) {
        this.pv05 = value;
    }

    /**
     * Obtiene el valor de la propiedad pv06.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPV06() {
        return pv06;
    }

    /**
     * Define el valor de la propiedad pv06.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPV06(String value) {
        this.pv06 = value;
    }

    /**
     * Obtiene el valor de la propiedad pv07.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPV07() {
        return pv07;
    }

    /**
     * Define el valor de la propiedad pv07.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPV07(String value) {
        this.pv07 = value;
    }

    /**
     * Obtiene el valor de la propiedad pv08.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPV08() {
        return pv08;
    }

    /**
     * Define el valor de la propiedad pv08.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPV08(String value) {
        this.pv08 = value;
    }

    /**
     * Obtiene el valor de la propiedad pv09.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPV09() {
        return pv09;
    }

    /**
     * Define el valor de la propiedad pv09.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPV09(String value) {
        this.pv09 = value;
    }

    /**
     * Obtiene el valor de la propiedad pv10.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPV10() {
        return pv10;
    }

    /**
     * Define el valor de la propiedad pv10.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPV10(String value) {
        this.pv10 = value;
    }

    /**
     * Obtiene el valor de la propiedad pv11.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPV11() {
        return pv11;
    }

    /**
     * Define el valor de la propiedad pv11.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPV11(String value) {
        this.pv11 = value;
    }

    /**
     * Obtiene el valor de la propiedad pv12.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPV12() {
        return pv12;
    }

    /**
     * Define el valor de la propiedad pv12.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPV12(String value) {
        this.pv12 = value;
    }

    /**
     * Obtiene el valor de la propiedad pcCod.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPCCod() {
        return pcCod;
    }

    /**
     * Define el valor de la propiedad pcCod.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPCCod(String value) {
        this.pcCod = value;
    }

    /**
     * Obtiene el valor de la propiedad pcDes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPCDes() {
        return pcDes;
    }

    /**
     * Define el valor de la propiedad pcDes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPCDes(String value) {
        this.pcDes = value;
    }

    /**
     * Obtiene el valor de la propiedad pcGru.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPCGru() {
        return pcGru;
    }

    /**
     * Define el valor de la propiedad pcGru.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPCGru(String value) {
        this.pcGru = value;
    }

    /**
     * Obtiene el valor de la propiedad pcTipDefA.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPCTipDefA() {
        return pcTipDefA;
    }

    /**
     * Define el valor de la propiedad pcTipDefA.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPCTipDefA(String value) {
        this.pcTipDefA = value;
    }

    /**
     * Obtiene el valor de la propiedad pcTipDefB.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPCTipDefB() {
        return pcTipDefB;
    }

    /**
     * Define el valor de la propiedad pcTipDefB.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPCTipDefB(String value) {
        this.pcTipDefB = value;
    }

    /**
     * Obtiene el valor de la propiedad pcTipDefATot.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPCTipDefATot() {
        return pcTipDefATot;
    }

    /**
     * Define el valor de la propiedad pcTipDefATot.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPCTipDefATot(String value) {
        this.pcTipDefATot = value;
    }

    /**
     * Obtiene el valor de la propiedad pcTipDefBTot.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPCTipDefBTot() {
        return pcTipDefBTot;
    }

    /**
     * Define el valor de la propiedad pcTipDefBTot.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPCTipDefBTot(String value) {
        this.pcTipDefBTot = value;
    }

    /**
     * Obtiene el valor de la propiedad pdCod.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPDCod() {
        return pdCod;
    }

    /**
     * Define el valor de la propiedad pdCod.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPDCod(String value) {
        this.pdCod = value;
    }

    /**
     * Obtiene el valor de la propiedad pdDes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPDDes() {
        return pdDes;
    }

    /**
     * Define el valor de la propiedad pdDes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPDDes(String value) {
        this.pdDes = value;
    }

    /**
     * Obtiene el valor de la propiedad pdGru.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPDGru() {
        return pdGru;
    }

    /**
     * Define el valor de la propiedad pdGru.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPDGru(String value) {
        this.pdGru = value;
    }

    /**
     * Obtiene el valor de la propiedad pdTipDefA.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPDTipDefA() {
        return pdTipDefA;
    }

    /**
     * Define el valor de la propiedad pdTipDefA.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPDTipDefA(String value) {
        this.pdTipDefA = value;
    }

    /**
     * Obtiene el valor de la propiedad pdTipDefB.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPDTipDefB() {
        return pdTipDefB;
    }

    /**
     * Define el valor de la propiedad pdTipDefB.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPDTipDefB(String value) {
        this.pdTipDefB = value;
    }

    /**
     * Obtiene el valor de la propiedad pdTipDefATot.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPDTipDefATot() {
        return pdTipDefATot;
    }

    /**
     * Define el valor de la propiedad pdTipDefATot.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPDTipDefATot(String value) {
        this.pdTipDefATot = value;
    }

    /**
     * Obtiene el valor de la propiedad pdTipDefBTot.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPDTipDefBTot() {
        return pdTipDefBTot;
    }

    /**
     * Define el valor de la propiedad pdTipDefBTot.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPDTipDefBTot(String value) {
        this.pdTipDefBTot = value;
    }

    /**
     * Obtiene el valor de la propiedad pd1Cod.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPD1Cod() {
        return pd1Cod;
    }

    /**
     * Define el valor de la propiedad pd1Cod.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPD1Cod(String value) {
        this.pd1Cod = value;
    }

    /**
     * Obtiene el valor de la propiedad pd1Des.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPD1Des() {
        return pd1Des;
    }

    /**
     * Define el valor de la propiedad pd1Des.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPD1Des(String value) {
        this.pd1Des = value;
    }

    /**
     * Obtiene el valor de la propiedad pd1Gru.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPD1Gru() {
        return pd1Gru;
    }

    /**
     * Define el valor de la propiedad pd1Gru.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPD1Gru(String value) {
        this.pd1Gru = value;
    }

    /**
     * Obtiene el valor de la propiedad pd1TipDefA.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPD1TipDefA() {
        return pd1TipDefA;
    }

    /**
     * Define el valor de la propiedad pd1TipDefA.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPD1TipDefA(String value) {
        this.pd1TipDefA = value;
    }

    /**
     * Obtiene el valor de la propiedad pd1TipDefB.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPD1TipDefB() {
        return pd1TipDefB;
    }

    /**
     * Define el valor de la propiedad pd1TipDefB.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPD1TipDefB(String value) {
        this.pd1TipDefB = value;
    }

    /**
     * Obtiene el valor de la propiedad pd1TipDefATot.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPD1TipDefATot() {
        return pd1TipDefATot;
    }

    /**
     * Define el valor de la propiedad pd1TipDefATot.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPD1TipDefATot(String value) {
        this.pd1TipDefATot = value;
    }

    /**
     * Obtiene el valor de la propiedad pd1TipDefBTot.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPD1TipDefBTot() {
        return pd1TipDefBTot;
    }

    /**
     * Define el valor de la propiedad pd1TipDefBTot.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPD1TipDefBTot(String value) {
        this.pd1TipDefBTot = value;
    }

    /**
     * Obtiene el valor de la propiedad peConRun.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPEConRun() {
        return peConRun;
    }

    /**
     * Define el valor de la propiedad peConRun.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPEConRun(String value) {
        this.peConRun = value;
    }

    /**
     * Obtiene el valor de la propiedad peApr.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPEApr() {
        return peApr;
    }

    /**
     * Define el valor de la propiedad peApr.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPEApr(String value) {
        this.peApr = value;
    }

    /**
     * Obtiene el valor de la propiedad pe1Apr.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPE1Apr() {
        return pe1Apr;
    }

    /**
     * Define el valor de la propiedad pe1Apr.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPE1Apr(String value) {
        this.pe1Apr = value;
    }

    /**
     * Obtiene el valor de la propiedad pfComObs.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPFComObs() {
        return pfComObs;
    }

    /**
     * Define el valor de la propiedad pfComObs.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPFComObs(String value) {
        this.pfComObs = value;
    }

    /**
     * Obtiene el valor de la propiedad phNomOpeReaRevTec.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPHNomOpeReaRevTec() {
        return phNomOpeReaRevTec;
    }

    /**
     * Define el valor de la propiedad phNomOpeReaRevTec.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPHNomOpeReaRevTec(String value) {
        this.phNomOpeReaRevTec = value;
    }

    /**
     * Obtiene el valor de la propiedad pgNomFirDirTec.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPGNomFirDirTec() {
        return pgNomFirDirTec;
    }

    /**
     * Define el valor de la propiedad pgNomFirDirTec.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPGNomFirDirTec(String value) {
        this.pgNomFirDirTec = value;
    }

    /**
     * Obtiene el valor de la propiedad pCausaRechazo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPCausaRechazo() {
        return pCausaRechazo;
    }

    /**
     * Define el valor de la propiedad pCausaRechazo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPCausaRechazo(String value) {
        this.pCausaRechazo = value;
    }

    /**
     * Obtiene el valor de la propiedad pFoto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPFoto() {
        return pFoto;
    }

    /**
     * Define el valor de la propiedad pFoto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPFoto(String value) {
        this.pFoto = value;
    }

}
