
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ingresar_fur_v3Result" type="{http://tempuri.org/}resultado" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "ingresarFurV3Result"
})
@XmlRootElement(name = "ingresar_fur_v3Response")
public class IngresarFurV3Response {

    @XmlElement(name = "ingresar_fur_v3Result")
    protected Resultado ingresarFurV3Result;

    /**
     * Obtiene el valor de la propiedad ingresarFurV3Result.
     * 
     * @return
     *     possible object is
     *     {@link Resultado }
     *     
     */
    public Resultado getIngresarFurV3Result() {
        return ingresarFurV3Result;
    }

    /**
     * Define el valor de la propiedad ingresarFurV3Result.
     * 
     * @param value
     *     allowed object is
     *     {@link Resultado }
     *     
     */
    public void setIngresarFurV3Result(Resultado value) {
        this.ingresarFurV3Result = value;
    }

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("IngresarFurV3Response [ingresarFurV3Result=");
		builder.append(ingresarFurV3Result);
		builder.append("]");
		return builder.toString();
	}

    
}
