
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.w3c.dom.Element;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="traer_fotoResult" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;any processContents='lax' namespace='urn:schemas-microsoft-com:xml-diffgram-v1'/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "traerFotoResult"
})
@XmlRootElement(name = "traer_fotoResponse")
public class TraerFotoResponse {

    @XmlElement(name = "traer_fotoResult")
    protected TraerFotoResponse.TraerFotoResult traerFotoResult;

    /**
     * Obtiene el valor de la propiedad traerFotoResult.
     * 
     * @return
     *     possible object is
     *     {@link TraerFotoResponse.TraerFotoResult }
     *     
     */
    public TraerFotoResponse.TraerFotoResult getTraerFotoResult() {
        return traerFotoResult;
    }

    /**
     * Define el valor de la propiedad traerFotoResult.
     * 
     * @param value
     *     allowed object is
     *     {@link TraerFotoResponse.TraerFotoResult }
     *     
     */
    public void setTraerFotoResult(TraerFotoResponse.TraerFotoResult value) {
        this.traerFotoResult = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;any processContents='lax' namespace='urn:schemas-microsoft-com:xml-diffgram-v1'/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "any2"
    })
    public static class TraerFotoResult {

        @XmlAnyElement(lax = true)
        protected Object any2;

        /**
         * Obtiene el valor de la propiedad any2.
         * 
         * @return
         *     possible object is
         *     {@link Element }
         *     {@link Object }
         *     
         */
        public Object getAny2() {
            return any2;
        }

        /**
         * Define el valor de la propiedad any2.
         * 
         * @param value
         *     allowed object is
         *     {@link Element }
         *     {@link Object }
         *     
         */
        public void setAny2(Object value) {
            this.any2 = value;
        }

    }

}
