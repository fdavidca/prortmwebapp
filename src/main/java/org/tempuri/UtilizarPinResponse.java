
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="utilizar_pinResult" type="{http://tempuri.org/}resultado" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "utilizarPinResult"
})
@XmlRootElement(name = "utilizar_pinResponse")
public class UtilizarPinResponse {

    @XmlElement(name = "utilizar_pinResult")
    protected Resultado utilizarPinResult;

    /**
     * Obtiene el valor de la propiedad utilizarPinResult.
     * 
     * @return
     *     possible object is
     *     {@link Resultado }
     *     
     */
    public Resultado getUtilizarPinResult() {
        return utilizarPinResult;
    }

    /**
     * Define el valor de la propiedad utilizarPinResult.
     * 
     * @param value
     *     allowed object is
     *     {@link Resultado }
     *     
     */
    public void setUtilizarPinResult(Resultado value) {
        this.utilizarPinResult = value;
    }

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UtilizarPinResponse [utilizarPinResult=");
		builder.append(utilizarPinResult);
		builder.append("]");
		return builder.toString();
	}

    
}
