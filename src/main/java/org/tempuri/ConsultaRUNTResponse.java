
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConsultaRUNTResult" type="{http://tempuri.org/}vehiculoSICOV" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultaRUNTResult"
})
@XmlRootElement(name = "ConsultaRUNTResponse")
public class ConsultaRUNTResponse {

    @XmlElement(name = "ConsultaRUNTResult")
    protected VehiculoSICOV consultaRUNTResult;

    /**
     * Obtiene el valor de la propiedad consultaRUNTResult.
     * 
     * @return
     *     possible object is
     *     {@link VehiculoSICOV }
     *     
     */
    public VehiculoSICOV getConsultaRUNTResult() {
        return consultaRUNTResult;
    }

    /**
     * Define el valor de la propiedad consultaRUNTResult.
     * 
     * @param value
     *     allowed object is
     *     {@link VehiculoSICOV }
     *     
     */
    public void setConsultaRUNTResult(VehiculoSICOV value) {
        this.consultaRUNTResult = value;
    }

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ConsultaRUNTResponse [consultaRUNTResult=");
		builder.append(consultaRUNTResult);
		builder.append("]");
		return builder.toString();
	}
    
    

}
