
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dato" type="{http://tempuri.org/}ConsultaJohn" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dato"
})
@XmlRootElement(name = "ConsultaRuntSicov")
public class ConsultaRuntSicov {

    protected ConsultaJohn dato;

    /**
     * Obtiene el valor de la propiedad dato.
     * 
     * @return
     *     possible object is
     *     {@link ConsultaJohn }
     *     
     */
    public ConsultaJohn getDato() {
        return dato;
    }

    /**
     * Define el valor de la propiedad dato.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsultaJohn }
     *     
     */
    public void setDato(ConsultaJohn value) {
        this.dato = value;
    }

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ConsultaRuntSicov [dato=");
		builder.append(dato);
		builder.append("]");
		return builder.toString();
	}

}
