
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="fur" type="{http://tempuri.org/}formulario_v3" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fur"
})
@XmlRootElement(name = "ingresar_fur_v3")
public class IngresarFurV3 {

    protected FormularioV3 fur;

    /**
     * Obtiene el valor de la propiedad fur.
     * 
     * @return
     *     possible object is
     *     {@link FormularioV3 }
     *     
     */
    public FormularioV3 getFur() {
        return fur;
    }

    /**
     * Define el valor de la propiedad fur.
     * 
     * @param value
     *     allowed object is
     *     {@link FormularioV3 }
     *     
     */
    public void setFur(FormularioV3 value) {
        this.fur = value;
    }

}
