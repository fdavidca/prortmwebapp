
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para vehiculoRUNT complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="vehiculoRUNT">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CodigoRespuesta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MensajeRespuesta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="noRegistro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="noPlaca" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="noChasis" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idMarca" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="marca" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idLinea" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="linea" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idTipoServicio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoServicio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idColor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="color" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="modelo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cilindraje" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idTipoCombustible" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoCombustible" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idClaseVehiculo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="claseVehiculo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="noMotor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="noSerie" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="noVIN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="capacidadTotalPasajeros" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="capacidadPasajerosSentados" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="blindado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esEnsenanza" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechaMatricula" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="datosCdasRtm" type="{http://tempuri.org/}ArrayOfDatosCdasRtm" minOccurs="0"/>
 *         &lt;element name="soatNacionales" type="{http://tempuri.org/}ArrayOfSoatNacionales" minOccurs="0"/>
 *         &lt;element name="codigoUUID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoResultado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "vehiculoRUNT", propOrder = {
    "codigoRespuesta",
    "mensajeRespuesta",
    "noRegistro",
    "noPlaca",
    "noChasis",
    "idMarca",
    "marca",
    "idLinea",
    "linea",
    "idTipoServicio",
    "tipoServicio",
    "idColor",
    "color",
    "modelo",
    "cilindraje",
    "idTipoCombustible",
    "tipoCombustible",
    "idClaseVehiculo",
    "claseVehiculo",
    "noMotor",
    "noSerie",
    "noVIN",
    "capacidadTotalPasajeros",
    "capacidadPasajerosSentados",
    "blindado",
    "esEnsenanza",
    "fechaMatricula",
    "datosCdasRtm",
    "soatNacionales",
    "codigoUUID",
    "codigoResultado",
    "fecha",
    "idUsuario"
})
public class VehiculoRUNT {

    @XmlElement(name = "CodigoRespuesta")
    protected String codigoRespuesta;
    @XmlElement(name = "MensajeRespuesta")
    protected String mensajeRespuesta;
    protected String noRegistro;
    protected String noPlaca;
    protected String noChasis;
    protected String idMarca;
    protected String marca;
    protected String idLinea;
    protected String linea;
    protected String idTipoServicio;
    protected String tipoServicio;
    protected String idColor;
    protected String color;
    protected String modelo;
    protected String cilindraje;
    protected String idTipoCombustible;
    protected String tipoCombustible;
    protected String idClaseVehiculo;
    protected String claseVehiculo;
    protected String noMotor;
    protected String noSerie;
    protected String noVIN;
    protected String capacidadTotalPasajeros;
    protected String capacidadPasajerosSentados;
    protected String blindado;
    protected String esEnsenanza;
    protected String fechaMatricula;
    protected ArrayOfDatosCdasRtm datosCdasRtm;
    protected ArrayOfSoatNacionales soatNacionales;
    protected String codigoUUID;
    protected String codigoResultado;
    protected String fecha;
    protected String idUsuario;

    /**
     * Obtiene el valor de la propiedad codigoRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoRespuesta() {
        return codigoRespuesta;
    }

    /**
     * Define el valor de la propiedad codigoRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoRespuesta(String value) {
        this.codigoRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad mensajeRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensajeRespuesta() {
        return mensajeRespuesta;
    }

    /**
     * Define el valor de la propiedad mensajeRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensajeRespuesta(String value) {
        this.mensajeRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad noRegistro.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNoRegistro() {
        return noRegistro;
    }

    /**
     * Define el valor de la propiedad noRegistro.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNoRegistro(String value) {
        this.noRegistro = value;
    }

    /**
     * Obtiene el valor de la propiedad noPlaca.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNoPlaca() {
        return noPlaca;
    }

    /**
     * Define el valor de la propiedad noPlaca.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNoPlaca(String value) {
        this.noPlaca = value;
    }

    /**
     * Obtiene el valor de la propiedad noChasis.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNoChasis() {
        return noChasis;
    }

    /**
     * Define el valor de la propiedad noChasis.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNoChasis(String value) {
        this.noChasis = value;
    }

    /**
     * Obtiene el valor de la propiedad idMarca.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdMarca() {
        return idMarca;
    }

    /**
     * Define el valor de la propiedad idMarca.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdMarca(String value) {
        this.idMarca = value;
    }

    /**
     * Obtiene el valor de la propiedad marca.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMarca() {
        return marca;
    }

    /**
     * Define el valor de la propiedad marca.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMarca(String value) {
        this.marca = value;
    }

    /**
     * Obtiene el valor de la propiedad idLinea.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdLinea() {
        return idLinea;
    }

    /**
     * Define el valor de la propiedad idLinea.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdLinea(String value) {
        this.idLinea = value;
    }

    /**
     * Obtiene el valor de la propiedad linea.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLinea() {
        return linea;
    }

    /**
     * Define el valor de la propiedad linea.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLinea(String value) {
        this.linea = value;
    }

    /**
     * Obtiene el valor de la propiedad idTipoServicio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdTipoServicio() {
        return idTipoServicio;
    }

    /**
     * Define el valor de la propiedad idTipoServicio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdTipoServicio(String value) {
        this.idTipoServicio = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoServicio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoServicio() {
        return tipoServicio;
    }

    /**
     * Define el valor de la propiedad tipoServicio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoServicio(String value) {
        this.tipoServicio = value;
    }

    /**
     * Obtiene el valor de la propiedad idColor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdColor() {
        return idColor;
    }

    /**
     * Define el valor de la propiedad idColor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdColor(String value) {
        this.idColor = value;
    }

    /**
     * Obtiene el valor de la propiedad color.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getColor() {
        return color;
    }

    /**
     * Define el valor de la propiedad color.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setColor(String value) {
        this.color = value;
    }

    /**
     * Obtiene el valor de la propiedad modelo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModelo() {
        return modelo;
    }

    /**
     * Define el valor de la propiedad modelo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModelo(String value) {
        this.modelo = value;
    }

    /**
     * Obtiene el valor de la propiedad cilindraje.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCilindraje() {
        return cilindraje;
    }

    /**
     * Define el valor de la propiedad cilindraje.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCilindraje(String value) {
        this.cilindraje = value;
    }

    /**
     * Obtiene el valor de la propiedad idTipoCombustible.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdTipoCombustible() {
        return idTipoCombustible;
    }

    /**
     * Define el valor de la propiedad idTipoCombustible.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdTipoCombustible(String value) {
        this.idTipoCombustible = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoCombustible.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoCombustible() {
        return tipoCombustible;
    }

    /**
     * Define el valor de la propiedad tipoCombustible.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoCombustible(String value) {
        this.tipoCombustible = value;
    }

    /**
     * Obtiene el valor de la propiedad idClaseVehiculo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdClaseVehiculo() {
        return idClaseVehiculo;
    }

    /**
     * Define el valor de la propiedad idClaseVehiculo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdClaseVehiculo(String value) {
        this.idClaseVehiculo = value;
    }

    /**
     * Obtiene el valor de la propiedad claseVehiculo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaseVehiculo() {
        return claseVehiculo;
    }

    /**
     * Define el valor de la propiedad claseVehiculo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaseVehiculo(String value) {
        this.claseVehiculo = value;
    }

    /**
     * Obtiene el valor de la propiedad noMotor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNoMotor() {
        return noMotor;
    }

    /**
     * Define el valor de la propiedad noMotor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNoMotor(String value) {
        this.noMotor = value;
    }

    /**
     * Obtiene el valor de la propiedad noSerie.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNoSerie() {
        return noSerie;
    }

    /**
     * Define el valor de la propiedad noSerie.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNoSerie(String value) {
        this.noSerie = value;
    }

    /**
     * Obtiene el valor de la propiedad noVIN.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNoVIN() {
        return noVIN;
    }

    /**
     * Define el valor de la propiedad noVIN.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNoVIN(String value) {
        this.noVIN = value;
    }

    /**
     * Obtiene el valor de la propiedad capacidadTotalPasajeros.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCapacidadTotalPasajeros() {
        return capacidadTotalPasajeros;
    }

    /**
     * Define el valor de la propiedad capacidadTotalPasajeros.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCapacidadTotalPasajeros(String value) {
        this.capacidadTotalPasajeros = value;
    }

    /**
     * Obtiene el valor de la propiedad capacidadPasajerosSentados.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCapacidadPasajerosSentados() {
        return capacidadPasajerosSentados;
    }

    /**
     * Define el valor de la propiedad capacidadPasajerosSentados.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCapacidadPasajerosSentados(String value) {
        this.capacidadPasajerosSentados = value;
    }

    /**
     * Obtiene el valor de la propiedad blindado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBlindado() {
        return blindado;
    }

    /**
     * Define el valor de la propiedad blindado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBlindado(String value) {
        this.blindado = value;
    }

    /**
     * Obtiene el valor de la propiedad esEnsenanza.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsEnsenanza() {
        return esEnsenanza;
    }

    /**
     * Define el valor de la propiedad esEnsenanza.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsEnsenanza(String value) {
        this.esEnsenanza = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaMatricula.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaMatricula() {
        return fechaMatricula;
    }

    /**
     * Define el valor de la propiedad fechaMatricula.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaMatricula(String value) {
        this.fechaMatricula = value;
    }

    /**
     * Obtiene el valor de la propiedad datosCdasRtm.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfDatosCdasRtm }
     *     
     */
    public ArrayOfDatosCdasRtm getDatosCdasRtm() {
        return datosCdasRtm;
    }

    /**
     * Define el valor de la propiedad datosCdasRtm.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfDatosCdasRtm }
     *     
     */
    public void setDatosCdasRtm(ArrayOfDatosCdasRtm value) {
        this.datosCdasRtm = value;
    }

    /**
     * Obtiene el valor de la propiedad soatNacionales.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfSoatNacionales }
     *     
     */
    public ArrayOfSoatNacionales getSoatNacionales() {
        return soatNacionales;
    }

    /**
     * Define el valor de la propiedad soatNacionales.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfSoatNacionales }
     *     
     */
    public void setSoatNacionales(ArrayOfSoatNacionales value) {
        this.soatNacionales = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoUUID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoUUID() {
        return codigoUUID;
    }

    /**
     * Define el valor de la propiedad codigoUUID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoUUID(String value) {
        this.codigoUUID = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoResultado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoResultado() {
        return codigoResultado;
    }

    /**
     * Define el valor de la propiedad codigoResultado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoResultado(String value) {
        this.codigoResultado = value;
    }

    /**
     * Obtiene el valor de la propiedad fecha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * Define el valor de la propiedad fecha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecha(String value) {
        this.fecha = value;
    }

    /**
     * Obtiene el valor de la propiedad idUsuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdUsuario() {
        return idUsuario;
    }

    /**
     * Define el valor de la propiedad idUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdUsuario(String value) {
        this.idUsuario = value;
    }

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("VehiculoRUNT [codigoRespuesta=");
		builder.append(codigoRespuesta);
		builder.append(", mensajeRespuesta=");
		builder.append(mensajeRespuesta);
		builder.append(", noRegistro=");
		builder.append(noRegistro);
		builder.append(", noPlaca=");
		builder.append(noPlaca);
		builder.append(", noChasis=");
		builder.append(noChasis);
		builder.append(", idMarca=");
		builder.append(idMarca);
		builder.append(", marca=");
		builder.append(marca);
		builder.append(", idLinea=");
		builder.append(idLinea);
		builder.append(", linea=");
		builder.append(linea);
		builder.append(", idTipoServicio=");
		builder.append(idTipoServicio);
		builder.append(", tipoServicio=");
		builder.append(tipoServicio);
		builder.append(", idColor=");
		builder.append(idColor);
		builder.append(", color=");
		builder.append(color);
		builder.append(", modelo=");
		builder.append(modelo);
		builder.append(", cilindraje=");
		builder.append(cilindraje);
		builder.append(", idTipoCombustible=");
		builder.append(idTipoCombustible);
		builder.append(", tipoCombustible=");
		builder.append(tipoCombustible);
		builder.append(", idClaseVehiculo=");
		builder.append(idClaseVehiculo);
		builder.append(", claseVehiculo=");
		builder.append(claseVehiculo);
		builder.append(", noMotor=");
		builder.append(noMotor);
		builder.append(", noSerie=");
		builder.append(noSerie);
		builder.append(", noVIN=");
		builder.append(noVIN);
		builder.append(", capacidadTotalPasajeros=");
		builder.append(capacidadTotalPasajeros);
		builder.append(", capacidadPasajerosSentados=");
		builder.append(capacidadPasajerosSentados);
		builder.append(", blindado=");
		builder.append(blindado);
		builder.append(", esEnsenanza=");
		builder.append(esEnsenanza);
		builder.append(", fechaMatricula=");
		builder.append(fechaMatricula);
		builder.append(", datosCdasRtm=");
		builder.append(datosCdasRtm);
		builder.append(", soatNacionales=");
		builder.append(soatNacionales);
		builder.append(", codigoUUID=");
		builder.append(codigoUUID);
		builder.append(", codigoResultado=");
		builder.append(codigoResultado);
		builder.append(", fecha=");
		builder.append(fecha);
		builder.append(", idUsuario=");
		builder.append(idUsuario);
		builder.append("]");
		return builder.toString();
	}
    
    

}
