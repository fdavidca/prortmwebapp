
package org.tempuri;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.tempuri package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.tempuri
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TraerFotoResponse }
     * 
     */
    public TraerFotoResponse createTraerFotoResponse() {
        return new TraerFotoResponse();
    }

    /**
     * Create an instance of {@link IngresarFurResponse }
     * 
     */
    public IngresarFurResponse createIngresarFurResponse() {
        return new IngresarFurResponse();
    }

    /**
     * Create an instance of {@link Resultado }
     * 
     */
    public Resultado createResultado() {
        return new Resultado();
    }

    /**
     * Create an instance of {@link ConsultaRuntSicovResponse }
     * 
     */
    public ConsultaRuntSicovResponse createConsultaRuntSicovResponse() {
        return new ConsultaRuntSicovResponse();
    }

    /**
     * Create an instance of {@link VehiculoRUNT }
     * 
     */
    public VehiculoRUNT createVehiculoRUNT() {
        return new VehiculoRUNT();
    }

    /**
     * Create an instance of {@link ConsultaPin }
     * 
     */
    public ConsultaPin createConsultaPin() {
        return new ConsultaPin();
    }

    /**
     * Create an instance of {@link Pin }
     * 
     */
    public Pin createPin() {
        return new Pin();
    }

    /**
     * Create an instance of {@link ConsultaPinPlaca }
     * 
     */
    public ConsultaPinPlaca createConsultaPinPlaca() {
        return new ConsultaPinPlaca();
    }

    /**
     * Create an instance of {@link IngresarFurV2Response }
     * 
     */
    public IngresarFurV2Response createIngresarFurV2Response() {
        return new IngresarFurV2Response();
    }

    /**
     * Create an instance of {@link IngresarFurV3 }
     * 
     */
    public IngresarFurV3 createIngresarFurV3() {
        return new IngresarFurV3();
    }

    /**
     * Create an instance of {@link FormularioV3 }
     * 
     */
    public FormularioV3 createFormularioV3() {
        return new FormularioV3();
    }

    /**
     * Create an instance of {@link UtilizarPin }
     * 
     */
    public UtilizarPin createUtilizarPin() {
        return new UtilizarPin();
    }

    /**
     * Create an instance of {@link IngresarFurV2 }
     * 
     */
    public IngresarFurV2 createIngresarFurV2() {
        return new IngresarFurV2();
    }

    /**
     * Create an instance of {@link FormularioV2 }
     * 
     */
    public FormularioV2 createFormularioV2() {
        return new FormularioV2();
    }

    /**
     * Create an instance of {@link TraerFoto }
     * 
     */
    public TraerFoto createTraerFoto() {
        return new TraerFoto();
    }

    /**
     * Create an instance of {@link ConsultaRUNTResponse }
     * 
     */
    public ConsultaRUNTResponse createConsultaRUNTResponse() {
        return new ConsultaRUNTResponse();
    }

    /**
     * Create an instance of {@link VehiculoSICOV }
     * 
     */
    public VehiculoSICOV createVehiculoSICOV() {
        return new VehiculoSICOV();
    }

    /**
     * Create an instance of {@link GenericoResponse }
     * 
     */
    public GenericoResponse createGenericoResponse() {
        return new GenericoResponse();
    }

    /**
     * Create an instance of {@link Echo }
     * 
     */
    public Echo createEcho() {
        return new Echo();
    }

    /**
     * Create an instance of {@link ConsultaRuntSicov }
     * 
     */
    public ConsultaRuntSicov createConsultaRuntSicov() {
        return new ConsultaRuntSicov();
    }

    /**
     * Create an instance of {@link ConsultaJohn }
     * 
     */
    public ConsultaJohn createConsultaJohn() {
        return new ConsultaJohn();
    }

    /**
     * Create an instance of {@link ConsultaPinResponse }
     * 
     */
    public ConsultaPinResponse createConsultaPinResponse() {
        return new ConsultaPinResponse();
    }

    /**
     * Create an instance of {@link ConsultaRUNT }
     * 
     */
    public ConsultaRUNT createConsultaRUNT() {
        return new ConsultaRUNT();
    }

    /**
     * Create an instance of {@link Consulta }
     * 
     */
    public Consulta createConsulta() {
        return new Consulta();
    }

    /**
     * Create an instance of {@link IngresarFur }
     * 
     */
    public IngresarFur createIngresarFur() {
        return new IngresarFur();
    }

    /**
     * Create an instance of {@link Formulario }
     * 
     */
    public Formulario createFormulario() {
        return new Formulario();
    }

    /**
     * Create an instance of {@link ConsultaPinPlacaResponse }
     * 
     */
    public ConsultaPinPlacaResponse createConsultaPinPlacaResponse() {
        return new ConsultaPinPlacaResponse();
    }

    /**
     * Create an instance of {@link Generico }
     * 
     */
    public Generico createGenerico() {
        return new Generico();
    }

    /**
     * Create an instance of {@link TraerFotoResponse.TraerFotoResult }
     * 
     */
    public TraerFotoResponse.TraerFotoResult createTraerFotoResponseTraerFotoResult() {
        return new TraerFotoResponse.TraerFotoResult();
    }

    /**
     * Create an instance of {@link IngresarFurV3Response }
     * 
     */
    public IngresarFurV3Response createIngresarFurV3Response() {
        return new IngresarFurV3Response();
    }

    /**
     * Create an instance of {@link EchoResponse }
     * 
     */
    public EchoResponse createEchoResponse() {
        return new EchoResponse();
    }

    /**
     * Create an instance of {@link UtilizarPinResponse }
     * 
     */
    public UtilizarPinResponse createUtilizarPinResponse() {
        return new UtilizarPinResponse();
    }

    /**
     * Create an instance of {@link ArrayOfDatosCdasRtm }
     * 
     */
    public ArrayOfDatosCdasRtm createArrayOfDatosCdasRtm() {
        return new ArrayOfDatosCdasRtm();
    }

    /**
     * Create an instance of {@link ArrayOfSoatNacionales }
     * 
     */
    public ArrayOfSoatNacionales createArrayOfSoatNacionales() {
        return new ArrayOfSoatNacionales();
    }

    /**
     * Create an instance of {@link DatosCdasRtm }
     * 
     */
    public DatosCdasRtm createDatosCdasRtm() {
        return new DatosCdasRtm();
    }

    /**
     * Create an instance of {@link SoatNacionales }
     * 
     */
    public SoatNacionales createSoatNacionales() {
        return new SoatNacionales();
    }

}
