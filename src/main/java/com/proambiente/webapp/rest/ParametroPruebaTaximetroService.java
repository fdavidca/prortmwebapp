package com.proambiente.webapp.rest;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.proambiente.modelo.ParametroPruebaTaximetro;
import com.proambiente.webapp.dao.ParametroTaximetroFacade;


@Path("parametro_prueba_taximetro")
public class ParametroPruebaTaximetroService {

	@Inject
	ParametroTaximetroFacade parametrosPruebaTaximetroDAO;
	
	@GET
	@Consumes("application/json")
	@Produces("application/json")
	public List<ParametroPruebaTaximetro> consultarParametrosPruebaTaximetro(){		
		
		List<ParametroPruebaTaximetro> listaConfiguraciones = parametrosPruebaTaximetroDAO.findAll();
		return listaConfiguraciones;
		
	}	
	
}
