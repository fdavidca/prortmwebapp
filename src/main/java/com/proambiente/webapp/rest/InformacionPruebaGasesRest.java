package com.proambiente.webapp.rest;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.proambiente.webapp.service.InformacionPruebasGasesService;

@Path("/informacion_pruebas_diesel")
@Produces("application/json")
public class InformacionPruebaGasesRest {
	
	@Inject
	InformacionPruebasGasesService informacionPruebasGasesService;
	
	@GET
	@Path("total")
	public Long obtenerNumeroPruebasDiesel(){
		return informacionPruebasGasesService.numeroPruebasDiesel();
	}
	
	@GET
	@Path("aprobadas")
	public Long obtenerNumeroPruebasDieselAprobadas(){
		return informacionPruebasGasesService.numeroPruebasDieselAprobadas();		
	}

	@GET
	@Path("reprobadas")
	public Long obtenerNumeroPruebasDieselReprobadas(){
		return informacionPruebasGasesService.numeroPruebasDieselReprobadas();
	}
	
	@GET
	@Path("abortadas")
	public Long obtenerNumeroPruebasDieselAbortadas(){
		return informacionPruebasGasesService.numeroPruebasDieselAbortadas();
	}
	
}
