package com.proambiente.webapp.rest;

import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.proambiente.modelo.dto.HumedadTemperaturaDTO;
import com.proambiente.webapp.controller.HumedadTemperaturaBean;

@Path("termohigrometro")
public class HumedadTemperaturaRest {

	private static final Logger logger = Logger.getLogger(HumedadTemperaturaRest.class.getName());

	@Inject
	HumedadTemperaturaBean humedadTemperaturaBean;

	@POST
	@Consumes("application/json")
	public void registrarHumedad(HumedadTemperaturaDTO humedadTemperatura) {

		Double humedad = Double.valueOf(humedadTemperatura.getHumedad());
		Double temperatura = Double.valueOf(humedadTemperatura.getTemperatura());
		humedadTemperaturaBean.setHumedad(humedad);
		humedadTemperaturaBean.setTemperatura(temperatura);
		LocalDateTime horaActual = LocalDateTime.now();
		humedadTemperaturaBean.setFechaUltimaRecepcion(horaActual);

	}

	@GET
	@Produces("application/json")
	public HumedadTemperaturaDTO consultarHumedad() {
		HumedadTemperaturaDTO humedadDTO = new HumedadTemperaturaDTO();
		// verificar que el termohigrometro ha enviado datos en los ultimos 10 minutos
		LocalDateTime horaActual = LocalDateTime.now();
		LocalDateTime horaUltimoPost = humedadTemperaturaBean.getFechaUltimaRecepcion();
		if (horaUltimoPost != null) {
			LocalDateTime horaUltimoPostMas10 = horaUltimoPost.plusSeconds(10);
			if (horaUltimoPostMas10.isBefore(horaActual)) {
				humedadDTO.setHumedad("-10.0");
				humedadDTO.setTemperatura("-10.0");
				logger.log(Level.SEVERE, "Error, el termohigrometro no ha enviado medidas en los últimos 10s");

			} else {
				humedadDTO.setHumedad(String.valueOf(humedadTemperaturaBean.getHumedad()));
				humedadDTO.setTemperatura(String.valueOf(humedadTemperaturaBean.getTemperatura()));
				// humedadTemperaturaBean.setFechaUltimaRecepcion( LocalDateTime.now() );
			}
		}else {
			
			humedadDTO.setHumedad("-99.0");
			humedadDTO.setTemperatura("-99.0");
		}
		return humedadDTO;
	}

}
