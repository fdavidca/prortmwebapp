package com.proambiente.webapp.rest.test;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

import com.proambiente.webapp.service.RegistrarFurService;



@Path("/test/registrarfur/")
public class GuardarRegistroFurTest {
	
	@Inject
	RegistrarFurService registrarFurService;
		
	@GET
	@Path("guardar")
	public void registrarFur(@QueryParam("placa")String placa,
							 @QueryParam("revision_id")Integer revisionId,
							 @QueryParam("descripcion")String descripcion,
							 @QueryParam("intento")Integer intento) {
		
		registrarFurService.guardarPdf(placa, revisionId, descripcion, intento);
		
	}

}
