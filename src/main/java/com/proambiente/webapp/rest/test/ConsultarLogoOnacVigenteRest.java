package com.proambiente.webapp.rest.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import org.jboss.logmanager.Level;

import com.proambiente.modelo.LogoOnac;
import com.proambiente.webapp.controller.LogoOnacCRUDBean;
import com.proambiente.webapp.service.ConsultarLogoOnacVigenteService;


@Path("/test/consultarlogovigente/")
public class ConsultarLogoOnacVigenteRest {
	
	private static final Logger logger = Logger.getLogger(LogoOnacCRUDBean.class.getName());
	
	@Inject
	ConsultarLogoOnacVigenteService consultaLogoOnac;
		
	@GET
	@Path("logo")
	@Produces("application/json")
	public LogoOnac registrarFur(@QueryParam("fecha")String fechaStr) {
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date fecha = new Date();
		try {
			fecha = sdf.parse(fechaStr);
		} catch (ParseException e) {
			logger.log(Level.SEVERE,"No se puede convertir fecha " + fechaStr);
			e.printStackTrace();
		}
		
		LogoOnac logo = consultaLogoOnac.consultarLogoOnac(fecha);
		return logo;
	}
}
