package com.proambiente.webapp.rest.test;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.proambiente.webapp.service.RevisionMismoDiaService;

@Path("/")
public class RevisionMismoDiaServiceRest {
	
	@Inject
	RevisionMismoDiaService revisionesParaVehiculoHoy;
	
	
	@GET
	@Path("test/revisionesvehiculohoy")
	@Consumes("application/json")
	@Produces("application/json")
	public long numeroRevisionesHoyParaPlaca(@QueryParam("placa")String placa) {
		return revisionesParaVehiculoHoy.revisionesMismoDiaPorPlaca(placa);
	}

}
