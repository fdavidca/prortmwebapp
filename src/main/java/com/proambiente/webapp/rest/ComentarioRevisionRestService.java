package com.proambiente.webapp.rest;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.proambiente.webapp.service.ActualizarComentarioRevisionService;
import com.proambiente.webapp.service.ConsultarComentarioRevisionService;
import com.proambiente.webapp.util.dto.ComentarioRevisionDTO;

@Path("comentariosrevision")
public class ComentarioRevisionRestService {
	
	@Inject
	ConsultarComentarioRevisionService consultarComentariosRevision;
	
	@Inject
	ActualizarComentarioRevisionService actualizarComentarioRevision;
	
	@GET
	@Path("consultar")
	@Consumes("application/json")
	@Produces("application/json")
	public String consultarComentarioRevision(@QueryParam("revisionid") Integer revisionId){	
		
		if(revisionId  == null || revisionId < 0){
			throw new IllegalArgumentException("Revision  Id es nulo o menor que cero");
		}
		String comentarios = consultarComentariosRevision.consultarComentarioRevision(revisionId);
		return comentarios;
		
	}
	
	@POST
	@Path("actualizar")
	@Consumes("application/json")
	@Produces("application/json")
	public void actualizarComentarioRevision(ComentarioRevisionDTO comentarioDTO) {
		
		if(comentarioDTO == null) {
			throw new IllegalArgumentException("Parametros para actualizar revision no recibidos");
		}
		
		if(comentarioDTO.getRevisionId() == null || comentarioDTO.getRevisionId() < 0) {
			throw new IllegalArgumentException("Revision Id es nulo o menor que cero");			
		}
		
		String comentario = comentarioDTO.getComentario() == null ? "" : comentarioDTO.getComentario();
		actualizarComentarioRevision.actualizarComentarioRevision(comentario, comentarioDTO.getRevisionId() );
		
		
	}
	

}
