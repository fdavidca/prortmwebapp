package com.proambiente.webapp.rest;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.proambiente.modelo.Revision;
import com.proambiente.webapp.service.RevisionService;

@Path("/")
public class RevisionesPorFechaRest {
	
	
	@Inject
	RevisionService revisionService;
	
	@GET
	@Path("revisiones_por_fecha")
	@Produces("application/json")
	public List<Revision> revisionesPorFecha(@QueryParam("fecha_inicio")String fechaInicial,@QueryParam("fecha_fin")String fechaFinal){
		boolean ok = fechaInicial != null && !fechaInicial.isEmpty();
		if(!ok)
			throw new RuntimeException("Fecha inicio no especificada en los argumentos");
		
		ok = fechaFinal != null && !fechaFinal.isEmpty();
		if(!ok)
			throw new RuntimeException("Fecha final no especificada en los argumentos");
		
		Date fechaInicio = null;
		Date fechaFin = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		
		
		try {
			fechaInicio = sdf.parse(fechaInicial);			
		}catch(Exception exc) {
			throw new RuntimeException("Error procesando fecha_inicio, no se puede convertir a fecha");
		}
		
		try {
			fechaFin = sdf.parse(fechaFinal);
		}catch(Exception exc) {
			throw new RuntimeException("Error procesando fecha_fin, no se puede convertir a fecha");
		}
		//revisiones sin hacer el segundo envio		
		return revisionService.consultarRevisionesInspecciones(fechaInicio,fechaFin,false,Optional.empty());
		
	}

}
