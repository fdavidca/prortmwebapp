package com.proambiente.webapp.rest;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.proambiente.webapp.service.InformacionPruebasGasesService;


@Path("/informacion_pruebas_gasolina")
@Produces("application/json")
public class InformacionPruebasGasolinaRest {

	@Inject
	InformacionPruebasGasesService informacionPruebasGasesService;
	
	@GET
	@Path("total")
	public Long obtenerNumeroPruebasGasolina(){
		return informacionPruebasGasesService.numeroPruebasGasolina();
	}
	
	@GET
	@Path("aprobadas")
	public Long obtenerNumeroPruebasGasolinaAprobadas(){
		return informacionPruebasGasesService.numeroPruebasGasolinaAprobadas();		
	}

	@GET
	@Path("reprobadas")
	public Long obtenerNumeroPruebasGasolinaReprobadas(){
		return informacionPruebasGasesService.numeroPruebasGasolinaReprobadas();
	}
	
	@GET
	@Path("abortadas")
	public Long obtenerNumeroPruebasGasolinaAbortadas(){
		return informacionPruebasGasesService.numeroPruebasGasolinaAbortadas();
	}
	
	
	
}
