package com.proambiente.webapp.rest;

import java.sql.Timestamp;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;

import com.proambiente.modelo.Bitacora;
import com.proambiente.webapp.dao.BitacoraFacade;
import com.proambiente.webapp.util.dto.BitacoraDTO;

@Path("bitacora")
public class BitacoraRestService {
	
	@Inject 
	BitacoraFacade bitacoraDAO;
	
	@POST
	@Path("registrarevento")
	@Consumes("application/json")
	public void registrarBitacora(BitacoraDTO bitacoraDTO,@Context HttpServletRequest request) {		
		
		Bitacora bitacora = new Bitacora();	
		bitacora.setDescripcion(bitacoraDTO.getDescripcion());
		bitacora.setTipoEvento(bitacoraDTO.getTipoEvento());
		bitacora.setAccionesRealizadas( bitacoraDTO.getMedidas() );
		Timestamp fechaEvento = new Timestamp(bitacoraDTO.getFechaEvento().getTime());
		bitacora.setUsuarioId(bitacoraDTO.getUsuarioId());
		bitacora.setFechaEvento(fechaEvento);
		bitacora.setDireccionIp(request.getRemoteAddr());
		bitacoraDAO.create(bitacora);	
	}

}

