package com.proambiente.webapp.rest;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.proambiente.modelo.Pista;
import com.proambiente.webapp.dao.PistaFacade;


@Path("pista")
public class PistaRestService {

	@Inject
	PistaFacade pistaFacade;
	
	@GET
	@Consumes("application/json")
	@Produces("application/json")
	public List<Pista> consultarPistas(){	
		
		List<Pista> listaPistas = pistaFacade.findAll();
		return listaPistas;
		
	}	
	
}
