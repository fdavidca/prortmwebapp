package com.proambiente.webapp.rest;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Revision;
import com.proambiente.webapp.service.FechaService; 
import com.proambiente.webapp.service.RevisionService;

@Path("/")
public class BuscarRevision {
	
	
	private static final Logger logger = Logger.getLogger(BuscarRevision.class
			.getName());

	@Inject
	RevisionService revisionService;
	
	@Inject
	FechaService fechaService;

	@GET
	@Path("revision/{placa}")
	@Produces("application/json")
	public Revision buscarRevisionPorPlaca(@PathParam("placa") String placa) {
		Revision r = revisionService.buscarRevisionPorPlaca(placa);
		return r;
	}

	@GET
	@Path("revision/{placa}/prueba/{tipoPrueba}")
	@Produces("application/json")
	public Prueba buscarPruebaNoFinalizada(@PathParam("placa") String placa,
			@PathParam("tipoPrueba") String tipoPrueba) {
		if (placa != null && tipoPrueba != null) {
			Integer tipoPruebaId = Integer.parseInt(tipoPrueba);
			try {
				Prueba p = revisionService.buscarPruebaNoFinalizada(placa,
						tipoPruebaId);
				return p;
			} catch (Exception e) {
				logger.log(Level.SEVERE, "error consultando tipo de prueba por placa", e);
				return null;
			}

		} else {
			return null;
		}
	}
	
	@GET
	@Path("test_revision/evaluar_revision")
	@Produces("application/json")
	public String evaluarRevision(@QueryParam("revision_id") Integer revisionId){
		String resultadoEvaluacion = revisionService.evaluarRevision(revisionId);
		return resultadoEvaluacion;
	}
	
	@POST
	@Path("comentario_revision")
	public void evaluarRevision(@FormParam("revision_id") Integer revisionId,@FormParam("comentario") String comentario){
		revisionService.cambiarComentario(revisionId,comentario);
	}
	
	@GET
	@Path("revisiones_registradas_hoy")
	@Produces("application/json")
	public List<Revision> revisionesRegistradasHoy(){
		Calendar calendar = fechaService.obtenerInicioHoy();
		Date fechaInicial = calendar.getTime();
		calendar.add(Calendar.DAY_OF_MONTH, 1);
		Date fechaFinal = calendar.getTime();
		return revisionService.consultarRevisionesInspecciones(fechaInicial, fechaFinal,false,Optional.empty());
		
	}

}
