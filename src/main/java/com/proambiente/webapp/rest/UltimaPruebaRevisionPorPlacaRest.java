package com.proambiente.webapp.rest;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Revision;
import com.proambiente.webapp.service.RevisionService;

@Path("/")
public class UltimaPruebaRevisionPorPlacaRest {

	@Inject
	RevisionService revisionService;
	
	
	@GET
	@Path("ultimaprueba/{placa}/tipoprueba/{tipo_prueba}")
	public Prueba consultarUltimaPrueba(@PathParam("placa")String placa,@PathParam("tipo_prueba")Integer tipoPrueba) {
		
		if(placa ==null || placa.isEmpty()) {
			throw new IllegalArgumentException("argumento placa es vacia o nula ");
		}
		if(tipoPrueba == null) {
			throw new IllegalArgumentException("argumento tipo_prueba es nulo");
		}		
		Revision r = revisionService.buscarRevisionPorPlaca(placa);
		if(r != null) {
			Prueba prueba = revisionService.buscarPruebaPorTipoPorRevision(r.getRevisionId(), tipoPrueba);
			return prueba;
		}else		
			return null;
		
	}
	
}
