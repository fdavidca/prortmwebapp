package com.proambiente.webapp.rest;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.proambiente.modelo.Equipo;
import com.proambiente.webapp.dao.EquipoFacade;


@Path("equipo")
public class EquipoRestService {

	@Inject
	EquipoFacade equipoDAO;
	
	@GET
	@Path("equipoportipo")
	@Consumes("application/json")
	@Produces("application/json")
	public List<Equipo> consultarEquipoPorTipo(@QueryParam("tipo_equipo") Integer tipoEquipo){	
		
		if(tipoEquipo  == null || tipoEquipo < 0){
			throw new IllegalArgumentException("Tipo Equipo Id es nulo o menor que cero");
		}
		List<Equipo> listaEquipos = equipoDAO.consultarEquipoPorTipo(tipoEquipo);
		return listaEquipos;
		
	}	
	
}
