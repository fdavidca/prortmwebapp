package com.proambiente.webapp.rest;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.proambiente.modelo.InformacionCda;
import com.proambiente.webapp.dao.InformacionCdaFacade;

@Path("informacioncda")
public class InformacionCDARest {
	
	@Inject
	InformacionCdaFacade informacionCdaDAO;
	
	
	@GET
	@Produces("application/json")
	@Path("info")
	public InformacionCda obtenerInformacionCDA(){
		
		return informacionCdaDAO.find(1);
		
	}

}
