package com.proambiente.webapp.rest;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import com.google.common.base.Preconditions;
import com.proambiente.modelo.Ciudad;
import com.proambiente.modelo.ClaseVehiculo;
import com.proambiente.modelo.Color;
import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.LineaVehiculo;
import com.proambiente.modelo.Marca;
import com.proambiente.modelo.Propietario;
import com.proambiente.modelo.Servicio;
import com.proambiente.modelo.TipoCombustible;
import com.proambiente.modelo.TipoDocumentoIdentidad;
import com.proambiente.modelo.TipoVehiculo;
import com.proambiente.modelo.Usuario;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.webapp.controller.OpcionesRevisionBean;
import com.proambiente.webapp.controller.VehiculoDefault;
import com.proambiente.webapp.dao.CiudadFacade;
import com.proambiente.webapp.dao.ColorFacade;
import com.proambiente.webapp.dao.DepartamentoFacade;
import com.proambiente.webapp.dao.InformacionCdaFacade;
import com.proambiente.webapp.dao.LineaVehiculoFacade;
import com.proambiente.webapp.dao.MarcaFacade;
import com.proambiente.webapp.dao.PropietarioFacade;
import com.proambiente.webapp.dao.TipoCombustibleFacade;
import com.proambiente.webapp.dao.TipoDocumentoIdentidadFacade;
import com.proambiente.webapp.dao.TipoVehiculoFacade;
import com.proambiente.webapp.dao.UsuarioFacade;
import com.proambiente.webapp.dao.VehiculoFacade;
import com.proambiente.webapp.util.dto.RegistroPropietarioDTO;
import com.proambiente.webapp.util.dto.RegistroVehiculoDTO;

@Path("ingreso_vehiculo")
public class IngresoVehiculoRestService {

	private static final Logger logger = Logger.getLogger(IngresoVehiculoRestService.class.getName());

	@Inject
	InformacionCdaFacade informacionCdaDAO;
	
	@Inject
	VehiculoFacade vehiculoDAO;
	
	@Inject
	LineaVehiculoFacade lineaDAO;

	@Inject
	@VehiculoDefault
	Vehiculo vehiculoPorDefecto;// es singleton o es uno por cada peticion

	@Inject
	TipoCombustibleFacade tipoCombustibleDAO;

	@Inject
	MarcaFacade marcaDAO;

	@Inject
	OpcionesRevisionBean opcionesRevision;

	@Inject
	ColorFacade colorFacade;

	@Inject
	TipoVehiculoFacade tipoVehiculoFacade;

	@Inject
	UsuarioFacade usuarioFacade;

	@Inject
	PropietarioFacade propietarioDAO;

	@Inject
	DepartamentoFacade departamentoDAO;

	@Inject
	CiudadFacade ciudadDAO;

	@Inject
	TipoDocumentoIdentidadFacade tipoDocumentoIdentidadDAO;

	@GET
	public Response testServicio() {
		System.out.println("Respuesta ok");
		return Response.ok().build();
	}

	@POST
	@Path("registrar_vehiculo")
	@Consumes("application/json")
	public Response registrarVehiculo(RegistroVehiculoDTO registroVehiculo) {
		try {
			
			logger.log(Level.INFO, "Datos recibidos : " + registroVehiculo );
			Preconditions.checkNotNull(registroVehiculo.getPlaca(), "Placa del vehiculo no presente");

			Vehiculo vehiculoNuevo = new Vehiculo();
			vehiculoNuevo.setPlaca(registroVehiculo.getPlaca().toUpperCase());

			Preconditions.checkArgument(registroVehiculo.getModelo() != null, "Modelo del vehiculo no presente");
			vehiculoNuevo.setModelo(registroVehiculo.getModelo());

			String numeroMotor = registroVehiculo.getNumeroMotor() == null ? "" : registroVehiculo.getNumeroMotor();
			vehiculoNuevo.setNumeroMotor(numeroMotor);

			String vin = registroVehiculo.getVin() == null ? "" : registroVehiculo.getVin();
			vehiculoNuevo.setVin(vin);

			String numeroSerie = registroVehiculo.getNumeroSerie() == null ? "" : registroVehiculo.getNumeroSerie();
			vehiculoNuevo.setNumeroSerie(numeroSerie);

			String numeroChasis = registroVehiculo.getNumeroChasis() == null ? "" : registroVehiculo.getNumeroChasis();

			String numeroLicencia = registroVehiculo.getNumeroLicencia() == null ? ""
					: registroVehiculo.getNumeroLicencia();
			if(!numeroLicencia.trim().isEmpty()) {
				vehiculoNuevo.setNumeroLicencia(numeroLicencia);
			}
			
			
			
			Boolean infoIdTipoVehiculoOk = false;
			Integer tipoVehiculoId = registroVehiculo.getTipoVehiculoId();
			
			if(tipoVehiculoId != null) {
				TipoVehiculo tv = tipoVehiculoFacade.find(tipoVehiculoId);
				if(tv != null) {
					vehiculoNuevo.setTipoVehiculo(tv);
					infoIdTipoVehiculoOk = true;
				}
			}
			
			Integer idTipoCombustible = registroVehiculo.getIdTipoCombustible();
			Boolean combustibleInfoOk = false;
			if(idTipoCombustible != null) {
				TipoCombustible tipoCombustible = tipoCombustibleDAO.find(idTipoCombustible);
				if(tipoCombustible != null) {
					vehiculoNuevo.setTipoCombustible(tipoCombustible);
					combustibleInfoOk = true;
				}
			}

			if(!combustibleInfoOk) {
				String combustible = registroVehiculo.getCombustible();
				Preconditions.checkNotNull(combustible,"combustible no presente");
				if (combustible == null || combustible.isEmpty()) {
					vehiculoNuevo.setTipoCombustible(vehiculoPorDefecto.getTipoCombustible());
				} else {
					List<TipoCombustible> tiposCombustible = tipoCombustibleDAO.findAll();
					Optional<TipoCombustible> optCombustible = tiposCombustible.stream()
							.filter(tc -> tc.getNombre().equalsIgnoreCase(combustible)).findAny();
					if (optCombustible.isPresent()) {
						vehiculoNuevo.setTipoCombustible(optCombustible.get());
					} else {
						vehiculoNuevo.setTipoCombustible(vehiculoPorDefecto.getTipoCombustible());
					}
				}
			}
			Marca m = null;
			
			Integer marcaId = registroVehiculo.getIdMarca();
			Boolean marcaEncontradaPorId = false;
			if(marcaId != null) {
				Marca mAux = marcaDAO.find(marcaId);
				if(mAux != null) {
					vehiculoNuevo.setMarca(mAux);
					marcaEncontradaPorId = true;
				}
				
			}
			
			String marca = registroVehiculo.getMarca();

			if(!marcaEncontradaPorId) {
				
				if (marca == null || marca.isEmpty()) {
					vehiculoNuevo.setMarca(vehiculoPorDefecto.getMarca());
				} else {
					List<Marca> marcas = marcaDAO.findMarcasByNombre(marca);
					if (marcas == null || marcas.isEmpty()) {
						vehiculoNuevo.setMarca(vehiculoPorDefecto.getMarca());
					} else {
						m = marcas.get(0);
						vehiculoNuevo.setMarca(m);
						String linea = registroVehiculo.getLinea();
					}
				}

			}
			
			
			
			Integer idLinea = registroVehiculo.getIdLinea();
			Boolean lineaEncontradaPorId = false;
			if(idLinea != null) {
				LineaVehiculo linea = lineaDAO.find(idLinea);
				if(linea != null) {
					vehiculoNuevo.setLineaVehiculo( linea );
					lineaEncontradaPorId = true;
				}
			}
			if( !lineaEncontradaPorId ) {
				String linea = registroVehiculo.getLinea();
				if (linea == null || linea.isEmpty()) {
					vehiculoNuevo.setLineaVehiculo(vehiculoPorDefecto.getLineaVehiculo());
				} else {
					if (marca != null) {
						List<LineaVehiculo> lineas = marcaDAO.findLineaVehiculosByMarca(m);
						Optional<LineaVehiculo> optLinea = lineas.stream()
								.filter(l -> l.getNombre().equalsIgnoreCase(linea)).findAny();
						if (optLinea.isPresent()) {
							LineaVehiculo lineaVehiculo = optLinea.get();
							vehiculoNuevo.setLineaVehiculo(lineaVehiculo);
						} else {
							vehiculoNuevo.setLineaVehiculo(vehiculoPorDefecto.getLineaVehiculo());
						}
					} else {
						vehiculoNuevo.setLineaVehiculo(vehiculoPorDefecto.getLineaVehiculo());
					}
				}
			}
			

			// Servicio
			Boolean infoIdServicioOk = false;
			Integer servicioId = registroVehiculo.getIdTipoServicio();
			
			if(servicioId != null) {
				Integer servicioIdPrortm = transformarServicio(servicioId);
				Optional<Servicio> optServicio = opcionesRevision.getListaServicios().stream().filter( s-> s.getServicioId().equals(servicioIdPrortm) ).findAny();
				if(optServicio.isPresent()) {
					infoIdServicioOk = true;
					vehiculoNuevo.setServicio(optServicio.get());
				}				
			}
			
			if(!infoIdServicioOk) {
			
				String servicio = registroVehiculo.getServicio();
				Preconditions.checkNotNull(servicio,"servicio no presente en la peticion");
				
				
				
				if (servicio == null || servicio.isEmpty()) {
					vehiculoNuevo.setServicio(vehiculoPorDefecto.getServicio());
				} else {
					Stream<Servicio> strServicio = opcionesRevision.getListaServicios().stream();
					Optional<Servicio> optServicio = strServicio.filter(s -> s.getNombre().equalsIgnoreCase(servicio))
							.findFirst();
					if (optServicio.isPresent()) {
						vehiculoNuevo.setServicio(optServicio.get());
					} else {
						vehiculoNuevo.setServicio(vehiculoPorDefecto.getServicio());
					}
				}
			}

			// Clase
			Integer claseId = registroVehiculo.getIdClaseVehiculo();
			Boolean infoIdClaseOk = false;
			if(claseId != null) {
				Optional<ClaseVehiculo> optClase = opcionesRevision.getListaClases().stream().filter(c->c.getClaseVehiculoId().equals(claseId)).findAny();
				if(optClase.isPresent()) {
					vehiculoNuevo.setClaseVehiculo( optClase.get());
					
					infoIdClaseOk = true;
					if(vehiculoNuevo.getTipoVehiculo() == null) {//arriba se debio establecer
						TipoVehiculo tipoVehiculo = getTipoVehiculoPorClase(optClase.get());
						vehiculoNuevo.setTipoVehiculo(tipoVehiculo);
					}
				}
			}
			
			if(!infoIdClaseOk) {
				String clase = registroVehiculo.getClase();
				Preconditions.checkNotNull(clase,"clase no presente en la peticion");
				if (clase == null || clase.isEmpty()) {
					vehiculoNuevo.setClaseVehiculo(vehiculoPorDefecto.getClaseVehiculo());
					vehiculoNuevo.setTipoVehiculo( vehiculoPorDefecto.getTipoVehiculo() );
				} else {
					Stream<ClaseVehiculo> streamClase = opcionesRevision.getListaClases().stream();
					Optional<ClaseVehiculo> optClase = streamClase.filter(c -> c.getNombre().equalsIgnoreCase(clase))
							.findFirst();
					if (optClase.isPresent()) {
						ClaseVehiculo claseVehiculo = optClase.get();
						vehiculoNuevo.setClaseVehiculo(claseVehiculo);
						if(registroVehiculo.getTipoVehiculoId() != null) {
							try {
								TipoVehiculo tipoVehiculo = tipoVehiculoFacade.find(registroVehiculo.getTipoVehiculoId());
								vehiculoNuevo.setTipoVehiculo(tipoVehiculo);
							}catch(Exception exc) {
								vehiculoNuevo.setClaseVehiculo(vehiculoPorDefecto.getClaseVehiculo());
								vehiculoNuevo.setTipoVehiculo( vehiculoPorDefecto.getTipoVehiculo() );
							}
						}else {
							TipoVehiculo tipoVehiculo = getTipoVehiculoPorClase(claseVehiculo);
							vehiculoNuevo.setTipoVehiculo(tipoVehiculo);
						}
						
						
						

					} else {
						vehiculoNuevo.setClaseVehiculo(vehiculoPorDefecto.getClaseVehiculo());
						vehiculoNuevo.setTipoVehiculo(vehiculoPorDefecto.getTipoVehiculo());
					}
				}

			}
			
			Integer idColor = registroVehiculo.getIdColor();
			Boolean colorEncontradoPorId = false;
			if(idColor != null) {
				Color color = colorFacade.find(idColor);
				if(color != null) {
					vehiculoNuevo.setColor(color);
					colorEncontradoPorId = true;
				}
			}
			
			if(!colorEncontradoPorId) {
				// Color
				String color = registroVehiculo.getColor();
				if (color == null || color.isEmpty()) {
					vehiculoNuevo.setColor(vehiculoPorDefecto.getColor());
				} else {
					List<Color> colores = colorFacade.findColoresByNombre(color);
					if (colores == null || colores.isEmpty()) {
						vehiculoNuevo.setColor(vehiculoPorDefecto.getColor());
					} else {
						vehiculoNuevo.setColor(colores.get(0));
					}

				}				
			}
			
			

			Usuario usuario = usuarioFacade.find(11);
			vehiculoNuevo.setUsuario(usuario);

			String cilindrajeStr = registroVehiculo.getCilindraje();
			try {
				int cilindraje = Integer.parseInt(cilindrajeStr);
				vehiculoNuevo.setCilindraje(cilindraje);
				if(cilindraje < 0 ) {
					throw new IllegalArgumentException("Cilindraje no puede ser menor a 0");
				}
			} catch (NumberFormatException exc) {
				logger.log(Level.INFO, "Cilindraje no es un numero valido, se asigna uno por defecto");
				vehiculoNuevo.setCilindraje(1);
			}

			
			Optional<Date> fechaMatriculaOpt = encontrarFechaMatricula(registroVehiculo);
			if(fechaMatriculaOpt.isPresent()) {
				vehiculoNuevo.setFechaMatricula( new Timestamp(fechaMatriculaOpt.get().getTime()));
			}
			
			String numeroSillas = registroVehiculo.getNumeroSillas();
			if(numeroSillas == null || numeroSillas.isEmpty()) {
				vehiculoNuevo.setNumeroSillas(2);
			}else {
				try {
					int numeroSillasInt = Integer.parseInt(numeroSillas);
					vehiculoNuevo.setNumeroSillas(numeroSillasInt);					
				}catch(NumberFormatException nexc) {					
					logger.log(Level.INFO, "numero de sillas no es un numero valido");
					vehiculoNuevo.setNumeroSillas(2);
				}				
			}
			
			String potenciaStr = registroVehiculo.getPotencia();
			if(potenciaStr == null || potenciaStr.isEmpty()) {
				
			}else {
				try {
					int potencia = Integer.parseInt(potenciaStr);
					vehiculoNuevo.setPotencia(potencia);
				}catch(NumberFormatException nexc) {
					logger.log(Level.INFO, "potencia no es un numero valido : " + potenciaStr);
				}
			}
			
			String tiemposMotorStr = registroVehiculo.getTiemposMotor();
			if(tiemposMotorStr == null || tiemposMotorStr.isEmpty()) {
				vehiculoNuevo.setTiemposMotor(4);
			}else {
				try {
					int tiemposMotor = Integer.parseInt(tiemposMotorStr);
					vehiculoNuevo.setTiemposMotor(tiemposMotor);
				}catch(NumberFormatException nexc) {
					logger.log(Level.INFO, "tiempos motor no es un numero valido : " + tiemposMotorStr);
					vehiculoNuevo.setTiemposMotor(4);
				}
			}

			// Otras opciones por defecto
			if( registroVehiculo.getConversionGNV()!=null && !registroVehiculo.getConversionGNV().isEmpty() ) {
				vehiculoNuevo.setConversionGnv( registroVehiculo.getConversionGNV() );
				if(registroVehiculo.getConversionGNV().equalsIgnoreCase("SI") || 
						registroVehiculo.getConversionGNV().equalsIgnoreCase("NO")) {
					
					if(registroVehiculo.getFechaConversionGNV()!=null && !registroVehiculo.getFechaConversionGNV().isEmpty()) {
						
						SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

						try {
							
							Date date1 = formatter.parse(registroVehiculo.getFechaConversionGNV());
							vehiculoNuevo.setFechaVenicimientoGNV(new Timestamp(date1.getTime()));
							
						}catch(Exception exc){
							System.out.println("Error parseando fecha" + registroVehiculo.getFechaConversionGNV());
						}
						
					}
					
				}
			}else {
				vehiculoNuevo.setConversionGnv("NO");
			}
			vehiculoNuevo.setNumeroEjes(2);
			vehiculoNuevo.setNumeroExostos(1);			
			vehiculoNuevo.setFechaSoat(new Timestamp(new Date().getTime()));
			vehiculoNuevo.setFechaExpiracionSoat(new Timestamp(new Date().getTime()));
			vehiculoNuevo.setDiseno("CONVENCIONAL");
			vehiculoNuevo.setPais(vehiculoPorDefecto.getPais());			
			vehiculoNuevo.setBlindaje(registroVehiculo.isBlindado());
			vehiculoNuevo.setVidriosPolarizados(registroVehiculo.isVidriosPolarizados());
			
			//por retrocompatilibdad ?
			
			String kilometrajeStr = registroVehiculo.getKilometraje();				
			vehiculoNuevo.setKilometraje(kilometrajeStr == null ? "":kilometrajeStr);			
			
			vehiculoNuevo.setNumeroPasajeros( obtenerPasajerosSentados( registroVehiculo ));
			copiarCamposSoat(vehiculoNuevo,registroVehiculo);
			
			

			
			
			vehiculoDAO.edit(vehiculoNuevo);
			
			

			RegistroPropietarioDTO regPropietario = registroVehiculo.getPropietario();
			Preconditions.checkNotNull(regPropietario, "no hay registro de propietario");
			
			Preconditions.checkNotNull(regPropietario.getNumeroDocumento(),"No hay numero de documento");
			Preconditions.checkNotNull(regPropietario.getTel(),"No hay telefono uno");
			Preconditions.checkNotNull(regPropietario.getNombres(),"No hay nombres");
			Preconditions.checkNotNull(regPropietario.getApellidos(),"No hay apellidos");
			registrarPropietario(regPropietario,usuario);
			
			RegistroPropietarioDTO regConductor = registroVehiculo.getConductor();
			if( regConductor == null || regConductor.getNumeroDocumento()==null || regConductor.getNumeroDocumento().isEmpty() ) {
				
			}else {
				registrarPropietario(regConductor,usuario);
			}
			
		}catch(IllegalArgumentException  | NullPointerException nexc) {
		  logger.log(Level.SEVERE,"Error ",nexc);
		  return Response
			      .status(Response.Status.BAD_REQUEST)
			      .entity(nexc.getMessage())
			      .build();
		}	
		catch (Exception exc) {
			logger.log(Level.SEVERE, "Error registrando vehiculo", exc);
			return Response
				      .status(Response.Status.INTERNAL_SERVER_ERROR)
				      .entity("Error registrando vehiculo " + exc.getMessage())
				      .build();
		}
	    return Response
			      .status(Response.Status.OK)
			      .entity("Vehiculo creado")
			      .build();
	}


	private void copiarCamposSoat(Vehiculo vehiculoNuevo, RegistroVehiculoDTO registroVehiculo) {
		
		String numeroSoat = registroVehiculo.getNumeroSoat();
		if(numeroSoat != null && ! numeroSoat.isEmpty()) {
			vehiculoNuevo.setNumeroSoat(numeroSoat);
		}
		String fechaSoat = registroVehiculo.getFechaSoat();
		if(fechaSoat != null && !fechaSoat.isEmpty()) {
			SimpleDateFormat parser = new SimpleDateFormat("dd/MM/yyyy");
	        try {
				Date formattedDate = parser.parse(fechaSoat);
				vehiculoNuevo.setFechaSoat( new Timestamp(formattedDate.getTime()));
			} catch (ParseException e) {
				System.out.println("Error de conversion de la fecha: " + fechaSoat);
			}
		}
		
	}

	private Integer obtenerPasajerosSentados(RegistroVehiculoDTO registroVehiculo) {
		if(registroVehiculo.getNumeroPasajerosSentados() != null) {
			return registroVehiculo.getNumeroPasajerosSentados();
		}else {
			return 1;
		}
	}

	private Optional<Date> encontrarFechaMatricula(RegistroVehiculoDTO registroVehiculo) {
		String fechaMatriculaStr = registroVehiculo.getFechaMatricula();
		if(fechaMatriculaStr != null) {
			SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd");
	        try {
				Date formattedDate = parser.parse(fechaMatriculaStr);
				return Optional.of(formattedDate);
			} catch (ParseException e) {
				return Optional.empty();
			}
		}else {
			return Optional.empty();
		}
	}

	private void registrarPropietario(RegistroPropietarioDTO regPropietario,Usuario usuario) {
		
		
		String tipoDocumentoStr = regPropietario.getTipoDocumento();
		String numeroDocumento = regPropietario.getNumeroDocumento();

		Preconditions.checkNotNull(numeroDocumento, "Numero de documento no presente");
		Long documento = 0l;
		try {
			documento = Long.parseLong(numeroDocumento);
		} catch (NumberFormatException nexc) {
			logger.log(Level.INFO, "Documento no se puede leer bien");
		}
		
		

		String nombres = regPropietario.getNombres();
		String apellidos = regPropietario.getApellidos();
		String telefono1 = regPropietario.getTel();
		String telefono2Str = regPropietario.getTel2();
		String direccion = regPropietario.getDireccion();
		String departamentoStr = regPropietario.getDepartamento();
		String ciudadStr = regPropietario.getCiudad();
		String correoStr = regPropietario.getCorreo();

		if (documento > 0) {
			Propietario propietario = propietarioDAO.find(documento);
		
			
			// Si el propietario ya existe en la base de datos
			if (propietario != null && propietario.getPropietarioId() != null) {
				propietario.setDireccion(direccion);
				propietario.setCorreoElectronico(correoStr);
				propietario.setTelefono1(telefono1);
				try {
					Long.parseLong(telefono2Str);
					propietario.setTelefono2(telefono2Str);
				}catch(NumberFormatException nexc) {						
					logger.log(Level.INFO, "Telefono 2 no se reconoce como un numero");
					propietario.setTelefono2("");
				}
			} else {// el propietario no existe en la base de datos

				propietario = new Propietario();
				propietario.setPropietarioId(documento);
				propietario.setNombres(nombres);
				propietario.setApellidos(apellidos);
				propietario.setDireccion(direccion);
				propietario.setCorreoElectronico(correoStr);
				propietario.setTelefono1(telefono1);
				try {
					Long.parseLong(telefono2Str);
					propietario.setTelefono2(telefono2Str);
				}catch(NumberFormatException nexc) {						
					logger.log(Level.INFO, "Telefono 2 no se reconoce como un numero");
					propietario.setTelefono2("");
				}					
				int tipoDocumentoInt = 1;
				try {
					tipoDocumentoInt = Integer.parseInt(tipoDocumentoStr);
				} catch (NumberFormatException nexc) {
					logger.log(Level.INFO, "Error tipo documento identidad");
				}
				TipoDocumentoIdentidad tipoDocumento = transformarTipoDocumentoIdentidad(tipoDocumentoInt);
				propietario.setTipoDocumentoIdentidad(tipoDocumento);
				int departamentoId = -1;
				try {
					departamentoId = Integer.valueOf(departamentoStr);
				} catch (NumberFormatException nexc) {
					logger.log(Level.INFO, "Id de departamento no convertible en numero");
				}
				//si la ciudad es null poner la ciudad registrada en la informacion de cda
				Optional<Ciudad> ciudadOpt = ciudadStr != null  ? ciudadDAO.buscarCiudadPorNombre(ciudadStr): Optional.empty();
				if(ciudadOpt.isPresent()) {
					propietario.setCiudad(ciudadOpt.get());
				}
				else {
					InformacionCda infoCda = informacionCdaDAO.find(1);
					Optional<Ciudad> ciudadCdaOpt = ciudadDAO.buscarCiudadPorNombre(infoCda.getCiudad());
					if(ciudadCdaOpt.isPresent()) {
						propietario.setCiudad(ciudadCdaOpt.get());
					}else {
						Ciudad ciudadPorDefecto = new Ciudad();
						ciudadPorDefecto.setCiudadId(11001);
						propietario.setCiudad(ciudadPorDefecto);
					}								
				}
			}
			propietario.setUsuario(usuario);
			propietarioDAO.edit(propietario);
		}
		
		
	}


	
	public String valorGnvPorDefecto(Vehiculo vehiculo) {
		String gnv = vehiculo.getConversionGnv();
		if( gnv == null || gnv.isEmpty() ) {
			gnv = "N/A";
		}
		return gnv;		
	}

	public TipoDocumentoIdentidad transformarTipoDocumentoIdentidad(int tipoDocumentoExterno) {
		switch (tipoDocumentoExterno) {
		case 1:
			return tipoDocumentoIdentidadDAO.find(1);
		case 2:
			return tipoDocumentoIdentidadDAO.find(4);
		case 3:
			return tipoDocumentoIdentidadDAO.find(5);
		case 4:
			return tipoDocumentoIdentidadDAO.find(3);
		default:
			return tipoDocumentoIdentidadDAO.find(1);
		}
	}
	
	private Integer transformarServicio(int idServicio) {
		switch(idServicio) {
		case 1:
			return 3;
		case 2:
			return 2;
		case 3:
			return 4;
		case 4:
			return 1;
			default :
				return 1;
			
		}
	}

	public TipoVehiculo getTipoVehiculoPorClase(ClaseVehiculo clase) {
		String nombreClase = clase.getNombre();

		switch (nombreClase) {
		case "AUTOMOVIL":
			return tipoVehiculoFacade.find(1);

		case "MOTOCICLETA":
			return tipoVehiculoFacade.find(4);
		case "BUS":
			return tipoVehiculoFacade.find(3);
		case "BUSETA":
			return tipoVehiculoFacade.find(3);
		case "CAMION":
			return tipoVehiculoFacade.find(1);
		case "CAMIONETA":
			return tipoVehiculoFacade.find(1);
		case "CAMPERO":
			return tipoVehiculoFacade.find(1);
		case "MICROBUS":
			return tipoVehiculoFacade.find(1);
		case "TRACTOCAMION":
			return tipoVehiculoFacade.find(3);
		case "MOTOCARRO":
			return tipoVehiculoFacade.find(5);
		case "VOLQUETA":
			return tipoVehiculoFacade.find(3);
		default:
			return tipoVehiculoFacade.find(1);
		}
	}

}
