package com.proambiente.webapp.rest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import com.proambiente.webapp.dao.FotoFacade;

@Path("/upload")
public class LogTaximetroService {
	
	
	private static final Logger logger = Logger.getLogger(LogTaximetroService.class
			.getName());
	
	@Inject
	FotoFacade fotoDAO;
	
	private final String UPLOADED_FILE_PATH;
	
	public LogTaximetroService(){
		String temp = System.getenv("TEMP");		
		if(temp != null){
			temp = temp + "\\";
			UPLOADED_FILE_PATH = temp;
		}
		else{ 
			UPLOADED_FILE_PATH = "c:/temp/";
		}
	}
	
	@POST
	@Path("/logs/{pruebaId}")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response uploadFile(@PathParam("pruebaId") Integer pruebaId,MultipartFormDataInput input)throws IOException{
		String filename = "";
		Map<String, List<InputPart>> formParts = input.getFormDataMap();
		filename = formParts.get("filename").get(0).getBodyAsString();
		 List<InputPart> inputParts = formParts.get("file");
		 for(InputPart inputPart:inputParts){
			 try{
				 
				 MultivaluedMap<String, String> header = inputPart.getHeaders();				 
				 InputStream inputStream = inputPart.getBody(InputStream.class, null);
				 byte[] bytes = IOUtils.toByteArray(inputStream);
				 filename = UPLOADED_FILE_PATH + filename;
	             writeFile(bytes, filename);            
	                   
				logger.log(Level.INFO, "Archivo Correctamente subido");
				 
			 }catch(Exception exc){
				 logger.log(Level.SEVERE, "Error subiendo foto 1", exc);				 
				 return Response.serverError().build();
			 }
		 }
		 return Response.status(200)
	                .entity("Uploaded file name : "+ filename).build();
		
	}
	
	 private void writeFile(byte[] content, String filename) throws IOException 
	    {
	        File file = new File(filename);
	        if (!file.exists()) {
	            file.createNewFile();
	        }
	        FileOutputStream fop = new FileOutputStream(file);
	        fop.write(content);
	        fop.flush();
	        fop.close();
	    }
	
	
	 
}
