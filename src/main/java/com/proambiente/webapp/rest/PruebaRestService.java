package com.proambiente.webapp.rest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;

import com.proambiente.modelo.Analizador;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Revision_;
import com.proambiente.modelo.VerificacionGasolina;
import com.proambiente.modelo.VerificacionLinealidad;
import com.proambiente.modelo.dto.DefectoDTO;
import com.proambiente.modelo.dto.MedidaDTO;
import com.proambiente.modelo.dto.PruebaDTO;
import com.proambiente.modelo.dto.ReportePruebaDTO;
import com.proambiente.webapp.dao.AnalizadorFacade;
import com.proambiente.webapp.dao.EquipoFacade;
import com.proambiente.webapp.dao.PruebaFacade;
import com.proambiente.webapp.dao.VerificacionGasolinaFacade;
import com.proambiente.webapp.dao.VerificacionLinealidadFacade;
import com.proambiente.webapp.service.PruebasService;
import com.proambiente.webapp.service.RegistrarPruebaRealizadaService;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.service.VehiculoService;
import com.proambiente.webapp.service.indra.UtilIndraEquipos;
import com.proambiente.webapp.service.notification.NotificationPrimariaService;
import com.proambiente.webapp.service.sicov.GeneradorInfoEventoRTMEC;
import com.proambiente.webapp.service.sicov.OperacionesSicov;
import com.proambiente.webapp.service.sicov.OperacionesSicovProducer;



@Path("/")
public class PruebaRestService {

	private static final Logger logger = Logger.getLogger(PruebaRestService.class.getName());

	@Inject
	PruebaFacade pruebaDAO;

	@Inject
	PruebasService pruebaService;

	@Inject
	RevisionService revisionService;
		
	@Inject
	AnalizadorFacade analizadorDAO;

	@Inject
	VerificacionGasolinaFacade verificacionGasolinaDAO;

	@Inject
	VerificacionLinealidadFacade verificacionLinealidadDAO;

	@Inject
	RegistrarPruebaRealizadaService registrarPruebaRealizadaService;

	@Inject
	NotificationPrimariaService notificationService;


	@Inject
	OperacionesSicovProducer operacionesSicovProducer;

	OperacionesSicov operacionesSicov;

	@Inject
	VehiculoService vehiculoService;
	
	@Inject
	EquipoFacade equipoService;
	
	@Inject
	UtilIndraEquipos utilIndraEquipos;

	@Resource
	ManagedExecutorService executor;
	
	@Inject
	GeneradorInfoEventoRTMEC generadorInfoEvntoRTMEC;

	@PUT
	@Path("prueba/{pruebaId}")
	@Consumes("application/json")
	@Produces("application/json")
	public Boolean actualizarPrueba(@PathParam("pruebaId") Integer pruebaId, PruebaDTO p) {
		// System.out.println("Prueba Id:" + pruebaId);
		// System.out.println("Prueba" + p.toString());
		pruebaDAO.actualizarPrueba(pruebaId, p);
		return Boolean.TRUE;
	}

	@POST
	@Path("prueba/infoadicionalotto")
	public Boolean registrarInfoAdicionalOtto(@FormParam("serial") String serial,
			@FormParam("verificacion_gasolina_id") Integer verificacionGasolinaId,
			@FormParam("metodo_temperatura") String metodoTemperatura, @FormParam("prueba_id") Integer pruebaId) {
		// System.out.println("Prueba:" + pruebaId);
		// System.out.println("Analizador" + serial);
		// System.out.println("VerificacionGasolinaId" +
		// verificacionGasolinaId);
		serial = serial.trim();// algunos casos es necesario por ejemplo
								// analizadores capelec
		Prueba prueba = pruebaDAO.find(pruebaId);
		// System.out.println("Prueba cargada");
		if (metodoTemperatura != null && !metodoTemperatura.isEmpty())
			prueba.setMetodoTemperatura(metodoTemperatura);
		Analizador analizador = analizadorDAO.find(serial);
		prueba.setAnalizador(analizador);
		VerificacionGasolina verificacion = verificacionGasolinaDAO.find(verificacionGasolinaId);
		prueba.setVerificacionGasolina(verificacion);
		pruebaDAO.edit(prueba);
		return Boolean.TRUE;
	}

	@POST
	@Path("prueba/infoadicionaldiesel")
	public Boolean registrarInfoAdicionalDiesel(@FormParam("serial") String serial,
			@FormParam("verificacion_linealidad_id") Integer verificacionLinealidadId,
			@FormParam("metodo_temperatura") String metodoTemperatura, @FormParam("prueba_id") Integer pruebaId) {
		// System.out.println("Prueba:" + pruebaId);
		// System.out.println("Analizador" + serial);
		// System.out.println("VerificacionGasolinaId" +
		// verificacionLinealidadId);
		Prueba prueba = pruebaDAO.find(pruebaId);
		logger.info("Prueba cargada");
		if (metodoTemperatura != null && !metodoTemperatura.isEmpty())
			prueba.setMetodoTemperatura(metodoTemperatura);
		Analizador analizador = analizadorDAO.find(serial);
		prueba.setAnalizador(analizador);
		VerificacionLinealidad verificacion = verificacionLinealidadDAO.find(verificacionLinealidadId);
		prueba.setVerificacionLinealidad(verificacion);
		pruebaDAO.edit(prueba);
		return Boolean.TRUE;
	}

	@POST
	@Path("prueba/registrarfechainicio")

	public void registrarFechaInicioPrueba(@FormParam("pruebaid") Integer pruebaId,
			@FormParam("pistaid") Integer pistaId, @FormParam("revisionid") Integer revisionId,
			@FormParam("seriales") String serialesEquipos,@FormParam("analizador")String  serialAnalizador) {
		if (pruebaId == null)
			throw new IllegalArgumentException("pruebaid es null");
		if (pistaId == null)
			throw new IllegalArgumentException("pistaid es null");
		logger.info("Prueba Fecha Inicio:" + pruebaId);
		pruebaService.registrarFechaInicioPrueba(pruebaId);
		executor.submit(() -> {
			try {
				Revision revision = revisionService.cargarRevisionDTO(revisionId, Revision_.fechaCreacionRevision,Revision_.preventiva);
				if(revision.getPreventiva()){					
					logger.log(Level.INFO, "Revision es preventiva " + revisionId);
					return;
				}
				operacionesSicov = operacionesSicovProducer.crearOperacionSicov();
				generadorInfoEvntoRTMEC.setOperacionesSicov(operacionesSicov);
				//notificationService.sendNotification(" Da inicio prueba : " + pruebaId, NivelAlerta.INFORMACION,
				//		Boolean.FALSE);
				
				Prueba prueba = pruebaDAO.find(pruebaId);
				List<String> listaSerialesEquipo = new ArrayList<>();
				if(serialAnalizador != null && !serialAnalizador.isEmpty()){
					Analizador analizador = new Analizador();
					analizador.setSerial(serialAnalizador);
					prueba.setAnalizador(analizador);
				}
				
				if(serialesEquipos != null && !serialesEquipos.isEmpty()){
					String[] parseada = serialesEquipos.split(";");
					listaSerialesEquipo = Arrays.asList(parseada);
				}
				
				generadorInfoEvntoRTMEC.generarInfoEventoRTMEC(prueba, listaSerialesEquipo);
				
			} catch (Exception exc) {
				logger.log(Level.SEVERE, "Error registrarFechaInicioPrueba", exc);
			}
		});

	}

	@POST
	@Path("prueba/registrarpruebarealizada")
	public Boolean registrarPruebaRealizada(ReportePruebaDTO reportePruebaDTO,@Context HttpServletRequest request) {
		logger.info(" Direccion ip " + request.getRemoteAddr() );
		logger.info("Prueba :" + reportePruebaDTO.getPrueba().getPruebaId());
		List<DefectoDTO> defectos = reportePruebaDTO.getDefectos();
		if (defectos != null)
			logger.info("Numero de defectos:" + defectos.size());
		List<MedidaDTO> medidas = reportePruebaDTO.getMedidas();
		if (medidas != null)
			logger.info("Numero de medidas:" + medidas.size());
		VerificacionLinealidad verificacionLinealidad = reportePruebaDTO.getPrueba().getVerificacionLinealidad();
		if (verificacionLinealidad != null)
			logger.info("Verificacion Linealidad: " + verificacionLinealidad.getVerificacionLinealidadId());
		VerificacionGasolina verificacionGasolina = reportePruebaDTO.getPrueba().getVerificacionGasolina();
		if (verificacionGasolina != null)
			logger.info("Verificacion Gasolina: " + verificacionGasolina.getVerificacionId());
		List<String> serialesEquipos = reportePruebaDTO.getListaSerialesEquipo();
		if (serialesEquipos != null)
			serialesEquipos.stream().forEach(serial -> {
				logger.info("equipo: " + serial.toString());
			});
		if (reportePruebaDTO.getPistaId() != null) {
			logger.log(Level.INFO, "Pista : " + reportePruebaDTO.getPistaId());
		}
		registrarPruebaRealizadaService.registrarPruebaFinalizada(reportePruebaDTO,request.getRemoteAddr());
		/*notificationService.sendNotification(
				"Prueba finalizada y registrada " + reportePruebaDTO.getPrueba().getPruebaId(),
				NivelAlerta.INFORMACION, Boolean.FALSE);*/
		executor.submit(() -> {
			
			try {
				
				operacionesSicov = operacionesSicovProducer.crearOperacionSicov();
				generadorInfoEvntoRTMEC.setOperacionesSicov(operacionesSicov);		
				generadorInfoEvntoRTMEC.generarInfoEventoRTMEC(reportePruebaDTO.getPrueba(), serialesEquipos);
				
			} catch (Exception exc) {
				logger.log(Level.SEVERE, "Error", exc);
			}
		});
		return Boolean.TRUE;
	}

}
