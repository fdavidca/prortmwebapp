package com.proambiente.webapp.rest;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.proambiente.modelo.CategoriaDefecto;
import com.proambiente.modelo.Defecto;
import com.proambiente.modelo.SubCategoriaDefecto;
import com.proambiente.modelo.dto.DefectoDTO;
import com.proambiente.webapp.dao.ConfiguracionGasesMotoFacade;
import com.proambiente.webapp.dao.DefectoFacade;
import com.proambiente.webapp.service.DefectoService;
import com.proambiente.webapp.service.DefectosInspeccionSensorialMotosService;
import com.proambiente.webapp.util.CausalesRechazoPruebasEmisionesContaminantes; 

@Path("/")
public class DefectosRest {
	
	@Inject
	DefectoFacade defectoDAO;
	
	@Inject
	DefectoService defectoService;
	
	@Inject
	DefectosInspeccionSensorialMotosService defectosIpService;
	
	@POST
	@Produces("application/json")
	@Path("registrardefectos")
	@Consumes("application/json")
	public Boolean guardarDefectos(List<DefectoDTO> listaDefectos){
//		for(DefectoDTO d:listaDefectos){
//			System.out.println("Defecto:" + d.getDefectoId());
//		}
		defectoDAO.registrarDefectos(listaDefectos);
		return Boolean.TRUE;
	}
	
	@GET
	@Produces("application/json")
	@Path("categoriadefectoinspeccionvisual")
	public List<CategoriaDefecto> obtenerCategoriaDefectoPorTipoVehiculo(@QueryParam("tipovehiculo")Integer tipoVehiculo){
		if(tipoVehiculo == null)
			throw new IllegalArgumentException("tipoVehiculoId no puede ser null");
		
		return defectoService.obtenerCategoriaDefectoInspeccionVisualPorTipoVehiculo(tipoVehiculo);
	}
	
	@GET
	@Produces("application/json")
	@Path("subcategoriadefectoinspeccionvisual")
	public List<SubCategoriaDefecto> obtenerSubcategoriaPorTipoVehiucloPorCategoria(@QueryParam("tipovehiculo")Integer tipoVehiculoId,@QueryParam("categoria")Integer categoriaId){
		if(tipoVehiculoId == null || categoriaId == null){
			throw new IllegalArgumentException("TipoVehiculo o categoria no puede ser null");
		}
		return defectoService.obtenerSubCategoriaDefectoInspeccionVisualPorTipoVehiculoPorCategoria(tipoVehiculoId, categoriaId);		
	}
	
	@GET
	@Produces("application/json")
	@Path("defectoinspeccionvisual")
	public List<Defecto> obtenerDefectoPorTipoVehiculoPorCategoriaPorSubCategoria(@QueryParam("tipovehiculo")Integer tipoVehiculo,
																				@QueryParam("categoria") Integer categoria,
																				@QueryParam("subcategoria") Integer subcategoria){
		if(tipoVehiculo == null || categoria == null || subcategoria == null){
			throw new IllegalArgumentException("TipoVehiculo o categoria o subcategoria no puede ser null");
		}
		return defectoService.obtenerDefectoPorTipoVehiculoPorCategoriaPorSubcategoria(tipoVehiculo, categoria, subcategoria);
		
	}
	
	@GET
	@Produces("application/json")
	@Path("defectosinspeccionsensorial")
	public List<Defecto> obtenerDefectoPorTipoVehiculo(@QueryParam("tipovehiculo")Integer tipoVehiculo){
		if(tipoVehiculo==null){
			throw new IllegalArgumentException("TipoVehiculo no puede ser null");
		}
		return defectoService.obtenerDefectoPorTipoVehiculo(tipoVehiculo);
	}
	

	@GET
	@Produces("application/json")
	@Path("defectosinspeccionpreviamotocicletas")
	public List<Defecto> obtenerDefectosInspeccionPreviaMotocicletas(){
		
		Integer[] defectos =  defectosIpService.consultarDefectosInspeccionMotos();
		return defectoService.obtenerCausalesRechazoInspeccionPrevia(defectos);
	}
	
	
	@GET
	@Produces("application/json")
	@Path("defectosinspeccionpreviavehiculos")
	public List<Defecto> obtenerDefectosInspeccionPreviaVehiculos(){
		Integer[] codigosDefectoVehiculos = CausalesRechazoPruebasEmisionesContaminantes.obtenerCausalesRechazoVehiculosInspeccionPrevia();
		return defectoService.obtenerCausalesRechazoInspeccionPrevia(codigosDefectoVehiculos);
	}

	
	@GET
	@Produces("application/json")
	@Path("defectosinspeccionpreviadiesel")
	public List<Defecto> obtenerDefectosInspeccionPreviaDiesel(){
		Integer[] codigosDefectoVehiculos = CausalesRechazoPruebasEmisionesContaminantes.obtenerCausalesRechazoInspeccionPreviaDiesel();
		return defectoService.obtenerCausalesRechazoInspeccionPrevia(codigosDefectoVehiculos);
	}
	
	@GET
	@Produces("application/json")
	@Path("defectoporclaveresumen")
	public Defecto obtenerDefectoPorResumen(@QueryParam("clave_resumen")String claveResumen){
		CausalesRechazoPruebasEmisionesContaminantes causal = CausalesRechazoPruebasEmisionesContaminantes.constantePorClaveCausa(claveResumen);
		return defectoService.obtenerDefectoPorId(causal.getCodigoDefecto());
	}
	

}
