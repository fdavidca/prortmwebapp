package com.proambiente.webapp.rest;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.proambiente.modelo.Permisible;
import com.proambiente.webapp.dao.PermisibleFacade;


@Path("/")
public class PermisibleRestService {
	
	@Inject
	PermisibleFacade permisibleDAO;
	
	@GET
	@Path("permisible")
	@Produces("application/json")
	public Permisible obtenerPermisible(@QueryParam("tipomedida") Integer tipoMedida,
										@QueryParam("tipovehiculo") Integer tipoVehiculo,
										@QueryParam("modelo") Integer modelo){
//		System.out.println("tipoMedida: "+tipoMedida );
//		System.out.println("tipovehiculo: " + tipoVehiculo);
//		System.out.println("modelo: " + modelo);
		
		return permisibleDAO.buscarPermisible(tipoMedida, tipoVehiculo, modelo);
		
	}
	
	@GET
	@Path("permisible_diesel")
	@Produces("application/json")
	public Permisible obtenerPermisibleDiesel(@QueryParam("tipomedida") Integer tipoMedida,
										@QueryParam("tipovehiculo") Integer tipoVehiculo,
										@QueryParam("modelo") Integer modelo,@QueryParam("cilindraje") Integer cilindraje){
//		System.out.println("tipoMedida: "+tipoMedida );
//		System.out.println("tipovehiculo: " + tipoVehiculo);
//		System.out.println("modelo: " + modelo);
		
		return permisibleDAO.buscarPermisibleCondicionalSecundario(tipoMedida, tipoVehiculo, modelo,cilindraje);
		
	}

}
