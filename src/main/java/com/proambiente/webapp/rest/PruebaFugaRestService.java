package com.proambiente.webapp.rest;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.proambiente.modelo.PruebaFuga;
import com.proambiente.webapp.dao.PruebaFugaFacade;


@Path("/")
public class PruebaFugaRestService {
	
	private static final Logger logger = Logger.getLogger(PruebaFugaRestService.class.getName());

	@Inject 
	PruebaFugaFacade pruebaFugaDAO;
	
	@GET
	@Path("pruebafuga")
	@Consumes("application/json")
	@Produces("application/json")	
	public PruebaFuga consultarPruebaFuga(@QueryParam("serial") String serial){
		if(serial != null && !serial.isEmpty()){
			return pruebaFugaDAO.buscarUltimaPruebaFugas(serial.trim());
		}else{
			return null;
		}
	}
	
	@POST
	@Path("pruebafuga")
	@Consumes("application/json")
	@Produces("application/json")
	public String registrarPruebaFuga(PruebaFuga pruebaFuga){
		
		if(pruebaFuga!= null)
			pruebaFugaDAO.create(pruebaFuga);
		logger.log(Level.INFO,"Prueba fuga " + pruebaFuga.toString());
		
		return String.valueOf( pruebaFuga.getPruebaFugasId() );
		
	}
	
}
