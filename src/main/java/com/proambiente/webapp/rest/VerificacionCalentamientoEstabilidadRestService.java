package com.proambiente.webapp.rest;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;



import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.proambiente.modelo.VerificacionEstabilidadCalentamiento;

import com.proambiente.webapp.dao.VerificacionEstabilidadCalentamientoFacade;


@Path("/")
public class VerificacionCalentamientoEstabilidadRestService {
	
	private static final Logger logger = Logger.getLogger(VerificacionCalentamientoEstabilidadRestService.class.getName());
	
	@Inject
	VerificacionEstabilidadCalentamientoFacade verificacionDAO;
	
	
	@GET
	@Path("verificacioncalentamientoestabilidad")
	@Consumes("application/json")
	@Produces("application/json")
	public VerificacionEstabilidadCalentamiento obtenerUltimaVerificacionEstabilidad(@QueryParam("serial") String serial){
		if(serial == null || serial.isEmpty())
			throw new IllegalArgumentException("Especifique serial de equipo para consultar la verificacion de gasolina");
		return verificacionDAO.obtenerVerificacionEstabilidadCalentamiento(serial);
	}

	

	

	
	@POST
	@Path("verificacioncalentamientoestabilidad")
	@Consumes("application/json")
	@Produces("application/json")
	public String crearVerificacionEstabilidad(VerificacionEstabilidadCalentamiento verificacion) throws JsonParseException, JsonMappingException, IOException{
		if(verificacion  == null )
			throw new IllegalArgumentException("Especifique la verificacion a crear, el parametro es null");
		logger.log(Level.INFO,"Verificacion " + verificacion.toString());
		verificacionDAO.create(verificacion);
		return String.valueOf( verificacion.getVerificacionCalentamientoEstabilidadId() );
	}
	
	
	
	
	
}
