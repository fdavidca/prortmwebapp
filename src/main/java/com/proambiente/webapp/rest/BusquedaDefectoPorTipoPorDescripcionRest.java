package com.proambiente.webapp.rest;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.proambiente.modelo.Defecto;
import com.proambiente.webapp.service.BusquedaDefectoService;

@Path("/")
public class BusquedaDefectoPorTipoPorDescripcionRest {
	
	@Inject
	private BusquedaDefectoService busquedaDefectoService;
	
	@GET
	@Produces("application/json")
	@Path("buscardefecto")	
	public List<Defecto> buscarDefecto(@QueryParam("descripcion")String descripcion,@QueryParam("tipovehiculo")Integer tipoVehiculoId){
		return busquedaDefectoService.buscarDefecto(descripcion,tipoVehiculoId);
	}

}
