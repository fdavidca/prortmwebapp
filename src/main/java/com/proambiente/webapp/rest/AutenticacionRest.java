package com.proambiente.webapp.rest;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;

import com.proambiente.modelo.Usuario;
import com.proambiente.modelo.dto.UsuarioDTO;
import com.proambiente.webapp.dao.UsuarioFacade;
import com.proambiente.webapp.service.CambioPasswordService;
import com.proambiente.webapp.service.PasswordHash; 

@Path("/")
public class AutenticacionRest {
	
	private static final Logger logger = Logger.getLogger(AutenticacionRest.class.getName());
	
	@Inject
	UsuarioFacade usuarioDAO;
	
    @Inject
    CambioPasswordService cambioPasswordService;
	
	@POST
	@Path("autenticar")
	@Consumes("application/json")
	@Produces("application/json")
	public Usuario autenticar(UsuarioDTO usuario){
		if(usuario!=null && usuario.getNombreUsuario()!=null && usuario.getPassword()!=null){
		Usuario u = usuarioDAO.buscarUsuarioPorNombreUsuario(usuario.getNombreUsuario());
		
		
		
		
		try {
			if(u != null && PasswordHash.validatePassword(usuario.getPassword(), u.getContrasenia())){
				u.setContrasenia("");
				boolean isPasswordExpirado = isPasswordExpirado(u);
				u.setPasswordCaducado(isPasswordExpirado);
				return u;
			}else {
				return null;
			}
		} catch (NoSuchAlgorithmException e) {			
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {			
			e.printStackTrace();
		}
		}else{
			return null;
		}
		return null;
	}
	
	public boolean isPasswordExpirado(Usuario u){
		Calendar calendar = Calendar.getInstance();
		Calendar cFechaExpiracion = Calendar.getInstance();
		if(u.getFechaExpiracionContrasenia() != null){
			Date fechaExpiracion = new Date(u.getFechaExpiracionContrasenia().getTime());
			cFechaExpiracion.setTime(fechaExpiracion);
			return calendar.compareTo(cFechaExpiracion)>0;
		}else {
			return true;
		}
	}
	

	@POST
	@Path("cambio_password")
	@Consumes("application/json")
	public void cambiarPassword(UsuarioDTO usuario,@Context HttpServletRequest request){
		if(usuario!=null && usuario.getNombreUsuario()!=null && usuario.getPassword()!=null){
			Usuario u = usuarioDAO.buscarUsuarioPorNombreUsuario(usuario.getNombreUsuario());
			try {
				String ip = request.getRemoteAddr();
				String nuevoPassword = PasswordHash.createHash(usuario.getPassword());
				cambioPasswordService.cambiarPassword(u,nuevoPassword,ip);			
				
			} catch (NoSuchAlgorithmException e) {
				logger.log(Level.SEVERE,"Error codificando password",e);
				
			} catch (InvalidKeySpecException e) {
				logger.log(Level.SEVERE,"Error codificando password",e);
				
			}
			
		}
		
	}
	
}
