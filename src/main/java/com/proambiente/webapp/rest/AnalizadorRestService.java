package com.proambiente.webapp.rest;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.proambiente.modelo.Analizador;
import com.proambiente.webapp.dao.AnalizadorFacade;


@Path("analizador")
public class AnalizadorRestService {

	@Inject
	AnalizadorFacade analizadorDAO;
	
	@POST
	@Consumes("application/json")
	public void guardarAnalizador(Analizador analizador){
		Analizador analizadorConsultado = analizadorDAO.find(analizador.getSerial());
		if(analizadorConsultado == null)
			analizadorDAO.edit(analizador);//lo crea en caso de que no exista			
	}
	
	@GET
	@Produces("application/json")	
	public Analizador getAnalizadorPorSerial(@QueryParam("serial") String serial){
		
			Analizador analizador = analizadorDAO.find(serial);
			return analizador;		
	}
	
	
	@POST
	@Path("actualizarbloqueoautocero/{serial}")
	@Consumes("application/json")
	public void actualizarBloqueoAutocero( @PathParam("serial")String serial,Boolean bloquear) {
		analizadorDAO.actualizarBloqueosAutocero(bloquear,serial);
	}
	
	
	@POST
	@Path("actualizarbloqueoresiduos/{serial}")
	@Consumes("application/json")
	public void actualizarBloqueoResiduos( @PathParam("serial")String serial,Boolean bloquear) {
		analizadorDAO.actualizarBloqueoResiduos(bloquear,serial);
	}
	
	@GET
	@Path("consultarbloqueoresiduos")
	public Boolean consultarBloqueoResiduos(@QueryParam("serial") String serial) {
		return analizadorDAO.consultarEstadoBloqueoResiduos(serial);
	}
	
	@GET
	@Path("consultarbloqueoautocero")
	public Boolean consultarBloqueoAutocero(@QueryParam("serial") String serial) {
		return analizadorDAO.consultarEstadoBloqueoAutocero(serial);
	}
	
	@GET
	@Path("consultarbloqueoautoridad")
	public Boolean consultarBloqueoAutoridad(@QueryParam("serial") String serial) {
		return analizadorDAO.consultarEstadoBloqueoAutoridad(serial);
	}
}
