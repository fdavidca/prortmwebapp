package com.proambiente.webapp.rest;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.proambiente.modelo.VerificacionLinealidad;
import com.proambiente.webapp.dao.VerificacionLinealidadFacade;


@Path("/")
public class VerificacionLinealidadRestService {

	@Inject
	VerificacionLinealidadFacade verificacionLinealidadDAO;
	
	
	@GET
	@Path("verificacionlinealidad")
	@Consumes("application/json")
	@Produces("application/json")
	public VerificacionLinealidad obtenerVerificacionLinealidad(@QueryParam("serial") String serial){
		if(serial == null || serial.isEmpty()){
			return null;
		}else {
			return  verificacionLinealidadDAO.obtenerVerificacionLinealidadPorSerial(serial);
		}
	}
	
	
	@POST
	@Path("verificacionlinealidad")
	@Consumes("application/json")
	@Produces("application/json")
	public Response registrarVerificacionLinealidad(VerificacionLinealidad verificacionLinealidad){
		verificacionLinealidadDAO.create(verificacionLinealidad);
		return Response.ok(Boolean.TRUE).build();
	}
	
}
