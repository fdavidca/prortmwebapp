package com.proambiente.webapp.rest;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import org.apache.commons.math3.stat.regression.SimpleRegression;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.VerificacionGasolina;
import com.proambiente.modelo.dto.MedicionGasesDTO;
import com.proambiente.webapp.dao.InformacionCdaFacade;
import com.proambiente.webapp.dao.VerificacionGasolinaFacade;
import com.proambiente.webapp.service.util.AnalizadorOxigenoVerificacion;
import com.proambiente.webapp.util.AnalizadorVerificacion;
import com.proambiente.webapp.util.MedicionGasesDTODecodificador;

@Path("/")
public class VerificacionGasolinaRestService {
	
	private static final Logger logger = Logger.getLogger(VerificacionGasolinaRestService.class.getName());
	
	@Inject
	VerificacionGasolinaFacade verificacionGasolinaDAO;
	
	@Inject
	InformacionCdaFacade informacionCdaFacade;
	
	
	@GET
	@Path("verificaciongasolina")
	@Consumes("application/json")
	@Produces("application/json")
	public VerificacionGasolina obtenerUltimaVerificacion(@QueryParam("serial") String serial){
		if(serial == null || serial.isEmpty())
			throw new IllegalArgumentException("Especifique serial de equipo para consultar la verificacion de gasolina");
		return verificacionGasolinaDAO.obtenerVerificacionGasolina(serial);
	}

	
	@GET
	@Path("verificaciongasolinatiempos")
	@Consumes("application/json")
	@Produces("application/json")
	public VerificacionGasolina obtenerUltimaVerificacionPorTiempos(@QueryParam("serial") String serial,@QueryParam("tiemposmotor") String tiempos){
		if(serial == null || serial.isEmpty())
			throw new IllegalArgumentException("Especifique serial de equipo para consultar la verificacion de gasolina");
		if(tiempos == null || tiempos.isEmpty()) {
			throw new IllegalArgumentException("Especifique tiempos de motor para consultar la verificacion de gasolina");
		}
		
		return verificacionGasolinaDAO.obtenerVerificacionGasolinaTiempos(serial,tiempos);
	}
	
	@GET
	@Path("verificaciongasolinatiempostipovehiculo")
	@Consumes("application/json")
	@Produces("application/json")
	public VerificacionGasolina obtenerUltimaVerificacionPorTiemposPorTipoVehiculo(@QueryParam("serial") String serial,@QueryParam("tiemposmotor") String tiempos,@QueryParam("tipovehiculo")Integer tipoVehiculo){
		if(serial == null || serial.isEmpty())
			throw new IllegalArgumentException("Especifique serial de equipo para consultar la verificacion de gasolina");
		if(tiempos == null || tiempos.isEmpty()) {
			throw new IllegalArgumentException("Especifique tiempos de motor para consultar la verificacion de gasolina");
		}
		if(tipoVehiculo ==null )
			throw new IllegalArgumentException("Especifique tipo de vehiculo de motor para consultar la verificacion de gasolina");		
		return verificacionGasolinaDAO.obtenerVerificacionGasolinaTiemposTipo(serial,tiempos,tipoVehiculo);
	}
	
	@POST
	@Path("verificaciongasolina")
	@Consumes("application/json")
	@Produces("application/json")
	public String crearVerificacionGasolina(VerificacionGasolina verificacion) throws JsonParseException, JsonMappingException, IOException{
		if(verificacion  == null )
			throw new IllegalArgumentException("Especifique la verificacion a crear, el parametro es null");
		
		InformacionCda infoCda = informacionCdaFacade.find(1);
		logger.log(Level.INFO,"Verificacion " + verificacion.toString());
		Integer tamanioMuestrasBaja = verificacion.getMuestrasBaja().length();
		Integer tamanioMuestraAlta = verificacion.getMuestraAlta().length();
		System.out.println("Tamanio muestra baja " + String.valueOf(tamanioMuestrasBaja));
		System.out.println("Tamanio muestra alta " + String.valueOf(tamanioMuestraAlta));
		AnalizadorVerificacion analizadorVerificacion = new AnalizadorVerificacion();
		
		
		
		List<MedicionGasesDTO> listaMedicionesBaja = MedicionGasesDTODecodificador.decodificarDesdeCadena(verificacion.getMuestrasBaja());
		List<MedicionGasesDTO> listaMedicionesAlta = MedicionGasesDTODecodificador.decodificarDesdeCadena(verificacion.getMuestraAlta());
		
		
		SimpleRegression regresionHC = analizadorVerificacion.calcularRectaHC(listaMedicionesBaja, listaMedicionesAlta,
																				verificacion.getValorGasRefBajaHC(), 
																				verificacion.getValorGasRefAltaHC(),
																				verificacion.getPef() );
		
		SimpleRegression regresionCO = analizadorVerificacion.calcularRectaCO(listaMedicionesBaja,listaMedicionesAlta,
																			  verificacion.getValorGasRefBajaCO(),
																			  verificacion.getValorGasRefAltaCO()
																			  );
		
		SimpleRegression regresionCO2 = analizadorVerificacion.calcularRectaCO2(listaMedicionesBaja, listaMedicionesAlta,
																				verificacion.getValorGasRefBajaCO2(),
																				verificacion.getValorGasRefAltaCO2() );
		
		StringBuilder sb = new StringBuilder();
		Boolean aprobada = analizadorVerificacion.validarConformidadVehiculo(regresionHC, 
														  regresionCO, 
														  regresionCO2, 
														  verificacion, 
														  verificacion.getPef(),
														  sb,
														  verificacion.getTipoVerificacion());
		if (infoCda.getVerificacionOxigeno()) {
			Boolean aprobadaOxigeno = new AnalizadorOxigenoVerificacion(listaMedicionesBaja,listaMedicionesAlta).analizar(sb);
			verificacion.setAprobada(aprobada && aprobadaOxigeno);
			verificacion.setVerificacionOxigeno(true);
		}else {
			verificacion.setAprobada(aprobada);
			verificacion.setVerificacionOxigeno(false);
		}
			
		verificacionGasolinaDAO.create(verificacion);		
		return String.valueOf( verificacion.getVerificacionId() );
	}
	
	
	
	
	
}
