package com.proambiente.webapp.rest;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.proambiente.modelo.FiltroDensidadNeutra;
import com.proambiente.webapp.dao.FiltroDensidadNeutraFacade;

@Path("filtrodensidadneutra")
public class FiltroDensidadNeutraRest {

	@Inject
	FiltroDensidadNeutraFacade filtroFacade;
	
	
	@GET
	@Produces("application/json")
	
	public List<FiltroDensidadNeutra> obtenerFiltros(){
		return filtroFacade.findAll();		
	}
	
}
