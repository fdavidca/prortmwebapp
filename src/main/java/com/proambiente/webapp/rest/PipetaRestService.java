package com.proambiente.webapp.rest;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.proambiente.modelo.Pipeta;
import com.proambiente.webapp.dao.PipetaFacade;
@Path("pipeta")
public class PipetaRestService {

	
	

		@Inject
		PipetaFacade pipetaFacade;
		
		
		@GET
		@Produces("application/json")
		
		public List<Pipeta> obtenerPipetas(){
			return pipetaFacade.findAll();		
		}
		
	
	
}
