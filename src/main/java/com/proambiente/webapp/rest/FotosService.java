package com.proambiente.webapp.rest;

import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.jfree.util.Log;

import com.proambiente.webapp.dao.FotoFacade;
import com.proambiente.webapp.service.util.CambiarPpiJpeg;

@Path("/upload")
public class FotosService {
	
	
	private static final Logger logger = Logger.getLogger(FotosService.class
			.getName());
	
	@Inject
	FotoFacade fotoDAO;
	
	private final String UPLOADED_FILE_PATH;
	
	public FotosService(){
		String temp = System.getenv("TEMP");		
		if(temp != null){
			temp = temp + "\\";
			UPLOADED_FILE_PATH = temp;
		}
		else{ 
			UPLOADED_FILE_PATH = "c:/temp/";
		}
	}
	
	@POST
	@Path("/foto1_upload/{placa}")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response uploadFile(@PathParam("placa") String placa,MultipartFormDataInput input)throws IOException{
		String filename = "";
		Map<String, List<InputPart>> formParts = input.getFormDataMap();
		filename = formParts.get("filename").get(0).getBodyAsString();
		 List<InputPart> inputParts = formParts.get("file");
		 for(InputPart inputPart:inputParts){
			 try{
				 
				 MultivaluedMap<String, String> header = inputPart.getHeaders();				 
				 InputStream inputStream = inputPart.getBody(InputStream.class, null);
				 byte[] bytes = IOUtils.toByteArray(inputStream);
				 filename = UPLOADED_FILE_PATH + filename;
	             writeFile(bytes, filename);
	             BufferedImage inputImage = ImageIO.read(new File(filename));
	                      
	             

	             new CambiarPpiJpeg().saveGridImage(new File(UPLOADED_FILE_PATH+"reducida.jpg"),inputImage);

	                         
	             
	             fotoDAO.insertarFotoUno(placa, UPLOADED_FILE_PATH+"reducida.jpg");
	             inputStream.close();
	             File file = new File(filename);
	             file.delete();	             
				logger.log(Level.INFO, "Foto correctamente subida");
				 
			 }catch(Exception exc){
				 logger.log(Level.SEVERE, "Error subiendo foto 1", exc);				 
				 return Response.serverError().build();
			 }
		 }
		 return Response.status(200)
	                .entity("Uploaded file name : "+ filename).build();
		
	}
	
	 private void writeFile(byte[] content, String filename) throws IOException 
	    {
	        File file = new File(filename);
	        if (!file.exists()) {
	            file.createNewFile();
	        }
	        FileOutputStream fop = new FileOutputStream(file);
	        fop.write(content);
	        fop.flush();
	        fop.close();
	    }
	
	 @GET
	 @Path("/foto_uno_tomada/{placa}")
	 public Response isFotoUnoTomada(@PathParam("placa") String placa){
		 try {
			Boolean fotoUnoTomada = fotoDAO.isFotoUnoTomada(placa);			 
			return Response.ok(fotoUnoTomada).build();
		} catch (Exception e) {
			Log.error("Error consultando foto uno tomada", e);
			return Response.serverError().build();
		}
	 }
	 
	 @POST
	 @Path("/foto2_upload/{placa}/{usuario_id}")
	 @Consumes(MediaType.MULTIPART_FORM_DATA)
	 public Response uploadFotoDos(@PathParam("placa") String placa,@PathParam("usuario_id") Integer usuarioId,MultipartFormDataInput input)throws IOException{
			String filename = "";
			Map<String, List<InputPart>> formParts = input.getFormDataMap();
			filename = formParts.get("filename").get(0).getBodyAsString();
			 List<InputPart> inputParts = formParts.get("file");
			 for(InputPart inputPart:inputParts){
				 try{
					 
					 MultivaluedMap<String, String> header = inputPart.getHeaders();				 
					 InputStream inputStream = inputPart.getBody(InputStream.class, null);
					 byte[] bytes = IOUtils.toByteArray(inputStream);
					 filename = UPLOADED_FILE_PATH + filename;					 
		             writeFile(bytes, filename);
		             
		             BufferedImage inputImage = ImageIO.read(new File(filename));
		             new CambiarPpiJpeg().saveGridImage(new File(UPLOADED_FILE_PATH+"reducida.jpg"),inputImage);

		                   
		             fotoDAO.insertarFotoDos(placa, UPLOADED_FILE_PATH+"reducida.jpg",usuarioId);
		             inputStream.close();
		             File file = new File(filename);
		             file.delete();
		             System.out.println("Foto Dos Registrada correctamente");
					 
		             
				 }catch(Exception exc){
					logger.log(Level.SEVERE, "Error subiendo foto dos", exc);
					 return Response.serverError().build();
				 }
			 }
			 return Response.status(200)
		                .entity("Foto dos creada correctamente : "+ filename).build();
			
		}
	 
	 @GET
	 @Path("/fechaplaca")	 
	 public String obtenerPlacaFecha(@QueryParam("placa") String placa){		 
		 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		 Date date = new Date();
		 SimpleDateFormat sdf2 = new SimpleDateFormat("HH-mm");
		 return String.format("%s, %s %s",sdf.format(date),placa,sdf2.format(date) );
	        
	 }
	 
}


