package com.proambiente.webapp.rest;

import javax.inject.Inject;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.webapp.dao.RevisionFacade;
import com.proambiente.webapp.dao.VehiculoFacade;

import static com.proambiente.webapp.util.ConstantesDisenioVehiculo.*; 

@Path("vehiculo")
public class VehiculoService {
	
	@Inject
	RevisionFacade revisionDAO;
	
	@Inject
	VehiculoFacade vehiculoDAO;

	@POST
	@Path("diametro")	
	public Boolean guardarDiametro(@FormParam("revision_id")Integer revisionId,@FormParam("diametro")Integer diametro){
		if(revisionId != null && diametro!=null){
			Revision revision = revisionDAO.find(revisionId);
			Vehiculo vehiculo = revision.getVehiculo();
			vehiculo.setDiametroExosto(diametro);
			vehiculoDAO.edit(vehiculo);
			return Boolean.TRUE;			
		}else{
			throw new IllegalArgumentException("Revision o diametro no especificados");
		}
	}
	
	@POST
	@Path("catalizador")	
	public Boolean registrarCatalizador(@FormParam("revision_id")Integer revisionId,@FormParam("catalizador")String catalizador){
		if(revisionId != null && catalizador!=null){
			Revision revision = revisionDAO.find(revisionId);
			Vehiculo vehiculo = revision.getVehiculo();
			vehiculo.setCatalizador( catalizador );
			vehiculoDAO.edit(vehiculo);
			return Boolean.TRUE;			
		}else{
			throw new IllegalArgumentException("Revision o diametro no especificados");
		}
	}
	
	@POST
	@Path("disenio")
	public Boolean registrarVehiculoEsScooter(@FormParam("revision_id") Integer revisionId,@FormParam("scooter")String disenio){
		if(revisionId != null && disenio!=null){
			Revision revision = revisionDAO.find(revisionId);
			Vehiculo vehiculo = revision.getVehiculo();
			if(disenio.equalsIgnoreCase(SCOOTER)){
				vehiculo.setDiseno(SCOOTER);
			}else if(disenio.equalsIgnoreCase(CONVENCIONAL)){
				vehiculo.setDiseno(CONVENCIONAL);
			}else {
				throw new IllegalArgumentException("Ha especificadio disenio no valido " + disenio);
			}
			vehiculoDAO.edit(vehiculo);
			return Boolean.TRUE;			
		}else{
			throw new IllegalArgumentException("Revision o disenio no especificados o null");
		}
	}
	
}
