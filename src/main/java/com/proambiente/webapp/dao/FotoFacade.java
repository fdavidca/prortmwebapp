
package com.proambiente.webapp.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.sql.DataSource;

import org.apache.commons.io.IOUtils;

import com.proambiente.modelo.Foto;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Usuario;
import com.proambiente.webapp.service.RevisionService;

/**
 *
 * @author FABIAN
 */
@Stateless
public class FotoFacade extends AbstractFacade<Foto> {
    @PersistenceContext 
    private EntityManager em;
    
    @Resource(name="PrortmDS")
    DataSource ds;
    
   @Inject
   RevisionService revisionService;
   
   @Inject
   PruebaFacade pruebaDAO;
    

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FotoFacade() {
        super(Foto.class);
    }
    
    public Foto consultarFotoPorPrueba(Prueba prueba){
    	
    	TypedQuery<Foto> query = em.createQuery("SELECT f FROM Foto f WHERE f.prueba.pruebaId =:pruebaId",Foto.class);
    	query.setParameter("pruebaId", prueba.getPruebaId());
    	try{
    		Foto f = query.getSingleResult();
    		return f;
    	}catch(NoResultException exc){
    		return new Foto();
    	}    	
    }
    
    
    public void insertarFotoUno(String placa,String filename) {
    	
    	FileInputStream fis = null;
    	try{
    		Prueba prueba = revisionService.buscarPruebaNoFinalizada(placa, 3);
    		if(prueba != null){
    			File file = new File(filename);
    			fis = new FileInputStream(file);
    			byte[] bytes = IOUtils.toByteArray(fis);
    			Foto foto = consultarFotoPorPrueba(prueba);
    			if(foto.getFotoId() != null){
    				foto.setPrueba(prueba);
    				foto.setFoto1(bytes);
    				edit(foto);
    				System.out.println("Registro de foto actualizado");
    			}else {
    				foto.setPrueba(prueba);
    				foto.setFoto1(bytes);
    				create(foto);
    				System.out.println("Registro de foto creado");
    			}
    		}else {
    			throw new RuntimeException(String.format("Prueba de foto para placa %s no existente",placa));
    		}
    	}
    	catch (FileNotFoundException e) {			
			e.printStackTrace();
			throw new RuntimeException("Archivo temporal no encontrado " + filename);
		} catch (Exception e) {			
			e.printStackTrace();
			throw new RuntimeException("Error al intentar guardar imagen " + filename + " placa " + placa);
		}
    	finally
    	{
    		if(fis != null)
				try {
					fis.close();
				} catch (IOException e) {
					
					e.printStackTrace();
				}
    	}
    	
    }
    
    
    public void insertarFotoDos(String placa,String filename,Integer usuarioId){
    	FileInputStream fis = null;
    	try{
    		Prueba prueba = revisionService.buscarPruebaNoFinalizada(placa, 3);
    		if(prueba != null){
    			File file = new File(filename);
    			fis = new FileInputStream(file);
    			byte[] bytes = IOUtils.toByteArray(fis);
    			Foto foto = consultarFotoPorPrueba(prueba);
    			if(foto.getFotoId() != null){
    				foto.setPrueba(prueba);
    				foto.setFoto2(bytes);
    				edit(foto);
    				Usuario usuario = em.find(Usuario.class, usuarioId);
    				prueba.setFinalizada(true);
    				prueba.setAprobada(true);
    				prueba.setUsuario(usuario);
    				pruebaDAO.edit(prueba);
    				System.out.println("Registro de foto actualizado");
    			}else {
    				throw new RuntimeException("Foto uno no tomada");    				
    			}
    		}else {
    			throw new RuntimeException(String.format("Prueba de foto para placa %s no existente",placa));
    		}
    	}
    	catch (FileNotFoundException e) {			
			e.printStackTrace();
			throw new RuntimeException("Archivo temporal no encontrado " + filename);
		} catch (Exception e) {			
			e.printStackTrace();
			throw new RuntimeException("Error al intentar guardar imagen " + filename + " placa " + placa);
		}
    	finally
    	{
    		if(fis != null)
				try {
					fis.close();
				} catch (IOException e) {
					
					e.printStackTrace();
				}
    	}    	
    	
    }
    
    public boolean isFotoUnoTomada(String placa) throws Exception{    	
    	Prueba prueba = revisionService.buscarPruebaNoFinalizada(placa, 3);
    	Foto f = consultarFotoPorPrueba(prueba);
    	boolean isFotoUnoTomada = false;
    	if(f.getFotoId() != null ){
    		isFotoUnoTomada = true;
    		
    	}else {
    		isFotoUnoTomada = false;
    	}
    	return isFotoUnoTomada;
    }
    
    
    
}
