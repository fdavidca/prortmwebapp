
package com.proambiente.webapp.dao;

import com.proambiente.modelo.CausaAborto;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author FABIAN
 */
@Stateless
public class CausaAbortoFacade extends AbstractFacade<CausaAborto> {
    @PersistenceContext 
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CausaAbortoFacade() {
        super(CausaAborto.class);
    }
    
}
