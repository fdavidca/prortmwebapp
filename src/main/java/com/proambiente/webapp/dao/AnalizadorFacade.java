package com.proambiente.webapp.dao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.proambiente.modelo.Analizador;
import com.proambiente.modelo.Analizador_;

@Stateless
public class AnalizadorFacade extends AbstractFacade<Analizador> {
	
	 @PersistenceContext
	    private EntityManager em;
	 
	 @Override
	    protected EntityManager getEntityManager() {
	        return em;
	    }

	    public AnalizadorFacade() {
	        super(Analizador.class);
	    }
	    
	    public String obtenerSerialEquipoCompleto(String serial){
	    	Analizador analizador = find(serial);
	    	return (analizador != null  && analizador.getSerialAnalizador() != null) ? analizador.getSerialAnalizador() : "Aun no configurado";
	    }
	    
	    
	    public void actualizarBloqueoResiduos(Boolean bloquear,String serial) {
	    	String str = "UPDATE Analizador a SET bloqueoResiduos=:bloqueop WHERE serial=:serialp";
	    	Query query = em.createQuery(str);
	    	query.setParameter("bloqueop",bloquear);
	    	query.setParameter("serialp", serial);
	    	query.executeUpdate();	    		
	    }
	    
	    public void actualizarBloqueosAutocero(Boolean bloquear,String serial) {
	    	String str = "UPDATE Analizador a SET bloqueoAutocero=:bloqueop WHERE serial=:serialp";
	    	Query query = em.createQuery(str);
	    	query.setParameter("bloqueop",bloquear);
	    	query.setParameter("serialp", serial);
	    	query.executeUpdate();	
	    }
	    
	    public Boolean consultarEstadoBloqueoAutocero(String serial) {
	    	CriteriaBuilder cb = em.getCriteriaBuilder();
	    	CriteriaQuery<Analizador> cq = cb.createQuery(Analizador.class);
	    	Root r = cq.from(Analizador.class); 
	    	cq.where(cb.equal(r.get(Analizador_.serial),serial));
	        Analizador analizador = em.createQuery(cq).getSingleResult();
	    	return analizador.getBloqueoAutocero();
	    }
	    
	    public Boolean consultarEstadoBloqueoResiduos(String serial) {
	    	CriteriaBuilder cb = em.getCriteriaBuilder();
	    	CriteriaQuery<Analizador> cq = cb.createQuery(Analizador.class);
	    	Root r = cq.from(Analizador.class); 
	    	cq.where(cb.equal(r.get(Analizador_.serial),serial));
	        Analizador analizador = em.createQuery(cq).getSingleResult();
	    	return analizador.getBloqueoResiduos();
	    }
	    
	    public Boolean consultarEstadoBloqueoAutoridad(String serial) {
	    	CriteriaBuilder cb = em.getCriteriaBuilder();
	    	CriteriaQuery<Analizador> cq = cb.createQuery(Analizador.class);
	    	Root r = cq.from(Analizador.class); 
	    	cq.where(cb.equal(r.get(Analizador_.serial),serial));
	        Analizador analizador = em.createQuery(cq).getSingleResult();
	    	return analizador.getBloqueoAutoridadAmbiental();
	    }
}
