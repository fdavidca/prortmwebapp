
package com.proambiente.webapp.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.proambiente.modelo.TipoVerificacion;
import com.proambiente.modelo.VerificacionEstabilidadCalentamiento;
import com.proambiente.modelo.VerificacionGasolina;
import com.proambiente.webapp.util.ConstantesTiposVehiculo;

/**
 *
 * @author FABIAN
 */
@Stateless
public class VerificacionEstabilidadCalentamientoFacade extends AbstractFacade<VerificacionEstabilidadCalentamiento> {
    @PersistenceContext 
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public VerificacionEstabilidadCalentamientoFacade() {
        super(VerificacionEstabilidadCalentamiento.class);
    }
    
    
    
    
    
    public VerificacionEstabilidadCalentamiento obtenerVerificacionEstabilidadCalentamiento(String serieEquipo){
    	if(serieEquipo == null || serieEquipo.isEmpty())
    		throw new IllegalArgumentException("Especifique serie de equipo para consultar verificacion de Gasolina");
    	
    	String consulta = "SELECT Max(v) FROM VerificacionEstabilidadCalentamiento v WHERE v.serieAnalizador=:serieEquipo ";    			
    	TypedQuery<VerificacionEstabilidadCalentamiento> query = em.createQuery(consulta, VerificacionEstabilidadCalentamiento.class);
    	query.setParameter("serieEquipo",serieEquipo);    	
    	try{
    		VerificacionEstabilidadCalentamiento verificacion = query.getSingleResult();
    		return verificacion;
    	}catch(NoResultException nre){
    		System.out.println("No se encuentra verificacion para:" + serieEquipo);
    		return null;
    	}
    	
    }
    
    
    
    
    
}
