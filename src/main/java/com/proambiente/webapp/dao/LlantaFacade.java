
package com.proambiente.webapp.dao;

import com.proambiente.modelo.Llanta;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author FABIAN
 */
@Stateless
public class LlantaFacade extends AbstractFacade<Llanta> {
    @PersistenceContext 
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public LlantaFacade() {
        super(Llanta.class);
    }
    
}
