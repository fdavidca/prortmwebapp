
package com.proambiente.webapp.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.proambiente.modelo.VerificacionLinealidad;

/**
 *
 * @author FABIAN
 */
@Stateless
public class VerificacionLinealidadFacade extends AbstractFacade<VerificacionLinealidad> {
    @PersistenceContext 
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public VerificacionLinealidadFacade() {
        super(VerificacionLinealidad.class);
    }
    
    public VerificacionLinealidad obtenerVerificacionLinealidadPorSerial(String serial){
    	String consulta = "SELECT Max(v) FROM VerificacionLinealidad v WHERE v.serieOpacimetro =:serial";
    	TypedQuery<VerificacionLinealidad> query = em.createQuery(consulta, VerificacionLinealidad.class);
    	query.setParameter("serial",serial);
    	VerificacionLinealidad verificacionLinealidad;
    	try{
    		verificacionLinealidad = query.getSingleResult();
    		return verificacionLinealidad;
    	}catch(NoResultException nre){
    		return null;
    	}
    	
    	
    }
    
    
    public List<VerificacionLinealidad> consultarVerificacionesLinealidad(Date fechaInicial,Date fechaFinal){
    	
    	String consulta = "SELECT v FROM VerificacionLinealidad v "+
    					  "WHERE v.fechaVerificacion BETWEEN :fechaInicial AND :fechaFinal";
    	TypedQuery<VerificacionLinealidad> query = em.createQuery(consulta, VerificacionLinealidad.class);
    	query.setParameter("fechaInicial", fechaInicial);
    	query.setParameter("fechaFinal",fechaFinal);
    	List<VerificacionLinealidad> verificaciones = null;
    	try{
    		verificaciones = query.getResultList();
    		return verificaciones;
    	}catch(NoResultException nre){
    		return new ArrayList<VerificacionLinealidad>();
    	}
    }
    
}
