
package com.proambiente.webapp.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.proambiente.modelo.RegistroFur;

/**
 *
 * @author FABIAN
 */
@Stateless
public class RegistroFurFacade extends AbstractFacade<RegistroFur> {
    @PersistenceContext 
    private EntityManager em;
 

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RegistroFurFacade() {
        super(RegistroFur.class);
    }
    
    public List<RegistroFur> buscarPorRevisionId(Long revisionId){
    	String queryStr = "SELECT rf FROM RegistroFur rf WHERE rf.revisionId=:revisionId";
    	Query query = em.createQuery(queryStr);
    	query.setParameter("revisionId",revisionId);
    	return query.getResultList();
    }
    
}
