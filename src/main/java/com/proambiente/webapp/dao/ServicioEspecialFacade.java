
package com.proambiente.webapp.dao;

import com.proambiente.modelo.ServicioEspecial;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author FABIAN
 */
@Stateless
public class ServicioEspecialFacade extends AbstractFacade<ServicioEspecial> {
    @PersistenceContext 
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ServicioEspecialFacade() {
        super(ServicioEspecial.class);
    }
    
}
