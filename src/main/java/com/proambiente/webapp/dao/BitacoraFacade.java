package com.proambiente.webapp.dao;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.proambiente.modelo.Bitacora;


@Stateless
public class BitacoraFacade extends AbstractFacade<Bitacora> {
	
	 @PersistenceContext
	 private EntityManager em;
	 
	 @Override
	    protected EntityManager getEntityManager() {
	        return em;
	    }

	    public BitacoraFacade() {
	        super(Bitacora.class);
	    }

	  public List<Bitacora> buscarBitacoraPorFecha(Date fechaInicial,Date fechaFinal){
		  String consulta = "Select b From Bitacora b where fechaRegistro between :fechaInicial and :fechaFinal";
		  TypedQuery<Bitacora> query = em.createQuery(consulta,Bitacora.class);
		  query.setParameter("fechaInicial",fechaInicial);
		  query.setParameter("fechaFinal",fechaFinal);
		  return query.getResultList();		  
	  }
	  
	 
	    
}
