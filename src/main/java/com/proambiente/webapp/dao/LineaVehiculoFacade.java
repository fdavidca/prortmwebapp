
package com.proambiente.webapp.dao;

import com.proambiente.modelo.LineaVehiculo;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author FABIAN
 */
@Stateless
public class LineaVehiculoFacade extends AbstractFacade<LineaVehiculo> {
    @PersistenceContext 
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public LineaVehiculoFacade() {
        super(LineaVehiculo.class);
    }
    
    public List<LineaVehiculo> findLineasByNombre(String nombre){
    	String query = "SELECT l FROM LineaVehiculo l  WHERE l.nombre =:nombre ";
    	Query q = em.createQuery(query);
    	q.setParameter("nombre", nombre.toUpperCase() + '%');
    	return q.<LineaVehiculo>getResultList();
    }
    
}
