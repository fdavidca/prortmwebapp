
package com.proambiente.webapp.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.proambiente.modelo.TipoVerificacion;
import com.proambiente.modelo.VerificacionGasolina;
import com.proambiente.webapp.util.ConstantesTiposVehiculo;

/**
 *
 * @author FABIAN
 */
@Stateless
public class VerificacionGasolinaFacade extends AbstractFacade<VerificacionGasolina> {
    @PersistenceContext 
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public VerificacionGasolinaFacade() {
        super(VerificacionGasolina.class);
    }
    
    
    public VerificacionGasolina obtenerVerificacionGasolinaTiempos(String serieEquipo,String numeroTiempos){
    	if(serieEquipo == null || serieEquipo.isEmpty())
    		throw new IllegalArgumentException("Especifique serie de equipo para consultar verificacion de Gasolina");
    	
    	if(numeroTiempos == null || numeroTiempos.isEmpty())
    		throw new IllegalArgumentException("Especifique numero de tiempos para verificacion de Gasolina");
    	
    	List<TipoVerificacion> listaTiposVerificacion = new ArrayList<>();
    	
    	switch(numeroTiempos) {
    		case "4":
    			listaTiposVerificacion.add(TipoVerificacion.VEHICULO_OTTO);
    			listaTiposVerificacion.add(TipoVerificacion.MOTOCICLETA_4T);
    		break;	
    		case "2":
    			listaTiposVerificacion.add(TipoVerificacion.MOTOCICLETA_2T);
    		break;	
    	}
    	
    	
    	String consulta = "SELECT Max(v) FROM VerificacionGasolina v WHERE v.numeroSerieEquipo=:serieEquipo "
    			+ " AND v.tipoVerificacion in :tiposVerificacion";
    	TypedQuery<VerificacionGasolina> query = em.createQuery(consulta, VerificacionGasolina.class);
    	query.setParameter("serieEquipo",serieEquipo);
    	query.setParameter("tiposVerificacion",listaTiposVerificacion);
    	try{
    		VerificacionGasolina verificacionGasolina = query.getSingleResult();
    		return verificacionGasolina;
    	}catch(NoResultException nre){
    		System.out.println("No se encuentra verificacion para:" + serieEquipo);
    		return null;
    	}
    	
    }
    
    public VerificacionGasolina obtenerVerificacionGasolinaTiemposTipo(String serieEquipo,String numeroTiempos,Integer tipoVehiculo){
    	if(serieEquipo == null || serieEquipo.isEmpty())
    		throw new IllegalArgumentException("Especifique serie de equipo para consultar verificacion de Gasolina");
    	
    	if(numeroTiempos == null || numeroTiempos.isEmpty())
    		throw new IllegalArgumentException("Especifique numero de tiempos para verificacion de Gasolina");
    	
    	List<TipoVerificacion> listaTiposVerificacion = new ArrayList<>();
    	
    	switch(numeroTiempos) {
    		case "4"://no motocicleta y no motocarro
    			if(!tipoVehiculo.equals(ConstantesTiposVehiculo.MOTO) 
    					&& !tipoVehiculo.equals(ConstantesTiposVehiculo.MOTO_CARRO)
    					   && !tipoVehiculo.equals(ConstantesTiposVehiculo.CICLOMOTOR)
    					    && !tipoVehiculo.equals(ConstantesTiposVehiculo.TRICIMOTO)
    					    && !tipoVehiculo.equals(ConstantesTiposVehiculo.CUATRIMOTO)
    					    && !tipoVehiculo.equals(ConstantesTiposVehiculo.CUADRICICLO)
    					    && !tipoVehiculo.equals(ConstantesTiposVehiculo.MOTOTRICICLO)) {
    				
    				listaTiposVerificacion.add(TipoVerificacion.VEHICULO_OTTO);
    				
    			}else {    			
    				listaTiposVerificacion.add(TipoVerificacion.MOTOCICLETA_4T);
    			}
    		break;	
    		case "2"://motocicleta o motocarro 2 tiempos
    			listaTiposVerificacion.add(TipoVerificacion.MOTOCICLETA_2T);
    		break;	
    	}
    	
    	
    	String consulta = "SELECT Max(v) FROM VerificacionGasolina v WHERE v.numeroSerieEquipo=:serieEquipo "
    			+ " AND v.tipoVerificacion in :tiposVerificacion";
    	TypedQuery<VerificacionGasolina> query = em.createQuery(consulta, VerificacionGasolina.class);
    	query.setParameter("serieEquipo",serieEquipo);
    	query.setParameter("tiposVerificacion",listaTiposVerificacion);
    	try{
    		VerificacionGasolina verificacionGasolina = query.getSingleResult();
    		return verificacionGasolina;
    	}catch(NoResultException nre){
    		System.out.println("No se encuentra verificacion para:" + serieEquipo);
    		return null;
    	}
    	
    }
    
    public VerificacionGasolina obtenerVerificacionGasolina(String serieEquipo){
    	if(serieEquipo == null || serieEquipo.isEmpty())
    		throw new IllegalArgumentException("Especifique serie de equipo para consultar verificacion de Gasolina");
    	
    	String consulta = "SELECT Max(v) FROM VerificacionGasolina v WHERE v.numeroSerieEquipo=:serieEquipo ";    			
    	TypedQuery<VerificacionGasolina> query = em.createQuery(consulta, VerificacionGasolina.class);
    	query.setParameter("serieEquipo",serieEquipo);    	
    	try{
    		VerificacionGasolina verificacionGasolina = query.getSingleResult();
    		return verificacionGasolina;
    	}catch(NoResultException nre){
    		System.out.println("No se encuentra verificacion para:" + serieEquipo);
    		return null;
    	}
    	
    }
    
    
    public List<VerificacionGasolina> consultarVerificaciones(Date fechaInicial,Date fechaFinal){
    	
    	String consulta = "SELECT v FROM VerificacionGasolina v " +
    					 "WHERE v.fechaVerificacion BETWEEN :fechaInicial AND :fechaFinal";
    	TypedQuery<VerificacionGasolina> query = em.createQuery(consulta, VerificacionGasolina.class);
    	query.setParameter("fechaInicial", fechaInicial);
    	query.setParameter("fechaFinal", fechaFinal);
    	List<VerificacionGasolina> verificaciones = null;
    	try{
    		verificaciones = query.getResultList();
    	}catch(NoResultException nre){
    		System.out.println("No se encontraron verificaciones ");
    	}
    	return verificaciones;
    }
    
    
}
