
package com.proambiente.webapp.dao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.proambiente.modelo.TipoDocumentoIdentidad;

/**
 *
 * @author FABIAN
 */
@Stateless
public class TipoDocumentoIdentidadFacade extends AbstractFacade<TipoDocumentoIdentidad> {
    @PersistenceContext 
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TipoDocumentoIdentidadFacade() {
        super(TipoDocumentoIdentidad.class);
    }
    
}
