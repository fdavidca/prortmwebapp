
package com.proambiente.webapp.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.proambiente.modelo.LineaVehiculo;
import com.proambiente.modelo.Marca;

/**
 *
 * @author FABIAN
 */
@Stateless
public class MarcaFacade extends AbstractFacade<Marca> {
	
    @PersistenceContext
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MarcaFacade() {
        super(Marca.class);
    }
    
    public List<Marca> findMarcasByNombre(String nombre){
    	String query = "SELECT m FROM Marca m  WHERE m.nombre LIKE :nombre ";
    	Query q = em.createQuery(query);
    	q.setParameter("nombre", nombre.toUpperCase() + '%');
    	return q.<Marca>getResultList();
    }
    
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<LineaVehiculo> findLineaVehiculosByMarca(Marca marca){    	
    	Marca m  = em.find(Marca.class,marca.getMarcaId());
    	m.getLineaVehiculos().size();
    	List<LineaVehiculo> listaVehiculos = m.getLineaVehiculos();
    	listaVehiculos.size();
    	return listaVehiculos;
    	
    }
    
    public void guardarLinea(LineaVehiculo linea){
    	
    	String query = "SELECT MAX(l.lineaVehiculoId) FROM LineaVehiculo l";
    	Query q = em.createQuery(query);
    	Integer lineaId = (Integer)q.getSingleResult();
    	linea.setLineaVehiculoId(lineaId+1);
    	em.persist(linea);
    }
    
    public void guardarMarca(Marca m){
    	String query = "SELECT MAX(m.marcaId) from Marca m";
    	Query q = em.createQuery(query);
    	Integer marcaId = (Integer)q.getSingleResult();
    	m.setMarcaId(marcaId+1);
    	create(m);
    }
    
}
