
package com.proambiente.webapp.dao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.proambiente.modelo.PruebaFuga;

/**
 *
 * @author FABIAN
 */
@Stateless
public class PruebaFugaFacade extends AbstractFacade<PruebaFuga> {
    @PersistenceContext 
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PruebaFugaFacade() {
        super(PruebaFuga.class);
    }
    
    public PruebaFuga buscarUltimaPruebaFugas(String serial){
    	String consulta = "SELECT Max(p) FROM PruebaFuga p WHERE p.serialEquipo =:serial";
    	TypedQuery<PruebaFuga> query = em.createQuery(consulta, PruebaFuga.class);
    	query.setParameter("serial",serial);
    	try{
    		PruebaFuga prueba = query.getSingleResult();
    		return prueba;
    	}catch(NoResultException nre){
    		nre.printStackTrace();
    		return null;
    	}
    }
    
}
