package com.proambiente.webapp.dao;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.proambiente.modelo.Pipeta;


@Stateless
public class PipetaFacade extends AbstractFacade<Pipeta> {

	@PersistenceContext
	EntityManager em;
	
	public PipetaFacade(){
		super(Pipeta.class);
	}

	@Override
	protected EntityManager getEntityManager() {		
		return em;
	}

	@Override
	public void create(Pipeta pipeta) {
		
		if( !pipeta.getAgotada() ) {
			pipeta.setFechaAgotada(null);
		}
		super.create(pipeta);
	}

	@Override
	public void remove(Pipeta pipeta) {
		pipeta.setAnulada(Boolean.TRUE);
		edit(pipeta);
	}

	@Override
	public List<Pipeta> findAll() {
		
		List<Pipeta> pipetas =  super.findAll();
		return pipetas.stream().filter(p->!p.getAnulada()).collect(Collectors.toList());
	}
	
	
	
	
	
}
