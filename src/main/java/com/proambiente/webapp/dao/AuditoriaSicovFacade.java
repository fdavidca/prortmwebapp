package com.proambiente.webapp.dao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.proambiente.modelo.RegistroAuditoria;

@Stateless
public class AuditoriaSicovFacade extends AbstractFacade<RegistroAuditoria> {

	 @PersistenceContext
	 private EntityManager em;

	
	public AuditoriaSicovFacade() {
		super(RegistroAuditoria.class);		
	}

	@Override
	protected EntityManager getEntityManager() {		
		return em;
	}

}
