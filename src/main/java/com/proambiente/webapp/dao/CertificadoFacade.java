
package com.proambiente.webapp.dao;

import com.proambiente.modelo.Certificado;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author FABIAN
 */
@Stateless
public class CertificadoFacade extends AbstractFacade<Certificado> {
    @PersistenceContext 
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CertificadoFacade() {
        super(Certificado.class);
    }
    
}
