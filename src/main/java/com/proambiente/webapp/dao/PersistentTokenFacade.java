
package com.proambiente.webapp.dao;

import com.proambiente.modelo.PersistentToken;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author FABIAN
 */
@Stateless
public class PersistentTokenFacade extends AbstractFacade<PersistentToken> {
    @PersistenceContext 
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PersistentTokenFacade() {
        super(PersistentToken.class);
    }
    
}
