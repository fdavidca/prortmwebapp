
package com.proambiente.webapp.dao;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;

import com.proambiente.modelo.Vehiculo;
import com.proambiente.modelo.Vehiculo_;

/**
 *
 * @author FABIAN
 */
@Stateless
public class VehiculoFacade extends AbstractFacade<Vehiculo> {
    @PersistenceContext 
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public VehiculoFacade() {
        super(Vehiculo.class);
    }
    
    public Vehiculo findVehicuoByPlaca(String placa){
    	Vehiculo v = null;
    	try{
	    	String consulta = "SELECT MAX(v) FROM Vehiculo v WHERE v.placa = :placa";
	    	TypedQuery<Vehiculo> query = em.createQuery(consulta, Vehiculo.class);
	    	query.setParameter("placa", placa);
	    	v = query.getSingleResult();
    	}catch(NoResultException exc){
    		v = null;
    		System.out.println("Vehiculo no preexistente en bd");
    	}
    	return v;    	
    }
    
    
    public Vehiculo findVehiculoEager(Integer vehiculoId,SingularAttribute<Vehiculo,?>... atributos){
    	CriteriaBuilder cb = 	em.getCriteriaBuilder();
    	CriteriaQuery<Vehiculo> query = cb.createQuery(Vehiculo.class);
    	Root<Vehiculo> root = query.from(Vehiculo.class);
    	Arrays.asList(atributos).stream().forEach( atributo-> root.fetch(atributo,JoinType.INNER));//iner join con los atributos
    	query.select(root);
    	query.where( cb.equal(root.get(Vehiculo_.vehiculoId), vehiculoId) );
    	Vehiculo vehiculo = em.createQuery(query).getSingleResult();    	
    	return vehiculo;
    }
    
    public Vehiculo findVehiculoLoadGraph(Integer vehiculoId,SingularAttribute<Vehiculo,?>... atributos){
    	EntityGraph<Vehiculo> graph = em.createEntityGraph(Vehiculo.class);
    	graph.addAttributeNodes(atributos);
    	Map hints = new HashMap();
    	hints.put("javax.persistence.loadgraph", graph);
    	
    	Vehiculo vehiculo = em.find(Vehiculo.class, vehiculoId,hints);    	
    	return vehiculo;
    	
    }
    
    public Vehiculo findVehicuoByVIN(String vin){
    	Vehiculo v = null;
    	try{
	    	String consulta = "SELECT MAX(v) FROM Vehiculo v WHERE v.vin = :vin";
	    	TypedQuery<Vehiculo> query = em.createQuery(consulta, Vehiculo.class);
	    	query.setParameter("vin", vin);
	    	v = query.getSingleResult();
    	}catch(NoResultException exc){
    		v = null;
    		System.out.println("Vehiculo no preexistente en bd");
    	}
    	return v;    	
    }
    
}
