package com.proambiente.webapp.dao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.proambiente.modelo.Pista;

@Stateless
public class PistaFacade extends AbstractFacade<Pista>{

	public PistaFacade() {
		super(Pista.class);		
	}

	@PersistenceContext 
    private EntityManager em;
	
	@Override
	protected EntityManager getEntityManager() {
		return em;	
	}

}
