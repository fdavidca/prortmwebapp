
package com.proambiente.webapp.dao;

import com.proambiente.modelo.CategoriaDefecto;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author FABIAN
 */
@Stateless
public class CategoriaDefectoFacade extends AbstractFacade<CategoriaDefecto> {
    @PersistenceContext
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CategoriaDefectoFacade() {
        super(CategoriaDefecto.class);
    }
    
}
