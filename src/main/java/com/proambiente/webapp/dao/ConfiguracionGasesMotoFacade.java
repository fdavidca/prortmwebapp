
package com.proambiente.webapp.dao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.proambiente.modelo.ConfiguracionGasesMoto;


/**
 *
 * @author FABIAN
 */
@Stateless
public class ConfiguracionGasesMotoFacade extends AbstractFacade<ConfiguracionGasesMoto> {
    @PersistenceContext 
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ConfiguracionGasesMotoFacade() {
        super(ConfiguracionGasesMoto.class);
    }
    
    
    public ConfiguracionGasesMoto getConfiguracionGasesMoto(){
    	return this.find(1);
    }   
    
}
