
package com.proambiente.webapp.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.proambiente.modelo.Color;

/**
 *
 * @author FABIAN
 */
@Stateless
public class ColorFacade extends AbstractFacade<Color> {
	
    @PersistenceContext 
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ColorFacade() {
        super(Color.class);
    }
    
    
    public List<Color> findColoresByNombre(String nombre){
    	String consulta = "SELECT c  FROM Color c WHERE c.nombre LIKE :nombre ORDER BY c.nombre";
    	Query query = em.createQuery(consulta);
    	query.setParameter("nombre",nombre.toUpperCase() + "%");
    	return query.getResultList();    	
    }
    
}
