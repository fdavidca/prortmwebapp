
package com.proambiente.webapp.dao;

import com.proambiente.modelo.TipoMedida;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author FABIAN
 */
@Stateless
public class TipoMedidaFacade extends AbstractFacade<TipoMedida> {
    @PersistenceContext 
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TipoMedidaFacade() {
        super(TipoMedida.class);
    }
    
}
