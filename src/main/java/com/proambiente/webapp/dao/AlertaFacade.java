
package com.proambiente.webapp.dao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.proambiente.modelo.Alerta;

/**
 *
 * @author FABIAN
 */
@Stateless
public class AlertaFacade extends AbstractFacade<Alerta> {
	
	public AlertaFacade() {
		super(Alerta.class);		
	}


	@PersistenceContext
	protected EntityManager em;

	
	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	

}
