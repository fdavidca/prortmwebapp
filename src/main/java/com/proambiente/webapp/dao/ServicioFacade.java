
package com.proambiente.webapp.dao;

import com.proambiente.modelo.Servicio;

import java.util.Optional;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author FABIAN
 */
@Stateless
public class ServicioFacade extends AbstractFacade<Servicio> {
    @PersistenceContext 
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ServicioFacade() {
        super(Servicio.class);
    }
    
    public Optional<Servicio> findServicioByNombre(String nombre) {
		try {
			nombre = nombre.toUpperCase();
			String query = "SELECT s FROM Servicio s  WHERE UPPER(s.nombre) =:nombre ";
			Query q = em.createQuery(query);
			q.setParameter("nombre", nombre.toUpperCase());
			return Optional.of((Servicio)q.getSingleResult());
		} catch (NoResultException nre) {
			return Optional.empty();
		}
	}
    
}
