
package com.proambiente.webapp.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.proambiente.modelo.Defecto;
import com.proambiente.modelo.DefectoAsociado;
import com.proambiente.modelo.EstadoDefecto;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.dto.DefectoDTO;

/**
 *
 * @author FABIAN
 */
@Stateless
public class DefectoFacade extends AbstractFacade<Defecto> {
    @PersistenceContext 
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DefectoFacade() {
        super(Defecto.class);
    }
    
    public void registrarDefectos(List<DefectoDTO> listaDefectos){
    	Prueba p = null;
    	
    	for(DefectoDTO d:listaDefectos){
    		Defecto defecto = em.getReference(Defecto.class,d.getDefectoId());
    		System.out.println("Defecto: " + d.getDefectoId()+" Prueba: " + d.getPruebaId());
    			p = em.getReference(Prueba.class, d.getPruebaId());
    			if(p.getDefectosAsociados() == null){
    				p.setDefectosAsociados( new ArrayList<DefectoAsociado>());
    			}		
    		
    			DefectoAsociado defectoAsociado = new DefectoAsociado();
    			defectoAsociado.setDefecto(defecto);
    			defectoAsociado.setPrueba(p);
    			EstadoDefecto estado = EstadoDefecto.values()[d.getAsociacion()];
    			defectoAsociado.setEstadoDefecto(estado);
    		p.getDefectosAsociados().add(defectoAsociado); 
    		if(p!= null){
        		em.merge(p);
        	}    
    	}
    		
    }
    

    public void registrarDefectos(List<DefectoDTO> listaDefectos,Prueba p){
    	
    	
		
		System.out.println("Medidas guardadas");
		
		Prueba pReference = em.getReference(Prueba.class, p.getPruebaId());
    	for(DefectoDTO d:listaDefectos){
    		
    		Defecto defecto = em.getReference(Defecto.class,d.getDefectoId());
    		System.out.println("Defecto: " + d.getDefectoId()+" Prueba: " + d.getPruebaId());
    			
    			if(pReference.getDefectosAsociados() == null){
    				pReference.setDefectosAsociados( new ArrayList<DefectoAsociado>());
    			}		
    		
    		DefectoAsociado defectoAsociado = new DefectoAsociado();
    		defectoAsociado.setPrueba(pReference);
    		defectoAsociado.setDefecto(defecto);
    		EstadoDefecto estado = EstadoDefecto.values()[d.getAsociacion()];
    		defectoAsociado.setEstadoDefecto(estado);
    		pReference.getDefectosAsociados().add(defectoAsociado); 
    		    
    	}
    	if(pReference!= null){
    		em.merge(pReference);
    	}
    		
    }
    
}
