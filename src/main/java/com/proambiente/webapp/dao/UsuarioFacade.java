
package com.proambiente.webapp.dao;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.proambiente.modelo.Rol;
import com.proambiente.modelo.Usuario;
import com.proambiente.webapp.service.PasswordHash;

/**
 *
 * @author FABIAN
 */
@Stateless
public class UsuarioFacade {
    @PersistenceContext 
    private EntityManager em;

    @Inject
    PasswordHash passwordHash;

    public UsuarioFacade() {
      
    }
    
    
    public Usuario buscarUsuarioPorNombreUsuario(String nombreUsuario){
    	String consulta = "SELECT u FROM Usuario u  WHERE u.aliasUsuario =:aliasUsuario";
    	TypedQuery<Usuario> query = em.createQuery(consulta, Usuario.class);
    	query.setParameter("aliasUsuario",nombreUsuario);
    	try{
    		Usuario u =  query.getSingleResult();
    		u.getRols().size();
    		return u;
    	}catch(NoResultException nre){
    		return null;
    	}  	
    	   	
    }
    
    
    public Usuario guardarUsuario(Usuario usuario,List<Rol> roles,String password) throws NoSuchAlgorithmException, InvalidKeySpecException{
    	List<Rol> rolesAttached = new ArrayList<Rol>();
    	for(Rol r:roles){
    		Rol rol = em.find(Rol.class, r.getRolId());
    		rolesAttached.add(rol);
    	}
    	if( password != null && !password.isEmpty()){
    		String hashPassword = PasswordHash.createHash(usuario.getContrasenia());
    		usuario.setContrasenia(hashPassword);
    	}
    	usuario.setRols(rolesAttached);
    	Usuario usuarioMerged = em.merge(usuario);
    	return usuarioMerged;
    }
    
    public Usuario atachUsuario(Usuario usuario){
    	Usuario usuarioAtached = em.find(Usuario.class, usuario.getUsuarioId());
    	usuarioAtached.getRols().size();
    	return usuarioAtached;
    }
    
    public List<Usuario> findAll(){
    	Query query = em.createNamedQuery("Usuario.findAll");    	
    	List<Usuario> usuarios = query.<Usuario>getResultList();
    	for(Usuario u:usuarios){
    		u.getRols().size();
    	}
    	return usuarios;    	
    }
    
    
    public List<Usuario> findUsuariosAdministradores(){
    	TypedQuery<Usuario> typedQuery = em.createQuery("SELECT u FROM Usuario u JOIN u.rols r WHERE r.nombreRol='ADMINISTRADOR'", Usuario.class);
    	List<Usuario> usuariosAdministradores;
    	try{
    		usuariosAdministradores = typedQuery.getResultList();
    	}catch(NoResultException nre){
    		usuariosAdministradores = new ArrayList<>();
    	}
    	return usuariosAdministradores;
    }
    
    public Optional<Usuario> buscarUsuarioPorDocumento(Long documento) {
    	TypedQuery<Usuario> typedQuery = em.createQuery("SELECT u FROM Usuario u  WHERE u.cedula =:documento",Usuario.class);
    	typedQuery.setParameter("documento", documento);
    	try {
    		Usuario usuario = typedQuery.getSingleResult();
    		return Optional.of(usuario);
    	}catch(NoResultException nre) {
    		return Optional.empty();
    	}
    	
    }
    
    public Usuario find(Integer id){
    	Usuario usuario = em.find(Usuario.class,id);
    	return usuario;
    }
    
    public Usuario getReference(Integer id) {
    	return em.getReference(Usuario.class,id);
    }


	public void edit(Usuario u) {
		em.merge(u);		
	}
    
}
