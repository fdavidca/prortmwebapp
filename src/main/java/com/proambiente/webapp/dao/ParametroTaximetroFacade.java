
package com.proambiente.webapp.dao;

import com.proambiente.modelo.ParametroPruebaTaximetro;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author FABIAN
 */
@Stateless
public class ParametroTaximetroFacade extends AbstractFacade<ParametroPruebaTaximetro> {
    @PersistenceContext 
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ParametroTaximetroFacade() {
        super(ParametroPruebaTaximetro.class);
    }
    
}
