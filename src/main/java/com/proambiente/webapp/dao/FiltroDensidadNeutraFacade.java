package com.proambiente.webapp.dao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.proambiente.modelo.FiltroDensidadNeutra;

@Stateless
public class FiltroDensidadNeutraFacade extends AbstractFacade<FiltroDensidadNeutra> {

	@PersistenceContext
	EntityManager em;
	
	public FiltroDensidadNeutraFacade(){
		super(FiltroDensidadNeutra.class);
	}

	@Override
	protected EntityManager getEntityManager() {		
		return em;
	}
	
	
}
