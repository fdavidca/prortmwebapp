package com.proambiente.webapp.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.proambiente.modelo.Equipo;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Prueba_;
import com.proambiente.modelo.dto.EquipoDTO;

@Stateless
public class EquipoFacade extends AbstractFacade<Equipo>{

	public EquipoFacade() {
		super(Equipo.class);		
	}

	@PersistenceContext 
    private EntityManager em;
	
	@Override
	protected EntityManager getEntityManager() {
		return em;	
	}
	
	public List<Equipo> consultarEquipoPorTipo(Integer tipoEquipo){
		String queryString = "SELECT e FROM Equipo e WHERE e.tipoEquipo.tipoEquipoId =:tipo_equipo_id";
		Query query = em.createQuery(queryString);
		query.setParameter("tipo_equipo_id", tipoEquipo);
		List<Equipo> listaEquipos = query.<Equipo>getResultList();
		return listaEquipos;
	}
	
	public List<EquipoDTO> consultarEquiposPorIds(List<String> idsEquipo){
		String queryString = "SELECT "
				+ "NEW com.proambiente.modelo.dto.EquipoDTO(e.serieEquipo,e.tipoEquipo.tipoEquipoId) "
				+ "FROM Equipo e "
				+ "WHERE e.serieEquipo "
				+ "IN :listaIds";
		Query query = em.createQuery(queryString);
		query.setParameter("listaIds", idsEquipo);
		List<EquipoDTO> listaEquipos = query.getResultList();
		return listaEquipos;
	}
	
	public List<String> consultarEquiposPrueba(Integer pruebaId){
			
		EntityGraph<Prueba> graph = em.createEntityGraph(Prueba.class);
		graph.addAttributeNodes(Prueba_.equipos,Prueba_.pruebaId);
		Map<String,Object> hints = new HashMap<>();
		hints.put("javax.persistence.fetchgraph",graph);
		Prueba prueba = em.find(Prueba.class,pruebaId,hints);
		return prueba.getEquipos().stream().map(e->e.getSerieEquipo()).collect(Collectors.toList());
		
	}
	
	public List<Equipo> consultarEquiposPorPrueba(Integer pruebaId){
		
		EntityGraph<Prueba> graph = em.createEntityGraph(Prueba.class);
		graph.addAttributeNodes(Prueba_.equipos,Prueba_.pruebaId);
		Map<String,Object> hints = new HashMap<>();
		hints.put("javax.persistence.fetchgraph",graph);
		Prueba prueba = em.find(Prueba.class,pruebaId,hints);
		return prueba.getEquipos();
		
	}
	
	public List<String> consultarEquiposPorPruebaId(Integer pruebaId){
		
		String consulta = " SELECT e.serieEquipo FROM Prueba p INNER JOIN p.equipos e WHERE p.pruebaId =:pruebaId";
		Query query = em.createQuery(consulta);
		query.setParameter("pruebaId", pruebaId);
		return query.getResultList();
				
		
	}

}
