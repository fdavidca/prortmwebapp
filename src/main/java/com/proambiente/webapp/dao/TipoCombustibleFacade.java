
package com.proambiente.webapp.dao;

import com.proambiente.modelo.TipoCombustible;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author FABIAN
 */
@Stateless
public class TipoCombustibleFacade extends AbstractFacade<TipoCombustible> {
    @PersistenceContext 
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TipoCombustibleFacade() {
        super(TipoCombustible.class);
    }
    
}
