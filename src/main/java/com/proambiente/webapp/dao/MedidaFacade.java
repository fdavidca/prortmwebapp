
package com.proambiente.webapp.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;



import com.proambiente.modelo.Medida;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.TipoMedida;
import com.proambiente.modelo.dto.MedidaDTO;

/**
 *
 * @author FABIAN
 */
@Stateless
public class MedidaFacade extends AbstractFacade<Medida> {
	
	
	private static final Logger logger = Logger.getLogger(MedidaFacade.class.getName());
	
    @PersistenceContext 
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MedidaFacade() {
        super(Medida.class);
    }

	public List<Medida> guardarMedidas(List<MedidaDTO> listaMedida) {
		List<Medida> medidas = new ArrayList<>();
		for(MedidaDTO m:listaMedida){
			Prueba p = em.getReference(Prueba.class, m.getPruebaId());
			TipoMedida tm = em.getReference(TipoMedida.class, m.getTipoMedidaId());
			Medida medida = new Medida();
			medida.setTipoMedida(tm);
			medida.setPrueba(p);
			medida.setValor(m.getValorMedida());
			em.persist(medida);
			medidas.add(medida);
			logger.info("Medida id :" + medida.getMedidaId());
			logger.info("Valor: " + medida.getValor());
		}
		return medidas;
		
	}
	
	public List<Medida> consultarMedidaProfundidadLabrado(Integer revisionId){
		Query query = em.createQuery("SELECT m FROM Medida m "
				+ "JOIN m.prueba p "
				+ "JOIN m.tipoMedida tp "
				+ "WHERE p.revision.revisionId =:revision_id "
				+ "AND tp.tipoMedidaId IN (5060,5061,5062,5063,5064)");
		query.setParameter("revision_id", revisionId);
		return query.<Medida>getResultList();
	}
	
	public List<Medida> consultarMedidaProfundidadLabrado(List<Prueba> pruebas){
		Query query = em.createQuery("SELECT m FROM Medida m "
				+ "JOIN m.prueba p "
				+ "JOIN m.tipoMedida tp "
				+ "WHERE p IN :pruebas "
				+ "AND tp.tipoMedidaId IN (5060,5061,5062,5063,5064,5072,"
				+ "5073,5074,5082,5083,5084,5092,5093,5094,5066,5067,5076,"
				+ "5077,5086,5087,5096,5097,5068,5069,5078,5079,5088,5089,"
				+ "5098,5099,5070,5071,5080,5081,5090,5091,5100,5101)");		
		query.setParameter("pruebas", pruebas);
		return query.<Medida>getResultList();
	}
    
}
