
package com.proambiente.webapp.dao;

import com.proambiente.modelo.ClaseVehiculo;

import java.util.Optional;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author FABIAN
 */
@Stateless
public class ClaseVehiculoFacade extends AbstractFacade<ClaseVehiculo> {
    @PersistenceContext 
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ClaseVehiculoFacade() {
        super(ClaseVehiculo.class);
    }
    
    public Optional<ClaseVehiculo> findClaseVehiculoByNombre(String nombre) {
		try {
			String query = "SELECT c FROM ClaseVehiculo c  WHERE c.nombre =:nombre ";
			Query q = em.createQuery(query);
			q.setParameter("nombre", nombre.toUpperCase());
			return Optional.of((ClaseVehiculo)q.getSingleResult());
		} catch (NoResultException nre) {
			return Optional.empty();
		}
	}
    
}
