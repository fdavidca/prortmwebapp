
package com.proambiente.webapp.dao;

import com.proambiente.modelo.TipoPrueba;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author FABIAN
 */
@Stateless
public class TipoPruebaFacade extends AbstractFacade<TipoPrueba> {
    @PersistenceContext 
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TipoPruebaFacade() {
        super(TipoPrueba.class);
    }
    
}
