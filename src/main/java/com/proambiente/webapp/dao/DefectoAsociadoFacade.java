
package com.proambiente.webapp.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.proambiente.modelo.Defecto;
import com.proambiente.modelo.DefectoAsociado;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.dto.DefectoDTO;

/**
 *
 * @author FABIAN
 */
@Stateless
public class DefectoAsociadoFacade extends AbstractFacade<DefectoAsociado> {
    @PersistenceContext 
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DefectoAsociadoFacade() {
        super(DefectoAsociado.class);
    }
    
    
    

    
}
