
package com.proambiente.webapp.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.proambiente.modelo.CambioPassword;
import com.proambiente.modelo.CambioPassword_;
import com.proambiente.modelo.Usuario_;

/**
 *
 * @author FABIAN
 */
@Stateless
public class CambioPasswordFacade extends AbstractFacade<CambioPassword> {
	
	public CambioPasswordFacade() {
		super(CambioPassword.class);		
	}


	@PersistenceContext
	private EntityManager em;

	
	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	public List<CambioPassword> consultarHistorial(Integer usuarioId){
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<CambioPassword> cq = cb.createQuery(CambioPassword.class);
		Root<CambioPassword> root = cq.from(CambioPassword.class);
		Predicate predicado = cb.equal(root.get(CambioPassword_.usuario).get(Usuario_.usuarioId),usuarioId);
		cq.select(root);
		cq.where(predicado);
		List<CambioPassword> resultados = em.createQuery(cq).getResultList();
		return resultados;
		
	}

}
