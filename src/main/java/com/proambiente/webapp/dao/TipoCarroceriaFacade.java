package com.proambiente.webapp.dao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.proambiente.modelo.TipoCarroceria;

@Stateless
public class TipoCarroceriaFacade extends AbstractFacade<TipoCarroceria>{
	
	 @PersistenceContext
	    private EntityManager em;

	    @Override
	    protected EntityManager getEntityManager() {
	        return em;
	    }

		public TipoCarroceriaFacade() {
			super(TipoCarroceria.class);
		}  

}
