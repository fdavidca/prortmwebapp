
package com.proambiente.webapp.dao;

import com.proambiente.modelo.Aseguradora;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author FABIAN
 */
@Stateless
public class AseguradoraFacade extends AbstractFacade<Aseguradora> {
    @PersistenceContext
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AseguradoraFacade() {
        super(Aseguradora.class);
    }
    
}
