
package com.proambiente.webapp.dao;



import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


import com.proambiente.modelo.LogoOnac;


/**
 *
 * @author FABIAN
 */
@Stateless
public class LogoOnacFacade extends AbstractFacade<LogoOnac> {
    @PersistenceContext 
    private EntityManager em;
 

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public LogoOnacFacade() {
        super(LogoOnac.class);
    }
    
    
    
}
