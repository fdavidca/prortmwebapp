
package com.proambiente.webapp.dao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.proambiente.modelo.InformacionCda;

/**
 *
 * @author FABIAN
 */
@Stateless
public class InformacionCdaFacade extends AbstractFacade<InformacionCda> {
    @PersistenceContext 
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public InformacionCdaFacade() {
        super(InformacionCda.class);
    }
    
    
    public InformacionCda getInformacionCda(){
    	return em.find(InformacionCda.class, 1);
    }
}
