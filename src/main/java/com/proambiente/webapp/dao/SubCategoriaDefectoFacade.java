
package com.proambiente.webapp.dao;

import com.proambiente.modelo.SubCategoriaDefecto;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author FABIAN
 */
@Stateless
public class SubCategoriaDefectoFacade extends AbstractFacade<SubCategoriaDefecto> {
    @PersistenceContext 
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SubCategoriaDefectoFacade() {
        super(SubCategoriaDefecto.class);
    }
    
}
