
package com.proambiente.webapp.dao;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.proambiente.modelo.CausaAborto;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Prueba_;
import com.proambiente.modelo.Usuario;
import com.proambiente.modelo.dto.PruebaDTO;

/**
 *
 * @author FABIAN
 */
@Stateless
public class PruebaFacade extends AbstractFacade<Prueba> {
    @PersistenceContext 
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PruebaFacade() {
        super(Prueba.class);
    }
    
    
    public void actualizarPrueba(Integer pruebaId,PruebaDTO p){    	
    	Prueba pruebaDesdeBaseDatos = em.find(Prueba.class, pruebaId);
    	if(p!=null && pruebaDesdeBaseDatos != null){
	    	pruebaDesdeBaseDatos.setAbortada(p.isAbortada());
	    	pruebaDesdeBaseDatos.setAprobada(p.isAprobada());
	    	pruebaDesdeBaseDatos.setFinalizada(p.isFinalizada());
	    	pruebaDesdeBaseDatos.setMotivoAborto(p.getComentarioAborto());
	    	if(p.getCausaAbortoId() != null){
	    		CausaAborto ca = em.getReference(CausaAborto.class, p.getCausaAbortoId());
	    		pruebaDesdeBaseDatos.setCausaAborto(ca);
	    	}
	    	if(p.getUsuarioId() != null){
	    		Usuario usuario = em.getReference(Usuario.class, p.getUsuarioId());
	    		pruebaDesdeBaseDatos.setUsuario(usuario);
	    	}
	    	em.merge(pruebaDesdeBaseDatos);
    	}else {
    		throw new RuntimeException("Error actualizando prueba: " + pruebaId + "DTO null" + (p == null) );
    	}
    }
    
    public Prueba obtenerPruebaAnalizador(Integer pruebaId){
    	EntityGraph<Prueba> graph =getEntityManager().createEntityGraph(Prueba.class);
		graph.addAttributeNodes(Prueba_.analizador,Prueba_.pruebaId);
		Map<String,Object> hints = new HashMap<>();
		hints.put("javax.persistence.loadgraph",graph);
		Prueba prueba = getEntityManager().find(Prueba.class,pruebaId,hints);
		return prueba;
    }
    
    public Prueba mergePrueba(Prueba p){
    	return em.merge(p);
    }
    
    public Prueba refrescarPrueba(Prueba p){
    	em.refresh(p);
    	return p;
    }
    
   
}
