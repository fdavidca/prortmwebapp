
package com.proambiente.webapp.dao;

import com.proambiente.modelo.Ciudad;

import java.util.List;
import java.util.Optional;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author FABIAN
 */
@Stateless
public class CiudadFacade extends AbstractFacade<Ciudad> {
    @PersistenceContext 
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CiudadFacade() {
        super(Ciudad.class);
    }
    
    public List<Ciudad> findCiudadByNombre(String nombre){
    	String consulta = "SELECT c  FROM Ciudad c WHERE c.nombre LIKE :nombre ORDER BY c.nombre";
    	Query query = em.createQuery(consulta);
    	query.setParameter("nombre",nombre.toUpperCase() + "%");
    	return query.getResultList();    	
    }
    
	public Optional<Ciudad> buscarCiudadPorNombre(String nombreCiudad) {
		try {
			String query = "SELECT c FROM Ciudad c  WHERE c.nombre =:nombre ";
			Query q = em.createQuery(query);
			q.setParameter("nombre", nombreCiudad.toUpperCase());
			List<Ciudad> ciudades = q.<Ciudad>getResultList();
			if(ciudades == null || ciudades.isEmpty()){
				return Optional.empty();
			}else{
				return Optional.of(ciudades.get(0) );
			}
		} catch (NoResultException nre) {
			return Optional.empty();
		} 

	}
    
}
