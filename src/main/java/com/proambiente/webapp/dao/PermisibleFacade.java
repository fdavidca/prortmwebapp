package com.proambiente.webapp.dao;

import java.sql.Timestamp;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.proambiente.modelo.Permisible;

/**
 *
 * @author FABIAN
 */
@Stateless
public class PermisibleFacade extends AbstractFacade<Permisible> {
	@PersistenceContext
	private EntityManager em;

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	public PermisibleFacade() {
		super(Permisible.class);
	}

	public Permisible buscarPermisible(Integer tipomedida,
			Integer tipovehiculo, Integer modelo) {
		try {
			String consulta = "SELECT Max(p) FROM Permisible p "
					+ "WHERE p.tipoMedida.tipoMedidaId=:tipoMedidaId "
					+ "AND p.tipoVehiculo.tipoVehiculoId=:tipoVehiculoId "
					+ "AND :modelo between p.condicionalMinimo AND p.condicionalMaximo "
					+ "AND p.anulado = false";
			TypedQuery<Permisible> query = em.createQuery(consulta,
					Permisible.class);
			query.setParameter("tipoMedidaId", tipomedida);
			query.setParameter("tipoVehiculoId", tipovehiculo);
			query.setParameter("modelo", Double.valueOf(modelo));
			Permisible p = query.getSingleResult();
			return p;
		} catch (NoResultException nre) {
			return null;
		}
	}
	
	public Permisible buscarPermisibleCondicionalSecundario(Integer tipomedida,
			Integer tipovehiculo, Integer modelo,Integer cilindraje) {
		try {
			String consulta = "SELECT Max(p) FROM Permisible p "
					+ "WHERE p.tipoMedida.tipoMedidaId=:tipoMedidaId "
					+ "AND p.tipoVehiculo.tipoVehiculoId=:tipoVehiculoId "
					+ "AND :modelo between p.condicionalMinimo AND p.condicionalMaximo "
					+ "AND :cilindraje between p.condicionalSecundarioMinimo AND p.condicionalSecundarioMaximo "
					+ "AND p.anulado = false";
			TypedQuery<Permisible> query = em.createQuery(consulta,
					Permisible.class);
			query.setParameter("tipoMedidaId", tipomedida);
			query.setParameter("tipoVehiculoId", tipovehiculo);
			query.setParameter("modelo", Double.valueOf(modelo));
			query.setParameter("cilindraje",Double.valueOf(cilindraje));
			Permisible p = query.getSingleResult();
			return p;
		} catch (NoResultException nre) {
			return null;
		}
	}
	
	
	public Permisible buscarPermisiblePorFecha(Integer tipomedida,
			Integer tipovehiculo, Integer modelo, Timestamp fechaPrueba) {
		try {
			String consulta = "SELECT Max(p) FROM Permisible p "
					+ "WHERE p.tipoMedida.tipoMedidaId=:tipoMedidaId "
					+ "AND p.tipoVehiculo.tipoVehiculoId=:tipoVehiculoId "
					+ "AND :modelo between p.condicionalMinimo AND p.condicionalMaximo "
					+ "AND :fechaPrueba >= p.fechaVigenciaDesde and :fechaPrueba < p.fechaVigenciaHasta ";
			TypedQuery<Permisible> query = em.createQuery(consulta,
					Permisible.class);
			query.setParameter("tipoMedidaId", tipomedida);
			query.setParameter("tipoVehiculoId", tipovehiculo);
			query.setParameter("modelo", Double.valueOf(modelo));
			query.setParameter("fechaPrueba",fechaPrueba);
			Permisible p = query.getSingleResult();
			return p;
		} catch (NoResultException nre) {
			return null;
		}
	}
	
	public Permisible buscarPermisiblePorFechaPorCondicionalSecundario(Integer tipomedida,
			Integer tipovehiculo, Integer modelo, Timestamp fechaPrueba,Integer cilindraje) {
		try {
			String consulta = "SELECT Max(p) FROM Permisible p "
					+ "WHERE p.tipoMedida.tipoMedidaId=:tipoMedidaId "
					+ "AND p.tipoVehiculo.tipoVehiculoId=:tipoVehiculoId "
					+ "AND :modelo between p.condicionalMinimo AND p.condicionalMaximo "
					+ "AND :fechaPrueba >= p.fechaVigenciaDesde and :fechaPrueba < p.fechaVigenciaHasta "
					+ "AND :cilindraje between p.condicionalSecundarioMinimo AND p.condicionalSecundarioMaximo ";
			TypedQuery<Permisible> query = em.createQuery(consulta,
					Permisible.class);
			query.setParameter("tipoMedidaId", tipomedida);
			query.setParameter("tipoVehiculoId", tipovehiculo);
			query.setParameter("modelo", Double.valueOf(modelo));
			query.setParameter("fechaPrueba",fechaPrueba);
			query.setParameter("cilindraje",Double.valueOf(cilindraje));
			Permisible p = query.getSingleResult();
			return p;
		} catch (NoResultException nre) {
			return null;
		}
	}
	

}
