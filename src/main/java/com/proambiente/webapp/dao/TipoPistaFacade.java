
package com.proambiente.webapp.dao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.proambiente.modelo.TipoPista;

/**
 *
 * @author FABIAN
 */
@Stateless
public class TipoPistaFacade extends AbstractFacade<TipoPista> {
    @PersistenceContext 
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TipoPistaFacade() {
        super(TipoPista.class);
    }
    
}
