package com.proambiente.webapp.dao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.proambiente.modelo.TipoEquipo;

@Stateless   
public class TipoEquipoFacade extends AbstractFacade<TipoEquipo>{
	
	public TipoEquipoFacade() {
		super(TipoEquipo.class);
	}

	@PersistenceContext
	EntityManager entityManager;
	
	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}

	
	
	
}
