
package com.proambiente.webapp.dao;



import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.proambiente.modelo.Inspeccion;

/**
 *
 * @author FABIAN
 */
@Stateless
public class InspeccionFacade extends AbstractFacade<Inspeccion> {
    @PersistenceContext 
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public InspeccionFacade() {
        super(Inspeccion.class);
    }
    
}
