package com.proambiente.webapp.service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.Inspeccion;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Usuario;
import com.proambiente.webapp.dao.InformacionCdaFacade;
import com.proambiente.webapp.dao.UsuarioFacade;

@Stateless
public class CerrarRevisionInspeccionService {
	
	@Inject
	RevisionService revisionService;
	
	@Inject
	InspeccionService inspeccionService;
	
	@Inject
	PruebasService pruebasService;
	
	@Inject
	InformacionCdaFacade infoCDADAO;
	
	@Inject
	UsuarioFacade usuarioDAO;
	
	@PersistenceContext 
	EntityManager em;
	
	public void cerrarRevisionInspeccion(Revision revision){
		List<Prueba> listaPruebasInspeccion = pruebasService.pruebasRevision(revision.getRevisionId());
		
		List<Inspeccion> inspecciones = inspeccionService.obtenerInspeccionesPorRevisionId(revision.getRevisionId());
		//revision con dos inspecciones no cerrada
		if(inspecciones.size() >=2 ){
			throw new RuntimeException("Revision aprobada con dos inspecciones no cerrada");			
		}		
		//Cierre de revision segunda inspeccion reprobada
		String evaluacion = revisionService.evaluarRevision( revision.getRevisionId() );
		if(inspecciones != null && !inspecciones.isEmpty() ) {
			Inspeccion inspeccionAnterior = inspecciones.get(0);
			Timestamp fechaInspeccionAnterior = inspeccionAnterior.getFechaInspeccionSiguiente();
			Timestamp fechaInspeccionSiguiente = new Timestamp((new Date()).getTime());
			Inspeccion inspeccionNueva = new Inspeccion();
			
			if(evaluacion.equalsIgnoreCase("aprobada")) {
				inspeccionNueva.setAprobada(true);
			}else if(evaluacion.equalsIgnoreCase("reprobada")){
				inspeccionNueva.setAprobada(false);
			}else {
				throw new RuntimeException("Revision no finalizada");
			}
			
			StringBuilder comentariosSb = new StringBuilder();
			String comentarios = revision.getComentario();
			if (comentarios != null)
				comentariosSb.append(comentarios).append("\t");

			for (Prueba p : listaPruebasInspeccion) {
				if (p.getMotivoAborto() != null)
					comentariosSb.append(p.getMotivoAborto()).append("\t");
			}
			inspeccionNueva.setComentario(revision.getComentario());
			inspeccionNueva.setConsecutivoRunt(revision.getConsecutivoRunt());
			inspeccionNueva.setRevision(revision);
			inspeccionNueva.setFechaInspeccionAnterior(fechaInspeccionAnterior);
			inspeccionNueva.setFechaInspeccionSiguiente(fechaInspeccionSiguiente);
			inspeccionNueva.setNumeroInspeccion(2);
			inspeccionNueva.setPruebas(listaPruebasInspeccion);
			revisionService.actualizarUsuarioResponsable(revision);
			InformacionCda informacionCda  = infoCDADAO.find(1);
			Usuario usuarioResponsable = informacionCda.getUsuarioResp();
			inspeccionNueva.setUsuarioResponsable(usuarioResponsable);
			inspeccionService.crearInspeccion(inspeccionNueva);			
			revisionService.editarRevision(revision);
			cerrarRevision(revision.getRevisionId());
		}
		//cierre de revision primera inspeccion reprobada, vehiculo que no regresó.
		if(inspecciones.isEmpty() && revision.getNumeroInspecciones() == 1) {
			Inspeccion inspeccionNueva = new Inspeccion();
			if(evaluacion.equalsIgnoreCase("aprobada")) {
				inspeccionNueva.setAprobada(true);
			}else if(evaluacion.equalsIgnoreCase("reprobada")){
				inspeccionNueva.setAprobada(false);
			}else {
				throw new RuntimeException("Revision no finalizada");
			}			
			StringBuilder comentariosSb = new StringBuilder();
			String comentarios = revision.getComentario();
			if (comentarios != null)
				comentariosSb.append(comentarios).append("\t");

			for (Prueba p : listaPruebasInspeccion) {
				if (p.getMotivoAborto() != null)
					comentariosSb.append(p.getMotivoAborto()).append("\t");
			}
			inspeccionNueva.setComentario(revision.getComentario());
			inspeccionNueva.setConsecutivoRunt(revision.getConsecutivoRunt());
			inspeccionNueva.setRevision(revision);
			inspeccionNueva.setFechaInspeccionAnterior( revision.getFechaCreacionRevision() );
			//no huvo fecha de inspeccion siguiente
			inspeccionNueva.setFechaInspeccionSiguiente( revision.getFechaCreacionRevision() );
			inspeccionNueva.setNumeroInspeccion(1);
			inspeccionNueva.setPruebas(listaPruebasInspeccion);
			Usuario usuario = usuarioDAO.find( revision.getUsuarioResponsableId() );
			inspeccionNueva.setUsuarioResponsable( usuario );
			inspeccionService.crearInspeccion(inspeccionNueva);		
			cerrarRevision(revision.getRevisionId());
		}
		
	}
	
	public void cerrarRevision(Integer revisionId) {
		String consulta = "UPDATE Revision r SET r.cerrada = true WHERE revisionId =:revisionId";
		Query query = em.createQuery(consulta);
		query.setParameter("revisionId", revisionId);
		query.executeUpdate();		
	}
	

}
