package com.proambiente.webapp.service;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
public class ConsultarComentarioRevisionService {
	
	@PersistenceContext
	EntityManager em;
	
	public String consultarComentarioRevision(Integer revisionId) {
		
		String queryStr = "SELECT r.comentario FROM Revision r WHERE r.revisionId=:revisionId";
		Query query = em.createQuery(queryStr);
		query.setParameter("revisionId",revisionId);
		String comentarios = (String) query.getSingleResult();		
		return comentarios;		
	}

}
