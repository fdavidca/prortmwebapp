package com.proambiente.webapp.service;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.proambiente.modelo.InformacionCda;
import com.proambiente.webapp.dao.InformacionCdaFacade;

@Stateless
public class VersionSoftwareService {
	
	
	private static final Logger logger = Logger.getLogger(VersionSoftwareService.class.getName());
	
	@Inject
	InformacionCdaFacade infoCdaFacade;
	
	public String obtenerVersionSoftware() {
		try {
			
			InformacionCda infoCda = infoCdaFacade.find(1);
			String versionSoftware = infoCda.getVersionSoftware();
			if(versionSoftware == null) {				
				logger.log(Level.INFO, "Información de version de software no configurada");
				versionSoftware = "0.0.1.0";
			}
			return versionSoftware;
		}catch(Exception exc) {
			logger.log(Level.SEVERE, "Error consultando version de software configurada en bd", exc);
			throw new RuntimeException("Error consultando version de software configurada en bd",exc);
		}
	}
	

}
