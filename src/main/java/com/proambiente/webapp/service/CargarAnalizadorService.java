package com.proambiente.webapp.service;

import java.io.Serializable;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

import com.proambiente.modelo.Analizador;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Prueba_;

@Stateless
public class CargarAnalizadorService implements Serializable{

	private static final long serialVersionUID = 3567079198270784595L;
	@PersistenceContext
	EntityManager em;
	
	public Analizador cargarAnalizador(Integer pruebaId) {
		
		CriteriaBuilder cb = em.getCriteriaBuilder();

		  CriteriaQuery<Analizador> q = cb.createQuery(Analizador.class);
		  Root<Prueba> c = q.from(Prueba.class);
		  ParameterExpression<Integer> p = cb.parameter(Integer.class);
		  q.select(c.get(Prueba_.analizador)).where(cb.equal(c.get(Prueba_.pruebaId), p));
		  
		  TypedQuery<Analizador> query = em.createQuery(q);		  
		  query.setParameter(p, pruebaId);
		  try {
			  Analizador analizador = query.getSingleResult();
			  return analizador;
		  }catch(NoResultException nre) {
			  return null;
		  }
	}
	
}
