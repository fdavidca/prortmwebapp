package com.proambiente.webapp.service;

import java.util.Date;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.proambiente.modelo.LogoOnac;

@Stateless
public class ConsultarLogoOnacVigenteService {
	
	@PersistenceContext
	EntityManager em;
	
	
	public LogoOnac consultarLogoOnac( Date fechaRevision) {
		
		String queryStr = "SELECT MAX(l) FROM LogoOnac l where l.anulado = false and l.fechaVigenciaDesde <:fecha AND l.fechaVigenciaHasta >=:fecha ";
		Query query = em.createQuery(queryStr);
		query.setParameter("fecha", fechaRevision);
		LogoOnac logo = (LogoOnac) query.getSingleResult();
		return logo;
	}

}
