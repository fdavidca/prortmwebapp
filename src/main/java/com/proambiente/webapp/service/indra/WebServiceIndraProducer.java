package com.proambiente.webapp.service.indra;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.xml.ws.BindingProvider;

import com.proambiente.indra.cliente.Sicov;
import com.proambiente.indra.cliente.SicovSoap;
import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.NivelAlerta;
import com.proambiente.modelo.OperadorSicov;
import com.proambiente.webapp.dao.InformacionCdaFacade;
import com.proambiente.webapp.service.notification.NotificationPrimariaService;

@ApplicationScoped
public class WebServiceIndraProducer implements Serializable{
	
	private static final Logger logger = Logger.getLogger(WebServiceIndraProducer.class.getName());
	/**
	 * 
	 */
	private static final long serialVersionUID = 2210L;
	@Inject
	InformacionCdaFacade informacionCdaFacade;
	
	@Inject
	NotificationPrimariaService notificationService;
	
	SicovSoap sicovSoap;
	
	@Produces @SicovSoapConfigurado
	public SicovSoap crearDatosSoap(){
		if(sicovSoap == null) {
			return crearInstancianueva();
		}else {
			return sicovSoap;
		}
	}
	
	
	public SicovSoap crearInstancianueva(){
		InformacionCda infoCda = informacionCdaFacade.find(1);
		String urlWebService = infoCda.getUrlWebService();
		SicovSoap ss = null;
		try {
			if(infoCda.getOperadorSicov().equals(OperadorSicov.INDRA)){
				URL url = new URL(urlWebService);				
				//URL url = new URL("http://localhost/wsdl/IndraService.wsdl");
				Sicov sicov = new Sicov(url);
				
				ss = sicov.getSicovSoap();
				
				 BindingProvider bindingProvider = (BindingProvider) ss;
				    bindingProvider.getRequestContext().put(
				          BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
				          urlWebService);
				    
				    logger.log(Level.INFO, "url " + urlWebService);   
				
				((BindingProvider)ss).getRequestContext().put("javax.xml.ws.client.connectionTimeout", new Integer(1000*60*1000));
				((BindingProvider)ss).getRequestContext().put("javax.xml.ws.client.receiveTimeout", new Integer(1000*60*1000));
			}
		} catch (MalformedURLException e) {
			throw new RuntimeException("Url del web service mal configurada" + urlWebService); 
		} catch(Exception exc){
			logger.log(Level.SEVERE, "Error no se pudo crear cliente de webservice indra", exc);
			notificationService.sendNotification("Error no se pudo crear cliente de webservice sicov", NivelAlerta.ERROR_GRAVE, true);
			throw new RuntimeException("No se puede crear cliente de webservice indra", exc);
		}
		return ss;
	}

}
