package com.proambiente.webapp.service.indra;

import static com.proambiente.webapp.service.indra.CodificadorFurIndra.SEPARADOR_SECCION;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import static com.proambiente.webapp.service.indra.CodificadorFurIndra.SEPARADOR_CAMPO;

public class CodificadorFurAsociados {
		
	public void codificarInfoFurAsociados(StringBuilder sb,Integer numeroRevision,Integer numeroInspeccion,Optional<LocalDateTime> fechaPrimeraInspeccionOpt ) {
		
		sb.append(numeroRevision).append("-").append(numeroInspeccion);
		sb.append(SEPARADOR_CAMPO);
		if(fechaPrimeraInspeccionOpt.isPresent()) {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
			String fechaFormateada = formatter.format(fechaPrimeraInspeccionOpt.get());
			sb.append(fechaFormateada);
		}			
		sb.append(SEPARADOR_SECCION);
		
	}
	
}
