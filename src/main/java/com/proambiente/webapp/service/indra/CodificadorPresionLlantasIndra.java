package com.proambiente.webapp.service.indra;

import java.util.List;

import com.proambiente.modelo.Medida;
import com.proambiente.webapp.service.generadordto.GeneradorPresionLlantasDTO;
import com.proambiente.webapp.util.dto.ResultadoPresionLlantasDTO;

import static com.proambiente.webapp.service.indra.CodificadorFurIndraV2.SEPARADOR_CAMPO;
import static com.proambiente.webapp.service.indra.CodificadorFurIndraV2.SEPARADOR_SECCION;

public class CodificadorPresionLlantasIndra {
	
	GeneradorPresionLlantasDTO generadorDTO = new GeneradorPresionLlantasDTO();

	public void codificarPresionLlantas(List<Medida> medidas,StringBuilder sb) {
		ResultadoPresionLlantasDTO resultado = generadorDTO.generarResultadoPresionLlantas(medidas);
		//23
		sb.append(resultado.getPresionLlantaEje1Der1()).append(SEPARADOR_CAMPO);
		//24
		sb.append(resultado.getPresionLlantaEje2Der2()).append(SEPARADOR_CAMPO);
		//25
		sb.append(resultado.getPresionLlantaEje3Der2()).append(SEPARADOR_CAMPO);
		//26
		sb.append(resultado.getPresionLlantaEje4Der2()).append(SEPARADOR_CAMPO);
		//27
		sb.append(resultado.getPresionLlantaEje5Der2()).append(SEPARADOR_CAMPO);
		//28
		sb.append(resultado.getPresionLlantaEje1Izq1()).append(SEPARADOR_CAMPO);
		//29
		sb.append(resultado.getPresionLlantaEje2Izq2()).append(SEPARADOR_CAMPO);
		//30
		sb.append(resultado.getPresionLlantaEje3Izq2()).append(SEPARADOR_CAMPO);
		//31
		sb.append(resultado.getPresionLlantaEje4Izq2()).append(SEPARADOR_CAMPO);
		//32
		sb.append(resultado.getPresionLlantaEje5Izq2()).append(SEPARADOR_CAMPO);
		//33
		sb.append(resultado.getPresionLlantaEje2Der1()).append(SEPARADOR_CAMPO);
		//34
		sb.append(resultado.getPresionLlantaEje3Der1()).append(SEPARADOR_CAMPO);
		//35
		sb.append(resultado.getPresionLlantaEje4Der1()).append(SEPARADOR_CAMPO);
		//36		
		sb.append(resultado.getPresionLlantaEje5Der1()).append(SEPARADOR_CAMPO);
		//37
		sb.append(resultado.getPresionLlantaEje2Izq1()).append(SEPARADOR_CAMPO);
		//38
		sb.append(resultado.getPresionLlantaEje3Izq1()).append(SEPARADOR_CAMPO);
		//39
		sb.append(resultado.getPresionLlantaEje4Izq1()).append(SEPARADOR_CAMPO);
		//40
		sb.append(resultado.getPresionLlantaEje5Izq1()).append(SEPARADOR_CAMPO);
		//41
		sb.append(resultado.getPresionLlantaRepuestoDer()).append(SEPARADOR_CAMPO);
		//42
		sb.append(resultado.getPresionLLantaRepuestoIzq()).append(SEPARADOR_SECCION);
		
		
		
	}
	
	
}
