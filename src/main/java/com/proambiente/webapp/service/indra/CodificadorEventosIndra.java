package com.proambiente.webapp.service.indra;

import static com.proambiente.webapp.util.ConstantesIndra.ID_PROVEEDOR;
import static com.proambiente.webapp.util.ConstantesIndra.SEPARADOR_SECCION;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.proambiente.modelo.dto.PruebaDTO;
import com.proambiente.webapp.util.ConstantesTiposPrueba;

public class CodificadorEventosIndra {

	Map<Integer, String> mapaNombresPruebaIndra = new HashMap<>();

	public CodificadorEventosIndra() {
		super();
		mapaNombresPruebaIndra.put(ConstantesTiposPrueba.TIPO_PRUEBA_GASES, "Gases");
		mapaNombresPruebaIndra.put(ConstantesTiposPrueba.TIPO_PRUEBA_FOTO, "Foto");
		mapaNombresPruebaIndra.put(ConstantesTiposPrueba.TIPO_PRUEBA_LUCES, "Luces");
		mapaNombresPruebaIndra.put(ConstantesTiposPrueba.TIPO_PRUEBA_RUIDO, "Ruidos");
		mapaNombresPruebaIndra.put(ConstantesTiposPrueba.TIPO_PRUEBA_INSPECCION_SENSORIAL, "Visual");
		mapaNombresPruebaIndra.put(ConstantesTiposPrueba.TIPO_PRUEBA_TAXIMETRO, "Taximetro");
		mapaNombresPruebaIndra.put(ConstantesTiposPrueba.TIPO_PRUEBA_FRENOS, "Frenos");
		mapaNombresPruebaIndra.put(ConstantesTiposPrueba.TIPO_PRUEBA_DESVIACION, "Alineacion");
		mapaNombresPruebaIndra.put(ConstantesTiposPrueba.TIPO_PRUEBA_SUSPENSION, "Suspension");
	}

	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public String codificarEvento(PruebaDTO prueba, String placa, Date fechaRevision, String serialesEquipos,
			String causaAborto, Integer tipoEventoPrueba) {

		String format = "%s|%s|%s|%s|%s|%s|%s";
		StringBuilder sb = new StringBuilder();
		Integer tipoPruebaId = prueba.getTipoPruebaId();

		sb.append(ID_PROVEEDOR).append(SEPARADOR_SECCION);
		sb.append(sdf.format(fechaRevision)).append(SEPARADOR_SECCION);
		sb.append(mapaNombresPruebaIndra.get(tipoPruebaId)).append(SEPARADOR_SECCION);
		sb.append(placa).append(SEPARADOR_SECCION);
		sb.append(serialesEquipos).append(SEPARADOR_SECCION);
		sb.append(tipoEventoPrueba).append(SEPARADOR_SECCION);
		sb.append(causaAborto);
		return sb.toString();
	}

	public String codificarEventoNuevaResolucion(PruebaDTO prueba, String placa, Date fechaRevision, String serialesEquipos,
			String causaAborto, Integer tipoEventoPrueba,String idRuntCda) {

		StringBuilder sb = new StringBuilder();
		Integer tipoPruebaId = prueba.getTipoPruebaId();

		sb.append(ID_PROVEEDOR).append(SEPARADOR_SECCION);
		sb.append(sdf.format(fechaRevision)).append(SEPARADOR_SECCION);
		sb.append(mapaNombresPruebaIndra.get(tipoPruebaId)).append(SEPARADOR_SECCION);
		sb.append(placa).append(SEPARADOR_SECCION);
		sb.append(serialesEquipos).append(SEPARADOR_SECCION);
		sb.append(tipoEventoPrueba).append(SEPARADOR_SECCION);
		sb.append(causaAborto).append(SEPARADOR_SECCION);
		sb.append(idRuntCda);
		return sb.toString();
	}

}
