package com.proambiente.webapp.service.indra;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.proambiente.modelo.Defecto;
import com.proambiente.modelo.Llanta;
import com.proambiente.modelo.Medida;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Usuario;
import com.proambiente.modelo.dto.ResultadoTaximetroDTO;
import com.proambiente.webapp.service.generadordto.GeneradorDTOResultados;
import com.proambiente.webapp.service.util.UtilInformesMedidas;

import static com.proambiente.webapp.service.indra.CodificadorFurIndra.SEPARADOR_CAMPO;
import static com.proambiente.webapp.service.indra.CodificadorFurIndra.SEPARADOR_SECCION;

public class CodificadorResultadoTaximetro {

	UtilInformesMedidas utilInformesMedidas = new UtilInformesMedidas();
	GeneradorDTOResultados generadorResultadosDTO = new GeneradorDTOResultados();

	public void codificarResultadoTaximetro(StringBuilder sb, List<Medida> medidas, List<Defecto> defectos,
			Usuario usuario, String refLlanta, Boolean aplicaTaximetro) {
		// 1 Nombres y apellidos de usuario que realizo
		sb.append(usuario.getNombres() + " " + usuario.getApellidos()).append(SEPARADOR_CAMPO);
		// 2 identificacion de usuario
		sb.append(usuario.getCedula() != null ? usuario.getCedula() : "").append(SEPARADOR_CAMPO);
		utilInformesMedidas.ordenarListaMedidas(medidas);
		ResultadoTaximetroDTO resultado = generadorResultadosDTO.generarResultadoTaximetro(medidas, defectos,
				aplicaTaximetro, refLlanta);
		// 3 aplica taximetro
		sb.append(resultado.getAplicaTaximetro() != null ? resultado.getAplicaTaximetro() : "").append(SEPARADOR_CAMPO);
		// 4 tiene taximetro
		sb.append(resultado.getTieneTaximetro() != null ? resultado.getTieneTaximetro() : "").append(SEPARADOR_CAMPO);
		// 5 taximetro visible
		sb.append(resultado.getTaximetroVisible() != null ? resultado.getTaximetroVisible() : "")
				.append(SEPARADOR_CAMPO);
		// 6 referencia llanta
		sb.append(resultado.getReferenciaLlanta() != null ? resultado.getReferenciaLlanta() : "")
				.append(SEPARADOR_CAMPO);
		// 7 error distancia
		sb.append(resultado.getErrorDistancia() != null ? resultado.getErrorDistancia() : "").append(SEPARADOR_CAMPO);
		// 8 error tiempo
		sb.append(resultado.getErrorTiempo() != null ? resultado.getErrorTiempo() : "").append(SEPARADOR_SECCION);
	}

	public String informacionReferenciaLlanta(List<Prueba> pruebas) {
		// buscar la prueba de taximetro
		String nombreLlanta = "";
		Optional<Prueba> pruebaTaximetroOpt = pruebas.stream()
				.filter(p -> p.getTipoPrueba().getTipoPruebaId().equals(9)).findAny();
		if (pruebaTaximetroOpt.isPresent()) {// si la prueba esta presente
			Prueba pruebaTaximetro = pruebaTaximetroOpt.get();
			String motivoAborto = pruebaTaximetro.getMotivoAborto();
			if (motivoAborto != null && !motivoAborto.isEmpty()) {
				String[] cadenaSeparada = motivoAborto.split(":");
				if (cadenaSeparada != null) {
					if (cadenaSeparada.length == 2) {
						nombreLlanta = cadenaSeparada[1];

					}
				}

			}

		}
		return nombreLlanta;
	}
	
	public void codificarTaximetroNoRequerido(StringBuilder sb) {
		sb.append(";");
		sb.append(";");
		sb.append("FALSE");
		sb.append(";");
		sb.append(";");
		sb.append(";");
		sb.append(";");
		sb.append(";");
		sb.append(SEPARADOR_SECCION);

	}

}
