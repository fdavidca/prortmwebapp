package com.proambiente.webapp.service.indra;

import static org.apache.commons.codec.binary.Base64.decodeBase64;

import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.engines.AESFastEngine;
import org.bouncycastle.crypto.modes.CBCBlockCipher;
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.ParametersWithIV;



/**
 * Clase para encriptar la información en el formato que pide indra
 * 
 * @author fdavi
 *
 */
public class IndraEncryptor {

	private final static String alg = "AES";
    // Definición del modo de cifrado a utilizar
    private final static String cI = "AES/CBC/NoPadding";
 
    /**
     * Función de tipo String que recibe una llave (key), un vector de inicialización (iv)
     * y el texto que se desea cifrar
     * @param key la llave en tipo String a utilizar
     * @param iv el vector de inicialización a utilizar
     * @param cleartext el texto sin cifrar a encriptar
     * @return el texto cifrado en modo String
     * @throws Exception puede devolver excepciones de los siguientes tipos: NoSuchAlgorithmException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, NoSuchPaddingException
     */
    public static String encrypt(String keyP, String ivP, String cleartext) throws Exception {    	

    	SecretKeySpec   key = new SecretKeySpec(keyP.getBytes(), "AES");
    	IvParameterSpec iv  = new IvParameterSpec(ivP.getBytes());

    	Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
    	
    	int blockSize = cipher.getBlockSize();
    	
    	byte[] dataBytes = cleartext.getBytes();
    	
    	int plaintextLength = dataBytes.length;
    	int remainder = plaintextLength % blockSize;
    	if(remainder != 0){
    		plaintextLength += (blockSize - remainder);
    	}

    	byte[] plaintext = new byte[plaintextLength];
    	
    	System.arraycopy(dataBytes, 0, plaintext, 0, dataBytes.length);
    	
    	cipher.init(Cipher.ENCRYPT_MODE, key, iv);
    	byte[] output = cipher.doFinal(plaintext);

    	String decriptada = Base64.encodeBase64String(output);
    	return decriptada;    	
    }
	
	/**
     * Función de tipo String que recibe una llave (key), un vector de inicialización (iv)
     * y el texto que se desea descifrar
     * @param key la llave en tipo String a utilizar
     * @param iv el vector de inicialización a utilizar
     * @param encrypted el texto cifrado en modo String
     * @return el texto desencriptado en modo String
     * @throws Exception puede devolver excepciones de los siguientes tipos: NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException
     */
    public  static String decrypt(String key, String iv, String encrypted) throws Exception {
            Cipher cipher = Cipher.getInstance(cI);
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), alg);
            IvParameterSpec ivParameterSpec = new IvParameterSpec(iv.getBytes());
            byte[] enc =Base64.decodeBase64(encrypted);
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivParameterSpec);
            byte[] decrypted = cipher.doFinal(enc);
            return new String(decrypted);
    }

}
