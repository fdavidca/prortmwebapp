package com.proambiente.webapp.service.indra;

import static com.proambiente.webapp.service.indra.CodificadorFurIndra.SEPARADOR_SECCION;
import static com.proambiente.webapp.service.indra.CodificadorFurIndra.SEPARADOR_CAMPO;


public class CodificadorInfoRunt {
		
	
	public void codificarInfoRunt(StringBuilder sb,String consecutivoRunt,String numeroCda) {
		sb.append(consecutivoRunt).append(SEPARADOR_CAMPO).append(numeroCda).append(SEPARADOR_SECCION);
	}
}
