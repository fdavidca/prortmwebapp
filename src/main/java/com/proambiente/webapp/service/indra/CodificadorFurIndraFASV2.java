package com.proambiente.webapp.service.indra;

import java.util.List;

import com.proambiente.modelo.Medida;
import com.proambiente.modelo.Usuario;
import com.proambiente.modelo.dto.ResultadoFasDTO;
import com.proambiente.webapp.service.generadordto.GeneradorDTOResultados;
import static com.proambiente.webapp.service.indra.CodificadorFurIndra.SEPARADOR_SECCION;
import static com.proambiente.webapp.service.indra.CodificadorFurIndra.SEPARADOR_CAMPO;

public class CodificadorFurIndraFASV2 {

	GeneradorDTOResultados generadorDTOResultados = new GeneradorDTOResultados();

	public void codificarResultadoFrenos(List<Medida> medidas, StringBuilder sb, Usuario usuario, Integer numeroEjes) {
		ResultadoFasDTO resultado = generadorDTOResultados.generarResultadoFAS(medidas);
		// 1 Nombres y apellidos de usuario que realizo
		sb.append(usuario.getNombres() + " " + usuario.getApellidos()).append(SEPARADOR_CAMPO);
		// 2 identificacion de usuario
		sb.append(usuario.getCedula()).append(SEPARADOR_CAMPO);
		// 3 ejes
		sb.append(numeroEjes).append(SEPARADOR_CAMPO);
		// 4 eficacia total
		sb.append(resultado.getEficaciaTotal()).append(SEPARADOR_CAMPO);
		// 5 eficacia auxiliar
		sb.append(resultado.getEficaciaAuxiliar()).append(SEPARADOR_CAMPO);
		// 6 desequilibrio der eje 1
		sb.append(resultado.getDesequilibrioEje1()).append(SEPARADOR_CAMPO);
		// 7 desequilibrio der eje 2
		sb.append(resultado.getDesequilibrioEje2()).append(SEPARADOR_CAMPO);
		// 8 desequilibrio der eje 3
		sb.append(resultado.getDesequilibrioEje3()).append(SEPARADOR_CAMPO);
		// 9 desequilibrio der eje 4
		sb.append(resultado.getDesequilibrioEje4()).append(SEPARADOR_CAMPO);
		// 10 desequilibrio der eje 5
		sb.append(resultado.getDesequilibrioEje5()).append(SEPARADOR_CAMPO);
		// 11 desequilibrio der eje 6
		sb.append(resultado.getDesequilibrioEje6()).append(SEPARADOR_CAMPO);
		//12 fuerza frenado izquierda
		sb.append(resultado.getFuerzaFrenadoIzqEje1()).append(SEPARADOR_CAMPO);
		//13 fuerza frenado izquierda
		sb.append(resultado.getFuerzaFrenadoIzqEje2()).append(SEPARADOR_CAMPO);
		//14 fuerza frenado izquierda
		sb.append(resultado.getFuerzaFrenadoIzqEje3()).append(SEPARADOR_CAMPO);
		//15 fuerza frenado izquierda
		sb.append(resultado.getFuerzaFrenadoIzqEje4()).append(SEPARADOR_CAMPO);
		//16 fuerza frenado izquierda
		sb.append(resultado.getFuerzaFrenadoIzqEje5()).append(SEPARADOR_CAMPO);
		//17 fuerza frenado izquierda
		sb.append(resultado.getFuerzaFrenadoIzqEje6()).append(SEPARADOR_CAMPO);
		//18 fuerza frenado derecha
		sb.append(resultado.getFuerzaFrenadoDerEje1()).append(SEPARADOR_CAMPO);
		//19 fuerza frenado derecha
		sb.append(resultado.getFuerzaFrenadoDerEje2()).append(SEPARADOR_CAMPO);
		//20 fuerza frenado derecha
		sb.append(resultado.getFuerzaFrenadoDerEje3()).append(SEPARADOR_CAMPO);
		//21 fuerza frenado derecha
		sb.append(resultado.getFuerzaFrenadoDerEje4()).append(SEPARADOR_CAMPO);
		//22 fuerza frenado derecha
		sb.append(resultado.getFuerzaFrenadoDerEje5()).append(SEPARADOR_CAMPO);
		//23 fuerza frenado derecha
		sb.append(resultado.getFuerzaFrenadoDerEje6()).append(SEPARADOR_CAMPO);
		//24 peso derecho 
		sb.append(resultado.getPesoDerEje1()).append(SEPARADOR_CAMPO);
		//25 peso derecho 
		sb.append(resultado.getPesoDerEje2()).append(SEPARADOR_CAMPO);
		//26 peso derecho 
		sb.append(resultado.getPesoDerEje3()).append(SEPARADOR_CAMPO);
		//27 peso derecho 
		sb.append(resultado.getPesoDerEje4()).append(SEPARADOR_CAMPO);
		//28 peso derecho 
		sb.append(resultado.getPesoDerEje5()).append(SEPARADOR_CAMPO);
		//29 peso derecho 
		sb.append(resultado.getPesoDerEje6()).append(SEPARADOR_CAMPO);
		//30 peso izquierdo
		sb.append(resultado.getPesoIzqEje1()).append(SEPARADOR_CAMPO);
		//31 peso izquierdo
		sb.append(resultado.getPesoIzqEje2()).append(SEPARADOR_CAMPO);
		//32 peso izquierdo
		sb.append(resultado.getPesoIzqEje3()).append(SEPARADOR_CAMPO);
		//33 peso izquierdo
		sb.append(resultado.getPesoIzqEje4()).append(SEPARADOR_CAMPO);
		//34 peso izquierdo
		sb.append(resultado.getPesoIzqEje5()).append(SEPARADOR_CAMPO);
		//35 peso izquierdo
		sb.append(resultado.getPesoIzqEje6()).append(SEPARADOR_CAMPO);
		//36 desviacion lateral
		sb.append(resultado.getDesviacionLateralEje1()).append(SEPARADOR_CAMPO);
		//37 desviacion lateral
		sb.append(resultado.getDesviacionLateralEje2()).append(SEPARADOR_CAMPO);
		//38 desviacion lateral
		sb.append(resultado.getDesviacionLateralEje3()).append(SEPARADOR_CAMPO);
		//39 desviacion lateral
		sb.append(resultado.getDesviacionLateralEje4()).append(SEPARADOR_CAMPO);
		//40 desviacion lateral
		sb.append(resultado.getDesviacionLateralEje5()).append(SEPARADOR_CAMPO);
		//41 desviacion lateral
		sb.append(resultado.getDesviacionLateralEje6()).append(SEPARADOR_CAMPO);
		//42 suspension
		sb.append(resultado.getSuspIzqEje1()).append(SEPARADOR_CAMPO);
		//43 suspension
		sb.append(resultado.getSuspIzqEje2()).append(SEPARADOR_CAMPO);
		//44 suspension
		sb.append(resultado.getSuspDerEje1()).append(SEPARADOR_CAMPO);
		//45 suspension
		sb.append(resultado.getSuspDerEje2()).append(SEPARADOR_CAMPO);
		//46 fuerza auxiliar total derecho
		sb.append(resultado.getFuerzaDerechaAuxiliar()).append(SEPARADOR_CAMPO);		
		//47 peso total derecho
		sb.append(resultado.getPesoDerechoTotal()).append(SEPARADOR_CAMPO);
		//48 fuerza de mano total izquierda
		sb.append(resultado.getFuerzaIzquierdaAuxiliar()).append(SEPARADOR_CAMPO);
		//49 peso total izquierdo
		sb.append(resultado.getPesoIzquierdoTotal()).append(SEPARADOR_SECCION);
		
	}

}
