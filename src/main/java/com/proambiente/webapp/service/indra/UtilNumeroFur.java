package com.proambiente.webapp.service.indra;

import com.proambiente.modelo.Revision;

public class UtilNumeroFur {
	
	public static String generarNumeroFur(Revision revision) {
		String numeroFur = String.valueOf(revision.getRevisionId()) +"-"+ revision.getNumeroInspecciones();
		return numeroFur;
	}

}
