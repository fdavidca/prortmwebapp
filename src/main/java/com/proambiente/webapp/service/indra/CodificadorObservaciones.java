package com.proambiente.webapp.service.indra;

import java.util.List;
import java.util.stream.Collectors;

import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Revision;

public class CodificadorObservaciones {
	
	public void codificarObservaciones(StringBuilder sb, Revision revision, List<Prueba> pruebas) {
		String comentarioRevision = revision.getComentario() != null ? revision.getComentario() : "";
		String comentariosSinComas = comentarioRevision.replace('.',',');
		comentariosSinComas = comentariosSinComas.replace("\n", " ");
		String comentariosPruebas = pruebas.stream().filter(p -> !( p.getMotivoAborto() == null || p.getMotivoAborto().trim().isEmpty() ) )
				.map( p -> p.getMotivoAborto().replaceAll("\\r|\\n","_") ).collect( Collectors.joining("_") );
		
		sb.append(comentariosSinComas).append(comentariosPruebas).append("_");
		
	}

}
