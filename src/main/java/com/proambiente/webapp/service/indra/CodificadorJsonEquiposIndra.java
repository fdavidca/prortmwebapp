package com.proambiente.webapp.service.indra;


import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.proambiente.modelo.Analizador;
import com.proambiente.modelo.Equipo;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.TipoPrueba;
import com.proambiente.webapp.service.CargarAnalizadorService;
import com.proambiente.webapp.service.PruebasService;
import com.proambiente.webapp.util.ConstantesTiposPrueba;
import com.proambiente.webapp.util.dto.InformacionEquipoDTO;

public class CodificadorJsonEquiposIndra {
	
	public String codificarInformacionEquipos(List<Prueba> pruebas,
											  PruebasService pruebaService,			
											  CargarAnalizadorService cargarAnalizadorService) 
											  throws JsonProcessingException {
		List<InformacionEquipoDTO> infoEquipos = new ArrayList<>();
		for (Prueba p : pruebas) {
			
			List<Equipo> equipos = pruebaService.obtenerEquipos(p);
			Analizador analizador = cargarAnalizadorService.cargarAnalizador(p.getPruebaId());
			if (analizador != null) {
				InformacionEquipoDTO infoEquipoDTO = construirInfoEquipo(analizador);
				infoEquipos.add(infoEquipoDTO);
			}
			if(equipos != null && !equipos.isEmpty()) {
				for (Equipo e : equipos) {
					InformacionEquipoDTO infoEquipoDTO = construirInfoEquipo(e,p);
					infoEquipos.add(infoEquipoDTO);
				}
			}
			
			
		}//final de for pruebas
		
		ObjectMapper mapper = new ObjectMapper();
		String jsonEquipos  = mapper.writeValueAsString( infoEquipos );
		StringBuilder sb = new StringBuilder();
		sb.append("{ ").append("\"Equipos\"").append(":").append(jsonEquipos).append(" } ");
		
		return sb.toString();
	}
	
	public InformacionEquipoDTO construirInfoEquipo(Analizador analizador) {
		
		DecimalFormat df = new DecimalFormat("0.000");
		InformacionEquipoDTO infoEquipo = new InformacionEquipoDTO();
		
		infoEquipo.setNombre(analizador.getTipo());
		infoEquipo.setMarca(analizador.getMarca());
		infoEquipo.setNoserie( analizador.getSerialAnalizador() != null? analizador.getSerialAnalizador() : "");
		infoEquipo.setPef( analizador.getPef() != null ? df.format( analizador.getPef() ) : "" );
		if(analizador.getTipo().equalsIgnoreCase("OPACIMETRO")) {
			if(analizador.getMarca().equalsIgnoreCase("Capelec" )) {
				infoEquipo.setLtoe("215");
			}else if (analizador.getMarca().equalsIgnoreCase("Sensors")) {
				infoEquipo.setLtoe("430");
			}
		}
		
		infoEquipo.setLtoe("");
		infoEquipo.setNoSerieBench(analizador.getSerialAnalizador());
		infoEquipo.setPrueba("1");
		infoEquipo.setEsperiferico("N");
		
		return infoEquipo;
	}
	
	public InformacionEquipoDTO construirInfoEquipo(Equipo equipo,Prueba prueba) {
		InformacionEquipoDTO infoEquipo = new InformacionEquipoDTO();
		infoEquipo.setNombre( equipo.getTipoEquipo().getNombreTipoEquipo() );
		String fabricante = equipo.getFabricante() != null ? equipo.getFabricante() : "";
		String modelo = equipo.getModeloEquipo() != null ? equipo.getModeloEquipo() : "";
		infoEquipo.setMarca( fabricante + " " + modelo);
		infoEquipo.setNoserie( equipo.getSerieEquipo() );
		infoEquipo.setPef("");
		infoEquipo.setLtoe("");
		infoEquipo.setNoSerieBench("");
		infoEquipo.setEsperiferico("S");
		int pruebaint = transformarIdPrueba(prueba.getTipoPrueba());
		infoEquipo.setPrueba(String.valueOf(pruebaint));
		return infoEquipo;
	}
	
	
	public int transformarIdPrueba(TipoPrueba tipoPrueba) {
		switch(tipoPrueba.getTipoPruebaId()) {
			case ConstantesTiposPrueba.TIPO_PRUEBA_INSPECCION_SENSORIAL:
				return 4;
			case ConstantesTiposPrueba.TIPO_PRUEBA_GASES:
				return 1;
			case ConstantesTiposPrueba.TIPO_PRUEBA_LUCES:
				return 2;
			case ConstantesTiposPrueba.TIPO_PRUEBA_FRENOS:
				return 6;
			case ConstantesTiposPrueba.TIPO_PRUEBA_TAXIMETRO:
				return 5;
			case ConstantesTiposPrueba.TIPO_PRUEBA_DESVIACION:
				return 7;
			case ConstantesTiposPrueba.TIPO_PRUEBA_SUSPENSION:
				return 8;
			case ConstantesTiposPrueba.TIPO_PRUEBA_RUIDO:
				return 9;
			default:
				return 0;
		}
	}

}
