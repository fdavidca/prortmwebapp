package com.proambiente.webapp.service.indra;

import java.util.List;
import java.util.stream.Collectors;

import com.proambiente.modelo.Usuario;

import static com.proambiente.webapp.service.indra.CodificadorFurIndra.SEPARADOR_CAMPO;
import static com.proambiente.webapp.service.indra.CodificadorFurIndra.SEPARADOR_SECCION;

public class CodificadorDatosDefectos {
	
	public void codificarDatosDefectos(StringBuilder sb, List<Integer> defectos, Usuario usuario) {
		// 1 Nombres y apellidos de usuario que realizo
		sb.append(usuario.getNombres() + " " + usuario.getApellidos()).append(SEPARADOR_CAMPO);
		// 2 identificacion de usuario
		sb.append(usuario.getCedula()).append(SEPARADOR_CAMPO);
		String defectosConcatenados = defectos.stream().filter(codigo->codigo != null).map(codigo -> String.valueOf(codigo))
				.collect(Collectors.joining("_"));
		sb.append(defectosConcatenados).append(SEPARADOR_SECCION);

	}

}
