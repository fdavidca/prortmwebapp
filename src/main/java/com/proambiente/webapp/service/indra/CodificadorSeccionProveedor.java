package com.proambiente.webapp.service.indra;

import static com.proambiente.webapp.util.ConstantesIndra.ID_PROVEEDOR;

import static com.proambiente.webapp.service.indra.CodificadorFurIndra.SEPARADOR_SECCION;


public class CodificadorSeccionProveedor {
	
	public void codificarSeccionProveedor(StringBuilder sb) {
		sb.append(ID_PROVEEDOR).append(SEPARADOR_SECCION);
	}
	

}
