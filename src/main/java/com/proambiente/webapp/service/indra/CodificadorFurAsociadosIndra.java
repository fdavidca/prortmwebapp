package com.proambiente.webapp.service.indra;

import java.text.SimpleDateFormat;
import java.util.Date;

import static com.proambiente.webapp.service.indra.CodificadorFurIndraV2.SEPARADOR_SECCION;

public class CodificadorFurAsociadosIndra {

	public void codificarFurAsociados(StringBuilder sb, String numeroFur, Date fechaHoraFur) {		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		sb.append(numeroFur).append(sdf.format(fechaHoraFur)).append(SEPARADOR_SECCION);		
	}
	
}
