package com.proambiente.webapp.service.indra;

public class UtilTipoMotor {
	
	public static Integer getTipoMotor(Integer numeroTiemposMotor) {
		switch (numeroTiemposMotor) {
		case 4:
			return 2;
		case 2:
			return 1;
		default:
			return -1;
		}
	}
	

}
