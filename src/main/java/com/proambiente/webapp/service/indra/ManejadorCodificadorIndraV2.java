package com.proambiente.webapp.service.indra;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.proambiente.webapp.service.indra.CodificadorFurIndra.SEPARADOR_CAMPO;
import static com.proambiente.webapp.service.indra.CodificadorFurIndra.SEPARADOR_SECCION;
import static com.proambiente.webapp.service.sicov.UtilEncontrarPruebas.encontrarPruebaPorTipo;
import static com.proambiente.webapp.service.util.UtilTipoVehiculo.isVehiculoMoto;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.proambiente.modelo.Analizador;
import com.proambiente.modelo.Defecto;
import com.proambiente.modelo.Foto;
import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.Inspeccion;
import com.proambiente.modelo.Medida;
import com.proambiente.modelo.Propietario;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Revision_;
import com.proambiente.modelo.Usuario;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.webapp.dao.FotoFacade;
import com.proambiente.webapp.dao.InformacionCdaFacade;
import com.proambiente.webapp.dao.UsuarioFacade;
import com.proambiente.webapp.service.CargarAnalizadorService;
import com.proambiente.webapp.service.CodigosDefectoNuevaResolucionService;
import com.proambiente.webapp.service.EvaluarRevisionService;
import com.proambiente.webapp.service.EvaluarRevisionService.EstadoRevision;
import com.proambiente.webapp.service.InspeccionService;
import com.proambiente.webapp.service.PruebasService;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.service.sicov.OperacionesSicov.Envio;
import com.proambiente.webapp.util.ConstantesNombreSoftware;
import com.proambiente.webapp.util.ConstantesTiposCombustible;
import com.proambiente.webapp.util.ConstantesTiposPrueba;
import com.proambiente.webapp.util.ConstantesTiposVehiculo;

public class ManejadorCodificadorIndraV2 {
	
	CodificadorFurIndraV2 codificador = new CodificadorFurIndraV2();
	
	CodificadorFurLucesIndraV2 codificadorLuces = new CodificadorFurLucesIndraV2();
	
	CodificadorFurIndraFASV2 codificadorFAS = new CodificadorFurIndraFASV2();
	
	CodificadorDatosDefectosNuevaResolucion codificadorDefectos = new CodificadorDatosDefectosNuevaResolucion();
	
	CodificadorResultadoTaximetro codificadorTaximetro = new CodificadorResultadoTaximetro();
	
	CodificadorObservaciones codificadorObservaciones = new CodificadorObservaciones();
	
	CodificadorProfLabradoIndra codificadorProfLabrado = new CodificadorProfLabradoIndra();
	
	@Inject
	RevisionService revisionService;
	
	@Inject
	PruebasService pruebaService;
	
	@Inject
	FotoFacade fotoDAO;
	
	@Inject
	CodigosDefectoNuevaResolucionService defectoService;
	
	@Inject
	InformacionCdaFacade infoCdaFacade;
	
	@Inject
	UsuarioFacade usuarioFacade;
	
	@Inject
	InspeccionService inspeccionService;
	
	@Inject
	CargarAnalizadorService cargarAnalizadorService;
	
	@Inject
	EvaluarRevisionService evaluarRevisionService;
	
	private static final Logger logger = Logger.getLogger(ManejadorCodificadorIndraV2.class.getName());
	
	public String  codificarFurInspeccion(Integer inspeccionId) {
		StringBuilder sbProveedor = generarSeccionIdProveedor();
		Inspeccion inspeccion = inspeccionService.cargarInspeccion(inspeccionId);
		
		Boolean aprobada = inspeccion.getAprobada();
		
		Integer revisionId = inspeccionService.revisionIdDesdeInspeccionId(inspeccionId);
		
		Revision revision = revisionService.cargarRevision(revisionId);	
		
		InformacionCda infoCda = infoCdaFacade.find(1);
		
		StringBuilder sbPropietario = generarSeccionPropietario(revision);
		
		Vehiculo vehiculo = revision.getVehiculo();
		
		Timestamp fechaInspeccion = inspeccion.getFechaInspeccionAnterior();
		
		checkNotNull(vehiculo,"Revision sin vehiculo registrado %d",revision.getRevisionId());
		
		Boolean furDeInspeccion = Boolean.TRUE;
		StringBuilder sbSeccionVehiculo = generarSeccionVehiculo(revision,vehiculo,aprobada,fechaInspeccion,furDeInspeccion);
		
		List<Prueba> pruebasInspeccion = inspeccionService.pruebasInspeccion(inspeccionId);
		
		Analizador analizador = new Analizador();
		Optional<Prueba> pruebaGasesOpt =pruebasInspeccion.stream().filter(p->p.getTipoPrueba().getTipoPruebaId().equals(ConstantesTiposPrueba.TIPO_PRUEBA_GASES)).findAny();
		if(!pruebaGasesOpt.isPresent()){
			throw new RuntimeException("No hay prueba de gases para la revision");
		}
		
		analizador = pruebaGasesOpt.get().getAnalizador();
		checkArgument( pruebasInspeccion != null && ! pruebasInspeccion.isEmpty(),"No existen pruebas para revision %d" , revisionId);
		
		StringBuilder sbSeccionFoto = generarSeccionFoto(revisionId, vehiculo, pruebasInspeccion);

		StringBuilder sbSeccionGases = generarSeccionGases(vehiculo,  pruebasInspeccion,analizador);
		
		StringBuilder sbSeccionLuces = generarSeccionLuces(revisionId, pruebasInspeccion,isVehiculoMoto(vehiculo.getTipoVehiculo()));
		
		StringBuilder sbSeccionFAS = generarSeccionFAS(revisionId, vehiculo,  pruebasInspeccion);
		
		StringBuilder sbInspeccionSensorial  = generarSeccionInspeccionSensorial(revisionId,  pruebasInspeccion);
		
		StringBuilder sbTaximetro = generarSeccionTaximetro(revisionId,vehiculo,pruebasInspeccion);
		
		StringBuilder sbObservaciones = generarObservaciones(revision,  pruebasInspeccion);
		
		StringBuilder sbInfoRunt = generarSeccionRunt( String.valueOf( revision.getRevisionId() )+"-"+inspeccion.getNumeroInspeccion(),infoCda );
		
		StringBuilder sbInfoLlantas = generarInformacionLlantas( pruebasInspeccion );
		
		StringBuilder sbEquipos = generarInformacionEquipos( pruebasInspeccion);
		
		StringBuilder sbSoftware = generarInformacionSoftware(infoCda);
		
		StringBuilder sbDirectorTecnico = generarInformacionDirectorTecnico(revision.getUsuarioResponsableId());
		
		StringBuilder sbFurAsociado = generarInformacionFurAsociados(revision,inspeccion.getNumeroInspeccion(),fechaInspeccion);
		
		StringBuilder sbTotal = new StringBuilder();
		
		sbTotal.append(sbProveedor);
		sbTotal.append(sbPropietario);
		sbTotal.append(sbSeccionVehiculo);
		sbTotal.append(sbSeccionFoto);
		sbTotal.append(sbSeccionGases);
		sbTotal.append(sbSeccionLuces);
		sbTotal.append(sbSeccionFAS);
		sbTotal.append(sbInspeccionSensorial);
		sbTotal.append(sbTaximetro);
		sbTotal.append(sbObservaciones);
		sbTotal.append(sbInfoRunt);
		sbTotal.append(sbInfoLlantas);
		sbTotal.append(sbEquipos);
		sbTotal.append(sbSoftware);
		sbTotal.append(sbDirectorTecnico);
		sbTotal.append(sbFurAsociado);
		
		return sbTotal.toString();
	}
	
	public String codificarSolamenteSeccionRunt(Integer revisionId) {
		revisionService.cargarRevisionDTO(revisionId, Revision_.revisionId,Revision_.numeroInspecciones);
		Revision revision = revisionService.cargarRevision(revisionId);
		String numeroFur = UtilNumeroFur.generarNumeroFur(revision);
		StringBuilder sb = new StringBuilder();
		sb.append(SEPARADOR_SECCION);//seccion 1 proveedor
		sb.append(SEPARADOR_SECCION);//seccion 2 datos del propietario
		sb.append(SEPARADOR_SECCION);//seccion 3 datos del vehiculo
		sb.append(SEPARADOR_SECCION);//seccion 4 fotos 
		sb.append(SEPARADOR_SECCION);//seccion 5 resultado gases
		sb.append(SEPARADOR_SECCION);//seccion 6 inspeccion previa gases
		sb.append(SEPARADOR_SECCION);//seccion 7 resultado luces
		sb.append(SEPARADOR_SECCION);//seccion 8 resultado fas
		sb.append(SEPARADOR_SECCION);//seccion 9 resultado visual
		sb.append(SEPARADOR_SECCION);//seccion 10 resultado taximetro
		sb.append(SEPARADOR_SECCION);//seccion 11 observaciones
		
		InformacionCda infoCda = infoCdaFacade.find(1);
		StringBuilder sbInfoRunt = generarSeccionRunt(numeroFur,infoCda);
		
		sb.append(sbInfoRunt);//seccion 12 info runt
		sb.append(SEPARADOR_SECCION);//seccion 13 llantas
		sb.append(SEPARADOR_SECCION);//seccion equipos
		sb.append(SEPARADOR_SECCION);//seccion software
		sb.append(SEPARADOR_SECCION);//seccion firma
		return sb.toString();
		
	}
	
	
	public String  codificarFurRevision(Integer revisionId,Envio envio) {
		
		if( envio.equals(Envio.SEGUNDO_ENVIO) ){
			
			String segundoEnvio = codificarSolamenteSeccionRunt(revisionId);
			return segundoEnvio;
			
		}
		
		
		StringBuilder sbProveedor = generarSeccionIdProveedor();
		
		
		Revision revision = revisionService.cargarRevision(revisionId);
		List<Inspeccion> inspecciones = inspeccionService.obtenerInspeccionesPorRevisionId(revision.getRevisionId());
		
		
		Timestamp fechaInspeccion = null;
		if(inspecciones != null && !inspecciones.isEmpty()) {
			Inspeccion inspeccion = inspecciones.get(0);
			if(inspeccion != null) {
				fechaInspeccion =  inspeccion.getFechaInspeccionSiguiente();
				
			}
		}else {
			fechaInspeccion = revision.getFechaCreacionRevision();
		}
		
		Boolean aprobada = false;
		EvaluarRevisionService.EstadoRevision evaluacion = evaluarRevisionService.obtenerEstadoRevision(revisionId);
		if(evaluacion.equals(EstadoRevision.NO_FINALIZADA)) {
			throw new RuntimeException("La revision no esta finalizada al momento de hacer el envio : "  + revisionId);
		}else {
			aprobada = evaluacion.equals(EstadoRevision.APROBADA);
		}
		
		InformacionCda infoCda = infoCdaFacade.find(1);
		
		StringBuilder sbPropietario = generarSeccionPropietario(revision);
		
		Vehiculo vehiculo = revision.getVehiculo();		
		
		checkNotNull(vehiculo,"Revision sin vehiculo registrado %d",revision.getRevisionId());
		Boolean furDeInspeccion = Boolean.FALSE;
		StringBuilder sbSeccionVehiculo = generarSeccionVehiculo(revision,vehiculo,aprobada,fechaInspeccion,furDeInspeccion);
		
		List<Prueba> pruebasRevision = pruebaService.pruebasRevision(revisionId);
		
		checkArgument(pruebasRevision != null && !pruebasRevision.isEmpty(),"No existen pruebas para revision %d" , revisionId);
		
		StringBuilder sbSeccionFoto = generarSeccionFoto(revisionId, vehiculo, pruebasRevision);
		
		

		
		Analizador analizador = new Analizador();
		Optional<Prueba> pruebaGasesOpt =pruebasRevision.stream().filter(p->p.getTipoPrueba().getTipoPruebaId().equals(ConstantesTiposPrueba.TIPO_PRUEBA_GASES)).findAny();
		if(!pruebaGasesOpt.isPresent()){
			throw new RuntimeException("No hay prueba de gases para la revision");
		}
		
		analizador = pruebaGasesOpt.get().getAnalizador();
		

		StringBuilder sbSeccionGases = generarSeccionGases(vehiculo, pruebasRevision,analizador);
		
		StringBuilder sbSeccionLuces = generarSeccionLuces(revisionId, pruebasRevision,isVehiculoMoto(vehiculo.getTipoVehiculo()));
		
		StringBuilder sbSeccionFAS = generarSeccionFAS(revisionId, vehiculo, pruebasRevision);
		
		StringBuilder sbInspeccionSensorial  = generarSeccionInspeccionSensorial(revisionId, pruebasRevision);
		
		StringBuilder sbTaximetro = generarSeccionTaximetro(revisionId,vehiculo,pruebasRevision);
		
		StringBuilder sbObservaciones = generarObservaciones(revision, pruebasRevision);
		
		StringBuilder sbInfoRunt = generarSeccionRunt( String.valueOf( revision.getRevisionId() )+"-"+revision.getNumeroInspecciones(),infoCda );
		
		StringBuilder sbInfoLlantas = generarInformacionLlantas(pruebasRevision );
		
		StringBuilder sbEquipos = generarInformacionEquipos(pruebasRevision);
		
		StringBuilder sbSoftware = generarInformacionSoftware(infoCda);
		
		StringBuilder sbDirectorTecnico = generarInformacionDirectorTecnico(revision.getUsuarioResponsableId());
		
		StringBuilder sbFurAsociado = generarInformacionFurAsociados(revision , revision.getNumeroInspecciones(), fechaInspeccion);
		
		StringBuilder sbTotal = new StringBuilder();
		
		sbTotal.append(sbProveedor);
		sbTotal.append(sbPropietario);
		sbTotal.append(sbSeccionVehiculo);
		sbTotal.append(sbSeccionFoto);
		sbTotal.append(sbSeccionGases);
		sbTotal.append(sbSeccionLuces);
		sbTotal.append(sbSeccionFAS);
		sbTotal.append(sbInspeccionSensorial);
		sbTotal.append(sbTaximetro);
		sbTotal.append(sbObservaciones);
		sbTotal.append(sbInfoRunt);
		sbTotal.append(sbInfoLlantas);
		sbTotal.append(sbEquipos);
		sbTotal.append(sbSoftware);
		sbTotal.append(sbDirectorTecnico);
		sbTotal.append(sbFurAsociado);
		
		return sbTotal.toString();
	}
	


	
	private StringBuilder generarInformacionFurAsociados(Revision revision,Integer numeroInspeccion,Timestamp fechaInspeccion) {
		StringBuilder sb = new StringBuilder();
		
		
		String numeroFur = String.valueOf( revision.getRevisionId() ) + "-" + numeroInspeccion;
		sb.append(numeroFur).append(SEPARADOR_CAMPO);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String fechaInspeccionStr = sdf.format(fechaInspeccion);
		sb.append(fechaInspeccionStr);
		return sb;
	}

	private StringBuilder generarInformacionDirectorTecnico(Integer usuarioResponsableId) {
		StringBuilder sb = new StringBuilder();
		Usuario director = usuarioFacade.find(usuarioResponsableId);
		String nombreCompleto = director.getNombres() + " " + director.getApellidos();
		sb.append(nombreCompleto).append(SEPARADOR_CAMPO).append(director.getCedula()).append(SEPARADOR_SECCION);
		return sb;
	}

	private StringBuilder generarInformacionSoftware(InformacionCda infoCda) {
		StringBuilder sb = new StringBuilder();
		String nombreSoftware = ConstantesNombreSoftware.NOMBRE_SOFTWARE;
		String versionSoftware = infoCda.getVersionSoftware();
		String aplicativos = infoCda.getAplicativosUtilizados();
		sb.append(nombreSoftware).append(" ").append(versionSoftware).append(" ");
		sb.append(aplicativos).append(SEPARADOR_SECCION);		
		return sb;
	}

	private StringBuilder generarInformacionEquipos(List<Prueba> pruebasRevision) {		
		StringBuilder sb = new StringBuilder();
		
		CodificadorJsonEquiposIndra codEquipos = new CodificadorJsonEquiposIndra();
		try {
			String equiposCodificados = codEquipos.codificarInformacionEquipos(pruebasRevision, pruebaService,cargarAnalizadorService);
			sb.append(equiposCodificados);
			sb.append(SEPARADOR_SECCION);
		} catch (JsonProcessingException e) {
			logger.log(Level.SEVERE,"Error",e);
			throw new RuntimeException("No es posible serializar equipos a json");
		}
		return sb;
	}

	private StringBuilder generarInformacionLlantas(List<Prueba> pruebasRevision) {
		StringBuilder sb = new StringBuilder();
		List<Medida> medidas = new ArrayList<>();
		Optional<Prueba> optPruebaIv = encontrarPruebaPorTipo(pruebasRevision,ConstantesTiposPrueba.TIPO_PRUEBA_INSPECCION_SENSORIAL);
		CodificadorProfLabradoIndra codificador = new CodificadorProfLabradoIndra();
		Usuario usuario = new Usuario();
		if( optPruebaIv.isPresent() ) {
			Prueba pruebaIv = optPruebaIv.get();
			usuario = pruebaIv.getUsuario();			
			medidas = pruebaService.obtenerMedidas(pruebaIv);		
		}else {
			throw new RuntimeException("No se encuentra prueba de inspeccion visual");
		}
		Optional<Prueba> optPruebaFrenos = encontrarPruebaPorTipo(pruebasRevision,ConstantesTiposPrueba.TIPO_PRUEBA_FRENOS);
		if( optPruebaFrenos.isPresent()) {
			Prueba pruebaFrenos = optPruebaFrenos.get();
			List<Medida> medidasPruebaFrenos = pruebaService.obtenerMedidas(pruebaFrenos);
			medidas.addAll(medidasPruebaFrenos);
		}
		
		codificador.codificarProfundidadLabrado(medidas, usuario, sb);
		
		CodificadorPresionLlantasIndra codificadorPs = new CodificadorPresionLlantasIndra();
		codificadorPs.codificarPresionLlantas(medidas, sb);
		
		return sb;
	}

	private StringBuilder generarObservaciones(Revision revision, List<Prueba> pruebasRevision) {
		StringBuilder sbObservaciones = new StringBuilder();	
		codificadorObservaciones.codificarObservaciones(sbObservaciones, revision, pruebasRevision);	
		sbObservaciones.append(SEPARADOR_SECCION);
		return sbObservaciones;
	}
	
	
	private StringBuilder generarSeccionTaximetro(Integer revisionId, Vehiculo vehiculo, List<Prueba> pruebasRevision) {
		StringBuilder sbTaximetro = new StringBuilder();
		Optional<Prueba> optPruebaTaximetro = encontrarPruebaPorTipo(pruebasRevision,ConstantesTiposPrueba.TIPO_PRUEBA_TAXIMETRO);
		if(optPruebaTaximetro.isPresent()){
			Prueba pruebaTaximetro = optPruebaTaximetro.get();
			Usuario usuarioTaximetro = pruebaTaximetro.getUsuario();
			List<Medida> medidasTaximetro = pruebaService.obtenerMedidas(pruebaTaximetro);
			List<Defecto> defectosTaximetro = pruebaService.obtenerDefectos(pruebaTaximetro);
			String refLlanta = codificadorTaximetro.informacionReferenciaLlanta(pruebasRevision);
			codificadorTaximetro.codificarResultadoTaximetro(sbTaximetro, medidasTaximetro, defectosTaximetro, usuarioTaximetro, refLlanta,Boolean.TRUE);
		}else{				
			logger.log(Level.INFO, "No hay prueba de taximetro para revision " + revisionId);
			codificadorTaximetro.codificarTaximetroNoRequerido(sbTaximetro);
		}
		return sbTaximetro;
	}
	
	private StringBuilder generarSeccionInspeccionSensorial(Integer revisionId, List<Prueba> pruebasRevision) {
		StringBuilder sbInspeccionSensorial = new StringBuilder();
		Optional<Prueba> optionalVisual = encontrarPruebaPorTipo(pruebasRevision,ConstantesTiposPrueba.TIPO_PRUEBA_INSPECCION_SENSORIAL);
		if(optionalVisual.isPresent()){
			
			Prueba pruebaVisual = optionalVisual.get();
			Usuario usuario = pruebaVisual.getUsuario();
			List<Integer> listaIdPruebas = pruebasRevision.stream().map(prueba->prueba.getPruebaId()).collect(Collectors.toList());			
			List<String> codigosDefectos = defectoService.obtenerTodosLosDefectos(listaIdPruebas);
			codificadorDefectos.codificarDatosDefectos(sbInspeccionSensorial, codigosDefectos, usuario);
		}else {
			logger.log(Level.SEVERE, "Error prueba inspeccion visual no presente para revision " + revisionId);
			throw new RuntimeException("Error prueba inspeccion visual no presente para revision " + revisionId);
		}
		return sbInspeccionSensorial;
	}
	
	

	private StringBuilder generarSeccionFAS(Integer revisionId, Vehiculo vehiculo, List<Prueba> pruebasRevision) {
		
		StringBuilder sbFrenos = new StringBuilder();
		Optional<Prueba> pruebaFrenos = encontrarPruebaPorTipo(pruebasRevision,ConstantesTiposPrueba.TIPO_PRUEBA_FRENOS);
		Optional<Prueba> pruebaSuspension = encontrarPruebaPorTipo(pruebasRevision,ConstantesTiposPrueba.TIPO_PRUEBA_SUSPENSION);
		Optional<Prueba> pruebaDesviacion = encontrarPruebaPorTipo(pruebasRevision,ConstantesTiposPrueba.TIPO_PRUEBA_DESVIACION);
		
		List<Medida> medidasUnidas = new ArrayList<>();
		//frenos nunca es opcional en una revision
		
		Usuario usuarioFrenos ;
		
		Integer tipoVehiculoId = vehiculo.getTipoVehiculo().getTipoVehiculoId();
		
		if(pruebaFrenos.isPresent()){
			List<Medida> medidasFrenos = pruebaService.obtenerMedidas(pruebaFrenos.get());			
			medidasUnidas.addAll(medidasFrenos);
			usuarioFrenos = pruebaFrenos.get().getUsuario();
			checkNotNull("No existe usuario registrado prueba de frenos revision %d",revisionId);
		}else {
			logger.log(Level.SEVERE, "No hay prueba de frenos para revision" + revisionId );
			throw new RuntimeException("No hay prueba de frenos para revision" + revisionId);
		}
		
		if(pruebaSuspension.isPresent()){
			List<Medida> medidasSuspension = pruebaService.obtenerMedidas(pruebaSuspension.get());			
			medidasUnidas.addAll(medidasSuspension);
		}else{
			Boolean isVehiculoLiviano = tipoVehiculoId.equals(ConstantesTiposVehiculo.LIVIANO) || tipoVehiculoId.equals(ConstantesTiposVehiculo.CUATROxCUATRO);
			if(isVehiculoLiviano){
				logger.log(Level.SEVERE, "Error el vehiculo debe tener suspensión");
			}
		}
		if(pruebaDesviacion.isPresent()){
			List<Medida> medidasDesviacion = pruebaService.obtenerMedidas(pruebaDesviacion.get());
			medidasUnidas.addAll(medidasDesviacion);				
		}else{				
			logger.log(Level.INFO, "Prueba desviacion no presente para revision" + revisionId );
		}
		
		Integer numeroEjes = vehiculo.getNumeroEjes();
		codificadorFAS.codificarResultadoFrenos( medidasUnidas,sbFrenos, usuarioFrenos, numeroEjes);
		return sbFrenos;
		
		
	}

	private StringBuilder generarSeccionLuces(Integer revisionId, List<Prueba> pruebasRevision, boolean vehiculoMoto) {
		StringBuilder sb = new StringBuilder();
		Optional<Prueba> optPruebaLuces = encontrarPruebaPorTipo(pruebasRevision,ConstantesTiposPrueba.TIPO_PRUEBA_LUCES);
		if(optPruebaLuces.isPresent()){
			
			Prueba pruebaLuces = optPruebaLuces.get();
			List<Medida> medidas = pruebaService.obtenerMedidas(pruebaLuces);
			checkArgument(medidas != null && !medidas.isEmpty(),"No hay medidas para prueba de luces revision %d",revisionId);
			Usuario usuario = pruebaLuces.getUsuario();
			
			if(vehiculoMoto) {
				codificadorLuces.codificarResultadoLucesMotocicleta( medidas,usuario,sb);
			} else {
				codificadorLuces.codificarResultadoLucesVehiculo(medidas,usuario,sb);
			}
			return sb;
			
		} else {
			logger.log(Level.SEVERE, "No hay prueba de luces revision " + revisionId);
			throw new RuntimeException("No hay prueba de luces revision " + revisionId);
		}
	}

	private StringBuilder generarSeccionIdProveedor() {
		StringBuilder sbIdProveedor = new StringBuilder();
		CodificadorSeccionProveedor cod = new CodificadorSeccionProveedor();
		cod.codificarSeccionProveedor(sbIdProveedor);
		return sbIdProveedor;
	}
	
	private StringBuilder generarSeccionPropietario(Revision revision) {
		StringBuilder sbPropietario = new StringBuilder();
		Propietario propietario = revision.getPropietario();
		checkNotNull(propietario, "Error propietario no existente para revision %d", revision.getRevisionId());
		codificador.generarInformacionPropietario(sbPropietario, propietario);
		return sbPropietario;
	}
	
	private StringBuilder generarSeccionVehiculo(Revision revision,Vehiculo vehiculo,Boolean aprobada,Timestamp fechaInspeccion,Boolean furPrimeraInspeccion) {
		StringBuilder sbVehiculo = new StringBuilder();		
		Usuario usuarioCreaRevision = revision.getUsuario();// se puede
															// hacer
															// consulta
															// aparte
		checkNotNull(vehiculo,"Vehiculo no presente en revision %d",revision.getRevisionId());
		codificador.generarInformacionVehiculo(sbVehiculo, vehiculo, revision, usuarioCreaRevision,aprobada,fechaInspeccion,furPrimeraInspeccion);
		return sbVehiculo;
	}
	
	private StringBuilder generarSeccionFoto(Integer revisionId, Vehiculo vehiculo, List<Prueba> pruebasRevision) {
		StringBuilder sbFoto = new StringBuilder();
		Optional<Prueba> optPruebaFoto = encontrarPruebaPorTipo(pruebasRevision, ConstantesTiposPrueba.TIPO_PRUEBA_FOTO);
		if (optPruebaFoto.isPresent()) {
			Prueba pruebaFoto = optPruebaFoto.get();
			Foto foto = fotoDAO.consultarFotoPorPrueba(pruebaFoto);			
			codificador.codificarFoto(sbFoto, foto, vehiculo.getPlaca(),pruebaFoto.isAbortada());
		} else {
			throw new RuntimeException("Prueba foto no encontrada revision " + revisionId);
		}
		return sbFoto;
	}
	
	private StringBuilder generarSeccionGases(Vehiculo vehiculo, List<Prueba> pruebasRevision,Analizador analizador) {
		Integer tipoCombustibleId = vehiculo.getTipoCombustible().getCombustibleId();
		StringBuilder sbGases = new StringBuilder();
		Optional<Prueba> optPruebaGases = pruebasRevision.stream().filter(
				prueba -> prueba.getTipoPrueba().getTipoPruebaId().equals(ConstantesTiposPrueba.TIPO_PRUEBA_GASES))
				.findFirst();
		if (optPruebaGases.isPresent()) {
			
			if(tipoCombustibleId.equals(ConstantesTiposCombustible.ELECTRICO)){				
				codificador.codificarGasesVaciaGases(sbGases);
				return sbGases;
			}			
			
			Prueba pruebaGases = optPruebaGases.get();
			List<Medida> medidas = pruebaService.obtenerMedidas(pruebaGases);			
			List<Defecto> defectos = pruebaService.obtenerDefectos(pruebaGases);
			Usuario usuarioGases = pruebaGases.getUsuario();
			checkNotNull("No existe usuario registrado para prueba de gases");

			boolean tipoCombustibleEsDiesel = tipoCombustibleId.equals(ConstantesTiposCombustible.DIESEL)
					|| tipoCombustibleId.equals(ConstantesTiposCombustible.BIODIESEL)
					|| tipoCombustibleId.equals(ConstantesTiposCombustible.DIESEL_ELECTRICO);
			
			boolean tipoCombustibleVehiculoEsOtto = tipoCombustibleId.equals(ConstantesTiposCombustible.GASOLINA )
					|| tipoCombustibleId.equals(ConstantesTiposCombustible.GAS_NATURAL)
					|| tipoCombustibleId.equals(ConstantesTiposCombustible.GAS_GASOLINA)
					|| tipoCombustibleId.equals(ConstantesTiposCombustible.GASOLINA_ELECTRICO);
			
			
			
			if (tipoCombustibleEsDiesel) {
				codificador.codificarResultadoGasesDiesel(sbGases, medidas, defectos, usuarioGases,vehiculo,analizador);
			} else {					
				if (tipoCombustibleVehiculoEsOtto){
					
					
					Integer tipoVehiculo = vehiculo.getTipoVehiculo().getTipoVehiculoId();
					Boolean vehiculoEsMoto = tipoVehiculo.equals(ConstantesTiposVehiculo.MOTO) || tipoVehiculo.equals(ConstantesTiposVehiculo.MOTO_CARRO);
					Boolean vehiculoEsRemolque = tipoVehiculo.equals(ConstantesTiposVehiculo.REMOLQUE); 
					if(vehiculoEsMoto){
						Boolean isDosTiempos = vehiculo.getTiemposMotor().equals(2);
						codificador.codificarInformacionGasesMotocicleta(sbGases, medidas, defectos, usuarioGases,isDosTiempos);														
					}else if(!vehiculoEsMoto && !vehiculoEsRemolque){
						Boolean catalizador = vehiculo.getCatalizador().equalsIgnoreCase("SI");
						codificador.codificarInformacionGasesOttoVehiculo(sbGases, medidas, defectos, usuarioGases, pruebaGases.isDilucion(),catalizador);
					}else{							
						logger.log(Level.INFO, "El vehiculo es remolque no se genera info");
					}
				}
				
				
			}

		} else {
				throw new RuntimeException("No hay prueba de gases para vehiculo " + vehiculo.getPlaca());
		}
		return sbGases;
	}

	
	public StringBuilder generarSeccionRunt(String consecutivoRevision,InformacionCda infoCda) {
		
		
		StringBuilder sb = new StringBuilder();
		sb.append(consecutivoRevision);
		sb.append(SEPARADOR_CAMPO);
		sb.append(infoCda.getIdRuntCda());
		sb.append(SEPARADOR_SECCION);
		return sb;
	}
	

}
