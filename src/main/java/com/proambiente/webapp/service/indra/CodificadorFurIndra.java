package com.proambiente.webapp.service.indra;

import static com.proambiente.webapp.util.ConstantesIndra.ID_PROVEEDOR;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.xml.bind.DatatypeConverter;

import com.proambiente.modelo.Certificado;
import com.proambiente.modelo.Defecto;
import com.proambiente.modelo.Foto;
import com.proambiente.modelo.Inspeccion;
import com.proambiente.modelo.Medida;
import com.proambiente.modelo.Propietario;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Usuario;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.modelo.dto.ResultadoDieselDTO;
import com.proambiente.modelo.dto.ResultadoFasDTO;
import com.proambiente.modelo.dto.ResultadoLucesDTO;
import com.proambiente.modelo.dto.ResultadoOttoMotocicletasDTO;
import com.proambiente.modelo.dto.ResultadoOttoVehiculosDTO;
import com.proambiente.modelo.dto.ResultadoSonometriaDTO;
import com.proambiente.modelo.dto.ResultadoTaximetroDTO;
import com.proambiente.webapp.service.generadordto.GeneradorDTOResultados;
import com.proambiente.webapp.service.util.UtilInformesMedidas;

/**
 * Clase para generar la información del fur en el formato que pide Indra
 * 
 * @author fdavi
 *
 */
public class CodificadorFurIndra {

	public static final String SEPARADOR_SECCION = "|";
	public static final String SEPARADOR_CAMPO = ";";
	private static final String TRUE = GeneradorDTOResultados.TRUE;
	private static final String FALSE = GeneradorDTOResultados.FALSE;

	UtilInformesMedidas utilInformesMedidas = new UtilInformesMedidas();
	GeneradorDTOResultados generadorResultadosDTO = new GeneradorDTOResultados();

	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	

	public void generarInformacionProveedor(StringBuilder sb) {
		sb.append(ID_PROVEEDOR).append(SEPARADOR_SECCION);
	}

	public CodificadorFurIndra() {

	}

	public void generarInformacionPropietario(StringBuilder sb, Propietario propietario) {
		// 1 Nombre o razon social
		sb.append(propietario.getNombres()).append(" ").append(propietario.getApellidos()).append(SEPARADOR_CAMPO);
		// 2 Tipo documento identidad según runt
		sb.append(propietario.getTipoDocumentoIdentidad().getCodigoParametricoRunt()).append(SEPARADOR_CAMPO);
		// 3 Numero de documento de identidad
		Long propietarioId = propietario.getPropietarioId();
		String numeroIdentidad = String.valueOf(propietarioId);
		sb.append(numeroIdentidad).append(SEPARADOR_CAMPO);
		// 4 direccion
		sb.append(propietario.getDireccion()).append(SEPARADOR_CAMPO);
		// 5 telefono
		sb.append(propietario.getTelefono1()).append(SEPARADOR_CAMPO);
		// 6 ciudad
		sb.append(propietario.getCiudad().getNombre()).append(SEPARADOR_CAMPO);
		// 7 departamento
		sb.append(propietario.getCiudad().getDepartamento().getNombre());

		// final
		sb.append(SEPARADOR_SECCION);
	}

	public void generarInformacionVehiculo(StringBuilder sb, Vehiculo vehiculo, Revision revision, Usuario usuario,Boolean aprobada) {
		// 1 Nombres y apellidos de usuario que realizo
		sb.append(usuario.getNombres() + " " + usuario.getApellidos()).append(SEPARADOR_CAMPO);
		// 2 identificacion de usuario
		sb.append(usuario.getCedula()).append(SEPARADOR_CAMPO);
		// 3 placa
		sb.append(vehiculo.getPlaca()).append(SEPARADOR_CAMPO);
		// 4 pais
		sb.append(vehiculo.getPais().getNombre()).append(SEPARADOR_CAMPO);
		// 5 servicio segun superintendencia
		
		sb.append( UtilConvertirServicioSuperintendencia.convertirServicioAsuperintendencia( vehiculo.getServicio().getServicioId() ) ).append(SEPARADOR_CAMPO);
		// 6 clase del vehiculo
		sb.append(vehiculo.getClaseVehiculo().getClaseVehiculoId()).append(SEPARADOR_CAMPO);
		// 7 marca
		sb.append(vehiculo.getMarca().getNombre()).append(SEPARADOR_CAMPO);
		// 8 linea
		sb.append(vehiculo.getLineaVehiculo().getNombre()).append(SEPARADOR_CAMPO);
		// 9 modelo
		sb.append(vehiculo.getModelo()).append(SEPARADOR_CAMPO);
		// 10 lic transito
		sb.append(vehiculo.getNumeroLicencia()).append(SEPARADOR_CAMPO);
		// 11 fecha matricula
		String fechaFormateada = sdf.format(vehiculo.getFechaMatricula());
		sb.append(fechaFormateada).append(SEPARADOR_CAMPO);
		// 12 color
		sb.append(vehiculo.getColor().getNombre()).append(SEPARADOR_CAMPO);
		// 13 combustible
		sb.append(vehiculo.getTipoCombustible().getCombustibleId()).append(SEPARADOR_CAMPO);
		// 14 vin chashis
		sb.append(vehiculo.getVin()).append(SEPARADOR_CAMPO);
		// 15 numero motor
		sb.append(vehiculo.getNumeroMotor()).append(SEPARADOR_CAMPO);
		// 16 tipo Motor
		sb.append(UtilTipoMotor.getTipoMotor(vehiculo.getTiemposMotor())).append(SEPARADOR_CAMPO);
		// 17 cilindraje
		sb.append(vehiculo.getCilindraje()).append(SEPARADOR_CAMPO);
		// 18 kilometraje
		sb.append(vehiculo.getKilometraje()).append(SEPARADOR_CAMPO);
		// 19numero de sillas
		sb.append(vehiculo.getNumeroSillas()).append(SEPARADOR_CAMPO);
		// 20 vidrios poralizados
		if(vehiculo.getVidriosPolarizados() != null) {
			String vidriosPoralizados = vehiculo.getVidriosPolarizados() ? TRUE : FALSE;
			sb.append(vidriosPoralizados).append(SEPARADOR_CAMPO);
		}else {
			sb.append(FALSE).append(SEPARADOR_CAMPO);
		}

		// 21 blindaje
		String blindaje = vehiculo.getBlindaje() ? TRUE : FALSE;
		sb.append(blindaje).append(SEPARADOR_CAMPO);
		// 22 reinspeccion
		String reinspeccion = revision.getNumeroInspecciones() > 1 ? TRUE : FALSE;
		sb.append(reinspeccion).append(SEPARADOR_CAMPO);
		// 23 fecha inspeccion
		String fechaRevisionFormateada = sdf.format(revision.getFechaCreacionRevision());
		sb.append(fechaRevisionFormateada).append(SEPARADOR_CAMPO);// TODO crear
																	// fechaFinalizacion;
		// 24 Estado revision
		String estadoRevision = aprobada ? "1" : "2";//TODO cambiar por algo más eficiente.
		sb.append(estadoRevision);
		// 25 Final
		sb.append(SEPARADOR_SECCION);

	}
	
	public void generarInformacionVehiculoInspeccion(StringBuilder sb, Vehiculo vehiculo, 
			 Usuario usuario,Boolean aprobada,Inspeccion inspeccion) {
		// 1 Nombres y apellidos de usuario que realizo
		sb.append(usuario.getNombres() + " " + usuario.getApellidos()).append(SEPARADOR_CAMPO);
		// 2 identificacion de usuario
		sb.append(usuario.getCedula()).append(SEPARADOR_CAMPO);
		// 3 placa
		sb.append(vehiculo.getPlaca()).append(SEPARADOR_CAMPO);
		// 4 pais
		sb.append(vehiculo.getPais().getNombre()).append(SEPARADOR_CAMPO);
		// 5 servicio segun superintendencia
		
		sb.append( UtilConvertirServicioSuperintendencia.convertirServicioAsuperintendencia( vehiculo.getServicio().getServicioId() ) ).append(SEPARADOR_CAMPO);
		// 6 clase del vehiculo
		sb.append(vehiculo.getClaseVehiculo().getClaseVehiculoId()).append(SEPARADOR_CAMPO);
		// 7 marca
		sb.append(vehiculo.getMarca().getNombre()).append(SEPARADOR_CAMPO);
		// 8 linea
		sb.append(vehiculo.getLineaVehiculo().getNombre()).append(SEPARADOR_CAMPO);
		// 9 modelo
		sb.append(vehiculo.getModelo()).append(SEPARADOR_CAMPO);
		// 10 lic transito
		sb.append(vehiculo.getNumeroLicencia()).append(SEPARADOR_CAMPO);
		// 11 fecha matricula
		String fechaFormateada = sdf.format(vehiculo.getFechaMatricula());
		sb.append(fechaFormateada).append(SEPARADOR_CAMPO);
		// 12 color
		sb.append(vehiculo.getColor().getNombre()).append(SEPARADOR_CAMPO);
		// 13 combustible
		sb.append(vehiculo.getTipoCombustible().getCombustibleId()).append(SEPARADOR_CAMPO);
		// 14 vin chashis
		sb.append(vehiculo.getVin()).append(SEPARADOR_CAMPO);
		// 15 numero motor
		sb.append(vehiculo.getNumeroMotor()).append(SEPARADOR_CAMPO);
		// 16 tipo Motor
		sb.append(UtilTipoMotor.getTipoMotor(vehiculo.getTiemposMotor())).append(SEPARADOR_CAMPO);
		// 17 cilindraje
		sb.append(vehiculo.getCilindraje()).append(SEPARADOR_CAMPO);
		// 18 kilometraje
		sb.append(vehiculo.getKilometraje()).append(SEPARADOR_CAMPO);
		// 19numero de sillas
		sb.append(vehiculo.getNumeroSillas()).append(SEPARADOR_CAMPO);
		// 20 vidrios poralizados
		String vidriosPoralizados = vehiculo.getVidriosPolarizados() ? TRUE : FALSE;
		sb.append(vidriosPoralizados).append(SEPARADOR_CAMPO);
		// 21 blindaje
		String blindaje = vehiculo.getBlindaje() ? TRUE : FALSE;
		sb.append(blindaje).append(SEPARADOR_CAMPO);
		// 22 reinspeccion
		String reinspeccion = inspeccion.getNumeroInspeccion() > 1 ? TRUE : FALSE;
		sb.append(reinspeccion).append(SEPARADOR_CAMPO);
		// 23 fecha inspeccion		
		
		String fechaRevisionFormateada = sdf.format(inspeccion.getFechaInspeccionAnterior());
		sb.append(fechaRevisionFormateada).append(SEPARADOR_CAMPO);
		// 24 Estado revision
		String estadoRevision = inspeccion.getAprobada() ? "1" : "2";
		sb.append(estadoRevision);
		// 25 Final
		sb.append(SEPARADOR_SECCION);

	}


	

	public void codificarFoto(StringBuilder sb, Foto foto, String placa, Boolean pruebaAbortada) {

		// 1 placa
		sb.append(placa).append(SEPARADOR_CAMPO);

		if (foto.getFoto1() == null && !pruebaAbortada) {
			throw new RuntimeException("Foto uno no ha sido tomada");
		} else {
			if (foto.getFoto1() != null) {
				String base64PrimeraFoto = DatatypeConverter.printBase64Binary(foto.getFoto1());
				// 2 foto1
				sb.append(base64PrimeraFoto).append(SEPARADOR_CAMPO);
			} else {
				sb.append("").append(SEPARADOR_CAMPO);
			}
		}
		if (foto.getFoto2() == null && !pruebaAbortada) {
			throw new RuntimeException("Foto dos no ha sido tomada");
		} else {
			if (foto.getFoto2() != null) {
				String base64SegundaFoto = DatatypeConverter.printBase64Binary(foto.getFoto2());
				// 3 segunda foto
				sb.append(base64SegundaFoto);
			} else {
				sb.append("");
			}
		}
		// 4 final de seccion
		sb.append(SEPARADOR_SECCION);

	}

	public void codificarInformacionGasesDiesel(StringBuilder sb, List<Medida> medidas, List<Defecto> defectos,
			Usuario usuario) {
		// 1 Nombres y apellidos de usuario que realizo
		sb.append(usuario.getNombres() + " " + usuario.getApellidos()).append(SEPARADOR_CAMPO);
		// 2 identificacion de usuario
		sb.append(usuario.getCedula()).append(SEPARADOR_CAMPO);
		// 3 Medida de temperatura
		if (medidas == null) {
			medidas = new ArrayList<>();
		}
		utilInformesMedidas.ordenarListaMedidas(medidas);

		ResultadoDieselDTO resultado = generadorResultadosDTO.generarResultadoDieselDTO(medidas, defectos);

		sb.append(resultado.getTemperaturaInicial()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getRpmGobernada()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getAceleracionCero()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getAceleracionUno()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getAceleracionDos()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getAceleracionTres()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getValorFinal()).append(SEPARADOR_SECCION);
		// Inspeccion Sensorial
		sb.append(resultado.getFugasTuboEscape()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getFugasSilenciador()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getTapaCombustible()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getTapaAceite()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getSistemaMuestreo()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getSalidasAdicionales()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getFiltroAire()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getSistemaRefrigeracion()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getRevolucionesFueraRango()).append(SEPARADOR_SECCION);
	}

	public void codificarGasesVacia(StringBuilder sb) {
		sb.append(SEPARADOR_SECCION).append(SEPARADOR_SECCION);
	}

	public void codificadorInformacionGasesOttoVehiculo(StringBuilder sb, List<Medida> medidas, List<Defecto> defectos,
			Usuario usuario, Boolean dilucion) {

		
		sb.append(usuario.getNombres() + " " + usuario.getApellidos()).append(SEPARADOR_CAMPO);//1
		
		sb.append(usuario.getCedula()).append(SEPARADOR_CAMPO);//2

		utilInformesMedidas.ordenarListaMedidas(medidas);
		String codigosMedidas = medidas.stream().map(medida -> String.valueOf(medida.getTipoMedida().getTipoMedidaId()))
				.collect(Collectors.joining(";"));
		System.out.println(codigosMedidas);
		ResultadoOttoVehiculosDTO resultado = generadorResultadosDTO.generarResultadoOtto(medidas, defectos);
		
		sb.append(resultado.getTempRalenti().equals("0")? -1 : resultado.getTempRalenti()).append(SEPARADOR_CAMPO);// 3
		sb.append(resultado.getRpmRalenti()).append(SEPARADOR_CAMPO);//4
		sb.append(resultado.getHcRalenti()).append(SEPARADOR_CAMPO);//5
		sb.append(resultado.getCoRalenti()).append(SEPARADOR_CAMPO);//6
		sb.append(resultado.getCo2Ralenti()).append(SEPARADOR_CAMPO);//7
		sb.append(resultado.getO2Ralenti()).append(SEPARADOR_CAMPO);//8
		sb.append(resultado.getTempCrucero().equals("0") ? "-1" : resultado.getTempCrucero()).append(SEPARADOR_CAMPO);//9
		sb.append(resultado.getRpmCrucero()).append(SEPARADOR_CAMPO);//10
		sb.append(resultado.getHcCrucero()).append(SEPARADOR_CAMPO);//11
		sb.append(resultado.getCoCrucero()).append(SEPARADOR_CAMPO);//12
		sb.append(resultado.getCo2Crucero()).append(SEPARADOR_CAMPO);//13
		sb.append(resultado.getO2Crucero()).append(SEPARADOR_CAMPO);//14

		// 15 dilucion
		String dilucionStr = dilucion ? TRUE : FALSE;
		sb.append(dilucionStr);

		sb.append(SEPARADOR_SECCION);

		// inspecion sensorial
		sb.append(resultado.getFugasTuboEscape()).append(SEPARADOR_CAMPO);//1
		sb.append(resultado.getFugasSilenciador()).append(SEPARADOR_CAMPO);//2
		sb.append(resultado.getTapaCombustible()).append(SEPARADOR_CAMPO);//3
		sb.append(resultado.getTapaAceite()).append(SEPARADOR_CAMPO);//4
		sb.append(resultado.getSalidasAdicionales()).append(SEPARADOR_CAMPO);//5
		sb.append(resultado.getPresenciaHumos()).append(SEPARADOR_CAMPO);//6
		sb.append(resultado.getRevolucionesFueraRango()).append(SEPARADOR_CAMPO);//7
		sb.append(resultado.getFallaSistemaRefrigeracion()).append(SEPARADOR_SECCION);
	}

	public void codificarInformacionGasesMotocicleta(StringBuilder sb, List<Medida> medidas, List<Defecto> defectos,
			Usuario usuario, Boolean dosTiempos) {
		// 1 Nombres y apellidos de usuario que realizo
		sb.append(usuario.getNombres() + " " + usuario.getApellidos()).append(SEPARADOR_CAMPO);
		// 2 identificacion de usuario
		sb.append(usuario.getCedula()).append(SEPARADOR_CAMPO);
		// 3 temperatura ralenti
		utilInformesMedidas.ordenarListaMedidas(medidas);
		ResultadoOttoMotocicletasDTO resultado = generadorResultadosDTO.generarResultadoOttoMotos(medidas, defectos,
				dosTiempos);

		sb.append(resultado.getTempRalenti()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getRpmRalenti()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getHcRalenti()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getCoRalenti()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getCo2Ralenti()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getO2Ralenti()).append(SEPARADOR_SECCION);
		// INSPECCION SENSORIAL
		sb.append(resultado.getRevolucionesFueraRango()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getFugasTuboEscape()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getFugasSilenciador()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getTapaCombustible()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getTapaAceite()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getSalidasAdicionales()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getPresenciaHumos()).append(SEPARADOR_SECCION);
	}

	public void codificarMedidasFas(StringBuilder sb, List<Medida> medidas, Usuario usuario, Integer numeroEjes) {
		// 1 Nombres y apellidos de usuario que realizo
		sb.append(usuario.getNombres() + " " + usuario.getApellidos()).append(SEPARADOR_CAMPO);
		// 2 identificacion de usuario
		sb.append(usuario.getCedula()).append(SEPARADOR_CAMPO);
		// 3 numero de Ejes
		sb.append(String.valueOf(numeroEjes)).append(SEPARADOR_CAMPO);
		utilInformesMedidas.ordenarListaMedidas(medidas);

		ResultadoFasDTO resultado = generadorResultadosDTO.generarResultadoFAS(medidas);
		// 4 Eficacia Total
		sb.append(resultado.getEficaciaTotal()).append(SEPARADOR_CAMPO);
		// 5 Eficacia Auxiliar
		sb.append(resultado.getEficaciaAuxiliar()).append(SEPARADOR_CAMPO);
		// Desequilibrios
		sb.append(resultado.getDesequilibrioEje1()).append(SEPARADOR_CAMPO);//6
		sb.append(resultado.getDesequilibrioEje2()).append(SEPARADOR_CAMPO);//7
		sb.append(resultado.getDesequilibrioEje3()).append(SEPARADOR_CAMPO);//8
		sb.append(resultado.getDesequilibrioEje4()).append(SEPARADOR_CAMPO);//9
		sb.append(resultado.getDesequilibrioEje5()).append(SEPARADOR_CAMPO);//10
		sb.append(resultado.getDesequilibrioEje6()).append(SEPARADOR_CAMPO);//11
		// FuerzaFrenadoIzquierda
		sb.append(resultado.getFuerzaFrenadoIzqEje1()).append(SEPARADOR_CAMPO);//12
		sb.append(resultado.getFuerzaFrenadoIzqEje2()).append(SEPARADOR_CAMPO);//13
		sb.append(resultado.getFuerzaFrenadoIzqEje3()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getFuerzaFrenadoIzqEje4()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getFuerzaFrenadoIzqEje5()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getFuerzaFrenadoIzqEje6()).append(SEPARADOR_CAMPO);
		// FuerzaFrenadoDerecha
		sb.append(resultado.getFuerzaFrenadoDerEje1()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getFuerzaFrenadoDerEje2()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getFuerzaFrenadoDerEje3()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getFuerzaFrenadoDerEje4()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getFuerzaFrenadoDerEje5()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getFuerzaFrenadoDerEje6()).append(SEPARADOR_CAMPO);
		// Peso derecho
		sb.append(resultado.getPesoDerEje1()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getPesoDerEje2()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getPesoDerEje3()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getPesoDerEje4()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getPesoDerEje5()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getPesoDerEje6()).append(SEPARADOR_CAMPO);
		// Peso izquierdo
		sb.append(resultado.getPesoIzqEje1()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getPesoIzqEje2()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getPesoIzqEje3()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getPesoIzqEje4()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getPesoIzqEje5()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getPesoIzqEje6()).append(SEPARADOR_CAMPO);
		// Desviacion Lateral
		sb.append(resultado.getDesviacionLateralEje1()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getDesviacionLateralEje2()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getDesviacionLateralEje3()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getDesviacionLateralEje4()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getDesviacionLateralEje5()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getDesviacionLateralEje6()).append(SEPARADOR_CAMPO);
		// suspension
		sb.append(resultado.getSuspIzqEje1()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getSuspIzqEje2()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getSuspDerEje1()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getSuspDerEje2());

		sb.append(SEPARADOR_SECCION);

	}

	public void codificarResultadoLuces(StringBuilder sb, List<Medida> medidas, Usuario usuario,Boolean isMoto) {

		// 1 Nombres y apellidos de usuario que realizo
		sb.append(usuario.getNombres() + " " + usuario.getApellidos()).append(SEPARADOR_CAMPO);//1
		// 2 identificacion de usuario
		sb.append(usuario.getCedula()).append(SEPARADOR_CAMPO);//2
		// 3 intensidad Baja Derecha
		utilInformesMedidas.ordenarListaMedidas(medidas);
		ResultadoLucesDTO resultadoLuces = isMoto ? generadorResultadosDTO.generarResultadoLucesMoto(medidas) : generadorResultadosDTO.generarResultadoLuces(medidas);
		sb.append(resultadoLuces.getDerBajaIntensidad()).append(SEPARADOR_CAMPO);//3
		sb.append(resultadoLuces.getIzqBajaIntensidad()).append(SEPARADOR_CAMPO);//4
		sb.append(resultadoLuces.getDerBajaInclinacion()).append(SEPARADOR_CAMPO);//5
		sb.append(resultadoLuces.getIzqBajaInclinacion()).append(SEPARADOR_CAMPO);//6		
		sb.append(resultadoLuces.getSumatoriaIntensidad()).append(SEPARADOR_SECCION);

	}

	public void codificarResultadoSonometria(StringBuilder sb, List<Medida> medidas, Usuario usuario) {
		// 1 Nombres y apellidos de usuario que realizo
		sb.append(usuario.getNombres() + " " + usuario.getApellidos()).append(SEPARADOR_CAMPO);
		// 2 identificacion de usuario
		sb.append(usuario.getCedula()).append(SEPARADOR_CAMPO);
		utilInformesMedidas.ordenarListaMedidas(medidas);
		ResultadoSonometriaDTO resultado = generadorResultadosDTO.generarResultadoSonometria(medidas);
		sb.append(resultado.getValor()).append(SEPARADOR_SECCION);
	}

	

	

	public void codificarEstructuraDatosRunt(StringBuilder sb, Revision revision, Certificado certificado,
			String numeroCda) {
		//1 numero de certificado
		if (certificado != null && certificado.getConsecutivoSustrato() != null) {
			sb.append(certificado.getConsecutivoSustrato()).append(SEPARADOR_CAMPO);
		} else {
			sb.append("").append(SEPARADOR_CAMPO);
		}
		//2 consecutivo runt
		if (revision.getConsecutivoRunt() != null) {
			sb.append(revision.getConsecutivoRunt()).append(SEPARADOR_CAMPO);
		} else {
			sb.append("").append(SEPARADOR_CAMPO);
		}
		//3 numero fur -id revision
		sb.append(revision.getRevisionId()).append(SEPARADOR_CAMPO);
		//4 numero de cda
		sb.append(numeroCda);// no se agrega separacion de seccion
	}
	
	public void codificarEstructuraDatosRuntVacia(StringBuilder sb, Revision revision,String numeroCda) {
		sb.append("").append(SEPARADOR_CAMPO);//certificado  vacio
		sb.append("").append(SEPARADOR_CAMPO);//consecutivo runt vacio
		sb.append(revision.getRevisionId()).append(SEPARADOR_CAMPO);
		sb.append(numeroCda);// no se agrega separacion de seccion
	};
	

	

	
	
	public void codificarPruebaAbortada(StringBuilder sb) {
		sb.append("");
	}
}
