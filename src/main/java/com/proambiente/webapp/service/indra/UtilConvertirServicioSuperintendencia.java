package com.proambiente.webapp.service.indra;

public class UtilConvertirServicioSuperintendencia {
	
//PRORTM	
public static final int OFICIAL = 1;
public static final int PUBLICO = 2;
public static final int PARTICULAR = 3;
public static final int DIPLOMATICO = 4;
public static final int ENSENIANZA = 5;
public static final int ESPECIAL_IRNMA = 7;
	
//RUNT
public static final int PARTICULAR_RUNT =1 ;
public static final int PUBLICO_RUNT = 2;
public static final int DIPLOMATICO_RUNT = 3;
public static final int OFICIAL_RUNT = 4;
public static final int ESPECIAL_RNMA_RUNT = 7;

public static Integer  convertirServicioAsuperintendencia(Integer servicioId) {
		
		switch(servicioId) {
		case OFICIAL://
			return OFICIAL_RUNT;
		case PUBLICO:
			return PUBLICO_RUNT;
		case PARTICULAR:
			return PARTICULAR_RUNT;
		case DIPLOMATICO:
			return DIPLOMATICO_RUNT;
		case ENSENIANZA:
			return PARTICULAR_RUNT;
		case ESPECIAL_IRNMA: 
			return ESPECIAL_RNMA_RUNT;
		default:
		throw new RuntimeException("Error tipo de servicio " + servicioId);
		}
		
	}

}

