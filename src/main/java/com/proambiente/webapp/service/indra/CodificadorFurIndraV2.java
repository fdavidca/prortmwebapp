package com.proambiente.webapp.service.indra;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.xml.bind.DatatypeConverter;

import com.proambiente.modelo.Analizador;
import com.proambiente.modelo.Defecto;
import com.proambiente.modelo.Foto;
import com.proambiente.modelo.Medida;
import com.proambiente.modelo.Propietario;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Usuario;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.modelo.dto.ResultadoDieselDTO;
import com.proambiente.modelo.dto.ResultadoOttoMotocicletasDTO;
import com.proambiente.modelo.dto.ResultadoOttoVehiculosDTO;
import com.proambiente.webapp.service.generadordto.GeneradorDTOResultados;
import com.proambiente.webapp.service.util.UtilContieneDefectosSensorialesMoto;
import com.proambiente.webapp.service.util.UtilContieneDefectosSensorialesOtto;
import com.proambiente.webapp.service.util.UtilInformesMedidas;
import com.proambiente.webapp.util.dto.ResultadoDieselDTOInformes;
import com.proambiente.webapp.util.dto.ResultadoOttoInformeDTO;
import com.proambiente.webapp.util.dto.ResultadoOttoMotocicletasInformeDTO;

public class CodificadorFurIndraV2 {

	public static final String SEPARADOR_SECCION = "|";
	public static final String SEPARADOR_CAMPO = ";";

	private static final String TRUE = GeneradorDTOResultados.TRUE;
	private static final String FALSE = GeneradorDTOResultados.FALSE;

	UtilInformesMedidas utilInformesMedidas = new UtilInformesMedidas();
	GeneradorDTOResultados generadorResultadosDTO = new GeneradorDTOResultados();

	public void generarInformacionPropietario(StringBuilder sb, Propietario propietario) {
		// 1 Nombre o razon social
		sb.append(propietario.getNombres()).append(" ").append(propietario.getApellidos()).append(SEPARADOR_CAMPO);
		// 2 Tipo documento identidad según runt
		sb.append(propietario.getTipoDocumentoIdentidad().getCodigoParametricoRunt()).append(SEPARADOR_CAMPO);
		// 3 Numero de documento de identidad
		Long propietarioId = propietario.getPropietarioId();
		String numeroIdentidad = String.valueOf(propietarioId);
		sb.append(numeroIdentidad).append(SEPARADOR_CAMPO);
		// 4 direccion
		sb.append(propietario.getDireccion()).append(SEPARADOR_CAMPO);
		// 5 telefono
		sb.append(propietario.getTelefono1()).append(SEPARADOR_CAMPO);
		// 6 ciudad
		sb.append(propietario.getCiudad().getNombre()).append(SEPARADOR_CAMPO);
		// 7 departamento
		sb.append(propietario.getCiudad().getDepartamento().getNombre()).append(SEPARADOR_CAMPO);
		// 8 email
		String email = propietario.getCorreoElectronico() != null ? propietario.getCorreoElectronico() : "";
		// final
		sb.append(email).append(SEPARADOR_SECCION);
	}

	public void generarInformacionVehiculo(StringBuilder sb, Vehiculo vehiculo, Revision revision, Usuario usuario,
			Boolean aprobada,Timestamp fechaRevision,Boolean furPrimeraInspeccion) {
		// 1 Nombres y apellidos de usuario que realizo
		sb.append(usuario.getNombres() + " " + usuario.getApellidos()).append(SEPARADOR_CAMPO);
		// 2 identificacion de usuario
		sb.append(usuario.getCedula()).append(SEPARADOR_CAMPO);
		// 3 placa
		sb.append(vehiculo.getPlaca()).append(SEPARADOR_CAMPO);
		// 4 pais
		sb.append(vehiculo.getPais().getNombre()).append(SEPARADOR_CAMPO);
		// 5 servicio segun superintendencia
		sb.append(UtilConvertirServicioSuperintendencia
				.convertirServicioAsuperintendencia(vehiculo.getServicio().getServicioId())).append(SEPARADOR_CAMPO);
		// 6 clase del vehiculo
		sb.append(vehiculo.getClaseVehiculo().getClaseVehiculoId()).append(SEPARADOR_CAMPO);
		// 7 marca
		sb.append(vehiculo.getMarca().getNombre()).append(SEPARADOR_CAMPO);
		// 8 linea
		sb.append(vehiculo.getLineaVehiculo().getNombre()).append(SEPARADOR_CAMPO);
		// 9 modelo
		sb.append(vehiculo.getModelo()).append(SEPARADOR_CAMPO);
		// 10 lic transito
		sb.append(vehiculo.getNumeroLicencia()).append(SEPARADOR_CAMPO);
		// 11 fecha matricula
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String fechaFormateada = sdf.format(vehiculo.getFechaMatricula());
		sb.append(fechaFormateada).append(SEPARADOR_CAMPO);
		// 12 color
		sb.append(vehiculo.getColor().getNombre()).append(SEPARADOR_CAMPO);
		// 13 combustible
		sb.append(vehiculo.getTipoCombustible().getCombustibleId()).append(SEPARADOR_CAMPO);
		// 14 vin chashis
		sb.append(vehiculo.getVin()).append(SEPARADOR_CAMPO);
		// 15 numero motor
		sb.append(vehiculo.getNumeroMotor()).append(SEPARADOR_CAMPO);
		// 16 tipo Motor
		sb.append(UtilTipoMotor.getTipoMotor(vehiculo.getTiemposMotor())).append(SEPARADOR_CAMPO);
		// 17 cilindraje
		sb.append(vehiculo.getCilindraje()).append(SEPARADOR_CAMPO);
		// 18 kilometraje
		if(vehiculo.getKilometraje().equalsIgnoreCase("NO FUNCIONAL")) {
			sb.append("").append(SEPARADOR_CAMPO);
		}else {
			sb.append(vehiculo.getKilometraje()).append(SEPARADOR_CAMPO);
		}
		// 19numero de sillas
		sb.append(vehiculo.getNumeroSillas()).append(SEPARADOR_CAMPO);
		// 20 blindaje
		String blindaje = vehiculo.getBlindaje() ? TRUE : FALSE;
		sb.append(blindaje).append(SEPARADOR_CAMPO);
		// 21 reinspeccion
		if(!furPrimeraInspeccion) {
			String reinspeccion = revision.getNumeroInspecciones() > 1 ? TRUE : FALSE;
			sb.append(reinspeccion).append(SEPARADOR_CAMPO);
		}else {
			sb.append(FALSE).append(SEPARADOR_CAMPO);
		}
		// 22 fecha inspeccion
		String fechaRevisionFormateada = sdf.format(fechaRevision);
		sb.append(fechaRevisionFormateada).append(SEPARADOR_CAMPO);// TODO crear
																	// fechaFinalizacion;
		// 23 Estado revision
		String estadoRevision = aprobada ? "1" : "2";
		sb.append(estadoRevision).append(SEPARADOR_CAMPO);
		// 24 Potencia
		Integer potencia = vehiculo.getPotencia() != null ? vehiculo.getPotencia() : 0;
		sb.append(String.valueOf(potencia)).append(SEPARADOR_CAMPO);
		// 25 Tipo Carroceria
		if (vehiculo.getTipoCarroceria() == null) {
			throw new RuntimeException("Tipo Carroceria del vehiculo no especificada");
		}
		String tipoCarroceriaStr = vehiculo.getTipoCarroceria().getCodigoRunt();
		sb.append(tipoCarroceriaStr).append(SEPARADOR_CAMPO);
		// 26 Fecha vencimiento soat
		SimpleDateFormat sdfFechaSoat = new SimpleDateFormat("yyyy-MM-dd");
		sb.append(sdfFechaSoat.format(vehiculo.getFechaExpiracionSoat())).append(SEPARADOR_CAMPO);
		// 27 Conversion GNV
		sb.append(vehiculo.getConversionGnv().replace("/", "")).append(SEPARADOR_CAMPO);
		// 28 fecha de vencimiento conversion gnv
		SimpleDateFormat sdfFechaConversionGnv = new SimpleDateFormat("yyyy-MM-dd");
		if (vehiculo.getFechaVenicimientoGNV() == null) {
			sb.append("");
		} else {
			String fechaConversionGnv = sdfFechaConversionGnv.format(vehiculo.getFechaVenicimientoGNV());
			sb.append(fechaConversionGnv);
		}
		sb.append(SEPARADOR_SECCION);

	}

	public void codificarFoto(StringBuilder sb, Foto foto, String placa, Boolean pruebaAbortada) {

		// 1 placa
		sb.append(placa).append(SEPARADOR_CAMPO);

		if (foto.getFoto1() == null && !pruebaAbortada) {
			throw new RuntimeException("Foto uno no ha sido tomada");
		} else {
			if (foto.getFoto1() != null) {
				String base64PrimeraFoto = DatatypeConverter.printBase64Binary(foto.getFoto1());
				// 2 foto1
				sb.append(base64PrimeraFoto).append(SEPARADOR_CAMPO);
			} else {
				sb.append("").append(SEPARADOR_CAMPO);
			}
		}
		if (foto.getFoto2() == null && !pruebaAbortada) {
			throw new RuntimeException("Foto dos no ha sido tomada");
		} else {
			if (foto.getFoto2() != null) {
				String base64SegundaFoto = DatatypeConverter.printBase64Binary(foto.getFoto2());
				// 3 segunda foto
				sb.append(base64SegundaFoto);
			} else {
				sb.append("");
			}
		}
		// 4 final de seccion
		sb.append(SEPARADOR_SECCION);

	}

	public void codificarResultadoGasesDiesel(StringBuilder sb, List<Medida> medidas, List<Defecto> defectos,
			Usuario usuario, Vehiculo vehiculo,Analizador analizador) {
		// 1 Nombres y apellidos de usuario que realizo
		sb.append(usuario.getNombres() + " " + usuario.getApellidos()).append(SEPARADOR_CAMPO);
		// 2 identificacion de usuario
		sb.append(usuario.getCedula()).append(SEPARADOR_CAMPO);
		// 3 Medida de temperatura
		if (medidas == null) {
			medidas = new ArrayList<>();
		}
		utilInformesMedidas.ordenarListaMedidas(medidas);

		ResultadoDieselDTO resultado = generadorResultadosDTO.generarResultadoDieselDTO(medidas, defectos);
		ResultadoDieselDTOInformes resultadoComplementario = generadorResultadosDTO
				.generarResultadoDieselDTOInformes(medidas, defectos);
		// 1 temperatura ambiental
		sb.append(resultadoComplementario.getTemperaturaAmbiental()).append(SEPARADOR_CAMPO);// ojo con el punto decimal
		// 2 rpm ralenti
		sb.append(resultadoComplementario.getRpmRalenti()).append(SEPARADOR_CAMPO);
		// 3 velocidad gobernada 0
		sb.append(resultadoComplementario.getRpmGobernadaCiclo0()).append(SEPARADOR_CAMPO);
		// 4 velocidad gobernada 1
		sb.append(resultadoComplementario.getRpmGobernadaCiclo1()).append(SEPARADOR_CAMPO);
		// 5 velocidad gobernada 2
		sb.append(resultadoComplementario.getRpmGobernadaCiclo2()).append(SEPARADOR_CAMPO);
		// 6 velocidad gobernada 3
		sb.append(resultadoComplementario.getRpmGobernadaCiclo3()).append(SEPARADOR_CAMPO);
		// 7 aceleracion ciclo 0
		sb.append(resultado.getAceleracionCero()).append(SEPARADOR_CAMPO);
		// 8 aceleracion ciclo 1
		sb.append(resultado.getAceleracionUno()).append(SEPARADOR_CAMPO);
		// 9 aceleracion ciclo 2
		sb.append(resultado.getAceleracionDos()).append(SEPARADOR_CAMPO);
		// 10 aceleracion ciclo 3
		sb.append(resultado.getAceleracionTres()).append(SEPARADOR_CAMPO);
		// 11 resultado final
		sb.append(resultado.getValorFinal()).append(SEPARADOR_CAMPO);
		// 12 temperatura inicial
		sb.append(resultado.getTemperaturaInicial()).append(SEPARADOR_CAMPO);
		// 13 temperatura final
		sb.append(resultadoComplementario.getTemperaturaFinal()).append(SEPARADOR_CAMPO);
		// 14 humedad relativa
		sb.append(resultadoComplementario.getHumedadRelativa()).append(SEPARADOR_CAMPO);
		// 15 ltoe estandar
		if(analizador==null)throw new RuntimeException("Analizador es null placa" + vehiculo.getPlaca());
		sb.append(analizador.getLtoe()==null ?"430":String.valueOf( analizador.getLtoe() ) ).append(SEPARADOR_CAMPO);//no va separador de campo
		// 16 densidad de humo 0
		sb.append(resultadoComplementario.getDensidadHumoCiclo0()).append(SEPARADOR_CAMPO);
		// 17 densidad de humo 1
		sb.append(resultadoComplementario.getDensidadHumoCiclo1()).append(SEPARADOR_CAMPO);
		// 18 densidad de humo 2
		sb.append(resultadoComplementario.getDensidadHumoCiclo2()).append(SEPARADOR_CAMPO);
		// 19 densidad de humo 3
		sb.append(resultadoComplementario.getDensidadHumoCiclo3()).append(SEPARADOR_CAMPO);
		//20
		sb.append(resultadoComplementario.getDensidadHumoFinal());
		sb.append(SEPARADOR_SECCION);
		// 21 fugas tubo de escape
		sb.append(resultado.getFugasTuboEscape()).append(SEPARADOR_CAMPO);
		// 22 fugas silenciador
		sb.append(resultado.getFugasSilenciador()).append(SEPARADOR_CAMPO);
		// 23 tapa combustible
		sb.append(resultado.getTapaCombustible()).append(SEPARADOR_CAMPO);
		// 24 tapa aceite
		sb.append(resultado.getTapaAceite()).append(SEPARADOR_CAMPO);
		// 25 sistema de muestreo
		sb.append(resultado.getSistemaMuestreo()).append(SEPARADOR_CAMPO);
		// 26 salidas adicionales
		sb.append(resultado.getSalidasAdicionales()).append(SEPARADOR_CAMPO);
		// 27 filtro aire
		sb.append(resultado.getFiltroAire()).append(SEPARADOR_CAMPO);
		// 28 sistema de refrigeracion
		sb.append(resultado.getSistemaRefrigeracion()).append(SEPARADOR_CAMPO);
		// 29 revoluciones inestables o fuera de rango
		sb.append(resultado.getRevolucionesFueraRango()).append(SEPARADOR_SECCION);
		//

	}

	public void codificarDefectosGasesDiesel(StringBuilder sb, List<Medida> medidas, List<Defecto> defectos) {
		if (medidas == null) {
			medidas = new ArrayList<>();
		}
		utilInformesMedidas.ordenarListaMedidas(medidas);
		ResultadoDieselDTO resultado = generadorResultadosDTO.generarResultadoDieselDTO(medidas, defectos);
		// 1 fugas tubo de escape
		sb.append(resultado.getFugasTuboEscape()).append(SEPARADOR_CAMPO);
		// 2 fugas silenciador
		sb.append(resultado.getFugasSilenciador()).append(SEPARADOR_CAMPO);
		// 3 tapa combustible
		sb.append(resultado.getTapaCombustible()).append(SEPARADOR_CAMPO);
		// 4 tapa aceite
		sb.append(resultado.getTapaAceite()).append(SEPARADOR_CAMPO);
		// 5 sistema de muestreo
		sb.append(resultado.getSistemaMuestreo()).append(SEPARADOR_CAMPO);
		// salidas adicionales
		sb.append(resultado.getSalidasAdicionales()).append(SEPARADOR_CAMPO);
		// filtro de aire
		sb.append(resultado.getFiltroAire()).append(SEPARADOR_CAMPO);
		// sistema refrigeracion
		sb.append(resultado.getSistemaRefrigeracion()).append(SEPARADOR_CAMPO);
		// revoluciones fuera de rango
		sb.append(resultado.getRevolucionesFueraRango()).append(SEPARADOR_SECCION);

	}

	public void codificarInformacionGasesOttoVehiculo(StringBuilder sb, List<Medida> medidas, List<Defecto> defectos,
			Usuario usuario, Boolean dilucion, Boolean catalizador) {

		sb.append(usuario.getNombres() + " " + usuario.getApellidos()).append(SEPARADOR_CAMPO);// 1

		sb.append(usuario.getCedula()).append(SEPARADOR_CAMPO);// 2

		utilInformesMedidas.ordenarListaMedidas(medidas);
		String codigosMedidas = medidas.stream().map(medida -> String.valueOf(medida.getTipoMedida().getTipoMedidaId()))
				.collect(Collectors.joining(";"));
		System.out.println(codigosMedidas);
		ResultadoOttoVehiculosDTO resultado = generadorResultadosDTO.generarResultadoOtto(medidas, defectos);
		ResultadoOttoInformeDTO resultadoComplementario = generadorResultadosDTO.generarResultadoOttoInforme(medidas,
				defectos);

		// 1 rpm ralenti
		sb.append(resultado.getRpmRalenti()).append(SEPARADOR_CAMPO);
		// 2 hc ralenti
		sb.append(resultado.getHcRalenti()).append(SEPARADOR_CAMPO);
		// 3 co ralenti
		sb.append(resultado.getCoRalenti()).append(SEPARADOR_CAMPO);
		// 4 co2 ralenti
		sb.append(resultado.getCo2Ralenti()).append(SEPARADOR_CAMPO);
		// 5 O2 ralenti
		sb.append(resultado.getO2Ralenti()).append(SEPARADOR_CAMPO);
		// 6 rpm crucero
		sb.append(resultado.getRpmCrucero()).append(SEPARADOR_CAMPO);
		// 7 hc crucero
		sb.append(resultado.getHcCrucero()).append(SEPARADOR_CAMPO);
		// 8 co crucero
		sb.append(resultado.getCoCrucero()).append(SEPARADOR_CAMPO);
		// 9 co2 crucero
		sb.append(resultado.getCo2Crucero()).append(SEPARADOR_CAMPO);
		// 10 o2 crucero
		sb.append(resultado.getO2Crucero()).append(SEPARADOR_CAMPO);
		// 11 dilucion
		String dilucionStr = dilucion ? TRUE : FALSE;
		sb.append(dilucionStr).append(SEPARADOR_CAMPO);
		// 12 catalizador
		String catalizadorStr = catalizador ? "S" : "N";
		sb.append(catalizadorStr).append(SEPARADOR_CAMPO);
		//13 temperatura prueba
		sb.append(resultado.getTempRalenti().equals("0") ? 0 : resultado.getTempRalenti()).append(SEPARADOR_CAMPO);
		// 14 temperatura ambiente
		
		if(new UtilContieneDefectosSensorialesOtto().contieneDefectosSensorialesOtto(defectos)) {
			sb.append("").append(SEPARADOR_CAMPO);
		} else {
			sb.append(resultadoComplementario.getTemperaturaAmbiente()).append(SEPARADOR_CAMPO);
		}
		
		
		// 15 humedad relativa
		if(new UtilContieneDefectosSensorialesOtto().contieneDefectosSensorialesOtto(defectos)) {
			sb.append("");
		} else {
			sb.append(resultadoComplementario.getHumedadRelativa());
		}
		
		sb.append(SEPARADOR_SECCION);
		// 16 fugas tubo de escape
		sb.append(resultado.getFugasTuboEscape()).append(SEPARADOR_CAMPO);// 1
		// 17 fugas silenciador
		sb.append(resultado.getFugasSilenciador()).append(SEPARADOR_CAMPO);// 2
		// 18 taba combustible
		sb.append(resultado.getTapaCombustible()).append(SEPARADOR_CAMPO);
		// 19 tapa de aceite
		sb.append(resultado.getTapaAceite()).append(SEPARADOR_CAMPO);
		// 20 salidas adicionales
		sb.append(resultado.getSalidasAdicionales()).append(SEPARADOR_CAMPO);
		// 21 presencia de humos
		sb.append(resultado.getPresenciaHumos()).append(SEPARADOR_CAMPO);
		// 22 revoluciones fuera de rango
		sb.append(resultado.getRevolucionesFueraRango()).append(SEPARADOR_CAMPO);
		// 23 falla sistema de refrigeracion
		sb.append(resultado.getFallaSistemaRefrigeracion()).append(SEPARADOR_SECCION);
	}

	public void codificarInformacionDefectosGasesOtto(StringBuilder sb, List<Medida> medidas, List<Defecto> defectos) {
		utilInformesMedidas.ordenarListaMedidas(medidas);
		ResultadoOttoVehiculosDTO resultado = generadorResultadosDTO.generarResultadoOtto(medidas, defectos);
		// 1 fugas tubo de escape
		sb.append(resultado.getFugasTuboEscape()).append(SEPARADOR_CAMPO);// 1
		// 2 fugas silenciador
		sb.append(resultado.getFugasSilenciador()).append(SEPARADOR_CAMPO);// 2
		// 3 taba combustible
		sb.append(resultado.getTapaCombustible()).append(SEPARADOR_CAMPO);
		// 4 tapa de aceite
		sb.append(resultado.getTapaAceite()).append(SEPARADOR_CAMPO);
		// 5 salidas adicionales
		sb.append(resultado.getSalidasAdicionales()).append(SEPARADOR_CAMPO);
		// 6 presencia de humos
		sb.append(resultado.getPresenciaHumos()).append(SEPARADOR_CAMPO);
		// 7 revoluciones fuera de rango
		sb.append(resultado.getRevolucionesFueraRango()).append(SEPARADOR_CAMPO);
		// 8 falla sistema de refrigeracion
		sb.append(resultado.getFallaSistemaRefrigeracion()).append(SEPARADOR_SECCION);

	}

	public void codificarInformacionGasesMotocicleta(StringBuilder sb, List<Medida> medidas, List<Defecto> defectos,
			Usuario usuario, Boolean dosTiempos) {
		// 1 Nombres y apellidos de usuario que realizo
		sb.append(usuario.getNombres() + " " + usuario.getApellidos()).append(SEPARADOR_CAMPO);
		// 2 identificacion de usuario
		sb.append(usuario.getCedula()).append(SEPARADOR_CAMPO);

		utilInformesMedidas.ordenarListaMedidas(medidas);
		ResultadoOttoMotocicletasDTO resultado = generadorResultadosDTO.generarResultadoOttoMotos(medidas, defectos,
				dosTiempos);
		ResultadoOttoMotocicletasInformeDTO resultadoComplementario = generadorResultadosDTO
				.generarResultadoOttoMotocicletasInforme(medidas, defectos);

		// 1 temperatura ralenti
		sb.append(resultado.getTempRalenti()).append(SEPARADOR_CAMPO);
		// 2 rpm ralenti
		sb.append(resultado.getRpmRalenti()).append(SEPARADOR_CAMPO);
		// 3 hc ralenti
		sb.append(resultado.getHcRalenti()).append(SEPARADOR_CAMPO);
		// 4 co ralenti
		sb.append(resultado.getCoRalenti()).append(SEPARADOR_CAMPO);
		// 5 co2 ralenti
		sb.append(resultado.getCo2Ralenti()).append(SEPARADOR_CAMPO);
		// 6 o2 ralenti
		sb.append(resultado.getO2Ralenti()).append(SEPARADOR_CAMPO);
		// 7 temperatura ambiente
		if( new UtilContieneDefectosSensorialesMoto().contieneDefectosSensorialesMotocicleta(defectos)) {
			sb.append("").append(SEPARADOR_CAMPO);
		}else {
			sb.append(resultadoComplementario.getTemperaturaAmbiente()).append(SEPARADOR_CAMPO);
		}
		
		// 8 humedad relativa
		if(new UtilContieneDefectosSensorialesMoto().contieneDefectosSensorialesMotocicleta(defectos)) {
			sb.append("");
		}else {
			sb.append(resultadoComplementario.getHumedadRelativa());
		}
		// final de seccion
		sb.append(SEPARADOR_SECCION);
		// INSPECCION SENSORIAL
		// 1 revoluciones fuera de rango
		sb.append(resultado.getRevolucionesFueraRango()).append(SEPARADOR_CAMPO);
		// 2 fugas tubo de escape
		sb.append(resultado.getFugasTuboEscape()).append(SEPARADOR_CAMPO);
		// 3 fugas silenciador
		sb.append(resultado.getFugasSilenciador()).append(SEPARADOR_CAMPO);
		// 4 tapa combustible
		sb.append(resultado.getTapaCombustible()).append(SEPARADOR_CAMPO);
		// 5 tapa aceite
		sb.append(resultado.getTapaAceite()).append(SEPARADOR_CAMPO);
		// 6 salidas adicionales
		sb.append(resultado.getSalidasAdicionales()).append(SEPARADOR_CAMPO);
		// 7 presencia humos
		sb.append(resultado.getPresenciaHumos()).append(SEPARADOR_SECCION);

	}

	public void codificarGasesVacia(StringBuilder sbGases) {
		sbGases.append(SEPARADOR_SECCION);
	}

	public void codificarGasesVaciaGases(StringBuilder sbGases) {
		
		sbGases.append(";;;;;;;;;;;;;;;;|;;;;;;;|");
		
	}

}
