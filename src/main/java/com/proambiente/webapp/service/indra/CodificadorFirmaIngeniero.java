package com.proambiente.webapp.service.indra;

import com.proambiente.modelo.Usuario;

import static com.proambiente.webapp.service.indra.CodificadorFurIndra.SEPARADOR_SECCION;
import static com.proambiente.webapp.service.indra.CodificadorFurIndra.SEPARADOR_CAMPO;

public class CodificadorFirmaIngeniero {
	
	public void codificarInfoFirmaIngeniero(Usuario usuario,StringBuilder sb) {
		sb.append(usuario.getNombres()).append(" ").append(usuario.getApellidos()).append(SEPARADOR_CAMPO);
		sb.append(usuario.getCedula()).append(SEPARADOR_SECCION);
	}

}
