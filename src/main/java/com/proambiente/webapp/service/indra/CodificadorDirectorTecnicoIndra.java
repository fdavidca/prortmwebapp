package com.proambiente.webapp.service.indra;

import static com.proambiente.webapp.service.indra.CodificadorFurIndraV2.SEPARADOR_SECCION;

import com.proambiente.modelo.Usuario;

public class CodificadorDirectorTecnicoIndra {

	
	
	public void codificarInformacionDirectorTecnico(StringBuilder sb,Usuario usuarioResponsable) {
		String nombreCompleto = usuarioResponsable.getNombres() + " "+ usuarioResponsable.getApellidos();
		sb.append(nombreCompleto);
		Long identificacionUsuarioResponsableLong = usuarioResponsable.getCedula();
		String identificacion = String.valueOf(identificacionUsuarioResponsableLong);
		sb.append(identificacion);
		sb.append(SEPARADOR_SECCION);
		
	}

}
