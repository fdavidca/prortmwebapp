package com.proambiente.webapp.service.indra;

import static com.proambiente.webapp.service.indra.CodificadorFurIndraV2.SEPARADOR_CAMPO;

import java.util.List;

import com.proambiente.modelo.Medida;
import com.proambiente.modelo.Usuario;
import com.proambiente.webapp.service.generadordto.GeneradorDTOProfundidadLabrado;
import com.proambiente.webapp.util.dto.ResultadoProfundidadLabradoDTO;

public class CodificadorProfLabradoIndra {

	GeneradorDTOProfundidadLabrado generadorDTO = new GeneradorDTOProfundidadLabrado();

	public void codificarProfundidadLabrado(List<Medida> medidas, Usuario usuario, StringBuilder sb) {

		ResultadoProfundidadLabradoDTO resultado = generadorDTO.generarResultadoProfundidadLabrado(medidas);
		String nombres = usuario.getNombres() + " " + usuario.getApellidos();
		// 1 nombre del usuario
		sb.append(nombres).append(SEPARADOR_CAMPO);
		// 2 identificacion del usuario
		sb.append(usuario.getCedula()).append(SEPARADOR_CAMPO);
		// 3 prof eje 1 der 2
		sb.append(resultado.getProfundidadLabradoEje1Der1()).append(SEPARADOR_CAMPO);
		// 4 prof eje 2 der 2
		sb.append(resultado.getProfundidadLabradoEje2Der2()).append(SEPARADOR_CAMPO);
		// 5 prof eje 3 der 2
		sb.append(resultado.getProfundidadLabradoEje3Der2()).append(SEPARADOR_CAMPO);
		// 6 prof eje 4 der 2
		sb.append(resultado.getProfundidadLabradoEje4Der2()).append(SEPARADOR_CAMPO);
		// 7 prof eje 5 der 2
		sb.append(resultado.getProfundidadLabradoEje5Der2()).append(SEPARADOR_CAMPO);
		// 8 prof eje 1 izq 2
		sb.append(resultado.getProfundidadLabradoEje1Izq1()).append(SEPARADOR_CAMPO);
		// 9 prof eje 2 izq 2
		sb.append(resultado.getProfundidadLabradoEje2Izq2()).append(SEPARADOR_CAMPO);
		// 10 prof eje 3 izq 2
		sb.append(resultado.getProfundidadLabradoEje3Izq2()).append(SEPARADOR_CAMPO);
		// 11 prof eje 4 izq 2
		sb.append(resultado.getProfundidadLabradoEje4Izq2()).append(SEPARADOR_CAMPO);
		// 12 prof eje 5 izq 2
		sb.append(resultado.getProfundidadLabradoEje5Izq2()).append(SEPARADOR_CAMPO);
		// 13 prof eje 1 der 1
		sb.append(resultado.getProfundidadLabradoEje2Der1()).append(SEPARADOR_CAMPO);
		// 14 prof eje 2 der 1
		sb.append(resultado.getProfundidadLabradoEje3Der1()).append(SEPARADOR_CAMPO);
		// 15 prof eje 3 der 1
		sb.append(resultado.getProfundidadLabradoEje4Der1()).append(SEPARADOR_CAMPO);
		// 16 prof eje 4 der 1
		sb.append(resultado.getProfundidadLabradoEje5Der1()).append(SEPARADOR_CAMPO);
		// 17 prof eje 5 der 1
		sb.append(resultado.getProfundidadLabradoEje2Izq1()).append(SEPARADOR_CAMPO);
		// 18 prof eje 1 izq 1
		sb.append(resultado.getProfundidadLabradoEje3Izq1()).append(SEPARADOR_CAMPO);
		// 19 prof eje 2 izq 1
		sb.append(resultado.getProfundidadLabradoEje4Izq1()).append(SEPARADOR_CAMPO);
		// 20 prof eje 3 izq 1
		sb.append(resultado.getProfundidadLabradoEje5Izq1()).append(SEPARADOR_CAMPO);
		//21 
		sb.append(resultado.getProfundidadLabradoRepuestoDerecha()).append(SEPARADOR_CAMPO);
		// 22
		sb.append(resultado.getProfundidadLabradoRepuestoIzquierda()).append(SEPARADOR_CAMPO);
		
	}
}
