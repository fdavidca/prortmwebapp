package com.proambiente.webapp.service.indra;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.proambiente.modelo.dto.EquipoDTO;
import static com.proambiente.webapp.util.ConstantesTiposPrueba.*;
import static com.proambiente.webapp.util.ConstantesTiposEquipo.*;

public class UtilIndraEquipos {
	
	Map<Integer, Integer> mapaTipoEquipoPrueba = new HashMap<>();
	
	
	
	public UtilIndraEquipos() {
		super();
		mapaTipoEquipoPrueba.put(TIPO_PRUEBA_INSPECCION_SENSORIAL,PROFUNDIMETRO );
		mapaTipoEquipoPrueba.put(TIPO_PRUEBA_DESVIACION ,ALINEADOR );
		mapaTipoEquipoPrueba.put(TIPO_PRUEBA_FRENOS, FRENOMETRO);
		mapaTipoEquipoPrueba.put(TIPO_PRUEBA_SUSPENSION, ANALIZADOR_SUSPENSION);
		mapaTipoEquipoPrueba.put(TIPO_PRUEBA_TAXIMETRO, TAXIMETRO);
		mapaTipoEquipoPrueba.put(TIPO_PRUEBA_LUCES, LUXOMETRO);
		mapaTipoEquipoPrueba.put(TIPO_PRUEBA_RUIDO, SONOMETRO);
		mapaTipoEquipoPrueba.put(TIPO_PRUEBA_FOTO, -1);
		mapaTipoEquipoPrueba.put(TIPO_PRUEBA_FOTO, -1);
	}



	public Optional<EquipoDTO> buscarEquipoPrincipalDePrueba(List<EquipoDTO> equipos,Integer tipoPruebaId){		
		Optional<EquipoDTO> optEquipoDTO = equipos.stream().filter(edto->edto.getTipoEquipo().equals(mapaTipoEquipoPrueba.get(tipoPruebaId))).findAny();		
		return optEquipoDTO;
	}

}
