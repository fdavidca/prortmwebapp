package com.proambiente.webapp.service.indra;

import com.proambiente.modelo.InformacionCda;

import static com.proambiente.webapp.service.indra.CodificadorFurIndraV2.SEPARADOR_SECCION;

public class CodificadorNombreAplicacion {

	public void codificarNombreAplicacion(StringBuilder sb, InformacionCda infoCda) {
		String nombreSoftware = "PRORTM";
		String versionSoftware = infoCda.getVersionSoftware();
		StringBuilder sbAux = new StringBuilder();
		sbAux.append(nombreSoftware).append(" ").append(versionSoftware);
		sb.append(sbAux).append(SEPARADOR_SECCION);
	}
}
