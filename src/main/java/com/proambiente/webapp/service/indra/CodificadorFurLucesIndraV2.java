package com.proambiente.webapp.service.indra;

import java.util.List;

import com.proambiente.modelo.Medida;
import com.proambiente.modelo.Usuario;
import com.proambiente.webapp.service.generadordto.GeneradorDTOResultadoLuces;
import com.proambiente.webapp.util.dto.ResultadoLucesDtoV2;
import static com.proambiente.webapp.service.indra.CodificadorFurIndraV2.SEPARADOR_CAMPO;
import static com.proambiente.webapp.service.indra.CodificadorFurIndraV2.SEPARADOR_SECCION;

public class CodificadorFurLucesIndraV2 {
	
	
	GeneradorDTOResultadoLuces generadorResultadoLuces = new GeneradorDTOResultadoLuces();
	
	/**
	 * Codificador para todos los vehiculos
	 * @param medidas
	 * @param defectos
	 */
	public void codificarResultadoLucesVehiculo(List<Medida> medidas,Usuario usuario,StringBuilder sb) {
		ResultadoLucesDtoV2 resultado = generadorResultadoLuces.generarResultadoLucesVehiculo(medidas);
		String nombres = usuario.getNombres() + " " + usuario.getApellidos();
		sb.append(nombres).append(SEPARADOR_CAMPO);
		sb.append(usuario.getCedula()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getIntensidadBajaDerecha1()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getIntensidadBajaDerecha2()).append(SEPARADOR_CAMPO);
	    sb.append(resultado.getIntensidadBajaDerecha3()).append(SEPARADOR_CAMPO);
	    sb.append(resultado.getDerBajaSimultaneas()).append(SEPARADOR_CAMPO);
	    sb.append(resultado.getIntensidadBajaIzquierda1()).append(SEPARADOR_CAMPO);
		sb.append(resultado.getIntensidadBajaIzquierda2()).append(SEPARADOR_CAMPO);
	    sb.append(resultado.getIntensidadBajaIzquierda3()).append(SEPARADOR_CAMPO);
	    sb.append(resultado.getIzqBajaSimultaneas()).append(SEPARADOR_CAMPO);
	    sb.append(resultado.getInclinacionBajaDerecha1()).append(SEPARADOR_CAMPO);
	    sb.append(resultado.getInclinacionBajaDerecha2()).append(SEPARADOR_CAMPO);
	    sb.append(resultado.getInclinacionBajaDerecha3()).append(SEPARADOR_CAMPO);
	    sb.append(resultado.getInclinacionBajaIzquierda1()).append(SEPARADOR_CAMPO);
	    sb.append(resultado.getInclinacionBajaIzquierda2()).append(SEPARADOR_CAMPO);
	    sb.append(resultado.getInclinacionBajaIzquierda3()).append(SEPARADOR_CAMPO);
	    sb.append(resultado.getSumatoriaIntensidad()).append(SEPARADOR_CAMPO);
	    sb.append(resultado.getIntensidadAltaDerecha1()).append(SEPARADOR_CAMPO);
	    sb.append(resultado.getIntensidadAltaDerecha2()).append(SEPARADOR_CAMPO);
	    sb.append(resultado.getIntensidadAltaDerecha3()).append(SEPARADOR_CAMPO);
	    sb.append(resultado.getDerAltaSimultaneas()).append(SEPARADOR_CAMPO);
	    sb.append(resultado.getIntensidadAltaIzquierda1()).append(SEPARADOR_CAMPO);
	    sb.append(resultado.getIntensidadAltaIzquierda2()).append(SEPARADOR_CAMPO);
	    sb.append(resultado.getIntensidadAltaIzquierda3()).append(SEPARADOR_CAMPO);
	    sb.append(resultado.getIzqAltaSimultaneas()).append(SEPARADOR_CAMPO);
	    sb.append(resultado.getIntensidadExploradoraDerecha1()).append(SEPARADOR_CAMPO);
	    sb.append(resultado.getIntensidadExploradoraDerecha2()).append(SEPARADOR_CAMPO);
	    sb.append(resultado.getIntensidadExploradoraDerecha3()).append(SEPARADOR_CAMPO);
	    sb.append(resultado.getDerExploradoraSimultaneas()).append(SEPARADOR_CAMPO);
	    sb.append(resultado.getIntensidadExploradoraIzquierda1()).append(SEPARADOR_CAMPO);
	    sb.append(resultado.getIntensidadExploradoraIzquierda2()).append(SEPARADOR_CAMPO);
	    sb.append(resultado.getIntensidadExploradoraIzquierda3()).append(SEPARADOR_CAMPO);
	    sb.append(resultado.getIzqExploradoraSimultaneas()).append(SEPARADOR_SECCION);
	}
	
	public void codificarResultadoLucesMotocicleta(List<Medida> medidas,Usuario usuario,StringBuilder sb) {
		ResultadoLucesDtoV2 resultado = generadorResultadoLuces.generarResultadoLucesMotocicleta(medidas);
		String nombres = usuario.getNombres() + " " + usuario.getApellidos();
		//1
		sb.append(nombres).append(SEPARADOR_CAMPO);
		//2
		sb.append(usuario.getCedula()).append(SEPARADOR_CAMPO);
		//3
		sb.append(resultado.getIntensidadBajaDerecha1()).append(SEPARADOR_CAMPO);
		//4
		sb.append(resultado.getIntensidadBajaDerecha2()).append(SEPARADOR_CAMPO);
		//5
	    sb.append(resultado.getIntensidadBajaDerecha3()).append(SEPARADOR_CAMPO);
	    //6
	    sb.append(resultado.getDerBajaSimultaneas()).append(SEPARADOR_CAMPO);
	    //7
	    sb.append(resultado.getIntensidadBajaIzquierda1()).append(SEPARADOR_CAMPO);
	    //8
		sb.append(resultado.getIntensidadBajaIzquierda2()).append(SEPARADOR_CAMPO);
		//9
	    sb.append(resultado.getIntensidadBajaIzquierda3()).append(SEPARADOR_CAMPO);
	    //10
	    sb.append(resultado.getIzqBajaSimultaneas()).append(SEPARADOR_CAMPO);
	    //11
	    sb.append(resultado.getInclinacionBajaDerecha1()).append(SEPARADOR_CAMPO);
	    //12
	    sb.append(resultado.getInclinacionBajaDerecha2()).append(SEPARADOR_CAMPO);
	    //13
	    sb.append(resultado.getInclinacionBajaDerecha3()).append(SEPARADOR_CAMPO);
	    //14
	    sb.append(resultado.getInclinacionBajaIzquierda1()).append(SEPARADOR_CAMPO);
	    //15
	    sb.append(resultado.getInclinacionBajaIzquierda2()).append(SEPARADOR_CAMPO);
	    //16
	    sb.append(resultado.getInclinacionBajaIzquierda3()).append(SEPARADOR_CAMPO);
	    //17
	    sb.append(resultado.getSumatoriaIntensidad()).append(SEPARADOR_CAMPO);
	    //18
	    sb.append(resultado.getIntensidadAltaDerecha1()).append(SEPARADOR_CAMPO);
	    //19
	    sb.append(resultado.getIntensidadAltaDerecha2()).append(SEPARADOR_CAMPO);
	    //20
	    sb.append(resultado.getIntensidadAltaDerecha3()).append(SEPARADOR_CAMPO);
	    //21
	    sb.append(resultado.getDerAltaSimultaneas()).append(SEPARADOR_CAMPO);
	    //22
	    sb.append(resultado.getIntensidadAltaIzquierda1()).append(SEPARADOR_CAMPO);
	    //23
	    sb.append(resultado.getIntensidadAltaIzquierda2()).append(SEPARADOR_CAMPO);
	    //24
	    sb.append(resultado.getIntensidadAltaIzquierda3()).append(SEPARADOR_CAMPO);
	    //25
	    sb.append(resultado.getIzqAltaSimultaneas()).append(SEPARADOR_CAMPO);
	    //26
	    sb.append(resultado.getIntensidadExploradoraDerecha1()).append(SEPARADOR_CAMPO);
	    //27
	    sb.append(resultado.getIntensidadExploradoraDerecha2()).append(SEPARADOR_CAMPO);
	    //28
	    sb.append(resultado.getIntensidadExploradoraDerecha3()).append(SEPARADOR_CAMPO);
	    //29
	    sb.append(resultado.getDerExploradoraSimultaneas()).append(SEPARADOR_CAMPO);
	    //30
	    sb.append(resultado.getIntensidadExploradoraIzquierda1()).append(SEPARADOR_CAMPO);
	    //31
	    sb.append(resultado.getIntensidadExploradoraIzquierda2()).append(SEPARADOR_CAMPO);
	    //32
	    sb.append(resultado.getIntensidadExploradoraIzquierda3()).append(SEPARADOR_CAMPO);
	    //33
	    sb.append(resultado.getIzqExploradoraSimultaneas()).append(SEPARADOR_SECCION);
		
	}
	
	

}
