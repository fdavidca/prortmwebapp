package com.proambiente.webapp.service.indra;

import java.net.Inet4Address;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.annotation.security.RolesAllowed;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.inject.Inject;
import javax.xml.ws.Response;

import com.proambiente.indra.cliente.EnviarEventosSicovResponse;
import com.proambiente.indra.cliente.EnviarFurSicovResponse;
import com.proambiente.indra.cliente.EnviarRuntSicovResponse;
import com.proambiente.indra.cliente.EventosRespuesta;
import com.proambiente.indra.cliente.FURRespuesta;
import com.proambiente.indra.cliente.PinRespuesta;
import com.proambiente.indra.cliente.SetFURResponse;
import com.proambiente.indra.cliente.SicovSoap;
import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.NivelAlerta;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Revision_;
import com.proambiente.modelo.Usuario;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.modelo.dto.PruebaDTO;
import com.proambiente.webapp.controller.PrincipalService;
import com.proambiente.webapp.dao.InformacionCdaFacade;
import com.proambiente.webapp.service.InspeccionService;
import com.proambiente.webapp.service.PinNoEncontradoException;
import com.proambiente.webapp.service.RegistrarFurService;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.service.guardarpdf.GuardarPdfCallback;
import com.proambiente.webapp.service.notification.NotificationPrimariaService;
import com.proambiente.webapp.service.notification.NotificationSecundariaService;
import com.proambiente.webapp.service.sicov.EtapaRevision;
import com.proambiente.webapp.service.sicov.OperacionesSicov;
import com.proambiente.webapp.util.ConstantesIndra;
import com.proambiente.webapp.util.ConstantesTiposPrueba;

public class OperacionesSicovIndraV2 implements OperacionesSicov {

	private static final Logger logger = Logger.getLogger(OperacionesSicovIndraV2.class.getName());

	public static final String iv = "sicovcontacindra";
	public static final String key = "v239pShjXXXXXXXXXXXXXXXXXXXXXXXX";

	private final static Boolean GENERAR_ALERTA = Boolean.TRUE;
	private final static Boolean NO_GENERAR_ALERTA = Boolean.FALSE;

	@Inject
	ManejadorCodificadorIndraV2 codificadorIndra;

	@Inject
	@SicovSoapConfigurado
	SicovSoap sicovSoap;

	@Inject
	NotificationPrimariaService notificacionService;

	@Inject
	NotificationSecundariaService notificacionSecondaryService;

	@Inject
	RevisionService revisionService;

	@Inject
	InspeccionService inspeccionService;

	@Inject
	CodificadorEventosIndra codificadorEventos;

	@Resource
	ManagedExecutorService executor;

	@Inject
	InformacionCdaFacade infoCdaFacade;

	@Inject
	PrincipalService principalService;

	@Inject
	RegistrarFurService guardarFurService;

	@Override
	public void registrarFUR(Integer revisionId, Integer numeroInspeccion, Envio envio,
			GuardarPdfCallback guardarPdfCallback) {

		try {
			Revision revision = revisionService.cargarRevisionDTO(revisionId, Revision_.preventiva);
			if (revision.getPreventiva()) {
				logger.log(Level.INFO, "Revision " + revisionId + "Es preventiva");
				return;
			}
			String revisionCodificada = codificadorIndra.codificarFurRevision(revisionId, envio);
			logger.log(Level.INFO, "Revision codificada: " + revisionCodificada);
			String revisionEncriptada = IndraEncryptor.encrypt(key, iv, revisionCodificada);
			Future<EnviarFurSicovResponse> response = sicovSoap.enviarFurSicovAsync(revisionEncriptada);
			executor.submit(() -> {
				try {
					EnviarFurSicovResponse respuestaSetFUR = response.get();
					FURRespuesta respuesta = respuestaSetFUR.getEnviarFurSicovResult();
					manejarFURRespuesta(respuesta, revisionId, envio, guardarPdfCallback);
				} catch (Exception exc) {
					logger.log(Level.SEVERE, " Error registrarFUR indra", exc);
					notificacionService.sendNotification(
							" Error registrarFUR indra no se pudo enviar el mensaje " + exc.getMessage(),
							NivelAlerta.ERROR_GRAVE, GENERAR_ALERTA);
				}
			});

		} catch (Exception exc) {
			logger.log(Level.SEVERE, " Error registrar fur indra, revision " + revisionId, exc);
			notificacionService.sendNotification(" Excepcion registrarFUR indra " + revisionId + " " + exc.getMessage(),
					NivelAlerta.ERROR_GRAVE, GENERAR_ALERTA);
		}

	}

	@RolesAllowed(value = { "ADMINISTRADOR" })
	@Override
	public void registrarInformacionRunt(Integer revisionId,GuardarPdfCallback guardarPdfCallback) {
		try {
			Revision revision = revisionService.cargarRevision(revisionId);
			if (revision.getPreventiva()) {
				logger.log(Level.INFO, "Revision " + revisionId + "Es preventiva");
				return;
			}
			Usuario usuario = principalService.getUsuarioAutenticado();

			String nombreEmpleado = usuario.getNombres() + " " + usuario.getApellidos();
			String numeroIdentificacion = String.valueOf(usuario.getCedula());

			Vehiculo vehiculo = revision.getVehiculo();
			String placa = vehiculo.getPlaca();
			String extranjero = vehiculo.getNacionalidad().equals("N") ? "N" : "S";
			String consecutivoRunt = revision.getConsecutivoRunt();
			consecutivoRunt = consecutivoRunt.trim();
			String idRunt = String.valueOf(infoCdaFacade.find(1).getIdRuntCda());
			String ip = Inet4Address.getLocalHost().getHostAddress();

			StringBuilder sb = new StringBuilder();
			sb.append("Revision Id:").append(revisionId);
			sb.append("Registo de informacion runt");
			sb.append("Nombre Empleado:").append(nombreEmpleado);
			sb.append("Numero de identificacion:").append(numeroIdentificacion);
			sb.append("Placa:").append(placa);
			sb.append("Extranjero:").append(extranjero);
			sb.append("Consecutivo Runt:").append(consecutivoRunt);
			sb.append("Id Runt").append(idRunt);
			sb.append("Direccion ip:").append(ip);
			logger.log(Level.INFO, sb.toString());

			Future<EnviarRuntSicovResponse> response = sicovSoap.enviarRuntSicovAsync(nombreEmpleado,
					numeroIdentificacion, placa, extranjero, consecutivoRunt, idRunt, ip);
			executor.submit(() -> {
				try {
					EnviarRuntSicovResponse respuesta = response.get();
					FURRespuesta furRespuesta = respuesta.getEnviarRuntSicovResult();
					manejarFURRespuesta(furRespuesta, revisionId, Envio.SEGUNDO_ENVIO,guardarPdfCallback);
				} catch (Exception exc) {
					logger.log(Level.SEVERE, " Error registrarFUR indra", exc);
					notificacionService.sendNotification(
							" Error registrarFUR indra no se pudo enviar el mensaje " + exc.getMessage(),
							NivelAlerta.ERROR_GRAVE, GENERAR_ALERTA);
				}
			});

		} catch (Exception exc) {
			logger.log(Level.SEVERE, " Error registrar fur indra, revision " + revisionId, exc);
			notificacionService.sendNotification(" Excepcion registrarFUR indra " + revisionId + " " + exc.getMessage(),
					NivelAlerta.ERROR_GRAVE, GENERAR_ALERTA);
		}
	}

	public Optional<FURRespuesta> registrarInformacionRuntSincrono(Integer revisionId, StringBuilder sb) {
		try {
			Revision revision = revisionService.cargarRevision(revisionId);
			if (revision.getPreventiva()) {
				logger.log(Level.INFO, "Revision " + revisionId + "Es preventiva");
				return Optional.empty();
			}
			Usuario usuario = principalService.getUsuarioAutenticado();

			String nombreEmpleado = usuario.getNombres() + " " + usuario.getApellidos();
			String numeroIdentificacion = String.valueOf(usuario.getCedula());

			Vehiculo vehiculo = revision.getVehiculo();
			String placa = vehiculo.getPlaca();
			String extranjero = vehiculo.getNacionalidad().equals("N") ? "N" : "S";
			String consecutivoRunt = revision.getConsecutivoRunt();
			String idRunt = String.valueOf(infoCdaFacade.find(1).getIdRuntCda());
			String ip = Inet4Address.getLocalHost().getHostAddress();

			sb.append("Revision Id:").append(revisionId);
			sb.append(" Reg info runt");
			sb.append(" Empleado: ").append(nombreEmpleado);
			sb.append(" identificacion: ").append(numeroIdentificacion);
			sb.append(" Placa: ").append(placa);
			sb.append(" Extranjero: ").append(extranjero);
			sb.append(" ConsecRunt: ").append(consecutivoRunt);
			sb.append(" Id Runt: ").append(idRunt);
			sb.append(" ip: ").append(ip);
			logger.log(Level.INFO, sb.toString());

			FURRespuesta response = sicovSoap.enviarRuntSicov(nombreEmpleado, numeroIdentificacion, placa, extranjero,
					consecutivoRunt, idRunt, ip);
			return Optional.of(response);

		} catch (Exception exc) {
			logger.log(Level.SEVERE, " Error registrar fur indra, revision " + revisionId, exc);
			throw new RuntimeException("Error registrar segundo envio indra" + exc.getMessage());
		}

	}

	@Override
	public void registrarFURInspeccionPrimerEnvio(Integer inspeccionId) {
		try {

			String revisionCodificada = codificadorIndra.codificarFurInspeccion(inspeccionId);
			Integer revisionId = inspeccionService.revisionIdDesdeInspeccionId(inspeccionId);
			logger.log(Level.INFO, "Inspeccion codificada: " + revisionCodificada);
			String revisionEncriptada = IndraEncryptor.encrypt(key, iv, revisionCodificada);

			Response<EnviarFurSicovResponse> response = sicovSoap.enviarFurSicovAsync(revisionEncriptada);
			executor.submit(() -> {
				try {
					EnviarFurSicovResponse respuestaSetFUR = response.get();
					FURRespuesta respuesta = respuestaSetFUR.getEnviarFurSicovResult();
					manejarFURRespuesta(respuesta, revisionId, Envio.SEGUNDO_ENVIO);
				} catch (Exception exc) {
					logger.log(Level.SEVERE, " Error registrarFUR indra", exc);
					notificacionService.sendNotification(
							" Error registrarFUR indra no se pudo enviar el mensaje " + exc.getMessage(),
							NivelAlerta.ERROR_GRAVE, GENERAR_ALERTA);
				}
			});

		} catch (Exception exc) {
			logger.log(Level.SEVERE, " Error registrar fur indra", exc);
			notificacionService.sendNotification(" Excepcion registrarFUR indra " + exc.getMessage(),
					NivelAlerta.ERROR_GRAVE, GENERAR_ALERTA);
		}

	}

	@Override
	public void registrarFURInspeccionSegundoEnvio(Integer inspeccionId) {
		try {

			String revisionCodificada = codificadorIndra.codificarFurInspeccion(inspeccionId);
			Integer revisionId = inspeccionService.revisionIdDesdeInspeccionId(inspeccionId);
			logger.log(Level.INFO, "Inspeccion codificada: " + revisionCodificada);
			String revisionEncriptada = IndraEncryptor.encrypt(key, iv, revisionCodificada);

			Response<EnviarFurSicovResponse> response = sicovSoap.enviarFurSicovAsync(revisionEncriptada);
			executor.submit(() -> {
				try {
					EnviarFurSicovResponse respuestaSetFUR = response.get();
					FURRespuesta respuesta = respuestaSetFUR.getEnviarFurSicovResult();
					manejarFURRespuesta(respuesta, revisionId, Envio.SEGUNDO_ENVIO);
				} catch (Exception exc) {
					logger.log(Level.SEVERE, " Error registrarFUR indra", exc);
					notificacionService.sendNotification(
							" Error registrarFUR indra no se pudo enviar el mensaje " + exc.getMessage(),
							NivelAlerta.ERROR_GRAVE, GENERAR_ALERTA);
				}
			});

		} catch (Exception exc) {
			logger.log(Level.SEVERE, " Error registrar fur indra", exc);
			notificacionService.sendNotification(" Excepcion registrarFUR indra " + exc.getMessage(),
					NivelAlerta.ERROR_GRAVE, GENERAR_ALERTA);
		}

	}

	@Override
	public void registrarEventoRTMEC(PruebaDTO prueba, String placa, Date fechaRevision, String serialesEquipos,
			String causaAborto, Integer tipoEventoPrueba, EtapaRevision etapaRevision) {
		Integer revisionId = prueba.getRevisionId();
		Revision revision = revisionService.cargarRevisionDTO(revisionId, Revision_.preventiva);
		if (revision.getPreventiva()) {
			logger.log(Level.INFO, "Revision " + revisionId + "Es preventiva");
			return;
		}

		InformacionCda infoCda = infoCdaFacade.find(1);
		String cadenaCodificada = codificadorEventos.codificarEventoNuevaResolucion(prueba, placa, fechaRevision,
				serialesEquipos, causaAborto, tipoEventoPrueba, String.valueOf(infoCda.getIdRuntCda()));

		logger.log(Level.INFO, "Codificada " + cadenaCodificada);

		executor.submit(() -> {

			try {
				String cadenaEncriptada = IndraEncryptor.encrypt(key, iv, cadenaCodificada);
				Response<EnviarEventosSicovResponse> response = sicovSoap.enviarEventosSicovAsync(cadenaEncriptada);
				EnviarEventosSicovResponse eventResponse = response.get();
				EventosRespuesta furRespuesta = eventResponse.getEnviarEventosSicovResult();
				StringBuilder descripcionSb = new StringBuilder();

				construirDescripcionEvento(tipoEventoPrueba, prueba.getTipoPruebaId(), descripcionSb);
				manejarEventosRespuesta(furRespuesta, placa, cadenaCodificada, descripcionSb.toString());
			} catch (InterruptedException e) {
				logger.log(Level.SEVERE, " Excepcion registrarEventoRTMEC", e);

			} catch (ExecutionException e) {
				logger.log(Level.SEVERE, " Excepcion registrarEventoRTMEC", e);
				notificacionService.sendNotification(" Excepcion registrarEventoRTMEC indra " + e.getMessage(),
						NivelAlerta.ERROR_GRAVE, GENERAR_ALERTA);

			} catch (Exception e) {
				logger.log(Level.SEVERE, " Excepcion registrarEventoRTMEC", e);
				notificacionService.sendNotification(" Excepcion registrarEventoRTMEC indra " + e.getMessage(),
						NivelAlerta.ERROR_GRAVE, GENERAR_ALERTA);
			}

		});

	}

	void construirDescripcionEvento(Integer tipoEventoPrueba, Integer tipoPruebaId, StringBuilder descripcionSb) {
		switch (tipoEventoPrueba) {
		case ConstantesIndra.INICIO_PRUEBA:
			descripcionSb.append("Inicio de prueba ");
			break;
		case ConstantesIndra.FINALIZACION_PRUEBA:
			descripcionSb.append("Fin de prueba ");
			break;
		case ConstantesIndra.ABORTO_PRUEBA:
			descripcionSb.append("Aborto de prueba ");
			break;
		}

		switch (tipoPruebaId) {
		case ConstantesTiposPrueba.TIPO_PRUEBA_GASES:
			descripcionSb.append(" Gases ");
			break;
		case ConstantesTiposPrueba.TIPO_PRUEBA_LUCES:
			descripcionSb.append(" Luces ");
			break;
		case ConstantesTiposPrueba.TIPO_PRUEBA_INSPECCION_SENSORIAL:
			descripcionSb.append(" Inspeccion Sensorial ");
			break;
		case ConstantesTiposPrueba.TIPO_PRUEBA_TAXIMETRO:
			descripcionSb.append(" Taximetro ");
			break;
		case ConstantesTiposPrueba.TIPO_PRUEBA_DESVIACION:
			descripcionSb.append(" Alineacion ");
			break;
		case ConstantesTiposPrueba.TIPO_PRUEBA_SUSPENSION:
			descripcionSb.append(" Suspension ");
			break;
		case ConstantesTiposPrueba.TIPO_PRUEBA_FRENOS:
			descripcionSb.append(" Frenos ");
			break;
		case ConstantesTiposPrueba.TIPO_PRUEBA_RUIDO:
			descripcionSb.append(" Ruido ");
			break;
		}

	}

	@Override
	public String consultarPinPlaca(String placa) throws PinNoEncontradoException {
		String PIN = "PIN-INDRA";
		InformacionCda infoCda = infoCdaFacade.find(1);
		LocalDate fecha = LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String fechaStr = fecha.format(formatter);
		String numeroRuntCda =infoCda.getNumeroCda() != null ? infoCda.getNumeroCda():"";
		PinRespuesta respuestaPin = sicovSoap.getInfoPin(placa, numeroRuntCda, fechaStr);
		logger.log(Level.INFO, "respuesta consultar pin indra: " + respuestaPin.toString());
		if (respuestaPin != null && respuestaPin.getCodRespuesta() == 1) {
			if (respuestaPin.getPinRespuesta().getPruebaActiva() == 1) {
				PIN = respuestaPin.getPinRespuesta().getNumeroPin();
			} else {
				throw new PinNoEncontradoException(respuestaPin.getPinRespuesta().toString());
			}

		}else {
			throw new PinNoEncontradoException(respuestaPin.getMsjRespuesta());
		}

		return PIN;
	}

	@Override
	public void enviarEco(String mensaje) {
		try {
			String ecoRespuesta = sicovSoap.setConexion(mensaje);
			logger.log(Level.INFO, " Respuesta eco indra " + ecoRespuesta);
		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Excepcion enviando eco", exc);
		}
	}

	@Override
	public void utilizarPin(String pin, String tipoRtm, String placa) {
		logger.log(Level.INFO, " Utilizacion pin indra no implementa");
	}

	private void manejarFURRespuesta(FURRespuesta rta, Integer revisionId, Envio envio,
			GuardarPdfCallback guardarPdfCallback) {

		logger.log(Level.INFO, "Id revision : " + revisionId + " Respuesta fur " + rta.toString());
		Integer codigoRespuesta = rta.getCodRespuesta();
		switch (codigoRespuesta) {
		case 0:

			if (envio.equals(Envio.SEGUNDO_ENVIO)) {
				if (rta.getMsjRespuesta().contains("Finalizada")) {
					logger.log(Level.INFO, "Segundo envio ya registrado " + rta.getMsjRespuesta());
					revisionService.registrarSegundoEnvio(revisionId);
					notificacionService.sendNotification("Revision finalizada " + rta.getMsjRespuesta(),
							NivelAlerta.INFORMACION, NO_GENERAR_ALERTA);
					return;
				}
			}
			notificacionService.sendNotification(
					" Error registrarFUR indra, revision  " + revisionId + " " + rta.toString(),
					NivelAlerta.ERROR_GRAVE, GENERAR_ALERTA);
			logger.log(Level.SEVERE,
					" Error registrarFUR indra, revisionId " + revisionId + " :  " + rta.getMsjRespuesta());
			break;
		case 1:
			logger.log(Level.INFO, " Operacion exitosa ");
			notificacionService.sendNotification(" Operacion Exitosa " + rta.getMsjRespuesta(), NivelAlerta.INFORMACION,
					NO_GENERAR_ALERTA);
			if (envio.equals(Envio.PRIMER_ENVIO)) {
				revisionService.registrarPrimerEnvio(revisionId);
				
			} else {
				revisionService.registrarSegundoEnvio(revisionId);
				guardarPdfCallback.guardarPdf(revisionId);
			}
			break;
		}

	}

	private void manejarFURRespuesta(FURRespuesta rta, Integer revisionId, Envio envio) {

		logger.log(Level.INFO, "Id revision : " + revisionId + " Respuesta fur " + rta.toString());
		Integer codigoRespuesta = rta.getCodRespuesta();
		switch (codigoRespuesta) {
		case 0:

			if (envio.equals(Envio.SEGUNDO_ENVIO)) {
				if (rta.getMsjRespuesta().contains("Finalizada")) {
					logger.log(Level.INFO, "Segundo envio ya registrado " + rta.getMsjRespuesta());
					revisionService.registrarSegundoEnvio(revisionId);
					notificacionService.sendNotification("Revision finalizada " + rta.getMsjRespuesta(),
							NivelAlerta.INFORMACION, NO_GENERAR_ALERTA);
					return;
				}
			}
			notificacionService.sendNotification(
					" Error registrarFUR indra, revision  " + revisionId + " " + rta.toString(),
					NivelAlerta.ERROR_GRAVE, GENERAR_ALERTA);
			logger.log(Level.SEVERE,
					" Error registrarFUR indra, revisionId " + revisionId + " :  " + rta.getMsjRespuesta());
			break;
		case 1:
			logger.log(Level.INFO, " Operacion exitosa ");
			notificacionService.sendNotification(" Operacion Exitosa " + rta.getMsjRespuesta(), NivelAlerta.INFORMACION,
					NO_GENERAR_ALERTA);
			if (envio.equals(Envio.PRIMER_ENVIO)) {
				revisionService.registrarPrimerEnvio(revisionId);

			} else {
				revisionService.registrarSegundoEnvio(revisionId);
			}
			break;
		}

	}

	private void manejarEventosRespuesta(EventosRespuesta rta, String placa, String cadena, String descripcion) {

		logger.log(Level.INFO, "Respuesta fur " + "placa " + placa + "Mensaje respuesta" + rta.getMsjRespuesta());
		Integer codigoRespuesta = rta.getCodRespuesta();
		switch (codigoRespuesta) {
		case 0:
			notificacionSecondaryService.sendSecondaryNotification(
					" Error Eventos indra  placa " + placa + " " + rta.getMsjRespuesta() + descripcion,
					NivelAlerta.ERROR_GRAVE, GENERAR_ALERTA);
			logger.log(Level.SEVERE, " Error registrarFUR indra" + rta.toString());
			break;
		case 1:
			logger.log(Level.INFO, " Operacion exitosa ");
			notificacionSecondaryService.sendSecondaryNotification(
					" Operacion Exitosa placa " + placa + ":" + rta.getMsjRespuesta() + descripcion,
					NivelAlerta.INFORMACION, NO_GENERAR_ALERTA);
			break;
		}

	}

}
