package com.proambiente.webapp.service.indra;

import com.proambiente.modelo.InformacionCda;
import com.proambiente.webapp.util.ConstantesNombreSoftware;
import static com.proambiente.webapp.service.indra.CodificadorFurIndra.SEPARADOR_SECCION;

public class CodificadorInfoSoftware {
	
	
	public void codificarInfornmacionSoftware(InformacionCda infoCda,StringBuilder sb) {
		
		String versionSoftware = infoCda.getVersionSoftware();
		sb.append(ConstantesNombreSoftware.NOMBRE_SOFTWARE).append(" v ").append(versionSoftware);
		String aplicativosUtilizados = infoCda.getAplicativosUtilizados() != null ? infoCda.getAplicativosUtilizados() : "";
		if(aplicativosUtilizados.isEmpty()) {
			
		}else {
			sb.append("-").append(aplicativosUtilizados);
		}
		sb.append(SEPARADOR_SECCION);		
	}

}
