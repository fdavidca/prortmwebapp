package com.proambiente.webapp.service.indra;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.proambiente.webapp.service.indra.CodificadorFurIndra.SEPARADOR_SECCION;
import static com.proambiente.webapp.service.sicov.UtilEncontrarPruebas.encontrarPruebaPorTipo;
import static com.proambiente.webapp.service.util.UtilTipoVehiculo.isVehiculoMoto;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.proambiente.modelo.Certificado;
import com.proambiente.modelo.Defecto;
import com.proambiente.modelo.Foto;
import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.Inspeccion;
import com.proambiente.modelo.Medida;
import com.proambiente.modelo.Propietario;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Usuario;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.webapp.dao.FotoFacade;
import com.proambiente.webapp.dao.InformacionCdaFacade;
import com.proambiente.webapp.service.CertificadoService;
import com.proambiente.webapp.service.DefectoService;
import com.proambiente.webapp.service.InspeccionService;
import com.proambiente.webapp.service.PruebasService;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.service.UtilInformacionProfundidadLabrado;
import com.proambiente.webapp.service.VehiculoService;
import com.proambiente.webapp.service.sicov.OperacionesSicov.Envio;
import com.proambiente.webapp.service.util.UtilTipoVehiculo;
import com.proambiente.webapp.util.ConstantesTiposCombustible;
import com.proambiente.webapp.util.ConstantesTiposPrueba;
import com.proambiente.webapp.util.ConstantesTiposVehiculo;

public class ManejadorCodificarIndra {

	private static final Logger logger = Logger.getLogger(ManejadorCodificarIndra.class.getName());
	
	CodificadorDatosDefectos codificadorDefectos = new CodificadorDatosDefectos();
	
	CodificadorResultadoTaximetro codificadorTaximetro = new CodificadorResultadoTaximetro();
	
	CodificadorObservaciones codificadorObservaciones = new CodificadorObservaciones();

	@Inject
	CodificadorFurIndra codificador;

	@Inject
	RevisionService revisionService;
	
	@Inject
	UtilInformacionProfundidadLabrado utilProfundidad;

	@Inject
	PruebasService pruebaService;

	@Inject
	FotoFacade fotoDAO;
	
	@Inject
	DefectoService defectoService;
	
	@Inject
	CertificadoService certificadoService;
	
	@Inject
	InformacionCdaFacade informacionCdaDAO;
	
	@Inject
	VehiculoService vehiculoService;
	
	@Inject 
	InspeccionService inspeccionService;
	
	public String codificarFURInspeccion(Integer inspeccionId,Envio envio) {
		StringBuilder sbSeccionProveedor = generarSeccionIdProveedor();
		Inspeccion inspeccion = inspeccionService.cargarInspeccion(inspeccionId);
		Boolean aprobada = inspeccion.getAprobada();
		//obtener informacion de vehiculo y propietario mediante la revision
		
		Integer revisionId = inspeccionService.revisionIdDesdeInspeccionId(inspeccionId);
		Revision revision = revisionService.cargarRevision(revisionId);
		
		StringBuilder sbSeccionPropietario = generarSeccionPropietario(revision);
		
		Vehiculo vehiculo = revision.getVehiculo();
		
		checkNotNull(vehiculo,"Revision sin vehiculo registrado %d",revision.getRevisionId());
		
		StringBuilder sbSeccionVehiculo = generarSeccionVehiculoInspeccion(revision,inspeccion,vehiculo,aprobada);
		
		List<Prueba> pruebasInspeccion = inspeccionService.pruebasInspeccion(inspeccionId);
		
		checkArgument(pruebasInspeccion != null && !pruebasInspeccion.isEmpty(),"No existen pruebas para inspeccion %d" , inspeccionId);
		
		StringBuilder sbSeccionFoto = generarSeccionFoto(revisionId, vehiculo, pruebasInspeccion);

		StringBuilder sbSeccionGases = generarSeccionGases(vehiculo, pruebasInspeccion);
		
		StringBuilder sbSeccionLuces = generarSeccionLuces(revisionId, pruebasInspeccion,isVehiculoMoto(vehiculo.getTipoVehiculo()));
		
		StringBuilder sbSeccionRuido = generarSeccionRuido(revisionId, pruebasInspeccion);
		
		//RESULTADO FAS
		StringBuilder sbSeccionFAS = generarSeccionFAS(revisionId, vehiculo, pruebasInspeccion);
		
		//RESULTADOS VISUAL
		StringBuilder sbInspeccionSensorial  = generarSeccionInspeccionSensorialReinspeccion(revisionId, pruebasInspeccion);
		
		//RESULTADOS TAXIMETROS
		StringBuilder sbTaximetro = generarSeccionTaximetro(revisionId, vehiculo, pruebasInspeccion);
		
		//CODIFICAR OBSERVACIONES
		StringBuilder sbObservaciones = generarObservacionesInspeccion(inspeccion,vehiculo,pruebasInspeccion);
		
		StringBuilder sbSeccionRunt = generarSeccionRuntInspeccion(inspeccion, revision,envio);
		//concatenar stringbuilders
		
		sbSeccionProveedor.append(sbSeccionPropietario).append(sbSeccionVehiculo);
		sbSeccionProveedor.append(sbSeccionFoto).append(sbSeccionGases).append(sbSeccionLuces);
		sbSeccionProveedor.append(sbSeccionRuido).append(sbSeccionFAS).append(sbInspeccionSensorial);
		sbSeccionProveedor.append(sbTaximetro).append(sbObservaciones).append(sbSeccionRunt);			
		return sbSeccionProveedor.toString();
		
	}

	public String codificarFURRevision(Integer revisionId, Envio envio) {
		
		
			StringBuilder sbSeccionProveedor = generarSeccionIdProveedor();

			Revision revision = revisionService.cargarRevision(revisionId);
			
			Boolean aprobada = revisionService.isRevisionAprobada(revisionId);

			StringBuilder sbSeccionPropietario = generarSeccionPropietario(revision);
			
			Vehiculo vehiculo = revision.getVehiculo();
			
			checkNotNull(vehiculo,"Revision sin vehiculo registrado %d",revision.getRevisionId());
			
			StringBuilder sbSeccionVehiculo = generarSeccionVehiculo(revision,vehiculo,aprobada);

			List<Prueba> pruebasRevision = pruebaService.pruebasRevision(revisionId);
			
			checkArgument(pruebasRevision != null && !pruebasRevision.isEmpty(),"No existen pruebas para revision %d" , revisionId);

			StringBuilder sbSeccionFoto = generarSeccionFoto(revisionId, vehiculo, pruebasRevision);

			StringBuilder sbSeccionGases = generarSeccionGases(vehiculo, pruebasRevision);
			
			StringBuilder sbSeccionLuces = generarSeccionLuces(revisionId, pruebasRevision,isVehiculoMoto(vehiculo.getTipoVehiculo()));
			
			//RESULTADO ruidos
			StringBuilder sbSeccionRuido = generarSeccionRuido(revisionId, pruebasRevision);
			
			//RESULTADO FAS
			StringBuilder sbSeccionFAS = generarSeccionFAS(revisionId, vehiculo, pruebasRevision);
			
			//RESULTADOS VISUAL
			StringBuilder sbInspeccionSensorial  = generarSeccionInspeccionSensorial(revisionId, pruebasRevision);
			
			//RESULTADOS TAXIMETROS
			StringBuilder sbTaximetro = generarSeccionTaximetro(revisionId, vehiculo, pruebasRevision);
			
			//CODIFICAR OBSERVACIONES
			StringBuilder sbObservaciones = generarObservaciones(revision, pruebasRevision);			
			
			//DATOS RUNT
			
			StringBuilder	sbSeccionRunt = generarSeccionRunt(revisionId, revision,aprobada,envio);			//concatenar stringbuilders
			
			sbSeccionProveedor.append(sbSeccionPropietario).append(sbSeccionVehiculo);
			sbSeccionProveedor.append(sbSeccionFoto).append(sbSeccionGases).append(sbSeccionLuces);
			sbSeccionProveedor.append(sbSeccionRuido).append(sbSeccionFAS).append(sbInspeccionSensorial);
			sbSeccionProveedor.append(sbTaximetro).append(sbObservaciones).append(sbSeccionRunt);			
			return sbSeccionProveedor.toString();
		
	}
	
	

	private StringBuilder generarSeccionFAS(Integer revisionId, Vehiculo vehiculo, List<Prueba> pruebasRevision) {
		
		StringBuilder sbFrenos = new StringBuilder();
		Optional<Prueba> pruebaFrenos = encontrarPruebaPorTipo(pruebasRevision,ConstantesTiposPrueba.TIPO_PRUEBA_FRENOS);
		Optional<Prueba> pruebaSuspension = encontrarPruebaPorTipo(pruebasRevision,ConstantesTiposPrueba.TIPO_PRUEBA_SUSPENSION);
		Optional<Prueba> pruebaDesviacion = encontrarPruebaPorTipo(pruebasRevision,ConstantesTiposPrueba.TIPO_PRUEBA_DESVIACION);
		
		List<Medida> medidasUnidas = new ArrayList<>();
		//frenos nunca es opcional en una revision
		
		Usuario usuarioFrenos ;
		
		Integer tipoVehiculoId = vehiculo.getTipoVehiculo().getTipoVehiculoId();
		
		if(pruebaFrenos.isPresent()){
			List<Medida> medidasFrenos = pruebaService.obtenerMedidas(pruebaFrenos.get());			
			medidasUnidas.addAll(medidasFrenos);
			usuarioFrenos = pruebaFrenos.get().getUsuario();
			checkNotNull("No existe usuario registrado prueba de frenos revision %d",revisionId);
		}else {
			logger.log(Level.SEVERE, "No hay prueba de frenos para revision" + revisionId );
			throw new RuntimeException("No hay prueba de frenos para revision" + revisionId);
		}
		
		if(pruebaSuspension.isPresent()){
			List<Medida> medidasSuspension = pruebaService.obtenerMedidas(pruebaSuspension.get());			
			medidasUnidas.addAll(medidasSuspension);
		}else{
			Boolean isVehiculoLiviano = tipoVehiculoId.equals(ConstantesTiposVehiculo.LIVIANO) || tipoVehiculoId.equals(ConstantesTiposVehiculo.CUATROxCUATRO);
			if(isVehiculoLiviano){
				logger.log(Level.SEVERE, "Error el vehiculo debe tener suspensión");
			}
		}
		if(pruebaDesviacion.isPresent()){
			List<Medida> medidasDesviacion = pruebaService.obtenerMedidas(pruebaDesviacion.get());
			medidasUnidas.addAll(medidasDesviacion);				
		}else{				
			logger.log(Level.INFO, "Prueba desviacion no presente para revision" + revisionId );
		}
		
		Integer numeroEjes = vehiculo.getNumeroEjes();
		codificador.codificarMedidasFas(sbFrenos, medidasUnidas, usuarioFrenos, numeroEjes);
		return sbFrenos;
	}
	
	private StringBuilder generarObservacionesInspeccion(Inspeccion inspeccion,Vehiculo vehiculo,List<Prueba> pruebasInspeccion) {
		StringBuilder sbObservaciones = new StringBuilder();
		String comentariosInspeccion = inspeccion.getComentario()!= null ? inspeccion.getComentario() : "";		
		String comentariosSinComas = comentariosInspeccion.replace('.',',');
		comentariosSinComas = comentariosSinComas.replace("\n", " ");
		List<Prueba> pruebasFrenosInspeccionSensorial = pruebasInspeccion.stream()
				.filter(p -> p.getTipoPrueba().getTipoPruebaId().equals(1) || p.getTipoPrueba().getTipoPruebaId().equals(5))
				.collect(Collectors.toList());
		
		Boolean isMoto = UtilTipoVehiculo.isVehiculoMoto(vehiculo.getTipoVehiculo());
		Boolean isPesado = UtilTipoVehiculo.isVehiculoPesado(vehiculo.getTipoVehiculo());
		String labrado = utilProfundidad.cargarInformacionProfundidadLabrado(pruebasFrenosInspeccionSensorial,isMoto,isPesado);
		labrado = labrado.replaceAll("\n"," ");
		sbObservaciones.append(comentariosSinComas).append(" ");
		sbObservaciones.append(labrado).append(SEPARADOR_SECCION);
		return sbObservaciones;
		
	}

	private StringBuilder generarObservaciones(Revision revision, List<Prueba> pruebasRevision) {
		StringBuilder sbObservaciones = new StringBuilder();
		
		
		codificadorObservaciones.codificarObservaciones(sbObservaciones, revision, pruebasRevision);
		
		List<Prueba> pruebasFrenosInspeccionSensorial = pruebasRevision.stream()
				.filter(p -> p.getTipoPrueba().getTipoPruebaId().equals(1) || p.getTipoPrueba().getTipoPruebaId().equals(5))
				.collect(Collectors.toList());
		
		Boolean isMoto = UtilTipoVehiculo.isVehiculoMoto(revision.getVehiculo().getTipoVehiculo());
		Boolean isPesado = UtilTipoVehiculo.isVehiculoPesado(revision.getVehiculo().getTipoVehiculo());
		String labrado = utilProfundidad.cargarInformacionProfundidadLabrado(pruebasFrenosInspeccionSensorial,isMoto,isPesado);
		labrado = labrado.replaceAll("\n"," ");
		sbObservaciones.append(labrado).append(SEPARADOR_SECCION);
		return sbObservaciones;
	}
	
	private StringBuilder generarSeccionRuntInspeccion(Inspeccion inspeccion,Revision revision,Envio envio) {
		StringBuilder sbRunt = new StringBuilder();
		InformacionCda infoCda = informacionCdaDAO.find(1);
		if(envio.equals(Envio.SEGUNDO_ENVIO)) {
			Certificado certificado = null;
			if(inspeccion.getAprobada()) {
				certificado = certificadoService.consultarUltimoCertificado(revision.getRevisionId());
			}			
			codificador.codificarEstructuraDatosRunt(sbRunt, revision, certificado, String.valueOf(infoCda.getIdRuntCda()));
		} else {
			codificador.codificarEstructuraDatosRuntVacia(sbRunt, revision,  String.valueOf(infoCda.getIdRuntCda()));
		}
		
		return sbRunt;
	}

	private StringBuilder generarSeccionRunt(Integer revisionId, Revision revision,Boolean aprobada, Envio envio) {
		StringBuilder sbRunt = new StringBuilder();
		Certificado certificado = null;
		try {
			if(aprobada && envio.equals(Envio.SEGUNDO_ENVIO)) {
				certificado = certificadoService.consultarUltimoCertificado(revisionId);
			}
		}catch(Exception exc) {
			throw new RuntimeException("No existe certificado para revision aprobada");
		}			
		InformacionCda infoCda = informacionCdaDAO.find(1);
		if(envio.equals(Envio.SEGUNDO_ENVIO)) {
			codificador.codificarEstructuraDatosRunt(sbRunt, revision, certificado, String.valueOf(infoCda.getIdRuntCda()));
		}else {
			codificador.codificarEstructuraDatosRuntVacia(sbRunt, revision,  String.valueOf(infoCda.getIdRuntCda()));
		}
		return sbRunt;
	}
	
	
	private StringBuilder generarSeccionTaximetro(Integer revisionId, Vehiculo vehiculo, List<Prueba> pruebasRevision) {
		StringBuilder sbTaximetro = new StringBuilder();
		Optional<Prueba> optPruebaTaximetro = encontrarPruebaPorTipo(pruebasRevision,ConstantesTiposPrueba.TIPO_PRUEBA_TAXIMETRO);
		if(optPruebaTaximetro.isPresent()){
			Prueba pruebaTaximetro = optPruebaTaximetro.get();
			Usuario usuarioTaximetro = pruebaTaximetro.getUsuario();
			List<Medida> medidasTaximetro = pruebaService.obtenerMedidas(pruebaTaximetro);
			List<Defecto> defectosTaximetro = pruebaService.obtenerDefectos(pruebaTaximetro);
			String refLlanta = codificadorTaximetro.informacionReferenciaLlanta(pruebasRevision);
			codificadorTaximetro.codificarResultadoTaximetro(sbTaximetro, medidasTaximetro, defectosTaximetro, usuarioTaximetro, refLlanta,Boolean.TRUE);
		}else{				
			logger.log(Level.INFO, "No hay prueba de taximetro para revision " + revisionId);
			codificadorTaximetro.codificarTaximetroNoRequerido(sbTaximetro);
		}
		return sbTaximetro;
	}

	private StringBuilder generarSeccionInspeccionSensorial(Integer revisionId, List<Prueba> pruebasRevision) {
		StringBuilder sbInspeccionSensorial = new StringBuilder();
		Optional<Prueba> optionalVisual = encontrarPruebaPorTipo(pruebasRevision,ConstantesTiposPrueba.TIPO_PRUEBA_INSPECCION_SENSORIAL);
		if(optionalVisual.isPresent()){
			
			Prueba pruebaVisual = optionalVisual.get();
			Usuario usuario = pruebaVisual.getUsuario();
			List<Integer> codigosDefectos = defectoService.obtenerTodosLosDefectosRevision(revisionId);
			codificadorDefectos.codificarDatosDefectos(sbInspeccionSensorial, codigosDefectos, usuario);
		}else {
			logger.log(Level.SEVERE, "Error prueba inspeccion visual no presente para revision " + revisionId);
			throw new RuntimeException("Error prueba inspeccion visual no presente para revision " + revisionId);
		}
		return sbInspeccionSensorial;
	}
	
	private StringBuilder generarSeccionInspeccionSensorialReinspeccion(Integer revisionId, List<Prueba> pruebasInspeccion) {
		StringBuilder sbInspeccionSensorial = new StringBuilder();
		Optional<Prueba> optionalVisual = encontrarPruebaPorTipo(pruebasInspeccion,ConstantesTiposPrueba.TIPO_PRUEBA_INSPECCION_SENSORIAL);
		if(optionalVisual.isPresent()){
			
			Prueba pruebaVisual = optionalVisual.get();
			Usuario usuario = pruebaVisual.getUsuario();
			List<Integer> listaIdPruebas = pruebasInspeccion.stream().map(prueba->prueba.getPruebaId()).collect(Collectors.toList());			
			List<Integer> codigosDefectos = defectoService.obtenerTodosLosDefectos(listaIdPruebas);
			codificadorDefectos.codificarDatosDefectos(sbInspeccionSensorial, codigosDefectos, usuario);
		}else {
			logger.log(Level.SEVERE, "Error prueba inspeccion visual no presente para revision " + revisionId);
			throw new RuntimeException("Error prueba inspeccion visual no presente para revision " + revisionId);
		}
		return sbInspeccionSensorial;
	}

	private StringBuilder generarSeccionRuido(Integer revisionId, List<Prueba> pruebasRevision) {
		StringBuilder sbRuido = new StringBuilder();
		Optional<Prueba> optPruebaRuidos = encontrarPruebaPorTipo(pruebasRevision,ConstantesTiposPrueba.TIPO_PRUEBA_RUIDO);
		if(optPruebaRuidos.isPresent()){
			
			Prueba pruebaRuido = optPruebaRuidos.get();
			if(pruebaRuido.isAprobada() && pruebaRuido.isFinalizada()) {
				Usuario usuario = pruebaRuido.getUsuario();
				checkNotNull(usuario,"Usuario no registrado prueba de ruido revision %d",revisionId);
				List<Medida> medidas = pruebaService.obtenerMedidas(pruebaRuido);
				checkArgument(medidas != null && !medidas.isEmpty(),"No existen medidas prueba de ruido revision %d",revisionId);
				codificador.codificarResultadoSonometria(sbRuido, medidas, usuario);
			} else if (pruebaRuido.isAbortada()) {//si la prueba fue abortada se envia una cadena vacia
				codificador.codificarPruebaAbortada(sbRuido);
			}
			
		}else{
			logger.log(Level.SEVERE, "No hay prueba ruido revision " + revisionId);
			throw new RuntimeException("No hay prueba ruido revision " + revisionId);
		}
		return sbRuido;
	}

	private StringBuilder generarSeccionLuces(Integer revisionId, List<Prueba> pruebasRevision,Boolean isMoto) {
		//RESULTADOS LUCES
		StringBuilder sbLuces = new StringBuilder();
		Optional<Prueba> optPruebaLuces = encontrarPruebaPorTipo(pruebasRevision,ConstantesTiposPrueba.TIPO_PRUEBA_LUCES);
		if(optPruebaLuces.isPresent()){
			
			Prueba pruebaLuces = optPruebaLuces.get();
			List<Medida> medidas = pruebaService.obtenerMedidas(pruebaLuces);
			checkArgument(medidas != null && !medidas.isEmpty(),"No hay medidas para prueba de luces revision %d",revisionId);
			Usuario usuarioLuces = pruebaLuces.getUsuario();
			checkNotNull(usuarioLuces,"No hay usuario registrado prueba luces revision %d",revisionId);			
			codificador.codificarResultadoLuces(sbLuces, medidas, usuarioLuces,isMoto);
			
			
		}else{
			logger.log(Level.SEVERE, "No hay prueba de luces revision " + revisionId);
			throw new RuntimeException("No hay prueba de luces revision " + revisionId);
		}
		return sbLuces;
	}

	private StringBuilder generarSeccionGases(Vehiculo vehiculo, List<Prueba> pruebasRevision) {
		Integer tipoCombustibleId = vehiculo.getTipoCombustible().getCombustibleId();
		StringBuilder sbGases = new StringBuilder();
		Optional<Prueba> optPruebaGases = pruebasRevision.stream().filter(
				prueba -> prueba.getTipoPrueba().getTipoPruebaId().equals(ConstantesTiposPrueba.TIPO_PRUEBA_GASES))
				.findFirst();
		if (optPruebaGases.isPresent()) {
			
			if(tipoCombustibleId.equals(ConstantesTiposCombustible.ELECTRICO)){
				codificador.codificarGasesVacia(sbGases);
				return sbGases;
			}
			
			
			Prueba pruebaGases = optPruebaGases.get();
			List<Medida> medidas = pruebaService.obtenerMedidas(pruebaGases);			
			List<Defecto> defectos = pruebaService.obtenerDefectos(pruebaGases);
			Usuario usuarioGases = pruebaGases.getUsuario();
			checkNotNull("No existe usuario registrado para prueba de gases");

			boolean tipoCombustibleEsDiesel = tipoCombustibleId.equals(ConstantesTiposCombustible.DIESEL)
					|| tipoCombustibleId.equals(ConstantesTiposCombustible.BIODIESEL)
					|| tipoCombustibleId.equals(ConstantesTiposCombustible.DIESEL_ELECTRICO);
			
			boolean tipoCombustibleVehiculoEsOtto = tipoCombustibleId.equals(ConstantesTiposCombustible.GASOLINA )
					|| tipoCombustibleId.equals(ConstantesTiposCombustible.GAS_NATURAL)
					|| tipoCombustibleId.equals(ConstantesTiposCombustible.GAS_GASOLINA)
					|| tipoCombustibleId.equals(ConstantesTiposCombustible.GASOLINA_ELECTRICO);
			
			if (tipoCombustibleEsDiesel) {
				codificador.codificarInformacionGasesDiesel(sbGases, medidas, defectos, usuarioGases);
			} else {					
				if (tipoCombustibleVehiculoEsOtto){
					
					
					Integer tipoVehiculo = vehiculo.getTipoVehiculo().getTipoVehiculoId();
					Boolean vehiculoEsMoto = tipoVehiculo.equals(ConstantesTiposVehiculo.MOTO) || tipoVehiculo.equals(ConstantesTiposVehiculo.MOTO_CARRO);
					Boolean vehiculoEsRemolque = tipoVehiculo.equals(ConstantesTiposVehiculo.REMOLQUE); 
					if(vehiculoEsMoto){
						Boolean isDosTiempos = vehiculo.getTiemposMotor().equals(2);
						codificador.codificarInformacionGasesMotocicleta(sbGases, medidas, defectos, usuarioGases,isDosTiempos);														
					}else if(!vehiculoEsMoto && !vehiculoEsRemolque){							
						codificador.codificadorInformacionGasesOttoVehiculo(sbGases, medidas, defectos, usuarioGases, pruebaGases.isDilucion());
					}else{							
						logger.log(Level.INFO, "El vehiculo es remolque no se genera info");
					}
				}
			}

		} else {
				throw new RuntimeException("No hay prueba de gases para vehiculo " + vehiculo.getPlaca());
		}
		return sbGases;
	}

	private StringBuilder generarSeccionFoto(Integer revisionId, Vehiculo vehiculo, List<Prueba> pruebasRevision) {
		StringBuilder sbFoto = new StringBuilder();
		Optional<Prueba> optPruebaFoto = encontrarPruebaPorTipo(pruebasRevision, ConstantesTiposPrueba.TIPO_PRUEBA_FOTO);
		if (optPruebaFoto.isPresent()) {
			Prueba pruebaFoto = optPruebaFoto.get();
			Foto foto = fotoDAO.consultarFotoPorPrueba(pruebaFoto);			
			codificador.codificarFoto(sbFoto, foto, vehiculo.getPlaca(),pruebaFoto.isAbortada());
		} else {
			throw new RuntimeException("Prueba foto no encontrada revision " + revisionId);
		}
		return sbFoto;
	}

	private StringBuilder generarSeccionVehiculo(Revision revision,Vehiculo vehiculo,Boolean aprobada) {
		StringBuilder sbVehiculo = new StringBuilder();		
		Usuario usuarioCreaRevision = revision.getUsuario();// se puede
															// hacer
															// consulta
															// aparte
		checkNotNull(vehiculo,"Vehiculo no presente en revision %d",revision.getRevisionId());
		codificador.generarInformacionVehiculo(sbVehiculo, vehiculo, revision, usuarioCreaRevision,aprobada);
		return sbVehiculo;
	}
	
	private StringBuilder generarSeccionVehiculoInspeccion(Revision revision,Inspeccion inspeccion,Vehiculo vehiculo,Boolean aprobada) {
		StringBuilder sbVehiculo = new StringBuilder();		
		Usuario usuarioCreaRevision = revision.getUsuario();// se puede
															// hacer
															// consulta
															// aparte
		checkNotNull(vehiculo,"Vehiculo no presente en revision %d",revision.getRevisionId());
		codificador.generarInformacionVehiculoInspeccion(sbVehiculo, vehiculo, usuarioCreaRevision,aprobada,inspeccion);
		return sbVehiculo;
	}

	private StringBuilder generarSeccionPropietario(Revision revision) {
		StringBuilder sbPropietario = new StringBuilder();
		Propietario propietario = revision.getPropietario();
		checkNotNull(propietario, "Error propietario no existente para revision %d", revision.getRevisionId());
		codificador.generarInformacionPropietario(sbPropietario, propietario);
		return sbPropietario;
	}

	private StringBuilder generarSeccionIdProveedor() {
		StringBuilder sbIdProveedor = new StringBuilder();
		CodificadorSeccionProveedor cod = new CodificadorSeccionProveedor();
		cod.codificarSeccionProveedor(sbIdProveedor);
		return sbIdProveedor;
	}
	
	

	
	
	
}
