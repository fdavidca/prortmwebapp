package com.proambiente.webapp.service;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.proambiente.modelo.PruebaFuga;

public class PruebaFugasService {
	
	
	
    @PersistenceContext 
    private EntityManager em;
	
	public List<PruebaFuga> consultarPruebaFugaPorFecha(Date fechaInicial,Date fechaFinal){
		String consulta = "Select pf From PruebaFuga pf where fechaRealizacion between :fechaInicial and :fechaFinal";
		TypedQuery<PruebaFuga> query = em.createQuery(consulta,PruebaFuga.class);
		query.setParameter("fechaInicial",fechaInicial);
		query.setParameter("fechaFinal", fechaFinal);
		return query.getResultList();		
	}	
	

}
