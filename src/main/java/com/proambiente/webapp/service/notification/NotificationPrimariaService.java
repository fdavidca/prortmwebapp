package com.proambiente.webapp.service.notification;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.inject.Inject;

import org.omnifaces.cdi.Push;
import org.omnifaces.cdi.PushContext;

import com.proambiente.modelo.Alerta;
import com.proambiente.modelo.NivelAlerta;
import com.proambiente.webapp.service.AlertasDeHoyService;

public class NotificationPrimariaService implements Serializable {
	
	private static final Logger logger = Logger.getLogger(NotificationPrimariaService.class.getName());
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 176L;

	@Inject
	AlertasDeHoyService alertaService;
	
	@Inject @Push(channel="notify")
	private PushContext chanel;




	public void sendNotification(String mensaje,NivelAlerta nivelAlerta,Boolean generarAlerta	){
		try{
			agregarAlerta(mensaje,nivelAlerta,generarAlerta);
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error guardando alerta", exc);
		}
		try{
			
			Map<String,String> parametros = new HashMap<>();
			parametros.put("mensaje",mensaje);
			parametros.put("nivel",nivelAlerta.name());
			chanel.send(parametros);
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error enviando alerta", exc);
		}
		
	}
	
	private void agregarAlerta(String mensaje, NivelAlerta nivelAlerta,Boolean generarAlerta) {
		Alerta alerta = null;
		switch(nivelAlerta){
		case DEBUG:
			if(generarAlerta){
				alerta = construirAlerta(mensaje, nivelAlerta);
			}
			break;
		case ERROR:
			alerta = construirAlerta(mensaje, nivelAlerta);			
			break;
		case ERROR_GRAVE:
			alerta = construirAlerta(mensaje, nivelAlerta);			
			break;
		case INFORMACION:
			if(generarAlerta){
				alerta = construirAlerta(mensaje,nivelAlerta);
			}
			break;
		default:
			break;		
		}
		if(alerta != null){
			alertaService.create(alerta);
		}
	}

	public void setSeverity(FacesMessage facesMessage,NivelAlerta nivelAlerta){
		
		switch(nivelAlerta){
		case DEBUG:
			facesMessage.setSeverity(FacesMessage.SEVERITY_WARN);
			break;
		case ERROR:
			facesMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
			break;
		case ERROR_GRAVE:
			facesMessage.setSeverity(FacesMessage.SEVERITY_FATAL);
			break;
		case INFORMACION:
			facesMessage.setSeverity(FacesMessage.SEVERITY_INFO);
			break;
		default:
			break;
			
		}
	}
	
	private Alerta construirAlerta(String mensaje,NivelAlerta nivel) {
		Alerta alerta = new Alerta();
		alerta.setNivel(nivel);
		alerta.setDescripcion(mensaje);
		alerta.setReconocida(false);
		return alerta;
	}
	
}
