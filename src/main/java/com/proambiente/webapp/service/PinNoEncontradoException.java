package com.proambiente.webapp.service;

public class PinNoEncontradoException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	
	public PinNoEncontradoException(String message) {
		super();
		this.message = message;
	}

	private String message = "Transacción no exitosa mientras se intenta consultar pin para placa ";
	
	@Override
	public String getMessage() {
		return message;
	}
	
	
	
	
}
