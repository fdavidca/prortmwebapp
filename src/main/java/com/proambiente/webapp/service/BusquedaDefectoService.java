package com.proambiente.webapp.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.proambiente.modelo.Defecto;

/**
 * busqueda de defecto por tipo de vehiculo y por texto en la descripcion
 * @author USUARIO
 *
 */
@Stateless
public class BusquedaDefectoService {
	
	@PersistenceContext
	EntityManager em;
	
	
	
	public List<Defecto> buscarDefecto(String texto,Integer idTipoVehiculo){
		
		Query query = em.createQuery("Select d "
				+ "FROM TipoVehiculo t "
				+ "JOIN t.defectosInspeccionVisual d "
				+ "WHERE t.tipoVehiculoId =:tipoVehiculoId "
				+ "AND lower(d.nombre) LIKE :descripcionParametro "
				+ "ORDER BY d.categoriaDefecto,d.subCategoriaDefecto"
					);
		query.setParameter("tipoVehiculoId", idTipoVehiculo);
		
		StringBuilder parametroFormat = new StringBuilder();
		parametroFormat.append("%").append(texto.toLowerCase()).append("%");
		query.setParameter("descripcionParametro",parametroFormat.toString());
		
		List<Defecto> defectos = query.<Defecto>getResultList();
		return defectos;
		
		
		
	}

}
