package com.proambiente.webapp.service.formatomedidas;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.ModoReporte;

import com.proambiente.webapp.dao.InformacionCdaFacade;
import com.proambiente.webapp.service.util.FormateadorMedidasInterface;
import com.proambiente.webapp.service.util.FormateadorMedidasMetrologia;
import com.proambiente.webapp.service.util.FormateadorMedidasResolucion;
import com.proambiente.webapp.service.util.FormateadorMedidasResolucionRedondeado;

import java.io.Serializable;

@Named
public class FormateadorMedidasProducer implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 875445L;
	@Inject
	InformacionCdaFacade informacionCDADAO;	
	
	@Produces
	public FormateadorMedidasInterface formateadorMedidas() {
		
		InformacionCda informacionCda = informacionCDADAO.find(1);
		FormateadorMedidasInterface formateador = null;
		
		ModoReporte modoReporte = informacionCda.getModoReporte();
		if( modoReporte.equals(ModoReporte.MODO_RESOLUCION_3625) ) {
			formateador = new FormateadorMedidasResolucion();
		} else if( modoReporte.equals(ModoReporte.MODO_RESOLUCION_3625_REDONDEADO) ){
			formateador = new FormateadorMedidasResolucionRedondeado();
		}else {
			formateador = new FormateadorMedidasMetrologia();
		}
		return formateador;
	}

}
