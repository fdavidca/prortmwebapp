package com.proambiente.webapp.service;

import java.util.List;


import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Revision;

@Stateless
public class PersistirPruebaService {
	
	@PersistenceContext
	EntityManager em;
	
	
	public void persistirPruebas(List<Prueba> listaPruebas, Revision revision) {
		try{
		Revision r = em.getReference(Revision.class, revision.getRevisionId());
		for (Prueba p : listaPruebas) {

			if (p.getPruebaId() != null) {
				p = em.getReference(Prueba.class, p.getPruebaId());
				p.setRevision(r);
				em.merge(p);
			} else {
				p.setRevision(r);
				em.persist(p);
			}
		}
		}catch(Exception exc){
			exc.printStackTrace();
			throw exc;
		}
	}


}
