package com.proambiente.webapp.service;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * Contar cuantas revisiones para la misma placa existen en el mismo día
 *  
 * @author USUARIO
 *
 */
@Stateless
public class RevisionMismoDiaService {
	
	@PersistenceContext
	EntityManager em;
	
	/**
	 * Cuenta las revisiones para la misma placa desde el inicio del dia
	 * @param placa
	 * @return
	 */
	public long revisionesMismoDiaPorPlaca(String placa) {
		
		String queryStr = "SELECT COUNT(r.revisionId) FROM Revision r WHERE r.vehiculo.placa =:placa AND r.fechaCreacionRevision >:fecha";
		String anulada = " AND anulada = false";//permitir un borrado suave
		
		StringBuilder sb = new StringBuilder();
		sb.append(queryStr);
		sb.append(anulada);
		
		Query query = em.createQuery(sb.toString());
		query.setParameter("placa", placa);
		
		//calcular la fecha al inicio del dia
		LocalDate ldHoy = LocalDate.now();
		LocalDateTime ldtHoy = ldHoy.atStartOfDay();
		Instant instant = ldtHoy.atZone(ZoneId.systemDefault()).toInstant();
		Date date = Date.from(instant);
		Timestamp ts = new Timestamp(date.getTime());
		
		query.setParameter("fecha",ts);
		return (long) query.getSingleResult();
	}
	
}
