package com.proambiente.webapp.service;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.proambiente.modelo.Inspeccion;
import com.proambiente.modelo.Inspeccion_;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Servicio;
import com.proambiente.modelo.TipoVehiculo;
import com.proambiente.modelo.Usuario;
import com.proambiente.modelo.dto.RevisionDTO;
import com.proambiente.modelo.dto.builder.RevisionDTOBuilder;
import com.proambiente.webapp.dao.InspeccionFacade;


@Stateless
public class InspeccionService {

	@Inject
	InspeccionFacade inspeccionDAO;

	@PersistenceContext
	EntityManager em;
	
	private static final Logger logger = Logger.getLogger(InspeccionService.class.getName());

	public List<Inspeccion> obtenerInspeccionesPorRevisionId(int revisionId) {
		TypedQuery<Inspeccion> query = em.createQuery(
				"SELECT i FROM Inspeccion i WHERE i.revision.revisionId =:revisionId ORDER BY i.inspeccionId",
				Inspeccion.class);
		query.setParameter("revisionId", revisionId);
		try {
			List<Inspeccion> inspecciones = query.getResultList();
			return inspecciones;
		} catch (NoResultException nrexc) {
			return new ArrayList<>();
		}

	}

	public void crearInspeccion(Inspeccion inspeccion) {
		inspeccionDAO.create(inspeccion);
	}

	public List<Inspeccion> inspeccionesPorPlaca(String placa) {
		if (placa == null || placa.isEmpty()) {
			throw new IllegalArgumentException("Placa null o vacia");
		}
		
		String consulta = "SELECT i.inspeccionId FROM Inspeccion i WHERE i.revision.vehiculo.placa =:placa";
		String anulada = " AND i.revision.anulada = false";
		
		StringBuilder sb = new StringBuilder();
		sb.append(consulta).append(anulada);

		TypedQuery<Integer> query = em.createQuery(
				sb.toString(), Integer.class);
		query.setParameter("placa", placa);
		try {
			List<Integer> inspeccionesId = query.getResultList();
			List<Inspeccion> inspecciones = new ArrayList<>();
			for (Integer inspeccionId : inspeccionesId) {
				Inspeccion i = this.cargarInspeccionParcialmente(inspeccionId);
				inspecciones.add(i);
			}
			return inspecciones;
		} catch (NoResultException exc) {
			exc.printStackTrace();
			return null;
		}

	}

	public Inspeccion cargarInspeccionParcialmente(Integer inspeccionId) {

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Tuple> tupleQuery = cb.createTupleQuery();

		Root<Inspeccion> from = tupleQuery.from(Inspeccion.class);
		tupleQuery.multiselect(

				from.get(Inspeccion_.inspeccionId).alias("inspeccionId"),
				from.get(Inspeccion_.fechaInspeccionAnterior).alias("fechaInspeccionAnterior"),
				from.get(Inspeccion_.numeroInspeccion).alias("numeroInspeccion"),
				from.get(Inspeccion_.aprobada).alias("aprobada"), from.get(Inspeccion_.comentario).alias("comentario")

		);
		tupleQuery.where(cb.equal(from.get(Inspeccion_.inspeccionId), inspeccionId));
		Tuple tupla = em.createQuery(tupleQuery).getSingleResult();
		Inspeccion inspeccion = new Inspeccion();
		inspeccion.setInspeccionId((Integer) tupla.get("inspeccionId"));
		inspeccion.setAprobada((Boolean) tupla.get("aprobada"));
		inspeccion.setFechaInspeccionAnterior((Timestamp) tupla.get("fechaInspeccionAnterior"));
		inspeccion.setNumeroInspeccion((Integer) tupla.get("numeroInspeccion"));
		inspeccion.setComentario((String) tupla.get("comentario"));
		return inspeccion;

	}

	public List<Revision> revisionInspeccionPorFechaSiguiente(Date fechaInicial, Date fechaFinal,boolean segundoEnvio) {

		TypedQuery<Integer> queryIds = em.createQuery("SELECT DISTINCT(i.revision.revisionId) FROM Inspeccion i "
				+ "WHERE i.fechaInspeccionSiguiente " + "between :fechaInicial AND :fechaFinal AND i.aprobada = false "
						+ "AND i.revision.segundoEnvioRealizado =:segundoEnvioRealizado AND i.revision.anulada = false",
				Integer.class);
		queryIds.setParameter("fechaInicial", fechaInicial);
		queryIds.setParameter("fechaFinal", fechaFinal);
		queryIds.setParameter("segundoEnvioRealizado",segundoEnvio);
		List<Integer> idRevisiones = new ArrayList<>(); 
		try {
			idRevisiones = queryIds.getResultList();
		} catch (Exception e) {
			logger.log(Level.SEVERE, "error consultando revisiones por inspeccion", e);
		}
		if(idRevisiones.isEmpty()) {
			return new ArrayList<>();
		}
		String consulta = "SELECT NEW com.proambiente.modelo.dto.RevisionDTO(r.revisionId,r.vehiculo.vehiculoId,r.vehiculo.placa,"
				+ "r.numeroInspecciones,r.fechaCreacionRevision,r.aprobada,r.consecutivoRunt,r.primerEnvioRealizado,r.preventiva,r.segundoEnvioRealizado) "
				+ "FROM Revision r  WHERE revisionId IN :idRevisiones";
		Query query = em.createQuery(consulta);
		query.setParameter("idRevisiones",idRevisiones);
		List<RevisionDTO> revisionesDTO = new ArrayList<>();
		List<Revision> revisiones = new ArrayList<>();
		try {
			revisionesDTO = query.getResultList();
			revisiones = revisionesDTO.stream().map(rdto -> RevisionDTOBuilder.revisionDesdeRevisionDTO(rdto))
					.collect(Collectors.toList());
		} catch (NoResultException nre) {
			logger.info("Ninguna revision encontrada");
		}
		return revisiones;
		
	}

	public Inspeccion cargarInspeccion(Integer inspeccionId) {
		Inspeccion inspeccion = inspeccionDAO.find(inspeccionId);
		inspeccion.getPruebas().size();
		return inspeccion;
	}

	public Prueba pruebaFotoInspeccion(Integer inspeccionId) {
		TypedQuery<Prueba> query = em.createQuery("SELECT p " + "FROM Inspeccion i " + "JOIN i.pruebas p "
				+ "WHERE p.tipoPrueba = 3 " + "AND i.inspeccionId =:inspeccionId", Prueba.class);
		query.setParameter("inspeccionId", inspeccionId);
		Prueba p = query.getSingleResult();
		return p;
	}

	public List<Prueba> pruebasOttoDeInspeccionPorFecha(Date fechaInicial, Date fechaFinal) {
		String consulta = "SELECT DISTINCT(p) FROM Inspeccion i JOIN i.pruebas p " + "WHERE p.tipoPrueba = 8 "
				+ "AND p.finalizada = true " + "AND p.revision.vehiculo.tipoVehiculo IN (1,2,3)"
				+ "AND p.revision.vehiculo.tipoCombustible IN (1,2,4,9,10) "
				+ "AND i.fechaInspeccionAnterior BETWEEN :fechaInicial AND :fechaFinal";
		TypedQuery<Prueba> query = em.createQuery(consulta, Prueba.class);
		query.setParameter("fechaInicial", fechaInicial);
		query.setParameter("fechaFinal", fechaFinal);
		List<Prueba> pruebas = null;// new ArrayList<Prueba>();
		try {
			pruebas = query.getResultList();
		} catch (NoResultException nre) {
			pruebas = new ArrayList<Prueba>();
		}
		return pruebas;
	}
	
	public List<Prueba> pruebasOttoDeInspeccionPorFechaPaginado(Date fechaInicial, Date fechaFinal,int inicio,
			int tamanioPagina) {
		String consulta = "SELECT DISTINCT(p) FROM Inspeccion i JOIN i.pruebas p " + "WHERE p.tipoPrueba = 8 "
				+ "AND p.finalizada = true " + "AND p.revision.vehiculo.tipoVehiculo IN (1,2,3)"
				+ "AND p.revision.vehiculo.tipoCombustible IN (1,2,4,9,10) "
				+ "AND i.fechaInspeccionAnterior BETWEEN :fechaInicial AND :fechaFinal";
		TypedQuery<Prueba> query = em.createQuery(consulta, Prueba.class);
		query.setParameter("fechaInicial", fechaInicial);
		query.setParameter("fechaFinal", fechaFinal);
		query.setFirstResult(inicio);
		query.setMaxResults(tamanioPagina);
		List<Prueba> pruebas = null;// new ArrayList<Prueba>();
		try {
			pruebas = query.getResultList();
		} catch (NoResultException nre) {
			pruebas = new ArrayList<Prueba>();
		}
		return pruebas;
	}

	public List<Prueba> pruebasDieselPorInspeccionPorFecha(Date fechaInicial, Date fechaFinal) {
		String consulta = "SELECT DISTINCT(p) FROM Inspeccion i JOIN i.pruebas p " + "WHERE p.tipoPrueba = 8 "
				+ "and p.finalizada = true " + "AND p.revision.vehiculo.tipoVehiculo IN (1,2,3)"
				+ "AND p.revision.vehiculo.tipoCombustible IN (3,8,11) "
				+ "AND p.fechaInicio BETWEEN :fechaInicial AND :fechaFinal";
		TypedQuery<Prueba> query = em.createQuery(consulta, Prueba.class);
		query.setParameter("fechaInicial", fechaInicial);
		query.setParameter("fechaFinal", fechaFinal);
		List<Prueba> pruebas = null;// new ArrayList<Prueba>();
		try {
			pruebas = query.getResultList();
		} catch (NoResultException nre) {
			pruebas = new ArrayList<Prueba>();
		}
		return pruebas;
	}

	public List<Prueba> consultarPruebasOttoMotocicletasPaginado(Date fechaInicial, Date fechaFinal, int inicio,
			int tamanioPagina) {

		String consulta = "SELECT DISTINCT(p) FROM Inspeccion i JOIN i.pruebas p " + "WHERE p.tipoPrueba = 8 "
				+ "AND p.finalizada=true " + "AND p.revision.vehiculo.tipoVehiculo IN (4,5) "
				+ "AND p.revision.vehiculo.tipoCombustible IN (1,2,4,9) "
				+ "AND i.fechaInspeccionAnterior BETWEEN :fechaInicial AND :fechaFinal " + "ORDER BY p.pruebaId ";
		TypedQuery<Prueba> query = em.createQuery(consulta, Prueba.class);
		query.setParameter("fechaInicial", fechaInicial);
		query.setParameter("fechaFinal", fechaFinal);
		query.setFirstResult(inicio);
		query.setMaxResults(tamanioPagina);
		List<Prueba> pruebas = null;// new ArrayList<Prueba>();
		try {
			pruebas = query.getResultList();
		} catch (NoResultException nre) {
			pruebas = new ArrayList<Prueba>();
		}
		return pruebas;
	}

	public Long contarPruebasOttoMotocicletasInspeccion(Date fechaInicial, Date fechaFinal) {
		String consulta = "select count(*) from (\r\n" + 
				"select distinct(prueba2_.prueba_id) as col_0_0_ \r\n" + 
				"from bdprortm.inspeccion inspeccion0_ left join bdprortm.inspeccion_prueba pruebas1_ \r\n" + 
				"on inspeccion0_.inspeccion_id=pruebas1_.inspeccion_id \r\n" + 
				"left join bdprortm.prueba prueba2_ on pruebas1_.prueba_id=prueba2_.prueba_id\r\n" + 
				"cross join bdprortm.revision revision3_ \r\n" + 
				"cross join bdprortm.vehiculo vehiculo4_ \r\n" + 
				"where prueba2_.revision_id=revision3_.revision_id \r\n" + 
				"and revision3_.vehiculo_id=vehiculo4_.vehiculo_id \r\n" + 
				"and prueba2_.tipo_prueba_id=8 \r\n" + 
				"and prueba2_.finalizada=true \r\n" + 
				"and (vehiculo4_.tipo_vehiculo_id in (4 , 5)) \r\n" + 
				"and (vehiculo4_.combustible_id in (1 , 2 , 4 , 9))\r\n" + 
				"and (inspeccion0_.fecha_inspeccion_anterior between :fechaInicial and :fechaFinal)\r\n" + 
				"group by prueba2_.prueba_id\r\n" + 
				")as sb1 ";
		Query query = em.createNativeQuery(consulta);
		query.setParameter("fechaInicial", fechaInicial);
		query.setParameter("fechaFinal", fechaFinal);	
		BigInteger pruebas = null;//new ArrayList<Prueba>();
		try{
			pruebas =  (BigInteger)query.getSingleResult();
		}catch(NoResultException nre){
			pruebas = new BigInteger("0");
		}
		return pruebas.longValue();
	}
	
	public Long contarPruebasOttoInspeccion(Date fechaInicial, Date fechaFinal) {
		String consulta = "SELECT COUNT(p) FROM Inspeccion i JOIN i.pruebas p " + "WHERE p.tipoPrueba = 8 "
				+ "AND p.finalizada=true " + "AND p.revision.vehiculo.tipoVehiculo IN (1,2,3) "
				+ "AND p.revision.vehiculo.tipoCombustible IN (1,2,4,9,10) "
				+ "AND i.fechaInspeccionAnterior BETWEEN :fechaInicial AND :fechaFinal ";
		Query query = em.createQuery(consulta, Long.class);
		query.setParameter("fechaInicial", fechaInicial);
		query.setParameter("fechaFinal", fechaFinal);	
		Long pruebas = null;//new ArrayList<Prueba>();
		try{
			pruebas =  (Long)query.getSingleResult();
		}catch(NoResultException nre){
			pruebas = 0l;
		}
		return pruebas;
	}

	public List<Prueba> pruebasInspeccion(Integer inspeccionId) {

		String consulta = " SELECT  i FROM Inspeccion i JOIN FETCH i.pruebas pr JOIN FETCH pr.tipoPrueba WHERE i.inspeccionId=:inspeccionId ";
		Query query = em.createQuery(consulta);
		query.setParameter("inspeccionId", inspeccionId);
		Inspeccion i = (Inspeccion) query.getSingleResult();
		return i.getPruebas();
	}

	public Prueba pruebaSonometroInspeccion(Integer inspeccionId) {
		String consulta = " SELECT pr  FROM Inspeccion i JOIN FETCH i.pruebas pr JOIN FETCH pr.tipoPrueba WHERE i.inspeccionId=:inspeccionId AND pr.tipoPrueba.tipoPruebaId = 7";
		Query query = em.createQuery(consulta);
		query.setParameter("inspeccionId", inspeccionId);
		Prueba pr = (Prueba) query.getSingleResult();
		return pr;
	}

	public TipoVehiculo obtenerTipoVehiculoPorInspeccionId(Integer inspeccionId) {
		String consulta = "SELECT i.revision.vehiculo.tipoVehiculo FROM Inspeccion i where i.inspeccionId =:inspeccionId";
		TypedQuery<TipoVehiculo> query = em.createQuery(consulta, TipoVehiculo.class);
		query.setParameter("inspeccionId", inspeccionId);
		return query.getSingleResult();
	}
	
	public Servicio obtenerServicioPorInspeccionId(Integer inspeccionId) {
		
		String consulta = "SELECT i.revision.vehiculo.servicio FROM Inspeccion i where i.inspeccionId =:inspeccionId";
		TypedQuery<Servicio> query = em.createQuery(consulta, Servicio.class);
		query.setParameter("inspeccionId", inspeccionId);
		return query.getSingleResult();
		
	}

	public Integer revisionIdDesdeInspeccionId(Integer inspeccionId) {
		String consulta = "SELECT i.revision.revisionId FROM Inspeccion i where i.inspeccionId =:inspeccionId";
		TypedQuery<Integer> query = em.createQuery(consulta, Integer.class);
		query.setParameter("inspeccionId", inspeccionId);
		return query.getSingleResult();

	}
	
	public Usuario usuarioResponsablePorInspeccionId(Integer inspeccionId) {
		String consulta = "SELECT i.usuarioResponsable FROM Inspeccion i where i.inspeccionId =:inspeccionId";
		TypedQuery<Usuario> query = em.createQuery(consulta, Usuario.class);
		query.setParameter("inspeccionId", inspeccionId);
		return query.getSingleResult();
	}

}
