package com.proambiente.webapp.service;

import java.io.Serializable;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.OperadorSicov;
import com.proambiente.webapp.dao.InformacionCdaFacade;

@Stateless
public class OperadorSicovService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@Inject
	InformacionCdaFacade informacionCdaDAO;
	
	public OperadorSicov consultarOperadorSicov(){
		InformacionCda infoCda = informacionCdaDAO.find(1);
		OperadorSicov operador  = infoCda.getOperadorSicov();
		return operador;
	}
	
	
}
