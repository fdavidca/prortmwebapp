package com.proambiente.webapp.service;

import java.io.BufferedOutputStream;
import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.proambiente.modelo.RegistroFur;
import com.proambiente.webapp.dao.RegistroFurFacade;

@WebServlet("/ServletFurRegistrado")
public class ServletFurRegistrado extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Inject
	RegistroFurFacade registroFurDAO;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String strInspeccionId = request.getParameter("revision_id");

		if (strInspeccionId != null) {
			Long inspeccionId = Long.parseLong(strInspeccionId);
			RegistroFur rgFur = registroFurDAO.find(inspeccionId);
			// fetch pdf
			byte[] pdf = new byte[] {}; // Load PDF byte[] into here
			if (pdf != null) {
				String contentType = "application/pdf";
				byte[] ba1 = rgFur.getPdf();
				String fileName = "pdffile.pdf";
				// set pdf content
				response.setContentType("application/pdf");
				// if you want to download instead of opening inline
				// response.addHeader("Content-Disposition", "attachment; filename=" +
				// fileName);
				// write the content to the output stream
				BufferedOutputStream fos1 = new BufferedOutputStream(response.getOutputStream());
				fos1.write(ba1);
				fos1.flush();
				fos1.close();

			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
