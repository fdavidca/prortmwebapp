package com.proambiente.webapp.service;

import java.sql.Timestamp;
import java.util.Date;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.TipoPrueba;

@Stateless
public class CrearPruebaService {
	
	@PersistenceContext
	EntityManager em;
	
	@RolesAllowed("ADMINISTRADOR")
	public Prueba crearPrueba(Revision revision,Integer tipoPruebaId){
		Prueba prueba = new Prueba();
		prueba.setRevision(revision);
		TipoPrueba tipoPrueba = em.getReference(TipoPrueba.class,tipoPruebaId);
		prueba.setTipoPrueba(tipoPrueba);
		Date date = new Date();
		Timestamp timestamp = new Timestamp(date.getTime());
		prueba.setFechaInicio(timestamp);
		return prueba;		
	}

}
