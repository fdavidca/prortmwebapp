package com.proambiente.webapp.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.proambiente.modelo.Analizador;
import com.proambiente.modelo.FiltroDensidadNeutra;
import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.Usuario;
import com.proambiente.modelo.VerificacionLinealidad;
import com.proambiente.webapp.dao.AnalizadorFacade;
import com.proambiente.webapp.dao.InformacionCdaFacade;
import com.proambiente.webapp.dao.UsuarioFacade;
import com.proambiente.webapp.dao.VerificacionLinealidadFacade;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;

/**
 * Servlet implementation class ServletReporte
 */
@WebServlet("/ServletVerificacionLinealidad")

public class ServletVerificacionLinealidad extends HttpServlet {

	private static final Logger logger = Logger.getLogger(ServletVerificacionLinealidad.class.getName());

	private static final long serialVersionUID = 1L;

	@Inject
	VerificacionLinealidadFacade verificacionLinealidadDAO;
	
	@Inject
	InformacionCdaFacade informacionCdaDAO;
	
	@Inject
	AnalizadorFacade analizadorDAO;
	

	@Inject
	UsuarioFacade usuarioDAO;

	public ServletVerificacionLinealidad() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			String idVerificacion = request.getParameter("id_verificacion_linealidad");
			if (idVerificacion == null || idVerificacion.isEmpty())
				throw new RuntimeException("error falta parametro de verificacion de linealidad");
			Integer verificacionId = Integer.valueOf(idVerificacion);
			VerificacionLinealidad verificacionLinealidad = verificacionLinealidadDAO.find(verificacionId);
			response.setContentType("application/pdf");
			JasperPrint reporte = cargarReporteLinealidad(verificacionLinealidad);
			JRPdfExporter exporter = new JRPdfExporter();
            exporter.setExporterInput(new SimpleExporterInput(reporte));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));
			exporter.exportReport();
			
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error cargando el reporte", e);
			throw new RuntimeException(e);
		} finally {
			response.getOutputStream().flush();
			response.getOutputStream().close();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}
	
	
	
	public JasperPrint cargarReporteLinealidad(VerificacionLinealidad vl) throws Exception{
		InputStream is = null;
		JasperPrint reporte = null;
		Map<String,Object> parametros = new HashMap<>();
		try{
			is = this
					.getClass()
					.getClassLoader()
					.getResourceAsStream(
							"com/proambiente/webapp/reporte/reporte_autoregulacion_linealidad.jasper");
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
			SimpleDateFormat sdfs = new SimpleDateFormat("yyyy-MM-dd");
			DecimalFormat df = new DecimalFormat("0.00");
			DecimalFormat dfSinDecimales = new DecimalFormat("#");
			
			InformacionCda infoCda = informacionCdaDAO.find(1);
			
			if(infoCda.getLogo() != null){
		       	 ByteArrayInputStream bis = new ByteArrayInputStream(infoCda.getLogo());
		            parametros.put("logo", bis);
		        }
			
			parametros.put("numero_prueba", String.valueOf( vl.getVerificacionLinealidadId()));
			parametros.put("fecha_linealidad", sdfs.format(vl.getFechaVerificacion()));
			parametros.put("numero_establecimiento", infoCda.getNumeroCda());
			parametros.put("nombre_organizacion", infoCda.getNombre() );
			parametros.put("NIT",infoCda.getNit());
			parametros.put("direccion", infoCda.getDireccion());
			parametros.put("telefono",infoCda.getTelefono1());
			parametros.put("telefono2", infoCda.getTelefono2());
			parametros.put("ciudad_establecimiento", infoCda.getCiudad());
			parametros.put("resolucion",infoCda.getNumeroResolucion());
			parametros.put( "fecha_resolucion", sdfs.format( infoCda.getFechaResolucion() ) );
			
			Analizador opacimetro = analizadorDAO.find(vl.getSerieOpacimetro());
			
			parametros.put("marca_equipo", opacimetro.getMarca());
			parametros.put("modelo_equipo", opacimetro.getModelo());
			parametros.put("serie_equipo", opacimetro.getSerial());
			
			String nombreSoftware = "PRO RTM";
			parametros.put("nombre_software", nombreSoftware);
			String versionSoftware = infoCda.getVersionSoftware();
			parametros.put("version_software", versionSoftware ); 
			
			Usuario usuario = usuarioDAO.find(vl.getUsuarioId());
			StringBuilder sb = new StringBuilder();
			sb.append(usuario.getNombres()).append("  ").append(usuario.getApellidos())
			.append("-").append(usuario.getCedula().toString());
			parametros.put("usuario_verificacion",sb.toString());
			
			parametros.put("fecha_verificacion", sdf.format(vl.getFechaVerificacion()));
			
			parametros.put("resultado_linealidad", vl.isAprobada() ? "APROBADA":"REPROBADA");
			
			String temperaturaAmbiente = dfSinDecimales.format( vl.getTemperaturaAmbiente() );
			
			parametros.put("temperatura_ambiente", temperaturaAmbiente + " C");
			
			String humedadRelativa = dfSinDecimales.format( vl.getHumedadRelativa() );
			
			parametros.put("humedad_relativa", humedadRelativa +  " %");
			
			FiltroDensidadNeutra filtroUno = vl.getFiltroUno();
			
			parametros.put("marca_filtro_uno",filtroUno.getMarca());
			parametros.put("modelo_filtro_uno",filtroUno.getModelo());
			parametros.put("serie_filtro_uno", filtroUno.getSerie());
			parametros.put("valor_filtro_uno",filtroUno.getValor().setScale(2,RoundingMode.HALF_UP).toString());
			parametros.put("numero_certificado_filtro_uno",filtroUno.getNumeroCertificado());
			parametros.put("fecha_calibracion_filtro_uno", sdfs.format( filtroUno.getFechaCalibracion() ) ); 
			parametros.put("empresa_filtro_uno",filtroUno.getCalibradoPor());
			
			FiltroDensidadNeutra filtroDos = vl.getFiltroDos();
			parametros.put("marca_filtro_dos",filtroDos.getMarca());
			parametros.put("modelo_filtro_dos",filtroDos.getModelo());
			parametros.put("serie_filtro_dos", filtroDos.getSerie());
			parametros.put("valor_filtro_dos",filtroDos.getValor().setScale(2,RoundingMode.HALF_UP).toString());
			parametros.put("numero_certificado_filtro_dos",filtroDos.getNumeroCertificado());
			parametros.put("fecha_calibracion_filtro_dos", sdfs.format( filtroDos.getFechaCalibracion() ) ); 
			parametros.put("empresa_filtro_dos",filtroDos.getCalibradoPor());
			
			parametros.put("refer_filtro_uno", df.format( vl.getValorFiltro1() ) );
			parametros.put("refer_filtro_dos", df.format( vl.getValorFiltro2() ) );
			parametros.put("refer_filtro_tres", df.format( vl.getValorFiltro3() ) );
			parametros.put("refer_filtro_cuatro",df.format(  vl.getValorFiltro4() ) );
			
			parametros.put("res_filtro_uno", df.format( vl.getResutadoFiltro1() ) );
			parametros.put("res_filtro_dos",df.format( vl.getResultadoFiltro2() ) );
			parametros.put("res_filtro_tres",df.format(  vl.getResultadoFiltro3() ) );
			parametros.put("res_filtro_cuatro", df.format( vl.getResutadoFiltro4() ) );
			
			Double errorFiltroUno = vl.getResutadoFiltro1() - vl.getResutadoFiltro1();
			errorFiltroUno = Math.abs(errorFiltroUno);
			
			parametros.put("error_filtro_uno", df.format(errorFiltroUno));
			
			Double errorFiltroDos = vl.getResultadoFiltro2() - vl.getValorFiltro2();
			errorFiltroDos = Math.abs(errorFiltroDos);
			
			parametros.put("error_filtro_dos", df.format(errorFiltroDos));
			
			Double errorFiltroTres = vl.getResultadoFiltro3() - vl.getValorFiltro3();
			errorFiltroTres = Math.abs(errorFiltroTres);
			
			parametros.put("error_filtro_tres",df.format(errorFiltroTres));
			
			Double errorFiltroCuatro = vl.getResutadoFiltro4()  - vl.getValorFiltro4();
			errorFiltroCuatro = Math.abs(errorFiltroCuatro);
			parametros.put("error_filtro_cuatro" , df.format(errorFiltroCuatro));
			
			reporte = JasperFillManager.fillReport(is, parametros, new JREmptyDataSource());
			
			
			return reporte;
			
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error generando reporte de prueba de linealidad", exc);
			throw exc;
		} finally{
			try{
				is.close();
			}catch(Exception exc){
				logger.log(Level.SEVERE, "error", exc);
			}
		}
	}
	
	
}
