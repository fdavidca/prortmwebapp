package com.proambiente.webapp.service;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;

/**
 * Servlet implementation class ServletReporteInspeccion
 */
@WebServlet("/ServletReporteInspeccion")
public class ServletReporteInspeccion extends HttpServlet {
	
	private static final Logger logger = Logger
			.getLogger(ServletReporteInspeccion.class.getName());
	private static final long serialVersionUID = 1L;
    
	@Inject
	ReporteService reporteService;
	

    public ServletReporteInspeccion() {
        super();
        
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			String strInspeccionId = request.getParameter("inspeccion_id");
			Integer inspeccionId = Integer.valueOf(strInspeccionId);
			
			JasperPrint jasperPrintReporte = reporteService.cargarReporteInspeccion(inspeccionId);			
            JRPdfExporter exporter = new JRPdfExporter();            
            exporter.setExporterInput( new SimpleExporterInput( jasperPrintReporte ));
            exporter.setExporterOutput( new SimpleOutputStreamExporterOutput( response.getOutputStream() ));
            exporter.exportReport();
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error cargando reporte reinspeccion", e);
			throw new RuntimeException(e);
		}finally{            
	        response.getOutputStream().flush();
	        response.getOutputStream().close();	        
		}
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
