package com.proambiente.webapp.service;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.proambiente.modelo.Prueba;

@Stateless
public class BorrarPruebaService {
	
	@PersistenceContext
	EntityManager em;
	
	@RolesAllowed("ADMINISTRADOR")
	public void quitarPrueba(Prueba pruebaSeleccionada) {
		try{
		Prueba pruebaMerged = em.merge(pruebaSeleccionada);
		em.remove(pruebaMerged);
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}
}
