package com.proambiente.webapp.service;

import java.util.Date;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.proambiente.modelo.PruebaFuga;

@Stateless
public class PruebaFugaAprobadaService {
	
	
	
    @PersistenceContext 
    private EntityManager em;
	
	public PruebaFuga consultarPruebaFugaPorFecha(Date fechaInicial,Date fechaFinal,String serial){
		String consulta = "Select MAX(pf) From PruebaFuga pf where fechaRealizacion between :fechaInicial and :fechaFinal and pf.aprobada=true and pf.serialEquipo =:serial";
		TypedQuery<PruebaFuga> query = em.createQuery(consulta,PruebaFuga.class);
		query.setParameter("fechaInicial",fechaInicial);
		query.setParameter("fechaFinal", fechaFinal);
		query.setParameter("serial",serial);
		return query.getSingleResult();		
	}	
	

}
