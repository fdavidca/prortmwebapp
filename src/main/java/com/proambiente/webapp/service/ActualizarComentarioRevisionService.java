package com.proambiente.webapp.service;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
public class ActualizarComentarioRevisionService {
	
	@PersistenceContext 
	EntityManager em;
	
	public void actualizarComentarioRevision(String comentario,Integer revisionId) {
		
		String queryStr = "UPDATE Revision r SET r.comentario =:comentario WHERE r.revisionId=:revisionId AND r.cerrada = false";
		Query query = em.createQuery(queryStr);
		query.setParameter("comentario",comentario);
		query.setParameter("revisionId",revisionId);		
		query.executeUpdate();	
		
	}

}
