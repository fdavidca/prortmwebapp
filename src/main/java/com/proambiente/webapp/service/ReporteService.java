package com.proambiente.webapp.service;

import static com.proambiente.webapp.util.ConstantesMedidasFrenos.*;
import static com.proambiente.webapp.util.ConstantesMedidasGases.*;
import static com.proambiente.webapp.util.ConstantesMedidasLuces.*;
import static com.proambiente.webapp.util.ConstantesMedidasAlineacion.*;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.inject.Named;
import javax.sql.DataSource;

import com.proambiente.modelo.DefectoAsociado;
import com.proambiente.modelo.EstadoDefecto;
import com.proambiente.modelo.Foto;
import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.Inspeccion;
import com.proambiente.modelo.Llanta;
import com.proambiente.modelo.LogoOnac;
import com.proambiente.modelo.Medida;
import com.proambiente.modelo.ModoMostrarNumPasajeros;
import com.proambiente.modelo.Permisible;
import com.proambiente.modelo.Propietario;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Revision_;
import com.proambiente.modelo.Servicio;
import com.proambiente.modelo.TipoCombustible;
import com.proambiente.modelo.TipoDocumentoIdentidad;
import com.proambiente.modelo.Usuario;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.webapp.dao.EquipoFacade;
import com.proambiente.webapp.dao.FotoFacade;
import com.proambiente.webapp.dao.InformacionCdaFacade;
import com.proambiente.webapp.dao.PermisibleFacade;
import com.proambiente.webapp.dao.UsuarioFacade;
import com.proambiente.webapp.service.formatomedidas.FormateadorMedidasProducer;
import com.proambiente.webapp.service.generadordto.GeneradorPresionLlantasDTO;
import com.proambiente.webapp.service.sicov.UtilEncontrarPruebas;
import com.proambiente.webapp.service.util.FormateadorMedidasInterface;
import com.proambiente.webapp.service.util.UtilTipoCombustible;
import com.proambiente.webapp.service.util.UtilTipoVehiculo;
import com.proambiente.webapp.service.util.UtilVencimientoRevision;
import com.proambiente.webapp.util.ConstantesMedidasKilometraje;
import com.proambiente.webapp.util.ConstantesNombreSoftware;
import com.proambiente.webapp.util.ConstantesTiposPrueba;
import com.proambiente.webapp.util.dto.EquipoPruebaDTO;
import com.proambiente.webapp.util.dto.ResultadoPresionLlantasDTO;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Named
public class ReporteService {

	private static final Logger logger = Logger.getLogger(ReporteService.class.getName());

	@Resource(lookup = "java:/jboss/datasources/PrortmDS")
	DataSource dataSource;

	@Inject
	FotoFacade fotoDAO;

	@Inject
	RevisionService revisionService;

	@Inject
	InformacionCdaFacade informacionCdaDAO;

	@Inject
	PruebasService pruebaService;

	@Inject
	PermisibleFacade permisibleService;

	@Inject
	InspeccionService inspeccionService;

	@Inject
	UtilInformacionProfundidadLabrado utilProfLabrado;

	@Inject
	DefectoService defectoService;

	@Inject
	EquipoFacade equipoFacade;

	@Inject
	FormateadorMedidasProducer formateadorMedidasProducer;

	@Inject
	ConsultarLogoOnacVigenteService consultaLogoOnac;

	private HashMap<String, Object> parametros;

	private Revision r;

	@Inject
	private UsuarioFacade usuarioDAO;

	private InformacionCda informacionCda;

	private boolean pruebaFueEvaluadaDensidad = false;

	public JasperPrint cargarReporte(String revisionId) {
		Integer revisionIdInt = Integer.valueOf(revisionId);
		this.informacionCda = this.informacionCdaDAO.find(1);
		Revision revDTO = this.revisionService.cargarRevisionDTO(revisionIdInt, Revision_.preventiva);
		if (revDTO.getPreventiva() && informacionCda.getFormatoEspecialPreventiva()) {
			return cargarReporteRevisionPreventiva(revisionIdInt);
		} else {
			return cargarReporteRevision(revisionIdInt);
		}

	}

	private JasperPrint cargarReporteRevisionPreventiva(Integer revisionId) {
		InputStream is = null;
		JasperPrint reporte = null;
		this.informacionCda = this.informacionCdaDAO.find(1);
		parametros = new HashMap<>();
		parametros.put("revision_id", revisionId);
		FormateadorMedidasInterface formateadorMedidas = formateadorMedidasProducer.formateadorMedidas();
		Connection cn = null;
		try {
			r = revisionService.cargarRevision(revisionId);
			parametros.put("consecutivo_runt", r.getConsecutivoRunt());
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			if (this.r.getNumeroInspecciones() == 2) {
				List<Inspeccion> inspecciones = inspeccionService.obtenerInspeccionesPorRevisionId(r.getRevisionId());
				if (inspecciones != null && !inspecciones.isEmpty()) {
					Inspeccion inspeccion = inspecciones.get(0);
					if (inspeccion != null && inspeccion.getFechaInspeccionSiguiente() != null) {
						String fechaInspeccion = sdf.format(inspeccion.getFechaInspeccionSiguiente());
						parametros.put("fecha_inspeccion", fechaInspeccion);
					}
				}
			} else {
				String fecha = sdf.format(this.r.getFechaCreacionRevision());
				this.parametros.put("fecha_inspeccion", fecha);
			}
			cn = dataSource.getConnection();
			is = this.getClass().getClassLoader()
					.getResourceAsStream("com/proambiente/webapp/reporte/formato_preventiva.jasper");
			ponerAsteriscos(revisionId.intValue());
			List<Prueba> pruebas = listaPruebas(revisionId);
			Boolean isDiesel = UtilTipoCombustible.isCombustibleDiesel(this.r.getVehiculo().getTipoCombustible());
			if (pruebas != null && !pruebas.isEmpty()) {
				Boolean isMoto = UtilTipoVehiculo.isVehiculoMoto(this.r.getVehiculo().getTipoVehiculo());
				Boolean isMotocarro = Boolean
						.valueOf(UtilTipoVehiculo.isVehiculoMotocarro(this.r.getVehiculo().getTipoVehiculo()));
				ponerMedidaPermisible(pruebas, revisionId, isMoto, isDiesel, formateadorMedidas, isMotocarro);
			}
			if (pruebaService.verificarDilucion(revisionId)) {
				parametros.put("dilusion", "X");
			} else {
				parametros.put("dilusion", "");
			}
			cargarInformacionCda(parametros, pruebas);
			StringBuilder comentariosSb = new StringBuilder();
			colocarInformacionDocumentoIdentidad(comentariosSb, r.getPropietario());
			colocarComentariosPruebas(pruebas, comentariosSb);
			cargarInformacionProfundidadLabrado(pruebas,
					UtilTipoVehiculo.isVehiculoMoto(r.getVehiculo().getTipoVehiculo()),
					UtilTipoVehiculo.isVehiculoPesado(r.getVehiculo().getTipoVehiculo()), parametros,
					formateadorMedidas);
			cargarInformacionPresionLlantas(pruebas, parametros);
			Integer numeroIntento = r.getNumeroInspecciones();
			parametros.put("numero_intento", numeroIntento.toString());
			cargarInformacionReferenciaLlanta(pruebas);
			cargarNombreServicio(r.getVehiculo());
			cargarTipoMotor(r.getVehiculo());
			cargarInformacionAplicativos();

			if (r.getVehiculo().getDiametroExosto() != null)
				parametros.put("diametro", String.valueOf(r.getVehiculo().getDiametroExosto()));

			if (revisionService.isRevisionAprobada(revisionId)) {
				parametros.put("revision_aprobada", "X");
				parametros.put("revision_reprobada", "");
				if (r.getVehiculo().getServicio().getServicioId().intValue() == 5) {// ensenianza
					parametros.put("aprobado_ense", "X");
					parametros.put("reprobado_ense", "");
				}
			} else {
				parametros.put("revision_reprobada", "X");
				parametros.put("revision_aprobada", "");
				if (numeroIntento.equals(Integer.valueOf(1))) {
					LocalDateTime ldTime = r.getFechaCreacionRevision().toLocalDateTime();
					UtilVencimientoRevision utilVencimientoRevision = new UtilVencimientoRevision();
					LocalDateTime ldTimeVencimientoPlazo = utilVencimientoRevision.calcularFechaVencimientoPlazo(
							Date.from(ldTime.atZone(ZoneId.systemDefault()).toInstant()));
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy MM dd hh:mm a");
					String text = ldTimeVencimientoPlazo.format(formatter);
					StringBuilder sb = new StringBuilder();
					sb.append(" El plazo para presentar el vehiculo por segunda vez es proximo a : ").append(text);
					sb.append(
							" Nota: El presente documento no lo exime de una multa de transito ni de la inmovilizacion Ley 1383 16 de Marzo de 2010");
					sb.append(" Señor usuario: Usted cuenta con un maximo de dos ingresos al centro de diagnostico ");
					sb.append(
							"si al segundo ingreso el vehiculo carro o moto no cuenta con la correccion de los defectos ");
					sb.append(
							"por los cuales fue rechazado, se generara un segundo cobro del 100 % de la revision al tercer ingreso ");
					comentariosSb.append(sb);
				}
				if (r.getVehiculo().getServicio().getServicioId().intValue() == 5) {
					parametros.put("reprobado_ense", "X");
					parametros.put("aprobado_ense", "");
				}
			}
			if (r.getPreventiva()) {
				this.parametros.put("fecha_vencimiento_repr",
						" Los resultados de las inspecciones preventivas NO tiene ninguna relacicon los resultados  de la RTMyEC oficial.");
				comentariosSb.append("/r/n").append(
						" Los resultados de las inspecciones preventivas NO tiene ninguna relacicon los resultados  de la RTMyEC oficial.");
			}
			parametros.put("comentarios", comentariosSb.toString());
			reporte = JasperFillManager.fillReport(is, parametros, cn);
			logger.info("Reporte cargado");
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error cargando reporte ", e);
			throw new RuntimeException(e);
		} finally {
			try {
				cn.close();
			} catch (Exception e2) {
				logger.info("Excepcion cerrando la conexion");
			}
			try {
				is.close();
			} catch (Exception exc) {
				logger.info("Excepcion cerrando el flujo de datos");
			}
		}
		return reporte;
	}

	private void colocarInformacionDocumentoIdentidad(StringBuilder comentariosSb, Propietario propietario) {
		if (propietario != null && propietario.getTipoDocumentoIdentidad() != null) {

			TipoDocumentoIdentidad tipoDocumento = propietario.getTipoDocumentoIdentidad();
			if (tipoDocumento.getTipoDocumentoIdentidadId() > 2) {

				comentariosSb.append("Tipo de Documento de identidad : ");
				comentariosSb.append(tipoDocumento.getNombreTipoDocumento());
				comentariosSb.append(" ");

			}

		}

	}

	private void cargarInformacionAplicativos() {

		InformacionCda infoCda = informacionCdaDAO.find(1);
		String listadoAplicativos = infoCda.getAplicativosUtilizados() != null ? infoCda.getAplicativosUtilizados()
				: "";

		StringBuilder sb = new StringBuilder();
		sb.append(ConstantesNombreSoftware.NOMBRE_SOFTWARE).append(" ");
		sb.append(infoCda.getVersionSoftware()).append(" ");
		sb.append(listadoAplicativos);
		parametros.put("listado_software", sb.toString());

	}

	public JasperPrint cargarReporteRevision(int revisionId) {
		InputStream is = null;
		JasperPrint reporte = null;
		ByteArrayInputStream bisFoto1 = null;
		ByteArrayInputStream bisFoto2 = null;
		this.informacionCda = this.informacionCdaDAO.find(1);
		ByteArrayInputStream bisLogoOnac = null;
		InputStream isLogoSuper = null;
		parametros = new HashMap<>();
		parametros.put("revision_id", revisionId);
		Connection cn = null;
		FormateadorMedidasInterface formateadorMedidas = formateadorMedidasProducer.formateadorMedidas();
		try {
			r = this.revisionService.cargarRevision(Integer.valueOf(revisionId));
			parametros.put("consecutivo_runt", r.getConsecutivoRunt());
			parametros.put("unidad_presion", "psi");
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			if (r.getNumeroInspecciones().intValue() == 2) {
				List<Inspeccion> inspecciones = inspeccionService.obtenerInspeccionesPorRevisionId(r.getRevisionId());
				if (inspecciones != null && !inspecciones.isEmpty()) {
					Inspeccion inspeccion = inspecciones.get(0);
					if (inspeccion != null && inspeccion.getFechaInspeccionSiguiente() != null) {
						String fechaInspeccion = sdf.format(inspeccion.getFechaInspeccionSiguiente());
						parametros.put("fecha_inspeccion", fechaInspeccion);
						cargarLogoOnac(inspeccion.getFechaInspeccionSiguiente(), bisLogoOnac);
						String fechaInspeccionAnterior = sdf.format(inspeccion.getFechaInspeccionAnterior());
						String primerIntento = r.getRevisionId() + "-1 ";
						String infoPrimerIntento = primerIntento + fechaInspeccionAnterior;
						parametros.put("info_primer_intento", infoPrimerIntento);
					}
				}
			} else {
				String fecha = sdf.format(r.getFechaCreacionRevision());
				cargarLogoOnac(this.r.getFechaCreacionRevision(), bisLogoOnac);
				parametros.put("fecha_inspeccion", fecha);
				parametros.put("info_primer_intento", fecha);
			}
			cn = this.dataSource.getConnection();
			is = getClass().getClassLoader()
					.getResourceAsStream("com/proambiente/webapp/reporte/formato_resultados.jasper");
			ponerAsteriscos(revisionId);
			List<Prueba> pruebas = listaPruebas(Integer.valueOf(revisionId));
			if (pruebas != null && !pruebas.isEmpty()) {
				Boolean isDiesel = Boolean
						.valueOf(UtilTipoCombustible.isCombustibleDiesel(this.r.getVehiculo().getTipoCombustible()));
				Boolean isMoto = Boolean
						.valueOf(UtilTipoVehiculo.isVehiculoMoto(this.r.getVehiculo().getTipoVehiculo()));
				Boolean isMotocarro = Boolean
						.valueOf(UtilTipoVehiculo.isVehiculoMotocarro(this.r.getVehiculo().getTipoVehiculo()));
				ponerMedidaPermisible(pruebas, Integer.valueOf(revisionId), isMoto, isDiesel, formateadorMedidas,
						isMotocarro);
				// String informacionEquipos =
				// UtilInformacionEquipos.ponerInformacionEquipos(pruebas, this.pruebaService,
				// isMoto, this.equipoFacade, this.r);
				// this.parametros.put("equipos", informacionEquipos);
				List<EquipoPruebaDTO> listadoEquipos = UtilInformacionEquipos.generarListadoEquipos(pruebas,
						this.pruebaService, isMoto, this.equipoFacade, this.r, isDiesel,
						this.pruebaFueEvaluadaDensidad);
				JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(listadoEquipos);
				parametros.put("dsEquipos", ds);
			}
			if (pruebaService.verificarDilucion(Integer.valueOf(revisionId))) {
				parametros.put("dilusion", "X");
			} else {
				parametros.put("dilusion", "");
			}
			cargarInformacionCda(parametros, pruebas);
			cargarLogoSuperIntendencia(r.getFechaCreacionRevision(), is, informacionCda);
			cargarFotos(parametros, pruebas, bisFoto1, bisFoto2);
			StringBuilder comentariosSb = new StringBuilder();
			colocarInformacionDocumentoIdentidad(comentariosSb, this.r.getPropietario());
			colocarComentariosPruebas(pruebas, comentariosSb);
			Boolean isMoto = Boolean.valueOf(UtilTipoVehiculo.isVehiculoMoto(this.r.getVehiculo().getTipoVehiculo()));
			Boolean isPesado = Boolean
					.valueOf(UtilTipoVehiculo.isVehiculoPesado(this.r.getVehiculo().getTipoVehiculo()));
			cargarNombreServicio(this.r.getVehiculo());
			cargarTipoMotor(this.r.getVehiculo());
			cargarInformacionProfundidadLabrado(pruebas, isMoto, isPesado, this.parametros, formateadorMedidas);
			List<Medida> medidas = cargarInformacionPresionLlantas(pruebas, this.parametros);
			cargarInformacionKilometraje(medidas, this.r.getVehiculo());
			Integer numeroIntento = this.r.getNumeroInspecciones();
			parametros.put("numero_intento", numeroIntento.toString());

			cargarInformacionReferenciaLlanta(pruebas);

			cargarInformacionAplicativos();

			cargarNumeroPasajeros(r.getVehiculo(), parametros);

			cargarLtoe(r.getVehiculo().getTipoCombustible(), r.getVehiculo().getDiametroExosto());

			if (revisionService.isRevisionAprobada(Integer.valueOf(revisionId))) {
				parametros.put("revision_aprobada", "X");
				parametros.put("revision_reprobada", "");
				if (r.getVehiculo().getServicio().getServicioId().intValue() == 5) {
					parametros.put("aprobado_ense", "X");
					parametros.put("reprobado_ense", "");
				}
			} else {
				parametros.put("revision_reprobada", "X");
				parametros.put("revision_aprobada", "");
				if (r.getVehiculo().getServicio().getServicioId().intValue() == 5) {
					Boolean cumpleEnsenianza = DeterminadorCumpleEnsenianza.cumpleEnsenianza(this.defectoService,
							this.r.getRevisionId());
					if (!cumpleEnsenianza.booleanValue()) {
						parametros.put("reprobado_ense", "X");
					} else {
						parametros.put("aprobado_ense", "X");
					}
				}
			}
			if (r.getPreventiva().booleanValue())
				parametros.put("fecha_vencimiento_repr",
						" Los resultados de las inspecciones preventivas NO tiene ninguna relacicon los resultados  de la RTMyEC oficial.");
			parametros.put("comentarios", comentariosSb.toString());
			reporte = JasperFillManager.fillReport(is, parametros, cn);
			logger.info("Reporte cargado");
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error cargando reporte ", e);
			throw new RuntimeException(e);
		} finally {
			try {
				cn.close();
			} catch (Exception e2) {
				logger.info("Excepcion cerrando la conexion");
			}
			try {
				is.close();
			} catch (Exception exc) {
				logger.info("Excepcion cerrando el flujo de datos");
			}
			try {
				if (bisFoto1 != null)
					bisFoto1.close();
			} catch (Exception exc) {
				logger.info("Excepcion cerrando el flujo de datos");
			}
			try {
				if (bisFoto2 != null)
					bisFoto2.close();
			} catch (Exception exc) {
				logger.info("Excepcion cerrando el flujo de datos");
			}
			try {
				if (isLogoSuper != null)
					isLogoSuper.close();
			} catch (Exception exc) {
				logger.info("Excepcion cerrando el flujo de datos");
			}

			try {
				if (bisLogoOnac != null)
					bisLogoOnac.close();
			} catch (Exception exc) {
				logger.info("Excepcion cerrando el flujo de datos");
			}
		}
		return reporte;
	}

	private void cargarLogoOnac(Date fechaCreacionRevision, ByteArrayInputStream bis) {

		LogoOnac logoOnac = consultaLogoOnac.consultarLogoOnac(fechaCreacionRevision);
		if (logoOnac != null && logoOnac.getImagen() != null) {
			bis = new ByteArrayInputStream(logoOnac.getImagen());
			parametros.put("logo_onac", bis);
		} else {
			InformacionCda info = informacionCdaDAO.find(1);
			if (info.getLogoOnac() != null) {
				bis = new ByteArrayInputStream(info.getLogoOnac());
				parametros.put("logo_onac", bis);
			}
		}

	}

	private void cargarLogoSuperIntendencia(Date fechaCreacionRevision, InputStream logoSuperIsParam,
			InformacionCda informacionCda) {

		LocalDate ldRevision = fechaCreacionRevision.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

		LocalDate ldActualizacion = informacionCda.getFechaCambioLogoSuperintendenciaTransporte() == null
				? LocalDate.of(2024, 06, 28)
				: informacionCda.getFechaCambioLogoSuperintendenciaTransporte().toInstant()
						.atZone(ZoneId.systemDefault()).toLocalDate();// cargar desde una columna de la base de datos

		if (ldRevision.isBefore(ldActualizacion)) {
			logoSuperIsParam = ReporteService.class
					.getResourceAsStream("/com/proambiente/webapp/reporte/vigilado_supertransporte.png");
		} else {

			logoSuperIsParam = ReporteService.class
					.getResourceAsStream("/com/proambiente/webapp/reporte/vigilado_supertransporte_nuevo.png");
		}

		if (logoSuperIsParam != null) {
			parametros.put("LogoSuperIntendencia", logoSuperIsParam);
		}
	}

	private void cargarInformacionKilometraje(List<Medida> medidas, Vehiculo v) {
		DecimalFormat df = new DecimalFormat("0");
		Optional<Medida> medidaKilometraje = medidas.stream().filter(
				m -> m.getTipoMedida().getTipoMedidaId().equals(ConstantesMedidasKilometraje.KILOMETRAJE_MEDIDA))
				.findAny();
		if (medidaKilometraje.isPresent()) {
			Double kilometraje = medidaKilometraje.get().getValor();
			if (kilometraje >= 0) {
				parametros.put("kilometraje", df.format(kilometraje));
			} else {
				parametros.put("kilometraje", "NO FUNCIONAL");
			}
		} else {
			if (v.getKilometraje() != null && !v.getKilometraje().isEmpty()) {
				parametros.put("kilometraje", v.getKilometraje());
			}
		}
	}

	private void cargarLtoe(TipoCombustible tipoCombustible, Integer diametro) {
		if (UtilTipoCombustible.isCombustibleDiesel(tipoCombustible) && !pruebaFueEvaluadaDensidad) {
			parametros.put("diametro", String.valueOf(diametro));
		} else if (UtilTipoCombustible.isCombustibleDiesel(tipoCombustible) && pruebaFueEvaluadaDensidad) {
			parametros.put("diametro", String.valueOf(diametro));
		} else {

			parametros.put("diametro", "");
		}
	}

	private List<Medida> cargarInformacionPresionLlantas(List<Prueba> pruebas, HashMap<String, Object> parametros) {
		List<Medida> medidas = new ArrayList<>();
		Optional<Prueba> optPrueba = UtilEncontrarPruebas.encontrarPruebaPorTipo(pruebas,
				ConstantesTiposPrueba.TIPO_PRUEBA_INSPECCION_SENSORIAL);
		Optional<Prueba> optPruebaFrenos = UtilEncontrarPruebas.encontrarPruebaPorTipo(pruebas,
				ConstantesTiposPrueba.TIPO_PRUEBA_FRENOS);
		if (optPrueba.isPresent()) {
			Prueba pruebaInspeccionSensorial = optPrueba.get();
			medidas = pruebaService.obtenerMedidas(pruebaInspeccionSensorial);
			if (optPruebaFrenos.isPresent()) {
				Prueba pruebaFreno = optPruebaFrenos.get();
				List<Medida> medidasPruebaFrenos = pruebaService.obtenerMedidas(pruebaFreno);
				medidas.addAll(medidasPruebaFrenos);
			}
			GeneradorPresionLlantasDTO genResPresLlanta = new GeneradorPresionLlantasDTO();
			genResPresLlanta.setPuntoComoSeparadorDecimales();
			ResultadoPresionLlantasDTO res = genResPresLlanta.generarResultadoPresionLlantas(medidas);
			parametros.put("pres_llanta_eje1_der", res.getPresionLlantaEje1Der1());
			parametros.put("pres_llanta_eje1_izq", res.getPresionLlantaEje1Izq1());
			parametros.put("pres_llanta_eje2_izq1", res.getPresionLlantaEje2Izq1());
			parametros.put("pres_llanta_eje2_izq2", res.getPresionLlantaEje2Izq2());
			parametros.put("pres_llanta_eje2_der1", res.getPresionLlantaEje2Der1());
			parametros.put("pres_llanta_eje2_der2", res.getPresionLlantaEje2Der2());
			parametros.put("pres_llanta_eje3_izq1", res.getPresionLlantaEje3Izq1());
			parametros.put("pres_llanta_eje3_izq2", res.getPresionLlantaEje3Izq2());
			parametros.put("pres_llanta_eje3_der1", res.getPresionLlantaEje3Der1());
			parametros.put("pres_llanta_eje3_der2", res.getPresionLlantaEje3Der2());
			parametros.put("pres_llanta_eje4_izq1", res.getPresionLlantaEje4Izq1());
			parametros.put("pres_llanta_eje4_izq2", res.getPresionLlantaEje4Izq2());
			parametros.put("pres_llanta_eje4_der1", res.getPresionLlantaEje4Der1());
			parametros.put("pres_llanta_eje4_der2", res.getPresionLlantaEje4Der2());
			parametros.put("pres_llanta_eje5_izq1", res.getPresionLlantaEje5Izq1());
			parametros.put("pres_llanta_eje5_izq2", res.getPresionLlantaEje5Izq2());
			parametros.put("pres_llanta_eje5_der1", res.getPresionLlantaEje5Der1());
			parametros.put("pres_llanta_eje5_der2", res.getPresionLlantaEje5Der2());
			parametros.put("pres_llanta_repuesto_der", res.getPresionLlantaRepuestoDer());
			parametros.put("pres_llanta_repuesto_izq", res.getPresionLLantaRepuestoIzq());

		} else {
			logger.info("No existe la prueba de inspeccion sensorial");
			throw new RuntimeException("No existe prueba de inspeccion sensorial");
		}
		return medidas;
	}

	private void cargarInformacionReferenciaLlanta(List<Prueba> pruebas) {
		// buscar la prueba de taximetro
		Optional<Prueba> pruebaTaximetroOpt = pruebas.stream()
				.filter(p -> p.getTipoPrueba().getTipoPruebaId().equals(9)).findAny();
		if (pruebaTaximetroOpt.isPresent()) {// si la prueba esta presente
			Prueba pruebaTaximetro = pruebaTaximetroOpt.get();
			String motivoAborto = pruebaTaximetro.getMotivoAborto();
			if (motivoAborto != null && !motivoAborto.isEmpty()) {
				String[] cadenaSeparada = motivoAborto.split(":");
				if (cadenaSeparada != null) {
					if (cadenaSeparada.length == 2) {
						String nombreLlanta = cadenaSeparada[1];
						parametros.put("ref_llanta", nombreLlanta);
					}
				}
			} else {// no se encontro motivo Aborto
				Llanta llanta = r.getVehiculo().getLlanta();
				if (llanta != null) {
					parametros.put("ref_llanta", llanta.getNombre() != null ? llanta.getNombre() : "r13");
				}
			}

		}
	}

	public JasperPrint cargarReporteInspeccion(Integer inspeccionId) {
		informacionCda = informacionCdaDAO.find(1);
		InputStream is = null;
		JasperPrint reporte = null;
		parametros = new HashMap<>();
		ByteArrayInputStream bisFoto1 = null;
		ByteArrayInputStream bisFoto2 = null;
		ByteArrayInputStream bisLogoOnac = null;

		InputStream isLogoSuper = null;

		FormateadorMedidasInterface formateadorMedidas = this.formateadorMedidasProducer.formateadorMedidas();
		this.parametros.put("id_reinspeccion", inspeccionId);
		Connection cn = null;
		try {
			cn = this.dataSource.getConnection();
			is = getClass().getClassLoader()
					.getResourceAsStream("com/proambiente/webapp/reporte/formato_resultados.jasper");
			Integer revisionId = inspeccionService.revisionIdDesdeInspeccionId(inspeccionId);
			r = revisionService.cargarRevision(revisionId);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			if (r.getNumeroInspecciones().intValue() == 2) {
				List<Inspeccion> inspecciones = inspeccionService
						.obtenerInspeccionesPorRevisionId(r.getRevisionId().intValue());
				if (inspecciones != null && !inspecciones.isEmpty()) {
					Inspeccion inspeccion1 = inspecciones.get(0);
					if (inspeccion1 != null) {
						String fechaInspeccion = sdf.format(inspeccion1.getFechaInspeccionSiguiente());
						cargarLogoOnac(inspeccion1.getFechaInspeccionSiguiente(), bisLogoOnac);
						this.parametros.put("fecha_inspeccion", fechaInspeccion);
						String fechaInspeccionAnterior = sdf.format(inspeccion1.getFechaInspeccionAnterior());
						String primerIntento = r.getRevisionId() + "-1 ";
						String infoPrimerIntento = primerIntento + fechaInspeccionAnterior;
						parametros.put("info_primer_intento", infoPrimerIntento);
					}
				}
			}
			Inspeccion inspeccion = inspeccionService.cargarInspeccionParcialmente(inspeccionId);
			parametros.put("revision_id", revisionId);
			parametros.put("unidad_presion", "psi");
			String fecha = sdf.format(inspeccion.getFechaInspeccionAnterior());
			cargarLogoOnac(inspeccion.getFechaInspeccionAnterior(), bisLogoOnac);
			parametros.put("fecha_inspeccion", fecha);
			List<Prueba> pruebas = inspeccionService.pruebasInspeccion(inspeccionId);
			Boolean isMotocarro = Boolean
					.valueOf(UtilTipoVehiculo.isVehiculoMotocarro(this.r.getVehiculo().getTipoVehiculo()));
			cargarNombreServicio(r.getVehiculo());
			if (pruebas != null && !pruebas.isEmpty()) {
				Boolean isMoto = Boolean
						.valueOf(UtilTipoVehiculo.isVehiculoMoto(this.r.getVehiculo().getTipoVehiculo()));
				Boolean isDiesel = Boolean
						.valueOf(UtilTipoCombustible.isCombustibleDiesel(this.r.getVehiculo().getTipoCombustible()));
				ponerMedidaPermisible(pruebas, revisionId, isMoto, isDiesel, formateadorMedidas, isMotocarro);
//				String informacionEquipos = UtilInformacionEquipos.ponerInformacionEquipos(pruebas, this.pruebaService,
//						isMoto, this.equipoFacade, this.r);
				// parametros.put("equipos", informacionEquipos);

				List<EquipoPruebaDTO> listadoEquipos = UtilInformacionEquipos.generarListadoEquipos(pruebas,
						this.pruebaService, isMoto, this.equipoFacade, this.r, isDiesel,
						this.pruebaFueEvaluadaDensidad);
				JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(listadoEquipos);
				parametros.put("dsEquipos", ds);
			}
			cargarInformacionCda(parametros, pruebas);
			cargarLogoSuperIntendencia(inspeccion.getFechaInspeccionAnterior(), is, informacionCda);
			cargarFotos(parametros, pruebas, bisFoto1, bisFoto2);
			StringBuilder comentariosSb = new StringBuilder();
			String comentarios = (inspeccion.getComentario() != null) ? inspeccion.getComentario() : "";
			comentariosSb.append(comentarios);
			colocarComentariosPruebas(pruebas, comentariosSb);
			parametros.put("comentarios", comentariosSb.toString());
			Boolean isMoto = Boolean.valueOf(UtilTipoVehiculo.isVehiculoMoto(r.getVehiculo().getTipoVehiculo()));
			Boolean isPesado = Boolean.valueOf(UtilTipoVehiculo.isVehiculoPesado(r.getVehiculo().getTipoVehiculo()));
			cargarInformacionProfundidadLabrado(pruebas,
					Boolean.valueOf((isMoto.booleanValue() || isMotocarro.booleanValue())), isPesado, this.parametros,
					formateadorMedidas);
			List<Medida> medidas = cargarInformacionPresionLlantas(pruebas, parametros);
			cargarInformacionKilometraje(medidas, r.getVehiculo());
			cargarInformacionReferenciaLlanta(pruebas);
			cargarInformacionAplicativos();
			parametros.put("numero_intento", inspeccion.getNumeroInspeccion().toString());
			cargarLtoe(r.getVehiculo().getTipoCombustible(), r.getVehiculo().getDiametroExosto());
			cargarTipoMotor(r.getVehiculo());
			cargarNumeroPasajeros(r.getVehiculo(), parametros);
			cargarInformacionUsuarioResponsable(parametros, r, inspeccion);
			if (inspeccion.getAprobada().booleanValue()) {
				parametros.put("revision_aprobada", "X");
				parametros.put("revision_reprobada", "");
				if (r.getVehiculo().getServicio().getServicioId() == 5) {
					parametros.put("aprobado_ense", "X");
					parametros.put("reprobado_ense", "");
				}
			} else {
				parametros.put("revision_reprobada", "X");
				parametros.put("revision_aprobada", "");
				if (r.getVehiculo().getServicio().getServicioId() == 5) {
					parametros.put("revision_reprobada", "X");
					parametros.put("revision_aprobada", "");
					if (r.getVehiculo().getServicio().getServicioId() == 5) {// ensenianza
						List<DefectoAsociado> defectos = defectoService
								.obtenerDefectosInspeccionSensorial(r.getRevisionId());
						Optional<DefectoAsociado> defectoEnseOpt = defectos.stream()
								.filter(da -> da.getEstadoDefecto().equals(EstadoDefecto.DETECTADO))
								.filter(da -> da.getDefecto().getCategoriaDefecto().getCategoriaDefectoId().equals(21))
								.findAny();
						if (defectoEnseOpt.isPresent()) {
							parametros.put("reprobado_ense", "X");
						} else {
							parametros.put("aprobado_ense", "X");
						}
					}
				}
			}
			logger.info("Consultas hibernate finalizadas");
			reporte = JasperFillManager.fillReport(is, this.parametros, cn);
			logger.info("Reporte de inspeccion generado");
		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Error cargando reporte reinspeccion", exc);
		} finally {
			try {
				cn.close();
			} catch (Exception e2) {
				logger.info("Excepcion cerrando la conexion");
			}
			try {
				is.close();
			} catch (Exception exc) {
				logger.info("Excepcion cerrando el flujo de datos");
			}
			try {
				if (bisFoto1 != null)
					bisFoto1.close();
			} catch (Exception exc) {
				logger.info("Excepcion cerrando el flujo de datos");
			}
			try {
				if (bisFoto2 != null)
					bisFoto2.close();
			} catch (Exception exc) {
				logger.info("Excepcion cerrando el flujo de datos");
			}

			try {
				if (bisLogoOnac != null)
					bisLogoOnac.close();
			} catch (Exception exc) {
				logger.info("Excepcion cerrando el flujo de datos");
			}

			try {
				if (isLogoSuper != null)
					isLogoSuper.close();
			} catch (Exception exc) {
				logger.info("Excepcion cerrando el flujo de datos");
			}
		}
		return reporte;
	}

	private void cargarNumeroPasajeros(Vehiculo vehiculo, HashMap<String, Object> parametros2) {

		Integer numeroSillas = vehiculo.getNumeroSillas() == null ? 1 : vehiculo.getNumeroSillas();
		Integer numeroPasajeros = vehiculo.getNumeroPasajeros() == null ? numeroSillas - 1
				: vehiculo.getNumeroPasajeros();

		if (informacionCda != null && informacionCda.getModoMostrarNumPasajeros() != null) {
			numeroPasajeros = informacionCda.getModoMostrarNumPasajeros().equals(ModoMostrarNumPasajeros.RESTAR_UNO)
					? numeroPasajeros - 1
					: numeroPasajeros - 0;
		}
		parametros.put("numero_pasajeros", numeroPasajeros.toString());

	}

	private void cargarNombreServicio(Vehiculo vehiculo) {
		Servicio servicio = vehiculo.getServicio();
		if (servicio != null) {
			if (servicio.getServicioId().equals(5)) {
				parametros.put("nombre_servicio", "PARTICULAR");
			} else {
				parametros.put("nombre_servicio", servicio.getNombre());
			}
		}
	}

	private void cargarTipoMotor(Vehiculo vehiculo) {
		TipoCombustible tc = vehiculo.getTipoCombustible();
		if (tc != null) {
			if ((tc.getCombustibleId().equals(5))) {// 5 es la id del tipo de combustible electrico
				parametros.put("tiempos_motor", "ELECTRICO");
			} else if (UtilTipoVehiculo.isVehiculoMoto( vehiculo.getTipoVehiculo() )|| UtilTipoVehiculo.isVehiculoMotocarro( vehiculo.getTipoVehiculo() )) {
				parametros.put("tiempos_motor", String.valueOf(vehiculo.getTiemposMotor()) + " T");
			}else if(UtilTipoCombustible.isCombustibleDiesel(tc)){
				parametros.put("tiempos_motor",String.valueOf(vehiculo.getTiemposMotor()) + "T"+  " DIESEL");
			}else{
				parametros.put("tiempos_motor",String.valueOf(vehiculo.getTiemposMotor()) + " T");
			}
		}
	}

	private void cargarFotos(HashMap<String, Object> parametros, List<Prueba> pruebas, ByteArrayInputStream bisF1,
			ByteArrayInputStream bisF2) {
		Optional<Prueba> optPrueba = pruebas.stream().filter(p -> p.getTipoPrueba().getTipoPruebaId().equals(3))
				.findAny();
		if (optPrueba.isPresent()) {
			Prueba p = optPrueba.get();
			Foto f = fotoDAO.consultarFotoPorPrueba(p);
			if (f.getFoto1() != null) {
				bisF1 = new ByteArrayInputStream(f.getFoto1());
				parametros.put("foto1", bisF1);
			}
			if (f.getFoto2() != null) {
				bisF2 = new ByteArrayInputStream(f.getFoto2());
				parametros.put("foto2", bisF2);
			}
		}

	}

	private void cargarInformacionProfundidadLabrado(List<Prueba> pruebas, Boolean isMoto_O_motoCarro, Boolean isPesado,
			HashMap<String, Object> parametros, FormateadorMedidasInterface formateadorMedidas) {
		List<Prueba> pruebasFrenosInspeccionSensorial = pruebas.stream().filter(
				p -> p.getTipoPrueba().getTipoPruebaId().equals(1) || p.getTipoPrueba().getTipoPruebaId().equals(5))
				.collect(Collectors.toList());

		utilProfLabrado.colocarInformacionProfundidadLabrado(pruebasFrenosInspeccionSensorial, isMoto_O_motoCarro,
				isPesado, parametros, formateadorMedidas);

	}

	private void cargarInformacionUsuarioResponsable(HashMap<String, Object> parametros2, Revision r2,
			Inspeccion inspeccion) {
		Usuario usuario = inspeccionService.usuarioResponsablePorInspeccionId(inspeccion.getInspeccionId());
		if (usuario == null) {
			Integer usuarioResponsableId = r2.getUsuarioResponsableId();
			Usuario usuarioResponsable = usuarioDAO.find(usuarioResponsableId);
			if (usuarioResponsable != null)
				parametros.put("nombre_resp",
						usuarioResponsable.getNombres() + " " + usuarioResponsable.getApellidos());
			else
				parametros.put("nombre_resp", "USUARIO RESPONSABLE NO CONFIGURADO");
		} else {

			parametros.put("nombre_resp", usuario.getNombres() + " " + usuario.getApellidos());

		}

	}

	private void cargarInformacionCda(HashMap<String, Object> parametros2, List<Prueba> pruebas) {
		InformacionCda info = informacionCdaDAO.find(1);
		if (r.getPropietario() != null && r.getPropietario().getTipoDocumentoIdentidad() != null) {
			Integer tipoDocumentoIdentidadId = r.getPropietario().getTipoDocumentoIdentidad()
					.getTipoDocumentoIdentidadId();
			if (tipoDocumentoIdentidadId <= 2) {
				String tipoDocumentoIdentidad = r.getPropietario().getTipoDocumentoIdentidad()
						.getCodigoParametricoRunt();
				String tmpString = "";
				StringBuilder sb = new StringBuilder();
				sb.append("CC.(");
				tmpString = tipoDocumentoIdentidad.equals("C") ? "X" : " ";
				sb.append(tmpString);
				sb.append(") NIT.(");
				tmpString = tipoDocumentoIdentidad.equals("N") ? "X" : " ";
				sb.append(tmpString);
				sb.append(")");
				parametros.put("exprTipoDoc", sb.toString());
			} else {
				parametros.put("exprTipoDoc", "");
			}
		}

		// Integer numeroIntento = r.getNumeroInspecciones();
		// parametros.put("numero_intento", numeroIntento.toString());
		String consecutivoRunt = r.getConsecutivoRunt();
		parametros.put("consecutivo_runt", consecutivoRunt);
		parametros.put("nitcda", info.getNit());
		parametros.put("nombre_cda", info.getNombre());
		parametros.put("ciudad_cda", info.getCiudad());
		parametros.put("telefono_cda", info.getTelefono1() + " - " + info.getTelefono2());
		parametros.put("direccion_cda", info.getDireccion());
		if (info.getLogo() != null) {
			ByteArrayInputStream bis = new ByteArrayInputStream(info.getLogo());
			parametros.put("logo", bis);
		}

		if (info.getCertificadoConformidad() == null || info.getCertificadoConformidad().isEmpty()) {
			parametros.put("titulo_logo_onac", "");
		} else {
			parametros.put("titulo_logo_onac", "ISO/IEC 17020:2012");
		}

		parametros.put("iso_onac",
				info.getCertificadoConformidad() != null ? info.getCertificadoConformidad() : "no configurado");
		if (info.getCorreoElectronico() == null) {
			parametros.put("correo_electronico_cda", "correo sin configurar");
		} else {
			parametros.put("correo_electronico_cda", info.getCorreoElectronico());
		}
		Integer usuarioResponsableId = r.getUsuarioResponsableId();
		Usuario usuarioResponsable = usuarioDAO.find(usuarioResponsableId);
		if (usuarioResponsable != null)
			parametros.put("nombre_resp", usuarioResponsable.getNombres() + " " + usuarioResponsable.getApellidos());
		else
			parametros.put("nombre_resp", "USUARIO RESPONSABLE NO CONFIGURADO");

	}

	private void colocarComentariosPruebas(List<Prueba> pruebas, StringBuilder comentariosSb) {
		String comentarios = r.getComentario();
		if (comentarios != null && !comentarios.isEmpty())
			comentariosSb.append(comentarios).append(" ");

		for (Prueba p : pruebas) {
			if (p.getMotivoAborto() != null && !p.getMotivoAborto().isEmpty())
				comentariosSb.append(p.getMotivoAborto()).append(" ");
			if (p.getTipoPrueba().getTipoPruebaId().equals(8)) {
				String lugarTemperatura = "";
				if (!UtilTipoVehiculo.isVehiculoMoto(r.getVehiculo().getTipoVehiculo())) {
					if (p.getMetodoTemperatura() != null) {
						switch (p.getMetodoTemperatura()) {
						case "1":
							lugarTemperatura = "ACEITE";
							break;
						case "2":
							lugarTemperatura = "BLOQUE";
							break;
						case "3":
							lugarTemperatura = "TIEMPO";
							break;

						}
						comentariosSb.append("Metodo de medicion de temperatura: ").append(lugarTemperatura);
					}
				}

			} // end of if prueba de gases

		} // end of for pruebas
	}

	private void ponerAsteriscos(int revisionId) throws SQLException {
		Connection cn = null;
		try {
			cn = dataSource.getConnection();
			String consulta = "SELECT check_medida_genera_defectos(?)";
			CallableStatement prepareCall = cn.prepareCall(consulta);
			prepareCall.setInt(1, revisionId);
			prepareCall.execute();
		} finally {
			try {
				if (cn != null) {
					cn.close();
				}
			} catch (Exception ignore) {

			}
		}
	}

	private void ponerMedidaPermisible(List<Prueba> listaPruebas, Integer idRevision, Boolean isMoto, Boolean isDiesel,
			FormateadorMedidasInterface formateadorMedidas, Boolean isMotocarro) {
		DecimalFormat df = (DecimalFormat) NumberFormat.getInstance(Locale.ENGLISH);
		df.applyPattern("#0.0#");

		Permisible permisible;
		for (Prueba p : listaPruebas) {
			List<Medida> medidas = this.pruebaService.obtenerMedidas(p);
			if (medidas != null && !medidas.isEmpty()) {
				System.out.println("Medida size:" + medidas.size());
				Integer modelo = r.getVehiculo().getModelo();
				if (p.getTipoPrueba().getTipoPruebaId().intValue() == 8) {
					Optional<Medida> mEvaluarDensidadHumo = medidas.stream()
							.filter(m -> m.getTipoMedida().getTipoMedidaId().equals(CODIGO_EVALUACION_DENSIDAD_HUMO))
							.findAny();

					pruebaFueEvaluadaDensidad = mEvaluarDensidadHumo.isPresent()
							&& mEvaluarDensidadHumo.get().getValor() > 0.0;

					parametros.put("prueba_evaluada_densidad", Boolean.valueOf(this.pruebaFueEvaluadaDensidad));
					parametros.put("unidad_diesel", pruebaFueEvaluadaDensidad ? " m-1 " : " %");

					Optional<Medida> mModificacionMotor = medidas.stream()
							.filter(m -> m.getTipoMedida().getTipoMedidaId().equals(Integer.valueOf(8066))).findAny();
					Boolean modificacionesMotor = Boolean.valueOf(
							(mModificacionMotor.isPresent() && ((Medida) mModificacionMotor.get()).getValor() > 0.0D));
					Optional<Medida> mModeloRepotenciacion = medidas.stream()
							.filter(m -> m.getTipoMedida().getTipoMedidaId().equals(Integer.valueOf(8085))).findAny();
					if (modificacionesMotor.booleanValue() && mModeloRepotenciacion.isPresent())
						modelo = (int) mModeloRepotenciacion.get().getValor();

					for (Medida m : medidas) {

						switch (m.getTipoMedida().getTipoMedidaId()) {
						case CODIGO_MEDIDA_HC_RALENTI:
							df.applyPattern("#");
							parametros.put("HC_ralenti", formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisiblePorFecha(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo,
									p.getFechaFinalizacion());
							if (permisible != null)
								parametros.put("permisible_HC_ral", df.format(permisible.getValorMaximo()));
							break;

						case CODIGO_MEDIDA_CO_RALENTI:
							df.applyPattern("#0.00");
							parametros.put("CO_ralenti", formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisiblePorFecha(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo,
									p.getFechaFinalizacion());
							if (permisible != null)
								parametros.put("permisible_CO_ral", df.format(permisible.getValorMaximo()));

							break;

						case CODIGO_MEDIDA_CO2_RALENTI:
							df.applyPattern("0.00");
							parametros.put("CO2_ralenti", formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisiblePorFecha(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo,
									p.getFechaFinalizacion());
							if (permisible != null) {
								df.applyPattern("#0");
								parametros.put("permisible_CO2", df.format(permisible.getValorMinimo()));
							}
							break;

						case CODIGO_MEDIDA_O2_RALENTI:
							df.applyPattern("0.00");
							parametros.put("O2_ralenti", formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());// dos
							if (r.getVehiculo().getTiemposMotor() == 2 && modelo >= 2010) {
								parametros.put("permisible_O2", "[0-6]");
								break;
							}
							// decimales

							permisible = permisibleService.buscarPermisiblePorFecha(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo,
									p.getFechaFinalizacion());
							if (permisible != null) {

								if (r.getVehiculo().getTipoCombustible().getCombustibleId() == 4
										|| r.getVehiculo().getTipoCombustible().getCombustibleId() == 9
										|| r.getVehiculo().getTipoCombustible().getCombustibleId() == 2) {

								} else {
									parametros.put("permisible_O2", formatearRangoPermisible(permisible, df));
								}
							} else {
								if (r.getVehiculo().getTipoVehiculo().getTipoVehiculoId() == 4
										|| r.getVehiculo().getTipoVehiculo().getTipoVehiculoId() == 5) {
									parametros.put("permisible_O2", "[0-6]");
								}
							}
							break;// medida de O2 Ralenti Cuatro Tiempos

						case CODIGO_MEDIDA_RPM_RALENTI:
							df.applyPattern("#");
							parametros.put("rev_gaso_ral", formateadorMedidas.formatearMedida(m));
							break; // Temperatura de Aceite

						case CODIGO_MEDIDA_TEMP_RALENTI:
							df.applyPattern("#");

							if (m.getValor() == 0.0) {

								if (isMoto) {
									if (informacionCda == null) {
										informacionCda = informacionCdaDAO.find(1);
									}
									String simbolo = informacionCda.getSimboloTemperaturaScooter();
									parametros.put("temp_gaso_ral", simbolo);
								} else {
									parametros.put("temp_gaso_ral", "");
								}
							} else {
								parametros.put("temp_gaso_ral", formateadorMedidas.formatearMedida(m));
							}

							if (m.getValor() == -375) {
								parametros.put("temp_gaso_ral", "");
							}
							break;

						case CODIGO_MEDIDA_RPM_CRUCERO:// Revoluciones
							df.applyPattern("#");

							parametros.put("rev_gaso_cruc", formateadorMedidas.formatearMedida(m));
							break;

						case CODIGO_MEDIDA_TEMP_CRUCERO:
							df.applyPattern("#");
							if (m.getValor() == 0.0) {
								parametros.put("temp_gaso_cruc", "");
							} else {
								parametros.put("temp_gaso_cruc", formateadorMedidas.formatearMedida(m));
							}
							break;

						case CODIGO_MEDIDA_HC_RALENTI_2T:
							df.applyPattern("#");
							parametros.put("HC_ralenti", formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());// Sin
							// decimales
							permisible = permisibleService.buscarPermisiblePorFecha(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo,
									p.getFechaFinalizacion());
							if (permisible != null)
								parametros.put("permisible_HC_ral", df.format(permisible.getValorMaximo()));
							break;

						case CODIGO_MEDIDA_CO_RALENTI_2T:// medida de CO Ralenti dos tiempos
							df.applyPattern("0.00");
							parametros.put("CO_ralenti", formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());// Dos
							// decimales
							permisible = permisibleService.buscarPermisiblePorFecha(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo,
									p.getFechaFinalizacion());
							if (permisible != null) {
								df.applyPattern("#0.0");
								parametros.put("permisible_CO_ral", "<" + df.format(permisible.getValorMaximo()));
							} // decimal
							break;

						case CODIGO_MEDIDA_CO2_RALENTI_2T:// medida de CO2 Ralenti dos tiempos
							df.applyPattern("#0.0");
							parametros.put("CO2_ralenti", formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());// Un
							// decimal
							permisible = permisibleService.buscarPermisiblePorFecha(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo,
									p.getFechaFinalizacion());
							if (permisible != null)
								parametros.put("permisible_CO_ral", formatearRangoPermisible(permisible, df));
							break;

						case CODIGO_MEDIDA_O2_RALENTI_2T:// medida de O2 Ralenti dos tiempos
							df.applyPattern("0.00");
							parametros.put("O2_ralenti", formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());// Dos
							// decimales

							if (r.getVehiculo().getTiemposMotor() == 2 && modelo >= 2010) {
								parametros.put("permisible_O2", "[0-6]");
								break;
							}

							permisible = permisibleService.buscarPermisiblePorFecha(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo,
									p.getFechaFinalizacion());
							if (permisible != null)
								parametros.put("permisible_O2", formatearRangoPermisible(permisible, df));
							else
								parametros.put("permisible_O2", "[0-11]");
							break;

						case CODIGO_MEDIDA_TEMP_RALENTI_2T:
							df.applyPattern("#");

							if (m.getValor() == 0.0) {

								if (isMoto) {
									if (informacionCda == null) {
										informacionCda = informacionCdaDAO.find(1);
									}
									String simbolo = informacionCda.getSimboloTemperaturaScooter();
									parametros.put("temp_gaso_ral", simbolo);
								} else {
									parametros.put("temp_gaso_ral", "");
								}
							} else {
								parametros.put("temp_gaso_ral", formateadorMedidas.formatearMedida(m));
							}

							if (m.getValor() == -375) {
								parametros.put("temp_gaso_ral", "");
							}

							break;

						case CODIGO_MEDIDA_RPM_RALENTI_2T:// medida de O2 Ralenti dos tiempos
							df.applyPattern("#");
							parametros.put("rev_gaso_ral",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());// Dos
							// decimales
							break;

						case CODIGO_MEDIDA_HC_CRUCERO:// medida de HC Crucero Cuatro tiempos
							df.applyPattern("#");
							parametros.put("HC_crucero", formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());// No
							// decimales
							permisible = permisibleService.buscarPermisiblePorFecha(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo,
									p.getFechaFinalizacion());
							if (permisible != null)
								parametros.put("permisible_HC_cruc", df.format(permisible.getValorMaximo()));
							break;

						case CODIGO_MEDIDA_CO_CRUCERO: // medida de CO Crucero Cuatro tiempos
							df.applyPattern("0.00");
							parametros.put("CO_crucero", formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());// dos
							// decimales
							permisible = permisibleService.buscarPermisiblePorFecha(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo,
									p.getFechaFinalizacion());
							if (permisible != null)
								parametros.put("permisible_CO_cruc", df.format(permisible.getValorMaximo()));
							break;

						case CODIGO_MEDIDA_CO2_CRUCERO:// medida de CO2 Crucero cuatro tiempos
							df.applyPattern("0.0");
							parametros.put("CO2_crucero", formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());// Un
							// decimal
							permisible = permisibleService.buscarPermisiblePorFecha(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo,
									p.getFechaFinalizacion());
							if (permisible != null) {

								parametros.put("permisible_CO2", df.format(permisible.getValorMinimo()));
							} // decimal
							break;

						case CODIGO_MEDIDA_O2_CRUCERO:// medida de O2 Crucero cuatro tiempos
							df.applyPattern("0.00");
							parametros.put("O2_crucero", formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());// dos
							// decimales
							permisible = permisibleService.buscarPermisiblePorFecha(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo,
									p.getFechaFinalizacion());
							if (permisible != null)
								if (r.getVehiculo().getTipoCombustible().getCombustibleId() == 4
										|| r.getVehiculo().getTipoCombustible().getCombustibleId() == 9
										|| r.getVehiculo().getTipoCombustible().getCombustibleId() == 2) {

								} else {
									parametros.put("permisible_O2", formatearRangoPermisible(permisible, df));
								}

							break;

						case CODIGO_DENSIDAD_HUMO_CICLO_PRELIMINAR:
							if (pruebaFueEvaluadaDensidad) {
								df.applyPattern("0.000");
								parametros.put("op_ciclo_1",
										formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							}
							break;
						case CODIGO_DENSIDAD_HUMO_CICLO_UNO:
							df.applyPattern("0.000");
							if (pruebaFueEvaluadaDensidad)
								parametros.put("op_ciclo_2",
										formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());// un
							break;
						case CODIGO_DENSIDAD_HUMO_CICLO_DOS:
							df.applyPattern("#0.000");
							if (pruebaFueEvaluadaDensidad)
								parametros.put("op_ciclo_3",
										formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());// un
							// decimal
							break;
						case CODIGO_DENSIDAD_HUMO_CICLO_TRES:
							df.applyPattern("#0.000");
							if (pruebaFueEvaluadaDensidad)
								parametros.put("op_ciclo_4",
										formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());// un
							// decimal
							break;

						case CODIGO_OPACIDAD_CICLO_PRELIMINAR:
							if (!pruebaFueEvaluadaDensidad) {
								df.applyPattern("#0.0");
								parametros.put("op_ciclo_1",
										formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							}
							break;

						case CODIGO_OPACIDAD_PRIMER_CICLO:
							df.applyPattern("#0.0");
							if (!pruebaFueEvaluadaDensidad)
								parametros.put("op_ciclo_2",
										formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());// un
							// decimal
							break;// OPACIDAD CICLO2

						case CODIGO_OPACIDAD_SEGUNDO_CICLO:
							df.applyPattern("#0.0");
							if (!pruebaFueEvaluadaDensidad)
								parametros.put("op_ciclo_3",
										formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());// un
							// decimal
							break;// OPACIDAD CICLO3

						case CODIGO_OPACIDAD_TERCER_CICLO:
							df.applyPattern("#0.0");
							if (!pruebaFueEvaluadaDensidad)
								parametros.put("op_ciclo_4",
										formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());// un
							// decimal
							break;

						case 8017:
							df.applyPattern("#0.0");
							if (!pruebaFueEvaluadaDensidad) {
								parametros.put("resultado_op",
										formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());// un
								// decimal
								permisible = permisibleService.buscarPermisiblePorFechaPorCondicionalSecundario(
										m.getTipoMedida().getTipoMedidaId(),
										r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo,
										p.getFechaFinalizacion(), r.getVehiculo().getCilindraje());
								if (permisible != null)
									parametros.put("permisible_opac", df.format(permisible.getValorMaximo()));
							}
							break;

						case CODIGO_RESULTADO_DENSIDAD_HUMO:
							df.applyPattern("#0.0");
							if (pruebaFueEvaluadaDensidad) {
								parametros.put("resultado_op",
										formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());// un
								// decimal
								permisible = permisibleService.buscarPermisiblePorFechaPorCondicionalSecundario(
										m.getTipoMedida().getTipoMedidaId(),
										r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo,
										p.getFechaFinalizacion(), r.getVehiculo().getCilindraje());
								if (permisible != null)
									parametros.put("permisible_opac", df.format(permisible.getValorMaximo()));
							}
							break;

						case CODIGO_MEDIDA_HUMEDAD_RELATIVA:
							df.applyPattern("#0.0");
							if (isDiesel) {
								parametros.put("humedad_relativa_diesel",
										formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							} else {
								parametros.put("humedad_relativa",
										formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							}
							break;

						case CODIGO_MEDIDA_TEMPERATURA_AMBIENTE:
							df.applyPattern("#0.0");
							if (isDiesel) {
								parametros.put("temperatura_ambiente_diesel",
										formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							} else {
								parametros.put("temperatura_ambiente",
										formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							}
							break;

						case CODIGO_DIESEL_TEMPERATURA_INICIAL:
							df.applyPattern("#");
							parametros.put("temp_diesel", formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case CODIGO_VELOCIDAD_GOBERNADA:
							df.applyPattern("#");
							parametros.put("rev_diesel", formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case CODIGO_REV_RAL_CICLO_PRELIMINAR:
							df.applyPattern("#");
							parametros.put("RpmRalCiclo0",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case CODIGO_REV_RAL_PRIMER_CICLO:
							df.applyPattern("#");
							parametros.put("RpmRalCiclo1",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case CODIGO_REV_RAL_SEGUNDO_CICLO:
							df.applyPattern("#");
							parametros.put("RpmRalCiclo2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case CODIGO_REV_RAL_TERCER_CICLO:
							df.applyPattern("#");
							parametros.put("RpmRalCiclo3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case CODIGO_REV_GOB_CICLO_PRELIMINAR:
							df.applyPattern("#");
							parametros.put("RpmGobCiclo0",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case CODIGO_REV_GOB_PRIMER_CICLO:
							df.applyPattern("#");
							parametros.put("RpmGobCiclo1",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case CODIGO_REV_GOB_SEGUNDO_CICLO:
							df.applyPattern("#");
							parametros.put("RpmGobCiclo2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case CODIGO_REV_GOB_TERCER_CICLO:
							df.applyPattern("#");
							parametros.put("RpmGobCiclo3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;

						case 8045:
							df.applyPattern("#");
							this.parametros.put("TempCiclo0",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case 8046:
							df.applyPattern("#");
							this.parametros.put("TempCiclo1",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case 8047:
							df.applyPattern("#");
							this.parametros.put("TempCiclo2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case 8048:
							df.applyPattern("#");
							this.parametros.put("TempCiclo3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							df.applyPattern("#");
							this.parametros.put("temp_diesel_final",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case 8050:
							df.applyPattern("#");
							this.parametros.put("RpmMediaRal",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case 8051:
							df.applyPattern("#");
							this.parametros.put("RpmMediaGob",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						}// end of switch
					} // end of for
				} // end of pruebagases
				if (p.getTipoPrueba().getTipoPruebaId() == 2) {// pruebas de
					// luces los
					df.applyPattern("#0.0"); // decimales los
// da el equipo
					for (Medida m : medidas) {
						switch (m.getTipoMedida().getTipoMedidaId()) {

						case INTENSIDAD_ALTA_DERECHA_MOTOS:// luz alta derecha motocicletas
							parametros.put("intensidad_alta_derecha_1",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null && permisible.getValorMinimo() > 0)
								parametros.put("permisible_luz_alta_derecha", df.format(permisible.getValorMinimo()));
							break;
						case INTENSIDAD_ALTA_IZQUIERDA_MOTOS:// luz alta izquierda
							parametros.put("intensidad_alta_izquierda_1",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null && permisible.getValorMinimo() > 0)
								parametros.put("permisible_luz_alta_izquierda", df.format(permisible.getValorMinimo()));
							break;
						case INTENSIDAD_ALTA_DERECHA_CICLOMOTOR:// luz alta derecha motocicletas
							parametros.put("intensidad_alta_derecha_1",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null && permisible.getValorMinimo() > 0)
								parametros.put("permisible_luz_alta_derecha", df.format(permisible.getValorMinimo()));
							break;
						case INTENSIDAD_ALTA_DERECHA_TRICIMOTO:// luz alta derecha motocicletas
							parametros.put("intensidad_alta_derecha_1",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null && permisible.getValorMinimo() > 0)
								parametros.put("permisible_luz_alta_derecha", df.format(permisible.getValorMinimo()));
							break;
						case INTENSIDAD_ALTA_DERECHA_CUADRICICLO:// luz alta derecha motocicletas
							parametros.put("intensidad_alta_derecha_1",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null && permisible.getValorMinimo() > 0)
								parametros.put("permisible_luz_alta_derecha", df.format(permisible.getValorMinimo()));
							break;
						case INTENSIDAD_ALTA_DERECHA_MOTOTRICICLO:// luz alta derecha motocicletas
							parametros.put("intensidad_alta_derecha_1",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null && permisible.getValorMinimo() > 0)
								parametros.put("permisible_luz_alta_derecha", df.format(permisible.getValorMinimo()));
							break;
						case INTENSIDAD_ALTA_IZQUIERDA_CICLOMOTOR:// luz alta izquierda
							parametros.put("intensidad_alta_izquierda_1",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null && permisible.getValorMinimo() > 0)
								parametros.put("permisible_luz_alta_izquierda", df.format(permisible.getValorMinimo()));
							break;
						case INTENSIDAD_ALTA_IZQUIERDA_TRICIMOTO:// luz alta izquierda
							parametros.put("intensidad_alta_izquierda_1",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null && permisible.getValorMinimo() > 0)
								parametros.put("permisible_luz_alta_izquierda", df.format(permisible.getValorMinimo()));
							break;
						case INTENSIDAD_ALTA_IZQUIERDA_CUATRIMOTO:// luz alta izquierda
							parametros.put("intensidad_alta_izquierda_1",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null && permisible.getValorMinimo() > 0)
								parametros.put("permisible_luz_alta_izquierda", df.format(permisible.getValorMinimo()));
							break;
						case INTENSIDAD_ALTA_IZQUIERDA_CUADRICICLO:// luz alta izquierda
							parametros.put("intensidad_alta_izquierda_1",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null && permisible.getValorMinimo() > 0)
								parametros.put("permisible_luz_alta_izquierda", df.format(permisible.getValorMinimo()));
							break;
						case INTENSIDAD_ALTA_IZQUIERDA_MOTOTRICICLO:// luz alta izquierda
							parametros.put("intensidad_alta_izquierda_1",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null && permisible.getValorMinimo() > 0)
								parametros.put("permisible_luz_alta_izquierda", df.format(permisible.getValorMinimo()));
							break;
						case INCLINACION_BAJA_IZQUIERDA_MOTOS:// angulo baja izquierda

							parametros.put("ang_incl_baja_izquierda",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_izq", formatearRangoPermisible(permisible, df));
							break;
						case INCLINACION_BAJA_IZQUIERDA_MOTOS_DOS:// angulo baja izquierda

							parametros.put("inclinacion_baja_izquierda_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_izq", formatearRangoPermisible(permisible, df));
							break;
						case INCLINACION_BAJA_IZQUIERDA_MOTOS_TRES:// angulo baja izquierda

							parametros.put("inclinacion_baja_izquierda_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_izq", formatearRangoPermisible(permisible, df));
							break;
						case INCLINACION_BAJA_IZQUIERDA_CICLOMOTOR:// angulo baja izquierda

							parametros.put("ang_incl_baja_izquierda",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_izq", formatearRangoPermisible(permisible, df));
							break;
						case INCLINACION_BAJA_IZQUIERDA_TRICIMOTO:// angulo baja izquierda

							parametros.put("ang_incl_baja_izquierda",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_izq", formatearRangoPermisible(permisible, df));
							break;
						case INCLINACION_BAJA_IZQUIERDA_CUATRIMOTO:// angulo baja izquierda

							parametros.put("ang_incl_baja_izquierda",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_izq", formatearRangoPermisible(permisible, df));
							break;
						case INCLINACION_BAJA_IZQUIERDA_CUADRICICLO:// angulo baja izquierda

							parametros.put("ang_incl_baja_izquierda",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_izq", formatearRangoPermisible(permisible, df));
							break;
						case INCLINACION_BAJA_IZQUIERDA_MOTOTRICICLO:// angulo baja izquierda

							parametros.put("ang_incl_baja_izquierda",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_izq", formatearRangoPermisible(permisible, df));
							break;
						case INCLINACION_BAJA_DERECHA:// angulo baja derecha para vehiculos
							parametros.put("ang_incl_baja_derecha",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_der", formatearRangoPermisible(permisible, df));
							break;

						case INCLINACION_BAJA_DERECHA_DOS:// angulo baja derecha para vehiculos
							parametros.put("inclinacion_baja_derecha_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_der", formatearRangoPermisible(permisible, df));
							break;
						case INCLINACION_BAJA_DERECHA_MOTOTRICICLO_DOS:// angulo baja derecha para vehiculos
							parametros.put("inclinacion_baja_derecha_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_der", formatearRangoPermisible(permisible, df));
							break;
						case INCLINACION_BAJA_DERECHA_CICLOMOTOR_DOS:// angulo baja derecha para vehiculos
							parametros.put("inclinacion_baja_derecha_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_der", formatearRangoPermisible(permisible, df));
							break;
						case INCLINACION_BAJA_DERECHA_TRICIMOTO_DOS:// angulo baja derecha para vehiculos
							parametros.put("inclinacion_baja_derecha_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_der", formatearRangoPermisible(permisible, df));
							break;
						case INCLINACION_BAJA_DERECHA_CUADRICICLO_DOS:// angulo baja derecha para vehiculos
							parametros.put("inclinacion_baja_derecha_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_der", formatearRangoPermisible(permisible, df));
							break;
						case INCLINACION_BAJA_DERECHA_CUATRIMOTO_DOS:// angulo baja derecha para vehiculos
							parametros.put("inclinacion_baja_derecha_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_der", formatearRangoPermisible(permisible, df));
							break;

						case INCLINACION_BAJA_DERECHA_TRES:// angulo baja derecha para vehiculos
							parametros.put("inclinacion_baja_derecha_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_der", formatearRangoPermisible(permisible, df));
							break;

						case INCLINACION_BAJA_DERECHA_CUATRIMOTO_TRES:// angulo baja derecha para vehiculos
							parametros.put("inclinacion_baja_derecha_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_der", formatearRangoPermisible(permisible, df));
							break;
						case INCLINACION_BAJA_DERECHA_CUADRICICLO_TRES:// angulo baja derecha para vehiculos
							parametros.put("inclinacion_baja_derecha_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_der", formatearRangoPermisible(permisible, df));
							break;
						case INCLINACION_BAJA_DERECHA_TRICIMOTO_TRES:// angulo baja derecha para vehiculos
							parametros.put("inclinacion_baja_derecha_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_der", formatearRangoPermisible(permisible, df));
							break;
						case INCLINACION_BAJA_DERECHA_CICLOMOTOR_TRES:// angulo baja derecha para vehiculos
							parametros.put("inclinacion_baja_derecha_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_der", formatearRangoPermisible(permisible, df));
							break;
						case INCLINACION_BAJA_DERECHA_MOTOTRICICLO_TRES:// angulo baja derecha para vehiculos
							parametros.put("inclinacion_baja_derecha_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_der", formatearRangoPermisible(permisible, df));
							break;

						case INCLINACION_BAJA_IZQUIERDA:// angulo baja izquierda para vehiculos
							parametros.put("ang_incl_baja_izquierda",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_izq", formatearRangoPermisible(permisible, df));
							break;
						case INCLINACION_BAJA_IZQUIERDA_DOS:// angulo baja izquierda para vehiculos
							parametros.put("inclinacion_baja_izquierda_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_izq", formatearRangoPermisible(permisible, df));
							break;
						case INCLINACION_BAJA_IZQUIERDA_MOTOTRICICLO_DOS:// angulo baja izquierda para vehiculos
							parametros.put("inclinacion_baja_izquierda_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_izq", formatearRangoPermisible(permisible, df));
							break;
						case INCLINACION_BAJA_IZQUIERDA_CICLOMOTOR_DOS:// angulo baja izquierda para vehiculos
							parametros.put("inclinacion_baja_izquierda_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_izq", formatearRangoPermisible(permisible, df));
							break;
						case INCLINACION_BAJA_IZQUIERDA_TRICIMOTO_DOS:// angulo baja izquierda para vehiculos
							parametros.put("inclinacion_baja_izquierda_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_izq", formatearRangoPermisible(permisible, df));
							break;
						case INCLINACION_BAJA_IZQUIERDA_CUADRICICLO_DOS:// angulo baja izquierda para vehiculos
							parametros.put("inclinacion_baja_izquierda_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_izq", formatearRangoPermisible(permisible, df));
							break;
						case INCLINACION_BAJA_IZQUIERDA_CUATRIMOTO_DOS:// angulo baja izquierda para vehiculos
							parametros.put("inclinacion_baja_izquierda_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_izq", formatearRangoPermisible(permisible, df));
							break;

						case INCLINACION_BAJA_IZQUIERDA_TRES:// angulo baja izquierda para vehiculos
							parametros.put("inclinacion_baja_izquierda_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_izq", formatearRangoPermisible(permisible, df));
							break;
						case INCLINACION_BAJA_IZQUIERDA_CUATRIMOTO_TRES:// angulo baja izquierda para vehiculos
							parametros.put("inclinacion_baja_izquierda_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_izq", formatearRangoPermisible(permisible, df));
							break;
						case INCLINACION_BAJA_IZQUIERDA_CUADRICICLO_TRES:// angulo baja izquierda para vehiculos
							parametros.put("inclinacion_baja_izquierda_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_izq", formatearRangoPermisible(permisible, df));
							break;
						case INCLINACION_BAJA_IZQUIERDA_TRICIMOTO_TRES:// angulo baja izquierda para vehiculos
							parametros.put("inclinacion_baja_izquierda_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_izq", formatearRangoPermisible(permisible, df));
							break;
						case INCLINACION_BAJA_IZQUIERDA_CICLOMOTOR_TRES:// angulo baja izquierda para vehiculos
							parametros.put("inclinacion_baja_izquierda_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_izq", formatearRangoPermisible(permisible, df));
							break;
						case INCLINACION_BAJA_IZQUIERDA_MOTOTRICICLO_TRES:// angulo baja izquierda para vehiculos
							parametros.put("inclinacion_baja_izquierda_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_izq", formatearRangoPermisible(permisible, df));
							break;

						case INTENSIDAD_BAJA_DERECHA:// intensidad luz baja derecha para vehiculos
							parametros.put("int_luz_baja_derecha",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_int_baja_der", formatearRangoPermisible(permisible, df));
							break;
						case INTENSIDAD_BAJA_DERECHA_DOS:// intensidad luz baja derecha para vehiculos
							parametros.put("intensidad_baja_derecha_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("", formatearRangoPermisible(permisible, df));
							break;
						case INTENSIDAD_BAJA_DERECHA_MOTOTRICICLO_DOS:// intensidad luz baja derecha para vehiculos
							parametros.put("intensidad_baja_derecha_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("", formatearRangoPermisible(permisible, df));
							break;
						case INTENSIDAD_BAJA_DERECHA_CICLOMOTOR_DOS:// intensidad luz baja derecha para vehiculos
							parametros.put("intensidad_baja_derecha_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("", formatearRangoPermisible(permisible, df));
							break;
						case INTENSIDAD_BAJA_DERECHA_TRICIMOTO_DOS:// intensidad luz baja derecha para vehiculos
							parametros.put("intensidad_baja_derecha_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("", formatearRangoPermisible(permisible, df));
							break;
						case INTENSIDAD_BAJA_DERECHA_CUADRICICLO_DOS:// intensidad luz baja derecha para vehiculos
							parametros.put("intensidad_baja_derecha_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("", formatearRangoPermisible(permisible, df));
							break;
						case INTENSIDAD_BAJA_DERECHA_CUATRIMOTO_DOS:// intensidad luz baja derecha para vehiculos
							parametros.put("intensidad_baja_derecha_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("", formatearRangoPermisible(permisible, df));
							break;
//

						case INTENSIDAD_BAJA_DERECHA_TRES:// intensidad luz baja derecha para vehiculos
							parametros.put("intensidad_baja_derecha_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_der", formatearRangoPermisible(permisible, df));
							break;
						case INTENSIDAD_BAJA_DERECHA_CUATRIMOTO_TRES:// intensidad luz baja derecha para vehiculos
							parametros.put("intensidad_baja_derecha_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_der", formatearRangoPermisible(permisible, df));
							break;
						case INTENSIDAD_BAJA_DERECHA_CUADRICICLO_TRES:// intensidad luz baja derecha para vehiculos
							parametros.put("intensidad_baja_derecha_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_der", formatearRangoPermisible(permisible, df));
							break;
						case INTENSIDAD_BAJA_DERECHA_TRICIMOTO_TRES:// intensidad luz baja derecha para vehiculos
							parametros.put("intensidad_baja_derecha_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_der", formatearRangoPermisible(permisible, df));
							break;
						case INTENSIDAD_BAJA_DERECHA_CICLOMOTOR_TRES:// intensidad luz baja derecha para vehiculos
							parametros.put("intensidad_baja_derecha_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_der", formatearRangoPermisible(permisible, df));
							break;
						case INTENSIDAD_BAJA_DERECHA_MOTOTRICICLO_TRES:// intensidad luz baja derecha para vehiculos
							parametros.put("intensidad_baja_derecha_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_der", formatearRangoPermisible(permisible, df));
							break;

						case INTENSIDAD_ALTA_DERECHA:// intensidad de la luz alta derecha para
// vehiculos
							parametros.put("intensidad_alta_derecha_1", formateadorMedidas.formatearMedida(m));
//
							break;
						case INTENSIDAD_ALTA_DERECHA_CUATRIMOTO:// intensidad de la luz alta derecha para
// vehiculos
							parametros.put("intensidad_alta_derecha_1", formateadorMedidas.formatearMedida(m));
//
							break;

						case INTENSIDAD_ALTA_DERECHA_DOS:// intensidad de la luz alta derecha para
// vehiculos
							parametros.put("intensidad_alta_derecha_2", formateadorMedidas.formatearMedida(m));
//
							break;
						case INTENSIDAD_ALTA_DER_MOTO_DOS:// intensidad de la luz alta derecha para
// vehiculos
							parametros.put("intensidad_alta_derecha_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
//
							break;
						case INTENSIDAD_ALTA_DER_CUATRIMOTO_DOS:// intensidad de la luz alta derecha para
// vehiculos
							parametros.put("intensidad_alta_derecha_2", formateadorMedidas.formatearMedida(m));
//
							break;
						case INTENSIDAD_ALTA_DER_CUADRICICLO_DOS:// intensidad de la luz alta derecha para
// vehiculos
							parametros.put("intensidad_alta_derecha_2", formateadorMedidas.formatearMedida(m));
//
							break;
						case INTENSIDAD_ALTA_DER_TRICIMOTO_DOS:// intensidad de la luz alta derecha para
// vehiculos
							parametros.put("intensidad_alta_derecha_2", formateadorMedidas.formatearMedida(m));
//
							break;
						case INTENSIDAD_ALTA_DER_CICLOMOTOR_DOS:// intensidad de la luz alta derecha para
// vehiculos
							parametros.put("intensidad_alta_derecha_2", formateadorMedidas.formatearMedida(m));
//
							break;
						case INTENSIDAD_ALTA_DER_MOTOTRICICLO_DOS:// intensidad de la luz alta derecha para
// vehiculos
							parametros.put("intensidad_alta_derecha_2", formateadorMedidas.formatearMedida(m));
//
							break;

						case INTENSIDAD_ALTA_DERECHA_TRES:// intensidad de la luz alta derecha para
// vehiculos
							parametros.put("intensidad_alta_derecha_3", formateadorMedidas.formatearMedida(m));
//
							break;

						case INTENSIDAD_ALTA_DER_MOTO_TRES:// intensidad de la luz alta derecha para
// vehiculos
							parametros.put("intensidad_alta_derecha_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
//
							break;

						case INTENSIDAD_ALTA_DER_MOTOTRICICLO_TRES:// intensidad de la luz alta derecha para
// vehiculos
							parametros.put("intensidad_alta_derecha_3", formateadorMedidas.formatearMedida(m));
//
							break;
						case INTENSIDAD_ALTA_DER_CICLOMOTOR_TRES:// intensidad de la luz alta derecha para
// vehiculos
							parametros.put("intensidad_alta_derecha_3", formateadorMedidas.formatearMedida(m));
//
							break;
						case INTENSIDAD_ALTA_DER_TRICIMOTO_TRES:// intensidad de la luz alta derecha para
// vehiculos
							parametros.put("intensidad_alta_derecha_3", formateadorMedidas.formatearMedida(m));
//
							break;
						case INTENSIDAD_ALTA_DER_CUADRICICLO_TRES:// intensidad de la luz alta derecha para
// vehiculos
							parametros.put("intensidad_alta_derecha_3", formateadorMedidas.formatearMedida(m));
//
							break;
						case INTENSIDAD_ALTA_DER_CUATRIMOTO_TRES:// intensidad de la luz alta derecha para
// vehiculos
							parametros.put("intensidad_alta_derecha_3", formateadorMedidas.formatearMedida(m));
//
							break;

						case SUMA_EXPLORADORAS:// intensidad de las luces exploradoras
// esta medida no sale en el reporte impreso
							break;
						case INTENSIDAD_BAJA_IZQUIERDA:// intensidad de la luz baja izquierda para
// vehiculos
							parametros.put("int_luz_baja_izquierda",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_BAJA_IZQUIERDA_DOS:// intensidad de la luz baja izquierda para
// vehiculos
							parametros.put("intensidad_baja_izquierda_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_BAJA_IZQUIERDA_MOTOTRICICLO_DOS:// intensidad de la luz baja izquierda para
// vehiculos
							parametros.put("intensidad_baja_izquierda_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_BAJA_IZQUIERDA_CICLOMOTOR_DOS:// intensidad de la luz baja izquierda para
// vehiculos
							parametros.put("intensidad_baja_izquierda_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_BAJA_IZQUIERDA_TRICIMOTO_DOS:// intensidad de la luz baja izquierda para
// vehiculos
							parametros.put("intensidad_baja_izquierda_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_BAJA_IZQUIERDA_CUADRICICLO_DOS:// intensidad de la luz baja izquierda para
// vehiculos
							parametros.put("intensidad_baja_izquierda_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_BAJA_IZQUIERDA_CUATRIMOTO_DOS:// intensidad de la luz baja izquierda para
// vehiculos
							parametros.put("intensidad_baja_izquierda_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;

						case INTENSIDAD_BAJA_IZQUIERDA_TRES:// intensidad de la luz baja izquierda para
// vehiculos
							parametros.put("intensidad_baja_izquierda_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_BAJA_IZQUIERDA_CUATRIMOTO_TRES:// intensidad de la luz baja izquierda para
// vehiculos
							parametros.put("intensidad_baja_izquierda_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_BAJA_IZQUIERDA_CUADRICICLO_TRES:// intensidad de la luz baja izquierda para
// vehiculos
							parametros.put("intensidad_baja_izquierda_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_BAJA_IZQUIERDA_TRICIMOTO_TRES:// intensidad de la luz baja izquierda para
// vehiculos
							parametros.put("intensidad_baja_izquierda_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_BAJA_IZQUIERDA_CICLOMOTOR_TRES:// intensidad de la luz baja izquierda para
// vehiculos
							parametros.put("intensidad_baja_izquierda_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_BAJA_IZQUIERDA_MOTOTRICICLO_TRES:// intensidad de la luz baja izquierda para
// vehiculos
							parametros.put("intensidad_baja_izquierda_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;

						case INTENSIDAD_ALTA_IZQUIERDA:// intensidad alta izquierda
							parametros.put("intensidad_alta_izquierda_1",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_ALTA_IZQUIERDA_DOS:// intensidad alta izquierda
							parametros.put("intensidad_alta_izquierda_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_ALTA_IZQ_MOTO_DOS:// intensidad alta izquierda
							parametros.put("intensidad_alta_izquierda_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_ALTA_IZQ_MOTOTRICICLO_DOS:// intensidad alta izquierda
							parametros.put("intensidad_alta_izquierda_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_ALTA_IZQ_CICLOMOTOR_DOS:// intensidad alta izquierda
							parametros.put("intensidad_alta_izquierda_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_ALTA_IZQ_TRICIMOTO_DOS:// intensidad alta izquierda
							parametros.put("intensidad_alta_izquierda_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_ALTA_IZQ_CUATRIMOTO_DOS:// intensidad alta izquierda
							parametros.put("intensidad_alta_izquierda_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;

						case INTENSIDAD_ALTA_IZQ_CUADRICICLO_DOS:// intensidad alta izquierda
							parametros.put("intensidad_alta_izquierda_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;

						case INTENSIDAD_ALTA_IZQUIERDA_TRES:// intensidad alta izquierda
							parametros.put("intensidad_alta_izquierda_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;

						case INTENSIDAD_ALTA_IZQ_MOTO_TRES:// intensidad alta izquierda
							parametros.put("intensidad_alta_izquierda_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_ALTA_IZQ_CUATRIMOTO_TRES:// intensidad alta izquierda
							parametros.put("intensidad_alta_izquierda_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;

						case INTENSIDAD_ALTA_IZQ_CUADRICICLO_TRES:// intensidad alta izquierda
							parametros.put("intensidad_alta_izquierda_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_ALTA_IZQ_TRICIMOTO_TRES:// intensidad alta izquierda
							parametros.put("intensidad_alta_izquierda_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_ALTA_IZQ_CICLOMOTOR_TRES:// intensidad alta izquierda
							parametros.put("intensidad_alta_izquierda_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_ALTA_IZQ_MOTOTRICICLO_TRES:// intensidad alta izquierda
							parametros.put("intensidad_alta_izquierda_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;

						case SUMA_LUCES:
							if (!isMoto) {
								parametros.put("suma_luces",
										formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
								permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
										r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
								if (permisible != null)
									parametros.put("permisible_suma_luces", df.format(permisible.getValorMaximo()));
							}
							if (isMotocarro) {
								parametros.put("suma_luces",
										formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
								permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
										r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
								if (permisible != null)
									parametros.put("permisible_suma_luces", df.format(permisible.getValorMaximo()));
							}
							break;

						case SUMA_LUCES_CUATRIMOTO:

							parametros.put("suma_luces", formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_suma_luces", df.format(permisible.getValorMaximo()));

							break;
						case SUMA_LUCES_CUADRICICLO:

							parametros.put("suma_luces", formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_suma_luces", df.format(permisible.getValorMaximo()));

							break;

						case SUMA_LUCES_MOTOTRICICLO:

							parametros.put("suma_luces", formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_suma_luces", df.format(permisible.getValorMaximo()));

							break;

						case SUMA_LUCES_CICLOMOTOR:

							parametros.put("suma_luces", formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_suma_luces", df.format(permisible.getValorMaximo()));

							break;

						case SUMA_LUCES_TRICIMOTO:

							parametros.put("suma_luces", formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_suma_luces", df.format(permisible.getValorMaximo()));

							break;

						case INCLINACION_BAJA_DERECHA_MOTOS:// angulo baja derecha
							parametros.put("ang_incl_baja_derecha",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_der", formatearRangoPermisible(permisible, df));
							break;
						case INCLINACION_BAJA_DERECHA_MOTOS_DOS:// angulo baja derecha
							parametros.put("inclinacion_baja_derecha_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_der", formatearRangoPermisible(permisible, df));
							break;
						case INCLINACION_BAJA_DERECHA_MOTOS_TRES:// angulo baja derecha
							parametros.put("inclinacion_baja_derecha_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_der", formatearRangoPermisible(permisible, df));
							break;
						case INCLINACION_BAJA_DERECHA_CICLOMOTOR:// angulo baja derecha
							parametros.put("ang_incl_baja_derecha",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_der", formatearRangoPermisible(permisible, df));
							break;
						case INCLINACION_BAJA_DERECHA_TRICIMOTO:// angulo baja derecha
							parametros.put("ang_incl_baja_derecha",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_der", formatearRangoPermisible(permisible, df));
							break;
						case INCLINACION_BAJA_DERECHA_CUATRIMOTO:// angulo baja derecha
							parametros.put("ang_incl_baja_derecha",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_der", formatearRangoPermisible(permisible, df));
							break;
						case INCLINACION_BAJA_DERECHA_CUADRICICLO:// angulo baja derecha
							parametros.put("ang_incl_baja_derecha",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_der", formatearRangoPermisible(permisible, df));
							break;
						case INCLINACION_BAJA_DERECHA_MOTOTRICICLO:// angulo baja derecha
							parametros.put("ang_incl_baja_derecha",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_der", formatearRangoPermisible(permisible, df));
							break;

						case INTENSIDAD_BAJA_DERECHA_MOTOS:// luz baja derecha
							parametros.put("int_luz_baja_derecha",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_int_baja_der", formatearRangoPermisible(permisible, df));
							break;
						case INTENSIDAD_BAJA_DERECHA_CICLOMOTOR:// luz baja derecha
							parametros.put("int_luz_baja_derecha",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_int_baja_der", formatearRangoPermisible(permisible, df));
							break;
						case INTENSIDAD_BAJA_DERECHA_TRICIMOTO:// luz baja derecha
							parametros.put("int_luz_baja_derecha",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_int_baja_der", formatearRangoPermisible(permisible, df));
							break;
						case INTENSIDAD_BAJA_DERECHA_CUATRIMOTO:// luz baja derecha
							parametros.put("int_luz_baja_derecha",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_int_baja_der", formatearRangoPermisible(permisible, df));
							break;

						case INTENSIDAD_BAJA_DERECHA_CUADRICICLO:// luz baja derecha
							parametros.put("int_luz_baja_derecha",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_int_baja_der", formatearRangoPermisible(permisible, df));
							break;
						case INTENSIDAD_BAJA_DERECHA_MOTOTRICICLO:// luz baja derecha
							parametros.put("int_luz_baja_derecha",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_int_baja_der", formatearRangoPermisible(permisible, df));
							break;
						case INTENSIDAD_BAJA_IZQUIERDA_MOTOS:// luz baja izquierda
							parametros.put("int_luz_baja_izquierda",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_int_baja_izq", formatearRangoPermisible(permisible, df));
							break;
						case INTENSIDAD_BAJA_IZQUIERDA_MOTOS_DOS:// luz baja izquierda
							parametros.put("intensidad_baja_izquierda_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_int_baja_izq", formatearRangoPermisible(permisible, df));
							break;
						case INTENSIDAD_BAJA_IZQUIERDA_MOTOS_TRES:// luz baja izquierda
							parametros.put("intensidad_baja_izquierda_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_int_baja_izq", formatearRangoPermisible(permisible, df));
							break;
						case INTENSIDAD_BAJA_DERECHA_MOTOS_DOS:// luz baja izquierda
							parametros.put("intensidad_baja_derecha_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_int_baja_der", formatearRangoPermisible(permisible, df));
							break;
						case INTENSIDAD_BAJA_DERECHA_MOTOS_TRES:// luz baja izquierda
							parametros.put("intensidad_baja_derecha_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_int_baja_der", formatearRangoPermisible(permisible, df));
							break;
						case INTENSIDAD_BAJA_IZQUIERDA_CICLOMOTOR:// luz baja izquierda
							parametros.put("int_luz_baja_izquierda",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_int_baja_izq", formatearRangoPermisible(permisible, df));
							break;
						case INTENSIDAD_BAJA_IZQUIERDA_TRICIMOTO:// luz baja izquierda
							parametros.put("int_luz_baja_izquierda",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_int_baja_izq", formatearRangoPermisible(permisible, df));
							break;
						case INTENSIDAD_BAJA_IZQUIERDA_CUATRIMOTO:// luz baja izquierda
							parametros.put("int_luz_baja_izquierda",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_int_baja_izq", formatearRangoPermisible(permisible, df));
							break;
						case INTENSIDAD_BAJA_IZQUIERDA_CUADRICICLO:// luz baja izquierda
							parametros.put("int_luz_baja_izquierda",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_int_baja_izq", formatearRangoPermisible(permisible, df));
							break;
						case INTENSIDAD_BAJA_IZQUIERDA_MOTOTRICICLO:// luz baja izquierda
							parametros.put("int_luz_baja_izquierda",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_int_baja_izq", formatearRangoPermisible(permisible, df));
							break;
						case LUCES_BAJAS_ALTAS_EXPL_SIMULTANEAS:

							if (m.getValor() == 1.0) {
								String simultaneas = "Si";
								parametros.put("luces_simultaneas", simultaneas);
								parametros.put("luces_altas_simultaneas", simultaneas);
								parametros.put("luces_exploradoras_simultaneas", simultaneas);
							} else {// retrocompatibilidad

								parametros.put("luces_simultaneas", "No");
								parametros.put("luces_altas_simultaneas", "Si");
								parametros.put("luces_exploradoras_simultaneas", "Si");
							}

							break;
						case LUCES_BAJAS_EXPL_SIMULTANEAS:
							parametros.put("luces_simultaneas", "Si");
							parametros.put("luces_altas_simultaneas", "No");
							parametros.put("luces_exploradoras_simultaneas", "Si");

							break;
						case LUCES_ALTAS_EXPL_SIMULTANEAS:
							parametros.put("luces_simultaneas", "No");
							parametros.put("luces_altas_simultaneas", "Si");
							parametros.put("luces_exploradoras_simultaneas", "Si");
							break;
						case LUCES_NINGUNA_SIMULTANEA:
							parametros.put("luces_simultaneas", "No");
							parametros.put("luces_altas_simultaneas", "No");
							parametros.put("luces_exploradoras_simultaneas", "No");
							break;
						case LUCES_BAJAS_ALTAS_SIMUL_NO_EXPL:
							parametros.put("luces_simultaneas", "Si");
							parametros.put("luces_altas_simultaneas", "Si");
							parametros.put("luces_exploradoras_simultaneas", "");
							break;

						case LUCES_BAJAS_ALTAS_NO_SIMUL_NO_EXPL:
							parametros.put("luces_simultaneas", "No");
							parametros.put("luces_altas_simultaneas", "No");
							parametros.put("luces_exploradoras_simultaneas", "");
							break;

						case 2016:// intensidad alt derecha en motocarro
// parametros.put("SumaLuces",String.valueOf(m.getValormedida()));//no
// sale en el reporte
							break;
						case 2017:// intensidad alta izquierda en motocarro
// parametros.put("SumaLuces",String.valueOf(m.getValormedida()));
							break;
						case 2018:// intensidad baja derecha motocarro
							parametros.put("int_luz_baja_derecha",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_int_baja_der", formatearRangoPermisible(permisible, df));
							break;
						case 2019:// intensidad baja izquierda en motocarro
							parametros.put("int_luz_baja_izquierda",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_int_baja_izq", formatearRangoPermisible(permisible, df));
							break;
						case 2020://
							parametros.put("ang_incl_baja_derecha",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_izq", formatearRangoPermisible(permisible, df));
							break;
						case 2021:// inclinacion baja derecha motocarro
							parametros.put("ang_incl_baja_derecha",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_ang_incl_der", formatearRangoPermisible(permisible, df));
							break;
						case INTENSIDAD_EXPLORADORA_DERECHA:// intensidad alta izquierda
							parametros.put("exploradora_derecha_1",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_EXP_DER_MOTO_UNO:// intensidad alta izquierda
							parametros.put("exploradora_derecha_1",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;

						case INTENSIDAD_EXPLORADORA_DERECHA_CUATRIMOTO:// intensidad alta izquierda
							parametros.put("exploradora_derecha_1",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_EXPLORADORA_DERECHA_CUADRICICLO:// intensidad alta izquierda
							parametros.put("exploradora_derecha_1",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_EXPLORADORA_DERECHA_MOTOTRICICLO:// intensidad alta izquierda
							parametros.put("exploradora_derecha_1",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_EXPLORADORA_DERECHA_TRICIMOTO:// intensidad alta izquierda
							parametros.put("exploradora_derecha_1",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_EXPLORADORA_DERECHA_CICLOMOTOR:// intensidad alta izquierda
							parametros.put("exploradora_derecha_1",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_EXPLORADORA_IZQUIERDA:// intensidad alta izquierda
							parametros.put("exploradora_izquierda_1",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_EXP_IZQ_MOTO_UNO:// intensidad alta izquierda
							parametros.put("exploradora_izquierda_1",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_EXPLORADORA_IZQUIERDA_CUATRIMOTO:// intensidad alta izquierda
							parametros.put("exploradora_izquierda_1",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_EXPLORADORA_IZQUIERDA_CUADRICICLO:// intensidad alta izquierda
							parametros.put("exploradora_izquierda_1",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_EXPLORADORA_IZQUIERDA_MOTOTRICICLO:// intensidad alta izquierda
							parametros.put("exploradora_izquierda_1",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_EXPLORADORA_IZQUIERDA_TRICIMOTO:// intensidad alta izquierda
							parametros.put("exploradora_izquierda_1",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_EXPLORADORA_IZQUIERDA_CICLOMOTOR:// intensidad alta izquierda
							parametros.put("exploradora_izquierda_1",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;

						case INTENSIDAD_EXPLORADORA_DERECHA_DOS:// intensidad alta izquierda
							parametros.put("exploradora_derecha_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_EXP_DER_CUATRIMOTO_DOS:// intensidad alta izquierda
							parametros.put("exploradora_derecha_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_EXP_DER_TRICIMOTO_DOS:// intensidad alta izquierda
							parametros.put("exploradora_derecha_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_EXP_DER_MOTOTRICICLO_DOS:// intensidad alta izquierda
							parametros.put("exploradora_derecha_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_EXP_DER_CICLOMOTOR_DOS:// intensidad alta izquierda
							parametros.put("exploradora_derecha_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;

						case INTENSIDAD_EXP_DER_MOTO_DOS:// intensidad alta izquierda
							parametros.put("exploradora_derecha_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_EXPLORADORA_IZQUIERDA_DOS:// intensidad alta izquierda
							parametros.put("exploradora_izquierda_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_EXP_IZQ_MOTO_DOS:// intensidad alta izquierda
							parametros.put("exploradora_izquierda_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;

						case INTENSIDAD_EXP_IZQ_TRICIMOTO_DOS:// intensidad alta izquierda
							parametros.put("exploradora_izquierda_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_EXP_IZQ_CICLOMOTOR_DOS:// intensidad alta izquierda
							parametros.put("exploradora_izquierda_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_EXP_IZQ_MOTOTRICICLO_DOS:// intensidad alta izquierda
							parametros.put("exploradora_izquierda_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_EXP_IZQ_CUATRIMOTO_DOS:// intensidad alta izquierda
							parametros.put("exploradora_izquierda_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;

						case INTENSIDAD_EXPLORADORA_DERECHA_TRES:// intensidad alta izquierda
							parametros.put("exploradora_derecha_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_EXP_DER_MOTO_TRES:// intensidad alta izquierda
							parametros.put("exploradora_derecha_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_EXP_DER_CUATRIMOTO_TRES:// intensidad alta izquierda
							parametros.put("exploradora_derecha_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_EXP_DER_CUADRICICLO_TRES:// intensidad alta izquierda
							parametros.put("exploradora_derecha_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_EXP_DER_TRICIMOTO_TRES:// intensidad alta izquierda
							parametros.put("exploradora_derecha_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_EXP_DER_CICLOMOTOR_TRES:// intensidad alta izquierda
							parametros.put("exploradora_derecha_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_EXP_DER_MOTOTRICICLO_TRES:// intensidad alta izquierda
							parametros.put("exploradora_derecha_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;

						case INTENSIDAD_EXPLORADORA_IZQUIERDA_TRES:// intensidad alta izquierda
							parametros.put("exploradora_izquierda_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_EXP_IZQ_MOTO_TRES:// intensidad alta izquierda
							parametros.put("exploradora_izquierda_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_EXP_IZQ_MOTOTRICICLO_TRES:// intensidad alta izquierda
							parametros.put("exploradora_izquierda_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_EXP_IZQ_CICLOMOTOR_TRES:// intensidad alta izquierda
							parametros.put("exploradora_izquierda_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_EXP_IZQ_TRICIMOTO_TRES:// intensidad alta izquierda
							parametros.put("exploradora_izquierda_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_EXP_IZQ_CUADRICICLO_TRES:// intensidad alta izquierda
							parametros.put("exploradora_izquierda_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case INTENSIDAD_EXP_IZQ_CUATRIMOTO_TRES:// intensidad alta izquierda
							parametros.put("exploradora_izquierda_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;

						default:
							break;
						}// end of switch
					} // end of for ista de Medida
				} // end of if pruebas de luces

				if (p.getTipoPrueba().getTipoPruebaId() == 5) {// pruebas de
					// frenos sin
					// decimales

					for (Medida m : medidas) {
						switch (m.getTipoMedida().getTipoMedidaId()) {
						case 5000:// peso derecho eje 1
							df.applyPattern("0");
							parametros.put("peso_eje_1_der",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case 5001:// peso derecho eje 2
							df.applyPattern("0");
							parametros.put("peso_eje_2_der",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case 5002:
							df.applyPattern("#");
							parametros.put("peso_eje_3_der",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case 5003:
							df.applyPattern("#");
							parametros.put("peso_eje_4_der",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case PESO_DERECHO_EJE_CINCO:
							df.applyPattern("#");
							parametros.put("peso_eje_5_der",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case 5004:
							df.applyPattern("#");
							parametros.put("peso_eje_1_izq",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case 5005:
							df.applyPattern("#");
							parametros.put("peso_eje_2_izq",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case 5006:
							df.applyPattern("#");
							parametros.put("peso_eje_3_izq",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case 5007:
							df.applyPattern("#");
							parametros.put("peso_eje_4_izq",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case PESO_IZQUIERDO_EJE_CINCO:
							df.applyPattern("#");
							parametros.put("peso_eje_5_izq",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
						case 5008:// fuerza de frenado eje 1
							df.applyPattern("#");
							parametros.put("frz_eje_1_der",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case 5009:// fuerza de frenado eje2
							df.applyPattern("#");
							parametros.put("frz_eje_2_der",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case 5010:// fuerza de frenado eje2
							df.applyPattern("#");
							parametros.put("frz_eje_3_der",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case 5011:// fuerza de frenado eje2
							df.applyPattern("#");
							parametros.put("frz_eje_4_der",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case FUERZA_DERECHA_EJE_CINCO:
							df.applyPattern("#");
							parametros.put("frz_eje_5_der",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());

						case 5012:// fuerza de frenado eje2
							df.applyPattern("#");
							parametros.put("frz_eje_1_izq",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case 5013:// fuerza de frenado eje2
							df.applyPattern("#");
							parametros.put("frz_eje_2_izq",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case 5014:// fuerza de frenado eje2
							df.applyPattern("#");
							parametros.put("frz_eje_3_izq",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case 5015:// fuerza de frenado eje2
							df.applyPattern("#");
							parametros.put("frz_eje_4_izq",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case FUERZA_IZQUIERDA_EJE_CINCO:
							df.applyPattern("#");
							parametros.put("frz_eje_5_izq",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case FUERZA_TOTAL_FRENO_MANO_DERECHA:
							df.applyPattern("#");
							parametros.put("frz_der_fren_mano",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case FUERZA_DERECHA_FREN_ESTA_CICLO:
							df.applyPattern("#");
							parametros.put("frz_der_fren_mano",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case FUERZA_TOTAL_FRENO_MANO_IZQUIERDA:
							df.applyPattern("#");
							parametros.put("frz_izq_fren_mano",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case PESO_DERECHO_TOTAL:
							df.applyPattern("#");
							parametros.put("peso_derecho_total",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case PESO_DERECHO_FREN_ESTA_CICLO:
							df.applyPattern("#");
							parametros.put("peso_derecho_total",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;
						case PESO_IZQUIERDO_TOTAL:
							df.applyPattern("#");
							parametros.put("peso_izquierdo_total",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());
							break;

						case 5024:// Eficacia de frenado
							df.applyPattern("#0.0");
							df.setMaximumFractionDigits(3);
							parametros.put("eficacia_total",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());// no
// decimal
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_efic_total", df.format(permisible.getValorMinimo()));
							break;
						case 5032:
							df.applyPattern("#0.0");
							df.setMaximumFractionDigits(3);
							parametros.put("des_eje_1", formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());// no
							// decimal
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_deseq", df.format(permisible.getValorMaximo()));
							break;
						case 5033:
							df.applyPattern("#0.0");
							df.setMaximumFractionDigits(3);
							parametros.put("des_eje_2", formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());// no
							// decimal
							break;
						case 5034:
							df.applyPattern("#0.0");
							df.setMaximumFractionDigits(3);
							parametros.put("des_eje_3", formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());// no
							// decimal
							break;
						case 5035:
							df.applyPattern("#0.0");
							df.setMaximumFractionDigits(3);
							parametros.put("des_eje_4", formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());// no
							// decimal
							break;
						case DESEQUILIBRIO_EJE_CINCO:
							df.applyPattern("#0.0");
							df.setMaximumFractionDigits(3);
							parametros.put("des_eje_5", formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());// no
							// decimal
							break;
						case 5036:
							df.applyPattern("#0.0");
							df.setMaximumFractionDigits(3);
							parametros.put("eficacia_auxiliar",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());// un
// decimal
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_efic_aux", df.format(permisible.getValorMinimo()));
							break;
						case EFICACIA_ESTACIONAMIENTO:
							df.applyPattern("#0.0");
							df.setMaximumFractionDigits(3);
							parametros.put("eficacia_auxiliar",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());// un
// decimal
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_efic_aux", df.format(permisible.getValorMinimo()));
							break;
						default:
							break;
						}// end of switch
					} // end of for ista de Medida
				} // end of frenos
				if (p.getTipoPrueba().getTipoPruebaId() == 7) {// pruebas de
					// sonometro sin
					// decimales
					double promedioRuidoPito = 0;
					double promedioRuidoMotor = 0;
					int tipoMedida = -1;
					for (Medida m : medidas) {
						tipoMedida = m.getTipoMedida().getTipoMedidaId();
						if (tipoMedida == 7002) {
							promedioRuidoPito = m.getValor();
						}
						if (tipoMedida == 7005) {
							promedioRuidoMotor = m.getValor();
						}
// promedioRuidoPito /= 3;
						df.applyPattern("#.0#");
						df.setMaximumFractionDigits(1);
						parametros.put("ruido_bocina", formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());// un
						// solo
// decimal
// promedioRuidoMotor /= 3;
						parametros.put("ruido_motor", formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());// un
						// solo
// decimal
					}
				}

				if (p.getTipoPrueba().getTipoPruebaId() == 6) {// pruebas de
					// suspension un
					// decimal
					df.applyPattern("#");// un solo digito no creo no

					for (Medida m : medidas) {
						switch (m.getTipoMedida().getTipoMedidaId()) {
						case 6016:
							parametros.put("susp_delantera_der",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());// sin
// decimales
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_susp", df.format(permisible.getValorMinimo()));
							break;
						case 6017:
							parametros.put("susp_trasera_der",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());// sin
// decimales
							break;
						case 6020:
							parametros.put("susp_delantera_izq",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());// sin
// decimales
							break;
						case 6021:
							parametros.put("susp_trasera_izq",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());// sin
// decimales
							break;
						default:
							break;
						}// end of switch
					} // end of for ista de Medida
				}

				if (p.getTipoPrueba().getTipoPruebaId() == 4) {// prueba de
					// desviacion
					// un decimal
					df.applyPattern("0.0");
					df.setMaximumFractionDigits(1);

					for (Medida m : medidas) {
						switch (m.getTipoMedida().getTipoMedidaId()) {
						case DESVIACION_EJE_UNO:
							parametros.put("desviacion_eje_1",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());// un
// decimal
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_desv", formatearRangoPermisibleDesviacion(permisible, df));
							break;
						case DESVIACION_EJE_DOS:
							parametros.put("desviacion_eje_2",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());// un
// decimal
							break;
						case DESVIACION_EJE_TRES:
							parametros.put("desviacion_eje_3",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());// un
// decimal
							break;
						case DESVIACION_EJE_CUATRO:
							parametros.put("desviacion_eje_4",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());// un
// decimal
							break;
						case DESVIACION_EJE_CINCO:
							parametros.put("desviacion_eje_5",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());// un
// decimal
							break;
						default:
							break;
						}// end of switch
					} // end of for ista de Medida
				} // end if desviacion

				if (p.getTipoPrueba().getTipoPruebaId() == 9) {// prueba de
					// taximetro
					// cuantos
					// decimales
					df.applyPattern("#0.0");
					df.setMaximumFractionDigits(1);
					for (Medida m : medidas) {
						switch (m.getTipoMedida().getTipoMedidaId()) {
						case 9002:
							parametros.put("error_distancia_tax",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());// un
// solo
// decimal
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_taxi", formatearRangoPermisible(permisible, df));
							break;
						case 9003:
							parametros.put("error_tiempo_tax",
									formateadorMedidas.formatearMedida(m) + m.getGeneraDefecto());// un
// solo
// decimal
							permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
									r.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), modelo);
							if (permisible != null)
								parametros.put("permisible_taxi", formatearRangoPermisible(permisible, df));
							break;
						default:
							break;

						}// end of switch
					} // end of for lista de Medida

				} // end if prueba de taximetro
			} // end if de medidas != null
		} // end for
	}// end metodo

	private List<Prueba> listaPruebas(Integer revisionId) {
		return pruebaService.pruebasRevision(revisionId);
	}

	private String formatearRangoPermisible(Permisible permisible, DecimalFormat df) {
		return String.format("[%s-%s]", df.format(permisible.getValorMinimo()), df.format(permisible.getValorMaximo()));
	}

	private String formatearRangoPermisibleDesviacion(Permisible permisible, DecimalFormat df) {
		return String.format("+/- %s", df.format(permisible.getValorMaximo()));
	}
}
