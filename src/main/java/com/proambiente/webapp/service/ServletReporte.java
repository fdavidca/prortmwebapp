package com.proambiente.webapp.service;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;

/**
 * Servlet implementation class ServletReporte
 */
@WebServlet("/ServletReporte")

public class ServletReporte extends HttpServlet {
	
	private static final Logger logger = Logger.getLogger(ServletReporte.class
			.getName());
	
	private static final long serialVersionUID = 1L;
	@Inject
	ReporteService reporteService;
	
	@Inject
	VehiculoService vehiculoService;
       
   
    public ServletReporte() {
        super();       
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
			String revision = request.getParameter("revision");
			if(revision == null || revision.isEmpty())
				throw new RuntimeException("error falta parametro placa");
			String placa = vehiculoService.placaDesdeRevisionId(Integer.parseInt(revision));
			response.addHeader("Content-disposition", "filename="+placa+"-"+revision+".pdf");
			JasperPrint jasperPrintReporte = reporteService.cargarReporte(revision);			
            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setExporterInput( new SimpleExporterInput( jasperPrintReporte ));
            exporter.setExporterOutput( new SimpleOutputStreamExporterOutput( response.getOutputStream() ));
            exporter.exportReport();
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error cargando el reporte", e);
			throw new RuntimeException(e);
		}finally{            
	        response.getOutputStream().flush();
	        response.getOutputStream().close();	        
		}
	}	
		

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
