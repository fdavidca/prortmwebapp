package com.proambiente.webapp.service;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.proambiente.modelo.Alerta;
import com.proambiente.webapp.dao.AlertaFacade;

@Stateless
public class AlertasDeHoyService  extends AlertaFacade {
	
	
	
	
	public List<Alerta> consultarAlertasHoy() {
		Date dateHoy = getStartOfDay(new Date());
		EntityManager em = getEntityManager();
		Query query = em.createQuery("SELECT a FROM Alerta a WHERE a.fecha >:parametro ORDER BY a.alertaId");
		query.setParameter("parametro", new java.sql.Date(dateHoy.getTime()));
		return query.getResultList();
	}

	public Date getStartOfDay(Date date) {
		ZonedDateTime localDateTime = dateToLocalDateTime(date);
		ZonedDateTime startOfDay = localDateTime.toLocalDate().atStartOfDay(ZoneId.systemDefault());

		return localDateTimeToDate(startOfDay);
	}

	private static Date localDateTimeToDate(ZonedDateTime startOfDay) {
		return Date.from(startOfDay.toLocalDateTime().atZone(ZoneId.systemDefault()).toInstant());
	}

	private static ZonedDateTime dateToLocalDateTime(Date date) {
		return ZonedDateTime.ofInstant(Instant.ofEpochMilli(date.getTime()), ZoneId.systemDefault());
	}
	

}
