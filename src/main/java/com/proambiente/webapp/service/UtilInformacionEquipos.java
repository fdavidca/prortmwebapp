package com.proambiente.webapp.service;

import com.proambiente.modelo.Analizador;
import com.proambiente.modelo.Equipo;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.webapp.dao.EquipoFacade;
import com.proambiente.webapp.service.util.UtilTipoCombustible;
import com.proambiente.webapp.util.ConstantesTiposEquipo;
import com.proambiente.webapp.util.dto.EquipoPruebaDTO;
import java.text.DecimalFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UtilInformacionEquipos {
  public static String ponerInformacionEquipos(List<Prueba> pruebas, PruebasService pruebaService, Boolean isMoto, EquipoFacade equipoFacade, Revision r) {
    Boolean isDiesel = Boolean.valueOf(UtilTipoCombustible.isCombustibleDiesel(r.getVehiculo().getTipoCombustible()));
    StringBuilder sb = new StringBuilder();
    for (Prueba p : pruebas) {
      List<Equipo> equipos = pruebaService.obtenerEquipos(p);
      Analizador analizador = p.getAnalizador();
      if (!equipos.isEmpty())
        sb.append(p.getTipoPrueba().getNombre()).append(": "); 
      for (Equipo e : equipos) {
        if (isMoto && isScooter(r) && e.getTipoEquipo().getTipoEquipoId().equals(Integer.valueOf(3)))
          continue; 
        if (!isMoto && isCatalizador(r) && e.getTipoEquipo().getTipoEquipoId().equals(Integer.valueOf(3)) && !isDiesel)
          continue; 
        sb.append(e.getTipoEquipo().getNombreTipoEquipo()).append("-").append(e.getFabricante()).append("-")
          .append(e.getSerieEquipo()).append(" ");
      } 
      DecimalFormat def = new DecimalFormat("0.000");
      if (analizador != null) {
        Date dateFechaRevision = new Date(r.getFechaCreacionRevision().getTime());
        LocalDate ldFechaRevision = asLocalDate(dateFechaRevision);
        LocalDate ldFechaActualizacion = LocalDate.of(2021, 8, 25);
        if (ldFechaRevision.isBefore(ldFechaActualizacion)) {
          sb.append(analizador.getTipo()).append("-").append("serial electronico interno del banco : ")
            .append(analizador.getSerial()).append(" serial del analizador de gases ")
            .append(analizador.getSerialAnalizador()).append(" marca: ").append(analizador.getMarca())
            .append(" ").append(" ");
          if (analizador.getTipo().equalsIgnoreCase("OPACIMETRO")) {
            sb.append("LTOE ");
            if (analizador.getMarca().equalsIgnoreCase("Capelec")) {
              sb.append("215");
              continue;
            } 
            sb.append("430");
            continue;
          } 
          sb.append("PEF ").append(def.format(analizador.getPef()));
          continue;
        } 
        if (analizador.getTipo().equalsIgnoreCase("OPACIMETRO")) {
          sb.append("OPACIMETRO : ");
          sb.append("serial : ").append(analizador.getSerialAnalizador()).append(" marca : ")
            .append(analizador.getMarca());
          sb.append(" LTOE ");
          if (analizador.getMarca().equalsIgnoreCase("Capelec")) {
            sb.append(" 215 ");
            continue;
          } 
          sb.append(" 430 ");
          continue;
        } 
        sb.append(analizador.getTipo()).append("-").append(" serial electronico interno del banco : ")
          .append(analizador.getSerial()).append(" serial del analizador de gases ")
          .append(analizador.getSerialAnalizador()).append(" serial banco de gases ")
          .append(analizador.getSerialBancoGases()).append(" marca  : ")
          .append(analizador.getMarca()).append(" ");
        sb.append("PEF ").append(def.format(analizador.getPef()));
      } 
    } 
    return sb.toString();
  }
  
  public static List<EquipoPruebaDTO> generarListadoEquipos(List<Prueba> pruebas, PruebasService pruebaService, Boolean isMoto, EquipoFacade equipoFacade, Revision r,Boolean isDiesel, boolean pruebaFueEvaluadaDensidad) {
    List<EquipoPruebaDTO> equiposDTO = new ArrayList<>();
    for (Prueba p : pruebas) {
      List<Equipo> equipos = pruebaService.obtenerEquipos(p);
      equipos.stream().forEach(e -> {
    	  
	    	  if (isMoto && isScooter(r) && e.getTipoEquipo().getTipoEquipoId().equals(ConstantesTiposEquipo.SONDA_TEMPERATURA)) {
	    		  
	    	  }else if (!isMoto && isCatalizador(r) && e.getTipoEquipo().getTipoEquipoId().equals(ConstantesTiposEquipo.SONDA_TEMPERATURA) && !isDiesel) {
	    		  
	    	  }else {            
	            
	            String tipoPrueba = p.getTipoPrueba().getNombre();
	            EquipoPruebaDTO epDTO = new EquipoPruebaDTO(e.getModeloEquipo(), e.getFabricante(), e.getSerieEquipo(), tipoPrueba, e.getTipoEquipo().getNombreTipoEquipo());
	            Boolean espiederey = e.getTipoEquipo().getTipoEquipoId().equals(ConstantesTiposEquipo.PIE_DE_REY);
	            if(isDiesel && pruebaFueEvaluadaDensidad && espiederey) {
	            	
	            }else {
	            	equiposDTO.add(epDTO);
	            }
	            
	    	  }
          });
      DecimalFormat def = new DecimalFormat("0.000");
      Analizador analizador = p.getAnalizador();
      if (analizador != null) {
        EquipoPruebaDTO epdto = new EquipoPruebaDTO();
        epdto.setFabricante(analizador.getFabricante());
        StringBuilder sbMarca = new StringBuilder();
        sbMarca.append("Marca:").append(analizador.getMarca());
        sbMarca.append(" Fabricante:").append(analizador.getFabricante());
        epdto.setFabricante(sbMarca.toString());
        epdto.setModeloEquipo(analizador.getModelo());
        epdto.setTipoEquipo(analizador.getTipo());
        epdto.setTipoPrueba(p.getTipoPrueba().getNombre());
        LocalDate ldFechaActualizacion = LocalDate.of(2021, 8, 25);
        Date dateFechaRevision = new Date(r.getFechaCreacionRevision().getTime());
        LocalDate ldFechaRevision = asLocalDate(dateFechaRevision);
        StringBuilder sbSerie = new StringBuilder();
        StringBuilder sbLTOE = new StringBuilder();
        if (ldFechaRevision.isBefore(ldFechaActualizacion)) {
          sbSerie.append(analizador.getTipo()).append("-").append("serie electronico interno del banco : ")
            .append(analizador.getSerial()).append(" serial del analizador de gases ")
            .append(analizador.getSerialAnalizador());
          epdto.setSerieEquipo(sbSerie.toString());
          if (analizador.getTipo().equalsIgnoreCase("OPACIMETRO")) {
            sbLTOE.append("LTOE ");
            if (analizador.getMarca().equalsIgnoreCase("Capelec")) {
              sbLTOE.append("215");
            } else {
              sbLTOE.append("364");
            } 
          } else {
            sbLTOE.append("PEF ").append(def.format(analizador.getPef()));
          } 
          epdto.setLtoe(sbLTOE.toString());
        } else {
          if (analizador.getTipo().equalsIgnoreCase("OPACIMETRO")) {
            sbSerie.append("Serial Opa ");
            sbSerie.append(analizador.getSerialAnalizador());
            sbSerie.append(" serial interno ").append(analizador.getSerial());
            epdto.setSerieEquipo(sbSerie.toString());
            sbLTOE.append(" LTOE ");
            if (analizador.getMarca().equalsIgnoreCase("Capelec")) {
              sbLTOE.append(" 215 ");
            } else {
              sbLTOE.append(" 364");
            } 
          } else {
            sbSerie.append("-").append(" serial electronico interno del banco : ")
              .append(analizador.getSerial()).append(" serial del analizador de gases ")
              .append(analizador.getSerialAnalizador()).append(" serial banco de gases ")
              .append(analizador.getSerialBancoGases());
            epdto.setSerieEquipo(sbSerie.toString());
            sbLTOE.append("PEF ").append(def.format(analizador.getPef()));
          } 
          epdto.setLtoe(sbLTOE.toString());
        } 
        equiposDTO.add(epdto);
      } 
    } 
    return equiposDTO;
  }
  
  public static LocalDate asLocalDate(Date date) {
    return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
  }
  
  public static Boolean isScooter(Revision r) {
    Boolean isScooter = Boolean.FALSE;
    if (r != null) {
      Vehiculo v = r.getVehiculo();
      if (v != null)
        isScooter = Boolean.valueOf((v.getDiseno() != null && v.getDiseno().equalsIgnoreCase("SCOOTER"))); 
    } 
    return isScooter;
  }
  
  public static Boolean isCatalizador(Revision r) {
    Boolean isCatalizador = Boolean.FALSE;
    if (r != null) {
      Vehiculo v = r.getVehiculo();
      if (v.getCatalizador() != null && v.getCatalizador().equalsIgnoreCase("SI"))
        isCatalizador = Boolean.TRUE; 
    } 
    return isCatalizador;
  }
}
