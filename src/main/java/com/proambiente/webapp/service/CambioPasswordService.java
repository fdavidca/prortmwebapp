package com.proambiente.webapp.service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.proambiente.modelo.CambioPassword;
import com.proambiente.modelo.Usuario;
import com.proambiente.webapp.dao.CambioPasswordFacade;
import com.proambiente.webapp.dao.UsuarioFacade;

@Stateless
public class CambioPasswordService {
	private static final Logger logger = Logger.getLogger(CambioPasswordService.class.getName());
	
	@Inject
	UsuarioFacade usuarioFacade;
	
	@Inject
	CambioPasswordFacade cambioPasswordFacade;
	
	public void cambiarPassword(Usuario usuario,String password,String ip) {		
			usuario.setContrasenia( password);			
			CambioPassword cambioPassword = new CambioPassword();
			cambioPassword.setFechaCaducidadAnterior(usuario.getFechaExpiracionContrasenia());
			Date hoy = new Date();
			Timestamp tsHoy = new Timestamp(hoy.getTime());
			cambioPassword.setFechaCambioPassword(tsHoy );	
			cambioPassword.setUsuario(usuario);
			cambioPassword.setDireccionIp(ip);
			usuarioFacade.edit(usuario);
			cambioPasswordFacade.create(cambioPassword);		
	}
	
}
