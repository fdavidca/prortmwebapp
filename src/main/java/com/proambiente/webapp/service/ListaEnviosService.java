package com.proambiente.webapp.service;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Singleton;



@Singleton
public class ListaEnviosService {
	private static final Logger logger = Logger.getLogger(ListaEnviosService.class.getName());
	
	private Set<Integer> listaRevisionesEnProceso = new HashSet<>();
	@RolesAllowed("ADMINISTRADOR")
	public Boolean agregarListaEnvio(Integer revisionId) {
		Boolean agregado = listaRevisionesEnProceso.add(revisionId);
		if(agregado) {
			logger.log(Level.INFO,"Revision agregada: " + revisionId);
		}else {
			logger.log(Level.INFO,"Revision no se agrega: " + revisionId);
		}
		logger.log(Level.INFO,"Tamanio lista" + listaRevisionesEnProceso.size());
		return !agregado;
	}
	
	@RolesAllowed("ADMINISTRADOR")
	public Boolean quitarListaEnvio(Integer revisionId) {
		Boolean removido = listaRevisionesEnProceso.remove(revisionId);
		if(removido) {
			logger.log(Level.INFO,"Revision agregada: " + revisionId);
		}else {
			logger.log(Level.INFO,"Revision no se agrega: " + revisionId);
		}
		return removido;
	}
	
	@RolesAllowed("ADMINISTRADOR")
	public void limpiar() {
		logger.log(Level.INFO," limpiar lista");
		listaRevisionesEnProceso.clear();
	}
	
	@RolesAllowed("ADMINISTRADOR")
	public Set<Integer> getListaEnvios() {
		return listaRevisionesEnProceso;
	}

}
