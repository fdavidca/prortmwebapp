package com.proambiente.webapp.service;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
public class PinCi2Service {

	
	@PersistenceContext
	EntityManager em;
	
	
	public void registrarPinUtilizado(Integer revisionId){
		String consulta = "UPDATE Revision r SET r.pinUtilizado = TRUE WHERE revisionId=:revisionId";
		Query query = em.createQuery(consulta);
		query.setParameter("revisionId", revisionId);
		query.executeUpdate();		
	}
	
	public void registrarPinUtilizado(String pin, Integer revisionId){
		String consulta = "UPDATE Revision r SET r.pin =:pin, r.pinUtilizado = TRUE WHERE revisionId=:revisionId";
		Query query = em.createQuery(consulta);
		query.setParameter("revisionId", revisionId);
		query.setParameter("pin", pin);
		query.executeUpdate();
	}
	
}
