package com.proambiente.webapp.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.TypedQuery;

import com.proambiente.modelo.Analizador;
import com.proambiente.modelo.Defecto;
import com.proambiente.modelo.Equipo;
import com.proambiente.modelo.EstadoDefecto;
import com.proambiente.modelo.Medida;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.TipoPrueba;
import com.proambiente.modelo.VerificacionGasolina;
import com.proambiente.modelo.VerificacionLinealidad;
import com.proambiente.modelo.dto.PruebaDTO;

@Stateless
public class PruebasService {
	
	private static final Integer TIPO_PRUEBA_ID_FOTO = 3;
	private static final Integer TIPO_PRUEBA_ID_VISUAL = 1;

	@PersistenceContext
	EntityManager em;

	public List<Prueba> sugerirPruebas(Integer revisionId) {
		//long numeroPruebasNoTerminadas = numeroPruebasNoTerminadas(revisionId);
		List<Prueba> listaReprobadas = pruebasReprobadas(revisionId);
		List<Prueba> listaSugeridas = new ArrayList<>();
		
		Prueba pruebaFoto = crearPruebaFoto(revisionId);		
		listaSugeridas.add(pruebaFoto);		
		Prueba pruebaVisual = crearPruebaVisual(revisionId);
		listaSugeridas.add(pruebaVisual);		
			
			if (listaReprobadas != null && listaReprobadas.size() > 0) {
				
				for (Prueba p : listaReprobadas) {
					if(p.getTipoPrueba().getTipoPruebaId() == TIPO_PRUEBA_ID_VISUAL){
						continue;
					}
					Prueba nuevaPrueba = new Prueba();
					nuevaPrueba.setTipoPrueba(p.getTipoPrueba());
					nuevaPrueba.setRevision(p.getRevision());
					Date date = new Date();
					Timestamp timestampFechaInicio = new Timestamp(date.getTime());
					nuevaPrueba.setFechaInicio(timestampFechaInicio);
					listaSugeridas.add(nuevaPrueba);
					
				}

				
		}		
		return listaSugeridas;
	}
	
	public Prueba crearPruebaFoto(Integer revisionId){
		Revision revision = em.getReference(Revision.class, revisionId);
		TipoPrueba tipoPrueba = em.getReference(TipoPrueba.class, TIPO_PRUEBA_ID_FOTO);
		Prueba nuevaPrueba = new Prueba();
		nuevaPrueba.setTipoPrueba(tipoPrueba);
		nuevaPrueba.setRevision(revision);
		Date date = new Date();
		Timestamp timestampFechaInicio = new Timestamp(date.getTime());
		nuevaPrueba.setFechaInicio(timestampFechaInicio);
		return nuevaPrueba;
	}
	
	public Prueba crearPruebaVisual(Integer revisionId){
		Revision revision = em.getReference(Revision.class, revisionId);
		TipoPrueba tipoPrueba = em.getReference(TipoPrueba.class, TIPO_PRUEBA_ID_VISUAL);
		Prueba nuevaPrueba = new Prueba();
		nuevaPrueba.setTipoPrueba(tipoPrueba);
		nuevaPrueba.setRevision(revision);
		Date date = new Date();
		Timestamp timestampFechaInicio = new Timestamp(date.getTime());
		nuevaPrueba.setFechaInicio(timestampFechaInicio);
		return nuevaPrueba;
	}
	
	
	public boolean existePruebaNoFinalizada(Integer revisionId,Integer tipoPruebaId){
		String consulta = "SELECT COUNT(p) FROM Prueba p "
				+ "	WHERE p IN (SELECT MAX(pr) FROM Prueba pr "
				+ " WHERE pr.revision.revisionId =:revisionId AND pr.tipoPrueba.tipoPruebaId =:tipoPruebaId ) and p.finalizada = false";
		Query query = em.createQuery(consulta);
		query.setParameter("revisionId", revisionId);
		query.setParameter("tipoPruebaId", tipoPruebaId);
		Long numeroPruebas = (Long)query.getSingleResult();
		return numeroPruebas>0;
	}
	

	public Long numeroPruebasNoTerminadas(Integer revisionId) {
		String consulta = "SELECT COUNT(p) FROM Prueba p "
				+ "	WHERE p IN (SELECT MAX(pr) FROM Prueba pr "
				+ " WHERE pr.revision.revisionId =:revisionId GROUP BY pr.tipoPrueba.tipoPruebaId ) and p.finalizada = false";
		Query query = em.createQuery(consulta);
		query.setParameter("revisionId", revisionId);
		Long numeroPruebasNoTerminadas = (Long) query.getSingleResult();
		return numeroPruebasNoTerminadas;
	}
	
	public Long numeroPruebasTerminadas(Integer revisionId){
		String consulta = "SELECT COUNT(p) FROM Prueba p "
				+ "	WHERE p IN (SELECT MAX(pr) FROM Prueba pr "
				+ " WHERE pr.revision.revisionId =:revisionId GROUP BY pr.tipoPrueba.tipoPruebaId ) and p.finalizada = true";
		Query query = em.createQuery(consulta);
		query.setParameter("revisionId", revisionId);
		Long numeroPruebasNoTerminadas = (Long) query.getSingleResult();
		return numeroPruebasNoTerminadas;
	}

	public List<Prueba> pruebasNoTerminadas(Integer revisionId) {
		String consulta = "SELECT p FROM Prueba p "
				+ "	WHERE p IN (SELECT MAX(pr) FROM Prueba pr "
				+ " WHERE pr.revision.revisionId =:revisionId GROUP BY pr.tipoPrueba.tipoPruebaId ) and p.finalizada = false";
		Query query = em.createQuery(consulta);
		query.setParameter("revisionId", revisionId);
		List<Prueba> listaPruebas = query.<Prueba> getResultList();
		return listaPruebas;

	}
	
	

	public List<Prueba> pruebasReprobadas(Integer revisionId) {
		String consulta = "SELECT p FROM Prueba p "
				+ "WHERE p IN ( "
				+ "SELECT MAX(pr) FROM Prueba pr "
				+ " WHERE pr.revision.revisionId =:revisionId GROUP BY pr.tipoPrueba.tipoPruebaId )"
				+ " and p.finalizada = true and p.aprobada=false";
		Query query = em.createQuery(consulta);
		query.setParameter("revisionId", revisionId);
		List<Prueba> listaPruebas = query.<Prueba> getResultList();
		return listaPruebas;
	}

	public List<Prueba> pruebasRevision(Integer revisionId) {
		List<Integer> idPruebas = consultarIdsPruebasPorRevision(revisionId);		
		String consulta = "SELECT pr FROM Prueba pr JOIN FETCH pr.tipoPrueba  WHERE pr.pruebaId IN :listaIds";				
		Query query = em.createQuery(consulta);
		query.setParameter("listaIds",idPruebas);
		List<Prueba> listaPruebas = query. getResultList();
		return listaPruebas;
	}
	
	
	
	public List<Prueba> pruebasMedidasRevision(Integer revisionId) {
		String consulta = "SELECT pr FROM Prueba pr  JOIN  FETCH pr.medidas WHERE pr.pruebaId IN "
				+ "(SELECT MAX(p.pruebaId)FROM Prueba p WHERE p.revision.revisionId =:revisionId  GROUP BY p.tipoPrueba.tipoPruebaId)";
		Query query = em.createQuery(consulta);
		query.setParameter("revisionId", revisionId);
		List<Prueba> listaPruebas = query. getResultList();
		return listaPruebas;
	}

	
	

	public Revision refrescarRevision(Revision revision) {

		Revision revisionRefrescada = em.find(Revision.class,
				revision.getRevisionId());
		em.refresh(revisionRefrescada);
		return revisionRefrescada;

	}
	
	public List<Medida> obtenerMedidas(Prueba p){
		String consulta = "SELECT m FROM Medida m JOIN FETCH m.tipoMedida WHERE m.prueba.pruebaId=:pruebaId";
		Query query = em.createQuery(consulta);
		query.setParameter("pruebaId", p.getPruebaId());
		List<Medida> medidas = query.getResultList();
		return medidas;
	}
	
	public List<Equipo> obtenerEquipos(Prueba p){
		Prueba prueba = em.getReference(Prueba.class, p.getPruebaId());
		prueba.getEquipos().size();
		return prueba.getEquipos();
	}
	
	
	public List<Defecto> obtenerDefectos(Prueba p){
		Prueba prueba = em.getReference(Prueba.class, p.getPruebaId());
		prueba.getDefectosAsociados().size();
		List<Defecto> defectos = prueba.getDefectosAsociados()
										.stream()
										.filter(d->d.getEstadoDefecto().equals(EstadoDefecto.DETECTADO) || d.getEstadoDefecto().equals(EstadoDefecto.CONDICION_ANORMAL))
										.map(da->da.getDefecto())
										.collect(Collectors.toList());
		return defectos;
	}
	
	public boolean verificarDilucion(Integer revisionId){
		String consulta = "SELECT p FROM Prueba p"
				+ " WHERE p.pruebaId IN "
				+ "(SELECT MAX(pr.pruebaId) "
				+ "FROM Prueba pr "
				+ "WHERE pr.tipoPrueba.tipoPruebaId = 8 "
				+ "AND pr.revision.revisionId =:revisionId)";
		TypedQuery<Prueba> query = em.createQuery(consulta, Prueba.class);
		query.setParameter("revisionId", revisionId);
		try {
			Prueba p = query.getSingleResult();
			String comentario = p.getMotivoAborto();
			if(comentario != null){
				if(comentario.equalsIgnoreCase("DILUCION DE MUESTRA"))
					return true;
					else return false;
			}else {
				return false;
			}
		} catch (NoResultException nre) {
			nre.printStackTrace();
			return false;
		}
	}
	
	public void crearPrueba(Prueba p){
		em.merge(p);
	}
	
	public List<Prueba> consultarPruebasOtto(Date fechaInicial,Date fechaFinal){
		
		String consulta = "SELECT p FROM Prueba p " +
						  "WHERE p.tipoPrueba = 8 " +
						  "AND p.finalizada = true " +
						  "AND p.revision.vehiculo.tipoVehiculo IN (1,2,3)" +
						  "AND p.revision.vehiculo.tipoCombustible IN (1,2,4,9,10) " +
						  "AND p.fechaInicio BETWEEN :fechaInicial AND :fechaFinal";
		TypedQuery<Prueba> query = em.createQuery(consulta, Prueba.class);
		query.setParameter("fechaInicial",fechaInicial);
		query.setParameter("fechaFinal", fechaFinal);
		List<Prueba> pruebas = null;//new ArrayList<Prueba>();
		try{
			pruebas =  query.getResultList();
		}catch(NoResultException nre){
			pruebas = new ArrayList<Prueba>();
		}
		return pruebas;
	}
	
	
	
	public List<Prueba> consultarPruebasRuido(Date fechaInicial,Date fechaFinal){
		
		String consulta = "SELECT p FROM Prueba p " +
						  "WHERE p.tipoPrueba = 7 " +
						  "AND p.finalizada = true " +
						  "AND p.fechaInicio BETWEEN :fechaInicial AND :fechaFinal";
		TypedQuery<Prueba> query = em.createQuery(consulta, Prueba.class);
		query.setParameter("fechaInicial",fechaInicial);
		query.setParameter("fechaFinal", fechaFinal);
		List<Prueba> pruebas = null;//new ArrayList<Prueba>();
		try{
			pruebas =  query.getResultList();
		}catch(NoResultException nre){
			pruebas = new ArrayList<Prueba>();
		}
		return pruebas;
	}
	
	
	public List<Prueba> consultarPruebasOttoMotocicletas(Date fechaInicial,Date fechaFinal){
		
		String consulta = "SELECT DISTINCT(p) FROM Prueba p " +
						  "WHERE p.tipoPrueba = 8 " +
						  "AND p.finalizada=true "+
						  "AND p.revision.vehiculo.tipoVehiculo IN (4,5)" +
						  "AND p.revision.vehiculo.tipoCombustible IN (1,2,4,10) " +
						  "AND p.fechaInicio BETWEEN :fechaInicial AND :fechaFinal";
		TypedQuery<Prueba> query = em.createQuery(consulta, Prueba.class);
		query.setParameter("fechaInicial",fechaInicial);
		query.setParameter("fechaFinal", fechaFinal);
		List<Prueba> pruebas = null;//new ArrayList<Prueba>();
		try{
			pruebas =  query.getResultList();
		}catch(NoResultException nre){
			pruebas = new ArrayList<Prueba>();
		}
		return pruebas;
	}
	
	
	public List<Prueba> consultarPruebasOttoMotocicletasPaginado(Date fechaInicial,Date fechaFinal,int inicio,int tamanioPagina){
		
		String consulta = "SELECT DISTINCT(p) FROM Prueba p " +
						  "WHERE p.tipoPrueba = 8 " +
						  "AND p.finalizada=true "+
						  "AND p.revision.vehiculo.tipoVehiculo IN (4,5) " +
						  "AND p.revision.vehiculo.tipoCombustible IN (1,2,4,10) " +
						  "AND p.fechaInicio BETWEEN :fechaInicial AND :fechaFinal " +
						  "ORDER BY p.pruebaId ";
		TypedQuery<Prueba> query = em.createQuery(consulta, Prueba.class);
		query.setParameter("fechaInicial",fechaInicial);
		query.setParameter("fechaFinal", fechaFinal);
		query.setFirstResult(inicio);
		query.setMaxResults(tamanioPagina);
		List<Prueba> pruebas = null;//new ArrayList<Prueba>();
		try{
			pruebas =  query.getResultList();
		}catch(NoResultException nre){
			pruebas = new ArrayList<Prueba>();
		}
		return pruebas;
	}
	
	
public long contarPruebasOttoMotocicletas(Date fechaInicial,Date fechaFinal){
		
		String consulta = "SELECT COUNT(p.pruebaId) FROM Prueba p " +
						  "WHERE p.tipoPrueba = 8 " +
						  "AND p.finalizada=true "+
						  "AND p.revision.vehiculo.tipoVehiculo IN (4,5) " +
						  "AND p.revision.vehiculo.tipoCombustible IN (1,2,4,10) " +
						  "AND p.fechaInicio BETWEEN :fechaInicial AND :fechaFinal";
		Query query = em.createQuery(consulta, Long.class);
		query.setParameter("fechaInicial",fechaInicial);
		query.setParameter("fechaFinal", fechaFinal);
		Long pruebas = null;//new ArrayList<Prueba>();
		try{
			pruebas =  (Long)query.getSingleResult();
		}catch(NoResultException nre){
			pruebas = 0l;
		}
		return pruebas;
	}
	
	
public List<Prueba> consultarPruebasDiesel(Date fechaInicial,Date fechaFinal){
		
		String consulta = "SELECT DISTINCT(p) FROM Prueba p " +
						  "WHERE p.tipoPrueba = 8 " +
						  "and p.finalizada = true " +
						  "AND p.revision.vehiculo.tipoVehiculo IN (1,2,3)" +
						  "AND p.revision.vehiculo.tipoCombustible IN (3,8,11) " +
						  "AND p.fechaInicio BETWEEN :fechaInicial AND :fechaFinal";
		TypedQuery<Prueba> query = em.createQuery(consulta, Prueba.class);
		query.setParameter("fechaInicial",fechaInicial);
		query.setParameter("fechaFinal", fechaFinal);
		List<Prueba> pruebas = null;//new ArrayList<Prueba>();
		try{
			pruebas =  query.getResultList();
		}catch(NoResultException nre){
			pruebas = new ArrayList<Prueba>();
		}
		return pruebas;
	}
	
	
	public Analizador consultarAnalizadorPrueba(Prueba prueba){		
		Prueba p = em.find(Prueba.class,prueba.getPruebaId() );
		return p.getAnalizador();
	}
	
	public VerificacionGasolina consultarVerificacionPrueba(Prueba prueba){
		Prueba p = em.find(Prueba.class, prueba.getPruebaId());
		return p.getVerificacionGasolina();
	}
	
	public VerificacionLinealidad consultarVerificacionLinealidad(Prueba prueba){
		Prueba p = em.find(Prueba.class, prueba.getPruebaId());
		return p.getVerificacionLinealidad();
	}
	
	public void registrarFechaInicioPrueba(Integer pruebaId){
		if(pruebaId == null){
			throw new IllegalArgumentException("pruebaId no debe ser null");
		}
		
		 StoredProcedureQuery storedProcedureQuery = em.createStoredProcedureQuery("registrar_fecha_inicio_prueba");
		 storedProcedureQuery.registerStoredProcedureParameter(1, Integer.class, ParameterMode.IN);
		 storedProcedureQuery.setParameter(1, pruebaId);
		 storedProcedureQuery.getSingleResult();		 
	}
	
	public List<Integer> consultarIdsPruebasPorRevision(Integer revisionId){
		String subConsulta = "SELECT MAX(pr.pruebaId) FROM Prueba pr WHERE pr.revision.revisionId =:revisionId GROUP BY pr.tipoPrueba";
		Query queryPruebas = em.createQuery(subConsulta);
		queryPruebas.setParameter("revisionId", revisionId);
		List<Integer> idPruebas = queryPruebas.getResultList();
		return idPruebas;
	}
	
	public PruebaDTO consultarInfoPruebaDTO(Integer pruebaId){
		String consulta = "SELECT "
							+ "	NEW com.proambiente.modelo.dto.PruebaDTO("
							+ "p.pruebaId,"
							+ "p.usuario.usuarioId,"
							+ "p.motivoAborto,"
							+ "p.causaAborto.causaAbortoId,"
							+ "p.revision.revisionId,"
							+ "p.tipoPrueba.tipoPruebaId,"
							+ "p.revision.vehiculo.vehiculoId) "
							+ "FROM Prueba p "
							+" WHERE p.pruebaId =:pruebaId";
		
		Query query = em.createQuery(consulta);
		query.setParameter("pruebaId",pruebaId);
		PruebaDTO pdto = (PruebaDTO)query.getSingleResult();
		return pdto;		
	}
	
}
