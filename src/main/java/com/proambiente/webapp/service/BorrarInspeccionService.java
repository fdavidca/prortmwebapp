package com.proambiente.webapp.service;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;

@Stateless
public class BorrarInspeccionService {
	
	@PersistenceContext
	EntityManager em;

	@RolesAllowed("ADMINISTRADOR")
	public Integer borrarInspeccion(Integer revisionId,String placa) {
		if(revisionId == null) {
			throw new IllegalArgumentException("Especifique id de revision para borrar");
		}
		if(placa == null || placa.isEmpty()) {
			throw new IllegalArgumentException("Especifique placa para borrar");
		}
		
		
		StoredProcedureQuery storedProcedureQuery = em.createStoredProcedureQuery("borrar_inspeccion");
		storedProcedureQuery.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
		storedProcedureQuery.registerStoredProcedureParameter(2, Integer.class, ParameterMode.IN);
		storedProcedureQuery.setParameter(2, revisionId);
		storedProcedureQuery.setParameter(1,placa);
		// storedProcedureQuery.execute();
		Object resultado = storedProcedureQuery.getSingleResult();
		Integer o = (Integer) resultado;
		return o;
	}

}
