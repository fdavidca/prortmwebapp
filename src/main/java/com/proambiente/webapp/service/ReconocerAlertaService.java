package com.proambiente.webapp.service;

import javax.ejb.Stateless;

import com.proambiente.modelo.Alerta;
import com.proambiente.webapp.dao.AlertaFacade;

@Stateless
public class ReconocerAlertaService  extends AlertaFacade {
	
	
	
	
	public void reconocerAlerta(Integer alertaId) {
		Alerta alerta = find(alertaId);
		alerta.setReconocida(true);
		em.merge(alerta);
	}
	

}
