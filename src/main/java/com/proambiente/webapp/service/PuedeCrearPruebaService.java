package com.proambiente.webapp.service;

import java.util.List;
import java.util.Optional;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.proambiente.modelo.Inspeccion;
import com.proambiente.modelo.Prueba;

@Stateless
public class PuedeCrearPruebaService {
	
	private static final Integer INSPECCION_SENSORIAL = 1;

	private static final Integer FOTO = 3;

	@Inject
	InspeccionService inspeccionService;
	
	@Inject
	PruebasService pruebaService;
	
	
	/**
	 * 
	 * 
	 * 
	 * @param sbError
	 * @param revisionId
	 * @param tipoPruebaId
	 * @return
	 */
	public Boolean puedeCrearPruebaService(StringBuilder sbError,Integer revisionId,Integer tipoPruebaId) {
		
		if(sbError == null) {
			sbError= new StringBuilder();
		}
		Boolean existePruebaNoFinalizada = pruebaService.existePruebaNoFinalizada(revisionId,tipoPruebaId);
		if(existePruebaNoFinalizada) {
			sbError.append("Existe prueba no finalizada");
			return Boolean.FALSE;
		}else {
			List<Inspeccion> inspecciones = inspeccionService.obtenerInspeccionesPorRevisionId(revisionId);
			if(inspecciones == null || inspecciones.size()==0) {
				return Boolean.TRUE;
			}else if(inspecciones.size() > 1) {
				sbError.append("La revision ya tiene dos intentos - o  inspecciones -");
				return Boolean.FALSE;
			}else {
				Inspeccion inspeccion = inspecciones.get(0);
				if(inspeccion.getAprobada()) {
					sbError.append("La revision tiene inspeccion aprobada");
					return Boolean.FALSE;
				}else {
					List<Prueba> pruebas = inspeccionService.pruebasInspeccion(inspeccion.getInspeccionId());
					if(contienePruebaAprobada(pruebas,tipoPruebaId)&& !( tipoPruebaId.equals(INSPECCION_SENSORIAL) || tipoPruebaId.equals(FOTO) ) ) {
						sbError.append("La revision ya tiene prueba aprobada de ese tipo");
						return Boolean.FALSE;
					}
					else {
						return Boolean.TRUE;
					}
				}
			}
			
		}		
	}
	
	
	public Boolean contienePruebaAprobada(List<Prueba> pruebas ,Integer tipoPruebaId) {
		Optional<Prueba> pruebaOpt = pruebas.stream().filter(p->p.getTipoPrueba().getTipoPruebaId().equals(tipoPruebaId)).findAny();
		if(pruebaOpt.isPresent()) {
			Prueba p = pruebaOpt.get();
			if (p.isAprobada()) {
				return Boolean.TRUE;
			}else {
				return Boolean.FALSE;
			}
		}else {
			return Boolean.FALSE;
		}
		
	}

}
