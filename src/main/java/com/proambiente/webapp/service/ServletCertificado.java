package com.proambiente.webapp.service;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;

/**
 * Servlet implementation class ServletCertificado
 */
@WebServlet("/ServletCertificado")
public class ServletCertificado extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static final Logger logger = Logger
			.getLogger(ServletCertificado.class.getName());
	
	@Inject
	LoadCertificadoService loadCertificadoService;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletCertificado() {
        super();        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			String revision = request.getParameter("revision");
			if(revision == null || revision.isEmpty())
				throw new RuntimeException("error falta parametro placa");
			Integer revision_id = Integer.parseInt(revision);
			JasperPrint jasperPrintReporte = loadCertificadoService.cargarCertificado(revision_id);			
            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrintReporte);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
            exporter.exportReport();
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error cargando certificado", e);
			throw new RuntimeException(e);
		}finally{            
	        response.getOutputStream().flush();
	        response.getOutputStream().close();	        
		}		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
