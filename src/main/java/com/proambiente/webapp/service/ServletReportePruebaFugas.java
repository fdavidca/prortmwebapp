package com.proambiente.webapp.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.knowm.xchart.BitmapEncoder;
import org.knowm.xchart.QuickChart;
import org.knowm.xchart.XYChart;

import com.proambiente.modelo.Analizador;
import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.PruebaFuga;
import com.proambiente.modelo.Usuario;
import com.proambiente.modelo.dto.Muestra;
import com.proambiente.webapp.dao.AnalizadorFacade;
import com.proambiente.webapp.dao.InformacionCdaFacade;
import com.proambiente.webapp.dao.PruebaFugaFacade;
import com.proambiente.webapp.dao.UsuarioFacade;
import com.proambiente.webapp.util.MuestrasJsonCodificador;
import com.proambiente.webapp.util.dto.MuestraFugasDTO;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;

/**
 * Servlet implementation class ServletReporte
 */
@WebServlet("/ServletPruebaFugas")

public class ServletReportePruebaFugas extends HttpServlet {

	private static final Logger logger = Logger.getLogger(ServletReportePruebaFugas.class.getName());

	private static final long serialVersionUID = 1L;

	@Inject
	PruebaFugaFacade pruebaFugaDAO;
	
	@Inject
	InformacionCdaFacade informacionCdaDAO;	
	
	@Inject
	AnalizadorFacade analizadorDAO;
	
	@Inject
	UsuarioFacade usuarioDAO;

	public ServletReportePruebaFugas() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			String idPruebaFugas = request.getParameter("id_prueba_fugas");
			if (idPruebaFugas == null || idPruebaFugas.isEmpty())
				throw new RuntimeException("error falta parametro de prueba de fugas");
			Integer pruebaFugaId = Integer.valueOf(idPruebaFugas);
			PruebaFuga pruebaFuga = pruebaFugaDAO.find(pruebaFugaId);
			response.setContentType("application/pdf");
			
			JasperPrint reporte = cargarReporteFugas(pruebaFuga);
			JRPdfExporter exporter = new JRPdfExporter();
            exporter.setExporterInput(new SimpleExporterInput(reporte));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));
			exporter.exportReport();
			
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error cargando el reporte", e);
			throw new RuntimeException(e);
		} finally {
			response.getOutputStream().flush();
			response.getOutputStream().close();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}
	
	public BufferedImage crearGrafico(List<Muestra> muestras){
		double[] datosX = new double[muestras.size()];
		double[] datosY = new double[muestras.size()];
		int i = 0;
		for(Muestra m : muestras){
			datosX[i] = i*1.0;
			datosY[i] = m.getValor();
			i++;
		}
		XYChart chart = QuickChart.getChart("Presion", "", "Presion mbar", "Presion", datosX, datosY);
		return BitmapEncoder.getBufferedImage(chart);
	}

	
	
	
	public JasperPrint cargarReporteFugas(PruebaFuga pf) throws Exception{
		InputStream is = null;
		JasperPrint reporte = null;
		Map<String,Object> parametros = new HashMap<>();
		try{
			is = this
					.getClass()
					.getClassLoader()
					.getResourceAsStream(
							"com/proambiente/webapp/reporte/reporte_autoregulacion_fugas.jasper");
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
			SimpleDateFormat sdfs = new SimpleDateFormat("yyyy-MM-dd");
			DecimalFormat df = new DecimalFormat("0.00");
			DecimalFormat dfSinDecimales = new DecimalFormat("#");
			
			parametros.put("numero_prueba",String.valueOf( pf.getPruebaFugasId() ));
			
			InformacionCda infoCda = informacionCdaDAO.find(1);
			
			if(infoCda.getLogo() != null){
		       	 ByteArrayInputStream bis = new ByteArrayInputStream(infoCda.getLogo());
		            parametros.put("logo", bis);
		    }
			
			parametros.put("numero_establecimiento", infoCda.getNumeroCda());
			parametros.put("nombre_organizacion", infoCda.getNombre() );
			parametros.put("NIT",infoCda.getNit());
			parametros.put("direccion", infoCda.getDireccion());
			parametros.put("telefono",infoCda.getTelefono1());
			parametros.put("telefono2", infoCda.getTelefono2());
			parametros.put("ciudad_establecimiento", infoCda.getCiudad());
			parametros.put("resolucion",infoCda.getNumeroResolucion());
			parametros.put( "fecha_resolucion", sdfs.format( infoCda.getFechaResolucion() ) );
			
			Analizador analizador = analizadorDAO.find(pf.getSerialEquipo());
			
			if(analizador != null){
				parametros.put("marca_equipo", analizador.getMarca());
				parametros.put("modelo_equipo", analizador.getModelo());
				parametros.put("serie_equipo", analizador.getSerial());
			}
			
			String nombreSoftware = "PRO RTM";
			parametros.put("nombre_software", nombreSoftware);
			String versionSoftware = infoCda.getVersionSoftware();
			parametros.put("version_software", versionSoftware ); 
			
			//resultado
			String presionInicial = dfSinDecimales.format( pf.getPresionInicial() ) ;
			String presionFinal = dfSinDecimales.format( pf.getPresionFinal() ) ;
			String resultado = pf.isAprobada() ? "APROBADA" : "REPROBADA";
			parametros.put("resultado", resultado);
			
			parametros.put("presion_inicial", presionInicial+" mbar" );
			parametros.put("presion_final", presionFinal+ " mbar " );
			
			String humedadRelativa = dfSinDecimales.format(pf.getHumedadRelativa());
			parametros.put("humedad_relativa", humedadRelativa + " %" );
			
			String temperaturaAmbiente = dfSinDecimales.format( pf.getTemperaturaAmbiente() );
			parametros.put("temperatura_ambiente", temperaturaAmbiente + " C");
			
			
			Usuario usuario = usuarioDAO.find(pf.getUsuarioId());
			StringBuilder sb = new StringBuilder();
			sb.append(usuario.getNombres()).append("  ").append(usuario.getApellidos())
			.append("-").append(usuario.getCedula().toString());
			parametros.put("usuario_fugas",sb.toString());
			
			parametros.put("fecha_verificacion", sdf.format(pf.getFechaRealizacion()));
			
			List<Muestra> muestras = MuestrasJsonCodificador.decodificarListaMuestras(pf.getMuestras());
			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy MM dd HH:mm:ss");
			List<MuestraFugasDTO> muestrasDTO = muestras.stream().map(m-> new MuestraFugasDTO(sdf1.format(m.getFecha()) ,m.getValor())  ).collect(Collectors.toList());
			
			
			BufferedImage chart = crearGrafico(muestras);
			parametros.put("chart", chart);
			
			JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(muestrasDTO);
			parametros.put("ds1", ds);
			reporte = JasperFillManager.fillReport(is, parametros, new JREmptyDataSource());
			
			
			return reporte;
			
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error generando reporte de prueba de linealidad", exc);
			throw exc;
		} finally{
			try{
				is.close();
			}catch(Exception exc){
				logger.log(Level.SEVERE, "error", exc);
			}
		}
	}
	
	


}
