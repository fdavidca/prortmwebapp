package com.proambiente.webapp.service;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.imageio.ImageIO;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.proambiente.modelo.LogoOnac;
import com.proambiente.webapp.dao.LogoOnacFacade;


/**
 * Servlet implementation class ServletFotos
 */
@WebServlet("/ServletLogoOnac")
public class ServletLogoOnac extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Inject
	LogoOnacFacade logoOnacDAO;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletLogoOnac() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String logoOnacIdStr = request.getParameter("logo_onac_id");

		if (logoOnacIdStr != null) {
			Long logoOnacId = Long.parseLong(logoOnacIdStr);

			LogoOnac logo = logoOnacDAO.find(logoOnacId);

			if (logo != null && logo.getImagen() != null) {
				BufferedImage originalImage = ImageIO.read(new ByteArrayInputStream(logo.getImagen()));
				OutputStream out = response.getOutputStream();
				response.setContentType("image/png");
				ImageIO.write(originalImage, "png", out);
				out.close();
			}
			else {
				BufferedImage bi= new BufferedImage(320,240,BufferedImage.TYPE_BYTE_GRAY);
				Graphics gc = bi.getGraphics();
				gc.setColor(Color.WHITE);
				gc.fillRect(0,0,320,240);
				OutputStream out = response.getOutputStream();
				response.setContentType("image/png");
				ImageIO.write(bi, "png", out);
				out.close();
			}

		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
