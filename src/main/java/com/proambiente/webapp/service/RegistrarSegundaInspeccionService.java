package com.proambiente.webapp.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.proambiente.modelo.Inspeccion;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Revision;
import com.proambiente.webapp.dao.InspeccionFacade;


@Stateless
public class RegistrarSegundaInspeccionService {
	
	@Inject
	InspeccionFacade inspeccionDAO;
	
	@PersistenceContext
	EntityManager em;
	
	@Inject
	PruebasService pruebaService;
	
	@Inject
	PersistirPruebaService persistirPruebaService;
	
	@Inject
	RevisionService revisionService;
	
	
	
	
	
	public void registrarPrimeraInspeccion(Inspeccion inspeccion,Revision revision,Integer numeroIntentos){
		inspeccionDAO.create(inspeccion);
		
		//crear las pruebas nuevas para la reinspeccion
		List<Prueba> listaSugeridas = pruebaService.sugerirPruebas(revision.getRevisionId());
		for(Prueba p:listaSugeridas){
			revision.getPruebas().add(p);
		}
		persistirPruebaService.persistirPruebas(revision.getPruebas(), revision);
		revision.setComentario("");
		revision.setNumeroInspecciones(numeroIntentos+1);
		revision.setPrimerEnvioRealizado(false);
		revision.setSegundoEnvioRealizado(false);
		revisionService.editarRevision(revision);
		
	}
	
}
