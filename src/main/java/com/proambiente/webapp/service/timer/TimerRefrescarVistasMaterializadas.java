package com.proambiente.webapp.service.timer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

@Singleton
@Startup
public class TimerRefrescarVistasMaterializadas {

	
	@PersistenceContext
	EntityManager em;
	
	@Resource(lookup = "java:/jboss/datasources/PrortmDS")
	DataSource dataSource;
	
    public  TimerRefrescarVistasMaterializadas() {
		
	}

    @Schedule(second = "0", minute = "0", hour = "7,9,11,13,15,17",persistent=false)
    public void actualizarVistasDuracionRevision() {
    	Connection c = null ;
    	try { 
    		
    		c = dataSource.getConnection();
    		PreparedStatement ps = c.prepareStatement("REFRESH MATERIALIZED VIEW bdprortm.res_autom_duracion_inspeccion ");
    		ps.executeUpdate();
    		ps.close();
    		
    	}catch(Exception exc) {
    		exc.printStackTrace();
    	}finally {
    		if (c != null)
				try {
					c.close();
				} catch (SQLException e) {					
					e.printStackTrace();
				}
    	}   	
    }
    
    @Schedule(second = "0", minute = "15", hour = "7,9,11,13,15,17",persistent=false)
    public void actualizarVistasOtrasMedidas() {
    	Connection c = null ;
    	try { 
    		
    		c = dataSource.getConnection();
    		PreparedStatement psMedidasIv = c.prepareStatement("REFRESH MATERIALIZED VIEW bdprortm.res_autom_iv_medidas");
    		psMedidasIv.executeUpdate();
    		psMedidasIv.close();
    		
    		
    		PreparedStatement psMedidasSonometro = c.prepareStatement("REFRESH MATERIALIZED VIEW bdprortm.res_autom_sonometro_medidas");
    		psMedidasSonometro.executeUpdate();
    		psMedidasSonometro.close();
    		
    		PreparedStatement psMedidasTaximetro = c.prepareStatement("REFRESH MATERIALIZED VIEW bdprortm.res_autom_taximetro_medidas");
    		psMedidasTaximetro.executeUpdate();
    		psMedidasTaximetro.close();
    		
    		PreparedStatement psMedidasSuspension = c.prepareStatement("REFRESH MATERIALIZED VIEW bdprortm.res_autom_suspension_medidas");
    		psMedidasSuspension.executeUpdate();
    		psMedidasSuspension.close();
    		
    		PreparedStatement psMedidasAlineacion = c.prepareStatement("REFRESH MATERIALIZED VIEW bdprortm.res_autom_alineacion_medidas");
    		psMedidasAlineacion.executeUpdate();
    		psMedidasAlineacion.close();
    		
    	}catch(Exception exc) {
    		exc.printStackTrace();
    	}finally {
    		if (c != null)
				try {
					c.close();
				} catch (SQLException e) {					
					e.printStackTrace();
				}
    	}   	
    }
    
    @Schedule(second = "0", minute = "25", hour = "7,9,11,13,15,17",persistent=false)
    public void actualizarVistasFrenos() {
    	Connection c = null ;
    	try { 
    		
    		c = dataSource.getConnection();
    		PreparedStatement psFrenosLivPesados = c.prepareStatement("REFRESH MATERIALIZED VIEW bdprortm.res_autom_frenos_livianos_pesado");
    		psFrenosLivPesados.executeUpdate();
    		psFrenosLivPesados.close();
    		
    		PreparedStatement ps = c.prepareStatement("REFRESH MATERIALIZED VIEW bdprortm.res_autom_frenos_moto");
    		ps.executeUpdate();
    		ps.close();
    		
    		
    	}catch(Exception exc) {
    		exc.printStackTrace();
    	}finally {
    		if (c != null)
				try {
					c.close();
				} catch (SQLException e) {					
					e.printStackTrace();
				}
    	}   	
    }
    
    @Schedule(second = "0", minute = "35", hour = "7,9,11,13,15,17",persistent=false)
    public void actualizarVistasLuces() {
    	Connection c = null ;
    	try { 
    		
    		c = dataSource.getConnection();
    		PreparedStatement psLucesCarros = c.prepareStatement("REFRESH MATERIALIZED VIEW bdprortm.res_autom_luces_medidas");
    		psLucesCarros.executeUpdate();
    		psLucesCarros.close();
    		
    		PreparedStatement psLucesMotos = c.prepareStatement("REFRESH MATERIALIZED VIEW bdprortm.res_autom_luces_moto");
    		psLucesMotos.executeUpdate();
    		psLucesMotos.close();		
    		
    		
    	}catch(Exception exc) {
    		exc.printStackTrace();
    	}finally {
    		if (c != null)
				try {
					c.close();
				} catch (SQLException e) {					
					e.printStackTrace();
				}
    	}   	
    }

    @Schedule(second = "0", minute = "40", hour = "7,9,11,13,15,17",persistent=false)
    public void actualizarVistaResultadoGases() {
    	Connection c = null ;
    	try { 
    		c = dataSource.getConnection();
    		PreparedStatement psGasesLivianosGasolina = c.prepareStatement("REFRESH MATERIALIZED VIEW bdprortm.res_autom_gases_livianos_pesados_gasolina ");
    		psGasesLivianosGasolina.executeUpdate();
    		psGasesLivianosGasolina.close();
    		
    		PreparedStatement psGasesDiesel = c.prepareStatement("REFRESH MATERIALIZED VIEW bdprortm.res_autom_gases_diesel ");
    		psGasesDiesel.executeUpdate();
    		psGasesDiesel.close();
    		
    		PreparedStatement psGasesMoto = c.prepareStatement("REFRESH MATERIALIZED VIEW bdprortm.res_autom_gases_moto ");
    		psGasesMoto.executeUpdate();
    		psGasesMoto.close();
    		

    		
    	}catch(Exception exc) {
    		exc.printStackTrace();
    	}finally {
    		if (c != null)
				try {
					c.close();
				} catch (SQLException e) {					
					e.printStackTrace();
				}
    	}   	
    }
}
