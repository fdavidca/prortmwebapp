package com.proambiente.webapp.service;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;

@Stateless
public class RolesService {
	
	@RolesAllowed("ADMINISTRADOR")
	public void checkAdministrador() {
		System.out.println(" Rol administrador requerido");
	}
	
}
