package com.proambiente.webapp.service;

import java.io.Serializable;
import java.sql.Date;
import java.util.Calendar;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Named
public class FechaService implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3567079198270784595L;
	@PersistenceContext
	EntityManager em;
	
	public Calendar obtenerInicioHoy(){
	    Query query = em.createNativeQuery("SELECT CURRENT_DATE");
		Date d = (Date)query.getSingleResult();
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(d.getTime());
		return c;		
	}

}
