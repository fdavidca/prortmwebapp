package com.proambiente.webapp.service.sicov;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import com.proambiente.modelo.dto.PruebaDTO;
import com.proambiente.webapp.service.InspeccionService;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.service.guardarpdf.GuardarPdfCallback;

public class OperadorSicovDummy implements OperacionesSicov {
	
	@Inject
	RevisionService revisionService;
	
	@Inject
	InspeccionService inspeccionService;

	
	private static final Logger logger = Logger.getLogger(OperadorSicovDummy.class.getName());
	
	@Override
	public void registrarFUR(Integer revisionId, Integer numeroInspeccion,Envio envio,GuardarPdfCallback guardarPdfCallback) {
		
		if(envio.equals(Envio.PRIMER_ENVIO)) {
			revisionService.registrarPrimerEnvio(revisionId);
		}else if(envio.equals(Envio.SEGUNDO_ENVIO)) {
			revisionService.registrarSegundoEnvio(revisionId);
		}
		logger.log(Level.INFO, "registrarFur Dummmy");
	}

	@Override
	public void registrarEventoRTMEC(PruebaDTO prueba, String placa, Date fechaRevision, String serialesEquipos,
			String causaAborto, Integer tipoEventoPrueba, EtapaRevision etapaRevision) {
		
		logger.log(Level.INFO, "RegistrarEventoRTMEC Dummy");

	}

	@Override
	public String consultarPinPlaca(String placa) {
		
		logger.log(Level.INFO, "consultar Pin Placa Dummy");
		return "123456789876";
	}

	@Override
	public void enviarEco(String mensaje) {
		
		logger.log(Level.INFO, "Enviar eco Dummy");

	}
	
	

	@Override
	public void utilizarPin(String pin, String tipoRtm, String placa) {
		
		logger.log(Level.INFO, "Utilizar pin Dummy");		
	}

	@Override
	public void registrarFURInspeccionPrimerEnvio(Integer inspeccionId) {
		
		Integer revisionId = inspeccionService.revisionIdDesdeInspeccionId(inspeccionId);
		revisionService.registrarPrimerEnvio(revisionId);
		logger.log(Level.INFO, "registrarFur inspeccion Dummmy");
	}

	@Override
	public void registrarFURInspeccionSegundoEnvio(Integer inspeccionId) {
		Integer revisionId = inspeccionService.revisionIdDesdeInspeccionId(inspeccionId);
		revisionService.registrarPrimerEnvio(revisionId);
		logger.log(Level.INFO,"registrar fur inspeccion segundo envio dummy");
	}

	@Override
	public void registrarInformacionRunt(Integer revisionId,GuardarPdfCallback guardarPdf) {
		logger.log(Level.INFO,"registrar infomracion runt dummy");
		
	}

}
