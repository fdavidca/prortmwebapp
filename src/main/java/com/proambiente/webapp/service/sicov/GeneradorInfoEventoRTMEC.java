package com.proambiente.webapp.service.sicov;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import com.proambiente.modelo.Analizador;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Revision_;
import com.proambiente.modelo.dto.EquipoDTO;
import com.proambiente.modelo.dto.PruebaDTO;
import com.proambiente.webapp.dao.AnalizadorFacade;
import com.proambiente.webapp.dao.EquipoFacade;
import com.proambiente.webapp.service.PruebasService;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.service.VehiculoService;
import com.proambiente.webapp.service.indra.UtilIndraEquipos;
import com.proambiente.webapp.util.ConstantesIndra;
import com.proambiente.webapp.util.ConstantesTiposPrueba;

public class GeneradorInfoEventoRTMEC {
	
	
	private static final Logger logger = Logger.getLogger(GeneradorInfoEventoRTMEC.class.getName());
	
	OperacionesSicov operacionesSicov;
	
	@Inject
	PruebasService pruebaService;
	
	@Inject
	EquipoFacade equipoService;
	
	@Inject
	VehiculoService vehiculoService;
	
	@Inject
	RevisionService revisionService;
	
	@Inject
	UtilIndraEquipos utilIndraEquipos;
	
	@Inject
	AnalizadorFacade analizadorService;
	
	public void generarInfoEventoRTMEC(Prueba prueba,List<String> serialesEquipos){
		
			try {
				
				
				
				PruebaDTO pruebaDTO = pruebaService.consultarInfoPruebaDTO(prueba.getPruebaId());
				if(pruebaDTO.getTipoPruebaId().equals(ConstantesTiposPrueba.TIPO_PRUEBA_FOTO) || pruebaDTO.getTipoPruebaId().equals(ConstantesTiposPrueba.TIPO_PRUEBA_RUIDO)) {//no se registra evento de prueba de foto
					return;
				}
				Revision revision = revisionService.cargarRevisionDTO(pruebaDTO.getRevisionId(),
						Revision_.fechaCreacionRevision,Revision_.preventiva);
				if(revision.getPreventiva()){//no registra el evento si la revision es preventiva
					return ;
				}
				Analizador analizador = prueba.getAnalizador();
				String serialEquipo = "";
				if(analizador != null){					
					serialEquipo = analizador.getSerial();
					Analizador analizadorRegistrado = analizadorService.find(serialEquipo);
					serialEquipo = analizadorRegistrado.getSerialAnalizador();
				} else {
					if(serialesEquipos != null && !serialesEquipos.isEmpty()){
						List<EquipoDTO> equipos = equipoService.consultarEquiposPorIds(serialesEquipos);
						Optional<EquipoDTO> optEquipo = utilIndraEquipos.buscarEquipoPrincipalDePrueba(equipos,pruebaDTO.getTipoPruebaId());
						serialEquipo = optEquipo.isPresent() ? optEquipo.get().getEquipoId() : "";
					}
				}
				
				Long numPruebasNoTerminadas = pruebaService.numeroPruebasNoTerminadas(pruebaDTO.getRevisionId());
				EtapaRevision etapaRevision;
				Integer tipoEvento = -1;
				if(prueba.isAbortada() && prueba.isFinalizada()){
					tipoEvento = ConstantesIndra.ABORTO_PRUEBA;
				}
				if(prueba.isFinalizada() && !prueba.isAbortada()){
					tipoEvento = ConstantesIndra.FINALIZACION_PRUEBA;
				}
				if(!prueba.isFinalizada()){
					tipoEvento = ConstantesIndra.INICIO_PRUEBA;
				}
				
				String placa = vehiculoService.placaDesdePruebaId(pruebaDTO.getPruebaId());
				
				Date fechaCreacionRevision = new Date(revision.getFechaCreacionRevision().getTime());
				if (numPruebasNoTerminadas.equals(0l)) {
					etapaRevision = EtapaRevision.TODAS_PRUEBAS_FINALIZADAS;
				} else {
					etapaRevision = EtapaRevision.REVISION_EN_PROCESO;
				}
				String causaAborto = "";
				Integer causaAbortoId = null;
				if (prueba.getCausaAborto() != null) {
					causaAborto =prueba.getCausaAborto().getMotivo();
					causaAbortoId = prueba.getCausaAborto().getCausaAbortoId();
				}			
				
				operacionesSicov.registrarEventoRTMEC(pruebaDTO, placa, fechaCreacionRevision, serialEquipo, causaAborto,
						tipoEvento, etapaRevision);
			} catch (Exception exc) {
				logger.log(Level.SEVERE, "Error", exc);
			}
		
	}

	public OperacionesSicov getOperacionesSicov() {
		return operacionesSicov;
	}

	public void setOperacionesSicov(OperacionesSicov operacionesSicov) {
		this.operacionesSicov = operacionesSicov;
	}
	
	

}
