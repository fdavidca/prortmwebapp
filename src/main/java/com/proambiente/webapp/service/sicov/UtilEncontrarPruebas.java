package com.proambiente.webapp.service.sicov;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import com.proambiente.modelo.Prueba;

public class UtilEncontrarPruebas {
	public static Optional<Prueba> encontrarPruebaPorTipo(List<Prueba> listaPruebas,Integer tipoPrueba){
		Stream<Prueba> streamPruebas = listaPruebas.stream();//importante crear nuevo stream cada vez que se invoca el metodo
		Optional<Prueba> optPrueba = streamPruebas.filter(prueba -> prueba.getTipoPrueba().getTipoPruebaId().equals(tipoPrueba))
					 .findFirst();
		return optPrueba;
	}
}
