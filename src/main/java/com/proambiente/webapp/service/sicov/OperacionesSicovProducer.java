package com.proambiente.webapp.service.sicov;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.OperadorSicov;
import com.proambiente.webapp.dao.InformacionCdaFacade;


import com.proambiente.webapp.service.ci2.OperacionesSicovCi2V3;
import com.proambiente.webapp.service.indra.OperacionesSicovIndraV2;

@ApplicationScoped
public class OperacionesSicovProducer {
	
	
	private static final Logger logger = Logger.getLogger(OperacionesSicovProducer.class.getName());


	@Inject
	InformacionCdaFacade infoCdaDAO;	
	
	
	
	@Inject
	Instance<OperacionesSicovIndraV2> operadorIndraV2Source;
	
	
	@Inject
	Instance<OperacionesSicovCi2V3> operadorCi2SourceV3;
	
	@Inject
	Instance<OperadorSicovDummy> operadorSicovDummySource;
	

	public OperacionesSicov crearOperacionSicov(){
		
		
		
		InformacionCda infoCda = infoCdaDAO.find(1);
		OperadorSicov operador  = infoCda.getOperadorSicov();
		
		logger.log(Level.INFO, "Operador sicov " + operador.name());
		
		OperacionesSicov operaciones;
		switch(operador){
		case CERO:
			operaciones = operadorSicovDummySource.get();
			break;
		case CI2:			
				operaciones = operadorCi2SourceV3.get();		 
			break;
		case SIN_OPERADOR:
			operaciones = operadorSicovDummySource.get();
			break;
		case INDRA:	
			operaciones = operadorIndraV2Source.get();			
			break;
		default:
			operaciones = new OperadorSicovDummy();
			break;
		
		}
		
		return operaciones;	
	}	
	

}
