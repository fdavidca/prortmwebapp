package com.proambiente.webapp.service.sicov;

import java.util.Date;


import com.proambiente.modelo.dto.PruebaDTO;
import com.proambiente.webapp.service.PinNoEncontradoException;
import com.proambiente.webapp.service.guardarpdf.GuardarPdfCallback;

public interface OperacionesSicov {
	
	public enum Envio {PRIMER_ENVIO,SEGUNDO_ENVIO}
	
	public void registrarFUR(Integer revisionId,Integer numeroInspeccion,Envio envio,GuardarPdfCallback pdfCallback);
	
	public void registrarInformacionRunt(Integer revisionId,GuardarPdfCallback pdfCallback);
	
	public void registrarFURInspeccionPrimerEnvio(Integer inspeccionId);
	
	public void registrarFURInspeccionSegundoEnvio(Integer inspeccionId);

	public void registrarEventoRTMEC(PruebaDTO prueba,String placa,Date fechaRevision,
			String serialesEquipos,String causaAborto,
			Integer tipoEventoPrueba,EtapaRevision etapaRevision);	
	
	public String consultarPinPlaca(String placa) throws PinNoEncontradoException;
	
	public void enviarEco(String mensaje);
	
	public void utilizarPin(String pin, String tipoRtm, String placa);
	
}
