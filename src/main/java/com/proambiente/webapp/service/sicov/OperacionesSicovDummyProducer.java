package com.proambiente.webapp.service.sicov;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import com.proambiente.indra.cliente.SicovSoap;
import com.proambiente.webapp.service.indra.SicovSoapConfigurado;

@ApplicationScoped
public class OperacionesSicovDummyProducer {

OperadorSicovDummy sicovSoap;
	
	@Produces @SicovSoapConfigurado
	public OperadorSicovDummy crearDatosSoap(){
		if(sicovSoap == null) {
			return new OperadorSicovDummy();
		}else {
			return sicovSoap;
		}
	}
	
	
	
}
