package com.proambiente.webapp.service;

import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;


import javax.inject.Inject;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.proambiente.modelo.Marca;
import com.proambiente.webapp.dao.MarcaFacade;
import com.proambiente.webapp.service.exception.CeldaNullException;

public class ImportarMarcasDesdeExcelService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3388214616555259255L;

	@Inject
	MarcaFacade marcaDAO;	

	private static final Logger logger = Logger.getLogger(ImportarMarcasDesdeExcelService.class.getName());

	public List<Marca> obtenerMarcasDesdeArchivo(InputStream inputStream) throws Exception {

		List<Marca> marcasCargadas = new ArrayList<>();
		try {
			XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
			XSSFSheet sheet = workbook.getSheetAt(0);
			Iterator<Row> rowIterator = sheet.iterator();
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				if(row.getRowNum() < 4) continue;
				
				Optional<Marca> optMarca = construirMarca(row);
				if (optMarca.isPresent()) {
					marcasCargadas.add(optMarca.get());
				}
			}
		} catch (CeldaNullException cexc) {
			logger.info("Se ha llegado a celda nula " + cexc.getMessage());
		} finally {
			inputStream.close();
		}

		return marcasCargadas;
	}

	public void guardarMarca(List<Marca> marcas) {
		for (Marca m : marcas) {
			try {
				logger.info("Creando o modificando " + m.getNombre());
				marcaDAO.edit(m);				
			} catch (Exception exc) {
				logger.log(Level.SEVERE, "Error creando marca : " + m.getNombre(), exc);
			}
		}
	}

	public Optional<Marca> construirMarca(Row rowParam) throws CeldaNullException {
		
		Row row = rowParam;
		StringBuilder sb = new StringBuilder();
		if (row == null) {
			return Optional.empty();
		}
		Cell cell = null;
		DataFormatter formatter = new DataFormatter(); // creating formatter
														// using the default
														// locale
		

		
		String marca = "";
		marca = leerMarcaDesdeCelda(row, formatter, sb);

		
		double codigoMarca = 0;
		cell = row.getCell(1);
		if (celdaEsTipoNumerico(cell).isPresent()) {
			if (celdaEsTipoNumerico(cell).get().equals(Boolean.TRUE)) {
				codigoMarca = row.getCell(1).getNumericCellValue();
			} else {
				sb.append("Celda que contiene modelo no es numerica \n");
				try {
					String modeloStr = formatter.formatCellValue(row.getCell(1));
					codigoMarca = Double.valueOf(modeloStr);
				} catch (NumberFormatException nexc) {
					sb.append("El modelo no corresponde con un número");
					return Optional.empty();
				}
			}
		} else if (cell == null) {
			return Optional.empty();
		}

		Marca m = new Marca();
		m.setMarcaId((int)codigoMarca);
		m.setNombre(marca);
		
		return Optional.of(m);

	}

	
	
	private String leerMarcaDesdeCelda(Row row, DataFormatter formatter, StringBuilder sb) throws CeldaNullException {
		String marca = "";
		Cell cell;
		cell = row.getCell(2);
		if (celdaEsTipoString(cell).isPresent()) {
			if (celdaEsTipoString(cell).get().equals(Boolean.TRUE)) {
				marca = row.getCell(2).getStringCellValue();
			} else {
				sb.append("Celda de marca no es tipo string \n");
				marca = formatter.formatCellValue(row.getCell(2));
			}
		} else if (cell == null) {
			throw new CeldaNullException(row.getRowNum(), 2);
		}
		return marca;
	}

	private Optional<Boolean> celdaEsTipoString(Cell cell) {
		if (cell == null) {
			return Optional.empty();
		} else {
			return Optional.of(cell.getCellType() == CellType.STRING);
		}
	}

	private Optional<Boolean> celdaEsTipoNumerico(Cell cell) {
		if (cell == null) {
			return Optional.empty();
		} else {
			return Optional.of(cell.getCellType() == CellType.NUMERIC);
		}
	}

}
