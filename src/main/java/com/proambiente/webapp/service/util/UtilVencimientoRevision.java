package com.proambiente.webapp.service.util;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class UtilVencimientoRevision {

    public boolean esRevisionVencida(Date fechaRevision, Date fechaReinspeccion) {
        LocalDateTime ldtFechaReinspeccion = convertirDateLocalDateTime(fechaReinspeccion);
        LocalDateTime fechaVencimientoPlazo = calcularFechaVencimientoPlazo(fechaRevision);
        return ldtFechaReinspeccion.isAfter(fechaVencimientoPlazo);

    }

    public LocalDateTime convertirDateLocalDateTime(Date date) {
        LocalDateTime ldt = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        return ldt;
    }

    public LocalDateTime calcularFechaVencimientoPlazo(Date fechaCreacionRevision) {
        LocalDateTime ldtFechaRevision = convertirDateLocalDateTime(fechaCreacionRevision);
        LocalDateTime fechaVencimientoPlazo = ldtFechaRevision.plusDays(15);
        return fechaVencimientoPlazo;
    }

}

