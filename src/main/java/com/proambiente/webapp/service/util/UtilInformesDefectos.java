package com.proambiente.webapp.service.util;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import com.proambiente.modelo.Defecto;

public class UtilInformesDefectos {
	
	public void ordenarListaDefectos(List<Defecto> defectos){
			Collections.sort(defectos , (defecto1,defecto2) -> defecto1.getDefectoId().compareTo(defecto2.getDefectoId()));
	}
	
	public Optional<Defecto> obtenerDefectoPorCodigo(Stream<Defecto> defectos,Integer defectoId){
		return defectos.filter(defecto->defecto.getDefectoId().equals(defectoId)).findFirst();
	}
	
	

}
