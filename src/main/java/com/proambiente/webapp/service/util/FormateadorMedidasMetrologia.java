package com.proambiente.webapp.service.util;



import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.inject.Inject;

import com.proambiente.modelo.Medida;
import com.proambiente.webapp.dao.TipoMedidaFacade;

public class FormateadorMedidasMetrologia implements FormateadorMedidasInterface {


	
	@Override
	public String formatearMedida(Medida m) {
		if(m == null) {
			throw new RuntimeException("Medida para formatear en string es null");
		}
		if(m.getTipoMedida() == null) {
			throw new RuntimeException("Tipo medida para formatear en string es null");
		}
		
		Integer numeroDecimales = m.getTipoMedida().getDecimales();
		BigDecimal bs = new BigDecimal(m.getValor()).setScale(numeroDecimales,RoundingMode.HALF_EVEN);
		return bs.toString();		
	}

}
