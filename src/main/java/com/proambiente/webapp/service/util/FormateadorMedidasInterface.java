package com.proambiente.webapp.service.util;

import com.proambiente.modelo.Medida;

public interface FormateadorMedidasInterface {
	
	public String formatearMedida(Medida medida);

}
