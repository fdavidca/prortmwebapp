package com.proambiente.webapp.service.util;

import com.proambiente.modelo.TipoCombustible;
import com.proambiente.webapp.util.ConstantesTiposCombustible;

public class UtilTipoCombustible {
	
	public static boolean isCombustibleDiesel(TipoCombustible tipoCombustible){
		Integer tipoCombustibleId = tipoCombustible.getCombustibleId();
		return tipoCombustibleId.equals(ConstantesTiposCombustible.DIESEL)
				|| tipoCombustibleId.equals(ConstantesTiposCombustible.BIODIESEL)
				|| tipoCombustibleId.equals(ConstantesTiposCombustible.DIESEL_ELECTRICO);
	}
	
	public static boolean isCombustibleGasolina(TipoCombustible tipoCombustible){
		Integer tipoCombustibleId = tipoCombustible.getCombustibleId();
		return tipoCombustibleId.equals(ConstantesTiposCombustible.GASOLINA )
		|| tipoCombustibleId.equals(ConstantesTiposCombustible.GAS_NATURAL)
		|| tipoCombustibleId.equals(ConstantesTiposCombustible.GAS_GASOLINA)
		|| tipoCombustibleId.equals(ConstantesTiposCombustible.GASOLINA_ELECTRICO);
	}

}
