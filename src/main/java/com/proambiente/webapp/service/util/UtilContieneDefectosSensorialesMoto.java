package com.proambiente.webapp.service.util;

import java.util.List;
import java.util.Optional;

import static com.proambiente.webapp.util.ConstantesCodigosDefectoGases.*;

import com.proambiente.modelo.Defecto;

public class UtilContieneDefectosSensorialesMoto {
	
	public Boolean contieneDefectosSensorialesMotocicleta(List<Defecto> defectos) {
		
		Optional<Defecto> optDefecto = defectos.stream().filter( d-> d.getDefectoId()
				.equals(CODIGO_FUGAS_TUBO_ESCAPE_MOTO) 
									||  d.getDefectoId().equals(CODIGO_SALIDAS_ADICIONALES_MOTO)
									|| d.getDefectoId().equals(CODIGO_AUSENCIA_TAPA_ACEITE_MOTO)
									|| d.getDefectoId().equals(PRESENCIA_HUMO_NEGRO_AZUL_MOTOS)
									|| d.getDefectoId().equals(REVOLUCIONES_INESTABLES_MOTOS)
								).findAny();
		return optDefecto.isPresent();
		
	}

}
