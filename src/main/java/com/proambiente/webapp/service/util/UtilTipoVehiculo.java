package com.proambiente.webapp.service.util;

import com.proambiente.modelo.TipoVehiculo;
import com.proambiente.webapp.util.ConstantesTiposVehiculo;

public class UtilTipoVehiculo {

	
	public static boolean isVehiculoMoto(TipoVehiculo tipoVehiculo){
		Boolean vehiculoEsMoto = tipoVehiculo.getTipoVehiculoId().equals(ConstantesTiposVehiculo.MOTO) || tipoVehiculo.getTipoVehiculoId().equals(ConstantesTiposVehiculo.MOTO_CARRO);
		return vehiculoEsMoto;
	}
	
	public static boolean isVehiculoPesado(TipoVehiculo tipoVehiculo) {
		Boolean vehiculoEsMoto = tipoVehiculo.getTipoVehiculoId().equals(ConstantesTiposVehiculo.PESADO) || tipoVehiculo.getTipoVehiculoId().equals(ConstantesTiposVehiculo.REMOLQUE);
		return vehiculoEsMoto;
	}
	
	public static boolean isVehiculoMotocarro(TipoVehiculo tipoVehiculo) {
		Boolean vehiculoEsMoto = tipoVehiculo.getTipoVehiculoId().equals(ConstantesTiposVehiculo.MOTO_CARRO);
		return vehiculoEsMoto;
	}
}
