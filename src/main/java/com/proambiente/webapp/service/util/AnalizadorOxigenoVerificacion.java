package com.proambiente.webapp.service.util;

import java.math.RoundingMode;
import java.text.DecimalFormat;

import java.util.List;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;

import com.proambiente.modelo.dto.MedicionGasesDTO;

public class AnalizadorOxigenoVerificacion {

	List<MedicionGasesDTO> medicionesBaja;
	List<MedicionGasesDTO> medicionesAlta;
	
	public AnalizadorOxigenoVerificacion(List<MedicionGasesDTO> listaMedicionesBaja,
			List<MedicionGasesDTO> listaMedicionesAlta) {
		
		this.medicionesBaja = listaMedicionesBaja;
		this.medicionesAlta = listaMedicionesAlta;
	}

	public Boolean analizar(StringBuilder sb) {
		SummaryStatistics dssBaja = new SummaryStatistics();
		medicionesBaja.forEach(mdto-> dssBaja.addValue( mdto.getO2().setScale(2, RoundingMode.HALF_UP).doubleValue() ) );
		Boolean oxigenoBajaMayorLimite = dssBaja.getMean() > 0.5;
		DecimalFormat df = new DecimalFormat("0.00");
		if(oxigenoBajaMayorLimite) {
			
			sb.append(" Oxigeno promedio de muestra Baja MAYOR a 0.5 : " ).append(df.format(dssBaja.getMean()));
			
		}else {
			sb.append(" Oxigeno promedio de muestra Baja MENOR a 0.5 : " ).append(df.format(dssBaja.getMean()));
		}
		
		SummaryStatistics ssAlta = new SummaryStatistics();
		medicionesAlta.forEach(mdto-> ssAlta.addValue( mdto.getO2().setScale(2, RoundingMode.HALF_UP).doubleValue() ) );
		Boolean oxigenoAltaMayorLimite = ssAlta.getMean() > 0.5;
		if(oxigenoAltaMayorLimite) {
			
			sb.append(" Oxigeno promedio de muestra Alta MAYOR a 0.5 : " ).append(df.format(ssAlta.getMean()));
		}else {
			sb.append(" Oxigeno promedio de muestra Alta MENOR a 0.5 : " ).append(df.format(ssAlta.getMean()));
		}
		
		return !oxigenoBajaMayorLimite && !oxigenoAltaMayorLimite;
	}

	
	
	
}
