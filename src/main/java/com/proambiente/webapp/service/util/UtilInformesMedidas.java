package com.proambiente.webapp.service.util;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.stream.Stream;

import com.proambiente.modelo.Medida;
import com.proambiente.modelo.TipoMedida;

public class UtilInformesMedidas {
	
	/**
	 * 
	 * @param medidas Ordenado preferiblemente
	 * @return
	 */
	
	public OptionalDouble obtenerMedidaPorCodigo(Stream<Medida> medidas,Integer codigoMedida){
		return medidas.filter(medida->medida.getTipoMedida().getTipoMedidaId().equals(codigoMedida)).mapToDouble(m->m.getValor()).findFirst();
	}
	
	public Optional<Medida> obtenerMedida(Stream<Medida> medidas,Integer codigoMedida){
		return medidas.filter(medida->medida.getTipoMedida().getTipoMedidaId().equals(codigoMedida)).findFirst();
	}
	
	/**
	 * 
	 * @param medidas
	 * @param codigoMedida
	 * @return
	 */
	public OptionalDouble obtenerMedidaPorCodigo(List<Medida> medidas,Integer codigoMedida){
		TipoMedida tipoMedida = new TipoMedida();
		tipoMedida.setTipoMedidaId(codigoMedida);
		Medida key = new Medida();
		key.setTipoMedida(tipoMedida);
		
		
		int indice = Collections.<Medida>binarySearch(medidas, key, 
				(medida1,medida2)->medida1.getTipoMedida().getTipoMedidaId().compareTo(medida2.getTipoMedida().getTipoMedidaId() ) );
		OptionalDouble valorRetorno ;
		if(indice >= 0){
			valorRetorno = OptionalDouble.of(medidas.get(indice).getValor());
		}else {
			valorRetorno = OptionalDouble.empty();
		}
		return valorRetorno;
	}

	
	public void ordenarListaMedidas(List<Medida> medidas){
		Collections.sort(medidas, (medida1,medida2)->medida1.getTipoMedida().getTipoMedidaId().compareTo(medida2.getTipoMedida().getTipoMedidaId() ));
	}
}
