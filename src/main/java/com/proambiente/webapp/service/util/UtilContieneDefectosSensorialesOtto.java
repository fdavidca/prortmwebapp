package com.proambiente.webapp.service.util;

import java.util.List;
import java.util.Optional;

import com.proambiente.modelo.Defecto;
import static  com.proambiente.webapp.util.ConstantesCodigosDefectoGases.*;

public class UtilContieneDefectosSensorialesOtto {
	
public Boolean contieneDefectosSensorialesOtto(List<Defecto> defectos) {
	
	
		
		Optional<Defecto> optDefecto = defectos.stream().filter( d-> d.getDefectoId()
				.equals(CODIGO_AUSENCIA_TAPA_ACEITE_OTTO) 
									||  d.getDefectoId().equals(CODIGO_AUSENCIA_TAPA_COMBUSTIBLE_OTTO)
									|| d.getDefectoId().equals(CODIGO_FUGAS_TUBO_SILENCIADOR)
									|| d.getDefectoId().equals(CODIGO_SALIDAS_ADICIONALES_OTTO)
									|| d.getDefectoId().equals(CODIGO_ACCESORIOS_IMPIDEN_OTTO)
									|| d.getDefectoId().equals(CODIGO_FALLA_SISTEMA_AD_AIRE_OTTO)
									|| d.getDefectoId().equals(CODIGO_DESCONEXION_PCV)
									|| d.getDefectoId().equals(CODIGO_INCORRECTA_OP_REFRIGERACION_OTTO)									
									|| d.getDefectoId().equals(CODIGO_HUMO_NEGRO_AZUL)
									|| d.getDefectoId().equals(CODIGO_REVOLUCIONES_FUERA_RANGO)
								).findAny();
		return optDefecto.isPresent();
		
	}


}
