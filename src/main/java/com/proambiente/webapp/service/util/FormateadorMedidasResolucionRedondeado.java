package com.proambiente.webapp.service.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

import com.proambiente.modelo.Medida;

public class FormateadorMedidasResolucionRedondeado implements FormateadorMedidasInterface{

	@Override
	public String formatearMedida(Medida medida) {
		Double valor = medida.getValor();
		String valorStr = "";
		if(valor <= -10) {
			BigDecimal bd = new BigDecimal(valor).setScale(1,RoundingMode.HALF_EVEN);
			valorStr = bd.toString();
		}else if(valor < 0 && valor > -10) {
			BigDecimal bd = new BigDecimal(valor).setScale(2,RoundingMode.HALF_EVEN);
			valorStr = bd.toString();
		}else if (valor < 10) {
			BigDecimal bd = new BigDecimal(valor).setScale(2,RoundingMode.HALF_EVEN);
			valorStr = bd.toString();
		}else if (valor < 100) {
			BigDecimal bd = new BigDecimal(valor).setScale(1,RoundingMode.HALF_EVEN);
			valorStr = bd.toString();
		}else {
			BigDecimal bd = new BigDecimal(valor).setScale(0,RoundingMode.HALF_EVEN);
			valorStr = bd.toString();
		}
		return valorStr;
	}

}
