package com.proambiente.webapp.service;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.proambiente.modelo.EstadoDefecto;
import com.proambiente.webapp.dao.DefectoFacade;

@Stateless
public class CodigosDefectoNuevaResolucionService implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	DefectoFacade defectoDAO;
	
	@PersistenceContext
	EntityManager em;
	
	public List<String> obtenerTodosLosDefectos(List<Integer> idPruebas) {
		Query query = em.createQuery("SELECT d.defecto.codigoMinisterio FROM DefectoAsociado d  WHERE d.prueba.pruebaId IN :listaIdPruebas and d.estadoDefecto =:estadoDefecto");
		query.setParameter("listaIdPruebas", idPruebas);
		query.setParameter("estadoDefecto",EstadoDefecto.DETECTADO);
		List<String> defectosAsociados = query.getResultList();		
		return defectosAsociados;
	}

}
