package com.proambiente.webapp.service.exception;

public class CeldaNullException extends Exception {

	private static final long serialVersionUID = 1L;

	private Integer numeroFila;
	private Integer numeroColumna;
	
	public CeldaNullException(Integer numeroFila,Integer numeroColumna){
		this.numeroFila = numeroFila;
		this.numeroColumna = numeroColumna;
	}

	@Override
	public String getMessage() {
		return "Celda Null en fila " + numeroFila + " columna " + numeroColumna;
	}
	
	
	
}
