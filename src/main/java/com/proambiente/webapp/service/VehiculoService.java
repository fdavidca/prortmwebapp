package com.proambiente.webapp.service;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.proambiente.modelo.Vehiculo;
import com.proambiente.webapp.dao.VehiculoFacade;

@Stateless
public class VehiculoService {
	
	@Inject
	VehiculoFacade vehiculoDAO;
	
	@PersistenceContext
	EntityManager em;
	
	
	public Vehiculo findVehiculoByPlaca(String placa){
		if(placa == null || placa.isEmpty()){
			return null;
		}else {
			return vehiculoDAO.findVehicuoByPlaca(placa);			
		}
	}
	
	public String consultarRefLlanta(Integer vehiculoId){
		String consulta = "SELECT v.llanta.nombre FROM Vehiculo v  WHERE v.vehiculoId=:vehiculoId";
		Query query = em.createQuery(consulta);
		query.setParameter("vehiculoId", vehiculoId);
		return (String) query.getSingleResult();
	}
	
	
	public String placaDesdePruebaId(Integer pruebaId){
		String consulta = "SELECT p.revision.vehiculo.placa FROM Prueba p WHERE p.pruebaId=:pruebaId";
		Query query = em.createQuery(consulta);
		query.setParameter("pruebaId", pruebaId);
		return (String) query.getSingleResult();
	}
	
	public Vehiculo vehiculoDesdePruebaId(Integer pruebaId) {
		String consulta = "SELECT p.revision.vehiculo FROM Prueba p WHERE p.pruebaId=:pruebaId";
		Query query = em.createQuery(consulta);
		query.setParameter("pruebaId", pruebaId);
		return (Vehiculo) query.getSingleResult();
	}
	
	public String placaDesdeRevisionId(Integer revisionId){
		String consulta = "SELECT r.vehiculo.placa FROM Revision r WHERE r.revisionId =:revisionId";
		Query query = em.createQuery(consulta);
		query.setParameter("revisionId", revisionId);
		return (String) query.getSingleResult();
	}
	
	public Vehiculo vehiculoDesdeRevisionId(Integer revisionId){
		String consulta = "SELECT r.vehiculo FROM Revision r WHERE r.revisionId =:revisionId";
		Query query = em.createQuery(consulta);
		query.setParameter("revisionId", revisionId);
		return (Vehiculo) query.getSingleResult();	
	}
}
