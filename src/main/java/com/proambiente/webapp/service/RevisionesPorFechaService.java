package com.proambiente.webapp.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Revision_;

@Stateless
public class RevisionesPorFechaService {
	
	private static final Logger logger = Logger.getLogger(RevisionesPorFechaService.class.getName());

	@PersistenceContext
	EntityManager em;
	
	public List<Revision> consultarRevisionesPorFecha(Date fechaInicial,Date fechaFinal,Boolean preventiva){
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Revision> cq = cb.createQuery(Revision.class);
		Root<Revision> root = cq.from(Revision.class);

		List<Predicate> predicates = new ArrayList<Predicate>();

		Expression<Timestamp> fechaCreacion = root.get("fechaCreacionRevision");
		Timestamp tsFechaInicio = new Timestamp(fechaInicial.getTime());
		Timestamp tsFechaFinal = new Timestamp(fechaFinal.getTime());
		Predicate pdFecha = cb.between(fechaCreacion, tsFechaInicio, tsFechaFinal);
		
		Predicate pdPreventiva = cb.equal(root.get(Revision_.preventiva),preventiva);

		predicates.add(pdFecha);
		predicates.add(pdPreventiva);
		cq.select(root);
		cq.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
		cq.orderBy(cb.asc(root.get("revisionId")));
		cq.distinct(true);
		List<Revision> revisiones = em.createQuery(cq).getResultList();		
		return revisiones;		
	}


}
