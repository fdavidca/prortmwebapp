package com.proambiente.webapp.service.builderdto;

import java.sql.Timestamp;

import javax.persistence.Tuple;
import javax.persistence.metamodel.SingularAttribute;

import com.proambiente.modelo.Revision;

public class RevisionDTOConstructor {

	
	public static Revision crearProyectionRevision(Tuple tupla,SingularAttribute<Revision,?>[] atributos){
		Revision revision = new Revision();
		int  i = 0;
		for(SingularAttribute<Revision,?> at:atributos){
			switch(at.getName()){
			case "cerrada":
				revision.setCerrada((Boolean)tupla.get(i));
				break;
			
			case "aprobada":
				revision.setAprobada((Boolean)tupla.get(i));
				break;
			case "revisionId" :
				revision.setRevisionId((Integer)tupla.get(i));
				break;
			case "fechaCreacionRevision":
				revision.setFechaCreacionRevision((Timestamp) tupla.get(i));
				break;
			case "pin" :
				revision.setPin((String)tupla.get(i));
				break;
			case "consecutivoRunt" :
				revision.setConsecutivoRunt((String)tupla.get(i));
				break;
			case "numeroSolicitud":
				revision.setNumeroSolicitud((String)tupla.get(i));
				break;
			case "numeroInspecciones":
				revision.setNumeroInspecciones((Integer)tupla.get(i));
				break;
			case "primerEnvioRealizado":
				revision.setPrimerEnvioRealizado((Boolean)tupla.get(i));
				break;
			case "preventiva":
				revision.setPreventiva((Boolean)tupla.get(i));
				break;	
				default:
					throw new RuntimeException("Atributo no especificado metodo que crea RevisionDTO " + at.getName());
			}			
			i++;
		}		
		return revision;
	}
}
