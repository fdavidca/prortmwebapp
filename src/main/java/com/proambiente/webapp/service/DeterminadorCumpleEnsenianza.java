package com.proambiente.webapp.service;

import java.util.List;
import java.util.Optional;

import com.proambiente.modelo.DefectoAsociado;
import com.proambiente.modelo.EstadoDefecto;

public class DeterminadorCumpleEnsenianza {
	
	public static Boolean cumpleEnsenianza(DefectoService defectoService,Integer revisionId) {
		
		List<DefectoAsociado> defectos =defectoService.obtenerDefectosInspeccionSensorial(revisionId);
		Optional<DefectoAsociado> defectoEnseOpt = defectos.stream().filter(da->da.getEstadoDefecto().equals(EstadoDefecto.DETECTADO))
						 .filter(da->da.getDefecto().getCategoriaDefecto().getCategoriaDefectoId().equals(21))
						 .findAny();
		
		return !defectoEnseOpt.isPresent();
	}

}
