package com.proambiente.webapp.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.math3.stat.regression.SimpleRegression;
import org.knowm.xchart.BitmapEncoder;
import org.knowm.xchart.XYChart;

import com.proambiente.modelo.Analizador;
import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.Usuario;
import com.proambiente.modelo.VerificacionGasolina;
import com.proambiente.modelo.dto.MedicionGasesDTO;
import com.proambiente.webapp.dao.AnalizadorFacade;
import com.proambiente.webapp.dao.InformacionCdaFacade;
import com.proambiente.webapp.dao.UsuarioFacade;
import com.proambiente.webapp.dao.VerificacionGasolinaFacade;
import com.proambiente.webapp.service.util.AnalizadorOxigenoVerificacion;
import com.proambiente.webapp.util.AnalizadorVerificacion;
import com.proambiente.webapp.util.MedicionGasesDTODecodificador;
import com.proambiente.webapp.util.dto.MedicionDTOHexano;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;

/**
 * Servlet para imprimir el reporte o informe de verificacion de gasolina
 * 
 */
@WebServlet("/ServletVerificacionGasolina")

public class ServletVerificacionGasolina extends HttpServlet {

	private static final Logger logger = Logger.getLogger(ServletVerificacionGasolina.class.getName());

	private static final long serialVersionUID = 1L;

	@Inject
	VerificacionGasolinaFacade verificacionGasolinaDAO;

	@Inject
	InformacionCdaFacade informacionCdaDAO;

	@Inject
	AnalizadorFacade analizadorDAO;

	@Inject
	UsuarioFacade usuarioDAO;

	public ServletVerificacionGasolina() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			String idVerificacion = request.getParameter("id_verificacion");
			if (idVerificacion == null || idVerificacion.isEmpty())
				throw new RuntimeException("error falta parametro de prueba de fugas");
			Integer verificacionId = Integer.valueOf(idVerificacion);
			VerificacionGasolina verificacionGasolina = verificacionGasolinaDAO.find(verificacionId);
			InformacionCda info = informacionCdaDAO.find(1);
			response.setContentType("application/pdf");
			JasperPrint reporte = cargarReporteVerificacion(verificacionGasolina);
			JRPdfExporter exporter = new JRPdfExporter();
			exporter.setExporterInput(new SimpleExporterInput(reporte));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));
			exporter.exportReport();

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error cargando el reporte", e);
			throw new RuntimeException(e);
		} finally {
			response.getOutputStream().flush();
			response.getOutputStream().close();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

	public BufferedImage crearGraficoHC(List<MedicionGasesDTO> muestras, double height, double width) {
		double[] datosX = new double[muestras.size()];
		double[] datosHC = new double[muestras.size()];

		int i = 0;
		for (MedicionGasesDTO m : muestras) {
			datosX[i] = i * 1.0;
			datosHC[i] = m.getHC();

			i++;
		}
		XYChart chart = new XYChart((int) width, (int) height);
		chart.addSeries("HC", datosHC);
		return BitmapEncoder.getBufferedImage(chart);
	}

	public BufferedImage crearGraficoOtros(List<MedicionGasesDTO> muestras, double height, double width) {
		double[] datosX = new double[muestras.size()];

		double[] datosCO = new double[muestras.size()];
		double[] datosCO2 = new double[muestras.size()];
		double[] datosO2 = new double[muestras.size()];
		int i = 0;
		for (MedicionGasesDTO m : muestras) {
			datosX[i] = i * 1.0;

			datosCO[i] = m.getCO().doubleValue();
			datosCO2[i] = m.getCO2().doubleValue();
			datosO2[i] = m.getO2().doubleValue();
			i++;
		}
		XYChart chart = new XYChart((int) width, (int) height);
		chart.addSeries("CO", datosCO);
		chart.addSeries("CO2", datosCO2);
		chart.addSeries("O2", datosO2);
		// chart.addSeries("O2", datosO2);
		return BitmapEncoder.getBufferedImage(chart);
	}

	public JasperPrint cargarReporteVerificacion(VerificacionGasolina vg) throws Exception {
		InputStream is = null;
		JasperPrint reporte = null;
		Map<String, Object> parametros = new HashMap<>();
		try {
			is = this.getClass().getClassLoader()
					.getResourceAsStream("com/proambiente/webapp/reporte/reporte_autoregulacion_verificacion.jasper");

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
			SimpleDateFormat sdfs = new SimpleDateFormat("yyyy-MM-dd");
			DecimalFormat df = new DecimalFormat("0.00");
			DecimalFormat dfSinDecimales = new DecimalFormat("#");
			InformacionCda infoCda = informacionCdaDAO.find(1);

			if (infoCda.getLogo() != null) {
				ByteArrayInputStream bis = new ByteArrayInputStream(infoCda.getLogo());
				parametros.put("logo", bis);
			}

			parametros.put("numero_prueba", String.valueOf(vg.getVerificacionId()));
			parametros.put("numero_establecimiento", infoCda.getNumeroCda());
			parametros.put("nombre_organizacion", infoCda.getNombre());
			parametros.put("NIT", infoCda.getNit());
			parametros.put("direccion", infoCda.getDireccion());
			parametros.put("telefono", infoCda.getTelefono1());
			parametros.put("telefono2", infoCda.getTelefono2());
			parametros.put("ciudad_establecimiento", infoCda.getCiudad());
			parametros.put("resolucion", infoCda.getNumeroResolucion());
			parametros.put("fecha_resolucion", sdfs.format(infoCda.getFechaResolucion()));

			Analizador analizador = analizadorDAO.find(vg.getNumeroSerieEquipo());
			if (analizador != null) {
				parametros.put("marca_equipo", analizador.getMarca());
				parametros.put("modelo_equipo", analizador.getModelo());
				parametros.put("serie_equipo", analizador.getSerial());
				DecimalFormat dfPef = new DecimalFormat("0.000");
				parametros.put("FEP", dfPef.format(vg.getPef()));
			} else {
				throw new RuntimeException("Analizador no registrado en la base de datos");
			}

			Double pef = vg.getPef();

			String nombreSoftware = "PRO RTM";
			parametros.put("nombre_software", nombreSoftware);
			String versionSoftware = infoCda.getVersionSoftware();
			parametros.put("version_software", versionSoftware);

			Usuario usuario = usuarioDAO.find(vg.getUsuarioId());
			StringBuilder sb = new StringBuilder();
			sb.append(usuario.getNombres()).append("  ").append(usuario.getApellidos()).append("-")
					.append(usuario.getCedula().toString());
			parametros.put("usuario_verificacion", sb.toString());

			parametros.put("fecha_verificacion", sdf.format(vg.getFechaVerificacion()));

			// gases de referencia para la verificacion

			parametros.put("gas_ref_baja_hc",
					String.format("P %.0f,H %.0f", vg.getValorGasRefBajaHC(), vg.getValorGasRefBajaHC() * pef));
			parametros.put("gas_ref_baja_co", String.format("%.2f", vg.getValorGasRefBajaCO()));
			parametros.put("gas_ref_baja_co2", String.format("%.2f", vg.getValorGasRefBajaCO2()));

			parametros.put("gas_ref_alta_hc",
					String.format("P %.0f,H %.0f", vg.getValorGasRefAltaHC(), vg.getValorGasRefAltaHC() * pef));
			parametros.put("gas_ref_alta_co", String.format("%.2f", vg.getValorGasRefAltaCO()));
			parametros.put("gas_ref_alta_co2", String.format("%.2f", vg.getValorGasRefAltaCO2()));

			// Resultado de la verificacion

			parametros.put("res_baja_hc",
					String.format("P %.0f,H %.0f", vg.getValorBajoHc(), vg.getValorBajoHc() * pef));
			parametros.put("res_baja_co", String.format("%.2f", vg.getValorBajoCo()));
			parametros.put("res_baja_co2", String.format("%.2f", vg.getValorBajoCo2()));

			parametros.put("res_alta_hc",
					String.format("P %.0f, H %.0f", vg.getValorAltoHc(), vg.getValorAltoHc() * pef));
			parametros.put("res_alta_co", String.format("%.2f", vg.getValorAltoCo()));
			parametros.put("res_alta_co2", String.format("%.2f", vg.getValorAltoCo2()));

			String humedadRelativa = dfSinDecimales.format(vg.getHumedadRelativa());
			parametros.put("humedad_relativa", humedadRelativa + " %");

			String temperaturaAmbiente = dfSinDecimales.format(vg.getTemperaturaAmbiente());
			parametros.put("temperatura_ambiente", temperaturaAmbiente + " C");

			List<MedicionGasesDTO> muestrasBaja = MedicionGasesDTODecodificador
					.decodificarDesdeCadena(vg.getMuestrasBaja());
			List<MedicionDTOHexano> muestrasBajasHexano = new ArrayList<>();
			muestrasBaja.stream().forEach(mdto -> {
				MedicionDTOHexano mhexano = new MedicionDTOHexano();
				mhexano.setCO(mdto.getCO());
				mhexano.setCO2(mdto.getCO2());
				mhexano.setOxigeno(mdto.getO2());
				mhexano.setFecha(mdto.getFecha());
				Double pefAux = vg.getPef();
				Integer hexano = new BigDecimal(mdto.getHC() * pefAux).setScale(0, RoundingMode.HALF_EVEN).intValue();
				mhexano.setHC(mdto.getHC());
				mhexano.setHCHexano(hexano);
				muestrasBajasHexano.add(mhexano);

			});

			List<MedicionGasesDTO> muestrasAlta = MedicionGasesDTODecodificador
					.decodificarDesdeCadena(vg.getMuestraAlta());
			List<MedicionDTOHexano> muestrasAltaHexano = new ArrayList<>();
			muestrasAlta.stream().forEach(mdto -> {
				MedicionDTOHexano mhexano = new MedicionDTOHexano();
				mhexano.setCO(mdto.getCO());
				mhexano.setCO2(mdto.getCO2());
				mhexano.setOxigeno(mdto.getO2());
				mhexano.setFecha(mdto.getFecha());
				Double pefAux = vg.getPef();
				Integer hexano = new BigDecimal(mdto.getHC() * pefAux).setScale(0, RoundingMode.HALF_EVEN).intValue();
				mhexano.setHC(mdto.getHC());
				mhexano.setHCHexano(hexano);
				muestrasAltaHexano.add(mhexano);

			});

			AnalizadorVerificacion analizadorVerificacion = new AnalizadorVerificacion();

			SimpleRegression regresionHC = analizadorVerificacion.calcularRectaHC(muestrasBaja, muestrasAlta,
					vg.getValorGasRefBajaHC(), vg.getValorGasRefAltaHC(), pef);
			Double pendienteHC = regresionHC.getSlope();
			Double interceptoHC = regresionHC.getIntercept();

			BigDecimal bdPendienteHC = new BigDecimal(pendienteHC);
			bdPendienteHC = bdPendienteHC.setScale(3, RoundingMode.HALF_EVEN);
			pendienteHC = bdPendienteHC.doubleValue();

			interceptoHC = new BigDecimal(interceptoHC).setScale(3, RoundingMode.HALF_EVEN).doubleValue();

			DecimalFormat fd = new DecimalFormat("0.000");
			parametros.put("pendienteHC", bdPendienteHC.toString());
			parametros.put("interceptoHC", fd.format(interceptoHC));

			System.out.println(String.format("Pendiente HC: %.3f, intercepto %.3f", pendienteHC, interceptoHC));

			StringBuilder sb2 = new StringBuilder();

			Double valorCOBaja = vg.getValorGasRefBajaCO();
			Double valorCOAlta = vg.getValorGasRefAltaCO();

			SimpleRegression regresionCO = analizadorVerificacion.calcularRectaCO(muestrasBaja, muestrasAlta,
					valorCOBaja, valorCOAlta);

			Double pendienteCO = regresionCO.getSlope();
			pendienteCO = new BigDecimal(pendienteCO).setScale(3, RoundingMode.HALF_EVEN).doubleValue();

			Double interceptoCO = regresionCO.getIntercept();
			interceptoCO = new BigDecimal(interceptoCO).setScale(3, RoundingMode.HALF_EVEN).doubleValue();

			parametros.put("pendienteCO", fd.format(pendienteCO));
			parametros.put("interceptoCO", fd.format(interceptoCO));

			System.out.println(String.format("Pendiente CO: %.3f, intercepto %.3f", pendienteCO, interceptoCO));

			Double valorCO2Baja = vg.getValorGasRefBajaCO2();
			Double valorCO2Alta = vg.getValorGasRefAltaCO2();
			SimpleRegression regresionCO2 = analizadorVerificacion.calcularRectaCO2(muestrasBaja, muestrasAlta,
					valorCO2Baja, valorCO2Alta);

			Double pendienteCO2 = regresionCO2.getSlope();
			pendienteCO2 = new BigDecimal(pendienteCO2).setScale(3, RoundingMode.HALF_EVEN).doubleValue();

			Double interceptoCO2 = regresionCO2.getIntercept();
			interceptoCO2 = new BigDecimal(interceptoCO2).setScale(3, RoundingMode.HALF_EVEN).doubleValue();

			parametros.put("pendienteCO2", fd.format(pendienteCO2));
			parametros.put("interceptoCO2", fd.format(interceptoCO2));

			System.out.println(String.format("Pendiente CO: %.3f, intercepto %.3f", pendienteCO2, interceptoCO2));

			StringBuilder sbRangos = new StringBuilder();

			boolean hcValido = analizadorVerificacion.validarTablaHC(pendienteHC, interceptoHC,
					vg.getTipoVerificacion(), sbRangos, 1.00);
			boolean coValido = analizadorVerificacion.validarTablaCO(pendienteCO, interceptoCO,
					vg.getTipoVerificacion(), sbRangos, 1.0);
			boolean co2Valido = analizadorVerificacion.validarTablaCO2(pendienteCO2, interceptoCO2,
					vg.getTipoVerificacion(), sbRangos, 1.0);

			boolean aprobada = hcValido && coValido && co2Valido;
			if (vg.getVerificacionOxigeno()) {
				StringBuilder sbO2 = new StringBuilder();
				Boolean aprobadaOxigeno = new AnalizadorOxigenoVerificacion(muestrasBaja, muestrasAlta).analizar(sbO2);
				boolean aprobacionAmbos = (aprobada && aprobadaOxigeno);
				if (!hcValido || !coValido || !co2Valido) {
					sbRangos.append(
							"--- VERIFICACION NO APROBADA \n ES NECESARIO REALIZAR ALGÚN \n AJUSTE O RUTINA DE MANTENIMIENTO AL EQUIPO\n");
				}
				if (!aprobadaOxigeno) {
					sbRangos.append(" --- NO APROBADA --- promedio de OXIGENO MAYOR a 0.5% en alguna muestra").append(sbO2.toString());
				}else {
					sbRangos.append(sbO2.toString());
				}
				parametros.put("resultado", aprobacionAmbos ? "APROBADA" : "REPROBADA");
			}else {
				parametros.put("resultado", aprobada ? "APROBADA" : "REPROBADA");
			}
			sb2.append("\n");
			sb2.append(sbRangos);
			parametros.put("comentario", sbRangos.toString());

			

			JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(muestrasBajasHexano);
			parametros.put("dsValGasesRalenti", ds);

			JRBeanCollectionDataSource dsAlta = new JRBeanCollectionDataSource(muestrasAltaHexano);

			parametros.put("dsValGasesCrucero", dsAlta);
			reporte = JasperFillManager.fillReport(is, parametros, new JREmptyDataSource());

			return reporte;

		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Error generando reporte de prueba de linealidad", exc);
			throw exc;
		} finally {
			try {
				is.close();
			} catch (Exception exc) {
				logger.log(Level.SEVERE, "error", exc);
			}
		}
	}

}
