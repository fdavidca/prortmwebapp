package com.proambiente.webapp.service;

import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;


import javax.inject.Inject;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.proambiente.modelo.LineaVehiculo;
import com.proambiente.modelo.Marca;
import com.proambiente.webapp.dao.LineaVehiculoFacade;
import com.proambiente.webapp.dao.MarcaFacade;
import com.proambiente.webapp.service.exception.CeldaNullException;

public class ImportarLineasDesdeExcelService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3388214616555259255L;

	@Inject
	MarcaFacade marcaDAO;
	
	@Inject
	LineaVehiculoFacade lineaVehiculoDAO;
	
	

	private static final Logger logger = Logger.getLogger(ImportarLineasDesdeExcelService.class.getName());

	public List<LineaVehiculo> obtenerLineasDesdeArchivo(InputStream inputStream) throws Exception {

		List<LineaVehiculo> lineasCargadas = new ArrayList<>();
		try {
			XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
			XSSFSheet sheet = workbook.getSheetAt(0);
			Iterator<Row> rowIterator = sheet.iterator();
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				if(row.getRowNum() < 1) continue;
				
				Optional<LineaVehiculo> optLinea = construirLinea(row);
				if (optLinea.isPresent()) {
					lineasCargadas.add(optLinea.get());
				}
			}
		} catch (CeldaNullException cexc) {
			logger.info("Se ha llegado a celda nula " + cexc.getMessage());
		} finally {
			inputStream.close();
		}

		return lineasCargadas;
	}

	public void guardarLinea(List<LineaVehiculo> lineas) {
		for (LineaVehiculo lv: lineas) {
			try {
				logger.info("Creando o modificando " + lv.getNombre());
				lineaVehiculoDAO.edit(lv);				
			} catch (Exception exc) {
				logger.log(Level.SEVERE, "Error creando marca : " + lv.getNombre(), exc);
			}
		}
	}

	public Optional<LineaVehiculo> construirLinea(Row rowParam) throws CeldaNullException {
		
		
		LineaVehiculo lv = new LineaVehiculo();
		Row row = rowParam;
		StringBuilder sb = new StringBuilder();
		if (row == null) {
			return Optional.empty();
		}
		Cell cell = null;
		DataFormatter formatter = new DataFormatter(); // creating formatter
														// using the default
														// locale
		

		
		String nombreLinea = "";
		nombreLinea = leerNombreLineaDesdeCelda(row, formatter, sb);
		lv.setNombre(nombreLinea);
		

		double codigoLinea = 0;
		cell = row.getCell(3);
		if (celdaEsTipoNumerico(cell).isPresent()) {
			if (celdaEsTipoNumerico(cell).get().equals(Boolean.TRUE)) {
				codigoLinea = row.getCell(3).getNumericCellValue();
			} else {
				sb.append("Celda que contiene modelo no es numerica \n");
				try {
					String modeloStr = formatter.formatCellValue(row.getCell(3));
					codigoLinea = Double.valueOf(modeloStr);
				} catch (NumberFormatException nexc) {
					sb.append("El modelo no corresponde con un número");
					return Optional.empty();
				}
			}
		} else if (cell == null) {
			return Optional.empty();
		}
		
		lv.setCodigoLinea((int)codigoLinea);
		
		double codigoMarca = 0;
		cell = row.getCell(2);
		if (celdaEsTipoNumerico(cell).isPresent()) {
			if (celdaEsTipoNumerico(cell).get().equals(Boolean.TRUE)) {
				codigoMarca = row.getCell(2).getNumericCellValue();
			} else {
				sb.append("Celda que contiene modelo no es numerica \n");
				try {
					String modeloStr = formatter.formatCellValue(row.getCell(2));
					codigoMarca = Double.valueOf(modeloStr);
				} catch (NumberFormatException nexc) {
					sb.append("El modelo no corresponde con un número");
					return Optional.empty();
				}
			}
		} else if (cell == null) {
			return Optional.empty();
		}
		
		Marca m = marcaDAO.find((int)codigoMarca);
		lv.setMarca(m);

		
		double lineaId = 0;
		cell = row.getCell(1);
		if (celdaEsTipoNumerico(cell).isPresent()) {
			if (celdaEsTipoNumerico(cell).get().equals(Boolean.TRUE)) {
				lineaId = row.getCell(1).getNumericCellValue();
			} else {
				sb.append("Celda que contiene modelo no es numerica \n");
				try {
					String modeloStr = formatter.formatCellValue(row.getCell(1));
					lineaId = Double.valueOf(modeloStr);
				} catch (NumberFormatException nexc) {
					sb.append("El modelo no corresponde con un número");
					return Optional.empty();
				}
			}
		} else if (cell == null) {
			return Optional.empty();
		}
		lv.setLineaVehiculoId((int)lineaId);
		
		return Optional.of( lv );

	}

	
	
	private String leerNombreLineaDesdeCelda(Row row, DataFormatter formatter, StringBuilder sb) throws CeldaNullException {
		String nombreLinea = "";
		Cell cell;
		cell = row.getCell(4);
		if (celdaEsTipoString(cell).isPresent()) {
			if (celdaEsTipoString(cell).get().equals(Boolean.TRUE)) {
				nombreLinea = row.getCell(4).getStringCellValue();
			} else {
				sb.append("Celda de marca no es tipo string \n");
				nombreLinea = formatter.formatCellValue(row.getCell(4));
			}
		} else if (cell == null) {
			throw new CeldaNullException(row.getRowNum(), 2);
		}
		return nombreLinea;
	}

	private Optional<Boolean> celdaEsTipoString(Cell cell) {
		if (cell == null) {
			return Optional.empty();
		} else {
			return Optional.of(cell.getCellType() == CellType.STRING);
		}
	}

	private Optional<Boolean> celdaEsTipoNumerico(Cell cell) {
		if (cell == null) {
			return Optional.empty();
		} else {
			return Optional.of(cell.getCellType() == CellType.NUMERIC);
		}
	}

}
