package com.proambiente.webapp.service;


import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.proambiente.modelo.CategoriaDefecto;
import com.proambiente.modelo.Defecto;
import com.proambiente.modelo.DefectoAsociado;
import com.proambiente.modelo.EstadoDefecto;
import com.proambiente.modelo.SubCategoriaDefecto;
import com.proambiente.webapp.dao.DefectoFacade;

public class DefectoService implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	DefectoFacade defectoDAO;
	
	@PersistenceContext
	EntityManager em;
	
	
	public List<CategoriaDefecto> obtenerCategoriaDefectoInspeccionVisualPorTipoVehiculo(Integer tipoVehiculoId){
		Query query = em.createQuery("SELECT DISTINCT(c) "
						+"FROM TipoVehiculo t "
						+ "JOIN t.defectosInspeccionVisual d "
						+ "JOIN d.subCategoriaDefecto sd "
						+ "JOIN sd.categoriaDefecto c "
						+"WHERE t.tipoVehiculoId =:tipoVehiculoId "
						+ "ORDER BY c.categoriaDefectoId");
		query.setParameter("tipoVehiculoId", tipoVehiculoId);
		List<CategoriaDefecto> categorias = query.<CategoriaDefecto>getResultList();
		return categorias;
	}
	
	
	public List<SubCategoriaDefecto> obtenerSubCategoriaDefectoInspeccionVisualPorTipoVehiculoPorCategoria(Integer tipoVehiculoId,Integer categoriaDefectoId){
		Query query = em.createQuery("SELECT DISTINCT(sd) "+ 
									  "FROM TipoVehiculo t " +
									  "JOIN t.defectosInspeccionVisual d "+
									  "JOIN d.subCategoriaDefecto sd "+
									  "JOIN sd.categoriaDefecto c "+
									  "WHERE t.tipoVehiculoId =:tipoVehiculoId " +
									  "AND c.categoriaDefectoId =:categoriaDefectoId "+
									  "ORDER BY sd.subCategoriaDefectoId" );
		query.setParameter("tipoVehiculoId", tipoVehiculoId);
		query.setParameter("categoriaDefectoId",categoriaDefectoId);
		List<SubCategoriaDefecto> subcategorias = query.<SubCategoriaDefecto>getResultList();
		return subcategorias;
		
	}
	
	
	public List<Defecto> obtenerDefectoPorTipoVehiculoPorCategoriaPorSubcategoria(Integer tipoVehiculoId,Integer categoriaId,Integer subCategoriaId){
		Query query = em.createQuery("SELECT d "+
									 "FROM TipoVehiculo t "+
									 "JOIN t.defectosInspeccionVisual d "+
									 "JOIN d.subCategoriaDefecto sd "+
									 "JOIN sd.categoriaDefecto c "+
									 "WHERE t.tipoVehiculoId =:tipoVehiculoId "+
									 "AND c.categoriaDefectoId =:categoriaDefectoId "+
									 "AND sd.subCategoriaDefectoId =:subCategoriaDefectoId "+
									 "ORDER BY sd.subCategoriaDefectoId");
		query.setParameter("tipoVehiculoId", tipoVehiculoId);
		query.setParameter("categoriaDefectoId",categoriaId);
		query.setParameter("subCategoriaDefectoId", subCategoriaId);
		List<Defecto> defectos = query.<Defecto>getResultList();
		return defectos;
	}
	
	public List<Defecto> obtenerDefectoPorTipoVehiculo(Integer tipoVehiculoId){
		Query query = em.createQuery("Select d "
									+ "FROM TipoVehiculo t "
									+ "JOIN t.defectosInspeccionVisual d "
									+ "WHERE t.tipoVehiculoId =:tipoVehiculoId "
									+ "ORDER BY d.categoriaDefecto,d.subCategoriaDefecto"
										);
		query.setParameter("tipoVehiculoId", tipoVehiculoId);
		List<Defecto> defectos = query.<Defecto>getResultList();
		return defectos;
	}
	
	

	public List<Defecto> obtenerCausalesRechazoInspeccionPrevia(Integer[] codigosDefectos){
		Query query = em.createQuery("SELECT d FROM Defecto d WHERE d.defectoId in :causales_rechazo_motocicletas");
		
		query.setParameter("causales_rechazo_motocicletas", Arrays.asList(codigosDefectos));
		List<Defecto> defectos = query.<Defecto>getResultList();
		return defectos;		
	}
	
	public Defecto obtenerDefectoPorId(Integer codigoDefecto){
		return defectoDAO.find(codigoDefecto);
	}
	
	public List<Defecto> obtenerDefectosInspeccionMecanizada(Integer revisionId){
		Query query = em.createQuery("SELECT p.defectosAsociados " + 
										" FROM Prueba p JOIN p.revision " +  
										" WHERE p.revision.revisionId =:revisionId " + 
										" AND p.pruebaId " +  
										" IN ( SELECT MAX(pr) FROM Prueba pr WHERE pr.revision.revisionId =:revisionId AND " +
										" pr.tipoPrueba.tipoPruebaId <> 1 GROUP BY pr.tipoPrueba )");
		query.setParameter("revisionId", revisionId);
		List<DefectoAsociado> defectosAsociados = query.getResultList();
		List<Defecto> defectos = defectosAsociados.stream().filter(da->da.getEstadoDefecto().equals(EstadoDefecto.DETECTADO))
												  .map(da->da.getDefecto()).collect(Collectors.toList());
		return defectos;		
	}
	
	public List<Defecto> obtenerDefectosInspeccionMecanizadaReinspeccion(Integer inspeccionId,EstadoDefecto estadoDefecto){
		Query query = em.createQuery("SELECT d " + 
				" FROM Inspeccion i JOIN i.pruebas p" +
				" JOIN p.defectosAsociados d " +
				" WHERE i.inspeccionId =:inspeccionId " + 
				" AND d.estadoDefecto =:estadoDefecto " +
				" AND p.tipoPrueba.tipoPruebaId <> 1 "
				);
		query.setParameter("inspeccionId", inspeccionId);
		query.setParameter("estadoDefecto", estadoDefecto );
		
		List<DefectoAsociado> defectosAsociados = query.getResultList();
		List<Defecto> defectos = defectosAsociados.stream().map(da->da.getDefecto()).collect(Collectors.toList());
		return defectos;		
	}
	
	public List<Defecto> obtenerDefectosInspeccionVisualReinspeccion(Integer inspeccionId,EstadoDefecto estadoDefecto){
		Query query = em.createQuery("SELECT d " + 
										" FROM Inspeccion i JOIN i.pruebas p" +
										" JOIN p.defectosAsociados d " +
										" WHERE i.inspeccionId =:inspeccionId " + 
										" AND d.estadoDefecto =:estadoDefecto " +
										" AND p.tipoPrueba.tipoPruebaId = 1 "
										);
		query.setParameter("inspeccionId", inspeccionId);
		query.setParameter("estadoDefecto", estadoDefecto );
		List<DefectoAsociado> defectosAsociados = query.getResultList();
		List<Defecto> defectos = defectosAsociados.stream().map(da->da.getDefecto()).collect(Collectors.toList());
		return defectos;		
	}	
	
	
	public List<DefectoAsociado> obtenerDefectosInspeccionSensorial (Integer revisionId){
		Query query = em.createQuery ( "SELECT p.defectosAsociados FROM Prueba p "
				+ "JOIN p.revision WHERE p.revision.revisionId =:revisionId" +
				" AND p.tipoPrueba.tipoPruebaId = 1 "
				+ "AND p.pruebaId " +  
				 " = ( SELECT MAX(pr) FROM Prueba pr WHERE pr.revision.revisionId =:revisionId AND " +
				 " pr.tipoPrueba.tipoPruebaId = 1  )" );
		query.setParameter("revisionId",revisionId);
		List<DefectoAsociado> defectosAsociados = query.getResultList();		
		return defectosAsociados;
	}
	
	public List<Integer> obtenerTodosLosDefectosRevision(Integer revisionId){
		String subConsulta = "SELECT MAX(pr.pruebaId) FROM Prueba pr WHERE pr.revision.revisionId =:revisionId GROUP BY pr.tipoPrueba";
		Query queryPruebas = em.createQuery(subConsulta);
		queryPruebas.setParameter("revisionId", revisionId);
		List<Integer> idPruebas = queryPruebas.getResultList();
		Query query = em.createQuery("SELECT d.defecto.codigoDefecto FROM DefectoAsociado d  WHERE d.prueba.pruebaId IN :listaIdPruebas and d.estadoDefecto =:estadoDefecto");
		query.setParameter("listaIdPruebas", idPruebas);
		query.setParameter("estadoDefecto",EstadoDefecto.DETECTADO);
		List<Integer> defectosAsociados = query.getResultList();		
		return defectosAsociados;
		
	}
	
	public List<Integer> obtenerTodosLosDefectos(List<Integer> idPruebas) {
		Query query = em.createQuery("SELECT d.defecto.codigoDefecto FROM DefectoAsociado d  WHERE d.prueba.pruebaId IN :listaIdPruebas and d.estadoDefecto =:estadoDefecto");
		query.setParameter("listaIdPruebas", idPruebas);
		query.setParameter("estadoDefecto",EstadoDefecto.DETECTADO);
		List<Integer> defectosAsociados = query.getResultList();		
		return defectosAsociados;
	}

}
