package com.proambiente.webapp.service;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.inject.Inject;

import com.proambiente.modelo.Medida;
import com.proambiente.modelo.Prueba;
import com.proambiente.webapp.dao.MedidaFacade;
import com.proambiente.webapp.service.util.FormateadorMedidasInterface;

import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.*;

public class UtilInformacionProfundidadLabrado {
	@Inject
	public MedidaFacade medidaDAO;

	public UtilInformacionProfundidadLabrado() {
	}

	@Deprecated
	public  String cargarInformacionProfundidadLabrado(List<Prueba> pruebas, Boolean isMoto, Boolean isPesado) {
		List<Medida> medidasProfundidadLabrado = medidaDAO.consultarMedidaProfundidadLabrado(pruebas);
		StringBuilder sb = new StringBuilder();
		for (Medida m : medidasProfundidadLabrado) {
			sb.append(m.getTipoMedida().getDescripcion()).append("\t").append(m.getValor()).append("\t");
		}
		Optional<Double> valorDelanteraDerecha = medidasProfundidadLabrado.stream()
				.filter(m -> m.getTipoMedida().getTipoMedidaId().equals(5060)).findAny().map(m -> m.getValor());
		Optional<Double> valorDelanteraIzquierda = medidasProfundidadLabrado.stream()
				.filter(m -> m.getTipoMedida().getTipoMedidaId().equals(5061)).findAny().map(m -> m.getValor());
		Optional<Double> valorTraseraDerecha = medidasProfundidadLabrado.stream()
				.filter(m -> m.getTipoMedida().getTipoMedidaId().equals(5062)).findAny().map(m -> m.getValor());

		Optional<Double> valorTraseraIzquierda = medidasProfundidadLabrado.stream()
				.filter(m -> m.getTipoMedida().getTipoMedidaId().equals(5063)).findAny().map(m -> m.getValor());

		Optional<Double> valorRepuesto = medidasProfundidadLabrado.stream()
				.filter(m -> m.getTipoMedida().getTipoMedidaId().equals(5064)).findAny().map(m -> m.getValor());
		String formateada = "";
		if (isMoto) {
			formateada = formatearMedidasProfundidadLabradoMoto(valorDelanteraDerecha, valorTraseraDerecha,
					valorTraseraIzquierda);
		} else if (!isPesado) {
			formateada = formatearMedidasProfundidadLabrado(valorDelanteraDerecha, valorDelanteraIzquierda,
					valorTraseraDerecha, valorTraseraIzquierda, valorRepuesto);
		} else {
			formateada = formatearProfundidadLabrado(medidasProfundidadLabrado);
		}

		return formateada;
	}

	public void colocarInformacionProfundidadLabrado(List<Prueba> pruebas, Boolean isMoto, Boolean isPesado,
			Map<String, Object> parametros,FormateadorMedidasInterface formateadorMedidas) {
		List<Medida> medidasProfundidadLabrado = medidaDAO.consultarMedidaProfundidadLabrado(pruebas);
		Optional<Medida> medidaDelanteraDerecha = medidasProfundidadLabrado.stream()
				.filter(m -> m.getTipoMedida().getTipoMedidaId().equals(5060)).findAny();
		Optional<Medida> medidaDelanteraIzquierda = medidasProfundidadLabrado.stream()
				.filter(m -> m.getTipoMedida().getTipoMedidaId().equals(5061)).findAny();
		Optional<Medida> medidaTraseraDerecha = medidasProfundidadLabrado.stream()
				.filter(m -> m.getTipoMedida().getTipoMedidaId().equals(5062)).findAny();

		Optional<Medida> medidaTraseraIzquierda = medidasProfundidadLabrado.stream()
				.filter(m -> m.getTipoMedida().getTipoMedidaId().equals(5063)).findAny();

		Optional<Medida> valorRepuesto = medidasProfundidadLabrado.stream()
				.filter(m -> m.getTipoMedida().getTipoMedidaId().equals(5064)).findAny();
		
		Optional<Medida> valorPosteriorDerechaSecundaria = medidasProfundidadLabrado.stream()
																					.filter(m -> m.getTipoMedida().getTipoMedidaId()
																					.equals(PROFUNDIDAD_LABRADO_EJE2_DER_2))
																					.findAny();
		
		Optional<Medida> valorPosteriorIzquierdaSecundaria = medidasProfundidadLabrado.stream()
				.filter(m -> m.getTipoMedida().getTipoMedidaId()
				.equals(PROFUNDIDAD_LABRADO_EJE2_IZQ_2))
				.findAny();
		
		
		if (isMoto) {
			colocarMedidasProfundidadLabradoMoto(medidaDelanteraDerecha,medidaDelanteraIzquierda, medidaTraseraDerecha, medidaTraseraIzquierda,valorRepuesto,
					parametros,formateadorMedidas,valorPosteriorDerechaSecundaria,valorPosteriorIzquierdaSecundaria);
		} else if (!isPesado) {
			colocarMedidasProfundidadLabrado(medidaDelanteraDerecha, medidaDelanteraIzquierda,
					medidaTraseraDerecha, medidaTraseraIzquierda, valorRepuesto,parametros,formateadorMedidas);
		} else {
			colocarProfundidadLabradoPesado(medidasProfundidadLabrado, parametros,formateadorMedidas);
		}

	}

	@Deprecated
	public String cargarInformacionProfundidadLabrado(List<Prueba> pruebas, Boolean isMoto, Boolean isPesado,
			Map<String, Object> parametros) {
		List<Medida> medidasProfundidadLabrado = medidaDAO.consultarMedidaProfundidadLabrado(pruebas);
		StringBuilder sb = new StringBuilder();
		for (Medida m : medidasProfundidadLabrado) {
			sb.append(m.getTipoMedida().getDescripcion()).append("\t").append(m.getValor()).append("\t");
		}
		Optional<Double> valorDelanteraDerecha = medidasProfundidadLabrado.stream()
				.filter(m -> m.getTipoMedida().getTipoMedidaId().equals(5060)).findAny().map(m -> m.getValor());
		Optional<Double> valorDelanteraIzquierda = medidasProfundidadLabrado.stream()
				.filter(m -> m.getTipoMedida().getTipoMedidaId().equals(5061)).findAny().map(m -> m.getValor());
		Optional<Double> valorTraseraDerecha = medidasProfundidadLabrado.stream()
				.filter(m -> m.getTipoMedida().getTipoMedidaId().equals(5062)).findAny().map(m -> m.getValor());

		Optional<Double> valorTraseraIzquierda = medidasProfundidadLabrado.stream()
				.filter(m -> m.getTipoMedida().getTipoMedidaId().equals(5063)).findAny().map(m -> m.getValor());

		Optional<Double> valorRepuesto = medidasProfundidadLabrado.stream()
				.filter(m -> m.getTipoMedida().getTipoMedidaId().equals(5064)).findAny().map(m -> m.getValor());
		String formateada = "";
		if (isMoto) {
			formateada = formatearMedidasProfundidadLabradoMoto(valorDelanteraDerecha, valorTraseraDerecha,
					valorTraseraIzquierda);
		} else if (!isPesado) {
			formateada = formatearMedidasProfundidadLabrado(valorDelanteraDerecha, valorDelanteraIzquierda,
					valorTraseraDerecha, valorTraseraIzquierda, valorRepuesto);
		} else {
			formateada = formatearProfundidadLabrado(medidasProfundidadLabrado);
		}

		return formateada;
	}

	@Deprecated
	public String formatearProfundidadLabrado(List<Medida> medidasProfundidadLabrado) {
		String tabla[][] = new String[6][8];
		for (Medida o : medidasProfundidadLabrado) {
			switch (o.getTipoMedida().getTipoMedidaId()) {
			case 5061:
				tabla[0][3] = "" + o.getValor();
				break;
			case 5060:
				tabla[0][4] = "" + o.getValor();
				break;
			case 5093:
				tabla[1][0] = "" + o.getValor();
				break;
			case 5083:
				tabla[1][1] = "" + o.getValor();
				break;
			case 5073:
				tabla[1][2] = "" + o.getValor();
				break;
			case 5063:
				tabla[1][3] = "" + o.getValor();
				break;
			case 5062:
				tabla[1][4] = "" + o.getValor();
				break;
			case 5072:
				tabla[1][5] = "" + o.getValor();
				break;
			case 5082:
				tabla[1][6] = "" + o.getValor();
				break;
			case 5092:
				tabla[1][7] = "" + o.getValor();
				break;
			case 5097:
				tabla[2][0] = "" + o.getValor();
				break;
			case 5087:
				tabla[2][1] = "" + o.getValor();
				break;
			case 5077:
				tabla[2][2] = "" + o.getValor();
				break;
			case 5067:
				tabla[2][3] = "" + o.getValor();
				break;
			case 5066:
				tabla[2][4] = "" + o.getValor();
				break;
			case 5076:
				tabla[2][5] = "" + o.getValor();
				break;
			case 5086:
				tabla[2][6] = "" + o.getValor();
				break;
			case 5096:
				tabla[2][7] = "" + o.getValor();
				break;
			case 5099:
				tabla[3][0] = "" + o.getValor();
				break;
			case 5089:
				tabla[3][1] = "" + o.getValor();
				break;
			case 5079:
				tabla[3][2] = "" + o.getValor();
				break;
			case 5069:
				tabla[3][3] = "" + o.getValor();
				break;
			case 5068:
				tabla[3][4] = "" + o.getValor();
				break;
			case 5078:
				tabla[3][5] = "" + o.getValor();
				break;
			case 5088:
				tabla[3][6] = "" + o.getValor();
				break;
			case 5098:
				tabla[3][7] = "" + o.getValor();
				break;
			case 5101:
				tabla[4][0] = "" + o.getValor();
				break;
			case 5091:
				tabla[4][1] = "" + o.getValor();
				break;
			case 5081:
				tabla[4][2] = "" + o.getValor();
				break;
			case 5071:
				tabla[4][3] = "" + o.getValor();
				break;
			case 5070:
				tabla[4][4] = "" + o.getValor();
				break;
			case 5080:
				tabla[4][5] = "" + o.getValor();
				break;
			case 5090:
				tabla[4][6] = "" + o.getValor();
				break;
			case 5100:
				tabla[4][7] = "" + o.getValor();
				break;
			case 5064:
				tabla[5][0] = "" + o.getValor();
				break;
			case 5074:
				tabla[5][1] = "" + o.getValor();
				break;
			case 5084:
				tabla[5][2] = "" + o.getValor();
				break;
			case 5094:
				tabla[5][3] = "" + o.getValor();
				break;
			}
		}
		StringBuilder sb = new StringBuilder();

		sb.append("    IZQ          DER    \n");
		sb.append("|1|   |   |   |").append(tabla[0][3] == null ? "   " : tabla[0][3]).append("|" + "|")
				.append(tabla[0][4] == null ? "   " : tabla[0][4]).append("|   |   |   |\n");

		sb.append("|2|").append(tabla[1][0] == null ? "   " : tabla[1][0]).append("|")
				.append(tabla[1][1] == null ? "   " : tabla[1][1]).append("|")
				.append(tabla[1][2] == null ? "   " : tabla[1][2]).append("|")
				.append(tabla[1][3] == null ? "   " : tabla[1][3]).append("|" + "|")
				.append(tabla[1][4] == null ? "   " : tabla[1][4]).append("|")
				.append(tabla[1][5] == null ? "   " : tabla[1][5]).append("|")
				.append(tabla[1][6] == null ? "   " : tabla[1][6]).append("|")
				.append(tabla[1][7] == null ? "   " : tabla[1][7]).append("|\n");

		sb.append("|3|").append(tabla[2][0] == null ? "   " : tabla[2][0]).append("|")
				.append(tabla[2][1] == null ? "   " : tabla[2][1]).append("|")
				.append(tabla[2][2] == null ? "   " : tabla[2][2]).append("|")
				.append(tabla[2][3] == null ? "   " : tabla[2][3]).append("|" + "|")
				.append(tabla[2][4] == null ? "   " : tabla[2][4]).append("|")
				.append(tabla[2][5] == null ? "   " : tabla[2][5]).append("|")
				.append(tabla[2][6] == null ? "   " : tabla[2][6]).append("|")
				.append(tabla[2][7] == null ? "   " : tabla[2][7]).append("|\n");

		sb.append("|4|").append(tabla[3][0] == null ? "   " : tabla[3][0]).append("|")
				.append(tabla[3][1] == null ? "   " : tabla[3][1]).append("|")
				.append(tabla[3][2] == null ? "   " : tabla[3][2]).append("|")
				.append(tabla[3][3] == null ? "   " : tabla[3][3]).append("|" + "|")
				.append(tabla[3][4] == null ? "   " : tabla[3][4]).append("|")
				.append(tabla[3][5] == null ? "   " : tabla[3][5]).append("|")
				.append(tabla[3][6] == null ? "   " : tabla[3][6]).append("|")
				.append(tabla[3][7] == null ? "   " : tabla[3][7]).append("|\n");

		sb.append("|5|").append(tabla[4][0] == null ? "   " : tabla[4][0]).append("|")
				.append(tabla[4][1] == null ? "   " : tabla[4][1]).append("|")
				.append(tabla[4][2] == null ? "   " : tabla[4][2]).append("|")
				.append(tabla[4][3] == null ? "    " : tabla[4][3]).append("|" + "|")
				.append(tabla[4][4] == null ? "   " : tabla[4][4]).append("|")
				.append(tabla[4][5] == null ? "   " : tabla[4][5]).append("|")
				.append(tabla[4][6] == null ? "   " : tabla[4][6]).append("|")
				.append(tabla[4][7] == null ? "   " : tabla[4][7]).append("|\n");

		sb.append("|Rp|").append(tabla[5][0] == null ? "   " : tabla[5][0]).append("|")
				.append(tabla[5][1] == null ? "   " : tabla[5][1]).append("|")
				.append(tabla[5][2] == null ? "   " : tabla[5][2]).append("|")
				.append(tabla[5][3] == null ? "    " : tabla[5][3]).append("|\n");

		return sb.toString();
	}
	
	public void colocarProfundidadLabradoPesado(List<Medida> medidasProfundidadLabrado,
												Map<String,Object> parametros,
												FormateadorMedidasInterface formateadorMedidas) {
		
		for (Medida m : medidasProfundidadLabrado) {
			switch (m.getTipoMedida().getTipoMedidaId()) {
			case PROFUNDIDAD_LABRADO_EJE1_IZQ:
				parametros.put("prof_labrado_eje1_izq",formateadorMedidas.formatearMedida(m));
				break;
			case PROFUNDIDAD_LABRADO_EJE1_DER:
				parametros.put("prof_labrado_eje1_der",formateadorMedidas.formatearMedida(m));
				break;		
			case PROFUNDIDAD_LABRADO_EJE2_IZQ_2:
				parametros.put("prof_labrado_eje2_izq_2",formateadorMedidas.formatearMedida(m));
				break;
			case PROFUNDIDAD_LABRADO_EJE2_IZQ_1:
				parametros.put("prof_labrado_eje2_izq_1",formateadorMedidas.formatearMedida(m));
				break;
			case PROFUNDIDAD_LABRADO_EJE2_DER_1:
				parametros.put("prof_labrado_eje2_der_1",formateadorMedidas.formatearMedida(m));
				break;
			case PROFUNDIDAD_LABRADO_EJE2_DER_2:
				parametros.put("prof_labrado_eje2_der_2",formateadorMedidas.formatearMedida(m));
				break;			
			case PROFUNDIDAD_LABRADO_EJE3_IZQ_2:
				parametros.put("prof_labrado_eje3_izq_2",formateadorMedidas.formatearMedida(m));
				break;
			case PROFUNDIDAD_LABRADO_EJE3_IZQ_1:
				parametros.put("prof_labrado_eje3_izq_1",formateadorMedidas.formatearMedida(m));
				break;
			case PROFUNDIDAD_LABRADO_EJE3_DER_1:
				parametros.put("prof_labrado_eje3_der_1",formateadorMedidas.formatearMedida(m));
				break;
			case PROFUNDIDAD_LABRADO_EJE3_DER_2:
			parametros.put("prof_labrado_eje3_der_2",formateadorMedidas.formatearMedida(m));
				break;
			case PROFUNDIDAD_LABRADO_EJE4_IZQ_2:
				parametros.put("prof_labrado_eje4_izq_2",formateadorMedidas.formatearMedida(m));
				break;
			case PROFUNDIDAD_LABRADO_EJE4_IZQ_1:
				parametros.put("prof_labrado_eje4_izq_1",formateadorMedidas.formatearMedida(m));
				break;
			case PROFUNDIDAD_LABRADO_EJE4_DER_1:
				parametros.put("prof_labrado_eje4_der_1",formateadorMedidas.formatearMedida(m));
				break;
			case PROFUNDIDAD_LABRADO_EJE4_DER_2:
				parametros.put("prof_labrado_eje4_der_2",formateadorMedidas.formatearMedida(m));
				break;
			case PROFUNDIDAD_LABRADO_EJE5_IZQ_2:
				parametros.put("prof_labrado_eje5_izq_2",formateadorMedidas.formatearMedida(m));
				break;
			case PROFUNDIDAD_LABRADO_EJE5_IZQ_1:
				parametros.put("prof_labrado_eje5_izq_1",formateadorMedidas.formatearMedida(m));
				break;
			case PROFUNDIDAD_LABRADO_EJE5_DER_1:
				parametros.put("prof_labrado_eje5_der_1",formateadorMedidas.formatearMedida(m));
				break;
			case PROFUNDIDAD_LABRADO_EJE5_DER_2:
				parametros.put("prof_labrado_eje5_der_2",formateadorMedidas.formatearMedida(m));
				break;
			case PROFUNDIDAD_LABRADO_REPUESTO_DER:
				parametros.put("prof_labrado_repuesto_der",formateadorMedidas.formatearMedida(m));
				break;
			case PROFUNDIDAD_LABRADO_REPUESTO_IZQ:
				parametros.put("prof_labrado_repuesto_izq",formateadorMedidas.formatearMedida(m));
				break;
			}
		}
	}

	public String formatearMedidasProfundidadLabrado(Optional<Double> valorDelanteraDerecha,
			Optional<Double> valorDelanteraIzquierda, Optional<Double> valorTraseraDerecha,
			Optional<Double> valorTraseraIzquierda, Optional<Double> valorRepuesto) {
		StringBuilder sb = new StringBuilder();
		DecimalFormat df = new DecimalFormat("0.0");

		String strDD = valorDelanteraDerecha.isPresent() ? df.format(valorDelanteraDerecha.get()) : "   ";
		String strDI = valorDelanteraIzquierda.isPresent() ? df.format(valorDelanteraIzquierda.get()) : "  ";
		String strTD = valorTraseraDerecha.isPresent() ? df.format(valorTraseraDerecha.get()) : "   ";
		String strTI = valorTraseraIzquierda.isPresent() ? df.format(valorTraseraIzquierda.get()) : "   ";
		String strRepuesto = valorRepuesto.isPresent() ? df.format(valorRepuesto.get()) : "   ";
		sb.append("--------------------").append("\n");
		sb.append("-DD:").append(strDD).append("- DI:").append(strDI).append("-").append("\n");
		;
		sb.append("--------------------").append("\n");
		;
		sb.append("-TD:").append(strTD).append("- TI:").append(strTI).append("-").append("\n");
		sb.append("--------------------").append("\n");
		;
		sb.append(strRepuesto).append("\t");
		;
		return sb.toString();
	}

	@Deprecated
	public String formatearMedidasProfundidadLabradoMoto(Optional<Double> valorDelanteraDerecha,
			Optional<Double> valorTraseraDerecha, Optional<Double> valorTraseraIzquierda) {
		StringBuilder sb = new StringBuilder();
		DecimalFormat df = new DecimalFormat("0.0");
		String strDD = valorDelanteraDerecha.isPresent() ? df.format(valorDelanteraDerecha.get()) : "   ";
		String strTD = valorTraseraDerecha.isPresent() ? df.format(valorTraseraDerecha.get()) : "   ";
		String strTI = valorTraseraIzquierda.isPresent() ? df.format(valorTraseraIzquierda.get()) : "";

		sb.append("-EJE I :").append(strDD).append("--").append("\n");
		;
		sb.append("-EJE II :").append(strTD).append("--").append(strTI).append("\n");

		return sb.toString();
	}

	public void colocarMedidasProfundidadLabradoMoto(Optional<Medida> valorDelanteraDerecha,Optional<Medida> valorDelanteraIzquierda,
			Optional<Medida> valorTraseraDerecha, Optional<Medida> valorTraseraIzquierda,Optional<Medida> valorRepuesto,
			Map<String, Object> parametros,FormateadorMedidasInterface formateadorMedidas, Optional<Medida> valorPosteriorDerechaSecundaria, Optional<Medida> valorPosteriorIzquierdaSecundaria) {

		
		String strDD = valorDelanteraDerecha.isPresent() ? formateadorMedidas.formatearMedida(valorDelanteraDerecha.get()) : "   ";
		String strDI = valorDelanteraIzquierda.isPresent() ? formateadorMedidas.formatearMedida(valorDelanteraIzquierda.get()) : " ";
		String strTD = valorTraseraDerecha.isPresent() ? formateadorMedidas.formatearMedida(valorTraseraDerecha.get()) : "   ";
		String strTI = valorTraseraIzquierda.isPresent() ? formateadorMedidas.formatearMedida(valorTraseraIzquierda.get()) : "";
		String strRepuesto = valorRepuesto.isPresent() ? formateadorMedidas.formatearMedida(valorRepuesto.get()): " ";

		parametros.put("prof_labrado_eje1_der", strDD);
		parametros.put("prof_labrado_eje1_izq",strDI);
		parametros.put("prof_labrado_eje2_der_1", strTD);
		parametros.put("prof_labrado_eje2_izq_1", strTI);
		parametros.put("prof_labrado_repuesto_der",strRepuesto);
		
		if(valorPosteriorDerechaSecundaria.isPresent()) {
			String strPD2 = formateadorMedidas.formatearMedida(valorPosteriorDerechaSecundaria.get());
			parametros.put("prof_labrado_eje2_der_2",strPD2);
		}
		
		if(valorPosteriorIzquierdaSecundaria.isPresent()) {
			String strPI2 = formateadorMedidas.formatearMedida(valorPosteriorIzquierdaSecundaria.get());
			parametros.put("prof_labrado_eje2_izq_2",strPI2);
		}
	}

	public void colocarMedidasProfundidadLabrado(Optional<Medida> valorDelanteraDerecha,
			Optional<Medida> valorDelanteraIzquierda, Optional<Medida> valorTraseraDerecha,
			Optional<Medida> valorTraseraIzquierda, Optional<Medida> valorRepuesto,
			Map<String,Object> parametros,FormateadorMedidasInterface formateadorMedidas) {	

		String strDD = valorDelanteraDerecha.isPresent() ? formateadorMedidas.formatearMedida(valorDelanteraDerecha.get()) : "   ";
		String strDI = valorDelanteraIzquierda.isPresent() ? formateadorMedidas.formatearMedida(valorDelanteraIzquierda.get()) : "  ";
		String strTD = valorTraseraDerecha.isPresent() ? formateadorMedidas.formatearMedida(valorTraseraDerecha.get()) : "   ";
		String strTI = valorTraseraIzquierda.isPresent() ? formateadorMedidas.formatearMedida(valorTraseraIzquierda.get()) : "   ";
		String strRepuesto = valorRepuesto.isPresent() ? formateadorMedidas.formatearMedida(valorRepuesto.get()) : "   ";
		
		parametros.put("prof_labrado_eje1_der",strDD);
		parametros.put("prof_labrado_eje1_izq",strDI);
		parametros.put("prof_labrado_eje2_der_1",strTD);
		parametros.put("prof_labrado_eje2_izq_1",strTI);
		parametros.put("prof_labrado_repuesto_der",strRepuesto);
		
	}

}