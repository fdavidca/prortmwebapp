package com.proambiente.webapp.service.ci2;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Response;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.tempuri.ConsultaPinPlacaResponse;
import org.tempuri.Datos;
import org.tempuri.DatosSoap;
import org.tempuri.EchoResponse;
import org.tempuri.Formulario;
import org.tempuri.IngresarFurResponse;
import org.tempuri.Pin;
import org.tempuri.Resultado;
import org.tempuri.UtilizarPinResponse;

import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.NivelAlerta;
import com.proambiente.modelo.OperadorSicov;
import com.proambiente.webapp.dao.InformacionCdaFacade;
import com.proambiente.webapp.service.PinNoEncontradoException;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.service.notification.NotificationPrimariaService;

@Named
public class ManejadorCi2SicovService implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6663468442414747824L;
	public static final String TRANSACCION_EXITOSA = "0000";
	public static final String TRANSACCION_FALLIDA = "1000";
	public static final String DATO_NO_PUEDE_SER_NULO = "1001";
	public static final String VALOR_NO_VALIDO = "1002";
	public static final String FORMATO_NO_VALIDO = "1003";
	public static final String CAMPO_OBLIGATORIO = "1004";
	public static final String LONGITUD_NO_PERMITIDA = "1005";
	public static final String DATO_NO_EXISTE = "1006";
	public static final String USUARIO_CLAVE_INVALIDOS = "2001";
	public static final String USUARIO_NO_PERMITIDO = "2002";
	public static final String CDA_NO_PERMITIDO = "2003";
	public static final String VEHICULO_NO_PERMITIDO = "2004";
	public static final String PIN_ANULADO = "2005";
	public static final String PIN_DISPONIBLE = "2006";
	public static final String PIN_UTILIZADO = "2007";
	public static final String PIN_REPORTADO_CON_FUR = "2008";
	public static final String PIN_NO_VALIDO = "2009";

	private static final String FORMATO_ERROR = "Error en operacion %s para placa %s, el motivo es %s";

	private static final String CONSULTA_PLACA_PIN = "Consultar pin placa ";
	private static final String UTILIZAR_PIN = "Utilizar pin";
	private static final String REPORTE_FUR = "Reporte FUR";

	private static final Logger logger = Logger.getLogger(ManejadorCi2SicovService.class.getName());

	private PinInformationExtractor extractorInformacionPin = new PinInformationExtractor();

	private static final Boolean GENERAR_ALERTA = true;
	private static final Boolean NO_GENERAR_ALERTA = false;

	public static final String PRIMERA_INSPECCION = "1";
	public static final String SEGUNDA_INSPECCION = "2";

	DatosSoap datosSoap;

	@Inject
	NotificationPrimariaService notificacionService;

	@Inject
	InformacionCdaFacade informacionCDA;

	@Resource
	ManagedExecutorService executorService;

	@Inject
	RevisionService revisionService;
	
	@PostConstruct
	public void init() {
		datosSoap = crearDatosSoap();
	}

	DatosSoap crearDatosSoap() {
		InformacionCda infoCda = informacionCDA.find(1);
		String urlWebService = infoCda.getUrlWebService();
		DatosSoap datosSoap = null;
		try {
			if(infoCda.getOperadorSicov().equals(OperadorSicov.CI2)){
				URL url = new URL(urlWebService);
				//URL baseUrl = WebServiceClienteCi2Producer.class.getClassLoader().getResource(".");
				//URL url = new URL("http://localhost/wsdl/SOAService.wsdl");//new URL(baseUrl, "META-INF/wsdl/sincrofur.asmx.wsdl");
				Datos datos = new Datos(url);
				datosSoap = datos.getDatosSoap();
				BindingProvider bp = ((BindingProvider) datosSoap);
				bp.getRequestContext().put("javax.xml.ws.client.connectionTimeout", "600000");
				bp.getRequestContext().put("javax.xml.ws.client.receiveTimeout", "600000");
			}
		} catch (MalformedURLException e) {			
			notificacionService.sendNotification("Url del web service mal configurada" + urlWebService + e.getMessage(), NivelAlerta.ERROR_GRAVE, Boolean.TRUE);
		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Error creando cliente web service ci2", exc);
			notificacionService.sendNotification("Error creando cliente web service ci2 " + exc.getMessage(), NivelAlerta.ERROR_GRAVE, Boolean.TRUE);
			
		}
		return datosSoap;
	}

	
	static Map<String, String> mapaMensajesError = new HashMap<>();;

	static {
		mapaMensajesError.put(TRANSACCION_EXITOSA, "Transaccción exitosa no hace parte del flujo de datos correcto");
		mapaMensajesError.put(TRANSACCION_FALLIDA, "Transaccion Fallida");
		mapaMensajesError.put(DATO_NO_PUEDE_SER_NULO, "Dato no puede ser nulo");
		mapaMensajesError.put(VALOR_NO_VALIDO, "Valor inválido");
		mapaMensajesError.put(FORMATO_NO_VALIDO, "Formato no válido");
		mapaMensajesError.put(CAMPO_OBLIGATORIO, "Campo obligatorio");
		mapaMensajesError.put(LONGITUD_NO_PERMITIDA, "Longitud no permitida");
		mapaMensajesError.put(DATO_NO_EXISTE, "Dato no existe");
		mapaMensajesError.put(USUARIO_CLAVE_INVALIDOS, "Usuario clave inválidos");
		mapaMensajesError.put(USUARIO_NO_PERMITIDO, "Usuario no permitido");
		mapaMensajesError.put(CDA_NO_PERMITIDO, "Cda no permitido");
		mapaMensajesError.put(VEHICULO_NO_PERMITIDO, "Vehículo no permitido");
		mapaMensajesError.put(PIN_ANULADO, "Pin anulado");
		mapaMensajesError.put(PIN_DISPONIBLE, "Pin disponible no debe manejarse como error ");
		mapaMensajesError.put(PIN_UTILIZADO, "Pin utilizado");
		mapaMensajesError.put(PIN_REPORTADO_CON_FUR, "Pin reportado con fur");
		mapaMensajesError.put(PIN_NO_VALIDO, "Pin no valido");

	}

	public void reportarFur(Formulario formulario,Integer revisionId,Integer numeroEnvio) {
		
		logger.log( Level.INFO, "Enviando formulario " + formulario.toString() );
		Response<IngresarFurResponse> response = datosSoap.ingresarFurAsync(formulario);
		executorService.execute(() -> {
			try {
				IngresarFurResponse responseFur = response.get(300, TimeUnit.SECONDS);
				Resultado resultado = responseFur.getIngresarFurResult();				
				logger.log(Level.INFO, "Respuesta a reporte fur :" + resultado.toString());
				manejarRespuestaReporteFur(resultado, formulario, REPORTE_FUR,revisionId,numeroEnvio);
			} catch (Exception exc) {
				logger.log(Level.SEVERE, "Excepcion al reportar fur " + formulario.getP3Plac(), exc);
				notificacionService.sendNotification(" Excepcion en ingresar fur " + exc.getMessage(),
						NivelAlerta.ERROR_GRAVE, GENERAR_ALERTA);
			}

		});
	}

	public void consultarPinPlaca(String placa) {
		
		
		logger.log(Level.INFO, "Consultando pin placa para : " + placa);
		Pin pin = producirPinUsuarioClave();
		pin.setPPlaca(placa);

		Response<ConsultaPinPlacaResponse> response = datosSoap.consultaPinPlacaAsync(pin);
		executorService.execute(() -> {
			try {
				ConsultaPinPlacaResponse cr = response.get(120,TimeUnit.SECONDS);
				Resultado resultado = cr.getConsultaPinPlacaResult();				
				logger.log(Level.INFO, "Resultado consultar pin placa " + resultado);
				manejarResultadoConsultaPinPlaca(resultado, placa, CONSULTA_PLACA_PIN);
			} catch (InterruptedException e) {
				logger.log(Level.SEVERE, "Error interrupcion consultando placa" + placa, e);
			} catch (ExecutionException e) {
				logger.log(Level.SEVERE, "Error consultando pin placa " + placa, e);
				notificacionService.sendNotification(" Excepcion consultando pin placa " + e.getMessage(),
						NivelAlerta.ERROR_GRAVE, GENERAR_ALERTA);
			} catch (Exception exc) {
				logger.log(Level.SEVERE, "Error consultando pin placa " + placa, exc);
				notificacionService.sendNotification(" Excepcion consultando pin placa " + exc.getMessage(),
						NivelAlerta.ERROR_GRAVE, GENERAR_ALERTA);
			}

		});

	}
	
	public String consultarPinPlacaSincrono(String placa) throws PinNoEncontradoException{
		
		
			logger.log(Level.INFO, "Consultando pin para placa " + placa );
			Pin pin = producirPinUsuarioClave();
			pin.setPPlaca(placa);
			Resultado resultado = datosSoap.consultaPinPlaca(pin);			
			logger.log(Level.INFO, "Resultado consultar pin placa sincrono " + resultado);
			Optional<String> optPin = extraerPin(resultado);
			if(optPin.isPresent()){
				return optPin.get();
			}else {
				logger.log(Level.SEVERE, "no se pudo consultar pin para placa " + placa + resultado.toString());
				throw new PinNoEncontradoException("no se pudo consultar pin para placa " + placa + resultado.toString());				
			}		
	}
	
	
	public Optional<String> extraerPin(Resultado resultado){
		if (resultado.getCodigoRespuesta().equalsIgnoreCase(PIN_DISPONIBLE)){
			PinInformationExtractor pinExtractor = new PinInformationExtractor();
			InformacionPin infoPin = pinExtractor.extraerInformacionPin(resultado.getMensajeRespuesta());
			return Optional.of(infoPin.getPin());
		}else {
			
			logger.log(Level.INFO, "Resultado consultar pin no exitoso  " + resultado.toString());
			return Optional.empty();
		}
	}
	
	public Resultado utilizarPinSincrono(String pinStr,String placa,String tipoRtm){
		try{
			
			StringBuilder sb = new StringBuilder();
			sb.append("Utilizar pin ").append(pinStr).append(" Placa ").append(placa);
			logger.log(Level.INFO, sb.toString() );			
			Pin pin = producirPinUsuarioClave();
			pin.setPPin(pinStr);
			pin.setPPlaca(placa);
			pin.setPTipoRtm(tipoRtm);
			Resultado resultado = datosSoap.utilizarPin(pin);			
			logger.log(Level.INFO, "Resultado utilizar pin sincrono : " + resultado);
			return resultado;			
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error utilizar pin" + placa, exc);
			throw exc;
		}
	}
	
	
	

	public void enviarEco(String ecoMensaje) {
		Response<EchoResponse> response = datosSoap.echoAsync(ecoMensaje);
		executorService.execute(() -> {
			try {
				EchoResponse er = response.get(120,TimeUnit.SECONDS);
				String echoResult = er.getEchoResult();
				notificacionService.sendNotification("Respuesta a echo ci2" + echoResult, NivelAlerta.INFORMACION,
						NO_GENERAR_ALERTA);
			} catch (Exception exc) {
				logger.log(Level.SEVERE, "Excepcion en echo ci2", exc);
				notificacionService.sendNotification("Excepcion en echo ci2", NivelAlerta.ERROR, GENERAR_ALERTA);
			}
		});
	}

	public void utilizarPin(String pinStr, String tipoRtm, String placa) {
		
		StringBuilder sb = new StringBuilder();
		sb.append("Utilizar pin ").append(pinStr).append(" Placa ").append(placa);
		logger.log(Level.INFO, sb.toString() );
		Pin pin = producirPinUsuarioClave();
		pin.setPPin(pinStr);
		pin.setPTipoRtm(tipoRtm);
		pin.setPPlaca(placa);
		Response<UtilizarPinResponse> response = datosSoap.utilizarPinAsync(pin);
		executorService.execute(() -> {
			try {
				
				UtilizarPinResponse pinResponse = response.get(120,TimeUnit.SECONDS);
				Resultado resultado = pinResponse.getUtilizarPinResult();
				
				logger.log(Level.INFO, "Resultado utilizar Pin asincrono " + resultado);
				manejarResultadoUtilizarPin(resultado, placa, UTILIZAR_PIN);

			} catch (Exception exc) {
				logger.log(Level.SEVERE, "Excepcion en utilizarPin ci2", exc);
				notificacionService.sendNotification("Excepcion en echo utilizarPin ", NivelAlerta.ERROR,
						GENERAR_ALERTA);
			}
		});
	}

	private void manejarRespuestaReporteFur(Resultado resultado, Formulario formulario, String operacion,Integer revisionId,Integer numeroEnvio) {
			String codigoRespuesta = resultado.getCodigoRespuesta();
			if(codigoRespuesta.equals(TRANSACCION_EXITOSA)){
				//
				try {
					if(numeroEnvio.equals(1)) {
						revisionService.registrarPrimerEnvio(revisionId);
					}else {
						revisionService.registrarSegundoEnvio(revisionId);
					}
					notificacionService.sendNotification("FUR reportado exitosamente",NivelAlerta.INFORMACION, NO_GENERAR_ALERTA);
				}catch(Exception exc) {
					logger.log(Level.SEVERE, "Error  registrando primer envio", exc);
				}
			}else if(codigoRespuesta.equals(PIN_DISPONIBLE)) {
				notificacionService.sendNotification("ERROR FATAL FUR no pudo ser validado porque PIN no se ha utilizado " + resultado.toString(), NivelAlerta.ERROR_GRAVE, GENERAR_ALERTA);
			
			}else {
				enviarNotificacionError(resultado,formulario.getP3Plac(),operacion);
			}
	}

	private void manejarResultadoConsultaPinPlaca(Resultado resultado, String placa, String operacion) {
		String codigoRespuesta = resultado.getCodigoRespuesta();
		if (codigoRespuesta.equals(PIN_DISPONIBLE)) {
			String pin = extraerPin(resultado.getMensajeRespuesta());
			revisionService.registrarPinParaRevision(placa, pin);
			notificacionService.sendNotification("Pin consultado exitosamente para placa" + placa, NivelAlerta.INFORMACION, GENERAR_ALERTA);
		} else {
			enviarNotificacionError(resultado, placa, operacion);
		}
	}

	private void manejarResultadoUtilizarPin(Resultado resultado, String placa, String operacion) {
		String codigoRespuesta = resultado.getCodigoRespuesta();
		if (codigoRespuesta.equals(TRANSACCION_EXITOSA) || codigoRespuesta.equals(PIN_UTILIZADO)) {
			notificacionService.sendNotification("Se utilizo pin para " + placa,
					NivelAlerta.INFORMACION, GENERAR_ALERTA);

		} else {
			enviarNotificacionError(resultado, placa, operacion);
		}
	}

	private String extraerPin(String cadena) {
		InformacionPin informacionPin = extractorInformacionPin.extraerInformacionPin(cadena);
		return informacionPin.getPin();
	}

	private void enviarNotificacionError(Resultado resultado, String placa, String operacion) {
		String codigoRespuesta = resultado.getCodigoRespuesta();
		String mensajeError = "";
		String infoError = "";
		switch (codigoRespuesta) {

		case TRANSACCION_EXITOSA:
			throw new RuntimeException("Caso " + TRANSACCION_EXITOSA + "No es error");

		case CAMPO_OBLIGATORIO:
			infoError = String.format(FORMATO_ERROR, operacion, placa, mapaMensajesError.get(CAMPO_OBLIGATORIO));
			mensajeError = generarMensajeError(infoError, resultado);
			notificacionService.sendNotification(mensajeError, NivelAlerta.ERROR_GRAVE, GENERAR_ALERTA);
			break;

		case CDA_NO_PERMITIDO:
			infoError = String.format(FORMATO_ERROR, operacion, placa, mapaMensajesError.get(CDA_NO_PERMITIDO));
			mensajeError = generarMensajeError(infoError, resultado);
			notificacionService.sendNotification(mensajeError, NivelAlerta.ERROR_GRAVE, GENERAR_ALERTA);
			break;

		case DATO_NO_EXISTE:
			infoError = String.format(FORMATO_ERROR, operacion, placa, mapaMensajesError.get(DATO_NO_EXISTE));
			mensajeError = generarMensajeError(infoError, resultado);
			notificacionService.sendNotification(mensajeError, NivelAlerta.ERROR_GRAVE, GENERAR_ALERTA);
			break;

		case DATO_NO_PUEDE_SER_NULO:
			infoError = String.format(FORMATO_ERROR, operacion, placa, mapaMensajesError.get(DATO_NO_PUEDE_SER_NULO));
			mensajeError = generarMensajeError(infoError, resultado);
			notificacionService.sendNotification(mensajeError, NivelAlerta.ERROR_GRAVE, GENERAR_ALERTA);
			break;

		case FORMATO_NO_VALIDO:
			infoError = String.format(FORMATO_ERROR, operacion, placa, mapaMensajesError.get(FORMATO_NO_VALIDO));
			mensajeError = generarMensajeError(infoError, resultado);
			notificacionService.sendNotification(mensajeError, NivelAlerta.ERROR_GRAVE, GENERAR_ALERTA);
			break;
		case LONGITUD_NO_PERMITIDA:
			infoError = String.format(FORMATO_ERROR, operacion, placa, mapaMensajesError.get(LONGITUD_NO_PERMITIDA));
			mensajeError = generarMensajeError(infoError, resultado);
			notificacionService.sendNotification(mensajeError, NivelAlerta.ERROR_GRAVE, GENERAR_ALERTA);
			break;

		case PIN_ANULADO:
			infoError = String.format(FORMATO_ERROR, operacion, placa, mapaMensajesError.get(PIN_ANULADO));
			mensajeError = generarMensajeError(infoError, resultado);
			notificacionService.sendNotification(mensajeError, NivelAlerta.ERROR_GRAVE, GENERAR_ALERTA);
			break;
		case PIN_DISPONIBLE:
			infoError = String.format(FORMATO_ERROR, operacion, placa, mapaMensajesError.get(PIN_DISPONIBLE));
			mensajeError = generarMensajeError(infoError, resultado);
			notificacionService.sendNotification(mensajeError, NivelAlerta.ERROR_GRAVE, GENERAR_ALERTA);
		break;	
		case PIN_NO_VALIDO:
			infoError = String.format(FORMATO_ERROR, operacion, placa, mapaMensajesError.get(PIN_NO_VALIDO));
			mensajeError = generarMensajeError(infoError, resultado);
			notificacionService.sendNotification(mensajeError, NivelAlerta.ERROR_GRAVE, GENERAR_ALERTA);
			break;

		case PIN_REPORTADO_CON_FUR:
			infoError = String.format(FORMATO_ERROR, operacion, placa, mapaMensajesError.get(PIN_REPORTADO_CON_FUR));
			mensajeError = generarMensajeError(infoError, resultado);
			notificacionService.sendNotification(mensajeError, NivelAlerta.ERROR_GRAVE, GENERAR_ALERTA);
			break;

		case PIN_UTILIZADO:
			infoError = String.format(FORMATO_ERROR, operacion, placa, mapaMensajesError.get(PIN_UTILIZADO));
			mensajeError = generarMensajeError(infoError, resultado);
			notificacionService.sendNotification(mensajeError, NivelAlerta.ERROR_GRAVE, GENERAR_ALERTA);
			break;
		case USUARIO_CLAVE_INVALIDOS:
			infoError = String.format(FORMATO_ERROR, operacion, placa, mapaMensajesError.get(USUARIO_CLAVE_INVALIDOS));
			mensajeError = generarMensajeError(infoError, resultado);
			notificacionService.sendNotification(mensajeError, NivelAlerta.ERROR_GRAVE, GENERAR_ALERTA);
			break;

		case USUARIO_NO_PERMITIDO:
			infoError = String.format(FORMATO_ERROR, operacion, placa, mapaMensajesError.get(USUARIO_NO_PERMITIDO));
			mensajeError = generarMensajeError(infoError, resultado);
			notificacionService.sendNotification(mensajeError, NivelAlerta.ERROR_GRAVE, GENERAR_ALERTA);
			break;
		case VALOR_NO_VALIDO:
			infoError = String.format(FORMATO_ERROR, operacion, placa, mapaMensajesError.get(VALOR_NO_VALIDO));
			mensajeError = generarMensajeError(infoError, resultado);
			notificacionService.sendNotification(mensajeError, NivelAlerta.ERROR_GRAVE, GENERAR_ALERTA);
			break;

		case VEHICULO_NO_PERMITIDO:
			infoError = String.format(FORMATO_ERROR, operacion, placa, mapaMensajesError.get(VEHICULO_NO_PERMITIDO));
			mensajeError = generarMensajeError(infoError, resultado);
			notificacionService.sendNotification(mensajeError, NivelAlerta.ERROR_GRAVE, GENERAR_ALERTA);
			break;
		case TRANSACCION_FALLIDA:
			infoError = String.format(FORMATO_ERROR, operacion, placa, mapaMensajesError.get(TRANSACCION_FALLIDA));
			mensajeError = generarMensajeError(infoError, resultado);
			notificacionService.sendNotification(mensajeError, NivelAlerta.ERROR_GRAVE, GENERAR_ALERTA);
			break;
		default:
			infoError = String.format(FORMATO_ERROR, operacion, placa, resultado.getCodigoRespuesta());
			mensajeError = generarMensajeError(infoError, resultado);
			notificacionService.sendNotification(mensajeError, NivelAlerta.ERROR_GRAVE, GENERAR_ALERTA);
			break;
		}

	}

	private Pin producirPinUsuarioClave() {
		Pin pin = new Pin();
		InformacionCda informacionCda = informacionCDA.find(1);
		StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
		encryptor.setPassword("cMB8apv23");
		String nombreUsuario = encryptor.decrypt(informacionCda.getUsuarioCi2());
		String password = encryptor.decrypt(informacionCda.getPasswordCi2());
		pin.setClave(password);
		pin.setUsuario(nombreUsuario);
		pin.setPTipoRtm("");
		pin.setPPin("");
		return pin;
	}

	private String generarMensajeError(String causa, Resultado resultado) {
		StringBuilder sb = new StringBuilder("ERROR ");
		sb.append(causa).append(" ").append(resultado.toString());
		return sb.toString();
	}

}
