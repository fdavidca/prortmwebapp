package com.proambiente.webapp.service.ci2;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.proambiente.webapp.service.sicov.UtilEncontrarPruebas.encontrarPruebaPorTipo;
import static com.proambiente.webapp.service.util.UtilTipoVehiculo.isVehiculoMoto;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.codec.binary.Base64;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.tempuri.FormularioV2;

import com.proambiente.modelo.Defecto;
import com.proambiente.modelo.DefectoAsociado;
import com.proambiente.modelo.EstadoDefecto;
import com.proambiente.modelo.Foto;
import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.Inspeccion;
import com.proambiente.modelo.Medida;
import com.proambiente.modelo.Propietario;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Usuario;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.modelo.dto.PermisibleDieselDTO;
import com.proambiente.modelo.dto.PermisibleOttoVehiculoDTO;
import com.proambiente.modelo.dto.PermisibleSuspensionDTO;
import com.proambiente.modelo.dto.PermisibleTaximetroDTO;
import com.proambiente.modelo.dto.PermisiblesDesviacionDTO;
import com.proambiente.modelo.dto.PermisiblesFrenosDTO;
import com.proambiente.modelo.dto.PermisiblesLucesDTO;
import com.proambiente.modelo.dto.PermisiblesMotoDTO;
import com.proambiente.modelo.dto.ResultadoDieselDTO;
import com.proambiente.modelo.dto.ResultadoFasDTO;
import com.proambiente.modelo.dto.ResultadoOttoMotocicletasDTO;
import com.proambiente.modelo.dto.ResultadoOttoVehiculosDTO;
import com.proambiente.modelo.dto.ResultadoSonometriaDTO;
import com.proambiente.modelo.dto.ResultadoTaximetroDTO;
import com.proambiente.webapp.dao.EquipoFacade;
import com.proambiente.webapp.dao.FotoFacade;
import com.proambiente.webapp.dao.InformacionCdaFacade;
import com.proambiente.webapp.dao.MedidaFacade;
import com.proambiente.webapp.dao.PermisibleFacade;
import com.proambiente.webapp.dao.UsuarioFacade;
import com.proambiente.webapp.service.CertificadoService;
import com.proambiente.webapp.service.DefectoService;
import com.proambiente.webapp.service.DeterminadorCumpleEnsenianza;
import com.proambiente.webapp.service.InspeccionService;
import com.proambiente.webapp.service.PruebasService;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.service.UtilInformacionEquipos;
import com.proambiente.webapp.service.UtilInformacionProfundidadLabrado;
import com.proambiente.webapp.service.generadordto.GeneradorDTOPermisibles;
import com.proambiente.webapp.service.generadordto.GeneradorDTOProfundidadLabrado;
import com.proambiente.webapp.service.generadordto.GeneradorDTOResultadoLuces;
import com.proambiente.webapp.service.generadordto.GeneradorDTOResultados;
import com.proambiente.webapp.service.generadordto.GeneradorPresionLlantasDTO;
import com.proambiente.webapp.service.util.UtilInformesMedidas;
import com.proambiente.webapp.service.util.UtilTipoVehiculo;
import com.proambiente.webapp.util.ConstantesNombreSoftware;
import com.proambiente.webapp.util.ConstantesTiposCombustible;
import com.proambiente.webapp.util.ConstantesTiposPrueba;
import com.proambiente.webapp.util.ConstantesTiposVehiculo;
import com.proambiente.webapp.util.dto.ResultadoDieselDTOInformes;
import com.proambiente.webapp.util.dto.ResultadoLucesDtoV2;
import com.proambiente.webapp.util.dto.ResultadoOttoInformeDTO;
import com.proambiente.webapp.util.dto.ResultadoPresionLlantasDTO;
import com.proambiente.webapp.util.dto.ResultadoProfundidadLabradoDTO;



public class FurCi2CodificadorV2 implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5854101447673667692L;

	private static final Logger logger = Logger.getLogger(FurCi2CodificadorV2.class
			.getName());

	private static final String CADENA_VACIA = "";
	private static final String EQUIS = "X";
		
	@Inject
	InspeccionService inspeccionService;
	
	@Inject
	RevisionService revisionService;

	@Inject
	InformacionCdaFacade informacionCDADAO;

	@Inject
	PruebasService pruebaService;

	@Inject
	PermisibleFacade permisibleService;

	@Inject
	DefectoService defectoService;	

	@Inject
	MedidaFacade medidaDAO;	
	
	@Inject
	UsuarioFacade usuarioDAO;
	
	@Inject
	FotoFacade fotoDAO;
	
	@Inject
	CertificadoService certificadoService;
	
	@Inject
	GeneradorDTOPermisibles generadorDTOPermisibles;
	
	@Inject
	EquipoFacade equipoFacade;
	
	GeneradorDTOResultadoLuces generadorResultadoLuces = new GeneradorDTOResultadoLuces();

	DecimalFormat df = new DecimalFormat();

	SimpleDateFormat sdfFechaRevision = new SimpleDateFormat("ddMMyyyy HH:mm");
	SimpleDateFormat sdfFechaMatricula = new SimpleDateFormat("ddMMyyyy");

	
	
	UtilInformesMedidas utilInformesMedidas = new UtilInformesMedidas();
	GeneradorDTOResultados generadorResultadosDTO = new GeneradorDTOResultados();
	
	@Inject
	UtilInformacionProfundidadLabrado utilProfLabrado;

	
	/**
	 * Se supone que el certificado ya fue registrado en el software
	 * Se supone que el consecutivo runt ya fue registrado en el software
	 * @param revisionId
	 * @return
	 */
	public FormularioV2 generarFormularioSegundaEtapa(Integer revisionId){
		FormularioV2 formulario = new FormularioV2();
		try {
			setEmpty(formulario);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			logger.log(Level.SEVERE, "Error haciendo campos por defecto vacios", e);
		}
		Revision revision = revisionService.cargarRevision(revisionId);
		String pin = revision.getPin();
		if(pin == null){
			throw new RuntimeException(" Pin no asignado para revision " + revisionId);
		}
		InformacionCda informacionCda = informacionCDADAO.find(1);
		cargarAutenticacionCi2(formulario, informacionCda);
		formulario.setPPin(pin);
		formulario.setP3Plac(revision.getVehiculo().getPlaca());
		String consecutivoRunt = revision.getConsecutivoRunt();
		if( consecutivoRunt == null || consecutivoRunt.isEmpty() ){
			throw new RuntimeException("No se ha asignado consecutivo runt para la revision " + revisionId);			
		}
		formulario.setPEConRun(consecutivoRunt);		
		//si la revisión es aprobada 
		if(revision.getAprobada()){
			String ultimoConsecutivoCertificado = certificadoService.consultarUltimoConsecutivoCertificado(revisionId);
			formulario.setPTw01(ultimoConsecutivoCertificado);
		} 
		return formulario;
		
	}

	public FormularioV2 generarFormularioPrimeraEtapa(Integer revisionId) {

		FormularioV2 formulario = new FormularioV2();
		try {
			setEmpty(formulario);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			logger.log(Level.SEVERE, "Error haciendo campos por defecto vacios", e);
		}
		generadorResultadosDTO.setPuntoComoSeparadorDecimales();

		Revision revision = revisionService.cargarRevision(revisionId);
		String pin = revision.getPin();
		if(pin == null){
			throw new RuntimeException(" Pin no asignado para revision " + revisionId);
		}

		InformacionCda informacionCda = informacionCDADAO.find(1);
		// Cargar datos de usuario, clave
		cargarAutenticacionCi2(formulario,informacionCda);	
		
		formulario.setPPin(pin);
		
		String fur_aso = revision.getRevisionId() + "-" + revision.getNumeroInspecciones();
		formulario.setPFurNum(fur_aso);//provisional
		formulario.setPFurAso(fur_aso);
		formulario.setPCda(informacionCda.getNombre());
		formulario.setPNit(informacionCda.getNit());
		formulario.setPDir(informacionCda.getDireccion());		
		formulario.setPDiv(informacionCda.getCodigoDivipo());// codigo divipo
		formulario.setPCiu(informacionCda.getCiudad());
		formulario.setPTel(informacionCda.getTelefono1());
		String correoElectronicoCda = informacionCda.getCorreoElectronico() == null ? "" : informacionCda.getCorreoElectronico();
		formulario.setPEma( correoElectronicoCda );

		if(revision.getNumeroInspecciones()==2) {
			List<Inspeccion> inspecciones = inspeccionService.obtenerInspeccionesPorRevisionId(revision.getRevisionId());
			if(inspecciones != null && !inspecciones.isEmpty()) {
				Inspeccion inspeccion = inspecciones.get(0);
				if(inspeccion != null) {
					String fechaInspeccion = sdfFechaRevision.format( inspeccion.getFechaInspeccionSiguiente() );
					formulario.setP1FecPru(fechaInspeccion);
				}
			}			
		}else {
			
			String fechaRevisionFormateada = sdfFechaRevision.format(revision.getFechaCreacionRevision());
			formulario.setP1FecPru(fechaRevisionFormateada);
		}

		Propietario propietario = revision.getPropietario();
		if(propietario == null ){
			throw new RuntimeException("Propietario no configurado");
		}
		String nombreRazonSocial = propietario.getNombres() + " " + propietario.getApellidos();
		formulario.setP2NomRaz(nombreRazonSocial);
		
		
		formulario.setP2Doc(String.valueOf(propietario.getPropietarioId()));
		formulario.setP2DocTip(String.valueOf(propietario.getTipoDocumentoIdentidad().getTipoDocumentoIdentidadId()));
		formulario.setP2Dir(propietario.getDireccion());
		formulario.setP2Tel(propietario.getTelefono1());
		formulario.setP2Ciu(propietario.getCiudad().getNombre());
		formulario.setP2Dep(propietario.getCiudad().getDepartamento().getNombre());
		String email = propietario.getCorreoElectronico() == null ? "": propietario.getCorreoElectronico(); 
		formulario.setP2Ema(email);

		// SECCION DE DATOS DEL VEHICULO
		Vehiculo vehiculo = llenarInformacionVehiculo(formulario, revision);

		//// SECCION DE MEDIDAS
		List<Prueba> pruebas = pruebaService.pruebasRevision(revisionId);
		
		llenarSeccionRuido(revisionId,pruebas,formulario);
		
		llenarSeccionLuces(revisionId,pruebas,formulario,isVehiculoMoto(vehiculo.getTipoVehiculo()),revision);
		
		llenarSeccionSuspension(revisionId,pruebas,formulario,revision);
		
		llenarSeccionFrenos(pruebas,formulario,revision,isVehiculoMoto(vehiculo.getTipoVehiculo()));
		
		llenarSeccionDesviacion(pruebas,formulario,revision);
		
		llenarSeccionTaximetro(pruebas,formulario,revision);

		llenarSeccionGases(pruebas,formulario,revision);
		
		
		List<Defecto> defectosInspeccionMecanizada = defectoService.obtenerDefectosInspeccionMecanizada(revisionId);
		// cargar defectos inspeccion mecanizada
		ponerDefectosInspeccionMecanizada(revisionId, formulario,defectosInspeccionMecanizada);
		// cargar defectos inspeccion visual
		List<DefectoAsociado> defectosAsociados = defectoService.obtenerDefectosInspeccionSensorial(revisionId);
		List<Defecto> defectosInspeccionVisual = defectosAsociados.stream()
																  .filter(da->da.getEstadoDefecto().equals(EstadoDefecto.DETECTADO))
																	.map(da->da.getDefecto())
																	.collect(Collectors.toList()); 
				
		
		ponerDefectosInspeccionVisual(revisionId, formulario, defectosInspeccionVisual);
		
		
		// cargar defectos inspeccion visual ensenianza
		ponerDefectosEnsenianza(revisionId, formulario, defectosInspeccionVisual);

		// cargar nombres de usuario
		String consecutivoRunt = revision.getConsecutivoRunt() == null ? "" : revision.getConsecutivoRunt();
		formulario.setPEConRun(consecutivoRunt);

		

		if(revisionService.isRevisionAprobada(revisionId)){
			formulario.setPEApr("SI");
			if (vehiculo.getServicio().getServicioId() == 5){
				formulario.setPE1Apr("SI");
			}
		}else {
			formulario.setPEApr("NO");
			if(vehiculo.getServicio().getServicioId() == 5){
				Boolean cumpleEnsenianza = DeterminadorCumpleEnsenianza.cumpleEnsenianza(defectoService,revision.getRevisionId());
				formulario.setPE1Apr( cumpleEnsenianza ? "SI" : "NO");
			}
		}
		//cargar comentarios de prueba incluye dilucion, motivos de aborto y medidas de labrado
		StringBuilder sb = new StringBuilder();
		String comentarioRevision = revision.getComentario();
		
		String resultadoPresionLlantas = codificarPresionLLantas(pruebas);		
		sb.append(resultadoPresionLlantas);
		
		if(comentarioRevision != null){
			sb.append(comentarioRevision);
		}
		colocarComentariosPruebas(pruebas, sb);	
		
		formulario.setPFComObs(sb.toString());
		
		llenarSeccionLlantas(pruebas,formulario);		
		
		// cargar nombres de usuario
		cargarUsuarios(revisionId, formulario, pruebas);
		
		Boolean isMoto = UtilTipoVehiculo.isVehiculoMoto(vehiculo.getTipoVehiculo());
		
		//informacion de equipos
		String informacionEquipos = UtilInformacionEquipos.ponerInformacionEquipos(pruebas,pruebaService,isMoto,equipoFacade,revision);
		formulario.setPHEquRev(informacionEquipos);

		//cargar aplicativos
		cargarInformacionAplicativos(formulario);
		
		//cargar nombre director tecnico
		Usuario usuarioResponsable = cargarUsuarioResponsable(revision);
		if(usuarioResponsable != null){
			String nombreUsuario = usuarioResponsable.getNombres() + " " + usuarioResponsable.getApellidos();
			formulario.setPGNomFirDirTec(nombreUsuario);
		}	
		
		
		
		Optional<Prueba> optionalPruebaFoto = pruebas.stream().filter(pr -> pr.getTipoPrueba().getTipoPruebaId() == 3).findFirst();
		if(optionalPruebaFoto.isPresent()){
			Prueba prueba = optionalPruebaFoto.get();
			Foto foto = fotoDAO.consultarFotoPorPrueba(prueba);
			if(foto.getFoto1() == null){
				throw new RuntimeException("Foto Uno no ha sido tomada");
			}
			String base64PrimeraFoto = "";
			try {
				base64PrimeraFoto = new String(Base64.encodeBase64(foto.getFoto1()), "UTF-8");
			} catch (UnsupportedEncodingException e) {
				
				e.printStackTrace();
			}
			System.out.println("Tamanio foto 1 " + base64PrimeraFoto.length());
			
			if(foto.getFoto2() == null){
				throw new RuntimeException("Foto Dos no ha sido tomada");
			}
			String base64SegundaFoto = "";
			try {
				base64SegundaFoto = new String(Base64.encodeBase64(foto.getFoto2()), "UTF-8");
			} catch (UnsupportedEncodingException e) {
				
				e.printStackTrace();
			}
			System.out.println("Tamanio foto 1 " + base64SegundaFoto.length());
			StringBuilder sbFoto = new StringBuilder();
			sbFoto.append(base64PrimeraFoto).append(";").append(base64SegundaFoto);
			formulario.setPFoto(sbFoto.toString());
		}
		
		List<Defecto> defectos = new ArrayList<>();
		if(defectosInspeccionMecanizada!= null && !defectosInspeccionMecanizada.isEmpty()){
			defectos.addAll(defectosInspeccionMecanizada);
		}
		if(defectosInspeccionVisual != null && !defectosInspeccionVisual.isEmpty()){
			defectos.addAll(defectosInspeccionVisual);
		}
		if(!defectos.isEmpty()){
			String defectosConcatenados = defectos.stream().map(d -> String.valueOf(d.getCodigoMinisterio())).collect(Collectors.joining(";"));
			formulario.setPCausaRechazo(defectosConcatenados);
			logger.info(defectosConcatenados);
		}else{
			formulario.setPCausaRechazo("");
		}
		
		return formulario;
	}
	
	private void cargarInformacionAplicativos(FormularioV2 formulario) {
		
		InformacionCda infoCda = informacionCDADAO.find(1);
		String listadoAplicativos = infoCda.getAplicativosUtilizados() != null ? infoCda.getAplicativosUtilizados() : "" ;
		
		StringBuilder sb = new StringBuilder();
		sb.append(ConstantesNombreSoftware.NOMBRE_SOFTWARE).append(" ");
		sb.append(infoCda.getVersionSoftware() ).append(" ");
		sb.append( listadoAplicativos );
		formulario.setPISofRev(sb.toString());
		
		
	}
	
	
	private String codificarPresionLLantas(List<Prueba> pruebas) {
		Optional<Prueba> optPruebaIv = pruebas.stream().filter(pr -> pr.getTipoPrueba().getTipoPruebaId().equals( ConstantesTiposPrueba.TIPO_PRUEBA_INSPECCION_SENSORIAL)).findFirst();
		List<Medida> medidasIv = new ArrayList<>();
		if( optPruebaIv.isPresent() ) {
			Prueba pruebaIv = optPruebaIv.get();			
			medidasIv = pruebaService.obtenerMedidas(pruebaIv);
		}else {
			throw new RuntimeException("No se encuentra prueba de inspeccion visual");
		}
		
		Optional<Prueba> optFrenos = pruebas.stream().filter(pr -> pr.getTipoPrueba().getTipoPruebaId().equals( ConstantesTiposPrueba.TIPO_PRUEBA_FRENOS)).findFirst();
		if( optFrenos.isPresent() ) {
			Prueba pruebaFrenos = optFrenos.get();
			List<Medida> medidasFrenos = pruebaService.obtenerMedidas(pruebaFrenos);
			medidasIv.addAll(medidasFrenos);
		}
		GeneradorPresionLlantasDTO generadorPresion = new GeneradorPresionLlantasDTO();
		ResultadoPresionLlantasDTO resultado = generadorPresion.generarResultadoPresionLlantas(medidasIv);
		
		return resultado.toString();
		
		
	}
	
	private void llenarSeccionLlantas(List<Prueba> pruebas,FormularioV2 formulario) {
		
		List<Medida> medidas = medidaDAO.consultarMedidaProfundidadLabrado(pruebas);
		
		GeneradorDTOProfundidadLabrado generadorDTO = new GeneradorDTOProfundidadLabrado();
		generadorDTO.setPuntoComoSeparadorDecimales();
		ResultadoProfundidadLabradoDTO resultadoProfundidad = generadorDTO.generarResultadoProfundidadLabrado(medidas);
		
		
        formulario.setPD2Ej1Izq( resultadoProfundidad.getProfundidadLabradoEje1Izq1()     );
		formulario.setPD2Ej2IzqR1( resultadoProfundidad.getProfundidadLabradoEje2Izq1()   );
		formulario.setPD2Ej2IzqR2( resultadoProfundidad.getProfundidadLabradoEje2Izq2()   );
		
		formulario.setPD2Ej3IzqR1( resultadoProfundidad.getProfundidadLabradoEje3Izq1()   );
		formulario.setPD2Ej3IzqR2( resultadoProfundidad.getProfundidadLabradoEje3Izq2()   );
		
		formulario.setPD2Ej4IzqR1( resultadoProfundidad.getProfundidadLabradoEje4Izq1()   );
		formulario.setPD2Ej4IzqR2( resultadoProfundidad.getProfundidadLabradoEje4Izq2()   );
		
		formulario.setPD2Ej5IzqR1( resultadoProfundidad.getProfundidadLabradoEje5Izq1()   );
		formulario.setPD2Ej5IzqR2( resultadoProfundidad.getProfundidadLabradoEje5Izq2()   );
		
		formulario.setPD2Ej1Der(  resultadoProfundidad.getProfundidadLabradoEje1Der1() );
		
		formulario.setPD2Ej2DerR1( resultadoProfundidad.getProfundidadLabradoEje2Der1()   );
		formulario.setPD2Ej2DerR2( resultadoProfundidad.getProfundidadLabradoEje2Der2()   );
		
		formulario.setPD2Ej3DerR1( resultadoProfundidad.getProfundidadLabradoEje3Der1()   );
		formulario.setPD2Ej3DerR2( resultadoProfundidad.getProfundidadLabradoEje3Der2()   );
		
		formulario.setPD2Ej4DerR1( resultadoProfundidad.getProfundidadLabradoEje4Der1()   );
		formulario.setPD2Ej4DerR2( resultadoProfundidad.getProfundidadLabradoEje4Der2()   );
		
		formulario.setPD2Ej5DerR1( resultadoProfundidad.getProfundidadLabradoEje5Der1()   );
		formulario.setPD2Ej5DerR2( resultadoProfundidad.getProfundidadLabradoEje5Der2()   );
		
		formulario.setPD2RepR1(resultadoProfundidad.getProfundidadLabradoRepuestoDerecha());
		formulario.setPD2RepR2(resultadoProfundidad.getProfundidadLabradoRepuestoIzquierda());
		
	}
	
	

	public FormularioV2 generarFormularioSegundaEtapaInspeccion(Integer inspeccionId) {
		FormularioV2 formulario = new FormularioV2();
		try {
			setEmpty(formulario);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			logger.log(Level.SEVERE, "Error haciendo campos por defecto vacios", e);
		}
		
		generadorResultadosDTO.setPuntoComoSeparadorDecimales();

		int revisionId = inspeccionService.revisionIdDesdeInspeccionId(inspeccionId); 
		Revision revision = revisionService.cargarRevision(revisionId);
		String pin = revision.getPin();
		if(pin == null){
			throw new RuntimeException(" Pin no asignado para revision " + revisionId);
		}
		
		Inspeccion inspeccion = inspeccionService.cargarInspeccion(inspeccionId);

		InformacionCda informacionCda = informacionCDADAO.find(1);
		// Cargar datos de usuario, clave
		cargarAutenticacionCi2(formulario,informacionCda);	
		
		formulario.setPPin(pin);
		
		formulario.setP3Plac(revision.getVehiculo().getPlaca());
		String consecutivoRunt = revision.getConsecutivoRunt();
		if( consecutivoRunt == null || consecutivoRunt.isEmpty() ){
			throw new RuntimeException("No se ha asignado consecutivo runt para la revision " + revisionId);			
		}
		formulario.setPEConRun(consecutivoRunt);		
		//si la inspeccion es aprobada se genera info de certificado
		if(inspeccion.getAprobada()) {
			String ultimoConsecutivoCertificado = certificadoService.consultarUltimoConsecutivoCertificado(revisionId);
			formulario.setPTw01(ultimoConsecutivoCertificado);
			formulario.setPEApr("SI");
		}else {
			formulario.setPEApr("NO");
		}
		return formulario;
	}
	
	
	public FormularioV2 generarFormularioPrimeraEtapaInspeccion(Integer inspeccionId) {

		FormularioV2 formulario = new FormularioV2();
		try {
			setEmpty(formulario);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			logger.log(Level.SEVERE, "Error haciendo campos por defecto vacios", e);
		}
		generadorResultadosDTO.setPuntoComoSeparadorDecimales();

		int revisionId = inspeccionService.revisionIdDesdeInspeccionId(inspeccionId); 
		Revision revision = revisionService.cargarRevision(revisionId);
		String pin = revision.getPin();
		if(pin == null){
			throw new RuntimeException(" Pin no asignado para revision " + revisionId);
		}
		
		Inspeccion inspeccion = inspeccionService.cargarInspeccion(inspeccionId);

		InformacionCda informacionCda = informacionCDADAO.find(1);
		// Cargar datos de usuario, clave
		cargarAutenticacionCi2(formulario,informacionCda);	
		
		formulario.setPPin(pin);
		
		String fur_aso = revision.getRevisionId() + "-" + inspeccion.getNumeroInspeccion();
		formulario.setPFurAso(fur_aso);
		formulario.setPCda(informacionCda.getNombre());
		formulario.setPNit(informacionCda.getNit());
		formulario.setPDir(informacionCda.getDireccion());		
		formulario.setPDiv(informacionCda.getCodigoDivipo());// codigo divipo
		formulario.setPCiu(informacionCda.getCiudad());
		formulario.setPTel(informacionCda.getTelefono1());
		String correoElectronicoCda = informacionCda.getCorreoElectronico() == null ? "" : informacionCda.getCorreoElectronico();
		formulario.setPEma( correoElectronicoCda );
		
			// SECCION DE INFORMACION GENERAL		
		String fechaRevisionFormateada = sdfFechaRevision.format( inspeccion.getFechaInspeccionAnterior() );
		formulario.setP1FecPru(fechaRevisionFormateada);
	
		Propietario propietario = revision.getPropietario();
		if(propietario == null ){
			throw new RuntimeException("Propietario no configurado");
		}
		String nombreRazonSocial = propietario.getNombres() + " " + propietario.getApellidos();
		formulario.setP2NomRaz(nombreRazonSocial);
		
		// TODO validar el id del propietario sin puntos ni comas
		formulario.setP2Doc(String.valueOf(propietario.getPropietarioId()));
		formulario.setP2DocTip(String.valueOf(propietario.getTipoDocumentoIdentidad().getTipoDocumentoIdentidadId()));
		formulario.setP2Dir(propietario.getDireccion());
		formulario.setP2Tel(propietario.getTelefono1());
		formulario.setP2Ciu(propietario.getCiudad().getNombre());
		formulario.setP2Dep(propietario.getCiudad().getDepartamento().getNombre());

		// SECCION DE DATOS DEL VEHICULO
		Vehiculo vehiculo = llenarInformacionVehiculo(formulario, revision);

		//// SECCION DE MEDIDAS
		List<Prueba> pruebas = inspeccionService.pruebasInspeccion(inspeccionId);
		
		llenarSeccionRuido(revisionId,pruebas,formulario);
		
		llenarSeccionLuces(revisionId,pruebas,formulario,isVehiculoMoto(vehiculo.getTipoVehiculo()),revision);
		
		llenarSeccionSuspension(revisionId,pruebas,formulario,revision);
		
		llenarSeccionFrenos(pruebas,formulario,revision,isVehiculoMoto(vehiculo.getTipoVehiculo()));
		
		llenarSeccionDesviacion(pruebas,formulario,revision);
		
		llenarSeccionTaximetro(pruebas,formulario,revision);

		llenarSeccionGases(pruebas,formulario,revision);
		
		
		List<Defecto> defectosInspeccionMecanizada = defectoService.obtenerDefectosInspeccionMecanizadaReinspeccion(inspeccionId, EstadoDefecto.DETECTADO);
		// cargar defectos inspeccion mecanizada
		ponerDefectosInspeccionMecanizada(revisionId, formulario,defectosInspeccionMecanizada);
		// cargar defectos inspeccion visual		
		List<DefectoAsociado> defectosAsociados = defectoService.obtenerDefectosInspeccionSensorial(revisionId);
		List<Defecto> defectosInspeccionVisual = defectosAsociados.stream()
																  .filter(da->da.getEstadoDefecto().equals(EstadoDefecto.DETECTADO))
																	.map(da->da.getDefecto())
																	.collect(Collectors.toList());
		
		ponerDefectosInspeccionVisual(revisionId, formulario, defectosInspeccionVisual);
		
		// cargar defectos inspeccion visual ensenianza
		ponerDefectosEnsenianza(revisionId, formulario, defectosInspeccionVisual);
		
		// cargar nombres de usuario
		
		formulario.setPEConRun("");	// es el primer envio	

		if(inspeccion.getAprobada()){
			formulario.setPEApr("SI");
			if (vehiculo.getServicio().getServicioId() == 5){
				formulario.setPE1Apr("SI");
			}
		}else {
			formulario.setPEApr("NO");
			if(vehiculo.getServicio().getServicioId() == 5){
				formulario.setPE1Apr("NO");
			}
		}
		
		//cargar comentarios de prueba incluye dilucion, motivos de aborto y medidas de labrado		
		StringBuilder sb = new StringBuilder();
		String comentarioRevision = revision.getComentario();
		
		String resultadoPresionLlantas = codificarPresionLLantas(pruebas);		
		sb.append(resultadoPresionLlantas);
		
		if(comentarioRevision != null){
			sb.append(comentarioRevision);
		}
		colocarComentariosPruebas(pruebas, sb);	
		
		formulario.setPFComObs(sb.toString());
		
		llenarSeccionLlantas(pruebas,formulario);		
		
		// cargar nombres de usuario
		cargarUsuarios(revisionId, formulario, pruebas);
		
		Boolean isMoto = UtilTipoVehiculo.isVehiculoMoto(vehiculo.getTipoVehiculo());
		
		String informacionEquipos = UtilInformacionEquipos.ponerInformacionEquipos(pruebas,pruebaService,isMoto,equipoFacade,revision);
		formulario.setPHEquRev(informacionEquipos);

		//cargar aplicativos
		cargarInformacionAplicativos(formulario);
		

		//cargar nombre director tecnico
		Usuario usuarioResponsable = inspeccion.getUsuarioResponsable();
		if(usuarioResponsable != null){
			String nombreUsuario = usuarioResponsable.getNombres() + " " + usuarioResponsable.getApellidos();
			formulario.setPGNomFirDirTec(nombreUsuario);
		}	
		
		Optional<Prueba> optionalPruebaFoto = pruebas.stream().filter(pr -> pr.getTipoPrueba().getTipoPruebaId() == 3).findFirst();
		if(optionalPruebaFoto.isPresent()){
			Prueba prueba = optionalPruebaFoto.get();
			Foto foto = fotoDAO.consultarFotoPorPrueba(prueba);
			if(foto.getFoto1() == null){
				throw new RuntimeException("Foto Uno no ha sido tomada");
			}
			String base64PrimeraFoto = DatatypeConverter.printBase64Binary(foto.getFoto1());
			if(foto.getFoto2() == null){
				throw new RuntimeException("Foto Dos no ha sido tomada");
			}
			String base64SegundaFoto = DatatypeConverter.printBase64Binary(foto.getFoto2());
			StringBuilder sbFoto = new StringBuilder();
			sbFoto.append(base64PrimeraFoto).append(";").append(base64SegundaFoto);
			formulario.setPFoto(sbFoto.toString());
		}
		
		List<Defecto> defectos = new ArrayList<>();
		if(defectosInspeccionMecanizada!= null && !defectosInspeccionMecanizada.isEmpty()){
			defectos.addAll(defectosInspeccionMecanizada);
		}
		if(defectosInspeccionVisual != null && !defectosInspeccionVisual.isEmpty()){
			defectos.addAll(defectosInspeccionVisual);
		}
		if(!defectos.isEmpty()){
			String defectosConcatenados = defectos.stream().map(d -> String.valueOf(d.getCodigoMinisterio())).collect(Collectors.joining(";"));
			formulario.setPCausaRechazo(defectosConcatenados);
			logger.info(defectosConcatenados);
		}else{
			formulario.setPCausaRechazo("");
		}
		
		return formulario;
	}
	

	public void llenarSeccionGases(List<Prueba> pruebas, FormularioV2 formulario, Revision revision) {
		generadorResultadosDTO.setPuntoComoSeparadorDecimales();
		//parametro por defecto
		//formulario.setPG17(CADENA_VACIA);
		//parametros que no se usan
		
		preRellenarFormularioGases(formulario);
		
		//combustible distinto a hidrogeno y electrico
		formulario.setPV03(CADENA_VACIA);
		Vehiculo vehiculo = revision.getVehiculo();
		Integer tipoCombustibleId = vehiculo.getTipoCombustible().getCombustibleId();
		Optional<Prueba> optPruebaGases = encontrarPruebaPorTipo(pruebas, ConstantesTiposPrueba.TIPO_PRUEBA_GASES);
		
		if (optPruebaGases.isPresent()) {
			
			
			if(tipoCombustibleId.equals(ConstantesTiposCombustible.ELECTRICO)){
				formulario.setPV01(EQUIS);
				return;
			}else {
				formulario.setPV01(CADENA_VACIA);
			}
			if(tipoCombustibleId.equals(ConstantesTiposCombustible.HIDROGENO)){
				formulario.setPV02(EQUIS);
				return;
			}else {
				formulario.setPV02(CADENA_VACIA);
			}
			
			
			Prueba pruebaGases = optPruebaGases.get();
			if(!pruebaGases.isFinalizada()) {
				throw new RuntimeException("Prueba de gases no finalizada");
			}
			
			List<Medida> medidas = pruebaService.obtenerMedidas(pruebaGases);
			utilInformesMedidas.ordenarListaMedidas(medidas);
			List<Defecto> defectos = new ArrayList<>();
			Usuario usuarioGases = pruebaGases.getUsuario();
			checkNotNull("No existe usuario registrado para prueba de gases");

			boolean tipoCombustibleEsDiesel = tipoCombustibleId.equals(ConstantesTiposCombustible.DIESEL)
					|| tipoCombustibleId.equals(ConstantesTiposCombustible.BIODIESEL)
					|| tipoCombustibleId.equals(ConstantesTiposCombustible.DIESEL_ELECTRICO);
			
			boolean tipoCombustibleVehiculoEsOtto = tipoCombustibleId.equals(ConstantesTiposCombustible.GASOLINA )
					|| tipoCombustibleId.equals(ConstantesTiposCombustible.GAS_NATURAL)
					|| tipoCombustibleId.equals(ConstantesTiposCombustible.GAS_GASOLINA)
					|| tipoCombustibleId.equals(ConstantesTiposCombustible.GASOLINA_ELECTRICO);
			
			if (tipoCombustibleEsDiesel) {
				generadorResultadosDTO.setPuntoComoSeparadorDecimales();
				ResultadoDieselDTO resultado = generadorResultadosDTO.generarResultadoDieselDTO(medidas, defectos);
				ResultadoDieselDTOInformes resultadoComplementario = generadorResultadosDTO
						.generarResultadoDieselDTOInformes(medidas, defectos);
				
				//formulario.setPO01(CADENA_VACIA);
				formulario.setP11BCi1( retornarStringCadenaVacia( resultado.getAceleracionCero() ) );
				formulario.setP11BC1Gob( retornarStringCadenaVacia (resultadoComplementario.getRpmGobernadaCiclo0() ));
				formulario.setP11BCi2( retornarStringCadenaVacia( resultado.getAceleracionUno() ) );
				formulario.setP11BC2Gob( resultadoComplementario.getRpmGobernadaCiclo1() );
				formulario.setP11BCi3( retornarStringCadenaVacia( resultado.getAceleracionDos() ) );
				formulario.setP11BC3Gob ( resultadoComplementario.getRpmGobernadaCiclo2());
				formulario.setP11BCi4( retornarStringCadenaVacia( resultado.getAceleracionTres() ) );
				formulario.setP11BC4Gob( resultadoComplementario.getRpmGobernadaCiclo3() );
				formulario.setP11BResVal( retornarStringCadenaVacia( resultado.getValorFinal() ) );
				
				formulario.setP11BTemIni(resultado.getTemperaturaInicial() );
				formulario.setP11BTemFin( resultadoComplementario.getTemperaturaFinal() );
				formulario.setP11BTemAmb( resultadoComplementario.getTemperaturaAmbiental( ));
				formulario.setP11BHum( resultadoComplementario.getHumedadRelativa() );
				String diametro = vehiculo.getDiametroExosto() != null ? String.valueOf(vehiculo.getDiametroExosto()) : "";
				formulario.setP11BLot( diametro );
				
				//formulario.setP11BTem( retornarStringCadenaVacia( resultado.getTemperaturaInicial() ));
				formulario.setP11BRpm( retornarStringCadenaVacia( resultado.getRpmGobernada() ) );
				//formulario.setPO10(CADENA_VACIA);
				PermisibleDieselDTO permisible = generadorDTOPermisibles.generarPermisibleDieselDTO(medidas, revision);
				formulario.setP11BResNor( retornarStringCadenaVacia( permisible.getPermisibleDiesel() ) );
				
			} else {					
				if (tipoCombustibleVehiculoEsOtto){
					
					ResultadoOttoInformeDTO resultadoComplementario = generadorResultadosDTO.generarResultadoOttoInforme(medidas,
							defectos);
					Integer tipoVehiculo = vehiculo.getTipoVehiculo().getTipoVehiculoId();
					Boolean vehiculoEsMoto = tipoVehiculo.equals(ConstantesTiposVehiculo.MOTO) || tipoVehiculo.equals(ConstantesTiposVehiculo.MOTO_CARRO);
					Boolean vehiculoEsRemolque = tipoVehiculo.equals(ConstantesTiposVehiculo.REMOLQUE); 
					if(vehiculoEsMoto){
						Boolean isDosTiempos = vehiculo.getTiemposMotor().equals(2);
						ResultadoOttoMotocicletasDTO resultado = generadorResultadosDTO.generarResultadoOttoMotos(medidas, defectos,
								isDosTiempos);
						
						formulario.setP11HcRalVal( retornarStringCadenaVacia( resultado.getHcRalenti() ) );
						formulario.setP11CoRalVal( retornarStringCadenaVacia( resultado.getCoRalenti() ) );
						formulario.setP11Co2RalVal( retornarStringCadenaVacia( resultado.getCo2Ralenti() ) );
						formulario.setP11O2RalVal( retornarStringCadenaVacia( resultado.getO2Ralenti() ) );
						formulario.setP11RpmRal(retornarStringCadenaVacia( resultado.getRpmRalenti() ) );
						formulario.setP11TemRal( retornarStringCadenaVacia( resultado.getTempRalenti() ) );
						
						formulario.setP11Cat( vehiculo.getCatalizador().equalsIgnoreCase("SI") ? "SI" : "NO" );
						formulario.setP11HumAmb( resultadoComplementario.getHumedadRelativa() );
						formulario.setP11HumRel( resultadoComplementario.getTemperaturaAmbiente() );
						
						PermisiblesMotoDTO permisibleMoto = generadorDTOPermisibles.generarPermsibleMoto(medidas, revision, isDosTiempos);
						formulario.setP11HcRalNor( retornarStringCadenaVacia( permisibleMoto.getPermisibleHC() ) );
						formulario.setP11CoRalNor( retornarStringCadenaVacia( permisibleMoto.getPermisibleCO() ) );
						formulario.setP11O2RalNor( retornarStringCadenaVacia( permisibleMoto.getPermisibleO2() ) );
						
						
					}else if(!vehiculoEsMoto && !vehiculoEsRemolque){							
						ResultadoOttoVehiculosDTO resultado = generadorResultadosDTO.generarResultadoOtto(medidas, defectos);
						
						
						formulario.setP11HcRalVal( retornarStringCadenaVacia( resultado.getHcRalenti() ) );
						formulario.setP11CoRalVal( retornarStringCadenaVacia( resultado.getCoRalenti() ) );
						formulario.setP11Co2RalVal( retornarStringCadenaVacia( resultado.getCo2Ralenti() ) );
						formulario.setP11O2RalVal( retornarStringCadenaVacia( resultado.getO2Ralenti() ) );
						formulario.setP11RpmRal( retornarStringCadenaVacia( resultado.getRpmRalenti() ) );
						formulario.setP11TemRal( retornarStringCadenaVacia( resultado.getTempRalenti() ) );
						
						formulario.setP11HcCruVal( retornarStringCadenaVacia( resultado.getHcCrucero() ) );
						formulario.setP11CoCruVal( retornarStringCadenaVacia( resultado.getCoCrucero()  ) );
						formulario.setP11Co2CruVal( retornarStringCadenaVacia( resultado.getCo2Crucero() ) );
						formulario.setP11O2CruVal( retornarStringCadenaVacia( resultado.getO2Ralenti() ) );
						formulario.setP11RpmCru( retornarStringCadenaVacia( resultado.getRpmCrucero() ) );
						formulario.setP11TemCru( retornarStringCadenaVacia( resultado.getTempCrucero() ) );
						
						formulario.setP11RpmRal( resultado.getRpmRalenti() );
						formulario.setP11RpmCru( resultado.getRpmCrucero() );
						formulario.setP11TemCru( resultado.getTempCrucero() );
						
						formulario.setP11Cat( vehiculo.getCatalizador().equalsIgnoreCase("SI") ? "SI" : "NO" );
						formulario.setP11HumAmb( resultadoComplementario.getHumedadRelativa() );
						formulario.setP11HumRel( resultadoComplementario.getTemperaturaAmbiente() );
						
						PermisibleOttoVehiculoDTO permisibleOtto = generadorDTOPermisibles.generarPermisibleOttoVehiculo(medidas, revision);
						formulario.setP11HcRalNor( retornarStringCadenaVacia( permisibleOtto.getPermisibleHcRalenti() ) );
						formulario.setP11HcCruNor( retornarStringCadenaVacia( permisibleOtto.getPermisibleHcCrucero() ) );
						
						formulario.setP11CoRalNor( retornarStringCadenaVacia( permisibleOtto.getPermisibleCoRalenti() ) );
						formulario.setP11CoCruNor( retornarStringCadenaVacia( permisibleOtto.getPermisibleCoCrucero() ) );
						
						formulario.setP11Co2RalNor( retornarStringCadenaVacia( permisibleOtto.getPermisibleCo2Ralenti() ) );
						formulario.setP11Co2CruNor( retornarStringCadenaVacia( permisibleOtto.getPermisibleCo2Crucero() ) );
						
						formulario.setP11O2RalNor( retornarStringCadenaVacia( permisibleOtto.getPermisibleO2Ralenti() ) );
						formulario.setP11O2CruNor( retornarStringCadenaVacia( permisibleOtto.getPermisibleO2Crucero()  ) );
						
					}else{							
						logger.log(Level.INFO, "El vehiculo es remolque no se genera info");
					}
				}
			}

		} else {
				throw new RuntimeException("No hay prueba de gases para vehiculo " + vehiculo.getPlaca());
		}		
	}

	private void preRellenarFormularioGases(FormularioV2 formulario) {
			formulario.setP11CoRalVal(CADENA_VACIA);
			formulario.setP11CoRalNor(CADENA_VACIA);
			
			formulario.setP11Co2RalVal(CADENA_VACIA);
			formulario.setP11Co2RalNor(CADENA_VACIA);
			
			formulario.setP11O2RalVal(CADENA_VACIA);
			formulario.setP11O2RalNor(CADENA_VACIA);
			
			formulario.setP11HcRalVal(CADENA_VACIA);
			formulario.setP11HcRalNor(CADENA_VACIA);
			
			//CRUCERO
			formulario.setP11CoCruVal(CADENA_VACIA);
			formulario.setP11CoCruNor(CADENA_VACIA);
			
			formulario.setP11Co2CruVal(CADENA_VACIA);
			formulario.setP11Co2CruNor(CADENA_VACIA);
			
			formulario.setP11O2CruVal(CADENA_VACIA);
			formulario.setP11O2CruNor(CADENA_VACIA);
			
			formulario.setP11HcCruVal(CADENA_VACIA);
			formulario.setP11HcCruNor(CADENA_VACIA);
			
			//formulario.setPG17(CADENA_VACIA);
			
			formulario.setP11TemRal(CADENA_VACIA);
			formulario.setP11RpmRal(CADENA_VACIA);
			
			formulario.setP11TemCru(CADENA_VACIA);
			formulario.setP11RpmCru(CADENA_VACIA);
			
			formulario.setP11NoRalVal(CADENA_VACIA);
			formulario.setP11NoRalNor(CADENA_VACIA);
			formulario.setP11NoCruNor(CADENA_VACIA);
			formulario.setP11NoCruVal(CADENA_VACIA);
			
			//DIESEL
			//formulario.setPO01(CADENA_VACIA);
			formulario.setP11BCi1(CADENA_VACIA);
			formulario.setP11BCi2(CADENA_VACIA);
			formulario.setP11BCi3(CADENA_VACIA);
			formulario.setP11BCi4(CADENA_VACIA);
			formulario.setP11BResVal(CADENA_VACIA);
			formulario.setP11BResNor(CADENA_VACIA);
			//formulario.setP11BTem(CADENA_VACIA);
			formulario.setP11BRpm(CADENA_VACIA);
			//formulario.setPO10(CADENA_VACIA);
			
	}

	public void llenarSeccionDesviacion(List<Prueba> pruebas, FormularioV2 formulario, Revision revision) {
		generadorResultadosDTO.setPuntoComoSeparadorDecimales();
		Optional<Prueba> pruebaDesviacion = encontrarPruebaPorTipo(pruebas,ConstantesTiposPrueba.TIPO_PRUEBA_DESVIACION);
		if(pruebaDesviacion.isPresent()){
			
			List<Medida> medidasDesviacion = pruebaService.obtenerMedidas( pruebaDesviacion.get());
			utilInformesMedidas.ordenarListaMedidas(medidasDesviacion);
			ResultadoFasDTO resultadoFAS = generadorResultadosDTO.generarResultadoFAS(medidasDesviacion);
			formulario.setP9Ej1( retornarStringCadenaVacia( resultadoFAS.getDesviacionLateralEje1() ) );
			formulario.setP9Ej2( retornarStringCadenaVacia( resultadoFAS.getDesviacionLateralEje2() ) );
			formulario.setP9Ej3( retornarStringCadenaVacia( resultadoFAS.getDesviacionLateralEje3() ) );
			formulario.setP9Ej4( retornarStringCadenaVacia( resultadoFAS.getDesviacionLateralEje4() ) );
			formulario.setP9Ej5( retornarStringCadenaVacia( resultadoFAS.getDesviacionLateralEje5() ) );
			
			PermisiblesDesviacionDTO permisiblesDesviacion = generadorDTOPermisibles.generarPermisibleDesviacion(medidasDesviacion, revision);
			formulario.setP9Max(permisiblesDesviacion.getPermisibleDesviacionEje1());
			
		}else {
			formulario.setP9Ej1(CADENA_VACIA);
			formulario.setP9Ej2(CADENA_VACIA);
			formulario.setP9Ej3(CADENA_VACIA);
			formulario.setP9Ej4(CADENA_VACIA);
			formulario.setP9Ej5(CADENA_VACIA);
			formulario.setP9Max(CADENA_VACIA);
		}
	}
	
	public void llenarSeccionTaximetro(List<Prueba> pruebas,FormularioV2 formulario,Revision revision){
		generadorResultadosDTO.setPuntoComoSeparadorDecimales();
		Optional<Prueba> pruebaTaximetroOpt = encontrarPruebaPorTipo(pruebas,ConstantesTiposPrueba.TIPO_PRUEBA_TAXIMETRO);
		if(pruebaTaximetroOpt.isPresent()){
			Prueba pruebaTaximetro = pruebaTaximetroOpt.get();
			formulario.setP10RefComLla(CADENA_VACIA);
		
			String motivoAborto = pruebaTaximetro.getMotivoAborto();
			if(motivoAborto != null && !motivoAborto.isEmpty()) {
				String[] cadenaSeparada = motivoAborto.split(":");
				if(cadenaSeparada != null ) {
					if(cadenaSeparada.length == 2) {
						String nombreLlanta = cadenaSeparada[1];
						formulario.setP10RefComLla(nombreLlanta);
					}
				}
			}
			
			List<Medida> medidasTaximetro = pruebaService.obtenerMedidas(pruebaTaximetroOpt.get());
			utilInformesMedidas.ordenarListaMedidas(medidasTaximetro);
			List<Defecto> defectos = new ArrayList<>();
			ResultadoTaximetroDTO resultado = generadorResultadosDTO.generarResultadoTaximetro(medidasTaximetro, defectos,Boolean.TRUE , "");
			formulario.setP10ErrDis( retornarStringCadenaVacia( resultado.getErrorDistancia() ) );
			formulario.setP10ErrTie( retornarStringCadenaVacia( resultado.getErrorTiempo()) );

			PermisibleTaximetroDTO permisibleTaximetro = generadorDTOPermisibles.generarPermisiblesTaximetros(medidasTaximetro, revision);
			formulario.setP10Max( retornarStringCadenaVacia( permisibleTaximetro.getPermisibleErrorDistancia() ));
			
		}else {
			
			formulario.setP10ErrDis(CADENA_VACIA);
			formulario.setP10ErrTie(CADENA_VACIA);
			formulario.setP10Max(CADENA_VACIA);
			formulario.setP10RefComLla(CADENA_VACIA);
			
		}
	}

	public void llenarSeccionSuspension(Integer revisionId, List<Prueba> pruebas, FormularioV2 formulario,Revision revision) {
		generadorResultadosDTO.setPuntoComoSeparadorDecimales();
		Optional<Prueba> pruebaSuspension = encontrarPruebaPorTipo(pruebas,ConstantesTiposPrueba.TIPO_PRUEBA_SUSPENSION);
		if(pruebaSuspension.isPresent()){
			List<Medida> medidasSuspension = pruebaService.obtenerMedidas(pruebaSuspension.get());
			utilInformesMedidas.ordenarListaMedidas(medidasSuspension);
			ResultadoFasDTO resultado = generadorResultadosDTO.generarResultadoFAS(medidasSuspension);
			formulario.setP7DelDerVal(resultado.getSuspDerEje1());
			formulario.setP7DelIzqVal(resultado.getSuspIzqEje1());
			formulario.setP7TraDerVal(resultado.getSuspDerEje2());
			formulario.setP7TraIzqVal(resultado.getSuspIzqEje2());
			
			PermisibleSuspensionDTO permisibleSuspension = generadorDTOPermisibles.generarPermisibleSuspension(medidasSuspension, revision);
			formulario.setP7Min(permisibleSuspension.getValorMinimoSuspension());
			
		}else {
			
			formulario.setP7DelDerVal("");
			formulario.setP7DelIzqVal("");
			formulario.setP7TraDerVal("");
			formulario.setP7TraIzqVal("");
			formulario.setP7Min("");
		}
	}
	
	public void llenarSeccionFrenos(List<Prueba> pruebas,FormularioV2 formulario,Revision revision,Boolean isMoto){
		
		Optional<Prueba> pruebaFrenos = encontrarPruebaPorTipo( pruebas , ConstantesTiposPrueba.TIPO_PRUEBA_FRENOS );
		if(pruebaFrenos.isPresent()){
			List<Medida> medidasFrenos = pruebaService.obtenerMedidas(pruebaFrenos.get());
			utilInformesMedidas.ordenarListaMedidas(medidasFrenos);
			ResultadoFasDTO resultadoFAS = generadorResultadosDTO.generarResultadoFAS(medidasFrenos);
			formulario.setP8EfiAux(resultadoFAS.getEficaciaAuxiliar());
			formulario.setP8EfiTot(resultadoFAS.getEficaciaTotal());
			
			formulario.setP8Ej1DerFue( retornarStringCadenaVacia( resultadoFAS.getFuerzaFrenadoDerEje1() )  );
			formulario.setP8Ej1DerPes( retornarStringCadenaVacia ( resultadoFAS.getPesoDerEje1() ) );
			
			
			formulario.setP8Ej2DerFue( retornarStringCadenaVacia( resultadoFAS.getFuerzaFrenadoDerEje2() )  );
			formulario.setP8Ej2DerPes( retornarStringCadenaVacia ( resultadoFAS.getPesoDerEje2() ) );
			
			
			formulario.setP8Ej3DerFue( retornarStringCadenaVacia( resultadoFAS.getFuerzaFrenadoDerEje3() )  );
			formulario.setP8Ej3DerPes( retornarStringCadenaVacia ( resultadoFAS.getPesoDerEje3() ) );
			
			
			formulario.setP8Ej4DerFue( retornarStringCadenaVacia( resultadoFAS.getFuerzaFrenadoDerEje4() )  );
			formulario.setP8Ej4DerPes( retornarStringCadenaVacia ( resultadoFAS.getPesoDerEje4() ) );
			
			formulario.setP8Ej5DerFue( retornarStringCadenaVacia( resultadoFAS.getFuerzaFrenadoDerEje5() )  );
			formulario.setP8Ej5DerPes( retornarStringCadenaVacia ( resultadoFAS.getPesoDerEje5() ) );
			
			
			formulario.setP8Ej1IzqFue( retornarStringCadenaVacia( resultadoFAS.getFuerzaFrenadoIzqEje1() )  );
			formulario.setP8Ej1IzqPes( retornarStringCadenaVacia ( resultadoFAS.getPesoIzqEje1() ) );
			
			formulario.setP8Ej2IzqFue( retornarStringCadenaVacia( resultadoFAS.getFuerzaFrenadoIzqEje2() )  );
			formulario.setP8Ej2IzqPes( retornarStringCadenaVacia ( resultadoFAS.getPesoIzqEje2() ) );
			
			formulario.setP8Ej3IzqFue( retornarStringCadenaVacia( resultadoFAS.getFuerzaFrenadoIzqEje3() )  );
			formulario.setP8Ej3IzqPes( retornarStringCadenaVacia ( resultadoFAS.getPesoIzqEje3() ) );
			
			formulario.setP8Ej4IzqFue( retornarStringCadenaVacia( resultadoFAS.getFuerzaFrenadoIzqEje4() )  );
			formulario.setP8Ej4IzqPes( retornarStringCadenaVacia ( resultadoFAS.getPesoIzqEje4() ) );
			
			formulario.setP8Ej5IzqFue( retornarStringCadenaVacia( resultadoFAS.getFuerzaFrenadoIzqEje5() )  );
			formulario.setP8Ej5IzqPes( retornarStringCadenaVacia ( resultadoFAS.getPesoIzqEje5() ) );
			
			//DESEQUILIBRIO
			
			formulario.setP8Ej1Des( retornarStringCadenaVacia( resultadoFAS.getDesequilibrioEje1() ) );
			formulario.setP8Ej2Des( retornarStringCadenaVacia( resultadoFAS.getDesequilibrioEje2() ) );
			formulario.setP8Ej3Des( retornarStringCadenaVacia( resultadoFAS.getDesequilibrioEje3() ) );
			formulario.setP8Ej4Des( retornarStringCadenaVacia( resultadoFAS.getDesequilibrioEje4() ) );
			formulario.setP8Ej5Des( retornarStringCadenaVacia( resultadoFAS.getDesequilibrioEje5() ));
			
			PermisiblesFrenosDTO permisiblesFrenos = generadorDTOPermisibles.generarPermisibleFrenos(medidasFrenos, revision, isVehiculoMoto(revision.getVehiculo().getTipoVehiculo()));
			generadorDTOPermisibles.setPuntoComoSeparadorDecimales();
			formulario.setP8EfiAuxMin( retornarStringCadenaVacia( permisiblesFrenos.getPermisibleEficaciaAuxiliar() ) );
			formulario.setP8EfiTotMin( retornarStringCadenaVacia( permisiblesFrenos.getPermisibleEficaciaTotal() ) );
			formulario.setP8Ej1Max( retornarStringCadenaVacia( permisiblesFrenos.getPermisibleDesequilibrioEje1() ));
			formulario.setP8Ej2Max( retornarStringCadenaVacia( permisiblesFrenos.getPermisibleDesequilibrioEje1() ));
			formulario.setP8Ej3Max( retornarStringCadenaVacia( permisiblesFrenos.getPermisibleDesequilibrioEje1() ));
			formulario.setP8Ej4Max( retornarStringCadenaVacia( permisiblesFrenos.getPermisibleDesequilibrioEje1() ));
			formulario.setP8Ej5Max( retornarStringCadenaVacia( permisiblesFrenos.getPermisibleDesequilibrioEje1() ));
			if(!isMoto) {
				formulario.setP8Ej1Ran("[20-30]");
				formulario.setP8Ej2Ran("[20-30]");
				formulario.setP8Ej3Ran("[20-30]");
				formulario.setP8Ej4Ran("[20-30]");
				formulario.setP8Ej5Ran("[20-30]");
			}
			
			formulario.setP8SumIzqAuxFue(resultadoFAS.getFuerzaIzquierdaAuxiliar());
			formulario.setP8SumDerAuxFue(resultadoFAS.getFuerzaDerechaAuxiliar());
			formulario.setP8SumDerAuxPes(resultadoFAS.getPesoDerechoTotal());
			formulario.setP8SumIzqAuxPes(resultadoFAS.getPesoIzquierdoTotal());
			
			
		}else {
			throw new RuntimeException("No se enuentra prueba de frenos para la revision");
		}
	}
	
	public String retornarStringCadenaVacia(String cadena){
		return cadena != null ? cadena : CADENA_VACIA;
	}

	private Vehiculo llenarInformacionVehiculo(FormularioV2 formulario, Revision revision) {
		Vehiculo vehiculo = revision.getVehiculo();
		formulario.setP3Plac(vehiculo.getPlaca());
		formulario.setP3Mar(vehiculo.getMarca().getNombre());
		formulario.setP3Lin(vehiculo.getLineaVehiculo().getNombre());
		formulario.setP3Cla(vehiculo.getClaseVehiculo().getNombre());
		String modeloStr = String.valueOf(vehiculo.getModelo());
		formulario.setP3Mod(modeloStr);
		String cilindrajeStr = String.valueOf(vehiculo.getCilindraje());
		formulario.setP3Cil(cilindrajeStr);
		formulario.setP3Ser(vehiculo.getServicio().getNombre().toUpperCase());
		formulario.setP3Vin(vehiculo.getVin());
		formulario.setP3Mot(vehiculo.getNumeroMotor());
		formulario.setP3Lic(vehiculo.getNumeroLicencia());
		formulario.setP3Com(vehiculo.getTipoCombustible().getNombre());
		formulario.setP3Col(vehiculo.getColor().getNombre());
		formulario.setP3Nac(vehiculo.getPais().getNombre());
		String fechaMatricula = sdfFechaMatricula.format(vehiculo.getFechaMatricula());
		formulario.setP3FecLic(fechaMatricula);
		formulario.setP3TipMot(vehiculo.getTiemposMotor().toString());
		formulario.setP3Kil(String.valueOf(vehiculo.getKilometraje()));
		formulario.setP3Sil(String.valueOf(vehiculo.getNumeroSillas()));
		if(vehiculo.getVidriosPolarizados() != null) {
			formulario.setP3VidPol(vehiculo.getVidriosPolarizados() ? "SI" : " NO");
		}else {
			formulario.setP3VidPol("NO");
		}
		
		formulario.setP3Bli(vehiculo.getBlindaje() ? "SI" : "NO");		
		String potencia = vehiculo.getPotencia() == null ? "" :  String.valueOf(vehiculo.getPotencia());
		formulario.setP3Pot(potencia);
		String tipoCarroceria = vehiculo.getTipoCarroceria() == null ? "" : vehiculo.getTipoCarroceria().getNombre();
		formulario.setP3TipCar(tipoCarroceria);
		String fechaVencimientoSoat = vehiculo.getFechaSoat() == null ? "": sdfFechaMatricula.format(vehiculo.getFechaSoat());
		formulario.setP3FecVenSoa(fechaVencimientoSoat);
		String conversionGnv = vehiculo.getConversionGnv() == null ? "":vehiculo.getConversionGnv();
		if(conversionGnv.equalsIgnoreCase("N/A")) {
			conversionGnv = "NA";
		}
		formulario.setP3ConGnv(conversionGnv);
		String fechaConversionGnv = vehiculo.getFechaVenicimientoGNV() == null ? "" : sdfFechaMatricula.format(vehiculo.getFechaVenicimientoGNV());
		formulario.setP3FecVenGnv(fechaConversionGnv);	
		return vehiculo;
	}
	
	public void llenarSeccionLuces(Integer revisionId, List<Prueba> pruebas, FormularioV2 formulario,Boolean isMoto,Revision revision) {
		
		Optional<Prueba> optPruebaLuces = encontrarPruebaPorTipo(pruebas,ConstantesTiposPrueba.TIPO_PRUEBA_LUCES);
		if(optPruebaLuces.isPresent()){
			Prueba pruebaLuces = optPruebaLuces.get();
			List<Medida> medidas = pruebaService.obtenerMedidas(pruebaLuces);
			checkArgument(medidas != null && !medidas.isEmpty(),"No hay medidas para prueba de luces revision %d",revisionId);
			utilInformesMedidas.ordenarListaMedidas(medidas);
			generadorResultadoLuces.setPuntoComoSeparadorDecimales();
			ResultadoLucesDtoV2 resultado = isMoto ? generadorResultadoLuces.generarResultadoLucesMotocicleta(medidas): generadorResultadoLuces.generarResultadoLucesVehiculo(medidas);
			formulario.setP5DerIntB1(resultado.getIntensidadBajaDerecha1());
			formulario.setP5DerIntB2(resultado.getIntensidadBajaDerecha2());
			formulario.setP5DerIntB3(resultado.getIntensidadBajaDerecha3());
			
			formulario.setP5DerIncB1(resultado.getInclinacionBajaDerecha1());
			formulario.setP5DerIncB2(resultado.getInclinacionBajaDerecha2());
			formulario.setP5DerIncB3(resultado.getInclinacionBajaDerecha3());
			
			formulario.setP5SimDerB( resultado.getDerBajaSimultaneas().equalsIgnoreCase("S") ? "SI" : "NO");
			
			formulario.setP5IzqIntB1(resultado.getIntensidadBajaIzquierda1());
			formulario.setP5IzqIntB2(resultado.getIntensidadBajaIzquierda2());
			formulario.setP5IzqIntB3(resultado.getIntensidadBajaIzquierda3());
			
			formulario.setP5IzqIncB1(resultado.getInclinacionBajaIzquierda1());
			formulario.setP5IzqIncB2(resultado.getInclinacionBajaIzquierda2());
			formulario.setP5IzqIncB3(resultado.getInclinacionBajaIzquierda3());
			
			formulario.setP5SimIzqB( resultado.getIzqBajaSimultaneas().equalsIgnoreCase("S") ? "SI" : "NO");
			if(!isMoto) {			
				formulario.setP5DerIntA1( resultado.getIntensidadAltaDerecha1());
				formulario.setP5DerIntA2( resultado.getIntensidadAltaDerecha2());
				formulario.setP5DerIntA3( resultado.getIntensidadAltaDerecha3());
				
				formulario.setP5IzqIntA1( resultado.getIntensidadAltaIzquierda1());
				formulario.setP5IzqIntA2( resultado.getIntensidadAltaIzquierda2());
				formulario.setP5IzqIntA3( resultado.getIntensidadAltaIzquierda3());
			}
			
			formulario.setP5DerMinA("");
			formulario.setP5SimDerA( resultado.getDerAltaSimultaneas().equalsIgnoreCase("S") ? "SI" : "NO");
			formulario.setP5SimIzqA( resultado.getIzqAltaSimultaneas().equalsIgnoreCase("S") ? "SI" : "NO");			
			formulario.setP5IzqMinA("");
			
			formulario.setP5DerIntE1( resultado.getIntensidadExploradoraDerecha1() );
			formulario.setP5DerIntE2( resultado.getIntensidadExploradoraDerecha2() );
			formulario.setP5DerIntE3( resultado.getIntensidadExploradoraDerecha3() );
			
			formulario.setP5IzqIntE1( resultado.getIntensidadExploradoraIzquierda1() );
			formulario.setP5IzqIntE2( resultado.getIntensidadExploradoraIzquierda2() );
			formulario.setP5IzqIntE3( resultado.getIntensidadExploradoraIzquierda3() );
			
			formulario.setP5DerMinE("");
			formulario.setP5SimDerE( resultado.getDerExploradoraSimultaneas().equalsIgnoreCase("S") ? "SI" : "NO");
			formulario.setP5SimIzqE( resultado.getIzqExploradoraSimultaneas().equalsIgnoreCase("S") ? "SI" : "NO");			
			formulario.setP5IzqMinE("");
			
			generadorDTOPermisibles.setPuntoComoSeparadorDecimales();
			PermisiblesLucesDTO permLuces = generadorDTOPermisibles.generarPermisibleLuces(isMoto, revision, medidas);
			
			formulario.setP5DerMin(permLuces.getPermisibleIntensidadBajaDerecha() != null ? permLuces.getPermisibleIntensidadBajaDerecha() : "");
			formulario.setP5IzqMin(permLuces.getPermisibleIntensidadBajaIzquierda() != null ? permLuces.getPermisibleIntensidadBajaIzquierda() : CADENA_VACIA);
			formulario.setP5DerRan(permLuces.getPermisibleRangoInclinacionBajaDerecha() != null ? permLuces.getPermisibleRangoInclinacionBajaDerecha() : "");
			formulario.setP5IzqRan(permLuces.getPermisibleRangoInclinacionBajaIzquierda() != null ? permLuces.getPermisibleRangoInclinacionBajaIzquierda() : CADENA_VACIA);
			formulario.setP6Int(resultado.getSumatoriaIntensidad());
			formulario.setP6Max(permLuces.getPermisibleLuces() != null ? permLuces.getPermisibleLuces() : "");
			
		}else {
			throw new RuntimeException("Revision no tiene prueba de luces");
		}
		
		
		
	}

	public void llenarSeccionRuido(Integer revisionId, List<Prueba> pruebas, FormularioV2 formulario) {
		generadorResultadosDTO.setPuntoComoSeparadorDecimales();
		Optional<Prueba> optPruebaRuidos = encontrarPruebaPorTipo(pruebas,ConstantesTiposPrueba.TIPO_PRUEBA_RUIDO);
		if(optPruebaRuidos.isPresent()){
			Prueba pruebaRuido = optPruebaRuidos.get();
			List<Medida> medidas = pruebaService.obtenerMedidas(pruebaRuido);
			utilInformesMedidas.ordenarListaMedidas(medidas);
			ResultadoSonometriaDTO resultado = generadorResultadosDTO.generarResultadoSonometria(medidas);			
			formulario.setP4RuiMax(CADENA_VACIA);
			formulario.setP4RuiVal(resultado.getValor());			
		}else {
			logger.log(Level.SEVERE, "No hay prueba de ruido revision " + revisionId);
			throw new RuntimeException("No hay prueba de ruido para revision " + revisionId);
		}		
	}

	private Usuario cargarUsuarioResponsable(Revision r){
		Integer usuarioResponsableId = r.getUsuarioResponsableId();
		Usuario usuarioResponsable = usuarioDAO.find(usuarioResponsableId);
		return usuarioResponsable;		
	}
	
		private void cargarAutenticacionCi2(FormularioV2 formulario, InformacionCda informacionCda) {
		
		String usuarioCi2Encriptado = informacionCda.getUsuarioCi2();
		String passwordCi2Encriptado = informacionCda.getPasswordCi2();
		
		StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
		
		encryptor.setPassword("cMB8apv23");
		
		String usuarioCi2 = encryptor.decrypt(usuarioCi2Encriptado);
		String passwordCi2 = encryptor.decrypt(passwordCi2Encriptado);
		
		formulario.setUsuario(usuarioCi2);
		formulario.setClave(passwordCi2);
		
	}

	private void cargarUsuarios(Integer revisionId, FormularioV2 formulario, List<Prueba> pruebas) {
		Stream<Prueba> pruebasStream = pruebas.stream();
		StringBuilder concat = pruebasStream.collect(() -> new StringBuilder(),
				(sb, prueba) -> sb.append(prueba.getTipoPrueba().getNombre()).append(":")
						.append(prueba.getUsuario() != null ? prueba.getUsuario().getNombres() : CADENA_VACIA)
						.append(" ")
						.append(prueba.getUsuario() != null ? prueba.getUsuario().getApellidos() : CADENA_VACIA)
						.append(";"),
				(sb, sb2) -> sb.append(sb2));
		formulario.setPHNomOpeReaRevTec(concat.toString());
	}

	private void ponerDefectosEnsenianza(Integer revisionId, FormularioV2 formulario,
			List<Defecto> defectosInspeccionVisual) {
		if (defectosInspeccionVisual != null && !defectosInspeccionVisual.isEmpty()) {
			StringBuilder sbCodigo = new StringBuilder();
			StringBuilder sbDescripcion = new StringBuilder();
			StringBuilder sbCategoria = new StringBuilder();
			StringBuilder sbDefectoA = new StringBuilder();
			StringBuilder sbDefectoB = new StringBuilder();

			Stream<Defecto> stream = defectosInspeccionVisual.stream();

			// saca la categoria de
			stream.filter(d -> d.getCategoriaDefecto().getCategoriaDefectoId() == 2).forEach(d -> {
				sbCodigo.append(d.getCodigoDefecto()).append(";");
				sbDescripcion.append(d.getNombre()).append(";");
				sbCategoria.append(d.getCategoriaDefecto().getNombre()).append(";");
				if (d.getTipoDefecto().equalsIgnoreCase("A")) {
					sbDefectoA.append("A").append(";");
					sbDefectoB.append("").append(";");
				} else {
					sbDefectoB.append("B").append(";");
					sbDefectoA.append("").append(";");
				}
			});
			stream = defectosInspeccionVisual.stream();
			Map<String, List<Defecto>> mapaDefectos = stream
					.filter(d -> d.getCategoriaDefecto().getCategoriaDefectoId() == 2)
					.collect(Collectors.groupingBy(Defecto::getTipoDefecto));
			List<Defecto> defectosTipoA = mapaDefectos.get("A");
			List<Defecto> defectosTipoB = mapaDefectos.get("B");

			formulario.setPD1Cod(sbCodigo.toString());
			formulario.setPD1Des(sbDescripcion.toString());
			formulario.setPD1Gru(sbCategoria.toString());
			formulario.setPD1TipDefA(sbDefectoA.toString());
			formulario.setPD1TipDefB(sbDefectoB.toString());
			if (defectosTipoA != null) {
				formulario.setPD1TipDefATot(String.valueOf(defectosTipoA.size()));
			} else {
				formulario.setPD1TipDefATot("0");
			}
			if (defectosTipoB != null) {
				formulario.setPD1TipDefBTot(String.valueOf(defectosTipoB.size()));				
			} else {
				formulario.setPD1TipDefBTot( "0" );
			}

		} else {
			formulario.setPD1TipDefATot("0");
			formulario.setPD1TipDefBTot("0");
		}

	}

	private void ponerDefectosInspeccionVisual(Integer revisionId, FormularioV2 formulario,
			List<Defecto> defectosInspeccionVisual) {

		if (defectosInspeccionVisual != null && !defectosInspeccionVisual.isEmpty()) {
			StringBuilder sbCodigo = new StringBuilder();
			StringBuilder sbDescripcion = new StringBuilder();
			StringBuilder sbCategoria = new StringBuilder();
			StringBuilder sbDefectoA = new StringBuilder();
			StringBuilder sbDefectoB = new StringBuilder();

			Stream<Defecto> stream = defectosInspeccionVisual.stream();

			// saca la categoria de
			stream.filter(d -> d.getCategoriaDefecto().getCategoriaDefectoId() != 2).forEach(d -> {
				sbCodigo.append(d.getCodigoMinisterio()).append(";");
				sbDescripcion.append(d.getNombre()).append(";");
				sbCategoria.append(d.getCategoriaDefecto().getNombre()).append(";");
				if (d.getTipoDefecto().equalsIgnoreCase("A")) {
					sbDefectoA.append("A").append(";");
					sbDefectoB.append("").append(";");
				} else {
					sbDefectoB.append("B").append(";");
					sbDefectoA.append("").append(";");
				}
			});
			stream = defectosInspeccionVisual.stream();
			Map<String, List<Defecto>> mapaDefectos = stream
					.filter(d -> d.getCategoriaDefecto().getCategoriaDefectoId() != 2)
					.collect(Collectors.groupingBy(Defecto::getTipoDefecto));
			List<Defecto> defectosTipoA = mapaDefectos.get("A");
			List<Defecto> defectosTipoB = mapaDefectos.get("B");

			formulario.setPDCod(sbCodigo.toString());
			formulario.setPDDes(sbDescripcion.toString());
			formulario.setPDGru(sbCategoria.toString());
			formulario.setPDTipDefA(sbDefectoA.toString());
			formulario.setPDTipDefB(sbDefectoB.toString());
			if (defectosTipoA != null) {
				formulario.setPDTipDefATot(String.valueOf(defectosTipoA.size()));
			} else {
				formulario.setPDTipDefATot("0");
			}
			if (defectosTipoB != null) {
				formulario.setPDTipDefBTot(String.valueOf(defectosTipoB.size()));
			}else {
				formulario.setPDTipDefBTot("0");
			}

		} else {
			formulario.setPDTipDefATot("0");
			formulario.setPD1TipDefBTot("0");
		}
	}

	public void ponerDefectosInspeccionMecanizada(Integer revisionId, FormularioV2 formulario,List<Defecto> defectosInspeccionMecanizada) {

		

		if (defectosInspeccionMecanizada != null && !defectosInspeccionMecanizada.isEmpty()) {
			StringBuilder sbCodigo = new StringBuilder();
			StringBuilder sbDescripcion = new StringBuilder();
			StringBuilder sbCategoria = new StringBuilder();
			StringBuilder sbDefectoA = new StringBuilder();
			StringBuilder sbDefectoB = new StringBuilder();

			Stream<Defecto> stream = defectosInspeccionMecanizada.stream();			
			stream = defectosInspeccionMecanizada.stream();

			stream.forEach(d -> {
				sbCodigo.append(d.getCodigoMinisterio()).append(";");
				sbDescripcion.append(d.getNombre()).append(";");
				sbCategoria.append(d.getCategoriaDefecto().getNombre()).append(";");
				if (d.getTipoDefecto().equalsIgnoreCase("A")) {
					sbDefectoA.append("A").append(";");
					sbDefectoB.append("").append(";");
				} else {
					sbDefectoB.append("B").append(";");
					sbDefectoA.append("").append(";");
				}
			});
			stream = defectosInspeccionMecanizada.stream();
			Map<String, List<Defecto>> mapaDefectos = stream.collect(Collectors.groupingBy(Defecto::getTipoDefecto));
			List<Defecto> defectosTipoA = mapaDefectos.get("A");
			List<Defecto> defectosTipoB = mapaDefectos.get("B");

			formulario.setPCCod(sbCodigo.toString());
			formulario.setPCDes(sbDescripcion.toString());
			formulario.setPCGru(sbCategoria.toString());
			formulario.setPCTipDefA(sbDefectoA.toString());
			formulario.setPCTipDefB(sbDefectoB.toString());
			if (defectosTipoA != null) {
				formulario.setPCTipDefATot(String.valueOf(defectosTipoA.size()));
			}else {
				formulario.setPCTipDefATot("0");
			}
			
			if (defectosTipoB != null) {
				formulario.setPCTipDefBTot(String.valueOf(defectosTipoB.size()));
			}else {
				formulario.setPCTipDefBTot("0");
			}

		} else { //no hay defectos en la inspeccion mecanizada
			formulario.setPCCod( CADENA_VACIA );
			formulario.setPCDes( CADENA_VACIA );
			formulario.setPCGru( CADENA_VACIA );
			formulario.setPCTipDefA( CADENA_VACIA );
			formulario.setPCTipDefB( CADENA_VACIA );
			formulario.setPCTipDefATot( "0" );
			formulario.setPCTipDefBTot( "0" );
		}

	}	
	
	private void colocarComentariosPruebas(List<Prueba> pruebas, StringBuilder comentariosSb) {
		
		for (Prueba p : pruebas) {
			if (p.getMotivoAborto() != null && !p.getMotivoAborto().isEmpty() )
				comentariosSb.append(p.getMotivoAborto()).append("\t");
		}
	}
	
	
		

	 public static void setEmpty(Object object) throws IllegalArgumentException, IllegalAccessException {
	     Class<?> clazz = object.getClass();
	     Field[] fields = clazz.getDeclaredFields();
	     for (Field field : fields) {
	         if (String.class.equals(field.getType())) {
	             field.setAccessible(true);
	             if (field.get(object) == null) {
	                 field.set(object, "");
	             }
	         }
	     }
	 }



}
