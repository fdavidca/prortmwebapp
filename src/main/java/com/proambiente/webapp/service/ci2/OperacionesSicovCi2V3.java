package com.proambiente.webapp.service.ci2;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.tempuri.FormularioV3;

import com.proambiente.modelo.NivelAlerta;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Revision_;
import com.proambiente.modelo.dto.PruebaDTO;
import com.proambiente.webapp.service.CertificadoService;
import com.proambiente.webapp.service.InspeccionService;
import com.proambiente.webapp.service.ListaEnviosService;
import com.proambiente.webapp.service.PinNoEncontradoException;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.service.guardarpdf.GuardarPdfCallback;
import com.proambiente.webapp.service.guardarpdf.GuardarPdfDummyCallback;
import com.proambiente.webapp.service.notification.NotificationPrimariaService;
import com.proambiente.webapp.service.sicov.EtapaRevision;
import com.proambiente.webapp.service.sicov.OperacionesSicov;

public class OperacionesSicovCi2V3 implements OperacionesSicov {

	public static final String SEGUNDA_INSPECCION = "2";
	public static final String PRIMERA_INSPECCION = "1";
	public static final Integer PRIMER_ENVIO = 1;
	public static final Integer SEGUNDO_ENVIO = 2;

	private static final Logger logger = Logger.getLogger(OperacionesSicovCi2V3.class.getName());

	@Inject
	ManejadorCi2SicovServiceV3 manejadorCi2Service;

	@Inject
	RevisionService revisionService;

	@Inject
	FurCi2CodificadorV3 codificadorFur;

	@Inject
	NotificationPrimariaService notificacionService;

	@Inject
	CertificadoService certificadoService;

	@Inject
	InspeccionService inspeccionService;

	@Inject
	ListaEnviosService listaEnviosService;

	@Override
	public void registrarFURInspeccionPrimerEnvio(Integer inspeccionId) {
		try {
			FormularioV3 formulario = codificadorFur.generarFormularioPrimeraEtapaInspeccion(inspeccionId);
			Integer revisionId = inspeccionService.revisionIdDesdeInspeccionId(inspeccionId);
			manejadorCi2Service.reportarFur(formulario, revisionId, PRIMER_ENVIO, new GuardarPdfDummyCallback());
		} catch (Exception exc) {
			logger.log(Level.SEVERE, " Excepcion registrarFUR ci2", exc);
			notificacionService.sendNotification(" Excepcion registrarFUR ci2" + inspeccionId + exc.getMessage(),
					NivelAlerta.ERROR_GRAVE, true);
		}

	}

	public void registrarFUR(Integer revisionId, Integer numeroInspeccion, Envio envio,GuardarPdfCallback callback) {
		try {

			FormularioV3 formulario = codificadorFur.generarFormularioPrimeraEtapa(revisionId);
			manejadorCi2Service.reportarFur(formulario, revisionId, PRIMER_ENVIO,callback);
		} catch (Exception exc) {
			logger.log(Level.SEVERE, " Excepcion registrarFUR ci2", exc);
			notificacionService.sendNotification(" Excepcion registrarFUR ci2" + revisionId + exc.getMessage(),
					NivelAlerta.ERROR_GRAVE, true);
		}
	}

	public void registrarInformacionCertificado(Integer revisionId, String numeroCertificado, String consecutivoRunt,GuardarPdfCallback guardarPdfCallback) {
		try {
			FormularioV3 formulario = codificadorFur.generarFormularioSegundaEtapa(revisionId);
			formulario.setPEConRun(consecutivoRunt);
			formulario.setPTw01(numeroCertificado);
			manejadorCi2Service.reportarFur(formulario, revisionId, SEGUNDO_ENVIO,guardarPdfCallback);
		} catch (Exception exc) {
			logger.log(Level.SEVERE, " Excepcion registrarFUR ci2", exc);
			notificacionService.sendNotification(" Excepcion registrarFUR ci2" + revisionId + exc.getMessage(),
					NivelAlerta.ERROR_GRAVE, true);
		}
	}

	public void registrarInformacionCertificadoYaImpreso(Integer revisionId) {
		try {
			FormularioV3 formulario = codificadorFur.generarFormularioSegundaEtapa(revisionId);
			manejadorCi2Service.reportarFur(formulario, revisionId, SEGUNDO_ENVIO,new GuardarPdfDummyCallback());
		} catch (Exception exc) {
			logger.log(Level.SEVERE, " Excepcion registrarFUR ci2", exc);
			notificacionService.sendNotification(" Excepcion registrarFUR ci2" + revisionId + exc.getMessage(),
					NivelAlerta.ERROR_GRAVE, true);
		}
	}

	public void enviarSegundoEnvioRevisionReprobada(Integer revisionId, String consecutivoRunt,GuardarPdfCallback guardarPdfCallback) {
		try {
			FormularioV3 formulario = codificadorFur.generarFormularioSegundaEtapa(revisionId);
			formulario.setPEConRun(consecutivoRunt);
			formulario.setPTw01("");
			manejadorCi2Service.reportarFur(formulario, revisionId, SEGUNDO_ENVIO,guardarPdfCallback);
		} catch (Exception exc) {
			logger.log(Level.SEVERE, " Excepcion registrarFUR ci2", exc);
			notificacionService.sendNotification(" Excepcion registrarFUR ci2" + revisionId + exc.getMessage(),
					NivelAlerta.ERROR_GRAVE, true);
		}
	}

	@Override
	public void registrarEventoRTMEC(PruebaDTO prueba, String placa, Date fechaRevision, String serialesEquipos,
			String causaAborto, Integer tipoEventoPrueba, EtapaRevision etapaRevision) {
		if (etapaRevision.equals(EtapaRevision.INICIO_REVISION)) {
			logger.log(Level.INFO, "Se inicia el proceso de revision para revisionId " + placa);
			// utilizar PIN de ci2
			Integer revisionId = prueba.getRevisionId();

			Revision revision = revisionService.cargarRevisionDTO(revisionId, Revision_.pin);
			String pin = revision.getPin();

			if (pin != null) {
				String tipoRevision = "";
				if (revision.getNumeroInspecciones().equals(1)) {
					tipoRevision = ManejadorCi2SicovService.PRIMERA_INSPECCION;
				} else {
					tipoRevision = ManejadorCi2SicovService.SEGUNDA_INSPECCION;
				}
				manejadorCi2Service.utilizarPin(pin, tipoRevision, placa);
			} else {
				notificacionService.sendNotification("Error, no hay pin asociado con revision placa " + placa,
						NivelAlerta.ERROR_GRAVE, Boolean.TRUE);
			}
		}

	}

	@Override
	public void enviarEco(String mensaje) {
		try {
			manejadorCi2Service.enviarEco(mensaje);
		} catch (Exception exc) {
			logger.log(Level.SEVERE, " Error enviando eco ci2 ", exc);
		}
	}

	@Override
	public String consultarPinPlaca(String placa) throws PinNoEncontradoException {
		return manejadorCi2Service.consultarPinPlacaSincrono(placa);
	}

	@Override
	public void utilizarPin(String pin, String tipoRtm, String placa) {

		manejadorCi2Service.utilizarPin(pin, tipoRtm, placa);

	}

	@Override
	public void registrarFURInspeccionSegundoEnvio(Integer inspeccionId) {
		try {
			FormularioV3 formulario = codificadorFur.generarFormularioSegundaEtapaInspeccion(inspeccionId);
			Integer revisionId = inspeccionService.revisionIdDesdeInspeccionId(inspeccionId);
			manejadorCi2Service.reportarFur(formulario, revisionId, SEGUNDO_ENVIO,new GuardarPdfDummyCallback());
		} catch (Exception exc) {
			logger.log(Level.SEVERE, " Excepcion registrarFUR ci2", exc);
			notificacionService.sendNotification(" Excepcion registrarFUR ci2" + inspeccionId + exc.getMessage(),
					NivelAlerta.ERROR_GRAVE, true);
		}
	}

	@Override
	public void registrarInformacionRunt(Integer revisionId,GuardarPdfCallback guardarPdfCallback) {
		try {
			FormularioV3 formulario = codificadorFur.generarFormularioSegundaEtapa(revisionId);		
			manejadorCi2Service.reportarFur(formulario, revisionId, SEGUNDO_ENVIO,guardarPdfCallback);
		} catch (Exception exc) {
			logger.log(Level.SEVERE, " Excepcion registrarFUR ci2", exc);
			notificacionService.sendNotification(" Excepcion registrarFUR ci2" + revisionId + exc.getMessage(),
					NivelAlerta.ERROR_GRAVE, true);
		}
	}

}
