package com.proambiente.webapp.service.ci2;

public class InformacionPin {
	
	private String pin;
	
	private String tipoDocumento;
	
	private String documento;
	
	
	
	public InformacionPin() {
		super();
	}

	public InformacionPin(String pin, String tipoDocumento, String documento) {
		super();
		this.pin = pin;
		this.tipoDocumento = tipoDocumento;
		this.documento = documento;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}
	
	

}
