package com.proambiente.webapp.service.ci2;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.xml.ws.BindingProvider;

import org.tempuri.Datos;
import org.tempuri.DatosSoap;

import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.NivelAlerta;
import com.proambiente.modelo.OperadorSicov;
import com.proambiente.webapp.dao.InformacionCdaFacade;
import com.proambiente.webapp.service.notification.NotificationPrimariaService;

@SessionScoped
public class WebServiceClienteCi2Producer implements Serializable {

	private static final Logger logger = Logger.getLogger(WebServiceClienteCi2Producer.class.getName());
	/**
	 * 
	 */
	private static final long serialVersionUID = 2210L;
	@Inject
	InformacionCdaFacade informacionCdaFacade;
	
	@Inject
	NotificationPrimariaService notificacionService;
	

	@Produces
	@DatosSoapConfigurado
	DatosSoap crearDatosSoap() {
		InformacionCda infoCda = informacionCdaFacade.find(1);
		String urlWebService = infoCda.getUrlWebService();
		DatosSoap datosSoap = null;
		try {
			if(infoCda.getOperadorSicov().equals(OperadorSicov.CI2)){
				URL url = new URL(urlWebService);
				//URL baseUrl = WebServiceClienteCi2Producer.class.getClassLoader().getResource(".");
				//URL url = new URL("http://localhost/wsdl/SOAService.wsdl");//new URL(baseUrl, "META-INF/wsdl/sincrofur.asmx.wsdl");
				Datos datos = new Datos(url);
				datosSoap = datos.getDatosSoap();
				BindingProvider bp = ((BindingProvider) datosSoap);
				bp.getRequestContext().put("javax.xml.ws.client.connectionTimeout", "600000");
				bp.getRequestContext().put("javax.xml.ws.client.receiveTimeout", "600000");
			}
		} catch (MalformedURLException e) {			
			notificacionService.sendNotification("Url del web service mal configurada" + urlWebService + e.getMessage(), NivelAlerta.ERROR_GRAVE, Boolean.TRUE);
		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Error creando cliente web service ci2", exc);
			notificacionService.sendNotification("Error creando cliente web service ci2 " + exc.getMessage(), NivelAlerta.ERROR_GRAVE, Boolean.TRUE);
			throw new RuntimeException("Error creando cliente web service ci2 " + exc.getMessage());
		}
		return datosSoap;
	}

}
