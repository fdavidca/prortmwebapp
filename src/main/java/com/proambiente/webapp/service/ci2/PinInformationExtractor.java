package com.proambiente.webapp.service.ci2;

public class PinInformationExtractor {
	
	
	
	public InformacionPin extraerInformacionPin(String cadena){
		String[] cadenaDividida = cadena.split("\\|");
		String subcadenaPin = cadenaDividida[0];
		String subcadenaTipoDocumento = cadenaDividida[1];
		String subcadenaDocumento = cadenaDividida[2];
		
		String pin = subcadenaPin.split("@")[1];
		String tipoDocumento = subcadenaTipoDocumento.split("@")[1];
		String documento = subcadenaDocumento.split("@")[1];
		
		return new InformacionPin(pin,tipoDocumento,documento);
	}

}
