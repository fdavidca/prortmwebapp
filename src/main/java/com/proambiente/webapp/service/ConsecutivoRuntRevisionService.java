package com.proambiente.webapp.service;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Revision_;

@Stateless
public class ConsecutivoRuntRevisionService {
	
	@PersistenceContext
	EntityManager em;
	
	
	public Long contarConsecutivoRuntRevision(String consecutivoRunt) {		
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<Revision> root = cq.from(Revision.class);		
		cq.select( cb.count( root.get(Revision_.revisionId) ) ).where( cb.equal( root.get(Revision_.consecutivoRunt),consecutivoRunt) );		
		TypedQuery<Long> query = em.createQuery(cq);
		return query.getSingleResult();
	}
	

}
