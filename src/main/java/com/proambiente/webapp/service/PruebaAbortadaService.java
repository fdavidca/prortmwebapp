package com.proambiente.webapp.service;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.proambiente.modelo.Prueba;

public class PruebaAbortadaService {
	
	
	
    @PersistenceContext 
    private EntityManager em;
	
	public List<Prueba> consultarPruebaAbortadaPorFecha(Date fechaInicial,Date fechaFinal){
		String consulta = "Select p From Prueba p where fechaInicio between :fechaInicial and :fechaFinal and p.abortada = true";
		TypedQuery<Prueba> query = em.createQuery(consulta,Prueba.class);
		query.setParameter("fechaInicial",fechaInicial);
		query.setParameter("fechaFinal", fechaFinal);
		return query.getResultList();		
	}	
	

}
