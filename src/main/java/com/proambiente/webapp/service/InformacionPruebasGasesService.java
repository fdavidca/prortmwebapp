package com.proambiente.webapp.service;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

public class InformacionPruebasGasesService {
	
	@Inject
	EntityManager em;
	
	public Long numeroPruebasDiesel(){
		String consulta = "SELECT COUNT(p) FROM Prueba p "
						  +"WHERE p.tipoPrueba.tipoPruebaId = 8 "
						  +"AND p.finalizada = true "
						  +"AND p.revision.vehiculo.tipoCombustible.combustibleId = 3";
		Query query = em.createQuery(consulta);
		Long numeroPruebas = (Long)query.getSingleResult();
		return numeroPruebas;
	}
	
	
	public Long numeroPruebasGasolina(){
		String consulta = "SELECT COUNT(p) FROM Prueba p "
				  +"WHERE p.tipoPrueba.tipoPruebaId = 8 "
				  +"AND p.finalizada = true "
				  +"AND p.revision.vehiculo.tipoCombustible.combustibleId = 1 ";
		Query query = em.createQuery(consulta);
		Long numeroPruebas = (Long)query.getSingleResult();
		return numeroPruebas;
	}
	
	
	
	public Long numeroPruebasDieselAprobadas(){
		String consulta = "SELECT COUNT(p) FROM Prueba p "
						  +"WHERE p.tipoPrueba.tipoPruebaId = 8 "
						  +"AND p.aprobada = true "
						  +"AND p.finalizada = true "
						  +"AND p.revision.vehiculo.tipoCombustible.combustibleId = 3";
		Query query = em.createQuery(consulta);		
		Long numeroPruebas = (Long)query.getSingleResult();
		return numeroPruebas;
	}
	
	public Long numeroPruebasGasolinaAprobadas(){
		String consulta = "SELECT COUNT(p) FROM Prueba p "
						  +"WHERE p.tipoPrueba.tipoPruebaId = 8 "
						  +"AND p.aprobada = true "
						  +"AND p.finalizada = true "
						  +"AND p.revision.vehiculo.tipoCombustible.combustibleId = 1";
		Query query = em.createQuery(consulta);		
		Long numeroPruebas = (Long)query.getSingleResult();
		return numeroPruebas;
	}
	
	
	public Long numeroPruebasDieselReprobadas(){
		String consulta = "SELECT COUNT(p) FROM Prueba p "
						  +"WHERE p.tipoPrueba.tipoPruebaId = 8 "
						  +"AND p.aprobada = false "
						  +"AND p.finalizada = true "
						  +"AND p.abortada = false "
						  +"AND p.revision.vehiculo.tipoCombustible.combustibleId = 3";
		Query query = em.createQuery(consulta);		
		Long numeroPruebas = (Long)query.getSingleResult();
		return numeroPruebas;
	}
	
	public Long numeroPruebasGasolinaReprobadas(){
		String consulta = "SELECT COUNT(p) FROM Prueba p "
						  +"WHERE p.tipoPrueba.tipoPruebaId = 8 "
						  +"AND p.aprobada = false "
						  +"AND p.finalizada = true "
						  +"AND p.abortada = false "
						  +"AND p.revision.vehiculo.tipoCombustible.combustibleId = 1";
		Query query = em.createQuery(consulta);		
		Long numeroPruebas = (Long)query.getSingleResult();
		return numeroPruebas;
	}
	
	
	public Long numeroPruebasDieselAbortadas(){
		String consulta = "SELECT COUNT(p) FROM Prueba p "
						  +"WHERE p.tipoPrueba.tipoPruebaId = 8 "
						  +"AND p.aprobada = false "
						  +"AND p.finalizada = true "
						  +"AND p.abortada = true "
						  +"AND p.revision.vehiculo.tipoCombustible.combustibleId = 3";
		Query query = em.createQuery(consulta);		
		Long numeroPruebas = (Long)query.getSingleResult();
		return numeroPruebas;
	}
	
	public Long numeroPruebasGasolinaAbortadas(){
		String consulta = "SELECT COUNT(p) FROM Prueba p "
						  +"WHERE p.tipoPrueba.tipoPruebaId = 8 "
						  +"AND p.aprobada = false "
						  +"AND p.finalizada = true "
						  +"AND p.abortada = true "
						  +"AND p.revision.vehiculo.tipoCombustible.combustibleId = 1";
		Query query = em.createQuery(consulta);		
		Long numeroPruebas = (Long)query.getSingleResult();
		return numeroPruebas;
	}

}
