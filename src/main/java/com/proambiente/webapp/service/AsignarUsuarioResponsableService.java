
package com.proambiente.webapp.service;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


import com.proambiente.modelo.Rol;
import com.proambiente.modelo.Usuario;
import com.proambiente.webapp.dao.UsuarioFacade;

@Stateless
public class AsignarUsuarioResponsableService {

	@PersistenceContext
	EntityManager em;
	
	@Inject
	UsuarioFacade usuarioFacade;
	
	public void asignarUsuarioResponsable(Integer revisionId,Integer usuarioId) {
			if(!usuarioAdministrador(usuarioId)) {
				throw new RuntimeException("No se puede asignar un usuario no administrador como director tecnico");
			}
			if(revisionCerrada(revisionId)) {
				throw new RuntimeException("La revision ya ha finalizado");
			}
			actualizarUsuarioResponsable(revisionId, usuarioId);
	}
	
	//ojo con un update sin where
	private void actualizarUsuarioResponsable(Integer revisionId,Integer usuarioId) {
			String queryStr = "UPDATE Revision r SET r.usuarioResponsableId=:usuarioId WHERE r.revisionId =:revisionId";
			Query query = em.createQuery(queryStr);
			query.setParameter("usuarioId",usuarioId);
			query.setParameter("revisionId",revisionId);
			int afectados = query.executeUpdate();
	}
	
	private Boolean usuarioAdministrador(Integer usuarioId) {
		Usuario usuario = em.getReference(Usuario.class,usuarioId);
		Usuario usuarioAtached = usuarioFacade.atachUsuario(usuario);
		for(Rol r : usuarioAtached.getRols()) {
			if(r.getNombreRol().equals("ADMINISTRADOR")) {
				return true;
			}
		}
		return false;
	}
	
	private Boolean revisionCerrada(Integer revisionId) {
		String queryStr = "SELECT r.cerrada FROM Revision r WHERE r.revisionId=:revisionId";
		Query query = em.createQuery(queryStr);
		query.setParameter("revisionId",revisionId);
		return (Boolean) query.getSingleResult();
	}
	
}
