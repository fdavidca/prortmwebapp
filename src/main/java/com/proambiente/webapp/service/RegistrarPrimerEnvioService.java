package com.proambiente.webapp.service;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
public class RegistrarPrimerEnvioService {
	
	@PersistenceContext
	EntityManager em;
	
	@Inject
	RevisionService revisionService;
	
	private static final Logger logger = Logger.getLogger(RegistrarPrimerEnvioService.class.getName());
	

	public void registrarPrimerEnvio(Integer revisionId,Boolean estadoPrimerEnvio) {
		
		String estado = revisionService.evaluarRevision(revisionId);
		if(estado.equalsIgnoreCase("no_finalizada")) {
			throw new RuntimeException("Revision no finalizada");
		}
		String string = "UPDATE Revision r SET r.primerEnvioRealizado =:estadoprimerenvio WHERE r.revisionId =:revisionId";
		Query query = em.createQuery(string);
		query.setParameter("revisionId", revisionId);
		query.setParameter("estadoprimerenvio",estadoPrimerEnvio);
		int numExec = query.executeUpdate();
		logger.log(Level.INFO, "Num registro actu " + numExec);
	}

}
