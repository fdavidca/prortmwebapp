package com.proambiente.webapp.service;

import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.proambiente.modelo.Color;
import com.proambiente.webapp.dao.ColorFacade;
import com.proambiente.webapp.service.exception.CeldaNullException;

public class ImportarColoresDesdeExcelService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3388214616555259255L;

	@Inject
	ColorFacade colorDAO;	

	private static final Logger logger = Logger.getLogger(ImportarColoresDesdeExcelService.class.getName());

	public List<Color> obtenerColoresDesdeArchivo(InputStream inputStream) throws Exception {

		List<Color> coloresCargados = new ArrayList<>();
		try {
			XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
			XSSFSheet sheet = workbook.getSheetAt(0);
			Iterator<Row> rowIterator = sheet.iterator();
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				if(row.getRowNum() < 1) continue;
				
				Optional<Color> optColor = construirColor(row);
				if (optColor.isPresent()) {
					coloresCargados.add(optColor.get());
				}
			}
		} catch (CeldaNullException cexc) {
			logger.info("Se ha llegado a celda nula " + cexc.getMessage());
		} finally {
			inputStream.close();
		}

		return coloresCargados;
	}

	public void guardarColores(List<Color> colores) {
		for (Color c : colores) {
			try {
				logger.info("Creando o modificando " + c.getNombre());
				colorDAO.edit(c);				
			} catch (Exception exc) {
				logger.log(Level.SEVERE, "Error creando color : " + c.getNombre(), exc);
			}
		}
	}

	public Optional<Color> construirColor(Row rowParam) throws CeldaNullException {
		
		Row row = rowParam;
		StringBuilder sb = new StringBuilder();
		if (row == null) {
			return Optional.empty();
		}
		Cell cell = null;
		DataFormatter formatter = new DataFormatter(); // creating formatter
														// using the default
														// locale
		

		
		String nombreColor = "";
		nombreColor = leerColorDesdeCelda(row, formatter, sb);

		
		double codigoColor = 0;
		cell = row.getCell(0);
		if (celdaEsTipoNumerico(cell).isPresent()) {
			if (celdaEsTipoNumerico(cell).get().equals(Boolean.TRUE)) {
				codigoColor = row.getCell(0).getNumericCellValue();
			} else {
				sb.append("Celda que contiene modelo no es numerica \n");
				try {
					String modeloStr = formatter.formatCellValue(row.getCell(1));
					codigoColor = Double.valueOf(modeloStr);
				} catch (NumberFormatException nexc) {
					sb.append("El modelo no corresponde con un número");
					return Optional.empty();
				}
			}
		} else if (cell == null) {
			return Optional.empty();
		}

		Color color = new Color();
		color.setColorId((int)codigoColor);
		color.setNombre(nombreColor);
		
		return Optional.of(color);

	}

	
	
	private String leerColorDesdeCelda(Row row, DataFormatter formatter, StringBuilder sb) throws CeldaNullException {
		String color = "";
		Cell cell;
		cell = row.getCell(1);
		if (celdaEsTipoString(cell).isPresent()) {
			if (celdaEsTipoString(cell).get().equals(Boolean.TRUE)) {
				color = row.getCell(1).getStringCellValue();
			} else {
				sb.append("Color no es tipo string \n");
				color = formatter.formatCellValue(row.getCell(1));
			}
		} else if (cell == null) {
			throw new CeldaNullException(row.getRowNum(), 1);
		}
		return color;
	}

	private Optional<Boolean> celdaEsTipoString(Cell cell) {
		if (cell == null) {
			return Optional.empty();
		} else {
			return Optional.of(cell.getCellType() == CellType.STRING);
		}
	}

	private Optional<Boolean> celdaEsTipoNumerico(Cell cell) {
		if (cell == null) {
			return Optional.empty();
		} else {
			return Optional.of(cell.getCellType() == CellType.NUMERIC);
		}
	}

}
