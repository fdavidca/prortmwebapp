package com.proambiente.webapp.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.TemporalType;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;
import javax.persistence.metamodel.SingularAttribute;

import org.jfree.util.Log;

import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.Inspeccion;
import com.proambiente.modelo.Propietario;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Revision_;
import com.proambiente.modelo.Usuario;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.modelo.dto.PruebaDTO;
import com.proambiente.modelo.dto.RevisionDTO;
import com.proambiente.modelo.dto.builder.RevisionDTOBuilder;
import com.proambiente.webapp.dao.InformacionCdaFacade;
import com.proambiente.webapp.dao.RevisionFacade;
import com.proambiente.webapp.service.util.UtilVencimientoRevision;

@Stateless
public class RevisionService {

	private static final Logger logger = Logger.getLogger(RevisionService.class.getName());

	@PersistenceContext
	EntityManager em;

	@Inject
	RevisionFacade revisionDAO;

	@Inject
	InspeccionService inspeccionService;

	@Inject
	InformacionCdaFacade informacionCDADAO;

	@Inject
	FechaService fechaService;

	public boolean isRevisionVencidaPorQuinceDias(Revision revisionParam) {
		Calendar fechaActual = fechaService.obtenerInicioHoy();
		Date dateFechaCreacionRevision = revisionParam.getFechaCreacionRevision();
		UtilVencimientoRevision utilVencimientoRevision = new UtilVencimientoRevision();
		return utilVencimientoRevision.esRevisionVencida(dateFechaCreacionRevision, fechaActual.getTime());
	}

	public Revision registrarRevisionSinPropietario(Revision revision, Vehiculo vehiculo, Usuario usuario) {
		Usuario referenciaUsuario = em.getReference(Usuario.class, usuario.getUsuarioId());
		vehiculo.setUsuario(referenciaUsuario);
		Vehiculo vehiculoRegistrado = em.merge(vehiculo);
		em.flush();
		revision.setUsuario(referenciaUsuario);
		revision.setVehiculo(vehiculoRegistrado);
		Revision revisionCreada = em.merge(revision);
		return revisionCreada;
	}

	public void registrarConsecutivoRuntFupa(Integer revisionId, String consecutivoRunt, String numeroSolicitud) {
		Revision revision = em.getReference(Revision.class, revisionId);
		revision.setConsecutivoRunt(consecutivoRunt.trim());
		revision.setNumeroSolicitud(numeroSolicitud.trim());
		em.merge(revision);
	}

	public Revision registrarRevision(Revision revision, Vehiculo vehiculo, Propietario propietario,
			Propietario conductor, Usuario usuarioPorDefecto) {

		// Usuario usuarioPorDefecto = principalService.getUsuarioAutenticado();
		Usuario u = em.getReference(Usuario.class, usuarioPorDefecto.getUsuarioId());
		propietario.setUsuario(u);
		Propietario p = em.merge(propietario);
		Propietario c = null;
		if (conductor.getPropietarioId() == propietario.getPropietarioId()) {
			c = p;
		} else {
			conductor.setUsuario(u);
			c = em.merge(conductor);
		}
		vehiculo.setPropietario(p);
		vehiculo.setConductor(c);
		vehiculo.setUsuario(u);
		Vehiculo v = em.merge(vehiculo);// crea o actualiza el vehiculo
		revision.setPropietario(p);
		revision.setConductorId(c.getPropietarioId());
		revision.setVehiculo(v);
		revision.setUsuario(u);
		Revision revisionMerge = em.merge(revision);
		return revisionMerge;

	}

	public List<Revision> consultarRevisionesInspecciones(Date fechaInicial, Date fechaFinal,
			boolean segundoEnvioRealizado,Optional<Boolean> preventivaOpt) {
		List<Revision> revisiones = consultarRevisionesRegistradas(fechaInicial, fechaFinal, segundoEnvioRealizado,preventivaOpt);
		List<Revision> inspecciones = inspeccionService.revisionInspeccionPorFechaSiguiente(fechaInicial, fechaFinal,
				segundoEnvioRealizado);
		List<Revision> consolidadas = unirRevisiones(revisiones, inspecciones, segundoEnvioRealizado);
		return consolidadas;
	}

	public List<Revision> consultarRevisionesRegistradas(Date fechaInicial, Date fechaFinal,
			boolean segundoEnvioRealizado,Optional<Boolean> preventivaOpt) {
		String consulta = "SELECT NEW com.proambiente.modelo.dto.RevisionDTO(r.revisionId,r.vehiculo.vehiculoId,r.vehiculo.placa,"
				+ " r.numeroInspecciones,r.fechaCreacionRevision,r.aprobada,r.consecutivoRunt,r.primerEnvioRealizado,r.preventiva,r.segundoEnvioRealizado) "
				+ " FROM Revision r " + "WHERE r.fechaCreacionRevision " + "BETWEEN :fechaInicio AND :fechaFinal "
				+ " AND r.segundoEnvioRealizado =:segundoEnvio  ";
		String anuladas = " AND r.anulada = false ";//para incluir borrado suave
		
		String preventivaString = " AND r.preventiva=:preventiva ";
		StringBuilder sb = new StringBuilder();
		sb.append(consulta).append(anuladas);
		
		if(preventivaOpt.isPresent()) {
			sb.append(preventivaString);
		}
		Query query = em.createQuery( sb.toString() );
		Timestamp tsFechaInicial = new Timestamp(fechaInicial.getTime());
		Timestamp tsFechaFinal = new Timestamp(fechaFinal.getTime());
		query.setParameter("fechaInicio", tsFechaInicial);
		query.setParameter("fechaFinal", tsFechaFinal);
		query.setParameter("segundoEnvio", segundoEnvioRealizado);
		if(preventivaOpt.isPresent()) {
			query.setParameter("preventiva",preventivaOpt.get());
		}

		
		List<RevisionDTO> revisionesDTO = new ArrayList<>();
		List<Revision> revisiones = new ArrayList<>();
		try {
			revisionesDTO = query.getResultList();
			revisiones = revisionesDTO.stream().map(rdto -> RevisionDTOBuilder.revisionDesdeRevisionDTO(rdto))
					.collect(Collectors.toList());
		} catch (NoResultException nre) {
			logger.info("Ninguna revision encontrada");
		}
		return revisiones;
	}

	public List<Revision> consultarRevisionesVencidas() {
		String consulta = "SELECT NEW com.proambiente.modelo.dto.RevisionDTO(r.revisionId,r.vehiculo.vehiculoId,r.vehiculo.placa,"
				+ "r.numeroInspecciones,r.fechaCreacionRevision,r.aprobada,r.consecutivoRunt,r.primerEnvioRealizado,r.preventiva,r.segundoEnvioRealizado) "
				+ "FROM Revision r "
				+ "WHERE r.fechaCreacionRevision  BETWEEN :fechaInicio AND :fechaFin AND r.aprobada = false "
				+ " AND r.cerrada = false AND r.preventiva = false " + " ORDER by r.revisionId DESC";
		Query query = em.createQuery(consulta);
		Calendar fechaHoyMenosQuince = Calendar.getInstance();
		fechaHoyMenosQuince.add(Calendar.DAY_OF_MONTH, -15);

		Calendar fechaHoyMenosTresMeses = Calendar.getInstance();
		fechaHoyMenosTresMeses.add(Calendar.MONTH, -3);
		Timestamp tsFechaInicial = new Timestamp(fechaHoyMenosTresMeses.getTimeInMillis());
		query.setParameter("fechaInicio", tsFechaInicial);

		Timestamp tsFechaFinal = new Timestamp(fechaHoyMenosQuince.getTimeInMillis());
		query.setParameter("fechaFin", tsFechaFinal);

		EntityGraph<Revision> graph = em.createEntityGraph(Revision.class);
		graph.addAttributeNodes("vehiculo");
		query.setHint("javax.persistence.fetchgraph", graph);
		query.setMaxResults(50);

		List<RevisionDTO> revisionesDTO = new ArrayList<>();
		List<Revision> revisiones = new ArrayList<>();
		try {
			revisionesDTO = query.getResultList();
			revisiones = revisionesDTO.stream().map(rdto -> RevisionDTOBuilder.revisionDesdeRevisionDTO(rdto))
					.collect(Collectors.toList());
		} catch (NoResultException nre) {
			logger.info("Ninguna revision encontrada");
		}
		return revisiones;
	}

	public List<Revision> consultarRevisionesRegistradas(Date fechaInicial, Date fechaFinal, boolean pruebasFinalizadas,
			boolean filtrarConsecutivoRunt, boolean filtrarNumeroSolicitud) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Revision> cq = cb.createQuery(Revision.class);
		Root<Revision> root = cq.from(Revision.class);

		List<Predicate> predicates = new ArrayList<Predicate>();

		Expression<Timestamp> fechaCreacion = root.get("fechaCreacionRevision");
		Timestamp tsFechaInicio = new Timestamp(fechaInicial.getTime());
		Timestamp tsFechaFinal = new Timestamp(fechaFinal.getTime());
		Predicate pdFecha = cb.between(fechaCreacion, tsFechaInicio, tsFechaFinal);

		predicates.add(pdFecha);
		Expression<Boolean> pruebasFinalizadasExp = root.get("pruebasFinalizadas");
		Predicate pdPruebasFinalizadas = cb.equal(pruebasFinalizadasExp, pruebasFinalizadas);
		predicates.add(pdPruebasFinalizadas);

		Expression<String> consecRunt = root.get("consecutivoRunt");
		Predicate pdConsecutivoRunt = null;
		if (filtrarConsecutivoRunt) {
			pdConsecutivoRunt = cb.or(cb.isNull(consecRunt), cb.equal(consecRunt, ""));
			predicates.add(pdConsecutivoRunt);
		}
		Expression<String> numeroSolicitud = root.get("numeroSolicitud");
		Predicate pdNumeroSolicitud = null;
		if (filtrarNumeroSolicitud) {
			pdNumeroSolicitud = cb.or(cb.isNull(numeroSolicitud), cb.equal(numeroSolicitud, ""));
			predicates.add(pdNumeroSolicitud);
		}
		cq.select(root);
		cq.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
		cq.orderBy(cb.asc(root.get("revisionId")));
		cq.distinct(true);
		List<Revision> revisiones = em.createQuery(cq).getResultList();
		List<Revision> revisionesReinspeccion = inspeccionService.revisionInspeccionPorFechaSiguiente(fechaInicial,
				fechaFinal, false);
		for (Revision revision : revisionesReinspeccion) {
			if (filtrarConsecutivoRunt && !filtrarNumeroSolicitud) {
				if (revision.getConsecutivoRunt() == null || revision.getConsecutivoRunt().isEmpty()) {
					if (!revisiones.contains(revision))
						revisiones.add(revision);
				}
			}
			if (filtrarNumeroSolicitud && !filtrarConsecutivoRunt) {
				if (revision.getNumeroSolicitud() == null || revision.getNumeroSolicitud().isEmpty()) {
					if (!revisiones.contains(revision))
						revisiones.add(revision);
				}
			}

			if (filtrarConsecutivoRunt && filtrarNumeroSolicitud) {
				if ((revision.getConsecutivoRunt() == null || revision.getConsecutivoRunt().isEmpty())
						&& (revision.getNumeroSolicitud() == null || revision.getNumeroSolicitud().isEmpty())) {
					if (!revisiones.contains(revision))
						revisiones.add(revision);
				}
			}

		}
		return revisiones;
	}

	public long contarRevisionesRegistradas(Date fechaInicial, Date fechaFinal) {
		String consulta = "SELECT COUNT(r) FROM Revision r WHERE r.fechaCreacionRevision BETWEEN :fechaInicio AND :fechaFinal ";
		Query query = em.createQuery(consulta);
		Timestamp tsFechaInicial = new Timestamp(fechaInicial.getTime());
		Timestamp tsFechaFinal = new Timestamp(fechaFinal.getTime());
		query.setParameter("fechaInicio", tsFechaInicial);
		query.setParameter("fechaFinal", tsFechaFinal);
		// query.setParameter("pruebasFinalizadas", false);
		long numeroRevisiones = -1;
		try {
			numeroRevisiones = (long) query.getSingleResult();
		} catch (NoResultException nre) {
			System.out.println("Ninguna revision encontrada");
		}
		return numeroRevisiones;
	}

	public List<Revision> consultarRevisionesRegistradasPaginado(Date fechaInicial, Date fechaFinal,
			boolean pruebasFinalizadas, int resultadoInicial, int tamanioPagina) {
		String consulta = "SELECT r FROM Revision r WHERE r.fechaCreacionRevision BETWEEN :fechaInicio AND :fechaFinal ";
		Query query = em.createQuery(consulta);

		Timestamp tsFechaInicial = new Timestamp(fechaInicial.getTime());
		Timestamp tsFechaFinal = new Timestamp(fechaFinal.getTime());
		query.setParameter("fechaInicio", tsFechaInicial);
		query.setParameter("fechaFinal", tsFechaFinal);
		// query.setParameter("pruebasFinalizadas", pruebasFinalizadas);
		query.setFirstResult(resultadoInicial);
		query.setMaxResults(tamanioPagina);
		List<Revision> revisiones = new ArrayList<Revision>();
		try {
			revisiones = query.<Revision>getResultList();
		} catch (NoResultException nre) {
			System.out.println("Ninguna revision encontrada");
		}
		return revisiones;
	}

	public List<Revision> unirRevisiones(List<Revision> revisiones, List<Revision> inspecciones, boolean segundoEnvio) {
		for (Revision inspeccion : inspecciones) {
			if (!revisiones.contains(inspeccion)) {
				if (inspeccion.getSegundoEnvioRealizado() == segundoEnvio)
					revisiones.add(inspeccion);
			}
		}
		return revisiones;
	}

	public Vehiculo editarVehiculo(Vehiculo vehiculo) {

		if (vehiculo.getVehiculoId() != null) {

			Vehiculo v = em.merge(vehiculo);
			return v;

		} else {

			em.persist(vehiculo);

			return vehiculo;
		}
	}

	public Propietario editarPropietario(Propietario propietario) {

		if (propietario.getPropietarioId() != null && propietario.getNombres() != null
				&& !propietario.getNombres().isEmpty() && propietario.getApellidos() != null
				&& !propietario.getApellidos().isEmpty() && propietario.getDireccion() != null
				&& !propietario.getDireccion().isEmpty() && propietario.getTelefono1() != null
				&& !propietario.getTelefono1().isEmpty()) {

			Propietario p = em.merge(propietario);
			return p;
		} else {
			throw new RuntimeException("Datos de vehiculo invalidos o faltantes");
		}

	}

	public void editarIdPropietario(Propietario propietario, Propietario propietarioAnterior) {

		String consulta = "UPDATE propietario   SET propietario_id=?, tipo_identificacion=?, apellidos=?, nombres=?,"
				+ " usuario_registra_id=?, fecha_registro=?, telefono1=?, direccion=?, ciudad_id=?, numero_licencia=?, tipo_licencia=?, telefono2=?"
				+ " WHERE propietario_id = ?";
		Query q = em.createNativeQuery(consulta);
		q.setParameter(1, propietario.getPropietarioId());
		q.setParameter(2, propietario.getTipoIdentificacion());
		q.setParameter(3, propietario.getApellidos());
		q.setParameter(4, propietario.getNombres());
		q.setParameter(5, propietario.getUsuario().getUsuarioId());
		Calendar c = (Calendar.getInstance());
		c.setTime(propietario.getFechaRegistro());
		q.setParameter(6, c, TemporalType.TIMESTAMP);
		q.setParameter(7, propietario.getTelefono1());
		q.setParameter(8, propietario.getDireccion());
		q.setParameter(9, propietario.getCiudad().getCiudadId());
		q.setParameter(10, propietario.getNumeroLicencia());
		q.setParameter(11, propietario.getTipoLicencia());
		q.setParameter(12, propietario.getTelefono2());
		q.setParameter(13, propietarioAnterior.getPropietarioId());
		q.executeUpdate();

	}

	public Revision cargarRevision(Integer revisionId) {
		EntityGraph<Revision> graph = em.createEntityGraph(Revision.class);
		graph.addAttributeNodes("vehiculo");
		graph.addAttributeNodes("pruebas");
		graph.addSubgraph("pruebas").addAttributeNodes("tipoPrueba");
		graph.addSubgraph("vehiculo").addAttributeNodes("tipoVehiculo", "tipoCombustible", "servicio");

		Map<String, Object> hints = new HashMap<>();
		hints.put("javax.persistence.loadgraph", graph);
		Revision r = em.find(Revision.class, revisionId, hints);		
		return r;
	}

	public Revision cargarRevisionDTO(Integer revisionId, SingularAttribute<Revision, ?>... attrs) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Tuple> query = cb.createTupleQuery();
		Root<Revision> root = query.from(Revision.class);
		Selection[] selection = new Selection[attrs.length];
		for (int i = 0; i < attrs.length; i++) {
			selection[i] = root.get(attrs[i]);
		}
		query.multiselect(selection);
		query.where(cb.equal(root.get(Revision_.revisionId), revisionId));
		Tuple tupla = em.createQuery(query).getSingleResult();
		Revision r = com.proambiente.webapp.service.builderdto.RevisionDTOConstructor.crearProyectionRevision(tupla, attrs);
		return r;
	}

	public Revision buscarRevisionPorPruebaId(Integer pruebaId) {
		Prueba prueba = em.find(Prueba.class, pruebaId);
		return prueba.getRevision();
	}

	public Revision buscarRevisionPorPlaca(String placa) {
		String consulta = "SELECT MAX(r.revisionId) FROM Revision r WHERE r.vehiculo.placa=:placa";
		String anulada = " AND r.anulada = false";
		
		StringBuilder sb = new StringBuilder();
		sb.append(consulta).append(anulada);
		
		
		TypedQuery<Integer> query = em.createQuery(sb.toString(), Integer.class);
		query.setParameter("placa", placa);
		
		Integer revisionId = query.getSingleResult();
		if (revisionId != null) {
			Revision revisionPruebas = cargarRevision(revisionId);
			return revisionPruebas;
		} else {
			return null;
		}
	}

	public List<Revision> buscarTodasRevisionesPorPlaca(String placa) {

		String consulta = "SELECT r FROM Revision r WHERE r.vehiculo.placa=:placa";
		String anulada =  " AND r.anulada = false";//importantes los espacios
		
		StringBuilder sb = new StringBuilder();
		sb.append(consulta).append(anulada);
		TypedQuery<Revision> query = em.createQuery(sb.toString(), Revision.class);

		EntityGraph<Revision> graph = em.createEntityGraph(Revision.class);
		graph.addAttributeNodes("vehiculo");
		query.setHint("javax.persistence.fetchgraph", graph);

		query.setParameter("placa", placa);
		List<Revision> revisiones = query.getResultList();
		return revisiones;
	}

	public Prueba buscarPruebaNoFinalizada(String placa, Integer tipoPrueba) throws Exception {
		String consulta = "SELECT p FROM Prueba p WHERE p.revision.revisionId "
				+ " IN (SELECT MAX(r.revisionId) FROM Revision r WHERE r.vehiculo.placa=:placa AND r.cerrada=false AND r.anulada = false) "
				+ " AND p.finalizada=false AND p.tipoPrueba.tipoPruebaId=:tipoPrueba";
		TypedQuery<Prueba> query = em.createQuery(consulta, Prueba.class);
		query.setParameter("placa", placa);
		query.setParameter("tipoPrueba", tipoPrueba);
		Prueba p = null;
		try {
			p = query.getSingleResult();
		} catch (NoResultException nre) {
			System.out.println("No prueba autorizada");
		}
		return p;
	}

	public Prueba buscarPruebaPorTipoPorRevision(Integer revisionId, Integer tipoPrueba) {
		String consulta = "SELECT MAX(p) FROM Prueba p WHERE p.revision.revisionId =:revisionId "
				+ " AND p.tipoPrueba.tipoPruebaId=:tipoPrueba GROUP BY p.tipoPrueba.tipoPruebaId";
		TypedQuery<Prueba> query = em.createQuery(consulta, Prueba.class);
		query.setParameter("revisionId", revisionId);
		query.setParameter("tipoPrueba", tipoPrueba);
		Prueba p = null;
		try {
			p = query.getSingleResult();
		} catch (NoResultException nre) {
			System.out.println("No prueba autorizada");
		}
		return p;
	}

	public List<PruebaDTO> pruebasDTOPorIdRevisiones(List<Integer> idRevisiones) {
		List<PruebaDTO> pruebasDTO = new ArrayList<>();
		if (idRevisiones != null && !idRevisiones.isEmpty()) {
			String consulta = "SELECT MAX(p.pruebaId) FROM Prueba p WHERE p.revision.revisionId IN :idRevisiones "
					+ " GROUP BY p.tipoPrueba.tipoPruebaId, p.revision.revisionId";
			TypedQuery<Integer> query = em.createQuery(consulta, Integer.class);
			query.setParameter("idRevisiones", idRevisiones);
			List<Integer> idPruebas = query.getResultList();

			if (idPruebas != null && !idPruebas.isEmpty()) {
				consulta = "SELECT NEW com.proambiente.modelo.dto.PruebaDTO(p.pruebaId,p.aprobada,p.finalizada,p.abortada,p.revision.revisionId,p.tipoPrueba.tipoPruebaId) "
						+ "FROM Prueba p WHERE p.pruebaId IN :idsPrueba";
				Query qPruebas = em.createQuery(consulta);
				qPruebas.setParameter("idsPrueba", idPruebas);
				pruebasDTO = qPruebas.getResultList();
			}
			;

		}
		return pruebasDTO;
	}

	public boolean isRevisionAprobada(Integer revisionId) {
		String evaluacionRevision = evaluarRevision(revisionId);		
		boolean revisionAprobada = evaluacionRevision.equalsIgnoreCase("aprobada");
		return revisionAprobada;
	}

	public String evaluarRevision(Integer revisionId) {
		StoredProcedureQuery storedProcedureQuery = em.createStoredProcedureQuery("evaluar_revision");
		storedProcedureQuery.registerStoredProcedureParameter(1, Integer.class, ParameterMode.IN);
		storedProcedureQuery.setParameter(1, revisionId);
		// storedProcedureQuery.execute();
		Object resultado = storedProcedureQuery.getSingleResult();
		String strResultado = (String) resultado;
		return strResultado;
	}

	

	/**
	 * Se borra la revision de manera suave
	 * @param revision
	 */
	public void borrarRevision(Revision revision) {
		Revision r = em.find(Revision.class, revision.getRevisionId());
		r.setAnulada(true);
		r.setPin("");
		em.merge(r);
		Log.info("Revision " + revision.getRevisionId() + " borrada");
	}

	public void editarRevision(Revision revision) {
		em.merge(revision);
	}

	public void cambiarComentario(Integer revisionId, String comentario) {
		try {
			Revision revision = em.find(Revision.class, revisionId);
			revision.setComentario(comentario);
			em.merge(revision);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void actualizarUsuarioResponsable(Revision revision) {

		InformacionCda infoCda = informacionCDADAO.find(1);
		revision.setUsuarioResponsableId(infoCda.getUsuarioResp().getUsuarioId());
		editarRevision(revision);

	}

	public void registrarPinParaRevision(String placa, String pin) {
		Revision revision = buscarRevisionPorPlaca(placa);
		revision.setPin(pin);
		revisionDAO.edit(revision);
	}

	public void registrarPrimerEnvio(Integer revisionId) {
		String string = "UPDATE Revision r SET r.primerEnvioRealizado = true WHERE r.revisionId =:revisionId";
		Query query = em.createQuery(string);
		query.setParameter("revisionId", revisionId);
		int numExec = query.executeUpdate();
		logger.log(Level.INFO, "Num registro actu " + numExec);
	}

	public void registrarSegundoEnvio(Integer revisionId) {
		String string = "UPDATE Revision r SET r.segundoEnvioRealizado = true WHERE r.revisionId =:revisionId";
		Query query = em.createQuery(string);
		query.setParameter("revisionId", revisionId);
		int numExec = query.executeUpdate();

		logger.log(Level.INFO, "Num reg actual " + numExec);
	}

}
