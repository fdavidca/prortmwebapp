package com.proambiente.webapp.service;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.inject.Named;
import javax.sql.DataSource;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

import com.proambiente.modelo.Certificado;
import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.Revision;
import com.proambiente.webapp.dao.InformacionCdaFacade;


@Named
public class LoadCertificadoService {
	
	
	private static final Logger logger = Logger
			.getLogger(LoadCertificadoService.class.getName());
	
	private HashMap<String, Object> parametros;
	
	@Resource(lookup = "java:/jboss/datasources/PrortmDS")
	DataSource dataSource;
	
	Revision r;
	
	@Inject
	RevisionService revisionService;
	
	@Inject
	InformacionCdaFacade infoCdaDAO;
	
	@Inject
	CertificadoService certificadoService;
	
	
	
	public JasperPrint cargarCertificado(int revisionId) {
		InputStream is = null;
		JasperPrint reporte = null;
		parametros = new HashMap<>();
		parametros.put("ID_HP",new Long(revisionId));
		Connection cn = null;
		try {
			r = revisionService.cargarRevision(revisionId);
			parametros.put("consecutivo_runt",r.getConsecutivoRunt());
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy hh:mm:ss ");
			String fecha=sdf.format(r.getFechaCreacionRevision());
			parametros.put("fecha_inspeccion",fecha);
			cn = dataSource.getConnection();
			
			InformacionCda infoCDA = infoCdaDAO.find(1);
			
			parametros.put("NombreCDA", infoCDA.getNombre());
			parametros.put("NITCDA",infoCDA.getNit() );
			parametros.put("CertificadosConformidad", infoCDA.getCertificadoConformidad());
			parametros.put("Responsable",infoCDA.getUsuarioResp().getNombres() + " " +infoCDA.getUsuarioResp().getApellidos());
			parametros.put("ConsecutivoRUNT", r.getConsecutivoRunt());
			is = this
					.getClass()
					.getClassLoader()
					.getResourceAsStream(
							"com/proambiente/webapp/reporte/certificado.jasper");
			Certificado certificado = certificadoService.consultarUltimoCertificado(revisionId);
			Timestamp tsFechaExpedicion = certificado.getFechaExpedicion();
			Timestamp tsFechaVencimiento = certificado.getFechaVencimiento();
			Date fechaExpedicion = new Date(tsFechaExpedicion.getTime());
			Date fechaVencimiento = new Date(tsFechaVencimiento.getTime());		
			
			Calendar calFechaExpedicion = Calendar.getInstance();
			Calendar calFechaVencimiento = Calendar.getInstance();
			calFechaExpedicion.setTime(fechaExpedicion);		
			calFechaVencimiento.setTime(fechaVencimiento);
			
			Integer diaExpedicion = calFechaExpedicion.get(Calendar.DAY_OF_MONTH);
			Integer mesExpedicion = calFechaExpedicion.get(Calendar.MONTH)+1;
			Integer anioExpedicion = calFechaExpedicion.get(Calendar.YEAR);
			
			parametros.put("DiaExpedicion",String.valueOf(diaExpedicion));
			parametros.put("MesExpedicion", String.valueOf(mesExpedicion));
			parametros.put("AñoExpedicion", String.valueOf(anioExpedicion));
			
			Integer diaVencimiento = calFechaVencimiento.get(Calendar.DAY_OF_MONTH);
			Integer mesVencimiento = calFechaVencimiento.get(Calendar.MONTH)+1;
			Integer anioVencimiento = calFechaVencimiento.get(Calendar.YEAR);
			
			
			parametros.put("DiaVencimiento",String.valueOf(diaVencimiento));
			parametros.put("MesVencimiento",String.valueOf(mesVencimiento));
			parametros.put("AñoVencimiento",String.valueOf(anioVencimiento));
			reporte = JasperFillManager.fillReport(is, parametros, cn);
			logger.info("Certificado cargado");
			
			
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error cargando certificado", exc);
		} finally {
			try {
				cn.close();
			} catch (Exception e2) {
				logger.info("Excepcion cerrando la conexion");
			}
			try {
				is.close();
			} catch (Exception exc) {
				logger.info("Excepcion cerrando el flujo de datos");
			}
		}
		return reporte;
	}

}
