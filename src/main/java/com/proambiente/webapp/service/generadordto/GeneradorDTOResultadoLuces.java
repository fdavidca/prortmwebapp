package com.proambiente.webapp.service.generadordto;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.OptionalDouble;

import com.proambiente.modelo.Medida;
import com.proambiente.webapp.service.util.UtilInformesMedidas;
import com.proambiente.webapp.util.ConstantesMedidasLuces;
import com.proambiente.webapp.util.dto.ResultadoLucesDtoV2;

public class GeneradorDTOResultadoLuces {

	UtilInformesMedidas utilInformesMedidas = new UtilInformesMedidas();

	DecimalFormat df = new DecimalFormat("0.0");

	private static final char DECIMAL_SEPARATOR_COMA = ',';
	private static final char DECIMAL_SEPARATOR_PUNTO = '.';
	private static final String SI = "S";
	private static final String NO = "N";

	public ResultadoLucesDtoV2 generarResultadoLucesVehiculo(List<Medida> medidas) {
		utilInformesMedidas.ordenarListaMedidas(medidas);

		ResultadoLucesDtoV2 resultado = new ResultadoLucesDtoV2();

		// bajas derechas
		OptionalDouble intensidadBajaDerecha = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasLuces.INTENSIDAD_BAJA_DERECHA);
		String vlrIntensidadBajaDerecha = intensidadBajaDerecha.isPresent()
				? df.format(intensidadBajaDerecha.getAsDouble())
				: "";
		resultado.setIntensidadBajaDerecha1(vlrIntensidadBajaDerecha);

		OptionalDouble intensidadBajaDerecha2 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasLuces.INTENSIDAD_BAJA_DERECHA_DOS);
		String vlrIntensidadBajaDerecha2 = intensidadBajaDerecha2.isPresent()
				? df.format(intensidadBajaDerecha2.getAsDouble())
				: "";
		resultado.setIntensidadBajaDerecha2(vlrIntensidadBajaDerecha2);

		OptionalDouble intensidadBajaDerecha3 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasLuces.INTENSIDAD_BAJA_DERECHA_TRES);
		String vlrIntensidadBajaDerecha3 = intensidadBajaDerecha3.isPresent()
				? df.format(intensidadBajaDerecha3.getAsDouble())
				: "";
		resultado.setIntensidadBajaDerecha3(vlrIntensidadBajaDerecha3);

		// bajas izquierdas
		OptionalDouble intensidadBajaIzquierda = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasLuces.INTENSIDAD_BAJA_IZQUIERDA);
		String vlrIntensidadBajaIzquierda = intensidadBajaIzquierda.isPresent()
				? df.format(intensidadBajaIzquierda.getAsDouble())
				: "";
		resultado.setIntensidadBajaIzquierda1(vlrIntensidadBajaIzquierda);

		OptionalDouble intensidadBajaIzquierda2 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasLuces.INTENSIDAD_BAJA_IZQUIERDA_DOS);
		String vlrIntensidadBajaIzquierda2 = intensidadBajaIzquierda2.isPresent()
				? df.format(intensidadBajaIzquierda2.getAsDouble())
				: "";
		resultado.setIntensidadBajaIzquierda2(vlrIntensidadBajaIzquierda2);

		OptionalDouble intensidadBajaIzquierda3 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasLuces.INTENSIDAD_BAJA_IZQUIERDA_TRES);
		String vlrIntensidadBajaIzquierda3 = intensidadBajaIzquierda3.isPresent()
				? df.format(intensidadBajaIzquierda3.getAsDouble())
				: "";
		resultado.setIntensidadBajaIzquierda3(vlrIntensidadBajaIzquierda3);

		// incliancion bajas derechas
		OptionalDouble incBajaDerecha = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasLuces.INCLINACION_BAJA_DERECHA);
		String vlrIncBajaDerecha = incBajaDerecha.isPresent() ? df.format(incBajaDerecha.getAsDouble()) : "";
		resultado.setInclinacionBajaDerecha1(vlrIncBajaDerecha);

		OptionalDouble incBajaDerecha2 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasLuces.INCLINACION_BAJA_DERECHA_DOS);
		String vlrIncBajaDerecha2 = incBajaDerecha2.isPresent() ? df.format(incBajaDerecha2.getAsDouble()) : "";
		resultado.setInclinacionBajaDerecha2(vlrIncBajaDerecha2);

		OptionalDouble incBajaDerecha3 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasLuces.INCLINACION_BAJA_DERECHA_TRES);
		String vlrIncBajaDerecha3 = incBajaDerecha3.isPresent() ? df.format(incBajaDerecha3.getAsDouble()) : "";
		resultado.setInclinacionBajaDerecha3(vlrIncBajaDerecha3);

		// inclinacion bajas izquierdas
		OptionalDouble incBajaIzquierda = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasLuces.INCLINACION_BAJA_IZQUIERDA);
		String vlrIncBajaIzquierda = incBajaIzquierda.isPresent() ? df.format(incBajaIzquierda.getAsDouble()) : "";
		resultado.setInclinacionBajaIzquierda1(vlrIncBajaIzquierda);

		OptionalDouble incBajaIzquierda2 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasLuces.INCLINACION_BAJA_IZQUIERDA_DOS);
		String vlrIncBajaIzquierda2 = incBajaIzquierda2.isPresent() ? df.format(incBajaIzquierda2.getAsDouble()) : "";
		resultado.setInclinacionBajaIzquierda2(vlrIncBajaIzquierda2);

		OptionalDouble incBajaIzquierda3 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasLuces.INCLINACION_BAJA_IZQUIERDA_TRES);
		String vlrIncBajaIzquierda3 = incBajaIzquierda3.isPresent() ? df.format(incBajaIzquierda3.getAsDouble()) : "";
		resultado.setInclinacionBajaIzquierda3(vlrIncBajaIzquierda3);

		// altas derechas
		OptionalDouble intensidadAltaDerecha1 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasLuces.INTENSIDAD_ALTA_DERECHA);
		String vlrIntensidadAltaDerecha1 = intensidadAltaDerecha1.isPresent()
				? df.format(intensidadAltaDerecha1.getAsDouble())
				: "";
		resultado.setIntensidadAltaDerecha1(vlrIntensidadAltaDerecha1);

		OptionalDouble intensidadAltaDerecha2 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasLuces.INTENSIDAD_ALTA_DERECHA_DOS);
		String vlrIntensidadAltaDerecha2 = intensidadAltaDerecha2.isPresent()
				? df.format(intensidadAltaDerecha2.getAsDouble())
				: "";
		resultado.setIntensidadAltaDerecha2(vlrIntensidadAltaDerecha2);

		OptionalDouble intensidadAltaDerecha3 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasLuces.INTENSIDAD_ALTA_DERECHA_TRES);
		String vlrIntensidadAltaDerecha3 = intensidadAltaDerecha3.isPresent()
				? df.format(intensidadAltaDerecha3.getAsDouble())
				: "";
		resultado.setIntensidadAltaDerecha3(vlrIntensidadAltaDerecha3);

		// altas izquierdas
		OptionalDouble intensidadAltaIzquierda1 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasLuces.INTENSIDAD_ALTA_IZQUIERDA);
		String vlrIntensidadAltaIzquierda1 = intensidadAltaIzquierda1.isPresent()
				? df.format(intensidadAltaIzquierda1.getAsDouble())
				: "";
		resultado.setIntensidadAltaIzquierda1(vlrIntensidadAltaIzquierda1);

		OptionalDouble intensidadAltaIzquierda2 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasLuces.INTENSIDAD_ALTA_IZQUIERDA_DOS);
		String vlrIntensidadAltaIzquierda2 = intensidadAltaIzquierda2.isPresent()
				? df.format(intensidadAltaIzquierda2.getAsDouble())
				: "";
		resultado.setIntensidadAltaIzquierda2(vlrIntensidadAltaIzquierda2);

		OptionalDouble intensidadAltaIzquierda3 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasLuces.INTENSIDAD_ALTA_IZQUIERDA_TRES);
		String vlrIntensidadAltaIzquierda3 = intensidadAltaIzquierda3.isPresent()
				? df.format(intensidadAltaIzquierda3.getAsDouble())
				: "";
		resultado.setIntensidadAltaIzquierda3(vlrIntensidadAltaIzquierda3);
		
		
		OptionalDouble intensidadExploradoraDerecha1 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasLuces.INTENSIDAD_EXPLORADORA_DERECHA);
		String vlrIntensidadExploradoraDerecha1 = intensidadExploradoraDerecha1.isPresent()
				? df.format(intensidadExploradoraDerecha1.getAsDouble())
				: "";
		resultado.setIntensidadExploradoraDerecha1(vlrIntensidadExploradoraDerecha1);
		
		OptionalDouble intensidadExploradoraDerecha2 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasLuces.INTENSIDAD_EXPLORADORA_DERECHA_DOS);
		String vlrIntensidadExploradoraDerecha2 = intensidadExploradoraDerecha2.isPresent()
				? df.format(intensidadExploradoraDerecha2.getAsDouble())
				: "";
		resultado.setIntensidadExploradoraDerecha2(vlrIntensidadExploradoraDerecha2);
		
		
		OptionalDouble intensidadExploradoraDerecha3 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasLuces.INTENSIDAD_EXPLORADORA_DERECHA_TRES);
		String vlrIntensidadExploradoraDerecha3 = intensidadExploradoraDerecha3.isPresent()
				? df.format(intensidadExploradoraDerecha3.getAsDouble())
				: "";
		resultado.setIntensidadExploradoraDerecha3(vlrIntensidadExploradoraDerecha3);
		
		
		OptionalDouble intensidadExploradoraIzquierda1 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasLuces.INTENSIDAD_EXPLORADORA_IZQUIERDA);
		String vlrIntensidadExploradoraIzquierda1 = intensidadExploradoraIzquierda1.isPresent()
				? df.format(intensidadExploradoraIzquierda1.getAsDouble())
				: "";
		resultado.setIntensidadExploradoraIzquierda1(vlrIntensidadExploradoraIzquierda1);
		
		OptionalDouble intensidadExploradoraIzquierda2 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasLuces.INTENSIDAD_EXPLORADORA_IZQUIERDA_DOS);
		String vlrIntensidadExploradoraIzquierda2 = intensidadExploradoraIzquierda2.isPresent()
				? df.format(intensidadExploradoraIzquierda2.getAsDouble())
				: "";
		resultado.setIntensidadExploradoraIzquierda2(vlrIntensidadExploradoraIzquierda2);
		
		OptionalDouble intensidadExploradoraIzquierda3 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasLuces.INTENSIDAD_EXPLORADORA_IZQUIERDA_TRES);
		String vlrIntensidadExploradoraIzquierda3 = intensidadExploradoraIzquierda3.isPresent()
				? df.format(intensidadExploradoraIzquierda3.getAsDouble())
				: "";
		resultado.setIntensidadExploradoraIzquierda3(vlrIntensidadExploradoraIzquierda3);

		OptionalDouble lucesSimultaneas = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasLuces.LUCES_BAJAS_ALTAS_EXPL_SIMULTANEAS);
		if (lucesSimultaneas.isPresent()) {
			
				resultado.setDerBajaSimultaneas(SI);
				resultado.setIzqBajaSimultaneas(SI);
				resultado.setDerAltaSimultaneas(SI);
				resultado.setIzqAltaSimultaneas(SI);
				resultado.setDerExploradoraSimultaneas(SI);
				resultado.setIzqExploradoraSimultaneas(SI);
			
		} 
		
		OptionalDouble lucesBajasExpl = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasLuces.LUCES_BAJAS_EXPL_SIMULTANEAS);
		if(lucesBajasExpl.isPresent()) {
			resultado.setDerBajaSimultaneas(SI);
			resultado.setIzqBajaSimultaneas(SI);
			resultado.setDerAltaSimultaneas(NO);
			resultado.setIzqAltaSimultaneas(NO);
			resultado.setDerExploradoraSimultaneas(SI);
			resultado.setIzqExploradoraSimultaneas(SI);
		}
		
		OptionalDouble lucesAltasExplSimultaneas = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasLuces.LUCES_ALTAS_EXPL_SIMULTANEAS);
		if(lucesAltasExplSimultaneas.isPresent()) {
			resultado.setDerBajaSimultaneas(NO);
			resultado.setIzqBajaSimultaneas(NO);
			resultado.setDerAltaSimultaneas(SI);
			resultado.setIzqAltaSimultaneas(SI);
			resultado.setDerExploradoraSimultaneas(NO);
			resultado.setIzqExploradoraSimultaneas(NO);
		}
		
		OptionalDouble lucesNingunaSimultanea = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasLuces.LUCES_NINGUNA_SIMULTANEA);
		if(lucesNingunaSimultanea.isPresent()) {
			resultado.setDerBajaSimultaneas(NO);
			resultado.setIzqBajaSimultaneas(NO);
			resultado.setDerAltaSimultaneas(NO);
			resultado.setIzqAltaSimultaneas(NO);
			resultado.setDerExploradoraSimultaneas(NO);
			resultado.setIzqExploradoraSimultaneas(NO);
		}
		
		OptionalDouble lucesNingunaSimultaneaIncluidoExploradoras = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasLuces.LUCES_BAJAS_ALTAS_NO_SIMUL_NO_EXPL);
		if( lucesNingunaSimultaneaIncluidoExploradoras.isPresent()) {
			resultado.setDerBajaSimultaneas(NO);
			resultado.setIzqBajaSimultaneas(NO);
			resultado.setDerAltaSimultaneas(NO);
			resultado.setIzqAltaSimultaneas(NO);
			resultado.setDerExploradoraSimultaneas(NO);
			resultado.setIzqExploradoraSimultaneas(NO);
		}
		
		
		
		OptionalDouble sumatoria = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasLuces.SUMA_LUCES);
		String vlrSumatoria = sumatoria.isPresent() ? df.format(sumatoria.getAsDouble()) : "";
		resultado.setSumatoriaIntensidad(vlrSumatoria);
		return resultado;
	}

	public ResultadoLucesDtoV2 generarResultadoLucesMotocicleta(List<Medida> medidas) {
		ResultadoLucesDtoV2 resultado = new ResultadoLucesDtoV2();
		utilInformesMedidas.ordenarListaMedidas(medidas);

		// bajas derechas
		OptionalDouble intensidadBajaDerecha = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasLuces.INTENSIDAD_BAJA_DERECHA_MOTOS);
		String vlrIntensidadBajaDerecha = intensidadBajaDerecha.isPresent()
				? df.format(intensidadBajaDerecha.getAsDouble())
				: "";
		resultado.setIntensidadBajaDerecha1(vlrIntensidadBajaDerecha);
		
		OptionalDouble intensidadBajaIzquierda = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasLuces.INTENSIDAD_BAJA_IZQUIERDA_MOTOS);
		String vlrIntensidadBajaIzquierda = intensidadBajaIzquierda.isPresent()
				? df.format(intensidadBajaIzquierda.getAsDouble())
				: "";
		resultado.setIntensidadBajaIzquierda1(vlrIntensidadBajaIzquierda);
		
		OptionalDouble incBajaDerecha = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasLuces.INCLINACION_BAJA_DERECHA_MOTOS);
		String vlrIncBajaDerecha = incBajaDerecha.isPresent() ? df.format(incBajaDerecha.getAsDouble()) : "";
		resultado.setInclinacionBajaDerecha1(vlrIncBajaDerecha);
		
		OptionalDouble incBajaIzquierda = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasLuces.INCLINACION_BAJA_IZQUIERDA_MOTOS);
		String vlrIncBajaIzquierda = incBajaIzquierda.isPresent() ? df.format(incBajaIzquierda.getAsDouble()) : "";
		resultado.setInclinacionBajaIzquierda1(vlrIncBajaIzquierda);
		
		OptionalDouble intensidadAltaDerecha1 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasLuces.INTENSIDAD_ALTA_DERECHA_MOTOS);
		String vlrIntensidadAltaDerecha1 = intensidadAltaDerecha1.isPresent()
				? df.format(intensidadAltaDerecha1.getAsDouble())
				: "";
		resultado.setIntensidadAltaDerecha1(vlrIntensidadAltaDerecha1);
		
		OptionalDouble intensidadAltaIzquierda1 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasLuces.INTENSIDAD_ALTA_IZQUIERDA_MOTOS);
		String vlrIntensidadAltaIzquierda1 = intensidadAltaIzquierda1.isPresent()
				? df.format(intensidadAltaIzquierda1.getAsDouble())
				: "";
		resultado.setIntensidadAltaIzquierda1(vlrIntensidadAltaIzquierda1);



		return resultado;
	}

	public void setPuntoComoSeparadorDecimales() {
		DecimalFormatSymbols ds = DecimalFormatSymbols.getInstance();
		ds.setDecimalSeparator(DECIMAL_SEPARATOR_PUNTO);
		df.setDecimalFormatSymbols(ds);
	}

	public void setComaComoSeparadorDecimales() {
		DecimalFormatSymbols ds = DecimalFormatSymbols.getInstance();
		ds.setDecimalSeparator(DECIMAL_SEPARATOR_COMA);
		df.setDecimalFormatSymbols(ds);
	}

}
