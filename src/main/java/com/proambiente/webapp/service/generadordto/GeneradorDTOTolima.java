package com.proambiente.webapp.service.generadordto;

import com.proambiente.modelo.Vehiculo;
import com.proambiente.webapp.util.dto.InformeCarInformacionCDADTO;

public class GeneradorDTOTolima {

	private static final String NO = "NO";

	public static void ponerInformacionVehiculo(InformeCarInformacionCDADTO informeDTO, Vehiculo vehiculo) {
		
		informeDTO.setMarca(String.valueOf(vehiculo.getMarca().getNombre()));
		informeDTO.setLinea(String.valueOf(vehiculo.getLineaVehiculo().getNombre()));
		informeDTO.setModelo(String.valueOf(vehiculo.getModelo()));
		informeDTO.setPlaca(String.valueOf(vehiculo.getPlaca()));
		informeDTO.setCilindraje(vehiculo.getCilindraje()==null?"0":String.valueOf(vehiculo.getCilindraje()) );
		informeDTO.setClaseVehiculo(String.valueOf(vehiculo.getClaseVehiculo().getNombre()));
		informeDTO.setServicio(String.valueOf(vehiculo.getServicio().getNombre()));

		informeDTO.setCombustible(String.valueOf(vehiculo.getTipoCombustible().getNombre()));
		informeDTO.setNumeroMotor(vehiculo.getNumeroMotor() != null ? vehiculo.getNumeroMotor() : "");
		informeDTO.setVin(vehiculo.getVin() != null ? vehiculo.getVin() : "");
		informeDTO.setLicenciaTransito(vehiculo.getNumeroLicencia() != null ? vehiculo.getNumeroLicencia() : "");
		informeDTO.setModificacionesMotor(NO);// vehiculo.getModificacionesMotor()? SI :
		// NO;// Modificaciones
		informeDTO.setKilometraje( vehiculo.getKilometraje() == null ? "0" :String.valueOf(vehiculo.getKilometraje()) );

		informeDTO.setPotenciaDelMotor("");// String.valueOf(vehiculo.getPotenciaMotor());//
		// Potencia del motor
		String diametroString = vehiculo.getDiametroExosto() != null ? String.valueOf(vehiculo.getDiametroExosto())
				: " ";
		informeDTO.setLtoeDiametro(diametroString);

		informeDTO.setTiemposMotor(String.valueOf(vehiculo.getTiemposMotor()) + "T");
		informeDTO.setDisenio(vehiculo.getDiseno());
	}

}
