package com.proambiente.webapp.service.generadordto;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.proambiente.modelo.Defecto;
import com.proambiente.modelo.Equipo;
import com.proambiente.modelo.Medida;
import com.proambiente.modelo.dto.ResultadoDieselDTO;
import com.proambiente.modelo.dto.ResultadoFasDTO;
import com.proambiente.modelo.dto.ResultadoLucesDTO;
import com.proambiente.modelo.dto.ResultadoOttoMotocicletasDTO;
import com.proambiente.modelo.dto.ResultadoOttoVehiculosDTO;
import com.proambiente.modelo.dto.ResultadoSonometriaDTO;
import com.proambiente.modelo.dto.ResultadoTaximetroDTO;
import com.proambiente.webapp.service.util.UtilInformesDefectos;
import com.proambiente.webapp.service.util.UtilInformesMedidas;
import com.proambiente.webapp.util.ConstantesCodigosDefectoGases;
import com.proambiente.webapp.util.ConstantesDefectosTaximetro;
import com.proambiente.webapp.util.ConstantesMedidasAlineacion;
import com.proambiente.webapp.util.ConstantesMedidasFrenos;
import com.proambiente.webapp.util.ConstantesMedidasGases;
import com.proambiente.webapp.util.ConstantesMedidasLuces;
import com.proambiente.webapp.util.ConstantesMedidasSonometria;
import com.proambiente.webapp.util.ConstantesMedidasSuspension;
import com.proambiente.webapp.util.ConstantesMedidasTaximetro;
import com.proambiente.webapp.util.dto.ResultadoDieselDTOInformes;
import com.proambiente.webapp.util.dto.ResultadoOttoInformeDTO;
import com.proambiente.webapp.util.dto.ResultadoOttoMotocicletasInformeDTO;
import com.proambiente.webapp.util.dto.ResultadoSonometriaInformeDTO;

public class GeneradorDTOResultados {

	UtilInformesMedidas utilInformesMedidas = new UtilInformesMedidas();
	private UtilInformesDefectos utilDefectos = new UtilInformesDefectos();

	DecimalFormat df = new DecimalFormat("#");
	private static final String DF_SIN_DECIMALES = "#";
	private static final String DF_UN_DECIMAL = "0.0";
	private static final String DF_DOS_DECIMALES = "#0.00";
	private static final String DF_TRES_DECIMALES = "0.000";
	private static final char DECIMAL_SEPARATOR_COMA = ',';
	private static final char DECIMAL_SEPARATOR_PUNTO = '.';
	public static final String TRUE = "TRUE";
	public static final String FALSE = "FALSE";

	Map<Integer, String> map = new HashMap<>();// mapa que relaciona la medida
												// con el numero de decimales

	public GeneradorDTOResultados() {
		DecimalFormatSymbols ds = DecimalFormatSymbols.getInstance();
		ds.setDecimalSeparator(DECIMAL_SEPARATOR_COMA);
		df.setDecimalFormatSymbols(ds);

		// medidas de frenos
		map.put(ConstantesMedidasFrenos.EFICACIA_FRENADO_TOTAL, DF_TRES_DECIMALES);
		map.put(ConstantesMedidasFrenos.EFICACIA_FRENADO_AUXILIAR,DF_TRES_DECIMALES );
		map.put(ConstantesMedidasFrenos.DESEQUILIBRIO_EJE_UNO, DF_TRES_DECIMALES);
		map.put(ConstantesMedidasFrenos.DESEQUILIBRIO_EJE_DOS, DF_TRES_DECIMALES);
		map.put(ConstantesMedidasFrenos.DESEQUILIBRIO_EJE_TRES, DF_TRES_DECIMALES);
		map.put(ConstantesMedidasFrenos.DESEQUILIBRIO_EJE_CUATRO, DF_TRES_DECIMALES);
		map.put(ConstantesMedidasFrenos.DESEQUILIBRIO_EJE_CINCO, DF_TRES_DECIMALES);
		map.put(ConstantesMedidasFrenos.DESEQUILIBRIO_EJE_SEIS, DF_TRES_DECIMALES);
		map.put(ConstantesMedidasFrenos.FUERZA_IZQUIERDA_EJE_UNO, DF_SIN_DECIMALES);
		map.put(ConstantesMedidasFrenos.FUERZA_IZQUIERDA_EJE_DOS, DF_SIN_DECIMALES);
		map.put(ConstantesMedidasFrenos.FUERZA_IZQUIERDA_EJE_TRES, DF_SIN_DECIMALES);
		map.put(ConstantesMedidasFrenos.FUERZA_IZQUIERDA_EJE_CUATRO, DF_SIN_DECIMALES);
		map.put(ConstantesMedidasFrenos.FUERZA_IZQUIERDA_EJE_CINCO, DF_SIN_DECIMALES);
		map.put(ConstantesMedidasFrenos.FUERZA_IZQUIERDA_EJE_SEIS, DF_SIN_DECIMALES);
		map.put(ConstantesMedidasFrenos.FUERZA_DERECHA_EJE_UNO, DF_SIN_DECIMALES);
		map.put(ConstantesMedidasFrenos.FUERZA_DERECHA_EJE_DOS, DF_SIN_DECIMALES);
		map.put(ConstantesMedidasFrenos.FUERZA_DERECHA_EJE_TRES, DF_SIN_DECIMALES);
		map.put(ConstantesMedidasFrenos.FUERZA_DERECHA_EJE_CUATRO, DF_SIN_DECIMALES);
		map.put(ConstantesMedidasFrenos.FUERZA_DERECHA_EJE_CINCO, DF_SIN_DECIMALES);
		map.put(ConstantesMedidasFrenos.FUERZA_DERECHA_EJE_SEIS, DF_SIN_DECIMALES);
		map.put(ConstantesMedidasFrenos.PESO_DERECHO_EJE_UNO, DF_SIN_DECIMALES);
		map.put(ConstantesMedidasFrenos.PESO_DERECHO_EJE_DOS, DF_SIN_DECIMALES);
		map.put(ConstantesMedidasFrenos.PESO_DERECHO_EJE_TRES, DF_SIN_DECIMALES);
		map.put(ConstantesMedidasFrenos.PESO_DERECHO_EJE_CUATRO, DF_SIN_DECIMALES);
		map.put(ConstantesMedidasFrenos.PESO_DERECHO_EJE_CINCO, DF_SIN_DECIMALES);
		map.put(ConstantesMedidasFrenos.PESO_DERECHO_EJE_SEIS, DF_SIN_DECIMALES);
		map.put(ConstantesMedidasFrenos.PESO_IZQUIERDO_EJE_UNO, DF_SIN_DECIMALES);
		map.put(ConstantesMedidasFrenos.PESO_IZQUIERDO_EJE_DOS, DF_SIN_DECIMALES);
		map.put(ConstantesMedidasFrenos.PESO_IZQUIERDO_EJE_TRES, DF_SIN_DECIMALES);
		map.put(ConstantesMedidasFrenos.PESO_IZQUIERDO_EJE_CUATRO, DF_SIN_DECIMALES);
		map.put(ConstantesMedidasFrenos.PESO_IZQUIERDO_EJE_CINCO, DF_SIN_DECIMALES);
		map.put(ConstantesMedidasFrenos.PESO_IZQUIERDO_EJE_SEIS, DF_SIN_DECIMALES);
		//campos nueva resolucion
		map.put(ConstantesMedidasFrenos.PESO_DERECHO_TOTAL,DF_SIN_DECIMALES);
		map.put(ConstantesMedidasFrenos.PESO_IZQUIERDO_TOTAL,DF_SIN_DECIMALES);
		
		map.put(ConstantesMedidasFrenos.FUERZA_TOTAL_FRENO_MANO_DERECHA,DF_SIN_DECIMALES);
		map.put(ConstantesMedidasFrenos.FUERZA_TOTAL_FRENO_MANO_IZQUIERDA,DF_SIN_DECIMALES);
		
		map.put(ConstantesMedidasAlineacion.DESVIACION_EJE_UNO, DF_UN_DECIMAL);
		map.put(ConstantesMedidasAlineacion.DESVIACION_EJE_DOS, DF_UN_DECIMAL);
		map.put(ConstantesMedidasAlineacion.DESVIACION_EJE_TRES, DF_UN_DECIMAL);
		map.put(ConstantesMedidasAlineacion.DESVIACION_EJE_CUATRO, DF_UN_DECIMAL);
		map.put(ConstantesMedidasAlineacion.DESVIACION_EJE_CINCO, DF_UN_DECIMAL);
		map.put(ConstantesMedidasAlineacion.DESVIACION_EJE_SEIS, DF_UN_DECIMAL);
		map.put(ConstantesMedidasSuspension.ADHERENCIA_IZQUIERDA_EJE_UNO, DF_UN_DECIMAL);
		map.put(ConstantesMedidasSuspension.ADHERENCIA_IZQUIERDA_EJE_DOS, DF_UN_DECIMAL);
		map.put(ConstantesMedidasSuspension.ADHERENCIA_DERECHA_EJE_UNO, DF_UN_DECIMAL);
		map.put(ConstantesMedidasSuspension.ADHERENCIA_DERECHA_EJE_DOS, DF_UN_DECIMAL);

	}

	public ResultadoFasDTO generarResultadoFAS(List<Medida> medidas) {
		ResultadoFasDTO resFas = new ResultadoFasDTO();

		resFas.setEficaciaTotal(obtenerRepresentacionMedida(medidas, ConstantesMedidasFrenos.EFICACIA_FRENADO_TOTAL));
		resFas.setEficaciaAuxiliar(
				obtenerRepresentacionMedida(medidas, ConstantesMedidasFrenos.EFICACIA_FRENADO_AUXILIAR));

		resFas.setDesequilibrioEje1(
				obtenerRepresentacionMedida(medidas, ConstantesMedidasFrenos.DESEQUILIBRIO_EJE_UNO));
		resFas.setDesequilibrioEje2(
				obtenerRepresentacionMedida(medidas, ConstantesMedidasFrenos.DESEQUILIBRIO_EJE_DOS));
		resFas.setDesequilibrioEje3(
				obtenerRepresentacionMedida(medidas, ConstantesMedidasFrenos.DESEQUILIBRIO_EJE_TRES));
		resFas.setDesequilibrioEje4(
				obtenerRepresentacionMedida(medidas, ConstantesMedidasFrenos.DESEQUILIBRIO_EJE_CUATRO));
		resFas.setDesequilibrioEje5(
				obtenerRepresentacionMedida(medidas, ConstantesMedidasFrenos.DESEQUILIBRIO_EJE_CINCO));
		resFas.setDesequilibrioEje6(
				obtenerRepresentacionMedida(medidas, ConstantesMedidasFrenos.DESEQUILIBRIO_EJE_SEIS));

		resFas.setFuerzaFrenadoIzqEje1(
				obtenerRepresentacionMedida(medidas, ConstantesMedidasFrenos.FUERZA_IZQUIERDA_EJE_UNO));
		resFas.setFuerzaFrenadoIzqEje2(
				obtenerRepresentacionMedida(medidas, ConstantesMedidasFrenos.FUERZA_IZQUIERDA_EJE_DOS));
		resFas.setFuerzaFrenadoIzqEje3(
				obtenerRepresentacionMedida(medidas, ConstantesMedidasFrenos.FUERZA_IZQUIERDA_EJE_TRES));
		resFas.setFuerzaFrenadoIzqEje4(
				obtenerRepresentacionMedida(medidas, ConstantesMedidasFrenos.FUERZA_IZQUIERDA_EJE_CUATRO));
		resFas.setFuerzaFrenadoIzqEje5(
				obtenerRepresentacionMedida(medidas, ConstantesMedidasFrenos.FUERZA_IZQUIERDA_EJE_CINCO));
		resFas.setFuerzaFrenadoIzqEje6(
				obtenerRepresentacionMedida(medidas, ConstantesMedidasFrenos.FUERZA_IZQUIERDA_EJE_SEIS));

		resFas.setFuerzaFrenadoDerEje1(
				obtenerRepresentacionMedida(medidas, ConstantesMedidasFrenos.FUERZA_DERECHA_EJE_UNO));
		resFas.setFuerzaFrenadoDerEje2(
				obtenerRepresentacionMedida(medidas, ConstantesMedidasFrenos.FUERZA_DERECHA_EJE_DOS));
		resFas.setFuerzaFrenadoDerEje3(
				obtenerRepresentacionMedida(medidas, ConstantesMedidasFrenos.FUERZA_DERECHA_EJE_TRES));
		resFas.setFuerzaFrenadoDerEje4(
				obtenerRepresentacionMedida(medidas, ConstantesMedidasFrenos.FUERZA_DERECHA_EJE_CUATRO));
		resFas.setFuerzaFrenadoDerEje5(
				obtenerRepresentacionMedida(medidas, ConstantesMedidasFrenos.FUERZA_DERECHA_EJE_CINCO));
		resFas.setFuerzaFrenadoDerEje6(
				obtenerRepresentacionMedida(medidas, ConstantesMedidasFrenos.FUERZA_DERECHA_EJE_SEIS));

		resFas.setPesoDerEje1(obtenerRepresentacionMedida(medidas, ConstantesMedidasFrenos.PESO_DERECHO_EJE_UNO));
		resFas.setPesoDerEje2(obtenerRepresentacionMedida(medidas, ConstantesMedidasFrenos.PESO_DERECHO_EJE_DOS));
		resFas.setPesoDerEje3(obtenerRepresentacionMedida(medidas, ConstantesMedidasFrenos.PESO_DERECHO_EJE_TRES));
		resFas.setPesoDerEje4(obtenerRepresentacionMedida(medidas, ConstantesMedidasFrenos.PESO_DERECHO_EJE_CUATRO));
		resFas.setPesoDerEje5(obtenerRepresentacionMedida(medidas, ConstantesMedidasFrenos.PESO_DERECHO_EJE_CINCO));
		resFas.setPesoDerEje6(obtenerRepresentacionMedida(medidas, ConstantesMedidasFrenos.PESO_DERECHO_EJE_SEIS));

		resFas.setPesoIzqEje1(obtenerRepresentacionMedida(medidas, ConstantesMedidasFrenos.PESO_IZQUIERDO_EJE_UNO));
		resFas.setPesoIzqEje2(obtenerRepresentacionMedida(medidas, ConstantesMedidasFrenos.PESO_IZQUIERDO_EJE_DOS));
		resFas.setPesoIzqEje3(obtenerRepresentacionMedida(medidas, ConstantesMedidasFrenos.PESO_IZQUIERDO_EJE_TRES));
		resFas.setPesoIzqEje4(obtenerRepresentacionMedida(medidas, ConstantesMedidasFrenos.PESO_IZQUIERDO_EJE_CUATRO));
		resFas.setPesoIzqEje5(obtenerRepresentacionMedida(medidas, ConstantesMedidasFrenos.PESO_IZQUIERDO_EJE_CINCO));
		resFas.setPesoIzqEje6(obtenerRepresentacionMedida(medidas, ConstantesMedidasFrenos.PESO_IZQUIERDO_EJE_SEIS));

		resFas.setDesviacionLateralEje1(
				obtenerRepresentacionMedida(medidas, ConstantesMedidasAlineacion.DESVIACION_EJE_UNO));
		resFas.setDesviacionLateralEje2(
				obtenerRepresentacionMedida(medidas, ConstantesMedidasAlineacion.DESVIACION_EJE_DOS));
		resFas.setDesviacionLateralEje3(
				obtenerRepresentacionMedida(medidas, ConstantesMedidasAlineacion.DESVIACION_EJE_TRES));
		resFas.setDesviacionLateralEje4(
				obtenerRepresentacionMedida(medidas, ConstantesMedidasAlineacion.DESVIACION_EJE_CUATRO));
		resFas.setDesviacionLateralEje5(
				obtenerRepresentacionMedida(medidas, ConstantesMedidasAlineacion.DESVIACION_EJE_CINCO));
		resFas.setDesviacionLateralEje6(
				obtenerRepresentacionMedida(medidas, ConstantesMedidasAlineacion.DESVIACION_EJE_SEIS));

		resFas.setSuspDerEje1(
				obtenerRepresentacionMedida(medidas, ConstantesMedidasSuspension.ADHERENCIA_DERECHA_EJE_UNO));
		resFas.setSuspDerEje2(
				obtenerRepresentacionMedida(medidas, ConstantesMedidasSuspension.ADHERENCIA_DERECHA_EJE_DOS));
		resFas.setSuspIzqEje1(
				obtenerRepresentacionMedida(medidas, ConstantesMedidasSuspension.ADHERENCIA_IZQUIERDA_EJE_UNO));
		resFas.setSuspIzqEje2(
				obtenerRepresentacionMedida(medidas, ConstantesMedidasSuspension.ADHERENCIA_IZQUIERDA_EJE_DOS));
		
		resFas.setPesoDerechoTotal(obtenerRepresentacionMedida(medidas, ConstantesMedidasFrenos.PESO_DERECHO_TOTAL));
		resFas.setPesoIzquierdoTotal(obtenerRepresentacionMedida(medidas, ConstantesMedidasFrenos.PESO_IZQUIERDO_TOTAL));
		
		resFas.setFuerzaDerechaAuxiliar( obtenerRepresentacionMedida ( medidas, ConstantesMedidasFrenos.FUERZA_TOTAL_FRENO_MANO_DERECHA) ); 
		resFas.setFuerzaIzquierdaAuxiliar( obtenerRepresentacionMedida (medidas, ConstantesMedidasFrenos.FUERZA_TOTAL_FRENO_MANO_IZQUIERDA));
		return resFas;
	}

	public String obtenerRepresentacionMedida(List<Medida> medidas, Integer tipoMedidaId) {

		OptionalDouble optMedida = utilInformesMedidas.obtenerMedidaPorCodigo(medidas.stream(), tipoMedidaId);
		String retorno;
		if (optMedida.isPresent()) {
			df.applyPattern(map.get(tipoMedidaId));
			retorno = df.format(optMedida.getAsDouble());
		} else {
			retorno = "";
		}
		return retorno;
	}

	
	public void setPuntoComoSeparadorDecimales(){
		DecimalFormatSymbols ds = DecimalFormatSymbols.getInstance();
		ds.setDecimalSeparator(DECIMAL_SEPARATOR_PUNTO);
		df.setDecimalFormatSymbols(ds);
	}
	
	/**
	 * Objeto dto para informes de
	 * entidades ambientales con informacion 
	 * adicional
	 * @param medidas
	 * @param defectos
	 * @return
	 */
	public ResultadoOttoInformeDTO generarResultadoOttoInforme(List<Medida> medidas,List<Defecto> defectos) {
		
		utilInformesMedidas.ordenarListaMedidas(medidas);
		
		ResultadoOttoInformeDTO resultado = new ResultadoOttoInformeDTO();
		OptionalDouble temperaturaAmbiente = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,ConstantesMedidasGases.CODIGO_MEDIDA_TEMPERATURA_AMBIENTE);
		df.applyPattern(DF_SIN_DECIMALES);
		
		String vlrTemperaturaAmbiente = temperaturaAmbiente.isPresent() ? df.format( temperaturaAmbiente.getAsDouble() ) : "";
		resultado.setTemperaturaAmbiente(vlrTemperaturaAmbiente);
		
		OptionalDouble humedadRelativa = utilInformesMedidas.obtenerMedidaPorCodigo(medidas, ConstantesMedidasGases.CODIGO_MEDIDA_HUMEDAD_RELATIVA);
		String vlrHumedadRelativa = humedadRelativa.isPresent() ? df.format( humedadRelativa.getAsDouble() ) : "";
		resultado.setHumedadRelativa(vlrHumedadRelativa);
		
		resultado.setIncumplimientoNivelesDeEmision( presenciaDefecto(defectos.stream(), ConstantesCodigosDefectoGases.CODIGO_INCUMPLIMIENTO_NIVELES_EMISION) );
		resultado.setFallaFiltroAire( presenciaDefecto(defectos.stream(),ConstantesCodigosDefectoGases.CODIGO_FALLA_SISTEMA_AD_AIRE_OTTO));
		resultado.setPresenciaAccesoriosImpiden( presenciaDefecto(defectos.stream(),ConstantesCodigosDefectoGases.CODIGO_ACCESORIOS_IMPIDEN_OTTO));
		
		return resultado;
		
	}
	
	public ResultadoOttoMotocicletasInformeDTO generarResultadoOttoMotocicletasInforme(List<Medida> medidas,List<Defecto> defectos) {
		
		ResultadoOttoMotocicletasInformeDTO resultado = new ResultadoOttoMotocicletasInformeDTO();
		OptionalDouble temperaturaAmbiente = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,ConstantesMedidasGases.CODIGO_MEDIDA_TEMPERATURA_AMBIENTE);
		df.applyPattern(DF_UN_DECIMAL);
		
		String vlrTemperaturaAmbiente = temperaturaAmbiente.isPresent() ? df.format( temperaturaAmbiente.getAsDouble() ) : "";
		resultado.setTemperaturaAmbiente(vlrTemperaturaAmbiente);
		
		OptionalDouble humedadRelativa = utilInformesMedidas.obtenerMedidaPorCodigo(medidas, ConstantesMedidasGases.CODIGO_MEDIDA_HUMEDAD_RELATIVA);
		String vlrHumedadRelativa = humedadRelativa.isPresent() ? df.format( humedadRelativa.getAsDouble() ) : "";
		resultado.setHumedadRelativa(vlrHumedadRelativa);		
		
		return resultado;
		
	}
	
	
	public ResultadoOttoVehiculosDTO generarResultadoOtto(List<Medida> medidas,List<Defecto> defectos) {

		ResultadoOttoVehiculosDTO resultado = new ResultadoOttoVehiculosDTO();
		
		utilInformesMedidas.ordenarListaMedidas(medidas);

		OptionalDouble temperaturaRalenti = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_MEDIDA_TEMP_RALENTI);
		df.applyPattern(DF_SIN_DECIMALES);
		String vlrTempRalenti = temperaturaRalenti.isPresent() ? df.format(temperaturaRalenti.getAsDouble()) : "";
		// 3 medida temperatura ralenti
		resultado.setTempRalenti(vlrTempRalenti);

		OptionalDouble rpmRalenti = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_MEDIDA_RPM_RALENTI);
		// 4 rpm ralenti
		String vlrRpmRalenti = rpmRalenti.isPresent() ? df.format(rpmRalenti.getAsDouble()) : "";
		resultado.setRpmRalenti(vlrRpmRalenti);

		// 5 hc ralenti
		OptionalDouble hcRalenti = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_MEDIDA_HC_RALENTI);
		String vlrHcRalenti = hcRalenti.isPresent() ? df.format(hcRalenti.getAsDouble()) : "";
		resultado.setHcRalenti(vlrHcRalenti);

		// 6 co Ralenti
		df.applyPattern(DF_DOS_DECIMALES);
		OptionalDouble coRalenti = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_MEDIDA_CO_RALENTI);
		String vlrCoRalenti = coRalenti.isPresent() ? df.format(coRalenti.getAsDouble()) : "";
		resultado.setCoRalenti(vlrCoRalenti);

		// 7 co2 Ralenti
		df.applyPattern(DF_UN_DECIMAL);
		OptionalDouble co2Ralenti = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_MEDIDA_CO2_RALENTI);
		String vlrCo2Ralenti = co2Ralenti.isPresent() ? df.format(co2Ralenti.getAsDouble()) : "";
		resultado.setCo2Ralenti(vlrCo2Ralenti);

		// 8 oxigeno ralenti
		df.applyPattern(DF_DOS_DECIMALES);
		OptionalDouble o2Ralenti = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_MEDIDA_O2_RALENTI);
		String vlrO2Ralenti = o2Ralenti.isPresent() ? df.format(o2Ralenti.getAsDouble()) : "";
		resultado.setO2Ralenti(vlrO2Ralenti);

		// 9 temperatura crucero
		df.applyPattern(DF_SIN_DECIMALES);
		OptionalDouble temperaturaCrucero = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_MEDIDA_TEMP_CRUCERO);
		String vlrTemperaturaCrucero = temperaturaCrucero.isPresent() ? df.format(temperaturaCrucero.getAsDouble())
				: "";
		resultado.setTempCrucero(vlrTemperaturaCrucero);

		// 10 rpm crucero
		df.applyPattern(DF_SIN_DECIMALES);
		OptionalDouble rpmCrucero = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_MEDIDA_RPM_CRUCERO);
		String vlrRpmCrucero = rpmCrucero.isPresent() ? df.format(rpmCrucero.getAsDouble()) : "";
		resultado.setRpmCrucero(vlrRpmCrucero);

		// 11 hc crucero
		df.applyPattern(DF_SIN_DECIMALES);
		OptionalDouble hcCrucero = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_MEDIDA_HC_CRUCERO);
		String vlrHcCrucero = hcCrucero.isPresent() ? df.format(hcCrucero.getAsDouble()) : "";
		resultado.setHcCrucero(vlrHcCrucero);

		// 12 co crucero
		df.applyPattern(DF_DOS_DECIMALES);
		OptionalDouble coCrucero = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_MEDIDA_CO_CRUCERO);
		String valorCoCrucero = coCrucero.isPresent() ? df.format(coCrucero.getAsDouble()) : "";
		resultado.setCoCrucero(valorCoCrucero);

		// 13 co2 crucero
		df.applyPattern(DF_UN_DECIMAL);
		OptionalDouble co2Crucero = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_MEDIDA_CO2_CRUCERO);
		String vlrCo2Crucero = co2Crucero.isPresent() ? df.format(co2Crucero.getAsDouble()) : "";
		resultado.setCo2Crucero(vlrCo2Crucero);

		// 14 o2 crucero
		df.applyPattern(DF_DOS_DECIMALES);
		OptionalDouble o2Crucero = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_MEDIDA_O2_CRUCERO);
		String vlrO2Crucero = o2Crucero.isPresent() ? df.format(o2Crucero.getAsDouble()) : "";

		resultado.setO2Crucero(vlrO2Crucero);
		
		//defectos
		resultado.setFugasTuboEscape(presenciaDefecto(defectos.stream(), ConstantesCodigosDefectoGases.CODIGO_FUGAS_TUBO_SILENCIADOR));
		resultado.setFugasSilenciador(FALSE);//se indica con el primero 
		resultado.setTapaCombustible(presenciaDefecto(defectos.stream(), ConstantesCodigosDefectoGases.CODIGO_AUSENCIA_TAPA_COMBUSTIBLE_OTTO));
		resultado.setTapaAceite(presenciaDefecto(defectos.stream(),ConstantesCodigosDefectoGases.CODIGO_AUSENCIA_TAPA_ACEITE_OTTO));
		resultado.setSalidasAdicionales(presenciaDefecto(defectos.stream(),ConstantesCodigosDefectoGases.CODIGO_SALIDAS_ADICIONALES_OTTO));
		resultado.setPresenciaHumos(presenciaDefecto(defectos.stream(),ConstantesCodigosDefectoGases.CODIGO_HUMO_NEGRO_AZUL));
		resultado.setRevolucionesFueraRango(presenciaDefecto(defectos.stream(),ConstantesCodigosDefectoGases.CODIGO_REVOLUCIONES_FUERA_RANGO));
		resultado.setFallaSistemaRefrigeracion(presenciaDefecto(defectos.stream(),ConstantesCodigosDefectoGases.CODIGO_INCORRECTA_OP_REFRIGERACION_OTTO));
		resultado.setDesconexionPCV(presenciaDefecto(defectos.stream(),ConstantesCodigosDefectoGases.CODIGO_DESCONEXION_PCV));

		return resultado;

	}
	
	
	/**
	 * Se genera otro objeto DTO para informe de entidad ambiental
	 * con informacion diferente a la informacion que se reporta a sicov
	 * 
	 * @param medidas
	 * @param defectos
	 * @return
	 */
	public ResultadoDieselDTOInformes generarResultadoDieselDTOInformes(List<Medida> medidas,List<Defecto> defectos) {
		ResultadoDieselDTOInformes resultado = new ResultadoDieselDTOInformes();
		
		OptionalDouble rpmRalenti = utilInformesMedidas.obtenerMedidaPorCodigo(medidas, ConstantesMedidasGases.CODIGO_VELOCIDAD_RALENTI_PROMEDIO);
		df.applyPattern(DF_SIN_DECIMALES);
		String vlrRpmRalenti = rpmRalenti.isPresent() ? df.format( rpmRalenti.getAsDouble() ): "";
		resultado.setRpmRalenti(vlrRpmRalenti);
		
		OptionalDouble temperaturaFinal = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_DIESEL_TEMPERATURA_FINAL);
		df.applyPattern(DF_SIN_DECIMALES);
		String vlrTemperaturaFinal = temperaturaFinal.isPresent() ? df.format(temperaturaFinal.getAsDouble())
				: "";
		resultado.setTemperaturaFinal(vlrTemperaturaFinal);

		OptionalDouble rpmPrimerCiclo = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_REV_GOB_CICLO_PRELIMINAR);
		String valorRpmPrimerCiclo = rpmPrimerCiclo.isPresent() ? df.format(rpmPrimerCiclo.getAsDouble()) : "";
		resultado.setRpmGobernadaCiclo0(valorRpmPrimerCiclo);
		
		
		// ralenti
		
		OptionalDouble rpmRalentiCicloPreliminar = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_REV_RAL_CICLO_PRELIMINAR);
		String valorRpmRalentiCiclo0 = rpmRalentiCicloPreliminar.isPresent() ? df.format( rpmRalentiCicloPreliminar.getAsDouble() ) : "";
		resultado.setRpmRalentiCiclo0(valorRpmRalentiCiclo0);
		
		OptionalDouble rpmRalentiCicloUno = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_REV_RAL_PRIMER_CICLO);
		String valorRpmRalentiCicloUno = rpmRalentiCicloUno.isPresent() ? df.format( rpmRalentiCicloUno.getAsDouble() ) : "";
		resultado.setRpmRalentiCiclo1(valorRpmRalentiCicloUno);
		
		OptionalDouble rpmRalentiCicloDos = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_REV_RAL_SEGUNDO_CICLO);
		String valorRpmRalentiCicloDos = rpmRalentiCicloDos.isPresent() ? df.format( rpmRalentiCicloDos.getAsDouble() ) : "";
		resultado.setRpmRalentiCiclo2(valorRpmRalentiCicloDos);
		
		OptionalDouble rpmRalentiCicloTres = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_REV_RAL_TERCER_CICLO);
		String valorRpmRalentiCicloTres = rpmRalentiCicloTres.isPresent() ? df.format( rpmRalentiCicloTres.getAsDouble() ) : "";
		resultado.setRpmRalentiCiclo3(valorRpmRalentiCicloTres);
		
		//densidades de humo
		DecimalFormat dfDensidad = new DecimalFormat("0.000");
		OptionalDouble densidadHumoCicloPreliminar = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_DENSIDAD_HUMO_CICLO_PRELIMINAR);
		String valorDensidadHumoCicloPreliminar = densidadHumoCicloPreliminar.isPresent() ? dfDensidad.format( densidadHumoCicloPreliminar.getAsDouble()) : "";
		resultado.setDensidadHumoCiclo0(valorDensidadHumoCicloPreliminar);
		
		OptionalDouble densidadHumoCicloUno = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_DENSIDAD_HUMO_CICLO_UNO);
		String valorDensidadHumoCicloUno = densidadHumoCicloUno.isPresent() ? dfDensidad.format( densidadHumoCicloUno.getAsDouble()) : "";
		resultado.setDensidadHumoCiclo1(valorDensidadHumoCicloUno);
		
		OptionalDouble densidadHumoCicloDos = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_DENSIDAD_HUMO_CICLO_DOS);
		String valorDensidadHumoCicloDos = densidadHumoCicloDos.isPresent() ? dfDensidad.format( densidadHumoCicloDos.getAsDouble()) : "";
		resultado.setDensidadHumoCiclo2(valorDensidadHumoCicloDos);
		
		OptionalDouble densidadHumoCicloTres = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_DENSIDAD_HUMO_CICLO_TRES);
		String valorDensidadHumoCicloTres = densidadHumoCicloTres.isPresent() ? dfDensidad.format( densidadHumoCicloTres.getAsDouble()) : "";
		resultado.setDensidadHumoCiclo3(valorDensidadHumoCicloTres);
		
		OptionalDouble densidadHumoResultado = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_RESULTADO_DENSIDAD_HUMO);
		String valorDensidadHumoResultado = densidadHumoResultado.isPresent() ? dfDensidad.format( densidadHumoResultado.getAsDouble()) : "";
		resultado.setDensidadHumoFinal(valorDensidadHumoResultado);
		
		OptionalDouble rpmSegundoCiclo = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_REV_GOB_PRIMER_CICLO);
		df.applyPattern(DF_SIN_DECIMALES);
		String valorRpmSegundoCiclo = rpmSegundoCiclo.isPresent() ? df.format(rpmSegundoCiclo.getAsDouble()) : "";
		resultado.setRpmGobernadaCiclo1(valorRpmSegundoCiclo);

		OptionalDouble rpmTercerCiclo = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_REV_GOB_SEGUNDO_CICLO);
		String valorRpmTercerCiclo = rpmTercerCiclo.isPresent()
				? df.format(rpmTercerCiclo.getAsDouble()) : "";
		resultado.setRpmGobernadaCiclo2(valorRpmTercerCiclo);

		OptionalDouble rpmCuartoCiclo = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_REV_GOB_TERCER_CICLO);
		String rpmCuartoCicloStr = rpmCuartoCiclo.isPresent() ? df.format(rpmCuartoCiclo.getAsDouble())
				: "";
		resultado.setRpmGobernadaCiclo3(rpmCuartoCicloStr);
		
		
		
		df.applyPattern(DF_UN_DECIMAL);
		OptionalDouble temperaturaAmbiente  = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_MEDIDA_TEMPERATURA_AMBIENTE);
		
		String valorTemperaturaAmbienteStr = temperaturaAmbiente.isPresent() ? df.format(temperaturaAmbiente.getAsDouble()) : "";
		resultado.setTemperaturaAmbiental(valorTemperaturaAmbienteStr);
		
		
		OptionalDouble humedadRelativa = utilInformesMedidas.obtenerMedidaPorCodigo(medidas, ConstantesMedidasGases.CODIGO_MEDIDA_HUMEDAD_RELATIVA);
		String valorHumedadRelativa = humedadRelativa.isPresent() ? df.format( humedadRelativa.getAsDouble() ) : "";
		resultado.setHumedadRelativa(valorHumedadRelativa);
		
		
		//defectos
		resultado.setVelocidadNoAlcanzada5Seg(presenciaDefecto(defectos.stream(),ConstantesCodigosDefectoGases.CODIGO_NO_ALCANZA_GOBERNADA));
		resultado.setAusenciaDeFiltroAire(presenciaDefecto(defectos.stream(),ConstantesCodigosDefectoGases.CODIGO_AUSENCIA_FILTRO_AIRE_DIESEL));
		resultado.setAusenciaTaponCombustible(presenciaDefecto(defectos.stream(),ConstantesCodigosDefectoGases.CODIGO_AUSENCIA_TAPA_COMBUSTIBLE_DIESEL));
		resultado.setAusenciaTaponesDeAceite(presenciaDefecto(defectos.stream(),ConstantesCodigosDefectoGases.CODIGO_AUSENCIA_TAPA_ACEITE_DIESEL));
		resultado.setDiferenciasAritmeticas(presenciaDefecto(defectos.stream(),ConstantesCodigosDefectoGases.CODIGO_DIFERENCIA_ARITMETICA_CICLOS));
		resultado.setFallaSubitaMotor(presenciaDefecto(defectos.stream(),ConstantesCodigosDefectoGases.CODIGO_FALLA_SUBITA_VEHICULO));
		resultado.setFugasTuvoUnionesMultipleSilenciador(presenciaDefecto(defectos.stream(),ConstantesCodigosDefectoGases.CODIGO_FUGAS_TUBO_ESCAPE_DIESEL));
		resultado.setIncorrectaOperacionSistemaRefrigeracion((presenciaDefecto(defectos.stream(),ConstantesCodigosDefectoGases.CODIGO_INDICIO_DANIO_MOTOR) ) );
		resultado.setIncumplimientoNiveles(presenciaDefecto(defectos.stream(),ConstantesCodigosDefectoGases.CODIGO_INCUMPLIMIENTO_NIVELES_EMISION));
		resultado.setInstalacionAccesoriosODeformaciones((presenciaDefecto(defectos.stream(),ConstantesCodigosDefectoGases.CODIGO_ACCESORIOS_DEFORMACIONES_IMPIDEN)));
		resultado.setInstalacionDispositivosAlteranRpms((presenciaDefecto(defectos.stream(),ConstantesCodigosDefectoGases.CODIGO_OP_INADECUADA_DISPOSITIVOS_IMPIDEN_ACELERAR_DIESEL)));
		resultado.setMalFuncionamientoMotor( presenciaDefecto(defectos.stream(),ConstantesCodigosDefectoGases.CODIGO_INDICIO_DANIO_MOTOR));
		resultado.setGobernadorNoLimitaRevoluciones( presenciaDefecto( defectos.stream(),ConstantesCodigosDefectoGases.CODIGO_MAL_FUNCIONAMIENTO_CONTROL_VELOCIDAD));
		resultado.setInestabilidadRevoluciones( presenciaDefecto( defectos.stream(),ConstantesCodigosDefectoGases.CODIGO_INESTABILIDAD_RPM_CICLOS));
		resultado.setDiferenciaTemperaturas( presenciaDefecto( defectos.stream(), ConstantesCodigosDefectoGases.CODIGO_DIFERENCIA_TEMPERATURA_MOTOR));
		
		return resultado;
		
	}

	public ResultadoDieselDTO generarResultadoDieselDTO(List<Medida> medidas,List<Defecto> defectos) {
		ResultadoDieselDTO resultado = new ResultadoDieselDTO();
		String codigosMedidas = medidas.stream().map(medida -> String.valueOf(medida.getTipoMedida().getTipoMedidaId()))
				.collect(Collectors.joining(";"));
		System.out.println(codigosMedidas);
		utilInformesMedidas.ordenarListaMedidas(medidas);
		OptionalDouble temperaturaInicial = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_DIESEL_TEMPERATURA_INICIAL);
		df.applyPattern(DF_SIN_DECIMALES);
		String vlrTemperaturaInicial = temperaturaInicial.isPresent() ? df.format(temperaturaInicial.getAsDouble())
				: "";
		resultado.setTemperaturaInicial(vlrTemperaturaInicial);
		
		OptionalDouble opt = medidas.stream().filter( m-> (				
				m.getTipoMedida().getTipoMedidaId() == ConstantesMedidasGases.CODIGO_REV_RAL_CICLO_PRELIMINAR || 
				m.getTipoMedida().getTipoMedidaId() == ConstantesMedidasGases.CODIGO_REV_RAL_PRIMER_CICLO ||
				m.getTipoMedida().getTipoMedidaId() == ConstantesMedidasGases.CODIGO_REV_RAL_SEGUNDO_CICLO ||
				m.getTipoMedida().getTipoMedidaId() == ConstantesMedidasGases.CODIGO_REV_RAL_SEGUNDO_CICLO ) ).mapToDouble( m->m.getValor()).average();
		if(opt.isPresent()) {
			
		}
				

		OptionalDouble rpmPromedio = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_VELOCIDAD_GOBERNADA_PROMEDIO);
		String valorRpmPromedio = rpmPromedio.isPresent() ? df.format(rpmPromedio.getAsDouble()) : "";
		resultado.setRpmGobernada(valorRpmPromedio);
		// gobernada

		OptionalDouble opacidadPrimerCiclo = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_OPACIDAD_CICLO_PRELIMINAR);
		df.applyPattern(DF_DOS_DECIMALES);
		String valorOpacidadPrimerCiclo = opacidadPrimerCiclo.isPresent() ? df.format(opacidadPrimerCiclo.getAsDouble()) : "";
		resultado.setAceleracionCero(valorOpacidadPrimerCiclo);

		OptionalDouble opacidadSegundoCiclo = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_OPACIDAD_PRIMER_CICLO);
		String valorOpacidadSegundoCiclo = opacidadSegundoCiclo.isPresent()
				? df.format(opacidadSegundoCiclo.getAsDouble()) : "";
		resultado.setAceleracionUno(valorOpacidadSegundoCiclo);

		OptionalDouble opacidadTercerCiclo = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_OPACIDAD_SEGUNDO_CICLO);
		String valorOpacidadTercerCiclo = opacidadTercerCiclo.isPresent() ? df.format(opacidadTercerCiclo.getAsDouble())
				: "";
		resultado.setAceleracionDos(valorOpacidadTercerCiclo);
		// aceleracion
		// 2

		OptionalDouble opacidadCuartoCiclo = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_OPACIDAD_TERCER_CICLO);
		String valorOpacidadUltimoCiclo = opacidadCuartoCiclo.isPresent() ? df.format(opacidadCuartoCiclo.getAsDouble())
				: "";
		// 8
		resultado.setAceleracionTres(valorOpacidadUltimoCiclo); // aceleracion
		// 3

		OptionalDouble opacidadResultado = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_RESULTADO_OPACIDAD);
		String valorResultadoOpa = opacidadResultado.isPresent() ? df.format(opacidadResultado.getAsDouble()) : "";
		resultado.setValorFinal(valorResultadoOpa);
		
		//defectos
		resultado.setFugasTuboEscape(presenciaDefecto(defectos.stream(),ConstantesCodigosDefectoGases.CODIGO_FUGAS_TUBO_ESCAPE_DIESEL));
		resultado.setFugasSilenciador(presenciaDefecto(defectos.stream(),ConstantesCodigosDefectoGases.CODIGO_FUGAS_SILENCIADOR_DIESEL));
		resultado.setTapaCombustible(presenciaDefecto(defectos.stream(),ConstantesCodigosDefectoGases.CODIGO_AUSENCIA_TAPA_COMBUSTIBLE_DIESEL));
		resultado.setTapaAceite(presenciaDefecto(defectos.stream(),ConstantesCodigosDefectoGases.CODIGO_AUSENCIA_TAPA_ACEITE_DIESEL));
		resultado.setSistemaMuestreo(presenciaDefecto(defectos.stream(),ConstantesCodigosDefectoGases.CODIGO_ACCESORIOS_DEFORMACIONES_IMPIDEN));
		resultado.setSalidasAdicionales(presenciaDefecto(defectos.stream(),ConstantesCodigosDefectoGases.CODIGO_SALIDAS_ADICIONALES_DIESEL));
		resultado.setFiltroAire(presenciaDefecto(defectos.stream(),ConstantesCodigosDefectoGases.CODIGO_AUSENCIA_FILTRO_AIRE_DIESEL));
		resultado.setSistemaRefrigeracion(presenciaDefecto(defectos.stream(),ConstantesCodigosDefectoGases.CODIGO_FALLA_SISTEMA_REFRIGERACION_DIESEL));
		resultado.setRevolucionesFueraRango(presenciaDefecto(defectos.stream(),ConstantesCodigosDefectoGases.CODIGO_REVOLUCIONES_FUERA_RANGO));

		return resultado;
	}
	
	
	
	
	
	public ResultadoOttoMotocicletasDTO generarResultadoOttoMotos(List<Medida> medidas,List<Defecto>defectos,Boolean dosTiempos){
		
		ResultadoOttoMotocicletasDTO resultado = new ResultadoOttoMotocicletasDTO();
		utilInformesMedidas.ordenarListaMedidas(medidas);
		
		Integer codigoTemperaturaRalenti = dosTiempos ? ConstantesMedidasGases.CODIGO_MEDIDA_TEMP_RALENTI_2T
				: ConstantesMedidasGases.CODIGO_MEDIDA_TEMP_RALENTI;
		OptionalDouble temperaturaRalenti = utilInformesMedidas.obtenerMedidaPorCodigo(medidas, codigoTemperaturaRalenti);
		df.applyPattern(DF_SIN_DECIMALES);
		String vlrTemperaturaRalenti = temperaturaRalenti.isPresent() ? df.format(temperaturaRalenti.getAsDouble()):"";
		resultado.setTempRalenti(vlrTemperaturaRalenti);
		
		// 4 rpm Ralenti
		Integer codigoRpmRalenti = dosTiempos ? ConstantesMedidasGases.CODIGO_MEDIDA_RPM_RALENTI_2T
				: ConstantesMedidasGases.CODIGO_MEDIDA_RPM_RALENTI;
		OptionalDouble rpmRalenti = utilInformesMedidas.obtenerMedidaPorCodigo(medidas, codigoRpmRalenti);
		String vlrRpmRalenti = rpmRalenti.isPresent()?df.format(rpmRalenti.getAsDouble()):"";
		resultado.setRpmRalenti(vlrRpmRalenti);

		// 5 hc ralenti
		Integer codigoHcRalenti = dosTiempos ? ConstantesMedidasGases.CODIGO_MEDIDA_HC_RALENTI_2T
				: ConstantesMedidasGases.CODIGO_MEDIDA_HC_RALENTI;
		OptionalDouble hcRalenti = utilInformesMedidas.obtenerMedidaPorCodigo(medidas, codigoHcRalenti);
		String vlrHcRalenti = hcRalenti.isPresent() ? df.format(hcRalenti.getAsDouble()):"";
		resultado.setHcRalenti(vlrHcRalenti);

		// 6 co Ralenti
		df.applyPattern(DF_DOS_DECIMALES);
		Integer codigoCoRalenti = dosTiempos ? ConstantesMedidasGases.CODIGO_MEDIDA_CO_RALENTI_2T
				: ConstantesMedidasGases.CODIGO_MEDIDA_CO_RALENTI;
		OptionalDouble coRalenti = utilInformesMedidas.obtenerMedidaPorCodigo(medidas, codigoCoRalenti);
		String vlrCoRalenti = coRalenti.isPresent() ?  df.format(coRalenti.getAsDouble()):"";
		resultado.setCoRalenti(vlrCoRalenti);

		// 7 co2Ralenti
		df.applyPattern(DF_UN_DECIMAL);
		Integer codigoCo2Ralenti = dosTiempos ? ConstantesMedidasGases.CODIGO_MEDIDA_CO2_RALENTI_2T
				: ConstantesMedidasGases.CODIGO_MEDIDA_CO2_RALENTI;
		OptionalDouble co2Ralenti = utilInformesMedidas.obtenerMedidaPorCodigo(medidas, codigoCo2Ralenti);
		String vlrCo2Ralenti = co2Ralenti.isPresent()?df.format(co2Ralenti.getAsDouble()):"";
		resultado.setCo2Ralenti(vlrCo2Ralenti);
		// 8 o2 ralenti
		df.applyPattern(DF_DOS_DECIMALES);
		Integer codigoO2Ralenti = dosTiempos ? ConstantesMedidasGases.CODIGO_MEDIDA_O2_RALENTI_2T
				: ConstantesMedidasGases.CODIGO_MEDIDA_O2_RALENTI;
		OptionalDouble o2Ralenti = utilInformesMedidas.obtenerMedidaPorCodigo(medidas, codigoO2Ralenti);
		String vlrO2Ralenti = o2Ralenti.isPresent() ? df.format(o2Ralenti.getAsDouble()) : "";
		resultado.setO2Ralenti(vlrO2Ralenti);
		
		//Defectos
		utilDefectos.ordenarListaDefectos(defectos);
				
		
		
		
		//compatibilidad con pruebas anteriores codigo minnsterio errado
		Optional<Defecto> optionalDefectoRevoluciones1 = utilDefectos.obtenerDefectoPorCodigo( defectos.stream(),ConstantesCodigosDefectoGases.CODIGO_REVOLUCIONES_FUERA_RANGO );
		//codigo nuevo debido a separacion
		Optional<Defecto> optionalDefectoRevoluciones2 = utilDefectos.obtenerDefectoPorCodigo( defectos.stream(), ConstantesCodigosDefectoGases.REVOLUCIONES_INESTABLES_MOTOS);
		Boolean defectoRevoluciones = optionalDefectoRevoluciones1.isPresent() || optionalDefectoRevoluciones2.isPresent();
		String presenciaRevoluciones = defectoRevoluciones ? TRUE:FALSE;
		resultado.setRevolucionesFueraRango( presenciaRevoluciones);
		
		//Compatibilidad con pruebas anteriores codigo ministerio errado humo negro
		Optional<Defecto> optionalDefectoHumoNegro1 = utilDefectos.obtenerDefectoPorCodigo( defectos.stream(),ConstantesCodigosDefectoGases.CODIGO_HUMO_NEGRO_AZUL );
		Optional<Defecto> optionalDefectoHumoNegro2 = utilDefectos.obtenerDefectoPorCodigo( defectos.stream(), ConstantesCodigosDefectoGases.PRESENCIA_HUMO_NEGRO_AZUL_MOTOS);
		//codigo nuevo debido a separacion
		
		Boolean defectoHumoNegro = optionalDefectoHumoNegro1.isPresent() || optionalDefectoHumoNegro2.isPresent();
		String presenciaHumoNegro = defectoHumoNegro ? TRUE:FALSE;
		resultado.setPresenciaHumos(presenciaHumoNegro);
		
		resultado.setFugasTuboEscape(presenciaDefecto(defectos.stream(),ConstantesCodigosDefectoGases.CODIGO_FUGAS_TUBO_ESCAPE_MOTO));
		resultado.setFugasSilenciador(presenciaDefecto(defectos.stream(),ConstantesCodigosDefectoGases.CODIGO_FUGAS_TUBO_SILENCIADOR));//no hay codigo de motos
		resultado.setTapaCombustible(presenciaDefecto(defectos.stream(),ConstantesCodigosDefectoGases.CODIGO_AUSENCIA_TAPA_COMBUSTIBLE_OTTO));//no hay codigo para moto, se usa el de vehiculo
		resultado.setTapaAceite(presenciaDefecto(defectos.stream(),ConstantesCodigosDefectoGases.CODIGO_AUSENCIA_TAPA_ACEITE_MOTO));
		resultado.setSalidasAdicionales(presenciaDefecto(defectos.stream(),ConstantesCodigosDefectoGases.CODIGO_SALIDAS_ADICIONALES_MOTO));
		
		return resultado;

	}
	
	
	public ResultadoLucesDTO generarResultadoLucesMoto(List<Medida> medidas){
		ResultadoLucesDTO resultado = new ResultadoLucesDTO();
		df.applyPattern(DF_UN_DECIMAL);
		
		OptionalDouble intensidadBajaDerechaMoto = utilInformesMedidas.obtenerMedidaPorCodigo(medidas, ConstantesMedidasLuces.INTENSIDAD_BAJA_DERECHA_MOTOS);
		String vlrIntensidadBajaDerechaMoto = intensidadBajaDerechaMoto.isPresent() ? df.format(intensidadBajaDerechaMoto.getAsDouble()) : "";
		resultado.setDerBajaIntensidad(vlrIntensidadBajaDerechaMoto);
		
		OptionalDouble incBajaDerechaMoto = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,ConstantesMedidasLuces.INCLINACION_BAJA_DERECHA_MOTOS);
		String vlrIncBajaDerechaMoto = incBajaDerechaMoto.isPresent() ? df.format(incBajaDerechaMoto.getAsDouble()):"";
		resultado.setDerBajaInclinacion(vlrIncBajaDerechaMoto);
		
		return resultado;
	}
	
	
	public ResultadoLucesDTO generarResultadoLuces(List<Medida> medidas){
		ResultadoLucesDTO resultado = new ResultadoLucesDTO();
		df.applyPattern(DF_UN_DECIMAL);
		OptionalDouble intensidadBajaDerecha = utilInformesMedidas.obtenerMedidaPorCodigo(medidas, ConstantesMedidasLuces.INTENSIDAD_BAJA_DERECHA);
		String vlrIntensidadBajaDerecha = intensidadBajaDerecha.isPresent() ? df.format(intensidadBajaDerecha.getAsDouble()) : "";
		resultado.setDerBajaIntensidad(vlrIntensidadBajaDerecha);
		
		OptionalDouble intensidadBajaIzquierda = utilInformesMedidas.obtenerMedidaPorCodigo(medidas, ConstantesMedidasLuces.INTENSIDAD_BAJA_IZQUIERDA);
		String vlrIntensidadBajaIzquierda = intensidadBajaIzquierda.isPresent() ? df.format(intensidadBajaIzquierda.getAsDouble()) : "";
		resultado.setIzqBajaIntensidad(vlrIntensidadBajaIzquierda);
		
		OptionalDouble incBajaDerecha = utilInformesMedidas.obtenerMedidaPorCodigo(medidas, ConstantesMedidasLuces.INCLINACION_BAJA_DERECHA);
		String vlrIncBajaDerecha = incBajaDerecha.isPresent() ? df.format(incBajaDerecha.getAsDouble()):"";
		resultado.setDerBajaInclinacion(vlrIncBajaDerecha);
		
		
		OptionalDouble incBajaIzquierda = utilInformesMedidas.obtenerMedidaPorCodigo(medidas, ConstantesMedidasLuces.INCLINACION_BAJA_IZQUIERDA);
		String vlrIncBajaIzquierda = incBajaIzquierda.isPresent() ? df.format(incBajaIzquierda.getAsDouble()):"";
		resultado.setIzqBajaInclinacion(vlrIncBajaIzquierda);
		
		OptionalDouble sumatoria = utilInformesMedidas.obtenerMedidaPorCodigo(medidas, ConstantesMedidasLuces.SUMA_LUCES);
		String vlrSumatoria = sumatoria.isPresent() ? df.format(sumatoria.getAsDouble()):"";
		resultado.setSumatoriaIntensidad(vlrSumatoria);
		
		return resultado;		
	}
	
	public ResultadoSonometriaDTO generarResultadoSonometria(List<Medida> medidas){
		ResultadoSonometriaDTO resultado = new ResultadoSonometriaDTO();
		df.applyPattern(DF_UN_DECIMAL);
		OptionalDouble ruidoTubo = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,ConstantesMedidasSonometria.CODIGO_PRESION_SONORA);
		String vlrRuidoTubo = ruidoTubo.isPresent() ? df.format(ruidoTubo.getAsDouble()) :"";
		resultado.setValor(vlrRuidoTubo);
		return resultado;
	}
	
	public ResultadoSonometriaInformeDTO generarInformacionSonometria(List<Medida> medidas,List<Equipo> equipos) {
		ResultadoSonometriaInformeDTO resultado = new ResultadoSonometriaInformeDTO();
		
		OptionalDouble ruidoTubo = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,ConstantesMedidasSonometria.CODIGO_PRESION_SONORA);
		String vlrRuidoTubo = ruidoTubo.isPresent() ? df.format(ruidoTubo.getAsDouble()) :"";
		resultado.setValorMedicion(vlrRuidoTubo);
		
		Optional<Equipo> sonometroOpt = equipos.stream().filter( e->e.getTipoEquipo().getTipoEquipoId().equals( 6)).findAny();
		if( sonometroOpt.isPresent()) {
			Equipo sonometro = sonometroOpt.get();
			resultado.setMarcaMedidor(sonometro.getFabricante());
			resultado.setSerialMedidor( sonometro.getSerieEquipo() );
		} else {
			resultado.setMarcaMedidor("");
			resultado.setSerialMedidor("");
		}
		return resultado;
		
	}
	
	
	public ResultadoTaximetroDTO generarResultadoTaximetro(List<Medida> medidas,List<Defecto> defectos,Boolean aplicaTaximetro,String refLlanta){
		ResultadoTaximetroDTO resultado = new ResultadoTaximetroDTO();
		String aplicaStr = aplicaTaximetro ? TRUE:FALSE;
		resultado.setAplicaTaximetro(aplicaStr);
		
		Optional<Defecto> ausenciaTaximetroDetectada = utilDefectos.obtenerDefectoPorCodigo(defectos.stream(), ConstantesDefectosTaximetro.CODIGO_AUSENCIA_TAXIMETRO);
		String vlrAusenciaTaximetro = ausenciaTaximetroDetectada.isPresent()?TRUE:FALSE;
		resultado.setTieneTaximetro(vlrAusenciaTaximetro);
		
		Optional<Defecto> taximetroNoVisible = utilDefectos.obtenerDefectoPorCodigo(defectos.stream(),ConstantesDefectosTaximetro.CODIGO_TAXIMETRO_NO_VISIBLE);
		String vlrTaximetroNoVisible = taximetroNoVisible.isPresent() ? TRUE : FALSE; 
		resultado.setTaximetroVisible(vlrTaximetroNoVisible);
		
		df.applyPattern(DF_UN_DECIMAL);
		OptionalDouble errorDistancia = utilInformesMedidas.obtenerMedidaPorCodigo(medidas, ConstantesMedidasTaximetro.ID_MEDIDA_ERROR_DISTANCIA);
		String vlrErrorDistancia = errorDistancia.isPresent() ? df.format(errorDistancia.getAsDouble()):"";
		resultado.setErrorDistancia(vlrErrorDistancia);
		
		OptionalDouble errorTiempo = utilInformesMedidas.obtenerMedidaPorCodigo(medidas, ConstantesMedidasTaximetro.ID_MEDIDA_ERROR_TIEMPO);
		String vlrErrorTiempo = errorTiempo.isPresent() ? df.format(errorTiempo.getAsDouble()):"";
		resultado.setErrorTiempo(vlrErrorTiempo);
		
		resultado.setReferenciaLlanta(refLlanta);
		
		return resultado;		
	}
	
	
	private String presenciaDefecto(Stream<Defecto> defecto,Integer codigo){
		Optional<Defecto> optionalDefecto = utilDefectos.obtenerDefectoPorCodigo(defecto, codigo);
		String presencia = optionalDefecto.isPresent() ? TRUE:FALSE;
		return presencia;
	}

}
