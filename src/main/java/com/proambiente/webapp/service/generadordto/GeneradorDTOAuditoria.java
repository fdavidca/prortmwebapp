package com.proambiente.webapp.service.generadordto;

import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE1_DER;
import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE1_IZQ;
import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE2_DER_1;
import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE2_DER_2;
import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE2_IZQ_1;
import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE2_IZQ_2;
import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE3_DER_1;
import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE3_DER_2;
import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE3_IZQ_1;
import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE3_IZQ_2;
import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE4_DER_1;
import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE4_DER_2;
import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE4_IZQ_1;
import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE4_IZQ_2;
import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE5_DER_1;
import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE5_DER_2;
import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE5_IZQ_1;
import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE5_IZQ_2;
import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_REPUESTO_DER;
import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_REPUESTO_IZQ;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import com.proambiente.modelo.Defecto;
import com.proambiente.modelo.DefectoAsociado;
import com.proambiente.modelo.EstadoDefecto;
import com.proambiente.modelo.Medida;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.modelo.dto.ResultadoDieselDTO;
import com.proambiente.modelo.dto.ResultadoFasDTO;

import com.proambiente.modelo.dto.ResultadoOttoMotocicletasDTO;
import com.proambiente.modelo.dto.ResultadoOttoVehiculosDTO;
import com.proambiente.modelo.dto.ResultadoSonometriaDTO;
import com.proambiente.modelo.dto.ResultadoTaximetroDTO;


import com.proambiente.modelo.dto.auditoria.LogAuditoriaSuspension;
import com.proambiente.modelo.dto.auditoria.LogAuditoriaTaximetro;
import com.proambiente.modelo.dto.auditoria.SonometriaLog;
import com.proambiente.webapp.service.util.UtilTipoCombustible;
import com.proambiente.webapp.service.util.UtilTipoVehiculo;
import com.proambiente.webapp.util.dto.ResultadoDieselDTOInformes;
import com.proambiente.webapp.util.dto.ResultadoOttoInformeDTO;
import com.proambiente.webapp.util.dto.ResultadoPresionLlantasDTO;
import com.proambiente.webapp.util.dto.logsicov.GeneradorLogLucesSicov;
import com.proambiente.webapp.util.dto.logsicov.LogAlineacionSicovDTO;
import com.proambiente.webapp.util.dto.logsicov.LogFrenosSicovDTO;
import com.proambiente.webapp.util.dto.logsicov.LogGasesSicovDTO;
import com.proambiente.webapp.util.dto.logsicov.LogInspeccionSensorialSicovDTO;
import com.proambiente.webapp.util.dto.logsicov.LogLlantasSicovDTO;
import com.proambiente.webapp.util.dto.logsicov.LogLucesSicovDTO;


public class GeneradorDTOAuditoria {
	
	
	private static final Logger logger = Logger.getLogger(GeneradorDTOAuditoria.class.getName());
	
	GeneradorDTOResultados generadorDTOResultados = new GeneradorDTOResultados();

	private static String TABLA_AFECTADA = "prueba|medida";
	
	public SonometriaLog generarLogSonometria(Prueba prueba,List<Medida> medidas){
		
		ResultadoSonometriaDTO resultado = generadorDTOResultados.generarResultadoSonometria(medidas);
		SonometriaLog sLog = new SonometriaLog();
		sLog.setRuidoEscape(resultado.getValor());
		StringBuilder sb = crearIdRegistroIdPrueba(prueba);
		sb.append("medida ").append(generarIdMedidas(medidas));
		sLog.setIdRegistro(sb.toString());
		sLog.setTablaAfectada(TABLA_AFECTADA);
		return sLog;		
		
	}
	
	
	public String generarIdMedidas(List<Medida> medidas){
		return medidas.stream().map(m->String.valueOf(m.getMedidaId())).collect(Collectors.joining("_"));		
	}
	
	
	public LogGasesSicovDTO generarLogPruebaGases(Prueba prueba,List<Medida> medidas,Vehiculo vehiculo,List<Defecto> defectos){
		LogGasesSicovDTO logGases = new LogGasesSicovDTO();
		if(UtilTipoCombustible.isCombustibleDiesel(vehiculo.getTipoCombustible())){
			ResultadoDieselDTO resultado = generadorDTOResultados.generarResultadoDieselDTO(medidas, defectos);
			ResultadoDieselDTOInformes resultadoDieselInformes = generadorDTOResultados
					.generarResultadoDieselDTOInformes(medidas, defectos);
			
			logGases =  crearLogAuditoriaDiesel(resultado,resultadoDieselInformes,vehiculo);
			
		}else if(UtilTipoCombustible.isCombustibleGasolina(vehiculo.getTipoCombustible())) {
			if(UtilTipoVehiculo.isVehiculoMoto(vehiculo.getTipoVehiculo())){
				 ResultadoOttoMotocicletasDTO resultado = generadorDTOResultados.generarResultadoOttoMotos(medidas, defectos, vehiculo.getTiemposMotor().equals(2));
				 ResultadoOttoInformeDTO r2 = generadorDTOResultados.generarResultadoOttoInforme(medidas, defectos);
				 logGases = crearLogAuditoriaMoto(resultado,r2);
			}else {
				ResultadoOttoVehiculosDTO resultado  =generadorDTOResultados.generarResultadoOtto(medidas, defectos);
				ResultadoOttoInformeDTO re2 = generadorDTOResultados.generarResultadoOttoInforme(medidas, defectos);
				logGases = crearLogAuditoriaGasolina(resultado,re2,vehiculo);
			}
		}else {
			logger.log(Level.SEVERE,"Prueba de gases para vehiculo no diesel, no otto, no gasolina ");
			
		}
		
		
		
		StringBuilder sb = crearIdRegistroIdPrueba(prueba);
		sb.append("medida ").append(generarIdMedidas(medidas));
		logGases.setIdRegistro( sb.toString() );
		logGases.setTablaAfectada(TABLA_AFECTADA);
		return logGases;
	}

	private LogGasesSicovDTO crearLogAuditoriaGasolina(ResultadoOttoVehiculosDTO resultado, ResultadoOttoInformeDTO re2,Vehiculo v){
		LogGasesSicovDTO logGases = new LogGasesSicovDTO();
		
		logGases.setTemperaturaAmbiente(re2.getTemperaturaAmbiente());
		logGases.setHumedadRelativa(re2.getHumedadRelativa());
		
		logGases.setCatalizador(v.getCatalizador());
		
		logGases.setTempRalenti(resultado.getTempRalenti());
		logGases.setRpmRalenti(resultado.getRpmRalenti());
		logGases.setHCRalenti(resultado.getHcRalenti());
		logGases.setCORalenti(resultado.getCoRalenti());
		logGases.setCO2Ralenti(resultado.getCo2Ralenti());
		logGases.setO2Ralenti(resultado.getO2Ralenti());
					
		logGases.setTemperaturaPrueba(resultado.getTempCrucero());
		logGases.setRpmCrucero(resultado.getRpmCrucero());
		logGases.setHCCrucero(resultado.getHcCrucero());
		logGases.setCOCrucero(resultado.getCoCrucero());
		logGases.setCO2Crucero(resultado.getCo2Crucero());
		logGases.setO2Crucero(resultado.getO2Crucero());
		
		
		logGases.setDilucion(resultado.getDilucion() == null ? "" : resultado.getDilucion() );
		return logGases;
		
	}
	

	private LogGasesSicovDTO crearLogAuditoriaMoto(ResultadoOttoMotocicletasDTO resultado, ResultadoOttoInformeDTO r2) {
		LogGasesSicovDTO logGases = new LogGasesSicovDTO();
		
		logGases.setTemperaturaAmbiente(r2.getTemperaturaAmbiente());
		logGases.setHumedadRelativa(r2.getHumedadRelativa());
		
		
		logGases.setTempRalenti(resultado.getTempRalenti());
		logGases.setRpmRalenti(resultado.getRpmRalenti());
		logGases.setHCRalenti(resultado.getHcRalenti());
		logGases.setCORalenti(resultado.getCoRalenti());
		logGases.setCO2Ralenti(resultado.getCo2Ralenti());
		logGases.setO2Ralenti(resultado.getO2Ralenti());		
		return logGases;
	}


	private LogGasesSicovDTO crearLogAuditoriaDiesel(ResultadoDieselDTO r1, ResultadoDieselDTOInformes r2,Vehiculo v) {
		LogGasesSicovDTO log = new LogGasesSicovDTO();
		
		log.setTemperaturaAmbiente(r2.getTemperaturaAmbiental());
		log.setHumedadRelativa(r2.getHumedadRelativa());
		
		log.setVelocidadGobernada0(r2.getRpmGobernadaCiclo0());
		log.setVelocidadGobernada1(r2.getRpmGobernadaCiclo1());
		log.setVelocidadGobernada2(r2.getRpmGobernadaCiclo2());
		log.setVelocidadGobernada3(r2.getRpmGobernadaCiclo3());
		
		log.setOpacidad0(r1.getAceleracionCero());
		log.setOpacidad1(r1.getAceleracionUno());
		log.setOpacidad2(r1.getAceleracionDos());
		log.setOpacidad3(r1.getAceleracionTres());
		
		log.setValorFinal(r1.getValorFinal());
		
		log.setTemperaturaInicial(r1.getTemperaturaInicial());
		log.setTemperaturaFinal(r2.getTemperaturaFinal());
		
		log.setLTOEStandar("215");
		
		
		return log;
	}


	public LogLucesSicovDTO generarLogPruebaLuces(Prueba prueba, List<Medida> medidas, Vehiculo vehiculo, List<Defecto> defectos) {
		GeneradorLogLucesSicov g = new GeneradorLogLucesSicov();
		LogLucesSicovDTO log = g.generarLogSicov(medidas);
		StringBuilder sb = crearIdRegistroIdPrueba(prueba);
		sb.append("medida ").append(generarIdMedidas(medidas));		
		log.setIdRegistro(sb.toString());
		log.setTablaAfectada(TABLA_AFECTADA);
		return log;	
	}





	public LogAuditoriaTaximetro generarLogPruebaTaximetro(Prueba prueba,List<Defecto> defectos, List<Medida> medidas, Boolean aplicaTaximetro,
			String refLlanta) {
		
		ResultadoTaximetroDTO resultado = generadorDTOResultados.generarResultadoTaximetro(medidas, defectos, aplicaTaximetro, refLlanta);
		LogAuditoriaTaximetro logTax = crearLogTaximetro(resultado);
		StringBuilder sb = crearIdRegistroIdPrueba(prueba);
		sb.append("medida ").append(generarIdMedidas(medidas));
		logTax.setIdRegistro(generarIdMedidas(medidas));
		logTax.setTablaAfectada(TABLA_AFECTADA);
		return logTax;
		
	}


	private LogAuditoriaTaximetro crearLogTaximetro(ResultadoTaximetroDTO resultado) {
		
		LogAuditoriaTaximetro logTaximetro = new LogAuditoriaTaximetro();
		logTaximetro.setErrorDistancia(resultado.getErrorDistancia());
		logTaximetro.setErrorTiempo(resultado.getErrorTiempo());
		logTaximetro.setRefComercialLlanta(resultado.getReferenciaLlanta());
		return logTaximetro;
	}


	public LogFrenosSicovDTO generarLogPruebaFrenos(Prueba prueba, List<Medida> medidas, Vehiculo vehiculo, List<Defecto> defectos) {
		ResultadoFasDTO resultado = generadorDTOResultados.generarResultadoFAS(medidas);
		LogFrenosSicovDTO logFrenos = crearLogResultadoFrenos(resultado);
		StringBuilder sb = crearIdRegistroIdPrueba(prueba);
		sb.append("medida ").append(generarIdMedidas(medidas));		
		logFrenos.setIdRegistro(sb.toString());
		logFrenos.setTablaAfectada(TABLA_AFECTADA);
		return logFrenos;
	}

	
	public LogLlantasSicovDTO generarLogLlantas(Prueba prueba,List<Medida> medidas, Vehiculo vehiculo,List<Defecto> defectos) {
		
		LogLlantasSicovDTO log = new LogLlantasSicovDTO();
		
		GeneradorPresionLlantasDTO genResPresLlanta = new GeneradorPresionLlantasDTO();
		genResPresLlanta.setPuntoComoSeparadorDecimales();
		
		ResultadoPresionLlantasDTO res = genResPresLlanta.generarResultadoPresionLlantas(medidas);		
		log.setDerPresionEje1( res.getPresionLlantaEje1Der1() );
		log.setIzqPresionEje1( res.getPresionLlantaEje1Izq1() );
		
		log.setDerPresionInternaEje2( res.getPresionLlantaEje2Der1() );
		log.setDerPresionInternaEje3( res.getPresionLlantaEje3Der1() );
		log.setDerPresionInternaEje4( res.getPresionLlantaEje4Der1() );
		log.setDerPresionInternaEje5( res.getPresionLlantaEje5Der1() );
		
		
		log.setDerPresionExternaEje2( res.getPresionLlantaEje2Der2() );
		log.setDerPresionExternaEje3( res.getPresionLlantaEje3Der2() );
		log.setDerPresionExternaEje4( res.getPresionLlantaEje4Der2() );
		log.setDerPresionExternaEje5( res.getPresionLlantaEje5Der2() );
		
		log.setIzqPresionInternaEje2( res.getPresionLlantaEje2Izq1() );
		log.setIzqPresionInternaEje3( res.getPresionLlantaEje3Izq1() );
		log.setIzqPresionInternaEje4( res.getPresionLlantaEje4Izq1() );
		log.setIzqPresionInternaEje5( res.getPresionLlantaEje5Izq1() );
		
		
		log.setIzqPresionExternaEje2( res.getPresionLlantaEje2Izq2() );
		log.setIzqPresionExternaEje3( res.getPresionLlantaEje3Izq2() );
		log.setIzqPresionExternaEje4( res.getPresionLlantaEje4Izq2() );
		log.setIzqPresionExternaEje5( res.getPresionLlantaEje5Izq2() );
		
		log.setRepuestoPresion( res.getPresionLlantaRepuestoDer());
		log.setRepuesto2Presion( res.getPresionLLantaRepuestoIzq());
		
		DecimalFormat df = new DecimalFormat();
		
		
		
		for (Medida m : medidas) {
			switch (m.getTipoMedida().getTipoMedidaId()) {
			case PROFUNDIDAD_LABRADO_EJE1_IZQ:
				log.setIzqProfundidadEje1( df.format(m.getValor()) );
				break;
			case PROFUNDIDAD_LABRADO_EJE1_DER:
				log.setDerProfundidadEje1( df.format(m.getValor()) );
				break;		
			case PROFUNDIDAD_LABRADO_EJE2_IZQ_2:
				log.setIzqProfundidadExternaEje2( df.format(m.getValor()) );
				break;
			case PROFUNDIDAD_LABRADO_EJE2_IZQ_1:
				log.setIzqProfundidadInternaEje2( df.format(m.getValor()) );
				break;
			case PROFUNDIDAD_LABRADO_EJE2_DER_1:
				log.setDerProfundidadInternaEje2( df.format(m.getValor()) );
				break;
			case PROFUNDIDAD_LABRADO_EJE2_DER_2:
				log.setDerProfundidadExternaEje2( df.format(m.getValor()) );
				break;			
			case PROFUNDIDAD_LABRADO_EJE3_IZQ_2:
				log.setIzqProfundidadExternaEje3( df.format(m.getValor()) );
				break;
			case PROFUNDIDAD_LABRADO_EJE3_IZQ_1:
				log.setIzqProfundidadInternaEje3( df.format(m.getValor()) );
				break;
			case PROFUNDIDAD_LABRADO_EJE3_DER_1:
				log.setDerProfundidadInternaEje3( df.format(m.getValor()) );
				break;
			case PROFUNDIDAD_LABRADO_EJE3_DER_2:
				log.setDerProfundidadExternaEje3( df.format(m.getValor()) );
				break;
			case PROFUNDIDAD_LABRADO_EJE4_IZQ_2:
				log.setIzqProfundidadExternaEje4( df.format(m.getValor()) );
				break;
			case PROFUNDIDAD_LABRADO_EJE4_IZQ_1:
				log.setIzqProfundidadInternaEje4( df.format(m.getValor()) );
				break;
			case PROFUNDIDAD_LABRADO_EJE4_DER_1:
				log.setDerProfundidadInternaEje4( df.format(m.getValor()) );
				break;
			case PROFUNDIDAD_LABRADO_EJE4_DER_2:
				log.setDerProfundidadExternaEje4( df.format(m.getValor()) );
				break;
			case PROFUNDIDAD_LABRADO_EJE5_IZQ_2:
				log.setIzqProfundidadExternaEje5( df.format(m.getValor()) );
				break;
			case PROFUNDIDAD_LABRADO_EJE5_IZQ_1:
				log.setIzqProfundidadInternaEje2( df.format(m.getValor()) );
				break;
			case PROFUNDIDAD_LABRADO_EJE5_DER_1:
				log.setDerProfundidadInternaEje5( df.format(m.getValor()) );
				break;
			case PROFUNDIDAD_LABRADO_EJE5_DER_2:
				log.setDerProfundidadExternaEje5( df.format(m.getValor()) );
				break;
			case PROFUNDIDAD_LABRADO_REPUESTO_DER:
				log.setRepuestoProfundidad( df.format( m.getValor() ) );
				break;
			case PROFUNDIDAD_LABRADO_REPUESTO_IZQ:
				log.setRepuesto2Profundidad( df.format(m.getValor()) );
				break;
			}
		}
		
		StringBuilder sb = crearIdRegistroIdPrueba(prueba);
		sb.append("medida ").append(generarIdMedidas(medidas));		
		log.setIdRegistro(sb.toString());
		log.setTablaAfectada(TABLA_AFECTADA);		
		return log;
	}

	private LogFrenosSicovDTO crearLogResultadoFrenos(ResultadoFasDTO resultado) {
		
		LogFrenosSicovDTO logFrenos = new LogFrenosSicovDTO();
		logFrenos.setEficaciaAuxiliar(resultado.getEficaciaAuxiliar());
		logFrenos.setEficaciaTotal(resultado.getEficaciaTotal());
		
		//eje1		
		logFrenos.setFuerzaEje1Izquierdo(resultado.getFuerzaFrenadoIzqEje1());
		logFrenos.setPesoEje1Izquierdo(resultado.getPesoIzqEje1());
		logFrenos.setFuerzaEje1Derecho(resultado.getFuerzaFrenadoDerEje1());
		logFrenos.setPesoEje1Derecho(resultado.getPesoDerEje1());
		logFrenos.setEje1Desequilibrio(resultado.getDesequilibrioEje1());
		
		//eje2
		logFrenos.setFuerzaEje2Izquierdo(resultado.getFuerzaFrenadoIzqEje2());
		logFrenos.setPesoEje2Izquierdo(resultado.getPesoIzqEje2());
		logFrenos.setFuerzaEje2Derecho(resultado.getFuerzaFrenadoDerEje2());
		logFrenos.setPesoEje2Derecho(resultado.getPesoDerEje2());
		logFrenos.setEje2Desequilibrio(resultado.getDesequilibrioEje2());
		
		//eje3
		logFrenos.setFuerzaEje3Izquierdo(resultado.getFuerzaFrenadoIzqEje3());
		logFrenos.setPesoEje3Izquierdo(resultado.getPesoIzqEje3());		
		logFrenos.setFuerzaEje3Derecho(resultado.getFuerzaFrenadoDerEje3());
		logFrenos.setPesoEje3Derecho(resultado.getPesoDerEje3());
		logFrenos.setEje3Desequilibrio(resultado.getDesequilibrioEje3());
		
		//eje4
		logFrenos.setFuerzaEje4Izquierdo(resultado.getFuerzaFrenadoIzqEje4());
		logFrenos.setPesoEje4Izquierdo(resultado.getPesoIzqEje4());
		logFrenos.setFuerzaEje4Derecho(resultado.getFuerzaFrenadoDerEje4());
		logFrenos.setPesoEje4Derecho(resultado.getPesoDerEje4());
		logFrenos.setEje4Desequilibrio(resultado.getDesequilibrioEje4());
		
		//eje 5
		logFrenos.setFuerzaEje5Izquierdo(resultado.getFuerzaFrenadoIzqEje5());
		logFrenos.setPesoEje5Izquierdo(resultado.getPesoIzqEje5());
		logFrenos.setFuerzaEje5Derecho(resultado.getFuerzaFrenadoDerEje5());
		logFrenos.setPesoEje5Derecho(resultado.getPesoDerEje5());
		logFrenos.setEje5Desequilibrio(resultado.getDesequilibrioEje5());
		
		logFrenos.setFuerzaEje6Izquierdo(resultado.getFuerzaFrenadoIzqEje6());
		logFrenos.setPesoEje6Izquierdo(resultado.getPesoIzqEje6());
		logFrenos.setFuerzaEje6Derecho(resultado.getFuerzaFrenadoDerEje6());
		logFrenos.setPesoEje6Derecho(resultado.getPesoDerEje6());
		logFrenos.setEje6Desequilibrio(resultado.getDesequilibrioEje6());
		
		logFrenos.setDerFuerzaAuxiliar( resultado.getFuerzaDerechaAuxiliar() );
		logFrenos.setIzqFuerzaAuxiliar( resultado.getFuerzaIzquierdaAuxiliar() );
		
		logFrenos.setDerFuerzaPeso( resultado.getPesoDerechoTotal());
		logFrenos.setIzqFuerzaPeso( resultado.getPesoIzquierdoTotal());
		
		
		return logFrenos;
			
	}


	public LogAuditoriaSuspension generarLogPruebaSuspension(Prueba prueba, List<Medida> medidas, Vehiculo vehiculo,
			List<Defecto> defectos) {
		ResultadoFasDTO resultado = generadorDTOResultados.generarResultadoFAS(medidas);
		LogAuditoriaSuspension logSuspension = crearLogSuspension(resultado);
		StringBuilder sb = crearIdRegistroIdPrueba(prueba);
		sb.append("medida ").append(generarIdMedidas(medidas));
		logSuspension.setIdRegistro(sb.toString());
		logSuspension.setTablaAfectada(TABLA_AFECTADA);
		return logSuspension;
	}


	private LogAuditoriaSuspension crearLogSuspension(ResultadoFasDTO resultado) {
		LogAuditoriaSuspension logSuspension = new LogAuditoriaSuspension();
		logSuspension.setDelanteraDerecha(resultado.getSuspDerEje1());
		logSuspension.setDelanteraIzquierda(resultado.getSuspIzqEje1());
		logSuspension.setTraseraDerecha(resultado.getSuspDerEje2());
		logSuspension.setTraseraIzquierda(resultado.getSuspIzqEje2());
		return logSuspension;
	}


	public LogAlineacionSicovDTO generarLogPruebaDesviacion(Prueba prueba, List<Medida> medidas, Vehiculo vehiculo,
			List<Defecto> defectos) {
		ResultadoFasDTO resultado = generadorDTOResultados.generarResultadoFAS(medidas);		
		LogAlineacionSicovDTO logDesviacion = crearLogDesviacion(resultado);
		StringBuilder sb = crearIdRegistroIdPrueba(prueba);
		sb.append("medida ").append(generarIdMedidas(medidas));
		logDesviacion.setIdRegistro( sb.toString() );
		logDesviacion.setTablaAfectada(TABLA_AFECTADA);
		return logDesviacion;
	}


	private LogAlineacionSicovDTO crearLogDesviacion(ResultadoFasDTO resultado) {
		LogAlineacionSicovDTO logDesviacion = new LogAlineacionSicovDTO();
		logDesviacion.setEje1(resultado.getDesviacionLateralEje1());
		logDesviacion.setEje2(resultado.getDesviacionLateralEje2());
		logDesviacion.setEje3(resultado.getDesviacionLateralEje3());
		logDesviacion.setEje4(resultado.getDesviacionLateralEje4());
		logDesviacion.setEje5(resultado.getDesviacionLateralEje5());
		logDesviacion.setEje6(resultado.getDesviacionLateralEje6());
		return logDesviacion;
	}


	public LogInspeccionSensorialSicovDTO generarLogPruebaInspeccionSensorial(Prueba prueba, List<Medida> medidas, Vehiculo vehiculo) {
		
		LogInspeccionSensorialSicovDTO logIV = new LogInspeccionSensorialSicovDTO();
		Set<DefectoAsociado> defectosDetectadosNoDuplicados = prueba.getDefectosAsociados().stream().filter(da->da.getEstadoDefecto().equals(EstadoDefecto.DETECTADO)).collect(Collectors.toSet());
		String defectosConcatenados = defectosDetectadosNoDuplicados.stream().map(d->String.valueOf( d.getDefecto().getCodigoDefecto() ) ).collect(Collectors.joining("_"));
		logIV.setCodigoRechazo(defectosConcatenados);
		logIV.setObservaciones(prueba.getMotivoAborto());
		StringBuilder sb = crearIdRegistroIdPrueba(prueba);
		sb.append("defecto_prueba ").append(generarIdDefectos(prueba.getDefectosAsociados()));
		logIV.setIdRegistro(sb.toString());
		logIV.setTablaAfectada("prueba|defecto_prueba");
		return logIV;
	}	
	
	public String generarIdDefectos(List<DefectoAsociado> defectosAsociados){
		Set<DefectoAsociado> conjuntoDefectosNoDuplicados = 	defectosAsociados.stream().filter(da->da.getEstadoDefecto().equals(EstadoDefecto.DETECTADO)).collect(Collectors.toSet());
		return conjuntoDefectosNoDuplicados.stream().map(da->String.valueOf(da.getDefectoPruebaId())).collect(Collectors.joining(";"));
	}
	
	public StringBuilder crearIdRegistroIdPrueba(Prueba prueba){
		StringBuilder sb = new StringBuilder();
		sb.append("prueba ").append(prueba.getPruebaId()).append("|");
		return sb;
	}
}
