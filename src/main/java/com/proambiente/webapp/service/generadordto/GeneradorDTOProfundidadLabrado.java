package com.proambiente.webapp.service.generadordto;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.OptionalDouble;

import com.proambiente.modelo.Medida;
import com.proambiente.webapp.service.util.UtilInformesMedidas;

import com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado;
import com.proambiente.webapp.util.dto.ResultadoProfundidadLabradoDTO;

public class GeneradorDTOProfundidadLabrado {
	
	UtilInformesMedidas utilInformesMedidas = new UtilInformesMedidas();
	DecimalFormat df = new DecimalFormat("0.0");
	
	private static final char DECIMAL_SEPARATOR_COMA = ',';
	private static final char DECIMAL_SEPARATOR_PUNTO = '.';
	
	public ResultadoProfundidadLabradoDTO generarResultadoProfundidadLabrado(List<Medida> medidas) {
		
		utilInformesMedidas.ordenarListaMedidas(medidas);		
		ResultadoProfundidadLabradoDTO resultado = new ResultadoProfundidadLabradoDTO();
		
		OptionalDouble profEje1Der1 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE1_DER );
		String vlrProfEje1Der1 = profEje1Der1.isPresent()
				? df.format(profEje1Der1.getAsDouble())
				: "";
		resultado.setProfundidadLabradoEje1Der1(vlrProfEje1Der1);
		
		OptionalDouble profEje1Izq1 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE1_IZQ );
		String vlrProfEje1Izq1 = profEje1Izq1.isPresent()
				? df.format(profEje1Izq1.getAsDouble())
				: "";
		resultado.setProfundidadLabradoEje1Izq1(vlrProfEje1Izq1);
		
		//eje 2
		
		OptionalDouble profEje2Der1 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE2_DER_1 );
		String vlrProfEje2Der1 = profEje2Der1.isPresent()
				? df.format(profEje2Der1.getAsDouble())
				: "";
		resultado.setProfundidadLabradoEje2Der1(vlrProfEje2Der1);
		
		OptionalDouble profEje2Der2 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE2_DER_2 );
		String vlrProfEje2Der2 = profEje2Der2.isPresent()
				? df.format(profEje2Der2.getAsDouble())
				: "";
		resultado.setProfundidadLabradoEje2Der2(vlrProfEje2Der2);
		
		OptionalDouble profEje2Der3 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE2_DER_3 );
		String vlrProfEje2Der3 = profEje2Der3.isPresent()
				? df.format(profEje2Der3.getAsDouble())
				: "";
		resultado.setProfundidadLabradoEje2Der3(vlrProfEje2Der3);
		
		OptionalDouble profEje2Der4 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE2_DER_4 );
		String vlrProfEje2Der4 = profEje2Der4.isPresent()
				? df.format(profEje2Der4.getAsDouble())
				: "";
		resultado.setProfundidadLabradoEje2Der4(vlrProfEje2Der4);
		
		OptionalDouble profEje2Izq1 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE2_IZQ_1 );
		String vlrProfEje2Izq1 = profEje2Izq1.isPresent()
				? df.format(profEje2Izq1.getAsDouble())
				: "";
		resultado.setProfundidadLabradoEje2Izq1(vlrProfEje2Izq1);
		
		OptionalDouble profEje2Izq2 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE2_IZQ_2 );
		String vlrProfEje2Izq2 = profEje2Izq2.isPresent()
				? df.format(profEje2Izq2.getAsDouble())
				: "";
		resultado.setProfundidadLabradoEje2Izq2(vlrProfEje2Izq2);
		
		OptionalDouble profEje2Izq3 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE2_IZQ_3 );
		String vlrProfEje2Izq3 = profEje2Izq3.isPresent()
				? df.format(profEje2Izq3.getAsDouble())
				: "";
		resultado.setProfundidadLabradoEje2Izq3(vlrProfEje2Izq3);
		
		OptionalDouble profEje2Izq4 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE2_IZQ_4 );
		String vlrProfEje2Izq4 = profEje2Izq4.isPresent()
				? df.format(profEje2Izq4.getAsDouble())
				: "";
		resultado.setProfundidadLabradoEje2Izq4(vlrProfEje2Izq4);
		
		//eje 3
		
		OptionalDouble profEje3Der1 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE3_DER_1 );
		String vlrProfEje3Der1 = profEje3Der1.isPresent()
				? df.format(profEje3Der1.getAsDouble())
				: "";
		resultado.setProfundidadLabradoEje3Der1(vlrProfEje3Der1);
		
		OptionalDouble profEje3Der2 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE3_DER_2 );
		String vlrProfEje3Der2 = profEje3Der2.isPresent()
				? df.format(profEje3Der2.getAsDouble())
				: "";
		resultado.setProfundidadLabradoEje3Der2(vlrProfEje3Der2);
		
		OptionalDouble profEje3Der3 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE3_DER_3 );
		String vlrProfEje3Der3 = profEje3Der3.isPresent()
				? df.format(profEje3Der3.getAsDouble())
				: "";
		resultado.setProfundidadLabradoEje3Der3(vlrProfEje3Der3);
		
		OptionalDouble profEje3Der4 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE3_DER_4 );
		String vlrProfEje3Der4 = profEje3Der4.isPresent()
				? df.format(profEje3Der4.getAsDouble())
				: "";
		resultado.setProfundidadLabradoEje3Der4(vlrProfEje3Der4);
		
		OptionalDouble profEje3Izq1 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE3_IZQ_1 );
		String vlrProfEje3Izq1 = profEje3Izq1.isPresent()
				? df.format(profEje3Izq1.getAsDouble())
				: "";
		resultado.setProfundidadLabradoEje3Izq1(vlrProfEje3Izq1);
		
		OptionalDouble profEje3Izq2 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE3_IZQ_2 );
		String vlrProfEje3Izq2 = profEje3Izq2.isPresent()
				? df.format(profEje3Izq2.getAsDouble())
				: "";
		resultado.setProfundidadLabradoEje3Izq2(vlrProfEje3Izq2);
		
		OptionalDouble profEje3Izq3 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE3_IZQ_3 );
		String vlrProfEje3Izq3 = profEje3Izq3.isPresent()
				? df.format(profEje3Izq3.getAsDouble())
				: "";
		resultado.setProfundidadLabradoEje3Izq3(vlrProfEje3Izq3);
		
		OptionalDouble profEje3Izq4 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE3_IZQ_4 );
		String vlrProfEje3Izq4 = profEje3Izq4.isPresent()
				? df.format(profEje3Izq4.getAsDouble())
				: "";
		resultado.setProfundidadLabradoEje3Izq4(vlrProfEje3Izq4);
		
		//eje 4
		
		OptionalDouble profEje4Der1 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE4_DER_1 );
		String vlrProfEje4Der1 = profEje4Der1.isPresent()
				? df.format(profEje4Der1.getAsDouble())
				: "";
		resultado.setProfundidadLabradoEje4Der1(vlrProfEje4Der1);
		
		OptionalDouble profEje4Der2 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE4_DER_2 );
		String vlrProfEje4Der2 = profEje4Der2.isPresent()
				? df.format(profEje4Der2.getAsDouble())
				: "";
		resultado.setProfundidadLabradoEje4Der2(vlrProfEje4Der2);
		
		OptionalDouble profEje4Der3 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE4_DER_3 );
		String vlrProfEje4Der3 = profEje4Der3.isPresent()
				? df.format(profEje4Der3.getAsDouble())
				: "";
		resultado.setProfundidadLabradoEje4Der3(vlrProfEje4Der3);
		
		OptionalDouble profEje4Der4 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE4_DER_4 );
		String vlrProfEje4Der4 = profEje4Der4.isPresent()
				? df.format(profEje4Der4.getAsDouble())
				: "";
		resultado.setProfundidadLabradoEje4Der4(vlrProfEje4Der4);
		
		OptionalDouble profEje4Izq1 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE4_IZQ_1 );
		String vlrProfEje4Izq1 = profEje4Izq1.isPresent()
				? df.format(profEje4Izq1.getAsDouble())
				: "";
		resultado.setProfundidadLabradoEje4Izq1(vlrProfEje4Izq1);
		
		OptionalDouble profEje4Izq2 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE4_IZQ_2 );
		String vlrProfEje4Izq2 = profEje4Izq2.isPresent()
				? df.format(profEje4Izq2.getAsDouble())
				: "";
		resultado.setProfundidadLabradoEje4Izq2(vlrProfEje4Izq2);
		
		OptionalDouble profEje4Izq3 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE4_IZQ_3 );
		String vlrProfEje4Izq3 = profEje4Izq3.isPresent()
				? df.format(profEje4Izq3.getAsDouble())
				: "";
		resultado.setProfundidadLabradoEje4Izq3(vlrProfEje4Izq3);
				
		OptionalDouble profEje4Izq4 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE4_IZQ_4 );
		String vlrProfEje4Izq4 = profEje4Izq4.isPresent()
				? df.format(profEje4Izq4.getAsDouble())
				: "";
		resultado.setProfundidadLabradoEje4Izq4(vlrProfEje4Izq4);
		
		//eje 5 
		
		OptionalDouble profEje5Der1 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE5_DER_1 );
		String vlrProfEje5Der1 = profEje5Der1.isPresent()
				? df.format(profEje5Der1.getAsDouble())
				: "";
		resultado.setProfundidadLabradoEje5Der1(vlrProfEje5Der1);
		
		OptionalDouble profEje5Der2 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE5_DER_2 );
		String vlrProfEje5Der2 = profEje5Der2.isPresent()
				? df.format(profEje5Der2.getAsDouble())
				: "";
		resultado.setProfundidadLabradoEje5Der2(vlrProfEje5Der2);
		
		OptionalDouble profEje5Der3 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE5_DER_3 );
		String vlrProfEje5Der3 = profEje5Der3.isPresent()
				? df.format(profEje5Der3.getAsDouble())
				: "";
		resultado.setProfundidadLabradoEje5Der3(vlrProfEje5Der3);
		
		
		OptionalDouble profEje5Der4 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE5_DER_4 );
		String vlrProfEje5Der4 = profEje5Der4.isPresent()
				? df.format(profEje5Der4.getAsDouble())
				: "";
		resultado.setProfundidadLabradoEje5Der4(vlrProfEje5Der4);
		
		OptionalDouble profEje5Izq1 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE5_IZQ_1 );
		String vlrProfEje5Izq1 = profEje5Izq1.isPresent()
				? df.format(profEje5Izq1.getAsDouble())
				: "";
		resultado.setProfundidadLabradoEje5Izq1(vlrProfEje5Izq1);
		
		OptionalDouble profEje5Izq2 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE5_IZQ_2 );
		String vlrProfEje5Izq2 = profEje5Izq2.isPresent()
				? df.format(profEje5Izq2.getAsDouble())
				: "";
		resultado.setProfundidadLabradoEje5Izq2(vlrProfEje5Izq2);
		
		OptionalDouble profEje5Izq3 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE5_IZQ_3 );
		String vlrProfEje5Izq3 = profEje5Izq3.isPresent()
				? df.format(profEje5Izq3.getAsDouble())
				: "";
		resultado.setProfundidadLabradoEje5Izq3(vlrProfEje5Izq3);
		
		OptionalDouble profEje5Izq4 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE5_IZQ_4 );
		String vlrProfEje5Izq4 = profEje5Izq4.isPresent()
				? df.format(profEje5Izq4.getAsDouble())
				: "";
		resultado.setProfundidadLabradoEje5Izq4(vlrProfEje5Izq4);
		
		//lantas de repuesto
		
		OptionalDouble profRepuestoDer = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_REPUESTO_DER );
		String vlrProfRepuestoDer = profRepuestoDer.isPresent()
				? df.format(profRepuestoDer.getAsDouble())
				: "";
		resultado.setProfundidadLabradoRepuestoDerecha(vlrProfRepuestoDer);
		
		OptionalDouble profRepuestoIzq = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_REPUESTO_IZQ );
		String vlrProfRepuestoIzq = profRepuestoIzq.isPresent()
				? df.format(profRepuestoIzq.getAsDouble())
				: "";
		resultado.setProfundidadLabradoRepuestoIzquierda(vlrProfRepuestoIzq);
		
		return resultado;
	}
	
	public void setPuntoComoSeparadorDecimales() {
		DecimalFormatSymbols ds = DecimalFormatSymbols.getInstance();
		ds.setDecimalSeparator(DECIMAL_SEPARATOR_PUNTO);
		df.setDecimalFormatSymbols(ds);
	}

	public void setComaComoSeparadorDecimales() {
		DecimalFormatSymbols ds = DecimalFormatSymbols.getInstance();
		ds.setDecimalSeparator(DECIMAL_SEPARATOR_COMA);
		df.setDecimalFormatSymbols(ds);
	}
	
}
