package com.proambiente.webapp.service.generadordto;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.OptionalDouble;

import com.proambiente.modelo.Medida;
import com.proambiente.webapp.service.util.UtilInformesMedidas;
import com.proambiente.webapp.util.ConstantesMedidasPresionLlanta;
import com.proambiente.webapp.util.dto.ResultadoPresionLlantasDTO;

public class GeneradorPresionLlantasDTO {
	
	UtilInformesMedidas utilInformesMedidas = new UtilInformesMedidas();
	DecimalFormat df = new DecimalFormat("#.0");
	private static final char DECIMAL_SEPARATOR_COMA = ',';
	private static final char DECIMAL_SEPARATOR_PUNTO = '.';
	
	public ResultadoPresionLlantasDTO generarResultadoPresionLlantas(List<Medida> medidas) {
		
		utilInformesMedidas.ordenarListaMedidas(medidas);
		ResultadoPresionLlantasDTO resultado = new ResultadoPresionLlantasDTO();
		
		OptionalDouble presEje1Der1 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasPresionLlanta.P_EJE_1_LL1_DER );
		String vlrPresEje1Der1 = presEje1Der1.isPresent()
				? df.format(presEje1Der1.getAsDouble())
				: "";
		resultado.setPresionLlantaEje1Der1(vlrPresEje1Der1);
		
		OptionalDouble presEje1Izq1 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasPresionLlanta.P_EJE_1_LL1_IZQ );
		String vlrPresEje1Izq1 = presEje1Izq1.isPresent()
				? df.format(presEje1Izq1.getAsDouble())
				: "";
		resultado.setPresionLlantaEje1Izq1(vlrPresEje1Izq1);
		
		//eje 2
		
		OptionalDouble presEje2Der1 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasPresionLlanta.P_EJE_2_LL1_DER );
		String vlrPresEje2Der1 = presEje2Der1.isPresent()
				? df.format(presEje2Der1.getAsDouble())
				: "";
		resultado.setPresionLlantaEje2Der1(vlrPresEje2Der1);
		
		
		OptionalDouble presEje2Der2 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasPresionLlanta.P_EJE_2_LL2_DER );
		String vlrPresEje2Der2 = presEje2Der2.isPresent()
				? df.format(presEje2Der2.getAsDouble())
				: "";
		resultado.setPresionLlantaEje2Der2(vlrPresEje2Der2);
		
		
		OptionalDouble presEje2Der3 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasPresionLlanta.P_EJE_2_LL3_DER );
		String vlrPresEje2Der3 = presEje2Der3.isPresent()
				? df.format(presEje2Der3.getAsDouble())
				: "";
		resultado.setPresionLlantaEje2Der3(vlrPresEje2Der3);
		
		OptionalDouble presEje2Der4 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasPresionLlanta.P_EJE_2_LL4_DER );
		String vlrPresEje2Der4 = presEje2Der4.isPresent()
				? df.format(presEje2Der4.getAsDouble())
				: "";
		resultado.setPresionLlantaEje2Der4(vlrPresEje2Der4);
		
		OptionalDouble presEje2Izq1 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasPresionLlanta.P_EJE_2_LL1_IZQ );
		String vlrPresEje2Izq1 = presEje2Izq1.isPresent()
				? df.format(presEje2Izq1.getAsDouble())
				: "";
		resultado.setPresionLlantaEje2Izq1(vlrPresEje2Izq1);
		
		OptionalDouble presEje2Izq2 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasPresionLlanta.P_EJE_2_LL2_IZQ );
		String vlrPresEje2Izq2 = presEje2Izq2.isPresent()
				? df.format(presEje2Izq2.getAsDouble())
				: "";
		resultado.setPresionLlantaEje2Izq2(vlrPresEje2Izq2);

		OptionalDouble presEje2Izq3 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasPresionLlanta.P_EJE_2_LL3_IZQ );
		String vlrPresEje2Izq3 = presEje2Izq3.isPresent()
				? df.format(presEje2Izq3.getAsDouble())
				: "";
		resultado.setPresionLlantaEje2Izq3(vlrPresEje2Izq3);
		
		OptionalDouble presEje2Izq4 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasPresionLlanta.P_EJE_2_LL4_IZQ );
		String vlrPresEje2Izq4 = presEje2Izq4.isPresent()
				? df.format(presEje2Izq4.getAsDouble())
				: "";
		resultado.setPresionLlantaEje2Izq4(vlrPresEje2Izq4);
		
		//eje 3
		
		OptionalDouble presEje3Der1 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasPresionLlanta.P_EJE_3_LL1_DER );
		String vlrPresEje3Der1 = presEje3Der1.isPresent()
				? df.format(presEje3Der1.getAsDouble())
				: "";
		resultado.setPresionLlantaEje3Der1(vlrPresEje3Der1);
		
		
		OptionalDouble presEje3Der2 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasPresionLlanta.P_EJE_3_LL2_DER );
		String vlrPresEje3Der2 = presEje3Der2.isPresent()
				? df.format(presEje3Der2.getAsDouble())
				: "";
		resultado.setPresionLlantaEje3Der2(vlrPresEje3Der2);
		
		
		OptionalDouble presEje3Der3 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasPresionLlanta.P_EJE_3_LL3_DER );
		String vlrPresEje3Der3 = presEje3Der3.isPresent()
				? df.format(presEje3Der3.getAsDouble())
				: "";
		resultado.setPresionLlantaEje3Der3(vlrPresEje3Der3);
		
		OptionalDouble presEje3Der4 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasPresionLlanta.P_EJE_3_LL4_DER );
		String vlrPresEje3Der4 = presEje3Der4.isPresent()
				? df.format(presEje3Der4.getAsDouble())
				: "";
		resultado.setPresionLlantaEje3Der4(vlrPresEje3Der4);
		
		OptionalDouble presEje3Izq1 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasPresionLlanta.P_EJE_3_LL1_IZQ );
		String vlrPresEje3Izq1 = presEje3Izq1.isPresent()
				? df.format(presEje3Izq1.getAsDouble())
				: "";
		resultado.setPresionLlantaEje3Izq1(vlrPresEje3Izq1);
		
		OptionalDouble presEje3Izq2 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasPresionLlanta.P_EJE_3_LL2_IZQ );
		String vlrPresEje3Izq2 = presEje3Izq2.isPresent()
				? df.format(presEje3Izq2.getAsDouble())
				: "";
		resultado.setPresionLlantaEje3Izq2(vlrPresEje3Izq2);

		OptionalDouble presEje3Izq3 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasPresionLlanta.P_EJE_3_LL3_IZQ );
		String vlrPresEje3Izq3 = presEje3Izq3.isPresent()
				? df.format(presEje3Izq3.getAsDouble())
				: "";
		resultado.setPresionLlantaEje3Izq3(vlrPresEje3Izq3);
		
		OptionalDouble presEje3Izq4 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasPresionLlanta.P_EJE_3_LL4_IZQ );
		String vlrPresEje3Izq4 = presEje2Izq4.isPresent()
				? df.format(presEje3Izq4.getAsDouble())
				: "";
		resultado.setPresionLlantaEje3Izq4(vlrPresEje3Izq4);
		
		//eje 4
		
		OptionalDouble presEje4Der1 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasPresionLlanta.P_EJE_4_LL1_DER );
		String vlrPresEje4Der1 = presEje4Der1.isPresent()
				? df.format(presEje4Der1.getAsDouble())
				: "";
		resultado.setPresionLlantaEje4Der1(vlrPresEje4Der1);
		
		
		OptionalDouble presEje4Der2 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasPresionLlanta.P_EJE_4_LL2_DER );
		String vlrPresEje4Der2 = presEje4Der2.isPresent()
				? df.format(presEje4Der2.getAsDouble())
				: "";
		resultado.setPresionLlantaEje4Der2(vlrPresEje4Der2);
		
		
		OptionalDouble presEje4Der3 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasPresionLlanta.P_EJE_4_LL3_DER );
		String vlrPresEje4Der3 = presEje4Der3.isPresent()
				? df.format(presEje4Der3.getAsDouble())
				: "";
		resultado.setPresionLlantaEje4Der3(vlrPresEje4Der3);
		
		OptionalDouble presEje4Der4 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasPresionLlanta.P_EJE_4_LL4_DER );
		String vlrPresEje4Der4 = presEje4Der4.isPresent()
				? df.format(presEje4Der4.getAsDouble())
				: "";
		resultado.setPresionLlantaEje4Der4(vlrPresEje4Der4);
		
		OptionalDouble presEje4Izq1 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasPresionLlanta.P_EJE_4_LL1_IZQ );
		String vlrPresEje4Izq1 = presEje4Izq1.isPresent()
				? df.format(presEje4Izq1.getAsDouble())
				: "";
		resultado.setPresionLlantaEje4Izq1(vlrPresEje4Izq1);
		
		OptionalDouble presEje4Izq2 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasPresionLlanta.P_EJE_4_LL2_IZQ );
		String vlrPresEje4Izq2 = presEje4Izq2.isPresent()
				? df.format(presEje4Izq2.getAsDouble())
				: "";
		resultado.setPresionLlantaEje4Izq2(vlrPresEje4Izq2);

		OptionalDouble presEje4Izq3 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasPresionLlanta.P_EJE_4_LL3_IZQ );
		String vlrPresEje4Izq3 = presEje4Izq3.isPresent()
				? df.format(presEje4Izq3.getAsDouble())
				: "";
		resultado.setPresionLlantaEje3Izq3(vlrPresEje4Izq3);
		
		OptionalDouble presEje4Izq4 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasPresionLlanta.P_EJE_4_LL4_IZQ );
		String vlrPresEje4Izq4 = presEje4Izq4.isPresent()
				? df.format(presEje4Izq4.getAsDouble())
				: "";
		resultado.setPresionLlantaEje4Izq4(vlrPresEje4Izq4);
		
		//eje 5
		OptionalDouble presEje5Der1 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasPresionLlanta.P_EJE_5_LL1_DER );
		String vlrPresEje5Der1 = presEje5Der1.isPresent()
				? df.format(presEje5Der1.getAsDouble())
				: "";
		resultado.setPresionLlantaEje5Der1(vlrPresEje5Der1);
		
		
		OptionalDouble presEje5Der2 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasPresionLlanta.P_EJE_5_LL2_DER );
		String vlrPresEje5Der2 = presEje5Der2.isPresent()
				? df.format(presEje5Der2.getAsDouble())
				: "";
		resultado.setPresionLlantaEje5Der2(vlrPresEje5Der2);
		
		
		OptionalDouble presEje5Der3 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasPresionLlanta.P_EJE_5_LL3_DER );
		String vlrPresEje5Der3 = presEje5Der3.isPresent()
				? df.format(presEje5Der3.getAsDouble())
				: "";
		resultado.setPresionLlantaEje5Der3(vlrPresEje5Der3);
		
		OptionalDouble presEje5Der4 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasPresionLlanta.P_EJE_5_LL4_DER );
		String vlrPresEje5Der4 = presEje5Der4.isPresent()
				? df.format(presEje5Der4.getAsDouble())
				: "";
		resultado.setPresionLlantaEje5Der4(vlrPresEje5Der4);
		
		OptionalDouble presEje5Izq1 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasPresionLlanta.P_EJE_5_LL1_IZQ );
		String vlrPresEje5Izq1 = presEje5Izq1.isPresent()
				? df.format(presEje5Izq1.getAsDouble())
				: "";
		resultado.setPresionLlantaEje5Izq1(vlrPresEje5Izq1);
		
		OptionalDouble presEje5Izq2 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasPresionLlanta.P_EJE_5_LL2_IZQ );
		String vlrPresEje5Izq2 = presEje5Izq2.isPresent()
				? df.format(presEje5Izq2.getAsDouble())
				: "";
		resultado.setPresionLlantaEje5Izq2(vlrPresEje5Izq2);

		OptionalDouble presEje5Izq3 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasPresionLlanta.P_EJE_5_LL3_IZQ );
		String vlrPresEje5Izq3 = presEje5Izq3.isPresent()
				? df.format(presEje5Izq3.getAsDouble())
				: "";
		resultado.setPresionLlantaEje5Izq3(vlrPresEje5Izq3);
		
		OptionalDouble presEje5Izq4 = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasPresionLlanta.P_EJE_5_LL4_IZQ );
		String vlrPresEje5Izq4 = presEje5Izq4.isPresent()
				? df.format(presEje5Izq4.getAsDouble())
				: "";
		resultado.setPresionLlantaEje5Izq4(vlrPresEje5Izq4);
		
		OptionalDouble presLlantaRepuestoDer = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasPresionLlanta.P_LLANTA_REPUESTO_1 );
		String vlrPresLlantaRepuesto = presLlantaRepuestoDer.isPresent()
				? df.format(presLlantaRepuestoDer.getAsDouble())
				: "";
		resultado.setPresionLlantaRepuestoDer(vlrPresLlantaRepuesto);
		
		OptionalDouble presLlantaRepuestoIzq = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasPresionLlanta.P_LLANTA_REPUESTO_2 );
		String vlrPresLlantaRepuestoIzq = presLlantaRepuestoIzq.isPresent()
				? df.format(presLlantaRepuestoIzq.getAsDouble())
				: "";
		resultado.setPresionLLantaRepuestoIzq(vlrPresLlantaRepuestoIzq);
		
		
		return resultado;
	}
	
	public void setPuntoComoSeparadorDecimales() {
		DecimalFormatSymbols ds = DecimalFormatSymbols.getInstance();
		ds.setDecimalSeparator(DECIMAL_SEPARATOR_PUNTO);
		df.setDecimalFormatSymbols(ds);
	}

	public void setComaComoSeparadorDecimales() {
		DecimalFormatSymbols ds = DecimalFormatSymbols.getInstance();
		ds.setDecimalSeparator(DECIMAL_SEPARATOR_COMA);
		df.setDecimalFormatSymbols(ds);
	}


}
