package com.proambiente.webapp.service.generadordto;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;

import javax.inject.Inject;

import com.proambiente.modelo.Medida;
import com.proambiente.modelo.Permisible;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.modelo.dto.PermisibleDieselDTO;
import com.proambiente.modelo.dto.PermisibleOttoVehiculoDTO;
import com.proambiente.modelo.dto.PermisibleSuspensionDTO;
import com.proambiente.modelo.dto.PermisibleTaximetroDTO;
import com.proambiente.modelo.dto.PermisiblesDesviacionDTO;
import com.proambiente.modelo.dto.PermisiblesFrenosDTO;
import com.proambiente.modelo.dto.PermisiblesLucesDTO;
import com.proambiente.modelo.dto.PermisiblesMotoDTO;
import com.proambiente.webapp.dao.PermisibleFacade;
import com.proambiente.webapp.service.util.UtilInformesMedidas;
import com.proambiente.webapp.util.ConstantesMedidasAlineacion;
import com.proambiente.webapp.util.ConstantesMedidasFrenos;
import com.proambiente.webapp.util.ConstantesMedidasGases;
import com.proambiente.webapp.util.ConstantesMedidasLuces;
import com.proambiente.webapp.util.ConstantesMedidasSuspension;
import com.proambiente.webapp.util.ConstantesMedidasTaximetro;

public class GeneradorDTOPermisibles {

	@Inject
	PermisibleFacade permisibleService;

	DecimalFormat df = new DecimalFormat("#");
	private static final String DF_SIN_DECIMALES = "#";
	private static final String DF_UN_DECIMAL = "0.0";
	private static final String DF_DOS_DECIMALES = "#0.00";
	private static final char DECIMAL_SEPARATOR_COMA = ',';
	private static final char DECIMAL_SEPARATOR_PUNTO = '.';
	private static final String CADENA_VACIA = "";

	private UtilInformesMedidas utilInformesMedidas = new UtilInformesMedidas();

	public PermisiblesLucesDTO generarPermisibleLuces(Boolean isMoto, Revision revision, List<Medida> medidas) {
		df.applyPattern(DF_UN_DECIMAL);
		PermisiblesLucesDTO permisibleLuces = new PermisiblesLucesDTO();
		if (!isMoto) {

			Optional<Medida> intensidadBajaDerecha = utilInformesMedidas.obtenerMedida(medidas.stream(),
					ConstantesMedidasLuces.INTENSIDAD_BAJA_DERECHA);
			MaximoMinimoPermisibleDTO permisibleDTO = formatearPermisible(intensidadBajaDerecha, revision);
			permisibleLuces.setPermisibleIntensidadBajaDerecha(permisibleDTO.getMinimo());

			Optional<Medida> intensidadBajaIzquierda = utilInformesMedidas.obtenerMedida(medidas.stream(),
					ConstantesMedidasLuces.INTENSIDAD_BAJA_IZQUIERDA);
			permisibleDTO = formatearPermisible(intensidadBajaIzquierda, revision);
			permisibleLuces.setPermisibleIntensidadBajaIzquierda(permisibleDTO.getMinimo());

			Optional<Medida> sumatoriaLuces = utilInformesMedidas.obtenerMedida(medidas.stream(),
					ConstantesMedidasLuces.SUMA_LUCES);
			permisibleDTO = formatearPermisible(sumatoriaLuces, revision);
			permisibleLuces.setPermisibleLuces(permisibleDTO.getMaximo());

			Optional<Medida> anguloBajaDerecha = utilInformesMedidas.obtenerMedida(medidas.stream(),
					ConstantesMedidasLuces.INCLINACION_BAJA_DERECHA);
			permisibleDTO = formatearPermisible(anguloBajaDerecha, revision);
			String formatoAnguloBajaDerecha = String.format("[%s - %s]", permisibleDTO.getMinimo(),
					permisibleDTO.getMaximo());
			permisibleLuces.setPermisibleRangoInclinacionBajaDerecha(formatoAnguloBajaDerecha);

			Optional<Medida> anguloBajaIzquierda = utilInformesMedidas.obtenerMedida(medidas.stream(),
					ConstantesMedidasLuces.INCLINACION_BAJA_IZQUIERDA);
			permisibleDTO = formatearPermisible(anguloBajaIzquierda, revision);
			String formagoAnguloBajaIzquierda = String.format("[%s - %s]", permisibleDTO.getMinimo(),
					permisibleDTO.getMaximo());
			permisibleLuces.setPermisibleRangoInclinacionBajaIzquierda(formagoAnguloBajaIzquierda);
		} else {
			Optional<Medida> intensidadBajaDerechaMoto = utilInformesMedidas.obtenerMedida(medidas.stream(), ConstantesMedidasLuces.INTENSIDAD_BAJA_DERECHA_MOTOS);
			MaximoMinimoPermisibleDTO permisibleDTO = formatearPermisible(intensidadBajaDerechaMoto, revision);
			permisibleLuces.setPermisibleIntensidadBajaDerecha( permisibleDTO.getMinimo());
			
			Optional<Medida> incBajaDerechaMoto = utilInformesMedidas.obtenerMedida(medidas.stream(),ConstantesMedidasLuces.INCLINACION_BAJA_DERECHA_MOTOS);
			permisibleDTO = formatearPermisible(incBajaDerechaMoto,revision);
			String formatoIncBajaDerecha = String.format("[%s - %s]", permisibleDTO.getMinimo(),
					permisibleDTO.getMaximo());
			permisibleLuces.setPermisibleRangoInclinacionBajaDerecha(formatoIncBajaDerecha);
		}
		return permisibleLuces;
	}

	public MaximoMinimoPermisibleDTO formatearPermisible(Optional<Medida> optMedida, Revision revision) {

		MaximoMinimoPermisibleDTO permisibleDTO = new MaximoMinimoPermisibleDTO(CADENA_VACIA, CADENA_VACIA);

		if (optMedida.isPresent()) {
			Medida m = optMedida.get();

			Permisible permisible = permisibleService.buscarPermisible(m.getTipoMedida().getTipoMedidaId(),
					revision.getVehiculo().getTipoVehiculo().getTipoVehiculoId(), revision.getVehiculo().getModelo());
			if (permisible != null) {
				Double valorMinimo = permisible.getValorMinimo();
				String valorMinimoStr = df.format(valorMinimo);
				permisibleDTO.setMinimo(valorMinimoStr);

				Double valorMaximo = permisible.getValorMaximo();
				String valorMaximoStr = df.format(valorMaximo);
				permisibleDTO.setMaximo(valorMaximoStr);
			}
		}
		return permisibleDTO;

	}
	
	private MaximoMinimoPermisibleDTO formatearPermisible(Optional<Medida> optMedida, Vehiculo vehiculo) {
		MaximoMinimoPermisibleDTO permisibleDTO = new MaximoMinimoPermisibleDTO(CADENA_VACIA, CADENA_VACIA);

		if (optMedida.isPresent()) {
			Medida m = optMedida.get();

			Permisible permisible = permisibleService.buscarPermisibleCondicionalSecundario(m.getTipoMedida().getTipoMedidaId(),
					vehiculo.getTipoVehiculo().getTipoVehiculoId(), vehiculo.getModelo(),vehiculo.getCilindraje());
			if (permisible != null) {
				Double valorMinimo = permisible.getValorMinimo();
				String valorMinimoStr = df.format(valorMinimo);
				permisibleDTO.setMinimo(valorMinimoStr);

				Double valorMaximo = permisible.getValorMaximo();
				String valorMaximoStr = df.format(valorMaximo);
				permisibleDTO.setMaximo(valorMaximoStr);
			}
		}
		return permisibleDTO;
	}

	public void setPuntoComoSeparadorDecimales() {
		DecimalFormatSymbols ds = DecimalFormatSymbols.getInstance();
		ds.setDecimalSeparator(DECIMAL_SEPARATOR_PUNTO);
		df.setDecimalFormatSymbols(ds);
	}
	
	public PermisibleSuspensionDTO generarPermisibleSuspension(List<Medida> medidas,Revision revision){
		
		PermisibleSuspensionDTO permisibleSuspensionDTO = new PermisibleSuspensionDTO(CADENA_VACIA);
		Optional<Medida> medidaAdherencia = utilInformesMedidas.obtenerMedida(medidas.stream(), ConstantesMedidasSuspension.ADHERENCIA_DERECHA_EJE_UNO);
		if(medidaAdherencia.isPresent()){
			MaximoMinimoPermisibleDTO permisibleDTO = formatearPermisible(medidaAdherencia,revision);
			permisibleSuspensionDTO.setValorMinimoSuspension(permisibleDTO.getMinimo());
		} else {
			
		}
		return permisibleSuspensionDTO;
	}
	
	public PermisiblesDesviacionDTO generarPermisibleDesviacion( List<Medida> medidas, Revision revision){
		PermisiblesDesviacionDTO permisiblesDesviacionDTO = new PermisiblesDesviacionDTO();
		Optional<Medida> medidaDesviacionEje1 = utilInformesMedidas.obtenerMedida( medidas.stream() , ConstantesMedidasAlineacion.DESVIACION_EJE_UNO );
		if(medidaDesviacionEje1.isPresent()){
			MaximoMinimoPermisibleDTO permisibleDTO = formatearPermisible( medidaDesviacionEje1,revision);
			String permisible = String.format( "[ %s, %s ]", permisibleDTO.getMinimo(),permisibleDTO.getMaximo());
			permisiblesDesviacionDTO.setPermisibleDesviacionEje1(permisible);
			permisiblesDesviacionDTO.setPermisibleDesviacionEje2(permisible);
			permisiblesDesviacionDTO.setPermisibleDesviacionEje3(permisible);
			permisiblesDesviacionDTO.setPermisibleDesviacionEje4(permisible);
			permisiblesDesviacionDTO.setPermisibleDesviacionEje5(permisible);
		}
		return permisiblesDesviacionDTO;
	}
	
	public PermisibleTaximetroDTO generarPermisiblesTaximetros(List<Medida> medidas,Revision revision){
		PermisibleTaximetroDTO permisibleTaximetro = new PermisibleTaximetroDTO();
		
		Optional<Medida> medidaErrorDistancia = utilInformesMedidas.obtenerMedida( medidas.stream(), ConstantesMedidasTaximetro.ID_MEDIDA_ERROR_DISTANCIA);
		if(medidaErrorDistancia.isPresent()){
			MaximoMinimoPermisibleDTO permisibleDTO = formatearPermisible( medidaErrorDistancia, revision);
			String rangoDistancia = String.format("[%s,%s]", permisibleDTO.getMinimo() , permisibleDTO.getMaximo() );
			permisibleTaximetro.setPermisibleErrorDistancia(rangoDistancia);
		}
		Optional<Medida> medidaErrorTiempo = utilInformesMedidas.obtenerMedida(medidas.stream(), ConstantesMedidasTaximetro.ID_MEDIDA_ERROR_TIEMPO);
		if(medidaErrorTiempo.isPresent()){
			MaximoMinimoPermisibleDTO permisibleDTO = formatearPermisible(medidaErrorTiempo,revision);
			String rangoTiempo = String.format("[%s,%s]", permisibleDTO.getMinimo(), permisibleDTO.getMaximo() );
			permisibleTaximetro.setPermisibleErrorTiempo(rangoTiempo);
		}
		return permisibleTaximetro;
	}
	
	public PermisibleDieselDTO generarPermisibleDieselDTO(List<Medida> medidas,Revision revision){
		PermisibleDieselDTO permisibleDieselDTO = new PermisibleDieselDTO();
		Optional<Medida> resultadoDiesel = utilInformesMedidas.obtenerMedida(medidas.stream(), ConstantesMedidasGases.CODIGO_RESULTADO_OPACIDAD);
		if(resultadoDiesel.isPresent()){
			MaximoMinimoPermisibleDTO permisibleDTO = formatearPermisible(resultadoDiesel, revision);
			permisibleDieselDTO.setPermisibleDiesel(permisibleDTO.getMaximo());
		}
		Vehiculo vehiculo = revision.getVehiculo();
		
		Optional<Medida> resultadoDieselDensidad = utilInformesMedidas.obtenerMedida(medidas.stream(), ConstantesMedidasGases.CODIGO_RESULTADO_DENSIDAD_HUMO);
		if(vehiculo != null && resultadoDieselDensidad.isPresent()) {
			MaximoMinimoPermisibleDTO permisibleDTO = formatearPermisible(resultadoDieselDensidad,vehiculo);
			permisibleDieselDTO.setPermisibleDieselDensidadHumo(permisibleDTO.getMaximo());
		}	
		return permisibleDieselDTO;
	}
	


	public PermisiblesMotoDTO generarPermsibleMoto(List<Medida> medidas,Revision revision, Boolean isMotoDosTiempos){
		PermisiblesMotoDTO permisiblesMoto = new PermisiblesMotoDTO();
		int codigoMedidaHcRalenti = !isMotoDosTiempos ? ConstantesMedidasGases.CODIGO_MEDIDA_HC_RALENTI :ConstantesMedidasGases.CODIGO_MEDIDA_HC_RALENTI_2T;
		Optional<Medida> medidaHCRalenti = utilInformesMedidas.obtenerMedida(medidas.stream(), codigoMedidaHcRalenti );
		if(medidaHCRalenti.isPresent()){
			MaximoMinimoPermisibleDTO permisibleDTO = formatearPermisible(medidaHCRalenti,revision);
			permisiblesMoto.setPermisibleHC(permisibleDTO.getMaximo());
		}
		int codigoMedidaCo = !isMotoDosTiempos ? ConstantesMedidasGases.CODIGO_MEDIDA_CO_RALENTI : ConstantesMedidasGases.CODIGO_MEDIDA_CO_RALENTI_2T;
		Optional<Medida> medidaCORalenti = utilInformesMedidas.obtenerMedida(medidas.stream(), codigoMedidaCo);
		if(medidaCORalenti.isPresent()){
			MaximoMinimoPermisibleDTO permisibleDTO = formatearPermisible(medidaCORalenti,revision);
			permisiblesMoto.setPermisibleCO( permisibleDTO.getMaximo() );			
		}
		int codigoMedidaO2 = !isMotoDosTiempos ? ConstantesMedidasGases.CODIGO_MEDIDA_O2_RALENTI : ConstantesMedidasGases.CODIGO_MEDIDA_O2_RALENTI_2T;
		Optional<Medida> medidaO2Ralenti = utilInformesMedidas.obtenerMedida(medidas.stream(), codigoMedidaO2);
		if(medidaO2Ralenti.isPresent()){
			MaximoMinimoPermisibleDTO permisibleDTO = formatearPermisible(medidaO2Ralenti,revision);
			permisiblesMoto.setPermisibleO2( permisibleDTO.getMaximo() );
		}
		return permisiblesMoto;
	}
	
	public PermisibleOttoVehiculoDTO generarPermisibleOttoVehiculo(List<Medida> medidas,Revision revision){
		PermisibleOttoVehiculoDTO permisibleOttoVehiculo = new PermisibleOttoVehiculoDTO();
		Optional<Medida> medidaHCRalenti = utilInformesMedidas.obtenerMedida(medidas.stream(), ConstantesMedidasGases.CODIGO_MEDIDA_HC_RALENTI);
		if(medidaHCRalenti.isPresent()){
			MaximoMinimoPermisibleDTO permisibleDTO = formatearPermisible(medidaHCRalenti,revision);
			permisibleOttoVehiculo.setPermisibleHcRalenti(permisibleDTO.getMaximo());
		}
		Optional<Medida> medidaCORalenti = utilInformesMedidas.obtenerMedida(medidas.stream(), ConstantesMedidasGases.CODIGO_MEDIDA_CO_RALENTI);
		if(medidaCORalenti.isPresent()){
			MaximoMinimoPermisibleDTO permisibleDTO = formatearPermisible(medidaCORalenti,revision);
			permisibleOttoVehiculo.setPermisibleCoRalenti(permisibleDTO.getMaximo());
		}
		Optional<Medida> medidaCO2Ralenti = utilInformesMedidas.obtenerMedida(medidas.stream(), ConstantesMedidasGases.CODIGO_MEDIDA_CO2_RALENTI);
		if(medidaCO2Ralenti.isPresent()){
			MaximoMinimoPermisibleDTO permisibleDTO = formatearPermisible(medidaCO2Ralenti,revision);
			String per = String.format("[%s,%s]", permisibleDTO.getMinimo(),permisibleDTO.getMaximo());
			permisibleOttoVehiculo.setPermisibleCo2Ralenti(per);
		}
		Optional<Medida> medidaO2Ralenti = utilInformesMedidas.obtenerMedida(medidas.stream(), ConstantesMedidasGases.CODIGO_MEDIDA_O2_RALENTI);
		if(medidaO2Ralenti.isPresent()){
			MaximoMinimoPermisibleDTO permisibleDTO = formatearPermisible(medidaO2Ralenti,revision);
			String per = String.format("[%s,%s]", permisibleDTO.getMinimo(),permisibleDTO.getMaximo());
			permisibleOttoVehiculo.setPermisibleO2Ralenti(per);
		}
		
		//crucero
		Optional<Medida> medidaHCCrucero = utilInformesMedidas.obtenerMedida(medidas.stream(), ConstantesMedidasGases.CODIGO_MEDIDA_HC_CRUCERO);
		if(medidaHCCrucero.isPresent()){
			MaximoMinimoPermisibleDTO permisibleDTO = formatearPermisible(medidaHCCrucero,revision);
			permisibleOttoVehiculo.setPermisibleHcCrucero(permisibleDTO.getMaximo());			
		}
		Optional<Medida> medidaCOCrucero = utilInformesMedidas.obtenerMedida(medidas.stream(), ConstantesMedidasGases.CODIGO_MEDIDA_CO_CRUCERO);
		if(medidaCOCrucero.isPresent()){
			MaximoMinimoPermisibleDTO permisibleDTO = formatearPermisible(medidaCOCrucero,revision);
			permisibleOttoVehiculo.setPermisibleCoCrucero(permisibleDTO.getMaximo());
		}
		Optional<Medida> medidaCO2Crucero = utilInformesMedidas.obtenerMedida(medidas.stream(), ConstantesMedidasGases.CODIGO_MEDIDA_CO2_CRUCERO);
		if(medidaCO2Crucero.isPresent()){
			MaximoMinimoPermisibleDTO permisibleDTO = formatearPermisible(medidaCO2Crucero,revision);
			String per = String.format("[%s,%s]", permisibleDTO.getMinimo(),permisibleDTO.getMaximo());
			permisibleOttoVehiculo.setPermisibleCo2Crucero( per );
		}
		Optional<Medida> medidaO2Crucero = utilInformesMedidas.obtenerMedida(medidas.stream(), ConstantesMedidasGases.CODIGO_MEDIDA_O2_CRUCERO);
		if(medidaO2Crucero.isPresent()){
			MaximoMinimoPermisibleDTO permisibleDTO = formatearPermisible(medidaO2Crucero,revision);
			String per = String.format("[%s,%s]", permisibleDTO.getMinimo(),permisibleDTO.getMaximo());
			permisibleOttoVehiculo.setPermisibleO2Crucero(per);
		}
		return permisibleOttoVehiculo;
	}
	
	public PermisiblesFrenosDTO generarPermisibleFrenos(List<Medida> medidas,Revision revision,Boolean isMoto){
		PermisiblesFrenosDTO permisiblesFrenosDTO = new PermisiblesFrenosDTO();
		if( isMoto ){
			Optional<Medida> medidaEficaciaTotal = utilInformesMedidas.obtenerMedida(medidas.stream(), ConstantesMedidasFrenos.EFICACIA_FRENADO_TOTAL);
			if(medidaEficaciaTotal.isPresent()){
				MaximoMinimoPermisibleDTO permisibleDTO = formatearPermisible(medidaEficaciaTotal,revision);
				permisiblesFrenosDTO.setPermisibleEficaciaTotal(permisibleDTO.getMinimo());
			}
			
		}else {
			Optional<Medida> medidaEficaciaTotal = utilInformesMedidas.obtenerMedida(medidas.stream(), ConstantesMedidasFrenos.EFICACIA_FRENADO_TOTAL);
			if( medidaEficaciaTotal.isPresent() ){
				MaximoMinimoPermisibleDTO permisibleDTO = formatearPermisible(medidaEficaciaTotal,revision);
				permisiblesFrenosDTO.setPermisibleEficaciaTotal(permisibleDTO.getMinimo());
			}
			Optional<Medida> medidaEficaciaAuxiliar = utilInformesMedidas.obtenerMedida(medidas.stream(), ConstantesMedidasFrenos.EFICACIA_FRENADO_AUXILIAR);
			if( medidaEficaciaAuxiliar.isPresent() ){
				MaximoMinimoPermisibleDTO permisibleDTO = formatearPermisible(medidaEficaciaAuxiliar,revision);
				permisiblesFrenosDTO.setPermisibleEficaciaAuxiliar(permisibleDTO.getMinimo());
			}
			Optional<Medida> medidaDesequilibrioEje1 = utilInformesMedidas.obtenerMedida(medidas.stream(), ConstantesMedidasFrenos.DESEQUILIBRIO_EJE_UNO);
			if(medidaDesequilibrioEje1.isPresent()){
				MaximoMinimoPermisibleDTO permisibleDTO = formatearPermisible(medidaDesequilibrioEje1,revision);
				permisiblesFrenosDTO.setPermisibleDesequilibrioEje1(permisibleDTO.getMaximo());
				permisiblesFrenosDTO.setPermisibleDesequilibrioEje2(permisibleDTO.getMaximo());
				permisiblesFrenosDTO.setPermisibleDesequilibrioEje3(permisibleDTO.getMaximo());
				permisiblesFrenosDTO.setPermisibleDesequilibrioEje4(permisibleDTO.getMaximo());
				permisiblesFrenosDTO.setPermisibleDesequilibrioEje5(permisibleDTO.getMaximo());
			}
			
		}
		return permisiblesFrenosDTO;
	}

}
