package com.proambiente.webapp.service.generadordto;

public class MaximoMinimoPermisibleDTO {

	private String minimo;
	private String maximo;

	public MaximoMinimoPermisibleDTO(String minimo, String maximo) {
		super();
		this.minimo = minimo;
		this.maximo = maximo;
	}

	public String getMinimo() {
		return minimo;
	}

	public void setMinimo(String minimo) {
		this.minimo = minimo;
	}

	public String getMaximo() {
		return maximo;
	}

	public void setMaximo(String maximo) {
		this.maximo = maximo;
	}

}