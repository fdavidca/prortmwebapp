package com.proambiente.webapp.service;

import java.io.Serializable;
import java.sql.Array;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.sql.DataSource;

@Named
public class DefectosInspeccionSensorialMotosService implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3567079198270784595L;
	
	private static final Logger logger = Logger.getLogger(ServletReporte.class
			.getName());
	
	@Resource(lookup = "java:/jboss/datasources/PrortmDS")
	DataSource dataSource;
	
	public Integer[] consultarDefectosInspeccionMotos(){
		
		Connection cn = null;
		try{
			cn = dataSource.getConnection();
			String sql = "SELECT defectos_inspeccion_previa FROM config_gases_moto WHERE config_gases_moto_id = 1";
			PreparedStatement ps = cn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			rs.next();
			Array codigosArr = rs.getArray(1);
			Integer[] codigos = (Integer[]) codigosArr.getArray(); 
			return codigos;
		}catch(Exception exc) {
			logger.log(Level.SEVERE, "Error cargando el reporte", exc);
			throw new RuntimeException(" No se pueden consultar codigos de defectos");
		}finally {
			if(cn != null) {
				try {
					cn.close();
				}catch(Exception exc) {
					
				}
			}
		}
	   	
	}

}