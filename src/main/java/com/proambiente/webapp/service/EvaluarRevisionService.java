package com.proambiente.webapp.service;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

@Stateless
public class EvaluarRevisionService {
	
	@PersistenceContext 
	EntityManager em;
	
	public enum EstadoRevision  { APROBADA,REPROBADA,NO_FINALIZADA };
	
	/**
	 * El procedimiento almacenado debe retornar "aprobada","reprobada" o "no_finalizada"
	 * @param revisionId
	 * @return
	 */
	
	public EstadoRevision obtenerEstadoRevision(int revisionId) {
		
			String evaluacionRevision = evaluarRevision(revisionId);		
			switch(evaluacionRevision) {
			
			case "aprobada":
				
				return EstadoRevision.APROBADA;
			case "reprobada":
				return EstadoRevision.REPROBADA;
			case "no_finalizada":
				return EstadoRevision.NO_FINALIZADA; 
			default:
				throw new RuntimeException("Error, revision en un estado no programado: " + evaluacionRevision);
			
			}
		
	}
	
	private String evaluarRevision(Integer revisionId) {
		StoredProcedureQuery storedProcedureQuery = em.createStoredProcedureQuery("evaluar_revision");
		storedProcedureQuery.registerStoredProcedureParameter(1, Integer.class, ParameterMode.IN);
		storedProcedureQuery.setParameter(1, revisionId);
		// storedProcedureQuery.execute();
		Object resultado = storedProcedureQuery.getSingleResult();
		String strResultado = (String) resultado;
		return strResultado;
	}


}
