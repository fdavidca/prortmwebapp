package com.proambiente.webapp.service;

import java.util.Optional;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Revision_;

@Stateless
public class RevisionIdPorConsecutivoRuntService {
	
	@PersistenceContext
	EntityManager em;
	
	public Optional<Integer> buscarRevisionIdPorConsecutivoRuntService(String consecutivoRunt) {
		
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Integer> cq = cb.createQuery(Integer.class);
		Root<Revision> root = cq.from(Revision.class);
		cq.select( root.get(Revision_.revisionId)).where( cb.equal(root.get(Revision_.consecutivoRunt), consecutivoRunt ) ) ;
		
		TypedQuery<Integer> query = em.createQuery(cq);
		Optional<Integer> first = query.getResultList().stream().findFirst();
		return first;
	}

}
