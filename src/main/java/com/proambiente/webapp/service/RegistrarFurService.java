package com.proambiente.webapp.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;


import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.io.IOUtils;

import com.proambiente.modelo.RegistroFur;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Revision_;
import com.proambiente.webapp.dao.RegistroFurFacade;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;

@Named
public class RegistrarFurService implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1568217456L;

	@Inject
	ReporteService reporteService;
	
	@Inject
	RegistroFurFacade regFacade;
	
	@Inject
	RevisionService revisionService;

	@Inject
	VehiculoService vehiculoService;
	
	public JasperPrint reportePorRevisionId(Integer revisionId) {
		JasperPrint reporte = reporteService.cargarReporteRevision(revisionId);
		return reporte;
	}
	
	public void guardarPdf(String placa,Integer revisionId,String descripcion,Integer intento) {
		JasperPrint reporte = reportePorRevisionId(revisionId);
		Long revisionIdLong = revisionId * 1L;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		JRPdfExporter exporter = new JRPdfExporter();
        exporter.setExporterInput( new SimpleExporterInput( reporte ));
        exporter.setExporterOutput( new SimpleOutputStreamExporterOutput( out ));
        ByteArrayInputStream bis = null;
        try {
			exporter.exportReport();			
			bis = new ByteArrayInputStream(out.toByteArray());
			byte[] bytes = IOUtils.toByteArray(bis);
			RegistroFur regFur = new RegistroFur();
			regFur.setPlaca(placa);
			regFur.setRevisionId(revisionIdLong);
			regFur.setIntento(intento);
			regFur.setDescripcion(descripcion);
			regFur.setPdf(bytes);
			
			regFacade.create(regFur);
		} catch (JRException e) {
			e.printStackTrace();
			throw new RuntimeException("Error generando pdf",e);
			
		} catch (IOException e) {
			
			e.printStackTrace();
		}finally {
			if (bis != null)
				try {
					bis.close();
				} catch (IOException e) {					
					e.printStackTrace();
				}
			if(out != null) {
				try {
					out.close();
				}catch (IOException e) {					
					e.printStackTrace();
				}
			}
		}
	}
	
	public void guardarPdfPrimerEnvio(int revisionId) {
		
		//consultar la placa
		String placa = vehiculoService.placaDesdeRevisionId(revisionId);
		Revision rev = revisionService.cargarRevisionDTO(revisionId, Revision_.numeroInspecciones);
		Integer intento = rev.getNumeroInspecciones();
		guardarPdf(placa, revisionId, "Segundo envio , intento " + intento, intento);
		//consultar el intento
		
		
	}

}
