package com.proambiente.webapp.service;

import java.io.InputStream;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.proambiente.modelo.Ciudad;
import com.proambiente.modelo.ClaseVehiculo;
import com.proambiente.modelo.LineaVehiculo;
import com.proambiente.modelo.Marca;
import com.proambiente.modelo.Propietario;
import com.proambiente.modelo.Servicio;
import com.proambiente.modelo.Usuario;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.webapp.controller.VehiculoDefault;
import com.proambiente.webapp.dao.CiudadFacade;
import com.proambiente.webapp.dao.ClaseVehiculoFacade;
import com.proambiente.webapp.dao.LineaVehiculoFacade;
import com.proambiente.webapp.dao.MarcaFacade;
import com.proambiente.webapp.dao.PropietarioFacade;
import com.proambiente.webapp.dao.ServicioFacade;
import com.proambiente.webapp.dao.VehiculoFacade;
import com.proambiente.webapp.service.exception.CeldaNullException;

public class ImportarDesdeExcelService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3388214616555259255L;

	@Inject
	MarcaFacade marcaDAO;

	@Inject
	LineaVehiculoFacade lineaVehiculoDAO;

	@Inject
	VehiculoFacade vehiculoDAO;

	@Inject
	@VehiculoDefault
	Vehiculo vehiculoPorDefecto;

	@Inject
	ClaseVehiculoFacade claseDAO;

	@Inject
	ServicioFacade servicioDAO;

	@Inject
	CiudadFacade ciudadDAO;

	@Inject
	PropietarioFacade propietarioFacade;

	Ciudad ciudadPorDefecto;

	Propietario propietarioPorDefecto;

	@PostConstruct
	public void init() {
		try {
			ciudadPorDefecto = ciudadDAO.find(11001);
			propietarioPorDefecto = propietarioFacade.find(1L);
		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Error init ImportarDeseExcelService", exc);
		}
	}

	private static final Logger logger = Logger.getLogger(ImportarDesdeExcelService.class.getName());

	public List<Vehiculo> obtenerVehiculosDesdeArchivo(InputStream inputStream) throws Exception {

		List<Vehiculo> vehiculosCargados = new ArrayList<>();
		try {
			XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
			XSSFSheet sheet = workbook.getSheetAt(0);
			Iterator<Row> rowIterator = sheet.iterator();
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				Optional<Vehiculo> optVehiculo = construirVehiculo(row);
				if (optVehiculo.isPresent()) {
					vehiculosCargados.add(optVehiculo.get());
				}
			}
		} catch (CeldaNullException cexc) {
			logger.info("Se ha llegado a celda nula " + cexc.getMessage());
		} finally {
			inputStream.close();
		}

		return vehiculosCargados;
	}

	public void guardarVehiculos(List<Vehiculo> vehiculos, Usuario usuarioQueCrea) {
		for (Vehiculo v : vehiculos) {
			try {
				logger.info("Creando vehículo de placa " + v.getPlaca());
				v.setUsuario(usuarioQueCrea);
				Propietario propietario = v.getPropietario();
				propietario.setUsuario(usuarioQueCrea);
				propietarioFacade.edit(propietario);
				v.setPropietario(propietario);
				v.setConductor(propietario);
				vehiculoDAO.create(v);
				logger.info("Vehículo creado placa " + v.getPlaca());
			} catch (Exception exc) {
				logger.log(Level.SEVERE, "Error creando vehiculo placa : " + v.getPlaca(), exc);
			}
		}
	}

	public Optional<Vehiculo> construirVehiculo(Row rowParam) throws CeldaNullException {
		Vehiculo v = clonarVehiculoConValoresPorDefecto();

		Row row = rowParam;
		if (row == null) {
			return Optional.empty();
		}
		Cell cell = null;
		DataFormatter formatter = new DataFormatter(); // creating formatter
														// using the default
														// locale
		StringBuilder sb = new StringBuilder();

		// obtener la placa
		String placa = leerPlacaDeCelda(row, formatter, sb);
		sb.append("\nPLACA: ").append(placa).append("\n");

		// obtener el vin
		String vin = "";
		vin = leerVinDeCelda(row, formatter, sb);
		sb.append("VIN: ").append(vin).append("\n");

		if (placa.isEmpty() && vin.isEmpty()) {
			logger.info("La fila no contiene información de vehículo");
			return Optional.empty();
		} else {
			logger.info(sb.toString());
		}

		if (placa.equals("PLACA")) {
			logger.info("La fila no contiene información de vehículo");
			return Optional.empty();
		}

		if (isPlacaExistente(placa)) {
			return Optional.empty();
		}

		v.setPlaca(placa);

		v.setVin(vin);

		String marca = "";
		marca = leerMarcaDesdeCelda(row, formatter, sb);
		sb.append("MARCA : ").append(marca).append("\n");

		String linea = leerLineaDesdeCelda(row, formatter, sb);
		sb.append("LINEA: ").append(linea).append("\n");
		Boolean lineaEncontrada = false;

		if (!marca.isEmpty()) {
			Optional<Marca> optMarca = buscarMarcaPorId(marca);
			if (optMarca.isPresent()) {
				Marca m = optMarca.get();
				v.setMarca(m);
				Optional<LineaVehiculo> optLinea = buscarLineaVehiculoSabiendoMarca(linea, m);
				if (optLinea.isPresent()) {
					v.setLineaVehiculo(optLinea.get());
					lineaEncontrada = true;
				}
			}
		}

		if (!linea.isEmpty()) {
			if (!lineaEncontrada) {
				Optional<LineaVehiculo> optLinea = buscarLineaVehiculoPorId(linea);
				if (optLinea.isPresent()) {
					LineaVehiculo lineaVehiculo = optLinea.get();
					v.setLineaVehiculo(lineaVehiculo);
					Marca m = lineaVehiculo.getMarca();
					if (m != null) {
						v.setMarca(m);
					}
				}
			}
		}

		double modelo = 0;
		cell = row.getCell(2);
		if (celdaEsTipoNumerico(cell).isPresent()) {
			if (celdaEsTipoNumerico(cell).get().equals(Boolean.TRUE)) {
				modelo = row.getCell(2).getNumericCellValue();
			} else {
				sb.append("Celda que contiene modelo no es numerica \n");
				try {
					String modeloStr = formatter.formatCellValue(row.getCell(2));
					modelo = Double.valueOf(modeloStr);
				} catch (NumberFormatException nexc) {
					sb.append("El modelo no corresponde con un número");
				}
			}
		} else if (cell == null) {
			throw new CeldaNullException(row.getRowNum(), 2);
		}

		sb.append("MODELO: ").append(modelo).append("\n");
		v.setModelo((int) modelo);

		double cilindraje = 0;
		cell = row.getCell(4);
		if (celdaEsTipoNumerico(cell).isPresent()) {
			if (celdaEsTipoNumerico(cell).get().equals(Boolean.TRUE)) {
				cilindraje = row.getCell(4).getNumericCellValue();
			} else {
				sb.append("Celda que contiene el cilindraje no es numerica \n");
				try {
					String cilindrajeStr = formatter.formatCellValue(row.getCell(4));
					cilindraje = Double.valueOf(cilindrajeStr);
				} catch (NumberFormatException nexc) {
					sb.append("El cilindraje no corresponde a un numero");
				}
			}
		} else if (cell == null) {
			throw new CeldaNullException(row.getRowNum(), 4);
		}

		sb.append("CILINDRAJE : ").append(cilindraje);
		v.setCilindraje((int) cilindraje);

		String clase = formatter.formatCellValue(row.getCell(5));
		sb.append("CLASE: ").append(clase);

		if (!clase.isEmpty()) {
			Optional<ClaseVehiculo> optClase = buscarClaseVehiculoPorId(clase);
			if (optClase.isPresent()) {
				v.setClaseVehiculo(optClase.get());
			}
		}

		String servicio = formatter.formatCellValue(row.getCell(6));
		sb.append("SERVICIO: ").append(servicio);
		
		
		if (!servicio.isEmpty()) {
			int servicioId = -1;
			try {
				servicioId = Integer.valueOf(servicio);
			}catch(NumberFormatException exc) {
				servicioId = 1;
			}
			Servicio servicioEntity = servicioDAO.find(servicioId);
			if(servicioEntity != null) {
				v.setServicio(servicioEntity);
			}
			
		}

		String motor = formatter.formatCellValue(row.getCell(7));
		sb.append("MOTOR: ").append(motor).append("\n");
		v.setNumeroMotor(motor);

		String licencia = formatter.formatCellValue(row.getCell(9));
		sb.append("LICENCIA").append(licencia);
		v.setNumeroLicencia(licencia);


		Optional<Propietario> optPropietario = leerPropietarioDesdeFila(rowParam, formatter);
		v.setPropietario(optPropietario.orElse(propietarioPorDefecto));

		return Optional.of(v);

	}

	private Optional<Propietario> leerPropietarioDesdeFila(Row row, DataFormatter formatter) {

		String numeroDocumentoStr = formatter.formatCellValue(row.getCell(13));
		Boolean identificacionPropietarioValida;
		Long numeroDocumento = null;
		try {
			numeroDocumento = Long.valueOf(numeroDocumentoStr);

			identificacionPropietarioValida = true;
		} catch (NumberFormatException nexc) {
			identificacionPropietarioValida = false;
		}
		if (!identificacionPropietarioValida) {
			return Optional.empty();
		}
		// si la identificación del propietario es valida
		
		String tipoDocumento = formatter.formatCellValue(row.getCell(12));

		Propietario propietario = new Propietario();
		propietario.setTipoIdentificacion(tipoDocumento);

		propietario.setPropietarioId(numeroDocumento);

		String nombres = formatter.formatCellValue(row.getCell(11));
		propietario.setNombres(nombres);

		String direccionStr = formatter.formatCellValue(row.getCell(14));
		propietario.setDireccion(direccionStr);

		String telefono = formatter.formatCellValue(row.getCell(16));
		propietario.setTelefono1(telefono);

		String ciudadStr = formatter.formatCellValue(row.getCell(17));
		Optional<Ciudad> optCiudad = ciudadDAO.buscarCiudadPorNombre(ciudadStr);
		propietario.setCiudad(optCiudad.orElse(ciudadPorDefecto));
		return Optional.of(propietario);
	}

	private Vehiculo clonarVehiculoConValoresPorDefecto() {
		Vehiculo v = new Vehiculo();
		v.setAseguradora(vehiculoPorDefecto.getAseguradora());
		v.setBlindaje(false);
		v.setCilindraje(0);
		v.setClaseVehiculo(vehiculoPorDefecto.getClaseVehiculo());
		v.setColor(vehiculoPorDefecto.getColor());
		v.setDiametroExosto(0);
		v.setDiseno(vehiculoPorDefecto.getDiseno());		
		v.setFechaMatricula(vehiculoPorDefecto.getFechaMatricula());
		v.setFechaSoat(new Timestamp(new Date().getTime()));
		v.setFechaExpiracionSoat(new Timestamp(new Date().getTime()));
		v.setKilometraje("0");
		v.setLineaVehiculo(vehiculoPorDefecto.getLineaVehiculo());
		v.setLlanta(vehiculoPorDefecto.getLlanta());		
		v.setMarca(vehiculoPorDefecto.getMarca());
		v.setModelo(0);		
		v.setNacionalidad(vehiculoPorDefecto.getNacionalidad());
		v.setNumeroEjes(2);
		v.setNumeroLicencia("");
		v.setNumeroExostos(1);
		v.setNumeroMotor("");
		v.setNumeroSerie("");
		v.setNumeroSillas(1);
		v.setNumeroSoat("");
		v.setPais(vehiculoPorDefecto.getPais());
		v.setPlaca("");		
		v.setPropietario(vehiculoPorDefecto.getPropietario());
		v.setServicio(vehiculoPorDefecto.getServicio());
		v.setServicioEspecial(vehiculoPorDefecto.getServicioEspecial());
		v.setTiemposMotor(4);
		v.setTipoCombustible(vehiculoPorDefecto.getTipoCombustible());
		v.setTipoVehiculo(vehiculoPorDefecto.getTipoVehiculo());
		v.setUsuario(vehiculoPorDefecto.getUsuario());
		v.setVelocidad(0);
		v.setVidriosPolarizados(false);
		v.setVin("");
		return v;
	}

	private String leerLineaDesdeCelda(Row row, DataFormatter formatter, StringBuilder sb) throws CeldaNullException {
		String linea = "";
		Cell cell;
		cell = row.getCell(1);
		if (celdaEsTipoString(cell).isPresent()) {

			if (celdaEsTipoString(cell).get().equals(Boolean.TRUE)) {

				linea = row.getCell(1).getStringCellValue();

			} else {

				sb.append("Celda que contiene linea no es tipo string \n");
				linea = formatter.formatCellValue(row.getCell(1));
			}
		} else if (cell == null) {

			throw new CeldaNullException(row.getRowNum(), 1);
		}
		return linea;
	}

	private Optional<ClaseVehiculo> buscarClaseVehiculoPorId(String nombreClase) {
		Integer claseId = -1;
		try {
			claseId = Integer.valueOf(nombreClase);
		}catch(NumberFormatException nexc) {
			return Optional.empty();
		}
		ClaseVehiculo clase = claseDAO.find(claseId);
		if(clase == null) {
			return Optional.empty();
		}else {
			return Optional.of(clase);
		}		
	}

	private Optional<Marca> buscarMarcaPorId(String marca) {
		Integer marcaId = -1;
		try {
			marcaId = Integer.valueOf(marca);
		}catch(NumberFormatException nexc) {
			return Optional.empty();
		}		
		Marca m = marcaDAO.find(marcaId);
		if (m == null ) {
			return Optional.empty();
		} else {
			return Optional.of(m);
		}
	}

	private Optional<LineaVehiculo> buscarLineaVehiculoPorId(String linea) {

		Integer lineaId = -1;
		try {
			lineaId = Integer.valueOf(linea);
		}catch(NumberFormatException nexc) {
			return Optional.empty();
		}
		
		LineaVehiculo lineaEntidad = lineaVehiculoDAO.find(lineaId);
		if (lineaEntidad == null ) {
			return Optional.empty();
		} else {
			return Optional.of(lineaEntidad);
		}
	}

	private Optional<LineaVehiculo> buscarLineaVehiculoSabiendoMarca(String linea, Marca marca) {
		List<LineaVehiculo> lineas = marcaDAO.findLineaVehiculosByMarca(marca);
		return lineas.stream().filter(l -> l.getCodigoLinea().toString().equalsIgnoreCase(linea)).findAny();

	}

	private String leerMarcaDesdeCelda(Row row, DataFormatter formatter, StringBuilder sb) throws CeldaNullException {
		String marca = "";
		Cell cell;
		cell = row.getCell(0);
		if (celdaEsTipoString(cell).isPresent()) {
			if (celdaEsTipoString(cell).get().equals(Boolean.TRUE)) {
				marca = row.getCell(0).getStringCellValue();
			} else {
				sb.append("Celda de marca no es tipo string \n");
				marca = formatter.formatCellValue(row.getCell(0));
			}
		} else if (cell == null) {
			throw new CeldaNullException(row.getRowNum(), 0);
		}
		return marca;
	}

	private String leerVinDeCelda(Row row, DataFormatter formatter, StringBuilder sb) throws CeldaNullException {
		String vin = "";
		Cell cell = row.getCell(8);
		if (celdaEsTipoString(cell).isPresent()) {
			if (celdaEsTipoString(cell).get().equals(Boolean.TRUE)) {
				vin = row.getCell(8).getStringCellValue();
			} else {
				sb.append("La celda VIN no es de tipo texto-cadena").append("\n");
				vin = formatter.formatCellValue(cell);
			}
		} else if (cell == null) {
			throw new CeldaNullException(row.getRowNum(), 8);
		}
		return vin;
	}

	public Boolean isPlacaExistente(String placa) {
		Vehiculo vehiculo = vehiculoDAO.findVehicuoByPlaca(placa);
		if (vehiculo != null && vehiculo.getVehiculoId() != null) {
			return true;
		} else {
			return false;
		}
	}

	public Boolean isVINExistente(String vin) {
		Vehiculo vehiculo = vehiculoDAO.findVehicuoByVIN(vin);
		if (vehiculo != null && vehiculo.getVehiculoId() != null) {
			return true;
		} else {
			return false;
		}

	}

	private String leerPlacaDeCelda(Row row, DataFormatter formatter, StringBuilder sb) throws CeldaNullException {
		String placa = "";
		if (celdaEsTipoString(row.getCell(1)).isPresent()) {
			if (celdaEsTipoString(row.getCell(1)).get() == Boolean.TRUE) {
				placa = row.getCell(3).getStringCellValue();
			} else {
				sb.append("Tipo de celda de placa no es cadena-string").append("\n");
				placa = formatter.formatCellValue(row.getCell(3));
			}
		} else if (row.getCell(1) == null) {
			throw new CeldaNullException(row.getRowNum(), 1);
		}
		return placa.trim().toUpperCase();
	}

	private Optional<Boolean> celdaEsTipoString(Cell cell) {
		if (cell == null) {
			return Optional.empty();
		} else {
			return Optional.of(cell.getCellType() == CellType.STRING);
		}
	}

	private Optional<Boolean> celdaEsTipoNumerico(Cell cell) {
		if (cell == null) {
			return Optional.empty();
		} else {
			return Optional.of(cell.getCellType() == CellType.NUMERIC);
		}
	}

}
