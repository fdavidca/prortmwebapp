package com.proambiente.webapp.service;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
public class PruebaFinalizadaAprobadaService {

	@PersistenceContext
	EntityManager em;

	
	public Boolean existePruebaFinalizadaAprobada(Integer revisionId,Integer tipoPruebaId) {
		if(revisionId == null) {
			throw new RuntimeException("El argumento revisionId pasado al metodo existePruebaFinalizadaAprobada es null");
		}
		if(tipoPruebaId == null) {
			throw new RuntimeException("El argumento tipoPruebaId pasado al metodo existePruebaFinalizadaAprobada es null");
		}
		if(revisionId <0) {
			throw new RuntimeException("El argumento revisionId pasado al metrodo existePruebaFinalizadaAprobada es menor a 0");
		}
		if(tipoPruebaId<0) {
			throw new RuntimeException("El argumento tipoPruebaId pasado al metodo existePruebaFinalizadaAprobada es menor a 0");
		}
		String consulta = "SELECT COUNT(p) FROM Prueba p "
				+ "	WHERE p IN (SELECT MAX(pr) FROM Prueba pr "
				+ " WHERE pr.revision.revisionId =:revisionId AND pr.tipoPrueba.tipoPruebaId =:tipoPruebaId ) and p.finalizada = true and p.aprobada = true";
		Query query = em.createQuery(consulta);
		query.setParameter("revisionId", revisionId);
		query.setParameter("tipoPruebaId", tipoPruebaId);
		Long numeroPruebas = (Long)query.getSingleResult();
		return numeroPruebas>0;
		
		
	}
}
