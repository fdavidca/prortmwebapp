package com.proambiente.webapp.service;

import static com.proambiente.webapp.util.ConstantesTiposPrueba.TIPO_PRUEBA_DESVIACION;
import static com.proambiente.webapp.util.ConstantesTiposPrueba.TIPO_PRUEBA_FRENOS;
import static com.proambiente.webapp.util.ConstantesTiposPrueba.TIPO_PRUEBA_GASES;
import static com.proambiente.webapp.util.ConstantesTiposPrueba.TIPO_PRUEBA_INSPECCION_SENSORIAL;
import static com.proambiente.webapp.util.ConstantesTiposPrueba.TIPO_PRUEBA_LUCES;
import static com.proambiente.webapp.util.ConstantesTiposPrueba.TIPO_PRUEBA_RUIDO;
import static com.proambiente.webapp.util.ConstantesTiposPrueba.TIPO_PRUEBA_SUSPENSION;
import static com.proambiente.webapp.util.ConstantesTiposPrueba.TIPO_PRUEBA_TAXIMETRO;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.inject.Inject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.proambiente.modelo.Defecto;
import com.proambiente.modelo.DefectoAsociado;
import com.proambiente.modelo.Equipo;
import com.proambiente.modelo.EstadoDefecto;
import com.proambiente.modelo.EventoPrueba;
import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.Medida;
import com.proambiente.modelo.Pista;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.RegistroAuditoria;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.TipoOperacion;
import com.proambiente.modelo.TipoPrueba;
import com.proambiente.modelo.Usuario;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.modelo.dto.DefectoDTO;
import com.proambiente.modelo.dto.MedidaDTO;
import com.proambiente.modelo.dto.ReportePruebaDTO;
import com.proambiente.modelo.dto.auditoria.LogAuditoriaSuspension;
import com.proambiente.modelo.dto.auditoria.LogAuditoriaTaximetro;
import com.proambiente.modelo.dto.auditoria.SonometriaLog;
import com.proambiente.webapp.dao.AuditoriaSicovFacade;
import com.proambiente.webapp.dao.DefectoAsociadoFacade;
import com.proambiente.webapp.dao.DefectoFacade;
import com.proambiente.webapp.dao.EquipoFacade;
import com.proambiente.webapp.dao.InformacionCdaFacade;
import com.proambiente.webapp.dao.MedidaFacade;
import com.proambiente.webapp.dao.PistaFacade;
import com.proambiente.webapp.dao.PruebaFacade;
import com.proambiente.webapp.dao.UsuarioFacade;
import com.proambiente.webapp.service.generadordto.GeneradorDTOAuditoria;
import com.proambiente.webapp.util.ConstantesIndra;
import com.proambiente.webapp.util.ConstantesMedidasPresionLlanta;
import com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado;
import com.proambiente.webapp.util.dto.logsicov.LogAlineacionSicovDTO;
import com.proambiente.webapp.util.dto.logsicov.LogFrenosSicovDTO;
import com.proambiente.webapp.util.dto.logsicov.LogGasesSicovDTO;
import com.proambiente.webapp.util.dto.logsicov.LogInspeccionSensorialSicovDTO;
import com.proambiente.webapp.util.dto.logsicov.LogLlantasSicovDTO;
import com.proambiente.webapp.util.dto.logsicov.LogLucesSicovDTO;

@Stateless
public class RegistrarPruebaRealizadaService {
	
	
	private static final Logger logger = Logger.getLogger(RegistrarPruebaRealizadaService.class.getName());
	
	
	ObjectMapper objectMapper = new ObjectMapper();
	
	@Inject
	VehiculoService vehiculoService;

	@Inject
	PruebaFacade pruebaDAO;
	
	@Inject
	PistaFacade pistaFacade;
	
	@Inject
	DefectoFacade defectoDAO;
	
	@Inject
	DefectoAsociadoFacade defectoAsociadoDAO;
	
	@Inject
	MedidaFacade medidaDAO;
	
	
	@Inject
	RevisionService revisionService;
	
	@Inject
	UsuarioFacade usuarioDAO;
	
	@Inject
	EquipoFacade equipoDAO;
	
	@Inject
	InformacionCdaFacade informacionCdaDAO;
	
	@Inject
	AuditoriaSicovFacade auditoriaSicovDAO;
	
	
	InformacionCda informacionCda;
	
	@PostConstruct
	public void init(){
		informacionCda = informacionCdaDAO.find(1); 
	}
	
	
	GeneradorDTOAuditoria generadorDTOAuditoria = new GeneradorDTOAuditoria();
	
	
	public void registrarPruebaFinalizada(ReportePruebaDTO reportePruebaDTO,String ip){
			Prueba prueba = reportePruebaDTO.getPrueba();
			Integer usuarioId = reportePruebaDTO.getUsuarioId();
			Integer pistaId = reportePruebaDTO.getPistaId();
			
			//Chequear la informacion obligatoria
			if(usuarioId == null)
				throw new IllegalArgumentException("El reporte de prueba no incluye el identificador de usuario");
			if(pistaId == null)
				throw new IllegalArgumentException("El reporte de prueba no incluye el identificador de pista");
			
			if(prueba.getAnalizador() == null && prueba.getTipoPrueba().getTipoPruebaId() == 8)
				throw new IllegalArgumentException("Falta informacion de analizador para el registro de la prueba");
			
			if(prueba.getVerificacionGasolina() == null && prueba.getVerificacionLinealidad() == null && prueba.getTipoPrueba().getTipoPruebaId() == 8 ){
				throw new IllegalArgumentException("Falta informacion de verificacion de gasolina o de verificacion de linealidad");
			}
			
			List<MedidaDTO> medidas = reportePruebaDTO.getMedidas();
			if(medidas == null || medidas.isEmpty() && !(prueba.getTipoPrueba().getTipoPruebaId() == 5 || prueba.getTipoPrueba().getTipoPruebaId() == 1) )				
				logger.log(Level.INFO, "No se indicaron medidas para prueba " + prueba.getPruebaId());
			if(prueba.isAbortada() && prueba.isAprobada())
				throw new IllegalArgumentException("La prueba se ha enviado en un estado inconsistente, abortada y aprobada");
			
			List<DefectoDTO> defectos = reportePruebaDTO.getDefectos();

			if(reportePruebaDTO.isPresenciaDilucion() != null && reportePruebaDTO.isPresenciaDilucion()){
				prueba.setDilucion(true);
				String comentarioAnterior = prueba.getMotivoAborto() != null ? prueba.getMotivoAborto() :"";
				prueba.setMotivoAborto(comentarioAnterior + " DILUCION DE MUESTRA");
			}
			
			Usuario usuario = usuarioDAO.getReference(usuarioId);
			prueba.setUsuario(usuario);
			
			Pista pista = pistaFacade.find(pistaId);
			prueba.setPista(pista);
			
			Revision revision = revisionService.buscarRevisionPorPruebaId(prueba.getPruebaId());
			prueba.setRevision(revision);
			
			Vehiculo vehiculo = vehiculoService.vehiculoDesdeRevisionId(revision.getRevisionId());
			
			final List<Equipo> listaEquipos = new ArrayList<>();
			List<String> listaSeriales = reportePruebaDTO.getListaSerialesEquipo();
			if(listaSeriales != null){
				listaSeriales.stream().forEach(serial->{ 
						Equipo equipo = equipoDAO.find(serial);
						listaEquipos.add(equipo);
					
				});				
			}
			prueba.setEquipos(listaEquipos);
			Prueba pReference = pruebaDAO.find(prueba.getPruebaId());
			prueba.setFechaInicio( pReference.getFechaInicio() );
			if(prueba.getFechaFinalizacion() == null ){
				prueba.setFechaFinalizacion(new Timestamp(new Date().getTime()));
			}
			String comentariosAnteriores = pReference.getMotivoAborto();
			comentariosAnteriores = comentariosAnteriores != null ? comentariosAnteriores : "";
			System.out.println("motivo anterior" + comentariosAnteriores) ;
			String motivoAbortoActual = prueba.getMotivoAborto() != null ? prueba.getMotivoAborto() : " ";
			prueba.setMotivoAborto( motivoAbortoActual +" " + comentariosAnteriores);
			System.out.println("motivo actual: " + motivoAbortoActual);
			List<DefectoAsociado> defectosExistentes = 	pReference.getDefectosAsociados();
			if(defectosExistentes == null){
				defectosExistentes = new ArrayList<>();
			}
			if(defectos != null){
				for(DefectoDTO d:defectos){
		    		
		    		Defecto defecto = defectoDAO.find(d.getDefectoId());
		    		DefectoAsociado defectoAsociado = new DefectoAsociado();
		    		defectoAsociado.setDefecto(defecto);
		    		defectoAsociado.setPrueba(prueba);
		    		EstadoDefecto estado = EstadoDefecto.values()[d.getAsociacion()];
		    		defectoAsociado.setEstadoDefecto(estado);
		    		defectoAsociadoDAO.create(defectoAsociado);
		    		System.out.println("Defecto: " + d.getDefectoId()+" Prueba: " + d.getPruebaId());
		    		defectosExistentes.add(defectoAsociado)	;
		    		 
		    		    
		    	}
			}
			prueba.setDefectosAsociados(defectosExistentes);
			
			//if(defectos != null && !defectos.isEmpty())
				//defectoDAO.registrarDefectos(defectos, prueba);
			prueba = pruebaDAO.mergePrueba(prueba);	
			List<Medida> medidasGuardadas =  medidaDAO.guardarMedidas(medidas);
			try {
				List< Optional<RegistroAuditoria> > optRegistroList = generarLogResultado(prueba.getTipoPrueba(),medidasGuardadas,defectoAsociadoADefecto(defectosExistentes),prueba,vehiculo,revision);
				for( Optional<RegistroAuditoria> optRegistro : optRegistroList) {
					if(optRegistro.isPresent()){
						RegistroAuditoria registro = optRegistro.get();
						registro.setIpEquipoMedicion(ip);
						if(listaSeriales != null ){					
							registro.setSerialEquipoMedicion(listaSeriales.stream().collect(Collectors.joining("-")));
						}
						auditoriaSicovDAO.create(registro);
					}
				}
			} catch (JsonProcessingException e) {
				logger.log(Level.SEVERE, "Error registrando log de auditoria", e);
			}
			
			
			
	}
	
	//debe retornar una lista de los registros para la auditoria
	//
	private List<Optional<RegistroAuditoria>> generarLogResultado(TipoPrueba tipoPrueba,List<Medida> medidas,List<Defecto> defectos,Prueba prueba,
			Vehiculo vehiculo,Revision revision) throws JsonProcessingException{
			
			List<Optional<RegistroAuditoria> >  listaRegistros = new ArrayList<>();
		
			RegistroAuditoria registroAuditoria = new RegistroAuditoria();
			registroAuditoria.setPlaca(vehiculo.getPlaca());
			registroAuditoria.setIdRevision( revision.getRevisionId() );
			registroAuditoria.setFechaEvento( prueba.getFechaFinalizacion() != null ? prueba.getFechaFinalizacion() : new Timestamp(new Date().getTime()));
			registroAuditoria.setFechaRegistroBd( new Timestamp( new Date().getTime() ) );
			
			registroAuditoria.setUsuario( prueba.getUsuario().getNombres() + " " + prueba.getUsuario().getApellidos() );
			registroAuditoria.setIdentificacionUsuario( String.valueOf( prueba.getUsuario().getCedula() ));
			registroAuditoria.setTipoOperacion(TipoOperacion.INSERCION);
			
			registroAuditoria.setCodigoProveedor(Integer.valueOf(ConstantesIndra.ID_PROVEEDOR));
			registroAuditoria.setIdRuntCda( informacionCda.getIdRuntCda() );
			
			switch (tipoPrueba.getTipoPruebaId())
				{
					case TIPO_PRUEBA_RUIDO:
						registroAuditoria.setEventoPrueba(EventoPrueba.SONOMETRIA);
						SonometriaLog logSonometria = generadorDTOAuditoria.generarLogSonometria( prueba, medidas);
						String logSonometriaJson = objectMapper.writeValueAsString(logSonometria);
						registroAuditoria.setTrama(logSonometriaJson);
						listaRegistros.add( Optional.of(registroAuditoria) );
						break;
					case TIPO_PRUEBA_GASES:
						registroAuditoria.setEventoPrueba(EventoPrueba.GASES);
						LogGasesSicovDTO logGases = generadorDTOAuditoria.generarLogPruebaGases(prueba, medidas, vehiculo, defectos);
						String logGasesJson = objectMapper.writeValueAsString(logGases);
						registroAuditoria.setTrama(logGasesJson);
						listaRegistros.add( Optional.of( registroAuditoria));
						//defectos
						if(defectos != null && !defectos.isEmpty() ){
							Set<Integer> defectosNoDuplicados = 
							defectos.stream().map(d->d.getCodigoDefecto() != null ? d.getCodigoDefecto() : d.getDefectoId()
							).collect(Collectors.toSet());
							String defectosConcatenados = defectosNoDuplicados.stream().map(d->String.valueOf(d)).collect(Collectors.joining("_"));
							StringBuilder sbDefectos = new StringBuilder("defectos : ");
							sbDefectos.append(defectosConcatenados);
							registroAuditoria.setObservacion( sbDefectos.toString() );							
						}
						
						break;
						
					case TIPO_PRUEBA_LUCES:
						registroAuditoria.setEventoPrueba(EventoPrueba.LUCES);
						LogLucesSicovDTO logLuces = generadorDTOAuditoria.generarLogPruebaLuces(prueba,medidas,vehiculo,defectos);
						String logLucesJson = objectMapper.writeValueAsString(logLuces);
						registroAuditoria.setTrama(logLucesJson);
						listaRegistros.add( Optional.of(registroAuditoria) );
						if(defectos != null && !defectos.isEmpty() ){
							Set<Integer> defectosNoDuplicados = 
							defectos.stream().map(d->d.getCodigoDefecto() != null ? d.getCodigoDefecto() : d.getDefectoId()
							).collect(Collectors.toSet());
							String defectosConcatenados = defectosNoDuplicados.stream().map(d->String.valueOf(d)).collect(Collectors.joining("_"));
							StringBuilder sbDefectos = new StringBuilder("defectos : ");
							sbDefectos.append(defectosConcatenados);
							registroAuditoria.setObservacion( sbDefectos.toString() );							
						}
						break;
					case TIPO_PRUEBA_TAXIMETRO:
						registroAuditoria.setEventoPrueba(EventoPrueba.TAXIMETRO);	
						String llanta = vehiculo.getLlanta() == null ?  "R15" : vehiculo.getLlanta().getNombre(); 
						LogAuditoriaTaximetro logTax = generadorDTOAuditoria.generarLogPruebaTaximetro(prueba,defectos,medidas,revision.getTaximetro(),llanta);
						String logTaximetroJson = objectMapper.writeValueAsString(logTax);
						registroAuditoria.setTrama(logTaximetroJson);
						listaRegistros.add( Optional.of(registroAuditoria) );
						if(defectos != null && !defectos.isEmpty() ){
							Set<Integer> defectosNoDuplicados = 
							defectos.stream().map(d->d.getCodigoDefecto() != null ? d.getCodigoDefecto() : d.getDefectoId()
							).collect(Collectors.toSet());
							String defectosConcatenados = defectosNoDuplicados.stream().map(d->String.valueOf(d)).collect(Collectors.joining("_"));
							StringBuilder sbDefectos = new StringBuilder("defectos : ");
							sbDefectos.append(defectosConcatenados);
							registroAuditoria.setObservacion( sbDefectos.toString() );							
						}
						break;
					case TIPO_PRUEBA_FRENOS:
						registroAuditoria.setEventoPrueba(EventoPrueba.FRENOS);
						LogFrenosSicovDTO logFrenos = generadorDTOAuditoria.generarLogPruebaFrenos(prueba,medidas,vehiculo,defectos);
						String logFrenosJson = objectMapper.writeValueAsString(logFrenos);
						registroAuditoria.setTrama(logFrenosJson);
						listaRegistros.add( Optional.of(registroAuditoria) );
						//si la prueba de frenos tiene medidas de profundidad de labrado y llantas
						if( contieneMedidasLlantas(medidas) ) {
							LogLlantasSicovDTO logLlantas = generadorDTOAuditoria.generarLogLlantas(prueba, medidas, vehiculo, defectos);
							RegistroAuditoria reg = new RegistroAuditoria();
							reg.setPlaca(vehiculo.getPlaca());
							reg.setIdRevision( revision.getRevisionId() );
							reg.setFechaEvento( prueba.getFechaFinalizacion() != null ? prueba.getFechaFinalizacion() : new Timestamp(new Date().getTime()));
							reg.setFechaRegistroBd( new Timestamp( new Date().getTime() ) );
							
							reg.setUsuario( prueba.getUsuario().getNombres() + " " + prueba.getUsuario().getApellidos() );
							reg.setIdentificacionUsuario( String.valueOf( prueba.getUsuario().getCedula() ));
							reg.setTipoOperacion(TipoOperacion.INSERCION);
							
							reg.setCodigoProveedor(Integer.valueOf(ConstantesIndra.ID_PROVEEDOR));
							reg.setIdRuntCda( informacionCda.getIdRuntCda() );							
							reg.setEventoPrueba( EventoPrueba.LLANTAS);
							String logLlantasStr = objectMapper.writeValueAsString(logLlantas);
							reg.setTrama( logLlantasStr );
							listaRegistros.add( Optional.of( reg));
							
						}
						if(defectos != null && !defectos.isEmpty() ){
							Set<Integer> defectosNoDuplicados = 
							defectos.stream().map(d->d.getCodigoDefecto() != null ? d.getCodigoDefecto() : d.getDefectoId()
							).collect(Collectors.toSet());
							String defectosConcatenados = defectosNoDuplicados.stream().map(d->String.valueOf(d)).collect(Collectors.joining("_"));
							StringBuilder sbDefectos = new StringBuilder("defectos : ");
							sbDefectos.append(defectosConcatenados);
							registroAuditoria.setObservacion( sbDefectos.toString() );							
						}
						
						
						
						break;
					case TIPO_PRUEBA_SUSPENSION:
						registroAuditoria.setEventoPrueba(EventoPrueba.SUSPENSION);
						LogAuditoriaSuspension logSuspension = generadorDTOAuditoria.generarLogPruebaSuspension(prueba,medidas,vehiculo,defectos);
						String logSuspensionJson = objectMapper.writeValueAsString(logSuspension);
						registroAuditoria.setTrama(logSuspensionJson);
						listaRegistros.add(Optional.of(registroAuditoria));
						if(defectos != null && !defectos.isEmpty() ){
							Set<Integer> defectosNoDuplicados = 
							defectos.stream().map(d->d.getCodigoDefecto() != null ? d.getCodigoDefecto() : d.getDefectoId()
							).collect(Collectors.toSet());
							String defectosConcatenados = defectosNoDuplicados.stream().map(d->String.valueOf(d)).collect(Collectors.joining("_"));
							StringBuilder sbDefectos = new StringBuilder("defectos : ");
							sbDefectos.append(defectosConcatenados);
							registroAuditoria.setObservacion( sbDefectos.toString() );							
						}
						break;
					case TIPO_PRUEBA_DESVIACION:
						registroAuditoria.setEventoPrueba(EventoPrueba.ALINEACION);						
						LogAlineacionSicovDTO logAlineacion = generadorDTOAuditoria.generarLogPruebaDesviacion(prueba,medidas,vehiculo,defectos);
						String logAlineacionJson = objectMapper.writeValueAsString(logAlineacion);
						registroAuditoria.setTrama(logAlineacionJson);						
						listaRegistros.add(Optional.of(registroAuditoria));
						if(defectos != null && !defectos.isEmpty() ){
							Set<Integer> defectosNoDuplicados = 
							defectos.stream().map(d->d.getCodigoDefecto() != null ? d.getCodigoDefecto() : d.getDefectoId()
							).collect(Collectors.toSet());
							String defectosConcatenados = defectosNoDuplicados.stream().map(d->String.valueOf(d)).collect(Collectors.joining("_"));
							StringBuilder sbDefectos = new StringBuilder("defectos : ");
							sbDefectos.append(defectosConcatenados);
							registroAuditoria.setObservacion( sbDefectos.toString() );							
						}
						break;
					case TIPO_PRUEBA_INSPECCION_SENSORIAL:
						registroAuditoria.setEventoPrueba(EventoPrueba.VISUAL);
						LogInspeccionSensorialSicovDTO logIV = generadorDTOAuditoria.generarLogPruebaInspeccionSensorial(prueba,medidas,vehiculo);
						String logIvJson = objectMapper.writeValueAsString(logIV);
						registroAuditoria.setTrama(logIvJson);
						listaRegistros.add(Optional.of(registroAuditoria));
						
						//si la prueba de frenos tiene medidas de profundidad de labrado y llantas
						if( contieneMedidasLlantas(medidas) ) {
							LogLlantasSicovDTO logLlantas = generadorDTOAuditoria.generarLogLlantas(prueba, medidas, vehiculo, defectos);
							RegistroAuditoria reg = new RegistroAuditoria();
							reg.setPlaca( vehiculo.getPlaca() );
							reg.setIdRevision( revision.getRevisionId() );
							reg.setFechaEvento( prueba.getFechaFinalizacion() != null ? prueba.getFechaFinalizacion() : new Timestamp(new Date().getTime()));
							reg.setFechaRegistroBd( new Timestamp( new Date().getTime() ) );
							
							reg.setUsuario( prueba.getUsuario().getNombres() + " " + prueba.getUsuario().getApellidos() );
							reg.setIdentificacionUsuario( String.valueOf( prueba.getUsuario().getCedula() ));
							reg.setTipoOperacion(TipoOperacion.INSERCION);
							
							reg.setCodigoProveedor(Integer.valueOf(ConstantesIndra.ID_PROVEEDOR));
							reg.setIdRuntCda( informacionCda.getIdRuntCda() );							
							reg.setEventoPrueba( EventoPrueba.LLANTAS);
							String logLlantasStr = objectMapper.writeValueAsString(logLlantas);
							reg.setTrama( logLlantasStr );
							listaRegistros.add( Optional.of( reg));
							
						}
						
				}
		
			return listaRegistros;
	}
	
	private List<Defecto> defectoAsociadoADefecto(List<DefectoAsociado> defectosAsociados){
		return defectosAsociados.stream().map(da-> da.getDefecto()).collect(Collectors.toList());
	}
	
	private Boolean contieneMedidasLlantas(List<Medida> medidas) {
		Optional<Medida> optMedida = medidas.stream().filter( m-> m.getTipoMedida().getTipoMedidaId().equals(ConstantesMedidasPresionLlanta.P_EJE_1_LL1_DER)).findAny();
		return optMedida.isPresent();
	}
	
	private Boolean contieneMedidasLabrado(List<Medida> medidas) {
		Optional<Medida> optMedida = medidas.stream().filter( m-> m.getTipoMedida().getTipoMedidaId().equals(ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE1_DER)).findAny();
		return optMedida.isPresent();
	}
	
}
