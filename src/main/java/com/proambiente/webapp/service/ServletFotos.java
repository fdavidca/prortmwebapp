package com.proambiente.webapp.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.imageio.ImageIO;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.proambiente.modelo.Foto;
import com.proambiente.modelo.Prueba;
import com.proambiente.webapp.dao.FotoFacade;

/**
 * Servlet implementation class ServletFotos
 */
@WebServlet("/ServletFotos")
public class ServletFotos extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Inject 
	FotoFacade fotoDAO;
       
	@Inject
	InspeccionService inspeccionService;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletFotos() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String strInspeccionId =request.getParameter("inspeccion_id");
		String strNumeroFoto = request.getParameter("numero_foto");
		if(strInspeccionId != null && strNumeroFoto !=null){
			Integer inspeccionId = Integer.parseInt(strInspeccionId);
			Integer numeroFoto = Integer.parseInt(strNumeroFoto);
			Prueba p = inspeccionService.pruebaFotoInspeccion(inspeccionId);
			Foto f = fotoDAO.consultarFotoPorPrueba(p);
			if(numeroFoto==1){
				BufferedImage fotoUno = ImageIO.read(new ByteArrayInputStream(f.getFoto1()));
				response.setContentType("image/jpeg");
				OutputStream out = response.getOutputStream();
				ImageIO.write(fotoUno, "jpeg", out);
				out.close();
			} else{
				BufferedImage fotoDos = ImageIO.read(new ByteArrayInputStream(f.getFoto2()));
				response.setContentType("image/jpeg");
				OutputStream out = response.getOutputStream();
				ImageIO.write(fotoDos, "jpeg", out);
				out.close();
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
