package com.proambiente.webapp.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.imageio.ImageIO;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.proambiente.modelo.Foto;
import com.proambiente.modelo.Prueba;
import com.proambiente.webapp.dao.FotoFacade;

/**
 * Servlet implementation class ServletFotos
 */
@WebServlet("/ServletFotosRevision")
public class ServletFotosRevision extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Inject 
	FotoFacade fotoDAO;
       
	@Inject
	PruebasService pruebaService;
	
	@Inject
	RevisionService revisionService;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletFotosRevision() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String revisionIdStr =request.getParameter("revision_id");
		String strNumeroFoto = request.getParameter("numero_foto");
		if(revisionIdStr != null && strNumeroFoto !=null){
			Integer revisionId = Integer.parseInt(revisionIdStr);
			Integer numeroFoto = Integer.parseInt(strNumeroFoto);
			Prueba p = revisionService.buscarPruebaPorTipoPorRevision(revisionId,3 );
			Foto f = fotoDAO.consultarFotoPorPrueba(p);
			if(numeroFoto==1){
				if(f.getFoto1() != null) {
				BufferedImage fotoUno = ImageIO.read(new ByteArrayInputStream(f.getFoto1()));
				response.setContentType("image/jpeg");
				OutputStream out = response.getOutputStream();
				ImageIO.write(fotoUno, "jpeg", out);
				out.close();
				}else {
					response.getOutputStream().println("Foto no tomada aun");
					response.getOutputStream().close();
					
				}
			} else{
				if(f.getFoto2() != null) {
				BufferedImage fotoDos = ImageIO.read(new ByteArrayInputStream(f.getFoto2()));
				response.setContentType("image/jpeg");
				OutputStream out = response.getOutputStream();
				ImageIO.write(fotoDos, "jpeg", out);
				out.close();
				}else {
					response.getOutputStream().println("Foto no tomada aun");
					response.getOutputStream().close();
				}
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
