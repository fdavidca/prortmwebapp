package com.proambiente.webapp.service.guardarpdf;

import javax.inject.Inject;

import com.proambiente.webapp.service.RegistrarFurService;

public class GuardarPdfCallbackImpl implements GuardarPdfCallback {

	
	@Inject
	RegistrarFurService guardarPdfService;
	
	@Override
	public void guardarPdf( Integer revisionId) {
		
		guardarPdfService.guardarPdfPrimerEnvio(revisionId);
		
	}

}
