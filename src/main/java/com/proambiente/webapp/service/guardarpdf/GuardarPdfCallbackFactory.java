package com.proambiente.webapp.service.guardarpdf;

import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import com.proambiente.modelo.InformacionCda;
import com.proambiente.webapp.dao.InformacionCdaFacade;

@ApplicationScoped
public class GuardarPdfCallbackFactory {
	
	private static final Logger logger = Logger.getLogger(GuardarPdfCallbackFactory.class.getName());


	@Inject
	InformacionCdaFacade infoCdaDAO;
	
	@Inject
	Instance<GuardarPdfCallbackImpl> guardarPdfImplementacion;
	
	public GuardarPdfCallback crearInstancia(){

		InformacionCda infoCda = infoCdaDAO.find(1);
		if(infoCda.getGuardarpdf()) {
			return guardarPdfImplementacion.get();
		}else {
			return new GuardarPdfDummyCallback();
		}
	}	

}
