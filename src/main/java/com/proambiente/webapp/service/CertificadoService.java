package com.proambiente.webapp.service;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;

import com.proambiente.modelo.Certificado;
import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.Revision;
import com.proambiente.webapp.dao.CertificadoFacade;
import com.proambiente.webapp.dao.InformacionCdaFacade;

@Stateless
public class CertificadoService {

	@Inject
	CertificadoFacade certficadoDAO;



	@Inject
	InformacionCdaFacade informacionCdaDAO;

	@PersistenceContext
	EntityManager em;

	public Long consultarNumeroCertificadosNoAnulados(Integer revisionId) {
		String consulta = "Select COUNT(c) From Certificado c " + "WHERE c.revision.revisionId =:revisionId "
				+ "AND c.anulado=false";
		Query query = em.createQuery(consulta);
		query.setParameter("revisionId", revisionId);

		Long numeroCertificadosNoAnulados = (Long) query.getSingleResult();
		return numeroCertificadosNoAnulados;
	}

	public Long consultarCertificadoConsecutivoSustratoExistente(String consecutivoSustrato) {
		String consulta = "Select COUNT(c) From Certificado c " + "WHERE c.consecutivoSustrato =:consecutivoSustrato "
				+ "AND c.anulado=false";
		Query query = em.createQuery(consulta);
		query.setParameter("consecutivoSustrato", consecutivoSustrato);

		Long numeroCertificadosNoAnulados = (Long) query.getSingleResult();
		return numeroCertificadosNoAnulados;
	}
	
	public Long consultarCertificadoConsecutivoRuntExistente(String consecutivoRunt) {
		String consulta = "Select COUNT(c) From Certificado c " + "WHERE c.consecutivoRunt =:consecutivoRunt "
				+ "AND c.anulado=false";
		Query query = em.createQuery(consulta);
		query.setParameter("consecutivoRunt", consecutivoRunt);

		Long numeroCertificadosNoAnulados = (Long) query.getSingleResult();
		return numeroCertificadosNoAnulados;
	}

	public Long consultarCuantosCertificadosTieneRevision(Integer revisionId) {
		String consulta = "Select COUNT(c) From Certificado c " + "WHERE c.revision.revisionId =:revisionId ";
		Query query = em.createQuery(consulta);
		query.setParameter("revisionId", revisionId);
		Long numeroCertificadosDeRevision = (Long) query.getSingleResult();
		return numeroCertificadosDeRevision;
	}

	public void crearCertificado(Certificado certificado, Revision revision, String consecutivoPreimpreso) {
		//revisionService.actualizarUsuarioResponsable(revision);
		certficadoDAO.create(certificado);
		System.out.println("Registrando el certificado");
		registrarInspeccionAprobada(revision.getRevisionId());
		System.out.println("Registrando la inspeccion aprobada");
		InformacionCda infoCDA = informacionCdaDAO.getInformacionCda();
		infoCDA.setConsecutivoCert(consecutivoPreimpreso);
		informacionCdaDAO.edit(infoCDA);// actualiza el consecutivo del preimpreso
	}
	
	public void registrarInspeccionAprobada(int revisionId) {
		StoredProcedureQuery storedProcedureQuery = em.createStoredProcedureQuery("registrar_inspeccion_aprobada");
		storedProcedureQuery.registerStoredProcedureParameter(1, Integer.class, ParameterMode.IN);
		storedProcedureQuery.setParameter(1, revisionId);
		storedProcedureQuery.execute();
	}

	public void actualizarCertificado(Certificado certificado) {
		certficadoDAO.edit(certificado);
	}

	public void anularCrearCertificado(Certificado certificadoNuevo, Certificado certificadoAnular) {

		em.merge(certificadoAnular);
		em.flush();
		em.persist(certificadoNuevo);

	}

	public Certificado consultarUltimoCertificado(Integer revisionId) {
		String consulta = "Select MAX(c) From Certificado c " + "WHERE c.revision.revisionId =:revisionId "
				+ "AND c.anulado = false ";
		Query query = em.createQuery(consulta);
		query.setHint("org.hibernate.cacheable", Boolean.TRUE);
		query.setParameter("revisionId", revisionId);
		Certificado ultimoCertificado = (Certificado) query.getSingleResult();
		return ultimoCertificado;
	}

	public String consultarUltimoConsecutivoCertificado(Integer revisionId) {
		String consulta = "Select MAX(c.certificadoId) From Certificado c "
				+ "WHERE c.revision.revisionId =:revisionId " + "AND c.anulado = false ";
		Query query = em.createQuery(consulta);
		query.setParameter("revisionId", revisionId);
		Integer ultimoCertificado = (Integer) query.getSingleResult();
		if (ultimoCertificado != null) {
			Certificado certificado = em.find(Certificado.class, ultimoCertificado);
			return certificado.getConsecutivoSustrato();
		} else {
			throw new RuntimeException("No se encuentra certificado para revision " + revisionId);
		}

	}

}
