package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.Pipeta;
import com.proambiente.webapp.controller.validator.PipetaValidator;
import com.proambiente.webapp.dao.PipetaFacade;
import com.proambiente.webapp.util.UtilErrores;

@Named
@ViewScoped
public class PipetasCRUDBean implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 31L;

	private static final Logger logger = Logger.getLogger(PipetasCRUDBean.class.getName());
	
	@Inject
	PipetaFacade pipetaFacade;
	
	@Inject
	UtilErrores utilErrores;
	
	List<Pipeta> listaPipetas;
	
	Pipeta pipetaSeleccionada;
	
	private Date fechaAuxiliar;
	
	@PostConstruct
	public void init(){
		
		listaPipetas = pipetaFacade.findAll();
		pipetaSeleccionada = new Pipeta();
		
	}

	

	public List<Pipeta> getListaPipetas() {
		return listaPipetas;
	}



	public void setListaPipetas(List<Pipeta> listaPipetas) {
		this.listaPipetas = listaPipetas;
	}

	

	public Pipeta getPipetaSeleccionada() {
		return pipetaSeleccionada;
	}



	public void setPipetaSeleccionada(Pipeta pipetaSeleccionada) {
		this.pipetaSeleccionada = pipetaSeleccionada;
	}



	public void guardarCambios(ActionEvent ae){
		
		try{
			
		
			PipetaValidator validador = new PipetaValidator();
			
			StringBuilder sb = new StringBuilder();
			Boolean valida = validador.validarPipeta(pipetaSeleccionada, sb);
			if(valida) {
				if(pipetaSeleccionada.getAgotada()) {
					pipetaSeleccionada.establecerPipetaAgotada(pipetaSeleccionada.getFechaAgotada());
				}else {
					pipetaSeleccionada.establecerPipetaNoAgotada();
				}
				pipetaFacade.edit(pipetaSeleccionada);
			} else {
				utilErrores.addError("Edicion creacion rechazada " + sb.toString());
				return;
			}
			
			
			
			
			listaPipetas = pipetaFacade.findAll();
			utilErrores.addInfo("Pipeta creada");
			
			
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error guardando pipeta", exc);
			utilErrores.addError("Error guardando pipeta " + exc.getMessage());
		}
	}
	
	public void nuevaPipeta(ActionEvent ae){
		pipetaSeleccionada = new Pipeta();
	}

	public Date getFechaAuxiliar() {
		return fechaAuxiliar;
	}

	public void setFechaAuxiliar(Date fechaAuxiliar) {
		this.fechaAuxiliar = fechaAuxiliar;
	}
	
	
	
}
