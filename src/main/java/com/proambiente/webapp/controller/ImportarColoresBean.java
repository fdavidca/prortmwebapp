package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.file.UploadedFile;

import com.proambiente.modelo.Color;
import com.proambiente.modelo.Usuario;
import com.proambiente.webapp.service.ImportarColoresDesdeExcelService;
import com.proambiente.webapp.util.UtilErrores;

@Named
@ViewScoped
public class ImportarColoresBean implements Serializable{
	
	private static final long serialVersionUID = -7340812940161862587L;
	
	private static final Logger logger = Logger.getLogger(ImportarColoresBean.class.getName());
	
	@Inject
	UtilErrores utilErrores;
	
	@Inject
	ImportarColoresDesdeExcelService importar ;
	
	@Inject
	@UsuarioDefault 
	Usuario usuarioAutenticado;
	
	private List<Color> coloresCreados = new ArrayList<>();

	private UploadedFile file;
	
	public UploadedFile getFile(){
		return file;
	}
	
	
	
	public void upload(FileUploadEvent event){
		UploadedFile uploadedFile = event.getFile();
		if(uploadedFile != null){
			logger.info("Archivo " + uploadedFile.getFileName() + "fue subido ");
			try {
				List<Color> colores = importar.obtenerColoresDesdeArchivo( uploadedFile.getInputStream() );
				importar.guardarColores(colores);
				coloresCreados = colores;
			} catch (Exception e) {
				logger.log(Level.SEVERE, "Error importando archivos", e);
				utilErrores.addError("Error importando archivos");
			}
			utilErrores.addInfo(uploadedFile.getFileName() + " is uploaded.");			 
		}
	}
	
	public void setFile(UploadedFile file){
		this.file = file;
	}



	public List<Color> getColoresCreados() {
		return coloresCreados;
	}



	public void setColoresCreados(List<Color> coloresCreados) {
		this.coloresCreados = coloresCreados;
	}



	


	
}
