package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;

import com.proambiente.modelo.Equipo;
import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.Medida;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.webapp.dao.InformacionCdaFacade;
import com.proambiente.webapp.service.PruebasService;
import com.proambiente.webapp.service.VehiculoService;
import com.proambiente.webapp.util.dto.InformePresionSonoraDTO;


@Named
@ViewScoped
public class InformePruebaRuido implements Serializable{
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -9010158931319338729L;

	private static final Logger logger = Logger
			.getLogger(InformePruebaRuido.class.getName());
	private Date fechaInicial;
	private Date fechaFinal;
	
	@Inject
	PruebasService pruebaService;
	
	@Inject
	VehiculoService vehiculoService;
	
	@Inject
	InformacionCdaFacade infoCdaDAO;
	
	private List<Prueba> pruebasRuido;
	
    private List<InformePresionSonoraDTO> listaRegistros = new ArrayList<>();
	
	
	public void consultarPruebas(ActionEvent ae){
		try {
			logger.info("Consultando pruebas de ruido");
			pruebasRuido = pruebaService.consultarPruebasRuido(fechaInicial, fechaFinal);
			InformacionCda informacionCDA = infoCdaDAO.find(1);
			pruebasRuido.stream().forEach(p->listaRegistros.add(formarRegistro(p, informacionCDA) ) );
			
		} catch (Exception e) {			
			logger.log(Level.SEVERE, "Error consultando pruebas de ruido", e);
		}
	}
	
	public InformePresionSonoraDTO formarRegistro(Prueba p,InformacionCda infoCDA) {
		
		InformePresionSonoraDTO informePresionSonora = new InformePresionSonoraDTO();
		informePresionSonora.setNombreCDA(infoCDA.getNombre());
		informePresionSonora.setNitCDA(infoCDA.getNit());
		informePresionSonora.setNumeroCDA(infoCDA.getNumeroCda());
		
		Vehiculo vehiculo = vehiculoService.vehiculoDesdePruebaId(p.getPruebaId());
		informePresionSonora.setPlacaVehiculo(vehiculo.getPlaca());
		informePresionSonora.setModeloVehiculo(vehiculo.getModelo());
		informePresionSonora.setCilindradaVehiculo(vehiculo.getCilindraje());
		informePresionSonora.setCombustibleVehiculo(vehiculo.getTipoCombustible().getNombre());
		informePresionSonora.setTipoVehiculo(vehiculo.getTipoVehiculo().getNombre());
		
		List<Equipo> equipos = pruebaService.obtenerEquipos(p);
		Optional<Equipo> equipoOpt = equipos.stream().filter(e->e.getTipoEquipo().getTipoEquipoId() == 6).findFirst();
		if(equipoOpt.isPresent()) {
			Equipo equipo = equipoOpt.get();
			informePresionSonora.setMarcaEquipo(equipo.getFabricante());
			informePresionSonora.setSerieEquipo(equipo.getSerieEquipo());
			informePresionSonora.setModeloEquipo(equipo.getModeloEquipo() != null ? equipo.getModeloEquipo() : "-");
		}else {
			informePresionSonora.setMarcaEquipo("");
			informePresionSonora.setSerieEquipo("");
			informePresionSonora.setModeloEquipo("");
		}
		
		informePresionSonora.setFechaMedicion( p.getFechaFinalizacion() );
		DecimalFormat df = new DecimalFormat("0.0");
		DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.getDefault());
		otherSymbols.setDecimalSeparator('.');
		df.setDecimalFormatSymbols(otherSymbols);
		
		Double valorMedicion = cargarValorMedicion(p);
		String valorMedicionStr = "";
		try {
		  valorMedicionStr= df.format(valorMedicion);
		}catch(Exception exc) {
			logger.log(Level.SEVERE,"error procesando valor de medicion para prueba " + p.getPruebaId(),exc);
			valorMedicionStr = "";
		}
		informePresionSonora.setMedicionEscape(valorMedicionStr);
		
		return informePresionSonora;
		
	}
	
	public Date getFechaInicial() {
		return fechaInicial;
	}
	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	public Date getFechaFinal() {
		return fechaFinal;
	}
	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}
	public List<Prueba> getPruebasRuido() {
		return pruebasRuido;
	}
	public void setPruebasRuido(List<Prueba> pruebasRuido) {
		this.pruebasRuido = pruebasRuido;
	}	
	public List<InformePresionSonoraDTO> getListaRegistros() {
		return listaRegistros;
	}

	public void setListaRegistros(List<InformePresionSonoraDTO> listaRegistros) {
		this.listaRegistros = listaRegistros;
	}

	public Double cargarValorMedicion(Prueba prueba){
		List<Medida> medidas = pruebaService.obtenerMedidas(prueba);
	   for(Medida m : medidas){
		   if(m.getTipoMedida().getTipoMedidaId() == 7005){
			   return m.getValor();
		   }
	   }
	   return 0.0;
	}
		
	
}
