package com.proambiente.webapp.controller.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.Pais;
import com.proambiente.webapp.dao.PaisFacade;

@Named
public class PaisConverter implements Converter {

	@Inject
	PaisFacade paisDAO;
	
	@Override
	public Object getAsObject(FacesContext fc, UIComponent ui, String str) {
		Integer id = Integer.parseInt(str);
		return paisDAO.find(id);
	}

	@Override
	public String getAsString(FacesContext fc, UIComponent ui, Object obj) {
		
		return	String.valueOf( ( ( Pais ) obj).getPaisId() );
		 
	}

}
