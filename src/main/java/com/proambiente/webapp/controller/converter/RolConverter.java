package com.proambiente.webapp.controller.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.Rol;
import com.proambiente.webapp.dao.RolFacade;

@Named
public class RolConverter implements Converter{
	
	@Inject
	RolFacade rolDAO;
	
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		Integer id = Integer.parseInt(arg2);
		return rolDAO.find(id);
	}

	@Override
	public String getAsString(FacesContext fc, UIComponent ui, Object obj) {
		Integer id = ( ( Rol ) obj ).getRolId();
		return String.valueOf(id);
		
	}

}
