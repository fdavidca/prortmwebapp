package com.proambiente.webapp.controller.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.ClaseVehiculo;
import com.proambiente.webapp.dao.ClaseVehiculoFacade;

@Named
public class ClaseVehiculoConverter implements Converter{

	@Inject 
	ClaseVehiculoFacade claseVehiculoDAO;
	
	@Override
	public Object getAsObject(FacesContext fc, UIComponent component, String str) {
		
		Integer id = Integer.parseInt(str);
		ClaseVehiculo claseVehiculo = claseVehiculoDAO.find(id);
		return claseVehiculo;
	}

	@Override
	public String getAsString(FacesContext fc, UIComponent component, Object obj) {
		
		return String.valueOf( ( (ClaseVehiculo)obj ).getClaseVehiculoId() );
		
	}

}
