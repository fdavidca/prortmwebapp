package com.proambiente.webapp.controller.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.LineaVehiculo;
import com.proambiente.webapp.dao.LineaVehiculoFacade;


@Named
public class LineaVehiculoConverter implements Converter{
	
	@Inject
	LineaVehiculoFacade lineaVehiculoDAO;
	
	@Override
	public Object getAsObject(FacesContext fc, UIComponent ui, String str) {
		if(str.equalsIgnoreCase("No Seleccionado")) {
			return null;
		}else {
			Integer id = Integer.parseInt(str);
			return lineaVehiculoDAO.find(id);
		}
	}

	@Override
	public String getAsString(FacesContext fc, UIComponent ui, Object obj) {
		
		if(obj != null) {
			return String.valueOf( ( (LineaVehiculo) obj ).getLineaVehiculoId() );
		}else {
			return "No Seleccionado";
		}
		
		
	}

}
