package com.proambiente.webapp.controller.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.TipoDocumentoIdentidad;
import com.proambiente.webapp.dao.TipoDocumentoIdentidadFacade;

@Named
public class TipoDocumentoIdentidadConverter implements Converter{

	@Inject
	TipoDocumentoIdentidadFacade tipoDocumentoIdentidadDAO;
	
	@Override
	public Object getAsObject(FacesContext fc, UIComponent ui, String str) {
		if(str.equalsIgnoreCase("No Seleccionado")){
			return null;
		}else {
			Integer id = Integer.parseInt(str);
			return tipoDocumentoIdentidadDAO.find(id);
		}
		
	}

	@Override
	public String getAsString(FacesContext fc, UIComponent ui, Object obj) {
		if(obj != null){
			return String.valueOf(( (TipoDocumentoIdentidad) obj ).getTipoDocumentoIdentidadId());
		}else {
			return "No Seleccionado";
		}
	}

}
