package com.proambiente.webapp.controller.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.Marca;
import com.proambiente.webapp.dao.MarcaFacade;

@Named
public class MarcaConverter implements Converter {
	
	@Inject
	MarcaFacade marcaDAO;
	

	@Override
	public Object getAsObject(FacesContext fc, UIComponent ui, String str) {
		
		Integer id = Integer.valueOf(str);
		return marcaDAO.find(id);
	}

	@Override
	public String getAsString(FacesContext fc, UIComponent ui, Object obj) {
		return String.valueOf( ( (Marca)obj ).getMarcaId() );		
	}

}
