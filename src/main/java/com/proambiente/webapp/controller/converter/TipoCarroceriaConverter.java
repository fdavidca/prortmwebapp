package com.proambiente.webapp.controller.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;


import com.proambiente.webapp.dao.TipoCarroceriaFacade;
import com.proambiente.modelo.LineaVehiculo;
import com.proambiente.modelo.TipoCarroceria;


@Named
public class TipoCarroceriaConverter implements Converter{
	
	@Inject
	TipoCarroceriaFacade tipoCarroceriaFacade;
	
	@Override
	public Object getAsObject(FacesContext fc, UIComponent ui, String str) {
		if(str.equalsIgnoreCase("No Seleccionado")) {
			return null;
		}else {
			Integer id = Integer.parseInt(str);
			return tipoCarroceriaFacade.find(id);
		}
	}

	@Override
	public String getAsString(FacesContext fc, UIComponent ui, Object obj) {
		if(obj != null) {
			Integer id = ( (TipoCarroceria ) obj ).getTipoCarroceriaId();
			return String.valueOf(id);
		}else {
			return "No Seleccionado";
		}
	}	

}
