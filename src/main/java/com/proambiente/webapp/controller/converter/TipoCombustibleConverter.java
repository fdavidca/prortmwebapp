package com.proambiente.webapp.controller.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.TipoCombustible;
import com.proambiente.webapp.dao.TipoCombustibleFacade;

@Named
public class TipoCombustibleConverter implements Converter{

	@Inject
	TipoCombustibleFacade tipoCombustibleDAO;
	
	@Override
	public Object getAsObject(FacesContext fc, UIComponent ui, String str) {
		
		Integer id = Integer.parseInt(str);
		
		return tipoCombustibleDAO.find(id);
	}

	@Override
	public String getAsString(FacesContext fc, UIComponent ui, Object obj) {
		
		return String.valueOf( ( (TipoCombustible) obj ).getCombustibleId() );		
		
	}
	
	

}
