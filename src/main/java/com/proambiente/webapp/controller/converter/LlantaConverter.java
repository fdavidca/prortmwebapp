package com.proambiente.webapp.controller.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.Llanta;
import com.proambiente.webapp.dao.LlantaFacade;

@Named
public class LlantaConverter implements Converter{

	@Inject
	LlantaFacade llantaDAO;
	
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		Integer id = Integer.parseInt(arg2);
		return llantaDAO.find(id);
	}

	@Override
	public String getAsString(FacesContext fc, UIComponent ui, Object obj) {
		Integer id = ( ( Llanta ) obj ).getLlantaId();
		return String.valueOf(id);
		
	}

}
