package com.proambiente.webapp.controller.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.Servicio;
import com.proambiente.webapp.dao.ServicioFacade;


@Named
public class ServicioConverter implements Converter{
	
	@Inject
	ServicioFacade servicioDAO;

	@Override
	public Object getAsObject(FacesContext fc, UIComponent ui, String str) {		
		Integer id = Integer.parseInt(str);
		return servicioDAO.find(id);
	}

	@Override
	public String getAsString(FacesContext fc, UIComponent ui, Object obj) {
		Integer id = ( ( Servicio ) obj ).getServicioId();		
		return String.valueOf( id );
		
	}

	
	
}
