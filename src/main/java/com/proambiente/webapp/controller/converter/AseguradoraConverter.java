package com.proambiente.webapp.controller.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.Aseguradora;
import com.proambiente.webapp.dao.AseguradoraFacade;

@Named
public class AseguradoraConverter implements Converter {

	@Inject
	AseguradoraFacade aseguradoraDAO;
	
	@Override
	public Object getAsObject(FacesContext fc, UIComponent ui, String str) {
		Integer id = Integer.parseInt(str);
		return aseguradoraDAO.find(id);
	}

	@Override
	public String getAsString(FacesContext fc, UIComponent ui, Object obj) {
		Integer id = ( (Aseguradora ) obj ).getAseguradoraId();
		return String.valueOf(id);
	}

}
