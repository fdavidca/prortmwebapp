package com.proambiente.webapp.controller.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.Ciudad;
import com.proambiente.webapp.dao.CiudadFacade;

@Named
public class CiudadConverter implements Converter{
	
	@Inject
	CiudadFacade ciudadDAO;

	@Override
	public Object getAsObject(FacesContext fc, UIComponent ui, String str) {
		
		Integer id = Integer.valueOf(str);
		return ciudadDAO.find(id);
	}

	@Override
	public String getAsString(FacesContext fc, UIComponent ui, Object obj) {
		Integer id = ( ( Ciudad ) obj ).getCiudadId();
		return String.valueOf(id);
	}

}
