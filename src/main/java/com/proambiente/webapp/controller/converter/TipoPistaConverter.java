package com.proambiente.webapp.controller.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.TipoPista;
import com.proambiente.webapp.dao.TipoPistaFacade;

@Named
public class TipoPistaConverter implements Converter{

	@Inject
	TipoPistaFacade tipoPistaDAO;
	
	@Override
	public Object getAsObject(FacesContext fc, UIComponent ui, String str) {
		
		Integer id = Integer.parseInt(str);
		return tipoPistaDAO.find(id);	
		
	}

	@Override
	public String getAsString(FacesContext fc, UIComponent ui, Object obj) {
		
		return String.valueOf(( (TipoPista) obj ).getTipoPistaId());
	}

}
