package com.proambiente.webapp.controller.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.TipoEquipo;
import com.proambiente.webapp.dao.TipoEquipoFacade;

@Named
public class TipoEquipoConverter implements Converter{

	@Inject
	TipoEquipoFacade tipoVehiculoDAO;
	
	@Override
	public Object getAsObject(FacesContext fc, UIComponent ui, String str) {
		
		Integer id = Integer.parseInt(str);
		return tipoVehiculoDAO.find(id);	
		
	}

	@Override
	public String getAsString(FacesContext fc, UIComponent ui, Object obj) {
		
		return String.valueOf(( (TipoEquipo) obj ).getTipoEquipoId());
	}

}
