package com.proambiente.webapp.controller.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.Color;
import com.proambiente.webapp.dao.ColorFacade;



@Named
public class ColorConverter implements Converter{

	@Inject
	ColorFacade colorDAO;
	
	@Override
	public Object getAsObject(FacesContext fc, UIComponent ui, String str) {
		Integer id = Integer.parseInt(str);
		return colorDAO.find(id);	
	}

	@Override
	public String getAsString(FacesContext fc, UIComponent ui, Object obj) {
		
		return String.valueOf( ( ( Color ) obj ).getColorId() );
	 
	}

}
