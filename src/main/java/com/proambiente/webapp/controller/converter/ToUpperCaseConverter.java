package com.proambiente.webapp.controller.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Named;

@Named
public class ToUpperCaseConverter implements Converter{

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String submittedValue) {
		 return (submittedValue != null) ? submittedValue.trim().toUpperCase() : null;		
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object modelValue) {
		return (modelValue != null) ? modelValue.toString() : "";		
	}

}
