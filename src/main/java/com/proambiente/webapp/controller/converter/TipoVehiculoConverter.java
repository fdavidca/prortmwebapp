package com.proambiente.webapp.controller.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.TipoVehiculo;
import com.proambiente.webapp.dao.TipoVehiculoFacade;

@Named
public class TipoVehiculoConverter implements Converter{

	@Inject
	TipoVehiculoFacade tipoVehiculoDAO;
	
	@Override
	public Object getAsObject(FacesContext fc, UIComponent ui, String str) {
		
		Integer id = Integer.parseInt(str);
		return tipoVehiculoDAO.find(id);	
		
	}

	@Override
	public String getAsString(FacesContext fc, UIComponent ui, Object obj) {
		
		return String.valueOf(( (TipoVehiculo) obj ).getTipoVehiculoId());
	}

}
