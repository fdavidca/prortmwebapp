package com.proambiente.webapp.controller.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.Usuario;
import com.proambiente.webapp.dao.UsuarioFacade;


@Named
public class UsuarioConverter implements Converter {

	@Inject
	UsuarioFacade usuarioDAO;
	
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		Integer id = Integer.parseInt(arg2);
		return usuarioDAO.find(id);
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object obj) {
		Integer id = ((Usuario) obj).getUsuarioId();
		return String.valueOf(id);
	}

}
