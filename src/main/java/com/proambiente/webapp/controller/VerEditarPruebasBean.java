package com.proambiente.webapp.controller;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJBAccessException;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;

import com.proambiente.modelo.Inspeccion;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Usuario;
import com.proambiente.webapp.dao.InformacionCdaFacade;
import com.proambiente.webapp.dao.UsuarioFacade;
import com.proambiente.webapp.service.BorrarPruebaService;
import com.proambiente.webapp.service.CrearPruebaService;
import com.proambiente.webapp.service.FechaService;
import com.proambiente.webapp.service.InspeccionService;
import com.proambiente.webapp.service.PersistirPruebaService;
import com.proambiente.webapp.service.PruebaFinalizadaAprobadaService;
import com.proambiente.webapp.service.PruebasService;
import com.proambiente.webapp.service.PuedeCrearPruebaService;
import com.proambiente.webapp.service.RegistrarFurService;
import com.proambiente.webapp.service.RegistrarSegundaInspeccionService;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.service.ci2.OperacionesSicovCi2V3;
import com.proambiente.webapp.service.notification.NotificationPrimariaService;
import com.proambiente.webapp.service.sicov.OperacionesSicov;
import com.proambiente.webapp.service.sicov.OperacionesSicovProducer;
import com.proambiente.webapp.util.UtilErrores;

/**
 * Clase que sirve como controlador de la pagina en la que se quitan o agregan pruebas
 * a la revision
 * @author FABIAN
 *
 */
@Named
@ViewScoped
public class VerEditarPruebasBean implements Serializable{
	
	/**
	 * 
	 */
	
	
	private static final Logger logger = Logger
			.getLogger(VerEditarPruebasBean.class.getName());
	
	private static final long serialVersionUID = 1L;

	private static final Boolean GENERAR_ALERTA = Boolean.TRUE;
	
	@Inject
	OperacionesSicovProducer operacionesProducer;

	private Revision revision;
	
	private Boolean deshabilitarBotones;
	
	@Inject
	RevisionService revisionService;
	
	@Inject
	PersistirPruebaService persistirPruebaService;
	
	@Inject
	CrearPruebaService crearPruebaService;
	
	@Inject
	UsuarioFacade usuarioDAO;
	
	@Inject
	InformacionCdaFacade infoCDADAO;
	
	@Inject
	UtilErrores utilErrores;
	
	@Inject
	PruebasService pruebasService;
	
	@Inject
	PruebaFinalizadaAprobadaService pruebaFinalizadaAprobadaService;
	
	@Inject
	BorrarPruebaService borrarPruebaService;
	
	@Inject
	InspeccionService inspeccionService;
	
	@Inject
	PuedeCrearPruebaService puedeCrearPruebaS;
	
	@Inject
	RegistrarSegundaInspeccionService registrarSegundaInspeccionService;
	
	@Inject
	transient FechaService fechaService;
	
	@Inject
	RegistrarFurService registrarFurService;
	
	private Prueba pruebaSeleccionada;
	
	private String revisionId;
	
	@Resource
	ManagedExecutorService executor;
	
	@Inject
	NotificationPrimariaService notificacionService;
	
	
	@PostConstruct
	public void init(){
		
		try{
			Integer id = (Integer) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("revision_id");
			revision = revisionService.cargarRevision(id);			
			logger.log(Level.INFO,"Pruebas cargadas para {0}",revision.getVehiculo().getPlaca());
		}catch(Exception exc){
			utilErrores.addError("No se puede restablecer la vista" + exc.getMessage());			
			logger.log(Level.SEVERE, "Error no se puede cargar la vista de pruebas ", exc);
		}
	}

	public Revision getRevision() {		
		return revision;
	}

	public void setRevision(Revision revision_p) {		
		this.revision = revision_p;		
	}



	public Prueba getPruebaSeleccionada() {
		return pruebaSeleccionada;
	}

	public void setPruebaSeleccionada(Prueba pruebaSeleccionada) {
		this.pruebaSeleccionada = pruebaSeleccionada;
	}

	public Boolean getDeshabilitarBotones() {
		return deshabilitarBotones;
	}

	public void setDeshabilitarBotones(Boolean deshabilitarBotones) {
		this.deshabilitarBotones = deshabilitarBotones;
	}
	
	

	public String getRevisionId() {
		return revisionId;
	}

	public void setRevisionId(String revisionId) {
		this.revisionId = revisionId;
		Integer revisionIdInt = Integer.valueOf(revisionId);
		revision = revisionService.cargarRevision(revisionIdInt);
	}

	
	//Este metodo debe convertirse en transaccional
	public void sugerirPruebas(ActionEvent ae){
		try{
			if(revision.getCerrada()){
				utilErrores.addError("La revision ya fue cerrada");
				return;
			}
			
			
			if(revisionService.isRevisionVencidaPorQuinceDias(revision) && !revision.getAprobada() ){
				String mensaje = "El plazo de quince dias para volver a presentar el vehiculo ha expirado";
				utilErrores.addError(mensaje);
				try {				       
					FacesContext.getCurrentInstance().getExternalContext().getFlash().put("revision_id",revision.getRevisionId() );
					FacesContext.getCurrentInstance().getExternalContext().getFlash().put("mensaje",mensaje );
					FacesContext.getCurrentInstance().getExternalContext().redirect("/prortmwebapp/pages/cerrarrevision.xhtml?faces-redirect=true");
					
				} catch (IOException e) {
					logger.log(Level.SEVERE, "Error redirigiendo a pagina de pruebas", e);
					utilErrores.addError("Error");
				}	
				return;
			}			
			long numeroPruebasNoTerminadas = pruebasService.numeroPruebasNoTerminadas(revision.getRevisionId());
			if(numeroPruebasNoTerminadas != 0){
				utilErrores.addInfo("No ha terminado todas las pruebas");
				return;
			}
			String evaluacionRevision = revisionService.evaluarRevision(revision.getRevisionId());
			if(evaluacionRevision.equalsIgnoreCase("aprobada")){
				utilErrores.addInfo("Revision aprobada no es necesaria reinspeccion");
				return;
			}		
			Timestamp fechaInspeccionAnterior = null;
			Timestamp fechaInspeccionSiguiente = null;
			int numeroIntentos;
			boolean aprobada = false;
			List<Prueba> listaPruebasInspeccion = pruebasService.pruebasRevision(revision.getRevisionId());
			List<Inspeccion> inspecciones = inspeccionService.obtenerInspeccionesPorRevisionId(revision.getRevisionId());
			if(inspecciones!=null && inspecciones.isEmpty()){//si es la primera inspeccion
				
				fechaInspeccionAnterior = new Timestamp(revision.getFechaCreacionRevision().getTime());
				fechaInspeccionSiguiente = new Timestamp((new Date()).getTime());
				numeroIntentos = 1;
				Inspeccion inspeccion = new Inspeccion();
				inspeccion.setAprobada(aprobada);
				inspeccion.setFechaInspeccionAnterior(fechaInspeccionAnterior);
				inspeccion.setFechaInspeccionSiguiente(fechaInspeccionSiguiente);
				StringBuilder comentariosSb = new StringBuilder();
		        String comentarios = revision.getComentario();
		        if(comentarios != null)
		        	 comentariosSb.append(comentarios).append("\t");
		         
		        for(Prueba p: listaPruebasInspeccion){
		        	 if(p.getMotivoAborto() != null)
		        		 comentariosSb.append(p.getMotivoAborto()).append("\t");
		        }				
				inspeccion.setComentario(comentariosSb.toString());
				inspeccion.setConsecutivoRunt(revision.getConsecutivoRunt());
				inspeccion.setRevision(revision);
				inspeccion.setPruebas(listaPruebasInspeccion);
				inspeccion.setNumeroInspeccion(numeroIntentos);
				Usuario usuarioResponsable = usuarioDAO.find( revision.getUsuarioResponsableId() );
				inspeccion.setUsuarioResponsable(usuarioResponsable);
					
				registrarSegundaInspeccionService.registrarPrimeraInspeccion(inspeccion, revision, numeroIntentos);
								
				if(revision.getPreventiva()){
					
				}else {
					
					executor.submit( ()->{
						try{
							OperacionesSicov operacionesSicov = operacionesProducer.crearOperacionSicov();
							operacionesSicov.utilizarPin(revision.getPin(), OperacionesSicovCi2V3.SEGUNDA_INSPECCION, revision.getVehiculo().getPlaca());							
						}catch(Exception exc){
							logger.log(Level.SEVERE, "Error ", exc);
						}
					});
				}
				
				
			}else{//segunda o tercera inspeccion
				
				
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("revision_id",revision.getRevisionId() );	        
				FacesContext.getCurrentInstance().getExternalContext().redirect("/prortmwebapp/pages/cerrarrevision.xhtml?faces-redirect=true");
								
			}
			
			
			
		}catch(Exception exc){
			utilErrores.addError("Error " + exc.getMessage());			
			logger.log(Level.SEVERE, "Error sugiriendo pruebas", exc);
		}
	}
	
	
	
	
	 
	
	public void crearPrueba(String nombrePrueba){
		try{
			if(revision.getCerrada()) {
				utilErrores.addError("Revision Cerrada");
				return;
			}
			StringBuilder sbError = new StringBuilder();
			
			boolean existePruebaNoFinalizada = true;
			switch(nombrePrueba){
			case "Frenos":
				Boolean puedeCrearPrueba = puedeCrearPruebaS.puedeCrearPruebaService(sbError, revision.getRevisionId(), new Integer(5) );//frenos es 5
				
				if(!puedeCrearPrueba){
					utilErrores.addError("No se puede crear prueba " + sbError.toString());					
				}else {
					Prueba p = crearPruebaService.crearPrueba(revision, 5);
					revision.getPruebas().add(p);
				}
				
				break;
			case "Gases":
				puedeCrearPrueba = puedeCrearPruebaS.puedeCrearPruebaService(sbError, revision.getRevisionId(), new Integer(8) );//frenos es 8
				
				if(!puedeCrearPrueba){
					utilErrores.addError("No se puede crear prueba " + sbError.toString());					
				}else {
					Prueba p = crearPruebaService.crearPrueba(revision, 8);
					revision.getPruebas().add(p);
				}
				
			    break;
			case "Visual":
				 
				 puedeCrearPrueba = puedeCrearPruebaS.puedeCrearPruebaService(sbError, revision.getRevisionId(), new Integer(1) );//visual es 1
					
					if(!puedeCrearPrueba){
						utilErrores.addError("No se puede crear prueba " + sbError.toString());					
					}else {
						Prueba p = crearPruebaService.crearPrueba(revision, 1);
						revision.getPruebas().add(p);
					}
				break;
			case "Suspension":
				 
				 puedeCrearPrueba = puedeCrearPruebaS.puedeCrearPruebaService(sbError, revision.getRevisionId(), new Integer(6) );//suspension es 6
				 if(!puedeCrearPrueba){
						utilErrores.addError("No se puede crear prueba " + sbError.toString());					
					}else {
						Prueba p = crearPruebaService.crearPrueba(revision, 6);
						revision.getPruebas().add(p);
					}
				break;
			case "Desviacion":
				 
				 puedeCrearPrueba = puedeCrearPruebaS.puedeCrearPruebaService(sbError, revision.getRevisionId(), new Integer(4) );//desviacion es 4
				 if(!puedeCrearPrueba){
						utilErrores.addError("No se puede crear prueba " + sbError.toString());					
					}else {
						Prueba p = crearPruebaService.crearPrueba(revision, 4);
						revision.getPruebas().add(p);
					}
					
				break;
			case "Luces":
				 
				 puedeCrearPrueba = puedeCrearPruebaS.puedeCrearPruebaService(sbError, revision.getRevisionId(), new Integer(2) );//luces es 2
				 if(!puedeCrearPrueba){
						utilErrores.addError("No se puede crear prueba " + sbError.toString());					
					}else {
						Prueba p = crearPruebaService.crearPrueba(revision, 2);
						revision.getPruebas().add(p);
					}	
				break;
			case "Sonido":
				 existePruebaNoFinalizada =	pruebasService.existePruebaNoFinalizada(revision.getRevisionId(),7);//sonido es 7
					if(existePruebaNoFinalizada){
						utilErrores.addError("Existe prueba no terminada de sonido");
					}else{
						Prueba p = crearPruebaService.crearPrueba(revision, 7);
						revision.getPruebas().add(p);
					}
				break;
			case "Foto":
				 existePruebaNoFinalizada =	pruebasService.existePruebaNoFinalizada(revision.getRevisionId(),3);//fot es 3
					if(existePruebaNoFinalizada){
						utilErrores.addError("Existe prueba no terminada de foto");
					}else{
						Prueba p = crearPruebaService.crearPrueba(revision, 3);
						revision.getPruebas().add(p);
					}
				break;
			case "Taximetro":
				 puedeCrearPrueba = puedeCrearPruebaS.puedeCrearPruebaService(sbError, revision.getRevisionId(), new Integer(9) );//taxim es 9
				 if(!puedeCrearPrueba){
						utilErrores.addError("No se puede crear prueba " + sbError.toString());					
					}else {
						Prueba p = crearPruebaService.crearPrueba(revision, 9);
						revision.getPruebas().add(p);
					}
				break;
			default:
				utilErrores.addError("Error prueba no in case");
				break;
				
			}
		}
		catch(EJBAccessException e) {
			utilErrores.addError("Operacion no autorizada para el usuario");			
			logger.log(Level.SEVERE, "Error creando prueba", e);
		}
		catch(Exception exc){
			utilErrores.addError("Error: " + exc.getMessage());			
			logger.log(Level.SEVERE, "Error creando prueba", exc);
		}
	}
	
	
	public void guardarCambios(){
		
		try{
			if(revision.getCerrada()){
				utilErrores.addError("No se pueden guardar cambios la revision esta cerrada");
				return;
			}
			if(revisionService.isRevisionVencidaPorQuinceDias(revision)){
				utilErrores.addError("No se pueden guardar cambios el plazo de quince dias \n para presentar el vehiculo de nuevo ha expirado");
				return;
			}			
			persistirPruebaService.persistirPruebas(revision.getPruebas(), revision);
			utilErrores.addInfo("OK");
			
		}catch(Exception exc){
			utilErrores.addError("Error" + exc.getMessage());			
			logger.log(Level.SEVERE, "Error al guardar pruebas", exc);
		}
		
		
	}
	
	
	public void quitarPrueba(ActionEvent ae){
		try {
			if(pruebaSeleccionada == null){
				utilErrores.addInfo("Ya existe prueba");
			}else{
				
				if(revision.getCerrada() == true ){
					utilErrores.addError("Error no puede realizar cambios a revision cerrada");
					return;
				}
				if(pruebaSeleccionada.getPruebaId() != null){
					borrarPruebaService.quitarPrueba(pruebaSeleccionada);
					revision.getPruebas().remove(pruebaSeleccionada);
				}else{
					revision.getPruebas().remove(pruebaSeleccionada);
				}
			}
			
		} catch(EJBAccessException e) {
			utilErrores.addError("Operacion no autorizada para el usuario");			
			logger.log(Level.SEVERE, "Error creando prueba", e);
		}catch (Exception e) {
			utilErrores.addError("Error" + e.getMessage());
			logger.log(Level.SEVERE, "Error quitando prueba", e);
		}
				
	}
	
	public void verDetallePrueba(ActionEvent ae){
		try{
			if(pruebaSeleccionada != null){
				if(pruebaSeleccionada.isFinalizada()){
					FacesContext.getCurrentInstance().getExternalContext().getFlash().put("prueba_id",pruebaSeleccionada.getPruebaId() );
					FacesContext.getCurrentInstance().getExternalContext().redirect("detalleprueba.xhtml?faces-redirect=true");
		        	//RequestContext.getCurrentInstance().openDialog("detalleprueba.xhtml", options, params);
				}else{
					utilErrores.addError("La prueba seleccionada no ha sido finalizada, no se editan comentarios");
				}
			}else {
				utilErrores.addError("Seleccione prueba");
			}
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error verDetallePruea", exc);
		}
	}
	
	public void verLogPrueba(ActionEvent ae){
		try{
			if(pruebaSeleccionada != null){
				if(pruebaSeleccionada.isFinalizada()){
					FacesContext.getCurrentInstance().getExternalContext().getFlash().put("prueba_id",pruebaSeleccionada.getPruebaId() );
					FacesContext.getCurrentInstance().getExternalContext().redirect("logpruebas.xhtml?faces-redirect=true");		        	
				}else{
					utilErrores.addError("La prueba seleccionada no ha sido finalizada");
				}
			}else {
				utilErrores.addError("Seleccione prueba");
			}
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error verDetallePruea", exc);
		}
	}
	


}
