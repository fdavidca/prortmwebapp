package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;

import com.proambiente.modelo.Defecto;
import com.proambiente.modelo.Inspeccion;
import com.proambiente.modelo.Medida;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Usuario;
import com.proambiente.webapp.service.InspeccionService;
import com.proambiente.webapp.service.PruebasService;
import com.proambiente.webapp.util.UtilErrores;

/**
 * Clase que sirve de controlador para la vista/pagina
 * detalleinspeccion.xhtml
 * @author FABIAN
 *
 */


@Named
@ViewScoped
public class DetalleInspeccionBean implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 160389229155079855L;

	private static final Logger logger = Logger
			.getLogger(DetalleInspeccionBean.class.getName());
	
	@Inject
	UtilErrores utilErrores;
	
	@Inject
	InspeccionService inspeccionService;
	
	@Inject
	PruebasService pruebaService;
	
	Usuario usuario;
	
	
	
	@PostConstruct
	public void init(){
		try{
			
			Integer inspeccionId= (Integer)FacesContext.getCurrentInstance().getExternalContext().getFlash().get("inspeccion_id");
			if(inspeccionId != null){
				inspeccion = inspeccionService.cargarInspeccion(inspeccionId);
				pruebas = inspeccion.getPruebas();			
				urlFotoUno = "/ServletFotos?inspeccion_id="+inspeccionId+"&"+"numero_foto="+1;
				urlFotoDos = "/ServletFotos?inspeccion_id="+inspeccionId+"&"+"numero_foto="+2;				
				logger.log(Level.INFO, "Inspeccion cargada: " + inspeccion.getInspeccionId());
			}			
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error consultando datos de inspecion", exc);
			utilErrores.addError("ERROR CONSULTANDO INSPECCION");
		}
	}

	private Inspeccion inspeccion;
	
	private List<Prueba> pruebas;
	
	private Prueba pruebaSeleccionada;

	private List<Medida> medidas;
	
	private List<Defecto> defectos;
	
	private String urlFotoUno;
	
	private String urlFotoDos;
	
	public String getUrlFotoUno() {
		return urlFotoUno;
	}

	public void setUrlFotoUno(String urlFotoUno) {
		this.urlFotoUno = urlFotoUno;
	}

	public Inspeccion getInspeccion() {
		return inspeccion;
	}

	public void setInspeccion(Inspeccion inspeccion) {
		this.inspeccion = inspeccion;
	}

	
	
	public String getUrlFotoDos() {
		return urlFotoDos;
	}

	public void setUrlFotoDos(String urlFotoDos) {
		this.urlFotoDos = urlFotoDos;
	}

	public List<Defecto> getDefectos() {
		return defectos;
	}

	public void setDefectos(List<Defecto> defectos) {
		this.defectos = defectos;
	}

	public List<Prueba> getPruebas() {
		return pruebas;
	}

	public void setPruebas(List<Prueba> pruebas) {
		this.pruebas = pruebas;
	}

	public List<Medida> getMedidas() {
		return medidas;
	}

	public void setMedidas(List<Medida> medidas) {
		this.medidas = medidas;
	}

	public Prueba getPruebaSeleccionada() {
		return pruebaSeleccionada;
	}

	public void setPruebaSeleccionada(Prueba pruebaSeleccionada) {
		this.pruebaSeleccionada = pruebaSeleccionada;
	}
	
	
	
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public void cargarMedidas(ActionEvent ae){
		if(pruebaSeleccionada != null){			
			utilErrores.addInfo("Medidas cargadas");
			medidas=pruebaService.obtenerMedidas(pruebaSeleccionada);
			logger.info("Medidas cargadas");
		}else{			
			utilErrores.addError("Seleccione prueba");
		}
	}
	
	
	
	public void cargarDefectos(ActionEvent ae){
		if(pruebaSeleccionada != null){
			defectos = pruebaService.obtenerDefectos(pruebaSeleccionada);
			logger.info("Defectos cargados");
		}else{
			utilErrores.addError("Seleccione prueba");
		}
	}
	
	
	public void cargarFoto(ActionEvent ae){
		String inspeccionId = String.valueOf(inspeccion.getInspeccionId());		
		urlFotoUno = "/ServletFotos?inspeccion_id="+inspeccionId;
	}
	
	
	
	
	
}
