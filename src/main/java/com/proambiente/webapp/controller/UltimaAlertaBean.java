package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.enterprise.context.SessionScoped;
import javax.faces.event.ActionEvent;

import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.Alerta;
import com.proambiente.modelo.NivelAlerta;
import com.proambiente.webapp.service.AlertasDeHoyNoAntendidasService;



@Named
@SessionScoped
public class UltimaAlertaBean implements Serializable{
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7410645805337351826L;

	private static final Logger logger = Logger
			.getLogger(UltimaAlertaBean.class.getName());

	private Date fechaInicial;
	private Date fechaFinal;
	
	@Inject
	AlertasDeHoyNoAntendidasService alertaService;
	
	private List<Alerta> alertasHoy;
	
	
	public void consultarAlertas(ActionEvent ae){
		try {
			
			alertasHoy = alertaService.consultarAlertasHoy();
			
		} catch (Exception e) {			
			logger.log(Level.SEVERE, "Error consultando pruebas de ruido", e);
		}
	}
	
	
	
	public Date getFechaInicial() {
		return fechaInicial;
	}
	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	public Date getFechaFinal() {
		return fechaFinal;
	}
	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}



	public Alerta getUltimaAlerta() { 
				
		if(alertasHoy != null && !alertasHoy.isEmpty()) {
			Optional<Alerta> ultimaAlerta = alertasHoy.stream()
			        .filter(a->a.getNivel().equals(NivelAlerta.ERROR) || a.getNivel().equals(NivelAlerta.ERROR_GRAVE))
			        .max(Comparator.comparing(Alerta::getFecha));
			if(ultimaAlerta.isPresent())
			return ultimaAlerta.get();
			else {
				return new Alerta();
			}
		}else {
			return new Alerta();
		}
		
	}
	
	
		
	
}
