
package com.proambiente.webapp.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.Aseguradora;
import com.proambiente.modelo.Ciudad;
import com.proambiente.modelo.ClaseVehiculo;
import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.LineaVehiculo;
import com.proambiente.modelo.Llanta;
import com.proambiente.modelo.Marca;
import com.proambiente.modelo.Pais;
import com.proambiente.modelo.Servicio;
import com.proambiente.modelo.ServicioEspecial;
import com.proambiente.modelo.TipoCarroceria;
import com.proambiente.modelo.TipoCombustible;
import com.proambiente.modelo.TipoDocumentoIdentidad;
import com.proambiente.modelo.TipoMuestra;
import com.proambiente.modelo.TipoVehiculo;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.webapp.dao.AseguradoraFacade;
import com.proambiente.webapp.dao.CiudadFacade;
import com.proambiente.webapp.dao.ClaseVehiculoFacade;
import com.proambiente.webapp.dao.InformacionCdaFacade;
import com.proambiente.webapp.dao.LineaVehiculoFacade;
import com.proambiente.webapp.dao.LlantaFacade;
import com.proambiente.webapp.dao.MarcaFacade;
import com.proambiente.webapp.dao.PaisFacade;
import com.proambiente.webapp.dao.ServicioEspecialFacade;
import com.proambiente.webapp.dao.ServicioFacade;
import com.proambiente.webapp.dao.TipoCarroceriaFacade;
import com.proambiente.webapp.dao.TipoCombustibleFacade;
import com.proambiente.webapp.dao.TipoDocumentoIdentidadFacade;
import com.proambiente.webapp.dao.TipoVehiculoFacade;
import com.proambiente.webapp.dao.UsuarioFacade;

/**
 *Singleton o instancia para cargar las opciones de los combos
 *en los que se selecciona la marca,linea, etc al crear una automovil
 * @author FABIAN
 */
@ApplicationScoped
public class OpcionesRevisionBean {
	
	private Pais paisPorDefecto;
	private Llanta llantaPorDefecto;
	private TipoCombustible tipoCombustiblePorDefecto;
	private Aseguradora aseguradoraPorDefecto;
	private Servicio servicioPorDefecto;
	private ServicioEspecial servicioEspecialPorDefecto;
	private ClaseVehiculo clasePorDefecto;
	private TipoVehiculo tipoPorDefecto;
	private Marca marcaPorDefecto;
	private LineaVehiculo lineaPorDefecto;
	private List<TipoCombustible> listaCombustibles;
	private List<ClaseVehiculo> listaClasesVehiculo;
	private List<Servicio> listaServicios;
	private List<Pais> listaPaises;
	private List<Ciudad> listaCiudades;
	private List<String> listaTiposIdentificacion;
	private List<Llanta> listaLlantas;
	private List<TipoCarroceria> listaTiposCarroceria;
	private List<TipoDocumentoIdentidad> listaTiposDocumentoIdentidad;
	private InformacionCda infoCda;
	
	@PostConstruct
	public void init(){
		Integer idPais = 90;
		paisPorDefecto = paisDAO.find(idPais);
    	listaCombustibles =  tipoCombustibleDAO.findAll();
    	listaClasesVehiculo = claseVehiculoFacade.findAll();
		listaServicios = servicioDAO.findAll();
		listaPaises = paisDAO.findAll();
		listaTiposCarroceria = tipoCarroceriaFacade.findAll();
		tipoCombustiblePorDefecto = tipoCombustibleDAO.find(1);
		aseguradoraPorDefecto = aseguradoraDAO.find(1);
		servicioPorDefecto = servicioDAO.find(3);
		marcaPorDefecto = marcaDAO.find(0);
		lineaPorDefecto = lineaDAO.find(0);
		String[] tiposIdentificacion = {"CC", "NIT", "CE", "U", "P", "T"};
		listaTiposIdentificacion = Arrays.asList(tiposIdentificacion);
		listaCiudades = ciudadDAO.findAll();
		listaLlantas = llantaDAO.findAll();
		llantaPorDefecto = listaLlantas.get(1);
		servicioEspecialPorDefecto = servicioEspecialDAO.find(1);
		listaTiposDocumentoIdentidad = tipoDocumentoIdentidadDAO.findAll();		
		refrescarOpciones();
		
	}

	private void refrescarOpciones() {
		
		infoCda = informacionCdaDAO.find(1);
		
    	
		if(infoCda.isEsPistaMotos()){
			clasePorDefecto = claseVehiculoFacade.find(10);
			tipoPorDefecto = tipoVehiculoDAO.find(4);
		}else{
			clasePorDefecto = claseVehiculoFacade.find(1);
			tipoPorDefecto = tipoVehiculoDAO.find(1);
		}
		
		
	}
	
    
    @Inject
    ClaseVehiculoFacade claseVehiculoFacade;
    
    @Inject
    MarcaFacade marcaDAO;
    
    @Inject
    TipoVehiculoFacade tipoVehiculoDAO;
    
    @Inject
    ServicioFacade servicioDAO;
    
    @Inject
    PaisFacade paisDAO;
    
    @Inject
    TipoCombustibleFacade tipoCombustibleDAO;
    
    @Inject
    AseguradoraFacade aseguradoraDAO;
    
    @Inject
    CiudadFacade ciudadDAO;
    
    @Inject
    UsuarioFacade usuarioDAO;
    
    @Inject
    LlantaFacade llantaDAO;
    
    @Inject
    LineaVehiculoFacade lineaDAO;
    
    @Inject
    ServicioEspecialFacade servicioEspecialDAO;
    

    @Inject
    InformacionCdaFacade informacionCdaDAO;
    
    @Inject
    TipoDocumentoIdentidadFacade tipoDocumentoIdentidadDAO;
    
    @Inject
    TipoCarroceriaFacade tipoCarroceriaFacade;
    
    @Named
    @Produces
    public List<TipoCarroceria> getListaTiposCarroceria(){
    	return listaTiposCarroceria;
    }
    
    @Named
    @Produces
    public List<ClaseVehiculo> getListaClases(){    	
    	return listaClasesVehiculo;        
    }
    
    @Named
    @Produces
    List<Marca> getListaMarcas(){    	
    	return marcaDAO.findAll();
    }
    
    @Named
    @Produces
    List<TipoVehiculo> getListaTipoVehiculos(){
    	return tipoVehiculoDAO.findAll();
    }
    
    @Named
    @Produces
    public List<Servicio> getListaServicios(){
    	return listaServicios;
    }
    
    @Named
    @Produces
    List<Pais> getListaPaises(){
    	return listaPaises;
    }
    
    @Named
    @Produces
    List<TipoCombustible> getListaTiposCombustible(){
    	return listaCombustibles;
    }
    
    @Named
    @Produces
    List<Aseguradora> getListaAseguradoras(){
    	return aseguradoraDAO.findAll();
    }
    
    @Named
    @Produces
    List<Ciudad> getListaCiudades(){
    	return listaCiudades;
    }
  
    @Named
    @Produces
    List<String> getListaTiposIdentificacion(){
    	return listaTiposIdentificacion;
    }
    
    
  
    
    @Named
    @Produces
    List<Llanta> getListaLlantas(){
    	return listaLlantas;
    }
    
    @Named
    @Produces
    List<TipoDocumentoIdentidad> getListaTiposDocumentoIdentidad(){
    	return listaTiposDocumentoIdentidad;
    }
    
    @Named
    @Produces
    List<TipoMuestra> getListaTipoMuestra(){
    	TipoMuestra[] tipos = TipoMuestra.values();
    	List<TipoMuestra> lista = Arrays.asList(tipos);    	
    	return lista;
    	
    }
    
    @VehiculoDefault
    @Produces
    public Vehiculo getVehiculoDefault(){
    	refrescarOpciones();
    	Vehiculo vehiculo = new Vehiculo();
    	vehiculo.setNumeroEjes(2);
    	if(infoCda.isEsPistaMotos()){
    		vehiculo.setNumeroSillas(2);
    	}else{
    		vehiculo.setNumeroSillas(5);
    	}
    	vehiculo.setFechaMatricula( new Timestamp( (new Date()).getTime() ));
    	vehiculo.setTiemposMotor(4);
    	vehiculo.setNumeroExostos(1);
    	vehiculo.setKilometraje("0");
    	vehiculo.setCilindraje(0);
    	vehiculo.setPais(paisPorDefecto);
    	vehiculo.setFechaSoat(new Timestamp( (new Date()).getTime() ));
    	vehiculo.setTipoCombustible(tipoCombustiblePorDefecto);
    	vehiculo.setAseguradora(aseguradoraPorDefecto);
    	vehiculo.setClaseVehiculo(clasePorDefecto);
    	vehiculo.setServicio(servicioPorDefecto);
    	vehiculo.setMarca(marcaPorDefecto);
    	vehiculo.setLineaVehiculo(lineaPorDefecto);
    	vehiculo.setServicioEspecial(servicioEspecialPorDefecto);
    	vehiculo.setTipoVehiculo(tipoPorDefecto);
    	vehiculo.setDiseno("CONVENCIONAL");
    	vehiculo.setLlanta(llantaPorDefecto);
    	vehiculo.setConversionGnv("NO");
    	vehiculo.setCatalizador("SI");
    	return vehiculo;
    	
    }
    
    
   
    
}
