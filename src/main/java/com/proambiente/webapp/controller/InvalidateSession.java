package com.proambiente.webapp.controller;

import java.io.IOException;
import java.io.Serializable;

import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

@Named
@SessionScoped
public class InvalidateSession implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void timeout(){
		
		try {
			
			FacesContext.getCurrentInstance().getExternalContext()
	        .invalidateSession();
			System.out.println("Sesion invalidada por inactividad");
			
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}
	
}
