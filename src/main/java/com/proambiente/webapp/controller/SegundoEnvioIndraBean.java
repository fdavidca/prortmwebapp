package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;
import org.tempuri.Resultado;

import com.proambiente.indra.cliente.FURRespuesta;
import com.proambiente.modelo.Revision;
import com.proambiente.webapp.service.AlertasDeHoyService;
import com.proambiente.webapp.service.PinCi2Service;
import com.proambiente.webapp.service.RegistrarSegundoEnvioService;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.service.ci2.FurCi2CodificadorV2;
import com.proambiente.webapp.service.ci2.ManejadorCi2SicovService2V2;
import com.proambiente.webapp.service.ci2.OperacionesSicovCi2V3;
import com.proambiente.webapp.service.guardarpdf.GuardarPdfCallback;
import com.proambiente.webapp.service.guardarpdf.GuardarPdfCallbackFactory;
import com.proambiente.webapp.service.indra.OperacionesSicovIndraV2;
import com.proambiente.webapp.service.notification.NotificationPrimariaService;
import com.proambiente.webapp.service.sicov.OperacionesSicov;
import com.proambiente.webapp.service.sicov.OperacionesSicovProducer;
import com.proambiente.webapp.util.UtilErrores;

@Named
@ViewScoped
public class SegundoEnvioIndraBean implements Serializable	{
	
	private static final Logger logger = Logger.getLogger(SegundoEnvioIndraBean.class
			.getName());
	private static final long serialVersionUID = 1L;
	
	
	
	@Resource
	private ManagedExecutorService managedExecutorService;
	
	@Inject
	UtilErrores utilErrores ;
	
	@Inject
	AlertasDeHoyService alertaService;
	
	@Inject
	transient FurCi2CodificadorV2 ci2Service;
	
	@Inject
	RevisionService revisionService;
	
	@Inject
	GuardarPdfCallbackFactory guardarPdfCallbackFactory;
	
	@Inject
	RegistrarSegundoEnvioService registrarSegundoEnvioService;
	
	@Inject
	NotificationPrimariaService notificationService;
	
	@Inject
	ManejadorCi2SicovService2V2 manejadorCi2;
	
	@Inject
	OperacionesSicovProducer sicovProducer;
	
	@Inject
	PinCi2Service pinCi2Service;
	
	@Inject
	transient UtilNumeroCertificadoBean utilNumeroCertificado;
	
	@Inject
	NotificationPrimariaService notificacionService;
	
	
	private String mensaje = "-";
	
	private Integer numeroRevision ;
	
	private Integer revisionId;
	
	private String placa;
	
	private String pin;
    
   
    private Resultado resultado;
    
    private Boolean segundoEnvioExitoso = Boolean.FALSE;
    
	
	public void realizarSegundoEnvioFormulario(ActionEvent ae){
		try{
			Revision revision = revisionService.cargarRevision(revisionId);
			if(revision.getPreventiva()){
				utilErrores.addError("La revision es preventiva");
				return;
			}
			String placaRevision = revision.getVehiculo().getPlaca();
			this.placa = placaRevision;

			String evaluacionRevision = revisionService.evaluarRevision(revisionId);
			if(evaluacionRevision.equalsIgnoreCase("NO_FINALIZADA")){
				utilErrores.addError("La revisión no ha sido finalizada");
				return;
			}else {
					String consecutivoPreimpreso = utilNumeroCertificado.numeroCertificado(revisionId);
					if(consecutivoPreimpreso.equalsIgnoreCase("----")) {
						utilErrores.addError("La revision no tiene certificado asociado");
						return;
					}
					OperacionesSicov opSicov = sicovProducer.crearOperacionSicov();
					GuardarPdfCallback guardarPdfCallback = guardarPdfCallbackFactory.crearInstancia();
					if(opSicov instanceof OperacionesSicovCi2V3){						
						
							OperacionesSicovCi2V3 opCi2 = (OperacionesSicovCi2V3)opSicov;
							opCi2.registrarInformacionCertificado(revision.getRevisionId(), 
									consecutivoPreimpreso, revision.getConsecutivoRunt(),guardarPdfCallback);						
					
					}else if ( opSicov instanceof OperacionesSicovIndraV2) {
						StringBuilder sb = new StringBuilder();
						Optional<FURRespuesta> furRespuestaOpt = ((OperacionesSicovIndraV2) opSicov).registrarInformacionRuntSincrono(revisionId, sb);
						this.mensaje = sb.toString();
						resultado = new Resultado();
						this.setPin(revision.getPin());
						if(furRespuestaOpt.isPresent()) {
							
							FURRespuesta r = furRespuestaOpt.get();							
							resultado.setMensajeRespuesta(r.getMsjRespuesta());
							if(r.getCodRespuesta() == 0) {
								this.segundoEnvioExitoso = false;
								resultado.setCodigoRespuesta("Error ");
							}else {
								this.segundoEnvioExitoso = true;
								resultado.setCodigoRespuesta("Operacion Exitosa");
							}
							
						}				
					}
				}					
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error realizando segundo envio ci2", exc);
			utilErrores.addError("Error realizando segundo envío ci2");
		}
	}
	
	public void registrarSegundoEnvio(ActionEvent ae) {
		try {
			
			registrarSegundoEnvioService.registrarSegundoEnvio(revisionId, this.segundoEnvioExitoso);
			utilErrores.addInfo("Registro Ok");
			
		}catch(Exception exc) {
			utilErrores.addError("Error registrando primer envio");
			logger.log(Level.SEVERE, "Error registrando primer envio", exc);
		}
	}
	

	public Resultado getResultado() {
		return resultado;
	}



	public void setResultado(Resultado resultado) {
		this.resultado = resultado;
	}


	

	public Integer getRevisionId() {
		return revisionId;
	}

	public void setRevisionId(Integer revisionId) {
		this.revisionId = revisionId;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public Integer getNumeroRevision() {
		return numeroRevision;
	}

	public void setNumeroRevision(Integer numeroRevision) {
		this.numeroRevision = numeroRevision;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}


	public Boolean getSegundoEnvioExitoso() {
		return segundoEnvioExitoso;
	}


	public void setSegundoEnvioExitoso(Boolean segundoEnvioExitoso) {
		this.segundoEnvioExitoso = segundoEnvioExitoso;
	}

	
	
}
