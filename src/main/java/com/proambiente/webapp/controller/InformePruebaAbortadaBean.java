package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.event.ActionEvent;
import org.omnifaces.cdi.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.Prueba;
import com.proambiente.webapp.dao.UsuarioFacade;
import com.proambiente.webapp.service.PruebaAbortadaService;

import com.proambiente.webapp.util.UtilErrores;

@Named
@ViewScoped
public class InformePruebaAbortadaBean implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8369252348485702933L;

	private static final Logger logger = Logger
			.getLogger(InformePruebaAbortadaBean.class.getName());
	
	@Inject
	transient PruebaAbortadaService pruebaAbortadaService;
	
	@Inject
	UtilErrores utilErrores;
	
	@Inject
	UsuarioFacade usuarioDAO;
	
	
	
	private Date fechaInicial;
	private Date fechaFinal;
	
	private List<Prueba> pruebasConsultadas;

	public void consultarPruebas(ActionEvent ae){
		try{
			
			pruebasConsultadas = pruebaAbortadaService.consultarPruebaAbortadaPorFecha(fechaInicial, fechaFinal);
			
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error consultando prueba de fugas", exc);
			utilErrores.addError("Error consultando prueba de fugas");
		}
	}
		
	public Date getFechaInicial() {
		return fechaInicial;
	}
	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	public Date getFechaFinal() {
		return fechaFinal;
	}
	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public List<Prueba> getPruebasConsultadas() {
		return pruebasConsultadas;
	}

	public void setPruebasConsultadas(List<Prueba> pruebasConsultadas) {
		this.pruebasConsultadas = pruebasConsultadas;
	}
	
	
	
}
