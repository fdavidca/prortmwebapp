package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.event.ActionEvent;
import org.omnifaces.cdi.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.Aseguradora;
import com.proambiente.webapp.dao.AseguradoraFacade;
import com.proambiente.webapp.service.ListaEnviosService;
import com.proambiente.webapp.util.UtilErrores; 

@Named
@ViewScoped
public class ListaEnviosBean implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8419116536696167400L;

	private static final Logger logger = Logger.getLogger(ListaEnviosBean.class
			.getName());
	
	@Inject
	ListaEnviosService listaEnviosService;

	@Inject
	UtilErrores utilErrores;
	
	
	
	public Set<Integer> getListaEnvios(){
		return listaEnviosService.getListaEnvios();
	}



	public void limpiarLista(ActionEvent ae){
		try{
			listaEnviosService.limpiar();
			utilErrores.addInfo("Lista Envios En Proceso limpia");			
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error creando aseguradora", exc);
			utilErrores.addError("Error creando aseguradora");			
		}	
	}
		
}
