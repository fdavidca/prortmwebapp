package com.proambiente.webapp.controller;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.proambiente.webapp.service.sicov.UtilEncontrarPruebas.encontrarPruebaPorTipo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.inject.Inject;

import org.primefaces.model.FilterMeta;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortMeta;

import com.proambiente.modelo.Certificado;
import com.proambiente.modelo.Defecto;
import com.proambiente.modelo.Medida;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.modelo.dto.ResultadoDieselDTO;
import com.proambiente.modelo.dto.ResultadoFasDTO;
import com.proambiente.modelo.dto.ResultadoLucesDTO;
import com.proambiente.modelo.dto.ResultadoOttoMotocicletasDTO;
import com.proambiente.modelo.dto.ResultadoOttoVehiculosDTO;
import com.proambiente.modelo.dto.ResultadoSonometriaDTO;
import com.proambiente.webapp.dao.MedidaFacade;
import com.proambiente.webapp.service.CertificadoService;
import com.proambiente.webapp.service.PruebasService;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.service.formatomedidas.FormateadorMedidasProducer;
import com.proambiente.webapp.service.generadordto.GeneradorDTOResultados;
import com.proambiente.webapp.service.util.FormateadorMedidasInterface;
import com.proambiente.webapp.service.util.UtilInformesMedidas;
import com.proambiente.webapp.service.util.UtilTipoVehiculo;
import com.proambiente.webapp.util.ConstantesMedidasGases;
import com.proambiente.webapp.util.ConstantesTiposCombustible;
import com.proambiente.webapp.util.ConstantesTiposPrueba;
import com.proambiente.webapp.util.ConstantesTiposVehiculo;
import com.proambiente.webapp.util.dto.InformeResultadoTotalDTO;
import com.proambiente.webapp.util.dto.ResultadoDieselDTOInformes;
import com.proambiente.webapp.util.resultadototal.ColocadorInformacionDesviacion;
import com.proambiente.webapp.util.resultadototal.ColocadorInformacionDiesel;
import com.proambiente.webapp.util.resultadototal.ColocadorInformacionLuces;
import com.proambiente.webapp.util.resultadototal.ColocadorInformacionSuspension;
import com.proambiente.webapp.util.resultadototal.ColocadorResultadoFrenos;
import com.proambiente.webapp.util.resultadototal.ColocadorResultadoMotos;
import com.proambiente.webapp.util.resultadototal.ColocadorResultadoOttosVehiculo;
import com.proambiente.webapp.util.resultadototal.UtilColocarInformacionCertificado;
import com.proambiente.webapp.util.resultadototal.UtilColocarInformacionRevision;
import com.proambiente.webapp.util.resultadototal.UtilColocarInformacionVehiculo;
import com.proambiente.webapp.util.resultadototal.UtilColocarProfundidadLabradoInforme;


@InformeSuperModel
public class LazyInformeSuperintendenciaDataModel extends LazyDataModel<InformeResultadoTotalDTO>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private static final Logger logger = Logger
			.getLogger(LazyInformeSuperintendenciaDataModel.class.getName());
	private Date fechaInicial,fechaFinal;
	
	@Resource
	private ManagedExecutorService managedExecutorService;
	
	
	@Inject PruebasService pruebaService;
	@Inject RevisionService revisionService;
	@Inject CertificadoService certificadoService;
	@Inject MedidaFacade medidaDAO;
	@Inject FormateadorMedidasProducer formateadorMedidasProducer;
	
	UtilInformesMedidas utilInformesMedidas = new UtilInformesMedidas();
	
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd hh:mm");
	
	
	

	@Override
	public List<InformeResultadoTotalDTO> load(int first, int pageSize, Map<String, SortMeta> sortBy, Map<String, FilterMeta> filterBy) {
		
		try{				
				List<InformeResultadoTotalDTO> datos = new ArrayList<InformeResultadoTotalDTO>();
				if(fechaInicial == null && fechaFinal == null){
					return null;
				}
				List<Revision> revisiones = consultarRevisionesPaginado(fechaInicial, fechaFinal,first,pageSize);
				this.setRowCount(revisiones.size());//!!!
				CountDownLatch contador = new CountDownLatch(revisiones.size());				
				FormateadorMedidasInterface formateador = formateadorMedidasProducer.formateadorMedidas();
				for (Revision revision : revisiones) {
					try {
						new ProcesarInformacionRevision(contador, revision, datos,formateador).run();
					}catch(Exception exc) {
						logger.log(Level.SEVERE,"Error procesando revision: "+ revision.getRevisionId(),exc);
					}
		        }							
				return datos;			
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error", exc);
			return null;
		}
	}

	
	
	public Date getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}	
	
	
	
	
	private List<Revision> consultarRevisionesPaginado(Date fechaInicial,Date fechaFinal,int primerResultado,int tamanioPagina){		
		List<Revision> revisiones = revisionService.consultarRevisionesRegistradasPaginado(fechaInicial, fechaFinal, false,primerResultado,tamanioPagina);
		return revisiones;
	}
	
	
	class ProcesarInformacionRevision implements Runnable{
		
		private CountDownLatch contador;
		private Revision revision;
		private List<InformeResultadoTotalDTO> datos;
		FormateadorMedidasInterface formateador;
		
		

		public ProcesarInformacionRevision(CountDownLatch contador,
				Revision revision, List<InformeResultadoTotalDTO> datos,FormateadorMedidasInterface formateador) {
			super();
			this.contador = contador;
			this.revision = revision;
			this.datos = datos;
			this.formateador = formateador;
		}



		@Override
		public void run() {
			try{
				Certificado certificado = null;
				try {
			     certificado = certificadoService.consultarUltimoCertificado(revision.getRevisionId());
				}catch(Exception exc) {
					logger.log(Level.SEVERE, "No se puede consultar certificado");
				}
				InformeResultadoTotalDTO informe = new InformeResultadoTotalDTO();
				
						
				new UtilColocarInformacionCertificado().colocarInformacionCertificado(certificado, informe);
				
	            new UtilColocarInformacionRevision().colocarInformacionRevision(revision, informe);
	            Vehiculo vehiculo = revision.getVehiculo();
	            new UtilColocarInformacionVehiculo().colocarInformacionVehiculo(vehiculo, informe);
	            
	            List<Prueba> pruebas = pruebaService.pruebasRevision( revision.getRevisionId() );
	            
	            GeneradorDTOResultados generadorResultados = new GeneradorDTOResultados();
	            
	            
	            //Generar Resultado de Ruido
	            Optional<Prueba> optPruebaRuidos = encontrarPruebaPorTipo(pruebas,ConstantesTiposPrueba.TIPO_PRUEBA_RUIDO);
	            
	            if(optPruebaRuidos.isPresent()){
	    			Prueba pruebaRuido = optPruebaRuidos.get();
	    			List<Medida> medidas = pruebaService.obtenerMedidas(pruebaRuido);
	    			utilInformesMedidas.ordenarListaMedidas(medidas);
	    			ResultadoSonometriaDTO resultado = generadorResultados.generarResultadoSonometria(medidas);
	    			informe.setRuidoEscape(resultado.getValor());
	            }else {
	            	
					logger.log(Level.INFO, "Revision sin resultado de ruido");
	            }
	          //Generar Resultado Luces
	            Boolean isMoto = UtilTipoVehiculo.isVehiculoMoto(vehiculo.getTipoVehiculo());
	            Boolean isPesado = UtilTipoVehiculo.isVehiculoPesado(vehiculo.getTipoVehiculo());
	            Optional<ResultadoLucesDTO> optResultadoLuces = generarResultadoLuces(generadorResultados, revision.getRevisionId(), pruebas, isMoto);
	            if(optResultadoLuces.isPresent()) {
	            	ResultadoLucesDTO resultado = optResultadoLuces.get();
	            	new ColocadorInformacionLuces().colocarInformacionLuces(resultado, informe);
	            }else {	            	
					logger.log(Level.INFO, "Revision sin resultado de luces");
	            }
	            //Generar Resultado Suspension
	            Optional<ResultadoFasDTO> optSuspension = generarResultadoSuspension(pruebas, generadorResultados);
	            if(optSuspension.isPresent()) {
	            	ResultadoFasDTO resultadoFas = optSuspension.get();
	            	new ColocadorInformacionSuspension().colocarInformacionSuspension(resultadoFas, informe);
	            }else {
	            	logger.log(Level.INFO, "Revision sin resultado de suspension");
	            }
	            //Generar Resultado Desviacion
	            Optional<ResultadoFasDTO> optDesviacion = generarResultadoDesviacion(pruebas, generadorResultados);
	            if(optDesviacion.isPresent()) {
	            	ResultadoFasDTO resultadoFas = optDesviacion.get();
	            	new ColocadorInformacionDesviacion().colocarInformacionDesviacion(resultadoFas, informe);
	            }else {
	            	logger.log(Level.INFO,"Revision sin resultado de desviacion");
	            }
	            //Resultado de frenos
	            Optional<Prueba> pruebaFrenos = encontrarPruebaPorTipo( pruebas , ConstantesTiposPrueba.TIPO_PRUEBA_FRENOS );
	    		if(pruebaFrenos.isPresent()){
	    			List<Medida> medidasFrenos = pruebaService.obtenerMedidas(pruebaFrenos.get());
	    			utilInformesMedidas.ordenarListaMedidas(medidasFrenos);
	    			ResultadoFasDTO resultadoFAS = generadorResultados.generarResultadoFAS(medidasFrenos);
	    			new ColocadorResultadoFrenos().colocarResultadoFrenos(resultadoFAS, informe);
	    		}else {
	    			logger.log(Level.INFO,"Revision sin resultado de frenos");
	    		}
	    		//llenar seccion de gases
	    		llenarSeccionGases(pruebas, vehiculo, generadorResultados, informe);  
	    		
	    		//profundidad de labrado
	    		List<Prueba> pruebasFrenosInspeccionSensorial = pruebas.stream().filter(
	    				p -> p.getTipoPrueba().getTipoPruebaId().equals(1) || p.getTipoPrueba().getTipoPruebaId().equals(5))
	    				.collect(Collectors.toList());
	    		
	    		List<Medida> medidas = medidaDAO.consultarMedidaProfundidadLabrado(pruebasFrenosInspeccionSensorial);
	    		
	    		new UtilColocarProfundidadLabradoInforme().colocarInformacionProfundidadLabrado(
	    					pruebasFrenosInspeccionSensorial, isMoto, isPesado,
	    					informe, formateador,
	    					medidas
	    				);
	    		
	            Thread.sleep(0,500000);
	            datos.add(informe);
			}catch(Exception ex){
				logger.log(Level.SEVERE, "Error consultando pruebas", ex);
			}finally{
				contador.countDown();
			}
		}
		
		
	}
	
	
	public Optional<ResultadoLucesDTO> generarResultadoLuces(GeneradorDTOResultados generador, Integer revisionId, List<Prueba> pruebas, Boolean isMoto) {
		
		Optional<Prueba> optPruebaLuces = encontrarPruebaPorTipo(pruebas,ConstantesTiposPrueba.TIPO_PRUEBA_LUCES);
		if(optPruebaLuces.isPresent()){
			Prueba pruebaLuces = optPruebaLuces.get();
			List<Medida> medidas = pruebaService.obtenerMedidas(pruebaLuces);
			if(medidas == null || medidas.isEmpty()) {
				logger.log(Level.SEVERE, "No hay medidas para  prueba de luces para revision: " + revisionId);
				return Optional.empty();
			};
			
			utilInformesMedidas.ordenarListaMedidas(medidas);
			ResultadoLucesDTO resultadoLuces = isMoto ? generador.generarResultadoLucesMoto(medidas) : generador.generarResultadoLuces(medidas);
			return Optional.of(resultadoLuces);
		}
		else {
			return Optional.empty();
		}	
	}
	
	public Optional<ResultadoFasDTO> generarResultadoSuspension(List<Prueba> pruebas,GeneradorDTOResultados generador){
		Optional<Prueba> pruebaSuspension = encontrarPruebaPorTipo(pruebas,ConstantesTiposPrueba.TIPO_PRUEBA_SUSPENSION);
		if(pruebaSuspension.isPresent()){
			List<Medida> medidasSuspension = pruebaService.obtenerMedidas(pruebaSuspension.get());
			utilInformesMedidas.ordenarListaMedidas(medidasSuspension);
			ResultadoFasDTO resultado = generador.generarResultadoFAS(medidasSuspension);
			return Optional.of( resultado );
		} else {
			return Optional.empty();
		}
	}
	
	public Optional<ResultadoFasDTO> generarResultadoDesviacion(List<Prueba> pruebas,GeneradorDTOResultados generador){
		Optional<Prueba> pruebaDesviacion = encontrarPruebaPorTipo(pruebas,ConstantesTiposPrueba.TIPO_PRUEBA_DESVIACION);
		if( pruebaDesviacion.isPresent() ) {
			List<Medida> medidasDesviacion = pruebaService.obtenerMedidas(pruebaDesviacion.get());
			utilInformesMedidas.ordenarListaMedidas(medidasDesviacion);
			ResultadoFasDTO resultado = generador.generarResultadoFAS(medidasDesviacion);
			return Optional.of(resultado);
		}else {
			return Optional.empty();
		}
	}
	
	public void llenarSeccionGases(List<Prueba> pruebas, Vehiculo vehiculo, 
									GeneradorDTOResultados generador,InformeResultadoTotalDTO informe) {
				
		
		Integer tipoCombustibleId = vehiculo.getTipoCombustible().getCombustibleId();
		Optional<Prueba> optPruebaGases = encontrarPruebaPorTipo(pruebas, ConstantesTiposPrueba.TIPO_PRUEBA_GASES);
		
		if (optPruebaGases.isPresent()) {	
			
			Prueba pruebaGases = optPruebaGases.get();
			if(!pruebaGases.isFinalizada()) {
				throw new RuntimeException("Prueba de gases no finalizada");
			}
			
			List<Medida> medidas = pruebaService.obtenerMedidas(pruebaGases);
			checkNotNull("No existe usuario registrado para prueba de gases");

			boolean tipoCombustibleEsDiesel = tipoCombustibleId.equals(ConstantesTiposCombustible.DIESEL)
					|| tipoCombustibleId.equals(ConstantesTiposCombustible.BIODIESEL)
					|| tipoCombustibleId.equals(ConstantesTiposCombustible.DIESEL_ELECTRICO);
			
			boolean tipoCombustibleVehiculoEsOtto = tipoCombustibleId.equals(ConstantesTiposCombustible.GASOLINA )
					|| tipoCombustibleId.equals(ConstantesTiposCombustible.GAS_NATURAL)
					|| tipoCombustibleId.equals(ConstantesTiposCombustible.GAS_GASOLINA)
					|| tipoCombustibleId.equals(ConstantesTiposCombustible.GASOLINA_ELECTRICO);
			
			List<Defecto> defectos = new ArrayList<>();
			if (tipoCombustibleEsDiesel) {
				
				ResultadoDieselDTO resultado = generador.generarResultadoDieselDTO(medidas, defectos);
				ResultadoDieselDTOInformes resultadoAdicional = generador.generarResultadoDieselDTOInformes(medidas, defectos);
				
				OptionalDouble opt = medidas.stream().filter( m-> (				
						m.getTipoMedida().getTipoMedidaId() == ConstantesMedidasGases.CODIGO_REV_RAL_CICLO_PRELIMINAR || 
						m.getTipoMedida().getTipoMedidaId() == ConstantesMedidasGases.CODIGO_REV_RAL_PRIMER_CICLO ||
						m.getTipoMedida().getTipoMedidaId() == ConstantesMedidasGases.CODIGO_REV_RAL_SEGUNDO_CICLO ||
						m.getTipoMedida().getTipoMedidaId() == ConstantesMedidasGases.CODIGO_REV_RAL_SEGUNDO_CICLO ) ).mapToDouble( m->m.getValor()).average();
				if(opt.isPresent()) {
					Integer revolucionesPromedio = new BigDecimal(opt.getAsDouble()).setScale(0,RoundingMode.HALF_EVEN).intValue();
					informe.setRpmRalentiDiesel(String.valueOf(revolucionesPromedio) );
				}
				new ColocadorInformacionDiesel().colocarInformacionDiesel(resultado, informe,resultadoAdicional);
				
			} else {					
				if (tipoCombustibleVehiculoEsOtto){
					
					
					Integer tipoVehiculo = vehiculo.getTipoVehiculo().getTipoVehiculoId();
					Boolean vehiculoEsMoto = tipoVehiculo.equals(ConstantesTiposVehiculo.MOTO) || tipoVehiculo.equals(ConstantesTiposVehiculo.MOTO_CARRO);
					Boolean vehiculoEsRemolque = tipoVehiculo.equals(ConstantesTiposVehiculo.REMOLQUE); 
					if(vehiculoEsMoto){
						Boolean isDosTiempos = vehiculo.getTiemposMotor().equals(2);
						ResultadoOttoMotocicletasDTO resultado = generador.generarResultadoOttoMotos(medidas, defectos,
								isDosTiempos);
						new ColocadorResultadoMotos().colocarResultadoMotos(informe, resultado);				
						
					}else if(!vehiculoEsMoto && !vehiculoEsRemolque){							
						ResultadoOttoVehiculosDTO resultado = generador.generarResultadoOtto(medidas, defectos);						
						new ColocadorResultadoOttosVehiculo().colocarResultadoOtto(resultado, informe);
						
					}else{							
						logger.log(Level.INFO, "El vehiculo es remolque no se genera info");
					}
				}
			}

		} else {
				throw new RuntimeException("No hay prueba de gases para vehiculo " + vehiculo.getPlaca());
		}		
	}



	@Override
	public int count(Map<String, FilterMeta> filterBy) {
		
		return 0;
	}



	
}
