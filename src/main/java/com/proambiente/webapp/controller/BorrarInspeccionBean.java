package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;

import com.proambiente.webapp.service.BorrarInspeccionService;
import com.proambiente.webapp.util.UtilErrores;


@Named
@ViewScoped
public class BorrarInspeccionBean implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4509826282743272360L;

	private static final Logger logger = Logger
			.getLogger(BorrarInspeccionBean.class.getName());
	
	@Inject
	BorrarInspeccionService borrarInspeccionService;
	
	@Inject
	UtilErrores utilErrores;
	
	
	private String placa;
	
	private Integer revisionId;
	
	private Boolean deshabilitarBoton;

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	
	
	public BorrarInspeccionService getBorrarInspeccionService() {
		return borrarInspeccionService;
	}

	public void setBorrarInspeccionService(BorrarInspeccionService borrarInspeccionService) {
		this.borrarInspeccionService = borrarInspeccionService;
	}

	public Integer getRevisionId() {
		return revisionId;
	}

	public void setRevisionId(Integer revisionId) {
		this.revisionId = revisionId;
	}

	public void borrarInspeccion(ActionEvent ae){
		if(placa != null && !placa.isEmpty()){
			
		}else{
			utilErrores.addError("Ingrese placa");
		}
		
		if(revisionId == null) {
			utilErrores.addError("Ingrese id de revision");
		}
		
		try {
			borrarInspeccionService.borrarInspeccion(revisionId,placa);
			deshabilitarBoton = true;
			logger.log(Level.INFO,"Inspeccion borrada placa " + placa + "revision id " + revisionId);
			utilErrores.addInfo("Inspeccion borrada");
		}catch(Exception exc) {
			logger.log(Level.SEVERE,"Error borrando revision ",exc);
			utilErrores.addError(" Error borrando inspeccion" );
		}
	}
	
	public String finalizar() {
		return "/inicio.xhtml?faces-redirect=true";
	}

	public Boolean getDeshabilitarBoton() {
		return deshabilitarBoton;
	}

	public void setDeshabilitarBoton(Boolean deshabilitarBoton) {
		this.deshabilitarBoton = deshabilitarBoton;
	}

	
	
	
}