package com.proambiente.webapp.controller;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class HumedadTemperaturaBean {

	
	private Double humedad;
	private Double temperatura;
	private LocalDateTime fechaUltimaRecepcion;
	
	
	public Double getHumedad() {
		return humedad;
	}
	public void setHumedad(Double humedad) {
		this.humedad = humedad;
	}
	public Double getTemperatura() {
		return temperatura;
	}
	public void setTemperatura(Double temperatura) {
		this.temperatura = temperatura;
	}
	public LocalDateTime getFechaUltimaRecepcion() {
		return fechaUltimaRecepcion;
	}
	public void setFechaUltimaRecepcion(LocalDateTime fechaUltimaRecepcion) {
		this.fechaUltimaRecepcion = fechaUltimaRecepcion;
	}
	
	
	
}
