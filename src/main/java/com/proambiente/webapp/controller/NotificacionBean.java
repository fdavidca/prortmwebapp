package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.util.Map;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;

import org.omnifaces.util.Faces;
import org.primefaces.PrimeFaces;

import com.proambiente.webapp.util.UtilErrores;



@Named
@SessionScoped
public class NotificacionBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	UtilErrores utilErrores;
	
	private String alerta;

	private boolean resultadoEnvioVisible;

	public void pushed(Object event) {
		
		System.out.println("Pushed: " );
		Map<String, String[]> map = Faces.getRequestParameterValuesMap();
		String mensaje = Faces.getRequestParameter("mensaje");
		String nivel = Faces.getRequestParameter("nivel");
		this.alerta="Alerta recibida " + mensaje;
		this.resultadoEnvioVisible = true;
		
		if(nivel.equalsIgnoreCase("error")) {
			utilErrores.addError("Error: " + mensaje);
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_FATAL, nivel,mensaje);
			PrimeFaces.current().dialog().showMessageDynamic(message);
		}else {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, nivel,mensaje);
			utilErrores.addInfo(mensaje);
		}     
		
	}

	public String getAlerta() {
		return alerta;
	}

	public void setAlerta(String alerta) {
		this.alerta = alerta;
	}

	public boolean isResultadoEnvioVisible() {
		return resultadoEnvioVisible;
	}

	public void setResultadoEnvioVisible(boolean resultadoEnvioVisible) {
		this.resultadoEnvioVisible = resultadoEnvioVisible;
	}
	
	
	
		
	
}
