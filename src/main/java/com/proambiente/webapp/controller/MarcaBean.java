package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.event.ActionEvent;
import org.omnifaces.cdi.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;


import com.proambiente.modelo.LineaVehiculo;
import com.proambiente.modelo.Marca;
import com.proambiente.webapp.dao.MarcaFacade;
import com.proambiente.webapp.util.UtilErrores;

/**
 * Controlador para la vista de crear marcas y lineas
 * @author FABIAN
 */

@Named
@ViewScoped
public class MarcaBean implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3207124148429133109L;

	private static final Logger logger = Logger.getLogger(MarcaBean.class
			.getName());
	
	@Inject
	MarcaFacade marcaDAO;
	
	@Inject
	UtilErrores utilErrores;
	
	private Marca marcaSeleccionada;
	
	private String nombreMarca;
	
	private List<Marca> marcas;
	private List<LineaVehiculo> lineasVehiculo;
	private LineaVehiculo lineaNueva = new LineaVehiculo();
	private Marca marcaNueva = new Marca();

	public String getNombreMarca() {
		return nombreMarca;
	}

	public void setNombreMarca(String nombreMarca) {
		this.nombreMarca = nombreMarca;
	}

	public List<Marca> getMarcas() {
		return marcas;
	}

	public void setMarcas(List<Marca> marcas) {
		this.marcas = marcas;
	}
	
	
	
	public LineaVehiculo getLineaNueva() {
		return lineaNueva;
	}

	public void setLineaNueva(LineaVehiculo lineaNueva) {
		this.lineaNueva = lineaNueva;
	}

	public void buscarMarcas(ActionEvent ae){
		if(nombreMarca!=null&&!nombreMarca.isEmpty() ){
			marcas = marcaDAO.findMarcasByNombre(nombreMarca);
			utilErrores.addInfo("Seleccione marca de la tabla");
			//RequestContext.getCurrentInstance().execute("ocultarTableView()");
		}
	}

	public Marca getMarcaSeleccionada() {
		return marcaSeleccionada;
	}

	public void setMarcaSeleccionada(Marca marcaSeleccionada) {
		this.marcaSeleccionada = marcaSeleccionada;
	}

	public List<LineaVehiculo> getLineasVehiculo() {
		return lineasVehiculo;
	}

	public void setLineasVehiculo(List<LineaVehiculo> lineasVehiculo) {
		this.lineasVehiculo = lineasVehiculo;
	}
	
	public Marca getMarcaNueva() {
		return marcaNueva;
	}

	public void setMarcaNueva(Marca marcaNueva) {
		this.marcaNueva = marcaNueva;
	}

	public void buscarLineas(ActionEvent ae){
		try{
		if(marcaSeleccionada!=null){
			lineasVehiculo = marcaDAO.findLineaVehiculosByMarca(marcaSeleccionada);
		}else{
			utilErrores.addInfo("Seleccione marca de la tabla");
		}
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error buscando lineas", exc);
		}
	}
	
	public void nuevaLinea(ActionEvent ae){
		lineaNueva = new LineaVehiculo();
	}
	
	public void crearLinea(ActionEvent ae){
		if(lineaNueva != null && marcaSeleccionada != null){
			try{
				lineaNueva.setMarca(marcaSeleccionada);
				marcaDAO.guardarLinea(lineaNueva);
				utilErrores.addInfo("Linea creada");
				//RequestContext.getCurrentInstance().closeDialog(lineaNueva);
			}catch(Exception exc){
				logger.log(Level.SEVERE, "Error al crear linea", exc);
				utilErrores.addError("Error guardando linea");
			}
			
		}else{
			utilErrores.addInfo("Seleccione marca");
		}
	}
	
	public void crearMarca(ActionEvent ae){
		try {			
			marcaDAO.guardarMarca(marcaNueva);
			marcas = new ArrayList<Marca>();
			marcas.add(marcaNueva);
		//RequestContext.getCurrentInstance().execute("ocultarTableView()");
			utilErrores.addInfo("Marca creada");
		} catch (Exception e) {
			logger.log(Level.SEVERE,"Error al crear marca",e);
			utilErrores.addError("ERROR CREANDO MARCA");
		}
	}
}
