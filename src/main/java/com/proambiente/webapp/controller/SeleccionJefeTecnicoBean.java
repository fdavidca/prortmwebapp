package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;


import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Revision_;
import com.proambiente.modelo.Rol;
import com.proambiente.modelo.Usuario;
import com.proambiente.webapp.dao.InformacionCdaFacade;
import com.proambiente.webapp.dao.UsuarioFacade;
import com.proambiente.webapp.service.AsignarUsuarioResponsableService;
import com.proambiente.webapp.service.ConsecutivoRuntRevisionService;
import com.proambiente.webapp.service.RevisionIdPorConsecutivoRuntService;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.service.RolesService;
import com.proambiente.webapp.service.VehiculoService;
import com.proambiente.webapp.util.UtilErrores;


@Named
@ViewScoped
public class SeleccionJefeTecnicoBean implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3507818342204007194L;

	private static final Logger logger = Logger
			.getLogger(SeleccionJefeTecnicoBean.class.getName());
	
	@Inject
	UtilErrores utilErrores;
	
	@Inject
	RevisionService revisionService;
	
	@Inject
	VehiculoService vehiculoService;
	
	@Inject
	PrincipalService principalService;
	
	@Inject
	UsuarioFacade usuarioFacade;
	
	@Inject
	InformacionCdaFacade informacionCdaDAO;
	
	@Inject 
	transient ConsecutivoRuntRevisionService consecutivoRuntService;
	
	@Inject
	transient RevisionIdPorConsecutivoRuntService revisionIdPorCRService;
	
	@Inject
	RolesService rolesService;
	
	
	private String revisionId;

	private Revision revision;

	private String placa;
	
	private String mensaje;
	
	private Usuario usuarioSeleccionado;
	
	@Inject
	AsignarUsuarioResponsableService asignarUsuarioResponsableService; 
	
	
	private Set<Usuario> usuarios = new HashSet<>();
	
	@PostConstruct
	public void init(){
		Usuario usuario = principalService.getUsuarioAutenticado();
		Usuario usuarioRoles = usuarioFacade.atachUsuario(usuario);//cargar los roles
		for(Rol r: usuarioRoles.getRols()) {
			if(r.getNombreRol().equalsIgnoreCase("ADMINISTRADOR")) {
				usuarios.add(usuario);
			}
		}
		
		InformacionCda infoCda = informacionCdaDAO.find(1);
		Usuario usuarioResponsable = infoCda.getUsuarioResp();
		usuarios.add(usuarioResponsable);
	}
	
	
	
	public void guardar(){		
		try{
			rolesService.checkAdministrador();
			if(revision.getCerrada()) {
				utilErrores.addError("Revision ya ha finalizado");
			}
			asignarUsuarioResponsableService.asignarUsuarioResponsable(revision.getRevisionId(), usuarioSeleccionado.getUsuarioId());
			mensaje = ("Director tecnico asignado");
			FacesContext.getCurrentInstance().addMessage("form1:lista_usuarios", new FacesMessage(FacesMessage.SEVERITY_INFO,"Jefe Tecnico asignado correctamente","Jefe Tecnico asignado correctamente"));
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error ", exc);
			FacesContext.getCurrentInstance().addMessage("form1:lista_usuarios", new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error: " + exc.getMessage(),"Error " + exc.getMessage()));
		}
	}
	
	
	public void cancelar(){
		try{
			//RequestContext.getCurrentInstance().closeDialog(null);
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error editando revision", exc);
			utilErrores.addError("Error guardando cambios");
		}
	}
	
	
	
	public Revision getRevision() {
		return revision;
	}

	public void setRevision(Revision revision) {
		this.revision = revision;
	}

	public String getRevisionId() {
		return revisionId;
	}
	
	public String getMensaje()
	{
		return this.mensaje;
	}
	public void setRevisionId(String revisionId) {
		this.revisionId = revisionId;
		Integer revisionIdInt = Integer.valueOf(revisionId);
		revision = revisionService.cargarRevisionDTO(revisionIdInt,Revision_.consecutivoRunt,Revision_.numeroSolicitud,Revision_.revisionId);
		placa = vehiculoService.placaDesdeRevisionId(revisionIdInt);
	}

	public String getPlaca() {
		return placa;
	}


	public void setPlaca(String placa) {
		this.placa = placa;
	}



	public Set<Usuario> getUsuarios() {
		return usuarios;
	}



	public void setUsuarios(Set<Usuario> usuarios) {
		this.usuarios = usuarios;
	}



	public Usuario getUsuarioSeleccionado() {
		return usuarioSeleccionado;
	}



	public void setUsuarioSeleccionado(Usuario usuarioSeleccionado) {
		this.usuarioSeleccionado = usuarioSeleccionado;
	}
	
	

}

