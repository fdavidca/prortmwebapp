package com.proambiente.webapp.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.Analizador;
import com.proambiente.modelo.Usuario;
import com.proambiente.modelo.VerificacionLinealidad;
import com.proambiente.webapp.dao.AnalizadorFacade;
import com.proambiente.webapp.dao.UsuarioFacade;
import com.proambiente.webapp.dao.VerificacionLinealidadFacade;
import com.proambiente.webapp.util.UtilErrores;

@Named
@ViewScoped
public class InformeVerificacionesLinealidadBean implements Serializable {

	private static final Logger logger = Logger.getLogger(InformeVerificacionesLinealidadBean.class.getName());
	private static final long serialVersionUID = 1L;

	@Inject
	VerificacionLinealidadFacade verificacionLinealidadDAO;

	@Inject
	UsuarioFacade usuarioDAO;

	@Inject
	AnalizadorFacade analizadorDAO;

	@Inject
	UtilErrores utilErrores;

	private List<VerificacionLinealidad> verificaciones;

	private Date fechaInicial, fechaFinal;

	public void consultarVerificaciones(ActionEvent ae) {
		try {
			Calendar calendarFechaFinal = Calendar.getInstance();
			calendarFechaFinal.setTime(fechaFinal);
			calendarFechaFinal.add(Calendar.MONTH, -1);
			Calendar calendarFechaInicial = Calendar.getInstance();
			calendarFechaInicial.setTime(fechaInicial);
			if (calendarFechaFinal.compareTo(calendarFechaInicial) > 0) {
				utilErrores.addInfo("No se puede consultar mas de un mes de informacion");
				return;
			}

			logger.log(Level.INFO, "Verificaciones linealidad opacimetro fecha inicial {0}, fecha final {1}",
					new Object[] { fechaInicial, fechaFinal });
			verificaciones = verificacionLinealidadDAO.consultarVerificacionesLinealidad(fechaInicial, fechaFinal);

		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Error consultando verificaciones", exc);
			utilErrores.addError("ERROR" + exc.getMessage());
		}
	}

	public void verReporte(Integer verificacionId) {
		try {
			logger.log(Level.INFO, "Ver reporte de verificacion linealidad para {0}", verificacionId);
			FacesContext.getCurrentInstance().getExternalContext()
					.redirect("/prortmwebapp/ServletVerificacionLinealidad?id_verificacion_linealidad=" + verificacionId);
		} catch (IOException e) {
			utilErrores.addError("Erro cargando reporte de verificacion lienealidad");
			logger.log(Level.SEVERE, "Error cargando reporte de verificacion lienalidad", e);
		}
	}

	public String consultarMarcaAnalizador(String serial) {
		Analizador analizador = analizadorDAO.find(serial);
		String marca = analizador.getMarca() != null ? analizador.getMarca() : "No configurado";
		return marca;
	}

	public Usuario consultarUsuario(Integer idUsuario) {
		if (idUsuario == null)
			return null;
		else
			return usuarioDAO.find(idUsuario);
	}

	public List<VerificacionLinealidad> getVerificaciones() {
		return verificaciones;
	}

	public void setVerificaciones(List<VerificacionLinealidad> verificaciones) {
		this.verificaciones = verificaciones;
	}

	public Date getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

}
