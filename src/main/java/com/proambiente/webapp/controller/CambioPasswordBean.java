package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.omnifaces.cdi.ViewScoped;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import com.proambiente.modelo.Usuario;
import com.proambiente.webapp.dao.UsuarioFacade;
import com.proambiente.webapp.security.PasswordValidator;
import com.proambiente.webapp.service.CambioPasswordService;
import com.proambiente.webapp.service.PasswordHash;
import com.proambiente.webapp.util.UtilErrores;



@Named
@ViewScoped
public class CambioPasswordBean implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7326488952696930310L;

	private static final Logger logger = Logger
			.getLogger(CambioPasswordBean.class.getName());
	
	@Inject @UsuarioDefault Usuario usuarioAutenticado;
	
	@Inject UtilErrores utilErrores;
	
	@Inject UsuarioFacade usuarioDAO;
	
	@Inject PasswordValidator passwordValidator;
	
	@Inject CambioPasswordService cambioPasswordService;
	
	private String nuevoPassword;
	private String password;

	public Usuario getUsuarioAutenticado() {
		return usuarioAutenticado;
	}

	public void setUsuarioAutenticado(Usuario usuarioAutenticado) {
		this.usuarioAutenticado = usuarioAutenticado;
	}

	public String getNuevoPassword() {
		return nuevoPassword;
	}

	public void setNuevoPassword(String nuevoPassword) {
		this.nuevoPassword = nuevoPassword;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String cambiarPassword(){
		try{
			if(password != null && !password.isEmpty()
					&& nuevoPassword!=null && !nuevoPassword.isEmpty()){
				
				if(password.equalsIgnoreCase(nuevoPassword)){
					//verificar que no ingreso el mismo password que ya tenia
					if(PasswordHash.validatePassword(password, usuarioAutenticado.getContrasenia())){
						utilErrores.addInfo("Error no ingrese su password actual");
						return "";
					}else{
						String validacionPassword = passwordValidator.validarPassword(nuevoPassword);
						if(validacionPassword.isEmpty()){
							HttpServletRequest httpServletRequest = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
							String ip = getRemoteAddress(httpServletRequest);
							String password = PasswordHash.createHash(nuevoPassword);
							cambioPasswordService.cambiarPassword(usuarioAutenticado, password, ip);
							utilErrores.addInfo("Password correctamente modificado");
							return "/inicio.xhtml?faces-redirect=true";
						}else{
							utilErrores.addError("Password invalido " + validacionPassword);
							return "";
						}
					}
					
				}else{
					utilErrores.addError("Los password no coinciden");
					return "";
				}				
				
			}else{
				utilErrores.addInfo("Ingrese datos no deje vacio campos de password");
				return "";
			}		
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error haciendo cambio de passwrod", exc);
		}
		return "inicio.xhtml";
	}
	
	public static String getRemoteAddress(HttpServletRequest request) {
	    String ipAddress = request.getHeader("X-FORWARDED-FOR");
	    if (ipAddress != null) {
	        // cares only about the first IP if there is a list
	        ipAddress = ipAddress.replaceFirst(",.*", "");
	    } else {
	        ipAddress = request.getRemoteAddr();
	    }
	    return ipAddress;
	}

}
