package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.omnifaces.cdi.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;







import com.proambiente.modelo.Rol;
import com.proambiente.modelo.Usuario;
import com.proambiente.webapp.dao.RolFacade;
import com.proambiente.webapp.dao.UsuarioFacade;
import com.proambiente.webapp.security.PasswordValidator;
import com.proambiente.webapp.service.PasswordHash;
import com.proambiente.webapp.util.UtilErrores;

/**
 * Clase que sirve como controlador para la vista de editar usuarios
 * @author FABIAN
 *
 */

@Named
@ViewScoped
public class UsuariosBean implements Serializable{
	
	
	private static final Logger logger = Logger.getLogger(UsuariosBean.class
			.getName());
	private static final long serialVersionUID = 1L;

	@Inject
	UsuarioFacade usuarioDAO;
	
	@Inject
	RolFacade rolDAO;
	
	@Inject 
	UtilErrores utilErrores;
	
	@Inject transient PasswordValidator passwordValidator;
	
	Usuario usuarioSeleccionado;
	
	private Boolean deshabilitarGuardar;
	
	private List<Usuario> listaUsuarios;
	
	private List<Rol> roles;
	private List<Rol> rolesUsuario;
	
	private String password;
	private String confimacionPassword;
	
	public UsuariosBean(){
		usuarioSeleccionado = new Usuario();
		
	}
	
	@PostConstruct
	public void init(){
		listaUsuarios = usuarioDAO.findAll();
		roles = rolDAO.findAll();
	}

	public List<Usuario> getListaUsuarios() {
		return listaUsuarios;
	}

	public void setListaUsuarios(List<Usuario> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}

	public Usuario getUsuarioSeleccionado() {
		return usuarioSeleccionado;
	}

	public void setUsuarioSeleccionado(Usuario usuarioSeleccionado) {
		this.usuarioSeleccionado = usuarioSeleccionado;
	}

	public List<Rol> getRoles() {
		return roles;
	}

	public void setRoles(List<Rol> roles) {
		this.roles = roles;
	}
	
	public void guardarCambios(ActionEvent ae){
		try {
		if(usuarioSeleccionado.getUsuarioId() == null){
			if(!password.equals(confimacionPassword)){
				password="";
				confimacionPassword = "";
				utilErrores.addError("Los campos de password no coinciden");
				return;
			}else if (password.isEmpty() || confimacionPassword.isEmpty()){
				addError("Los campos de password no deben estar vacios");
				return;
			}else{
				String validacionPassword = passwordValidator.validarPassword(password);
				if(validacionPassword.isEmpty()){
					//usuarioSeleccionado.setContrasenia(PasswordHash.createHash(password));					
					//usuarioDAO.edit(usuarioAutenticado);
					utilErrores.addInfo("Password valido");					
				}else{
					utilErrores.addError("Password invalido " + validacionPassword);
					return;
				}
				usuarioSeleccionado.setContrasenia(password);
			}
		}else{
			if(!password.isEmpty() && !confimacionPassword.isEmpty()){
				if(!password.equals(confimacionPassword)){
					password="";
					confimacionPassword = "";
					addError("Los campos de password no coinciden");
					return;
				}else{
					String validacionPassword = passwordValidator.validarPassword(password);
					if(validacionPassword.isEmpty()){
						//usuarioSeleccionado.setContrasenia(PasswordHash.createHash(password));
						//usuarioDAO.edit(usuarioAutenticado);
						utilErrores.addInfo("Password valido");
						
					}else{
						utilErrores.addError("Password invalido " + validacionPassword);
						return;
					}									
					usuarioSeleccionado.setContrasenia(password);
				}
			}
		}
		
			usuarioDAO.guardarUsuario(usuarioSeleccionado,rolesUsuario,password);
			deshabilitarGuardar = true;
		} catch (NoSuchAlgorithmException e) {
			
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			
			e.printStackTrace();
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error guardando cambios de usuario", exc);
			utilErrores.addError("Error creando usuario");
		}
		listaUsuarios = usuarioDAO.findAll();
		password="";
		confimacionPassword="";
	}
	
	private void addError(String string) {
		FacesContext fc = FacesContext.getCurrentInstance();
		FacesMessage fm = new FacesMessage();
		fm.setSummary(string);
		fm.setSeverity(FacesMessage.SEVERITY_ERROR);
		fc.addMessage(string,fm);
				
		
	}

	public void atachUsuario(ActionEvent ae){
		if(usuarioSeleccionado.getUsuarioId() != null){
			usuarioSeleccionado = usuarioDAO.atachUsuario(usuarioSeleccionado);
			rolesUsuario = new ArrayList<>();
			for(Rol r : usuarioSeleccionado.getRols()){
				rolesUsuario.add(r);
			}
			password="";
			confimacionPassword="";
		}else{
			addError("Seleccione usuario para editar");
		}
	}

	public List<Rol> getRolesUsuario() {
		return rolesUsuario;
	}

	public void setRolesUsuario(List<Rol> rolesUsuario) {
		this.rolesUsuario = rolesUsuario;
	}
    
	public void nuevoUsuario(ActionEvent ae){
		usuarioSeleccionado = new Usuario();
		usuarioSeleccionado.setActivo(true);
		usuarioSeleccionado.setAdministrador("S");//no importa no se usa
		
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfimacionPassword() {
		return confimacionPassword;
	}

	public void setConfimacionPassword(String confimacionPassword) {
		this.confimacionPassword = confimacionPassword;
	}

	public Boolean getDeshabilitarGuardar() {
		return deshabilitarGuardar;
	}

	public void setDeshabilitarGuardar(Boolean deshabilitarGuardar) {
		this.deshabilitarGuardar = deshabilitarGuardar;
	}

	
}
