package com.proambiente.webapp.controller;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import org.omnifaces.cdi.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.Analizador;
import com.proambiente.webapp.dao.AnalizadorFacade;
import com.proambiente.webapp.util.UtilErrores;


@Named
@ViewScoped
public class AnalizadorCRUDBean implements Serializable	{
	
	
	private static final Logger logger = Logger.getLogger(UsuariosBean.class
			.getName());
	private static final long serialVersionUID = 1L;
	
	@Inject 
	UtilErrores utilErrores;
	
	@Inject
	AnalizadorFacade analizadorDAO;
	
	Analizador analizadorSeleccionado;
	
	List<Analizador> analizadores;

	public Analizador getAnalizadorSeleccionado() {
		return analizadorSeleccionado;
	}

	public void setAnalizadorSeleccionado(Analizador analizadorSeleccionado) {
		this.analizadorSeleccionado = analizadorSeleccionado;
	}

	public List<Analizador> getAnalizadores() {
		return analizadores;
	}

	public void setAnalizadores(List<Analizador> analizadores) {
		this.analizadores = analizadores;
	}
	
	@PostConstruct
	public void init(){
		analizadores = analizadorDAO.findAll();
	}
	
	public void guardarCambios(ActionEvent ae){
		try{
			
			analizadorDAO.edit(analizadorSeleccionado);
			
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error guardando cambios de analizador", exc);
			utilErrores.addError("Error editando datos de analizador");
		}
	}
	
	public void atachAnalizador(){
		
	}
	
}
