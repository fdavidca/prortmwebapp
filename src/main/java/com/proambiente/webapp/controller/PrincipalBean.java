package com.proambiente.webapp.controller;


import java.io.Serializable;
import java.util.logging.Logger;

import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.Usuario;
import com.proambiente.webapp.service.FechaService;

/**
 * Clase de utilidad para implementar el cierre de sesion de la aplicacion
 * @author FABIAN
 */


@Named
@SessionScoped
public class PrincipalBean implements Serializable {
	
	@Inject FechaService fechaService;
	
	
	
	@Inject PrincipalService principalService;
	
	
	
	private static final Logger logger = Logger.getLogger(PrincipalBean.class
			.getName());
	private static final long serialVersionUID = 1L;

	public String getUsuarioAutenticado(){
		
		return principalService.getNombreUsuarioSession();
	}
	
	public String getNombresApellidosUsuario() {
		Usuario usuario = principalService.getUsuarioAutenticado();
		if(usuario == null) {
			return "";
		}else {
			return usuario.getNombres() + " " + usuario.getApellidos(); 
		}
	}
	
	

}
