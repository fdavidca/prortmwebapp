package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.FiltroDensidadNeutra;
import com.proambiente.webapp.dao.FiltroDensidadNeutraFacade;
import com.proambiente.webapp.util.UtilErrores;

@Named
@ViewScoped
public class FiltrosCRUDBean implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 31L;

	private static final Logger logger = Logger.getLogger(FiltrosCRUDBean.class.getName());
	
	@Inject
	FiltroDensidadNeutraFacade filtroFacade;
	
	@Inject
	UtilErrores utilErrores;
	
	List<FiltroDensidadNeutra> listaFiltros;
	
	FiltroDensidadNeutra filtroDensidadNeutra;
	
	private Date fechaAuxiliar;
	
	@PostConstruct
	public void init(){
		
		listaFiltros = filtroFacade.findAll();		
	}

	public List<FiltroDensidadNeutra> getListaFiltros() {
		return listaFiltros;
	}

	public void setListaFiltros(List<FiltroDensidadNeutra> listaFiltros) {
		this.listaFiltros = listaFiltros;
	}

	public FiltroDensidadNeutra getFiltroDensidadNeutra() {
		return filtroDensidadNeutra;
	}

	public void setFiltroDensidadNeutra(FiltroDensidadNeutra filtroDensidadNeutra) {
		this.filtroDensidadNeutra = filtroDensidadNeutra;
	}

	public void guardarCambios(ActionEvent ae){
		
		try{
			Timestamp ts = new Timestamp(fechaAuxiliar.getTime());
			filtroDensidadNeutra.setFechaCalibracion(ts);
			filtroFacade.edit(filtroDensidadNeutra);
			listaFiltros = filtroFacade.findAll();
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error guardando filtro", exc);
			utilErrores.addError("Error guardando filtro");
		}
	}
	
	public void nuevoFiltro(ActionEvent ae){
		filtroDensidadNeutra = new FiltroDensidadNeutra();
	}

	public Date getFechaAuxiliar() {
		return fechaAuxiliar;
	}

	public void setFechaAuxiliar(Date fechaAuxiliar) {
		this.fechaAuxiliar = fechaAuxiliar;
	}
	
	
	
}
