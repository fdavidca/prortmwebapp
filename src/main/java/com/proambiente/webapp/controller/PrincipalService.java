package com.proambiente.webapp.controller;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;



import com.proambiente.modelo.Usuario;
import com.proambiente.webapp.dao.UsuarioFacade;

/**
 * Clase que presta el servicio de obtener la informacion del usuario autenticado.
 * @author FABIAN
 *
 */
@Stateless
public class PrincipalService {
	
	@Resource
	SessionContext sessionContext;
	
	@Inject
	UsuarioFacade usuarioDAO;
	
	public String getNombreUsuarioSession(){
		String usuarioActual = sessionContext.getCallerPrincipal().getName();
		return usuarioActual;
	}
	
	@Produces
	@UsuarioDefault
	public Usuario getUsuarioAutenticado(){
		String nombreUsuario = getNombreUsuarioSession();
		Usuario  usuario = usuarioDAO.buscarUsuarioPorNombreUsuario(nombreUsuario);
		return usuario;
	}
	

}
