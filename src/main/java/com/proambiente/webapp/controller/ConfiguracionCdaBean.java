package com.proambiente.webapp.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import org.omnifaces.cdi.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.io.IOUtils;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.primefaces.model.file.UploadedFile;

import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.ModoMostrarNumPasajeros;
import com.proambiente.modelo.ModoReporte;
import com.proambiente.modelo.OperadorSicov;
import com.proambiente.modelo.Usuario;
import com.proambiente.webapp.dao.InformacionCdaFacade;
import com.proambiente.webapp.dao.UsuarioFacade;
import com.proambiente.webapp.util.UtilErrores;

/**
 * Clase que sirve como controlador para la vista en la que se configuran los
 * datos del cda
 * 
 * @author FABIAN
 *
 */
@Named
@ViewScoped
public class ConfiguracionCdaBean implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7090076735068962799L;


	private static final Logger logger = Logger.getLogger(ConfiguracionCdaBean.class.getName());

	
	InformacionCda infoConfiguracionCda;
	private List<Usuario> usuariosAdministradores;

	@Inject
	InformacionCdaFacade informacionCdaDAO;

	@Inject
	UsuarioFacade usuarioDAO;

	@Inject
	UtilErrores utilErrores;
	
	

	private Date fechaResolucionAuxiliar;
	
	private Date fechaCambioLogoSuperintendeciaAuxiliar;

	private UploadedFile uploadedFile;

	private UploadedFile uploadedFileOnac;

	private Usuario usuarioResponsable;

	private String usuarioCi2;

	private String passwordCi2;

	@PostConstruct
	public void init() {
		try {
			infoConfiguracionCda = informacionCdaDAO.find(1);
			if (infoConfiguracionCda.getFechaResolucion() != null) {
				fechaResolucionAuxiliar = new Date(infoConfiguracionCda.getFechaResolucion().getTime());
			}
			if(infoConfiguracionCda.getFechaCambioLogoSuperintendenciaTransporte() != null) {
				fechaCambioLogoSuperintendeciaAuxiliar = new Date(infoConfiguracionCda.getFechaCambioLogoSuperintendenciaTransporte().getTime());
			}
			usuariosAdministradores = usuarioDAO.findUsuariosAdministradores();
			usuarioResponsable = infoConfiguracionCda.getUsuarioResp();
		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Error intentando cargar", exc);
			throw new RuntimeException(exc);
		}
	}
	

	public InformacionCda getInfoConfiguracionCda() {
		return infoConfiguracionCda;
	}

	public void setInfoConfiguracionCda(InformacionCda infoConfiguracionCda) {
		this.infoConfiguracionCda = infoConfiguracionCda;
	}

	public List<Usuario> getUsuariosAdministradores() {
		return usuariosAdministradores;
	}

	public void setUsuariosAdministradores(List<Usuario> usuariosAdministradores) {
		this.usuariosAdministradores = usuariosAdministradores;
	}

	public Usuario getUsuarioResponsable() {
		return usuarioResponsable;
	}

	public void setUsuarioResponsable(Usuario usuarioResponsable) {
		this.usuarioResponsable = usuarioResponsable;
	}

	public Date getFechaResolucionAuxiliar() {
		return fechaResolucionAuxiliar;
	}

	public void setFechaResolucionAuxiliar(Date fechaResolucionAuxiliar) {
		this.fechaResolucionAuxiliar = fechaResolucionAuxiliar;
	}

	public String getUsuarioCi2() {
		return usuarioCi2;
	}

	public void setUsuarioCi2(String usuarioCi2) {
		this.usuarioCi2 = usuarioCi2;
	}

	public String getPasswordCi2() {
		return passwordCi2;
	}

	public void setPasswordCi2(String passwordCi2) {
		this.passwordCi2 = passwordCi2;
	}
	
	

	public Date getFechaCambioLogoSuperintendeciaAuxiliar() {
		return fechaCambioLogoSuperintendeciaAuxiliar;
	}


	public void setFechaCambioLogoSuperintendeciaAuxiliar(Date fechaCambioLogoSuperintendeciaAuxiliar) {
		this.fechaCambioLogoSuperintendeciaAuxiliar = fechaCambioLogoSuperintendeciaAuxiliar;
	}


	public void upload() {
		if (uploadedFile != null) {
			InputStream is = null;
			try {
				String filename = uploadedFile.getFileName();
				if( filename == null  || filename.isEmpty() ) {
					utilErrores.addError("Especifique archivo");
					return;
				}
				logger.info("Archivo subido:" + filename);
				is = uploadedFile.getInputStream();
				byte[] bytes = IOUtils.toByteArray(is);
				logger.info("Tamani de archivo" + bytes.length);
				infoConfiguracionCda.setLogo(bytes);
				informacionCdaDAO.edit(infoConfiguracionCda);
				utilErrores.addInfo("Logo guardado");

			} catch (Exception e) {
				logger.log(Level.SEVERE, "Error subiendo archivo", e);
				utilErrores.addError("Error subiendo archivo");
			} finally {
				try {
					if(is != null)
						is.close();
				} catch (IOException e) {
					logger.log(Level.SEVERE, "Error subiendo archivo logo para fur", e);
				}
			}

		}
	}

	public void uploadFileOnac() {
		if (uploadedFileOnac != null) {
			InputStream is = null;
			try {
				String filename = uploadedFileOnac.getFileName();
				if( filename == null  || filename.isEmpty() ) {
					utilErrores.addError("Especifique archivo");
					return;
				}
				logger.info("Archivo subido:" + filename);
				is = uploadedFileOnac.getInputStream();
				byte[] bytes = IOUtils.toByteArray(is);
				logger.info("Tamani de archivo" + bytes.length);
				infoConfiguracionCda.setLogoOnac(bytes);
				informacionCdaDAO.edit(infoConfiguracionCda);
				utilErrores.addInfo("Logo Onac guardado");

			} catch (Exception e) {
				logger.log(Level.SEVERE, "Error subiendo archivo", e);
				utilErrores.addError("Error subiendo archivo");
			} finally {
				try {
					if(is != null)
						is.close();
				} catch (IOException e) {
					logger.log(Level.SEVERE, "Error subiendo archivo logo para fur", e);
				}
			}

		}
	}

	public UploadedFile getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(UploadedFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

	public UploadedFile getUploadedFileOnac() {
		return uploadedFileOnac;
	}

	public void setUploadedFileOnac(UploadedFile uploadedFileOnac) {
		this.uploadedFileOnac = uploadedFileOnac;
	}

	public void guardarCambios(ActionEvent ae) {
		try {
			
			if(infoConfiguracionCda.getUrlWebService() == null || infoConfiguracionCda.getUrlWebService().isEmpty()) {
				utilErrores.addError("Digite la url de web service");
				return;
			}
			infoConfiguracionCda.setUsuarioResp(usuarioResponsable);
			Timestamp fechaResolucion = new Timestamp(fechaResolucionAuxiliar.getTime());
			infoConfiguracionCda.setFechaResolucion(fechaResolucion);
			
			Timestamp fechaCambioLogoSuperintendencia = new Timestamp(fechaCambioLogoSuperintendeciaAuxiliar.getTime());
			infoConfiguracionCda.setFechaCambioLogoSuperintendenciaTransporte(fechaCambioLogoSuperintendencia);
			
			if (usuarioCi2 != null && !usuarioCi2.isEmpty() && passwordCi2 != null && !passwordCi2.isEmpty()) {
				StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
				encryptor.setPassword("cMB8apv23");
				String usuarioci2Encriptado = encryptor.encrypt(usuarioCi2);
				String passwordci2Encriptado = encryptor.encrypt(passwordCi2);

				infoConfiguracionCda.setUsuarioCi2(usuarioci2Encriptado);
				infoConfiguracionCda.setPasswordCi2(passwordci2Encriptado);
			}
			informacionCdaDAO.edit(infoConfiguracionCda);
			utilErrores.addInfo("Configuracion Guarada OK");

		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Error guardando configuracion", exc);
			utilErrores.addError("Error guardando configuracion" + exc.getMessage());
		}
		
	}
	
	public ModoReporte[] getModosReporte() {
		ModoReporte[] modos = {ModoReporte.MODO_ANTERIOR,ModoReporte.MODO_RESOLUCION_3625,ModoReporte.MODO_RESOLUCION_3625_REDONDEADO};
		return modos;
	}
	
	public ModoMostrarNumPasajeros[] getModosMostrarNumPasajeros() {
		ModoMostrarNumPasajeros[] modos = {ModoMostrarNumPasajeros.NO_RESTAR,ModoMostrarNumPasajeros.RESTAR_UNO};
		return modos;
	}
	
	public OperadorSicov[] getOperadoresSicov() {
		OperadorSicov[] operadoresSicov = {OperadorSicov.CI2,OperadorSicov.INDRA,OperadorSicov.SIN_OPERADOR};
		return operadoresSicov;
	}

}
