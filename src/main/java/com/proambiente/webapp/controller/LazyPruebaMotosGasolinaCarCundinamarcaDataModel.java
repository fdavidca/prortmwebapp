package com.proambiente.webapp.controller;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.inject.Inject;

import org.primefaces.model.FilterMeta;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortMeta;

import com.proambiente.modelo.Analizador;
import com.proambiente.modelo.ClaseVehiculo;
import com.proambiente.modelo.Defecto;
import com.proambiente.modelo.Equipo;
import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.Inspeccion;
import com.proambiente.modelo.LineaVehiculo;
import com.proambiente.modelo.Marca;
import com.proambiente.modelo.Medida;
import com.proambiente.modelo.Pipeta;
import com.proambiente.modelo.Propietario;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.PruebaFuga;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Servicio;
import com.proambiente.modelo.TipoCombustible;
import com.proambiente.modelo.Usuario;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.modelo.VerificacionGasolina;
import com.proambiente.modelo.dto.ResultadoOttoMotocicletasDTO;
import com.proambiente.webapp.dao.EquipoFacade;
import com.proambiente.webapp.dao.InformacionCdaFacade;
import com.proambiente.webapp.dao.PermisibleFacade;
import com.proambiente.webapp.dao.PipetaFacade;
import com.proambiente.webapp.dao.UsuarioFacade;
import com.proambiente.webapp.service.CertificadoService;
import com.proambiente.webapp.service.InspeccionService;
import com.proambiente.webapp.service.PruebaFugaAprobadaService;
import com.proambiente.webapp.service.PruebasService;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.service.VersionSoftwareService;
import com.proambiente.webapp.service.generadordto.GeneradorDTOResultados;
import com.proambiente.webapp.service.indra.UtilConvertirServicioSuperintendencia;
import com.proambiente.webapp.util.ConstantesMedidasKilometraje;
import com.proambiente.webapp.util.UtilErrores;
import com.proambiente.webapp.util.dto.InformeGasolinaMotocicletasCarDTO;
import com.proambiente.webapp.util.dto.ResultadoOttoInformeDTO;
import com.proambiente.webapp.util.dto.ResultadoSonometriaInformeDTO;
import com.proambiente.webapp.util.dto.sda.CausaRechazoMotocicletas;
import com.proambiente.webapp.util.informes.DeterminadorCausaRechazoMotos;
import com.proambiente.webapp.util.informes.medellin.GeneradorInformeMotoRes0762;
import com.proambiente.webapp.util.informes.medellin.InformeMotocicletasRes0762;

@PeubaGasesModelCarCundinamarca
public class LazyPruebaMotosGasolinaCarCundinamarcaDataModel extends LazyDataModel<InformeGasolinaMotocicletasCarDTO> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String SI = "SI";
	private static final String NO = "NO";

	private static final Logger logger = Logger.getLogger(LazyPruebaMotosGasolinaCarCundinamarcaDataModel.class.getName());

	@Inject
	PruebasService pruebaService;

	private Date fechaInicial, fechaFinal;

	@Inject
	private InformacionCdaFacade informacionCda;
	
	@Inject
	InspeccionService inspeccionService;

	@Inject
	private VersionSoftwareService versionSoftwareService;

	@Inject
	private UtilErrores utilErrores;
	
	@Inject
	CertificadoService certificadoService;
	
	@Inject
	private EquipoFacade equipoFacade;
	
	@Inject
	PermisibleFacade permisibleFacade;
	
	@Inject
	RevisionService revisionService;
	
	@Inject
	PipetaFacade pipetaFacade;
	
	@Inject
	UsuarioFacade usuarioFacade;
	
	@Inject
	PruebaFugaAprobadaService pruebaFugaAprobadaService;
	
	@Resource
	private ManagedExecutorService managedExecutorService;

	private InformacionCda infoCda;

	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

	private SimpleDateFormat sdfPrueba = new SimpleDateFormat("yyyy/MM/dd,HH:mm:ss");

	private String versionSoftware;

	@Override
	public List<InformeGasolinaMotocicletasCarDTO> load(int first, int pageSize, Map<String, SortMeta> sortBy, Map<String, FilterMeta> filterBy) {
		List<InformeGasolinaMotocicletasCarDTO> datos = new ArrayList<>();

		try {

			List<Prueba> pruebas = pruebaService.consultarPruebasOttoMotocicletasPaginado(fechaInicial, fechaFinal,
					first, pageSize);
			long numeroPruebas = pruebaService.contarPruebasOttoMotocicletas(fechaInicial, fechaFinal);
			this.setRowCount((int) numeroPruebas);
			if (pruebas.isEmpty()) {
				utilErrores.addError("No se encontraron pruebas");
				return null;
			}
			logger.info("Numero Pruebas Totales:" + numeroPruebas);
			infoCda = informacionCda.find(1);

			versionSoftware = versionSoftwareService.obtenerVersionSoftware();
			String nombreSoftware = "PRORTM";
			if (datos != null)
				datos.clear();
			datos = new ArrayList<>();
			
			DeterminadorCausaRechazoMotos determinadorRechazo = new DeterminadorCausaRechazoMotos(permisibleFacade);

			GeneradorDTOResultados generadorResultados  = new GeneradorDTOResultados();
			GeneradorInformeMotoRes0762 generadorInforme = new GeneradorInformeMotoRes0762();
			generadorResultados.setPuntoComoSeparadorDecimales();
			for (Prueba prueba : pruebas) {
				InformeGasolinaMotocicletasCarDTO informe = new InformeGasolinaMotocicletasCarDTO();
				logger.log(Level.INFO, "Procesando prueba : " + prueba.getPruebaId() );
				informe.setConsecutivoPrueba(String.valueOf(prueba.getPruebaId()));
				informe.setNumeroCda(infoCda.getNumeroCda());
				String consecutivoCertificado = "";
				try {
					 consecutivoCertificado = certificadoService.consultarUltimoConsecutivoCertificado(prueba.getRevision().getRevisionId());
				}catch(Exception exc) {
					logger.log(Level.SEVERE, "Error cargando certificado" + prueba.getRevision().getRevisionId() );
				}

				InformeMotocicletasRes0762 subinforme = new InformeMotocicletasRes0762();

				generadorInforme.agregarInformacionCda(infoCda,subinforme);
				informe.setCiudad(infoCda.getCodigoDivipo());
				generadorInforme.agregarInformacionPrueba(subinforme, prueba);
				generarFechaInicioFinPrueba(informe, prueba);
				generadorInforme.agregarInformacionSoftware(subinforme, nombreSoftware, versionSoftware);
				generadorInforme.agregarInformacionCertificado(subinforme, consecutivoCertificado);
				
				Propietario propietario = prueba.getRevision().getPropietario();

				if (propietario != null) {
					generadorInforme.agregarInformacionPropietario( propietario, subinforme );
					agregarCodigoCiudadPropietario(propietario, informe);
				}
				Vehiculo vehiculo = prueba.getRevision().getVehiculo();
				if (vehiculo != null) {
					generadorInforme.agregarInformacionVehiculo( vehiculo , subinforme);
					agregarCodigoLinea(vehiculo,informe);
					agregarCodigoClase(vehiculo,informe);
					agregarCodigoCombustible(vehiculo,informe);
					agregarCodigoServicio(vehiculo,informe);
					agregarCodigoMarca(vehiculo,informe);
				}

				generadorInforme.agregarInformacionAnalizador(prueba.getAnalizador(), subinforme);
				Double pef = prueba.getAnalizador().getPef();
				
				VerificacionGasolina verificacion = prueba.getVerificacionGasolina();
				
				Pipeta pipetaAlta = pipetaFacade.find( verificacion.getSpanAltoPipetaId() );
				Pipeta pipetaBaja = pipetaFacade.find( verificacion.getSpanBajoPipetaId() );
				
				Usuario usuario = usuarioFacade.find(verificacion.getUsuarioId());
				
				
				generadorInforme.agregarInformacionPipetas( subinforme, pipetaBaja,pipetaAlta, pef);
				generadorInforme.agregarResultadoVerificacion(subinforme, verificacion, usuario, pef);
				
				String fechaFugas = cargarPruebaFugas(prueba.getFechaInicio(),prueba.getAnalizador().getSerial());
				subinforme.setFechaHoraPruebaFugas(fechaFugas);
				
				Revision revision = prueba.getRevision();
				Date fechaRevision = revision.getFechaCreacionRevision();
				Integer jefeTecnicoId = revision.getUsuarioResponsableId();
				Usuario jefeTecnico = null;
				if (revision.getNumeroInspecciones().equals(2)) {

					List<Inspeccion> inspecciones = inspeccionService
							.obtenerInspeccionesPorRevisionId(revision.getRevisionId());
					if (inspecciones.size() > 1) {
						Inspeccion segunda = inspecciones.get(1);
						fechaRevision = segunda.getFechaInspeccionAnterior();
						jefeTecnico = segunda.getUsuarioResponsable();
					}

				} else {

					jefeTecnico = usuarioFacade.find(jefeTecnicoId);

				}

				SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				String fechaFur = sdf.format(fechaRevision);

				generadorInforme.agregarInformacionRevision(subinforme, prueba.getRevision(), fechaFur, jefeTecnico);


				List<Medida> medidas = pruebaService.obtenerMedidas(prueba);
				List<Defecto> defectos = pruebaService.obtenerDefectos(prueba);
				
				cargarKilometraje(revision.getRevisionId(), subinforme);
				List<Equipo> equipos = pruebaService.obtenerEquipos(prueba);
				
				generadorInforme.agregarInformacionEquipos( equipos, subinforme);

				Boolean dosTiempos = prueba.getRevision().getVehiculo().getTiemposMotor().equals(2);

				ResultadoOttoMotocicletasDTO resultado = generadorResultados.generarResultadoOttoMotos(medidas,
						defectos, dosTiempos);

				generadorInforme.cargarInformacionResultado(subinforme, resultado);

				ResultadoOttoInformeDTO r2 = generadorResultados.generarResultadoOttoInforme(medidas, defectos);

				generadorInforme.cargarInformacionResultadoAdicional(subinforme, r2);
				
				generadorInforme.cargarInformacionMedidasVehiculo(subinforme,medidas);
				
				if(prueba.getMotivoAborto() != null) {
					generadorInforme.cargarMedicionesNoCorregidas(subinforme,prueba.getMotivoAborto());
				}
				
				CausaRechazoMotocicletas causa = null;
				if( !prueba.isAbortada()  ) {
					causa = determinadorRechazo.determinarCausaRechazo(resultado, prueba, medidas, vehiculo);
					if(causa != null) {
						if( causa.equals( CausaRechazoMotocicletas.HC_CUATRO_TIEMPOS) 
								|| causa.equals( CausaRechazoMotocicletas.CO_CUATRO_TIEMPOS) 
								|| causa.equals( CausaRechazoMotocicletas.HC_DOS_TIEMPOS_2010_MAYOR) 
								|| causa.equals( CausaRechazoMotocicletas.CO_DOS_TIEMPOS_MENOR_2009 )
								|| causa.equals( CausaRechazoMotocicletas.HC_DOS_TIEMPOS_MENOR_2009 )
								|| causa.equals( CausaRechazoMotocicletas.CO_DOS_TIEMPOS_2010_MAYOR ))
					    {
							subinforme.setIncumpimientoNivelesEmisiones("SI");
						}else {//el rechazo no es por emisiones contamintantes
							subinforme.setIncumpimientoNivelesEmisiones("NO");
							generadorInforme.cargarMedicionesNoCorregidas(subinforme,"");
						}
						
					}
					
				}else {
					subinforme.setIncumpimientoNivelesEmisiones("NO");
				}
				
				if(causa!=null)
					informe.setCausaRechazo(String.valueOf(causa.getValue()));

				String resultadoFinal = prueba.isAprobada() ? "1" : "2";
				resultadoFinal = prueba.isAbortada() ? "3" : resultadoFinal;
				
				subinforme.setResultadoFinalPrueba(resultadoFinal);
				logger.log(Level.INFO, "Terminada la prueba : " + prueba.getPruebaId() );
				informe.setSubInforme(subinforme);
				
				Integer revisionId = revision.getRevisionId();
				List<Prueba> pruebasRevision = pruebaService.pruebasRevision(revisionId);
				Optional<Prueba> pruebaSonometriaOpt = pruebasRevision.stream().filter( p->p.getTipoPrueba().getTipoPruebaId().equals(7)).findFirst();
				if(pruebaSonometriaOpt.isPresent()) {
					Prueba pruebaSonometria = pruebaSonometriaOpt.get();
					List<Medida> medidasSonometria = pruebaService.obtenerMedidas(pruebaSonometria);
					List<Equipo> equipos1 = equipoFacade.consultarEquiposPorPrueba( pruebaSonometria.getPruebaId() );
					ResultadoSonometriaInformeDTO resultadoSonometria = generadorResultados.generarInformacionSonometria(medidasSonometria, equipos1);
					informe.setResultadoSonometriaInformeDTO(resultadoSonometria);
				}else {
					ResultadoSonometriaInformeDTO resultadoSonometria = new ResultadoSonometriaInformeDTO();
					informe.setResultadoSonometriaInformeDTO(resultadoSonometria);
				}			
				datos.add(informe);
			}

			return datos;
		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Error cargando prueba metodo dos", exc);
			return null;
		}

	}
	
	private void agregarCodigoMarca(Vehiculo vehiculo, InformeGasolinaMotocicletasCarDTO informe) {
		if(vehiculo.getMarca()!=null) {
			Marca marca = vehiculo.getMarca();
			if(marca.getMarcaId()!=null) {
				Integer marcaId = marca.getMarcaId();
				String marcaStr = String.valueOf(marcaId);
				informe.setCodigoMarca(marcaStr);
			}
		}
		
	}

	private void agregarCodigoServicio(Vehiculo vehiculo, InformeGasolinaMotocicletasCarDTO informe) {
		if(vehiculo.getServicio()!=null) {
			Servicio servicio = vehiculo.getServicio();
			if(servicio.getServicioId() != null) {
				Integer codigoServicio =
				UtilConvertirServicioSuperintendencia
				.convertirServicioAsuperintendencia(vehiculo.getServicio().getServicioId());
				String codigoServicioStr = String.valueOf(codigoServicio);
				informe.setCodigoServicio(codigoServicioStr);
			}
		}
		
	}

	private void agregarCodigoCombustible(Vehiculo vehiculo, InformeGasolinaMotocicletasCarDTO informe) {
		if(vehiculo.getTipoCombustible()!=null) {
			TipoCombustible tc = vehiculo.getTipoCombustible();
			if(tc.getCombustibleId() != null) {
				Integer combustibleId = tc.getCombustibleId();
				String combustibleStr = String.valueOf(combustibleId);
				informe.setCodigoCombustible(combustibleStr);
			}
		}
		
	}

	private void agregarCodigoClase(Vehiculo vehiculo, InformeGasolinaMotocicletasCarDTO informe) {
		if(vehiculo.getClaseVehiculo()!=null) {
			ClaseVehiculo clase = vehiculo.getClaseVehiculo();
			if(clase.getClaseVehiculoId() != null) {
				Integer claseId = clase.getClaseVehiculoId();
				String claseIdStr = String.valueOf(claseId);
				informe.setCodigoClaseVehiculo(claseIdStr);
			}
		}
		
	}

	private void agregarCodigoLinea(Vehiculo vehiculo, InformeGasolinaMotocicletasCarDTO informe) {
		if(vehiculo.getLineaVehiculo() != null  ) {
			LineaVehiculo linea = vehiculo.getLineaVehiculo();
			if(linea.getCodigoLinea() != null ) {
				Integer codigoLinea = linea.getCodigoLinea();
				String codigoLineaStr = String.valueOf(codigoLinea);
				informe.setCodigoLineaVehiculo(codigoLineaStr);
			}
		}			
	}

	private void agregarCodigoCiudadPropietario(Propietario propietario, InformeGasolinaMotocicletasCarDTO informe) {
		if(propietario.getCiudad() != null) {
			Integer ciudadId = propietario.getCiudad().getCiudadId();
			String codigoCiudad = ciudadId != null ? String.valueOf(ciudadId) : "error";
			informe.setCodigoCiudadPropietario(codigoCiudad);
		}
	}
	
	private void generarFechaInicioFinPrueba(InformeGasolinaMotocicletasCarDTO informe, Prueba prueba) {
		
		SimpleDateFormat sdf = new SimpleDateFormat( "yyyy/MM/dd, HH:mm:ss");
		informe.setFechaInicioPrueba( sdf.format( prueba.getFechaInicio() ));
		informe.setFechaFinPrueba( sdf.format( prueba.getFechaFinalizacion() ));
		
	}
	
	private String cargarPruebaFugas(Date fechaPrueba,String serial) {
		LocalDate fecha = Instant.ofEpochMilli(fechaPrueba.getTime()) .atZone(ZoneId.systemDefault()).toLocalDate();
		LocalDateTime startOfDay = fecha.atStartOfDay();
		LocalDateTime endOfDay = LocalTime.MAX.atDate(fecha);
		
		Instant instant = startOfDay.atZone(ZoneId.systemDefault()).toInstant();
		Date dateInicio = Date.from(instant);
		
		Instant instantEnd = endOfDay.atZone(ZoneId.systemDefault()).toInstant();
		Date dateFin = Date.from(instantEnd);
		PruebaFuga pf = pruebaFugaAprobadaService.consultarPruebaFugaPorFecha(dateInicio,dateFin,serial);
		if(pf != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			return sdf.format(pf.getFechaRealizacion());
		}else {
			return "";
		}
		
	}




	
	private void cargarKilometraje(Integer revisionId, InformeMotocicletasRes0762 informe) {

		DecimalFormat df = new DecimalFormat("#");
		Prueba pruebaIv = revisionService.buscarPruebaPorTipoPorRevision(revisionId, 1);
		if (pruebaIv != null) {

			List<Medida> medidasIv = pruebaService.obtenerMedidas(pruebaIv);
			Optional<Medida> medidaKilometraje = medidasIv.stream().filter(
					m -> m.getTipoMedida().getTipoMedidaId().equals(ConstantesMedidasKilometraje.KILOMETRAJE_MEDIDA))
					.findAny();
			if (medidaKilometraje.isPresent()) {

				Double kilometraje = medidaKilometraje.get().getValor();
				if (kilometraje >= 0) {
					informe.setKilometraje(df.format(kilometraje));
				} else {
					informe.setKilometraje("NO FUNCIONAL");
				}

			} else {

				Prueba pruebaFrenos = revisionService.buscarPruebaPorTipoPorRevision(revisionId, 5);
				if (pruebaFrenos != null) {

					List<Medida> medidas = pruebaService.obtenerMedidas(pruebaIv);
					Optional<Medida> medidaKilom = medidas.stream().filter(m -> m.getTipoMedida().getTipoMedidaId()
							.equals(ConstantesMedidasKilometraje.KILOMETRAJE_MEDIDA)).findAny();
					if (medidaKilom.isPresent()) {

						Double kilometraje = medidaKilom.get().getValor();
						if (kilometraje >= 0) {
							informe.setKilometraje(df.format(kilometraje));
						} else {
							informe.setKilometraje("NO FUNCIONAL");
						}

					} else {

					}

				}

			}

		}

	}

	

	public Date getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}
	
	private static final char DECIMAL_SEPARATOR_PUNTO = '.';
	
	public void establecerPuntoSimboloDecimal(DecimalFormat df) {
		DecimalFormatSymbols ds = DecimalFormatSymbols.getInstance();
		ds.setDecimalSeparator(DECIMAL_SEPARATOR_PUNTO);
		df.setDecimalFormatSymbols(ds);
	}

	private void cargarInformacionVerificacion(Prueba prueba,
			InformeGasolinaMotocicletasCarDTO informe, SimpleDateFormat sdf) {
		VerificacionGasolina verif = pruebaService
				.consultarVerificacionPrueba(prueba);
		DecimalFormat df = new DecimalFormat();
		if (verif != null) {

			df = new DecimalFormat("#");
			String valorBajaHC = verif.getValorGasRefBajaHC() != null ? df.format(verif.getValorGasRefBajaHC()) : "";
			informe.setVlrSpanBajoHC(valorBajaHC);
			
			String resultadoVlrSpanBajoCH = df.format(verif.getValorBajoHc());
			informe.setResultadoVlrSpanBajoHC(resultadoVlrSpanBajoCH);
			
			df = new DecimalFormat("0.00");
			establecerPuntoSimboloDecimal(df);
			String valorBajaCO = verif.getValorGasRefBajaCO() != null ? df.format( verif.getValorGasRefBajaCO() ) : "";
			informe.setVlrSpanBajoCO(valorBajaCO);
			
			String resultadoVlrSpanBajoCO = df.format(verif.getValorBajoCo());
			informe.setResultadoVlrSpanBajoCO(resultadoVlrSpanBajoCO);
			
			df = new DecimalFormat("0.0");
			establecerPuntoSimboloDecimal(df);
			String valorBajaCO2 = verif.getValorGasRefBajaCO2() != null ? df.format(verif.getValorGasRefBajaCO2()) : "";
			informe.setVlrSpanBajoCO2( valorBajaCO2 );

			String resultadoVlrSpanBajoCO2 = df.format(verif.getValorBajoCo2());
			informe.setResultadoVlrSpanBajoCO2(resultadoVlrSpanBajoCO2);
			
			df = new DecimalFormat("#");
			String valorAltaHC = verif.getValorGasRefAltaHC() != null ? df.format( verif.getValorGasRefAltaHC() ) : "";
			informe.setVlrSpanAltoHC( valorAltaHC );
			
			String resultadoVlrSpanAltoHC = df.format(verif.getValorAltoHc());
			informe.setResultadoVlrSpanAltoHC(resultadoVlrSpanAltoHC);
			
			df = new DecimalFormat("0.00");
			establecerPuntoSimboloDecimal(df);
			String valorAltaCO = verif.getValorGasRefAltaCO() != null ? df.format( verif.getValorGasRefAltaCO() ) : "";
			informe.setVlrSpanAltoCO( valorAltaCO );
			
			String resultadoVlrSpanAltoCO = df.format( verif.getValorAltoCo());
			informe.setResultadoVlrSpanAltoCO(resultadoVlrSpanAltoCO);
			
			df = new DecimalFormat("0.0");
			establecerPuntoSimboloDecimal(df);
			String valorAltaCO2 = verif.getValorGasRefAltaCO2() != null ? df.format( verif.getValorGasRefAltaCO2() ) : "";
			informe.setVlrSpanAltoCO2( valorAltaCO2 );
			
			String resultadoVlrSpanAltoCO2 = df.format(verif.getValorAltoCo2());
			informe.setResultadoVlrSpanAltoCO2(resultadoVlrSpanAltoCO2);
			
			String fechaVerif = verif.getFechaVerificacion() != null ? sdf
					.format(verif.getFechaVerificacion()) : "";
			informe.setFechaHoraUltimaCalibracion( fechaVerif );
		} else {
			informe.setVlrSpanBajoHC("");
			informe.setVlrSpanBajoCO("");
			informe.setVlrSpanBajoCO2("");
			
			informe.setVlrSpanAltoHC("");
			informe.setVlrSpanAltoCO("");
			informe.setVlrSpanAltoCO2("");
			informe.setFechaHoraUltimaCalibracion("");
			
		}

	}

	private void cargarInformacionAnalizador(Prueba prueba,
			InformeGasolinaMotocicletasCarDTO informe) {
		Analizador analizador = pruebaService.consultarAnalizadorPrueba(prueba);
		DecimalFormat df = new DecimalFormat("0.000");
		establecerPuntoSimboloDecimal(df);
		if (analizador != null) {
			informe.setVlrPEf( df.format(analizador.getPef() ) );
			informe.setNumeroSerieBanco(analizador.getSerial());
			informe.setNumeroSerieAnalizador( analizador.getSerialAnalizador() );
			informe.setMarcaAnalizador(analizador.getMarca());
			
		} else {
			informe.setVlrPEf( "No config" );
			informe.setNumeroSerieBanco( "No config" ); 
			informe.setNumeroSerieAnalizador( "No config" );
			informe.setMarcaAnalizador( "No config" );
		}

	}	
	
	@Override
	public int count(Map<String, FilterMeta> filterBy) {		
		return 0;
	}

}
