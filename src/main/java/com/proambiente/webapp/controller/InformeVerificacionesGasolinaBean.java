package com.proambiente.webapp.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.Analizador;
import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.Usuario;
import com.proambiente.modelo.VerificacionGasolina;
import com.proambiente.webapp.dao.AnalizadorFacade;
import com.proambiente.webapp.dao.InformacionCdaFacade;
import com.proambiente.webapp.dao.UsuarioFacade;
import com.proambiente.webapp.dao.VerificacionGasolinaFacade;
import com.proambiente.webapp.util.UtilErrores;

/**
 * Clase que sirve como controlador para la pagina
 * que muestra las verificaciones de gasolina.
 * @author FABIAN
 *
 */
@Named
@ViewScoped
public class InformeVerificacionesGasolinaBean implements Serializable{
	

	
	private static final Logger logger = Logger
			.getLogger(InformeVerificacionesGasolinaBean.class.getName());
	
	private static final long serialVersionUID = 1L;

	@Inject
	VerificacionGasolinaFacade verificacionGasolinaDAO;
	
	@Inject
	UtilErrores utilErrores;
	
	@Inject
	UsuarioFacade usuarioDAO;
	
	@Inject
	AnalizadorFacade analizadorDAO;
	
	@Inject
	InformacionCdaFacade infoCdaDAO;
	
	private List<VerificacionGasolina> verificaciones;
	
	private Date fechaInicial,fechaFinal;
	
    private InformacionCda informacionCda;
	
	private String nombreSoftware = "PRORTM";
	
	private String versionSoftware = "0.0.1.0";
	
	
	
	
	@PostConstruct
	public void init(){
		
		informacionCda = infoCdaDAO.find(1);
		versionSoftware = informacionCda.getVersionSoftware();
		
	}
	

	public void consultarVerificaciones(ActionEvent ae){
		try{
			Calendar calendarFechaFinal = Calendar.getInstance();
			calendarFechaFinal.setTime(fechaFinal);
			calendarFechaFinal.add(Calendar.MONTH,-1);
			
			Calendar calendarFechaInicial = Calendar.getInstance();
			calendarFechaInicial.setTime(fechaInicial);
			if(calendarFechaFinal.compareTo(calendarFechaInicial) > 0){
				utilErrores.addInfo("No se puede consultar mas de un mes de informacion");
				return;
			}
			logger.log(Level.INFO,"Verificaciones gasolina fecha inicial {0}, fecha final {1}",
					new Object[]{fechaInicial,fechaFinal});
			verificaciones = verificacionGasolinaDAO.consultarVerificaciones(fechaInicial, fechaFinal);
		}catch(Exception exc){
			logger.log(Level.SEVERE,"Error consultando datos verificaciones gasolina",exc);
			utilErrores.addError("ERROR " + exc.getMessage());
		}		
	}
	
	public Usuario consultarUsuario(Integer idUsuario){
		if(idUsuario == null) return null;
		else return usuarioDAO.find(idUsuario); 
	}
	
	public String consultarSerialExternoEquipo(String serialInternoBanco){
		
		Analizador analizador = analizadorDAO.find(serialInternoBanco);
		if(analizador == null){
			return "analizador no registrado";
		}else{
			return analizador.getSerialAnalizador();
		}
		
	}
	
	public void verReporte(Integer pruebaFugaId) {
		try {
			logger.log(Level.INFO, "Ver reportede prueba de fugas para {0}", pruebaFugaId);
			FacesContext.getCurrentInstance().getExternalContext()
					.redirect("/prortmwebapp/ServletVerificacionGasolina?id_verificacion=" + pruebaFugaId);
		} catch (IOException e) {
			utilErrores.addError("Erro cargando reporte de fugas");
			logger.log(Level.SEVERE, "Error cargando reporte de fugas", e);
		}
	}


	public List<VerificacionGasolina> getVerificaciones() {
		return verificaciones;
	}

	public void setVerificaciones(List<VerificacionGasolina> verificaciones) {
		this.verificaciones = verificaciones;
	}

	public Date getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public String getNombreSoftware() {
		return nombreSoftware;
	}

	public void setNombreSoftware(String nombreSoftware) {
		this.nombreSoftware = nombreSoftware;
	}

	public String getVersionSoftware() {
		return versionSoftware;
	}

	public void setVersionSoftware(String versionSoftware) {
		this.versionSoftware = versionSoftware;
	}
	
	
		
}
