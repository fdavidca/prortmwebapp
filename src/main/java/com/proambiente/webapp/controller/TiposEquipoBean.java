package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import org.omnifaces.cdi.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.proambiente.modelo.TipoEquipo;
import com.proambiente.webapp.dao.TipoEquipoFacade;
import com.proambiente.webapp.util.UtilErrores;

@Named
@ViewScoped
public class TiposEquipoBean implements Serializable{
	
	private static final Logger logger = Logger.getLogger(TiposEquipoBean.class
			.getName());
	private static final long serialVersionUID = 1L;
	
	@Inject
	TipoEquipoFacade tipoEquipoFacade;
	
	@Inject
	UtilErrores utilErrores;
	

	private TipoEquipo tipoEquipoSeleccionado;
	
	private List<TipoEquipo> listaTiposEquipo;


	@PostConstruct
	public void init(){		
		listaTiposEquipo = tipoEquipoFacade.findAll();
	}


	
	
	public TipoEquipo getTipoEquipoSeleccionado() {
		return tipoEquipoSeleccionado;
	}




	public void setTipoEquipoSeleccionado(TipoEquipo tipoEquipoSeleccionado) {
		this.tipoEquipoSeleccionado = tipoEquipoSeleccionado;
	}




	public List<TipoEquipo> getListaTiposEquipo() {
		return listaTiposEquipo;
	}


	public void setListaTiposEquipo(List<TipoEquipo> listaTiposEquipo) {
		this.listaTiposEquipo = listaTiposEquipo;
	}


	public void guardarCambios(ActionEvent ae){
		try{
			logger.info("Guardando tipo equipo" + tipoEquipoSeleccionado.toString());
			tipoEquipoFacade.create(tipoEquipoSeleccionado);
			init();			
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error guardando tipo equipo" + tipoEquipoSeleccionado.toString(),exc);
			utilErrores.addError("Error guardando tipo equipo" + exc.getMessage());
		}
	}
	
	public void atachTipoEquipo(ActionEvent ae){
		System.out.println("Ejecutado attach");	
	}
	
	public void nuevoTipoEquipo(ActionEvent ae){
		tipoEquipoSeleccionado = new TipoEquipo();
		System.out.println("Ejecutado nuevo tipo equipo");
	}

}
