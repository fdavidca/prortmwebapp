package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;

import com.proambiente.modelo.Defecto;
import com.proambiente.modelo.DefectoAsociado;
import com.proambiente.modelo.EstadoDefecto;
import com.proambiente.modelo.Inspeccion;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Servicio;
import com.proambiente.modelo.TipoVehiculo;
import com.proambiente.modelo.Usuario;
import com.proambiente.webapp.service.DefectoService;
import com.proambiente.webapp.service.InspeccionService;
import com.proambiente.webapp.util.UtilErrores;
	

@Named
@ViewScoped
public class InformeSensorialPlacaBean implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final Logger logger = Logger
			.getLogger(InformeSensorialPlacaBean.class.getName());
	
	@Inject
	InspeccionService inspeccionService;
	
	@Inject
	UtilErrores utilErrores;
	
	@Inject
	transient UtilNumeroCertificadoBean utilNumeroCertificado;
	
	private String placa;
	private List<Inspeccion> revisiones;
	private Inspeccion revisionSeleccionada;
	
	private Usuario usuario;
	
	@Inject
	transient private DefectoService defectoService; 

	private List<Defecto> todosLosDefectos;
	
	private List<DefectoAsociado> defectosAsociados;
	
	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public List<Inspeccion> getRevisiones() {
		return revisiones;
	}

	public void setRevisiones(List<Inspeccion> revisiones) {
		this.revisiones = revisiones;
	}
	
	public void buscarRevisiones(ActionEvent ae){
		if(placa != null && !placa.isEmpty()){
			try{
				revisiones = inspeccionService.inspeccionesPorPlaca(placa);
			}catch(Exception exc){
				utilErrores.addError("Error consultando inspecciones");
				logger.log(Level.SEVERE, "Error consultando inspecciones por placa", exc);
			}
		}else{
			utilErrores.addError("Ingrese placa");
		}
	}

	public Inspeccion getRevisionSeleccionada() {
		return revisionSeleccionada;
	}

	public void setRevisionSeleccionada(Inspeccion revisionSeleccionada) {
		this.revisionSeleccionada = revisionSeleccionada;
	}

	public void imprimirReporteInspeccionSensorial(ActionEvent ae){
		if(revisionSeleccionada != null){
			
		    try {
		        
		    	
		    	
		    	TipoVehiculo tipoVehiculo = inspeccionService.obtenerTipoVehiculoPorInspeccionId(revisionSeleccionada.getInspeccionId());
		    	todosLosDefectos = defectoService.obtenerDefectoPorTipoVehiculo(tipoVehiculo.getTipoVehiculoId()); 		
		    	
		    	Servicio servicio = inspeccionService.obtenerServicioPorInspeccionId(revisionSeleccionada.getInspeccionId());
		    	Boolean ensenianza = false;
		    	Boolean servicioPublico = false;
		    	if(servicio != null && servicio.getServicioId()!= null) {
		    		ensenianza = servicio.getServicioId().equals(5);
		    		servicioPublico = servicio.getServicioId().equals(2);
		    	}
		    	
		    	if(!ensenianza) {
		    		Stream<Defecto> streamSinEnse =  todosLosDefectos.stream().filter(d->!d.getCategoriaDefecto().getCategoriaDefectoId().equals(21));
		    		todosLosDefectos = streamSinEnse.collect(Collectors.toList());		    		
		    	}
		    	
		    	if(!servicioPublico) {
		    		Stream<Defecto> streamSinDefectosPublicos = todosLosDefectos.stream().filter(d->!d.getServicioPublico());
		    		todosLosDefectos = streamSinDefectosPublicos.collect(Collectors.toList());
		    	}
		    	
		    	
		    	
		    	defectosAsociados = new ArrayList<>();
		    	for(Defecto d : todosLosDefectos){
		    		DefectoAsociado defectoAsociado = new DefectoAsociado();
		    		defectoAsociado.setDefecto(d);
		    		defectoAsociado.setEstadoDefecto(EstadoDefecto.NO_DETECTADO);//ojo cambiar por enumeracion
		    		defectosAsociados.add(defectoAsociado);
		    	}
		    	
		    	Collections.sort(defectosAsociados, (d1,d2) -> d1.getDefecto().getDefectoId().compareTo( d2.getDefecto().getDefectoId() ) );
		    	
		    	
		    	
		    	List<Prueba> pruebasRevision = inspeccionService.cargarInspeccion(revisionSeleccionada.getInspeccionId()).getPruebas();
		    	Optional<Prueba> optPrueba = pruebasRevision.stream().filter(p-> p.getTipoPrueba().getTipoPruebaId() == 1).findAny();
		    	if(optPrueba.isPresent()){
		    		Prueba pruebaInspeccionSensorial = optPrueba.get();
		    		usuario = pruebaInspeccionSensorial.getUsuario();
		    		List<DefectoAsociado> defAsociados = pruebaInspeccionSensorial.getDefectosAsociados();
		    		for(DefectoAsociado defAsoc : defAsociados){
		    			 int indiceDefecto = Collections.binarySearch(defectosAsociados, defAsoc,
		                            (d1, d2) -> {
		                                return d1.getDefecto().getDefectoId()
		                                		.compareTo(d2.getDefecto().getDefectoId());
		                            }
		                    );
		    			 if(indiceDefecto >= 0){
		                    DefectoAsociado defectoEncontrado = defectosAsociados.get(indiceDefecto);
		                    defectoEncontrado.setEstadoDefecto(defAsoc.getEstadoDefecto());
		    			 }
		                    
		    		}
		    	}
		    	
		    } catch (Exception e) {
		    	logger.log(Level.SEVERE, "Error imprimir reporte", e);
		    } finally {
		        
		    }
		} else {
			utilErrores.addInfo("Seleccione revison de la tabla");
			
		}
	}
	
	
	public String formatearUsuario() {
		StringBuilder sb = new StringBuilder();
	
		if(usuario != null) {		
			sb.append("Realizado por: ").append(usuario.getNombres());
			sb.append(" ").append(usuario.getApellidos()).append(" ")
			.append(" Doc: ").append(usuario.getCedula());
		}
		return sb.toString();
	}
	
	public String numeroCertificado(int revisionId){
		return utilNumeroCertificado.numeroCertificado(revisionId);	
	}

	public List<Defecto> getTodosLosDefectos() {
		return todosLosDefectos;
	}

	public void setTodosLosDefectos(List<Defecto> todosLosDefectos) {
		this.todosLosDefectos = todosLosDefectos;
	}

	public List<DefectoAsociado> getDefectosAsociados() {
		return defectosAsociados;
	}

	public void setDefectosAsociados(List<DefectoAsociado> defectosAsociados) {
		this.defectosAsociados = defectosAsociados;
	}
	
	
	
	
}



