package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.file.UploadedFile;

import com.proambiente.modelo.Marca;
import com.proambiente.modelo.Usuario;
import com.proambiente.webapp.service.ImportarMarcasDesdeExcelService;
import com.proambiente.webapp.util.UtilErrores;

@Named
@ViewScoped
public class ImportarMarcasBean implements Serializable{
	
	private static final long serialVersionUID = -7340812940161862587L;
	
	private static final Logger logger = Logger.getLogger(ImportarMarcasBean.class.getName());
	
	@Inject
	UtilErrores utilErrores;
	
	@Inject
	ImportarMarcasDesdeExcelService importar ;
	
	@Inject
	@UsuarioDefault 
	Usuario usuarioAutenticado;
	
	private List<Marca> marcasCreadas = new ArrayList<>();

	private UploadedFile file;
	
	public UploadedFile getFile(){
		return file;
	}
	
	
	
	public void upload(FileUploadEvent event){
		UploadedFile uploadedFile = event.getFile();
		if(uploadedFile != null){
			logger.info("Archivo " + uploadedFile.getFileName() + "fue subido ");
			try {
				List<Marca> marcas = importar.obtenerMarcasDesdeArchivo( uploadedFile.getInputStream() );
				importar.guardarMarca(marcas);
				marcasCreadas = marcas;
			} catch (Exception e) {
				logger.log(Level.SEVERE, "Error importando archivos", e);
				utilErrores.addError("Error importando archivos");
			}
			utilErrores.addInfo(uploadedFile.getFileName() + " is uploaded.");			 
		}
	}
	
	public void setFile(UploadedFile file){
		this.file = file;
	}



	public List<Marca> getMarcasCreadas() {
		return marcasCreadas;
	}



	public void setMarcasCreadas(List<Marca> marcasCreadas) {
		this.marcasCreadas = marcasCreadas;
	}


	
}
