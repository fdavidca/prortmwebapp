package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Faces;
import org.primefaces.PrimeFaces;
import org.tempuri.Resultado;

import com.proambiente.modelo.NivelAlerta;
import com.proambiente.modelo.Revision;
import com.proambiente.webapp.service.AlertasDeHoyService;
import com.proambiente.webapp.service.PinCi2Service;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.service.ci2.FurCi2CodificadorV2;
import com.proambiente.webapp.service.ci2.ManejadorCi2SicovService;
import com.proambiente.webapp.service.ci2.ManejadorCi2SicovService2V2;
import com.proambiente.webapp.service.ci2.OperacionesSicovCi2V3;
import com.proambiente.webapp.service.guardarpdf.GuardarPdfCallback;
import com.proambiente.webapp.service.guardarpdf.GuardarPdfCallbackFactory;
import com.proambiente.webapp.service.indra.OperacionesSicovIndraV2;
import com.proambiente.webapp.service.notification.NotificationPrimariaService;
import com.proambiente.webapp.service.sicov.OperacionesSicov;
import com.proambiente.webapp.service.sicov.OperacionesSicov.Envio;
import com.proambiente.webapp.service.sicov.OperacionesSicovProducer;
import com.proambiente.webapp.util.UtilErrores;

@Named
@ViewScoped
public class TestCi2EcoBean implements Serializable {

	private static final Logger logger = Logger.getLogger(TestCi2EcoBean.class.getName());
	private static final long serialVersionUID = 1L;

	@Resource
	private ManagedExecutorService managedExecutorService;

	@Inject
	UtilErrores utilErrores;

	@Inject
	AlertasDeHoyService alertaService;

	@Inject
	transient FurCi2CodificadorV2 ci2Service;

	@Inject
	RevisionService revisionService;

	@Inject
	NotificationPrimariaService notificationService;

	@Inject
	ManejadorCi2SicovService2V2 manejadorCi2;

	@Inject
	OperacionesSicovProducer sicovProducer;

	@Inject
	PinCi2Service pinCi2Service;



	@Inject
	transient UtilNumeroCertificadoBean utilNumeroCertificado;
	
	@Inject
	GuardarPdfCallbackFactory guardarPdfCallbackFactory;

	@Inject
	NotificationPrimariaService notificacionService;

	private String mensaje = "Test de Echo ";

	private Integer numeroRevision;

	private Integer revisionId;

	private String placa;

	private String pin;

	private String tipoRtm;

	private Resultado resultado;
	private String alerta;
	private boolean resultadoEnvioVisible;

	public void buttonClicked(ActionEvent ae) {
		try {
			// manejadorCi2.enviarEco("Eco enviado desde PRORTM");
			pin = manejadorCi2.consultarPinPlacaSincrono(placa);
			resultado = new Resultado();
			resultado.setFechaTransaccion("");
			utilErrores.addInfo("Pin: " + pin);
		} catch (Exception exc) {
			logger.log(Level.SEVERE, "error", exc);
			utilErrores.addError("Error consultando pin por placa " + placa + exc.getMessage());
		}
	}

	public void utilizarPin(ActionEvent ae) {
		try {
			resultado = manejadorCi2.utilizarPinSincrono(pin, placa, tipoRtm);
			if (resultado.getCodigoRespuesta().equals(ManejadorCi2SicovService.TRANSACCION_EXITOSA)) {
				pinCi2Service.registrarPinUtilizado(pin, revisionId);
				utilErrores.addInfo("Resultado Ok");
			} else {
				utilErrores.addError("Transaccion no exitosa el utilizar pin");
			}

		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Error operacion utilizar pin " + pin, exc);
			utilErrores.addError("Error operacion utilizar pin" + pin);
		}
	}

	public void primerEnvioFormulario(ActionEvent ae) {
		try {

			String evaluacion = revisionService.evaluarRevision(revisionId);
			if (evaluacion.equalsIgnoreCase("NO_FINALIZADA")) {
				utilErrores.addError("La revisión no ha sido finalizada");
				return;
			} else {
				Revision revision = revisionService.cargarRevision(revisionId);
				if (revision.getPreventiva()) {
					utilErrores.addError("La revision es preventiva");
				} else {

					OperacionesSicov operacionesSicov = sicovProducer.crearOperacionSicov();
					GuardarPdfCallback guardarPdfCallback = guardarPdfCallbackFactory.crearInstancia();
					if (operacionesSicov instanceof OperacionesSicovCi2V3) {
						OperacionesSicovCi2V3 opCi2 = (OperacionesSicovCi2V3) operacionesSicov;
						opCi2.registrarFUR(revision.getRevisionId(), 1, Envio.PRIMER_ENVIO,guardarPdfCallback);
					}
				}
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error enviando formulario primera etapa", e);
			utilErrores.addError("Error enviando formulario primera etapa");
		}
	}

	public void realizarSegundoEnvioFormulario(ActionEvent ae) {
		try {
			Revision revision = revisionService.cargarRevision(revisionId);
			if (revision.getPreventiva()) {
				utilErrores.addError("La revision es preventiva");
				return;
			}

			String evaluacionRevision = revisionService.evaluarRevision(revisionId);
			if (evaluacionRevision.equalsIgnoreCase("NO_FINALIZADA")) {
				utilErrores.addError("La revisión no ha sido finalizada");
				return;
			} else if (evaluacionRevision.equalsIgnoreCase("aprobada")) {
				String consecutivoPreimpreso = utilNumeroCertificado.numeroCertificado(revisionId);
				if (consecutivoPreimpreso.equalsIgnoreCase("----")) {
					utilErrores.addError("La revision no tiene certificado asociado");
					return;
				}
				OperacionesSicov opSicov = sicovProducer.crearOperacionSicov();
				GuardarPdfCallback guardarPdfCallback = guardarPdfCallbackFactory.crearInstancia();
				if ( opSicov instanceof OperacionesSicovCi2V3) {
					 
						OperacionesSicovCi2V3 opCi2 = (OperacionesSicovCi2V3) opSicov;
						opCi2.registrarInformacionCertificado(revision.getRevisionId(), consecutivoPreimpreso,
								revision.getConsecutivoRunt(),guardarPdfCallback);
					

				}  else if (opSicov instanceof OperacionesSicovIndraV2) {
					
					opSicov.registrarInformacionRunt(revisionId,guardarPdfCallback);
				} else {
					revisionService.registrarSegundoEnvio(revisionId);
				}
			} else if (evaluacionRevision.equalsIgnoreCase("reprobada")) {

				try {
					OperacionesSicov opSicov = sicovProducer.crearOperacionSicov();
					GuardarPdfCallback guardarPdfCallback = guardarPdfCallbackFactory.crearInstancia();
					if (opSicov instanceof OperacionesSicovCi2V3) {
						
						OperacionesSicovCi2V3 opCi2 = (OperacionesSicovCi2V3) opSicov;
						
						opCi2.enviarSegundoEnvioRevisionReprobada(revisionId, revision.getConsecutivoRunt(),guardarPdfCallback);

					}  else if (opSicov instanceof OperacionesSicovIndraV2) {
						opSicov.registrarInformacionRunt(revisionId,guardarPdfCallback);
					} else {
						revisionService.registrarSegundoEnvio(revisionId);
					}

				} catch (Exception e) {
					logger.log(Level.SEVERE, "Error, no se pudo registrar formulario", e);
					notificacionService.sendNotification(
							"Error, no se pudo registrar formulario segunda vez " + e.getMessage(),
							NivelAlerta.ERROR_GRAVE, Boolean.TRUE);
				}

			}

		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Error realizando segundo envio ci2", exc);
			utilErrores.addError("Error realizando segundo envío ci2");
		}
	}

	/**
	 * Metodo que se ejecuta cuando se recibe el resultado del primer envio o
	 * segundo envio ver NotificacionService
	 * 
	 * @param event
	 */
	public void pushed(Object event) {

		System.out.println("Pushed: ");
		Map<String, String[]> map = Faces.getRequestParameterValuesMap();
		String mensaje = Faces.getRequestParameter("mensaje");
		String nivel = Faces.getRequestParameter("nivel");
		this.alerta = "Alerta recibida " + mensaje;
		this.resultadoEnvioVisible = true;
		if (nivel.equalsIgnoreCase("error")) {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_FATAL, nivel, mensaje);
			PrimeFaces.current().dialog().showMessageDynamic(message);
		} else {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, nivel, mensaje);
			PrimeFaces.current().dialog().showMessageDynamic(message);
		}

	}

	public Resultado getResultado() {
		return resultado;
	}

	public void setResultado(Resultado resultado) {
		this.resultado = resultado;
	}

	public Integer getRevisionId() {
		return revisionId;
	}

	public void setRevisionId(Integer revisionId) {
		this.revisionId = revisionId;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public Integer getNumeroRevision() {
		return numeroRevision;
	}

	public void setNumeroRevision(Integer numeroRevision) {
		this.numeroRevision = numeroRevision;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getTipoRtm() {
		return tipoRtm;
	}

	public void setTipoRtm(String tipoRtm) {
		this.tipoRtm = tipoRtm;
	}

}
