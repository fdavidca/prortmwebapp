package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.faces.context.FacesContext;
import org.omnifaces.cdi.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.Inspeccion;
import com.proambiente.modelo.NivelAlerta;
import com.proambiente.modelo.Revision;
import com.proambiente.webapp.dao.InformacionCdaFacade;
import com.proambiente.webapp.service.CerrarRevisionInspeccionService;
import com.proambiente.webapp.service.InspeccionService;
import com.proambiente.webapp.service.PruebasService;
import com.proambiente.webapp.service.RegistrarFurService;
import com.proambiente.webapp.service.RevisionService;

import com.proambiente.webapp.service.notification.NotificationPrimariaService;

import com.proambiente.webapp.service.sicov.OperacionesSicovProducer;
import com.proambiente.webapp.util.UtilErrores;

@Named
@ViewScoped
public class CerrarRevisionBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8548380580403163503L;

	private static final Logger logger = Logger.getLogger(CerrarRevisionBean.class.getName());

	@Inject
	OperacionesSicovProducer operacionesSicovProducer;

	@Resource
	ManagedExecutorService executor;

	@Inject
	RevisionService revisionService;

	@Inject
	InspeccionService inspeccionService;

	@Inject
	UtilErrores utilErrores;

	@Inject
	InformacionCdaFacade informacionCdaDAO;

	@Inject
	PruebasService pruebasService;

	@Inject
	CerrarRevisionInspeccionService cerrarRevisionInspeccionService;

	@Inject
	NotificationPrimariaService notificacionService;
	
	@Inject
	RegistrarFurService guardarFurService;

	InformacionCda informacionCda;

	private Integer revisionId;
	private Revision revision;
	private String mensaje;

	@PostConstruct
	public void init() {

		try {
			Integer id = (Integer) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("revision_id");
			mensaje = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("mensaje");
			revision = revisionService.cargarRevision(id);
			logger.log(Level.INFO, "Revision cargada {0}", revision.getVehiculo().getPlaca());
			informacionCda = informacionCdaDAO.find(1);

		} catch (Exception exc) {
			utilErrores.addError("No se puede restablecer la vista" + exc.getMessage());
			logger.log(Level.SEVERE, "Error no se puede cargar la vista de cerrar revision ", exc);
		}
	}

	public void cerrarRevision() {
		try {

			String evaluacionRevision = revisionService.evaluarRevision(revision.getRevisionId());
			if (evaluacionRevision.equalsIgnoreCase("aprobada")) {
				utilErrores.addInfo("Revision aprobada, imprima certificado para cerrar ");
				return;
			}

			List<Inspeccion> inspecciones = inspeccionService
					.obtenerInspeccionesPorRevisionId(revision.getRevisionId());

			if (!inspecciones.isEmpty()) {// segunda inspeccion reprobada
				if (evaluacionRevision.equalsIgnoreCase("reprobada")) {
					if (revision.getConsecutivoRunt() == null || revision.getConsecutivoRunt().isEmpty()) {
						utilErrores.addError("Registre consecutivo runt de la revisión en los datos");
						return;
					}
					
					cerrarRevisionInspeccionService.cerrarRevisionInspeccion(revision);
					utilErrores.addInfo("Revision cerrada como reprobada");

				}
			} else {
				if (revisionService.isRevisionVencidaPorQuinceDias(revision) && !revision.getAprobada()) {
					if (evaluacionRevision.equalsIgnoreCase("reprobada")) {
						if (revision.getConsecutivoRunt() == null || revision.getConsecutivoRunt().isEmpty()) {
							utilErrores.addError("Registre consecutivo runt de la revisión en los datos para cerrar la revision");
							return;
						}
						
						cerrarRevisionInspeccionService.cerrarRevisionInspeccion(revision);
						utilErrores.addInfo("Revision cerrada como reprobada");

					}
				}
			} // en cualquier caso enviar el estado de revision cerrada
			executor.submit(() -> {
				try {
					
						revisionService.registrarSegundoEnvio(revision.getRevisionId());
					

				} catch (Exception e) {
					logger.log(Level.SEVERE, "Error, no se pudo registrar formulario", e);
					notificacionService.sendNotification(
							"Error, no se pudo registrar formulario segunda vez " + e.getMessage(),
							NivelAlerta.ERROR_GRAVE, Boolean.TRUE);
				}
			});

		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Error cerrando revisión", exc);
		}
	}
	
	public void guardarPdf(Revision revision) {
		Integer revisionId = 0;
		try {
			String placa = revision.getVehiculo().getPlaca();
			revisionId = revision.getRevisionId();
			Integer intento = revision.getNumeroInspecciones();
			String descripcion = "Fur aprobado";
			guardarFurService.guardarPdf(placa, revisionId, descripcion, intento);
			logger.log(Level.INFO, "fur placa {0} revisionid {1} guardado",
					new Object[] { revision.getVehiculo().getPlaca(), revision.getRevisionId() });

		} catch (Exception exc) {

			logger.log(Level.SEVERE, "Error generando fur ", exc);
			notificacionService.sendNotification(
					"Error, no se pudo guardar fur para la revision " + revisionId, NivelAlerta.ERROR_GRAVE,
					Boolean.TRUE);
		}
	}

	public Integer getRevisionId() {
		return revisionId;
	}

	public void setRevisionId(Integer revisionId) {
		this.revisionId = revisionId;
	}

	public Revision getRevision() {
		return revision;
	}

	public void setRevision(Revision revision) {
		this.revision = revision;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

}
