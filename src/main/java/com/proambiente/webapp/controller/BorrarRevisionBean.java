package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.event.ActionEvent;
import org.omnifaces.cdi.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;



import com.proambiente.modelo.Inspeccion;
import com.proambiente.modelo.Revision;
import com.proambiente.webapp.service.CertificadoService;
import com.proambiente.webapp.service.InspeccionService;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.util.UtilErrores; 

@Named
@ViewScoped
public class BorrarRevisionBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5986495108974178973L;


	private static final Logger logger = Logger.getLogger(BorrarRevisionBean.class
			.getName());
	

	@Inject
	RevisionService revisionService;
	
	@Inject
	InspeccionService inspeccionService;
	
	@Inject
	UtilErrores utilErrores;
	
	@Inject
	CertificadoService certificadoService;
	
	
	private String placa;
	private String test;
	private List<Revision> revisiones;
	private Revision revisionSeleccionada;
	private boolean noAutorizarBorrado = true;

	private List<Inspeccion> inspecciones;
	
	public void buscarRevisiones(ActionEvent ae){
		if(placa != null && !placa.isEmpty()){
			try{
				revisiones = revisionService.buscarTodasRevisionesPorPlaca(placa);
				logger.info("revisiones econtradas placa " + placa + " numero : " + revisiones.size());
				test = "revisiones econtradas placa " + placa + " numero : " + revisiones.size();
				inspecciones = null;
			}catch(Exception exc){
				utilErrores.addError("Error consultando inspecciones");
				logger.log(Level.SEVERE, "Error consultando inspecciones por placa", exc);
			}
		}else{
			utilErrores.addError("Ingrese placa");
		}
	}
	
	public void borrarRevision(){
		try{
			revisionSeleccionada.getReinspecciones();
			long numeroCertificados = certificadoService.consultarCuantosCertificadosTieneRevision(revisionSeleccionada.getRevisionId());
			if(numeroCertificados > 0){
				utilErrores.addInfo("No puede borrar una revision con certificado asociado");
				return;
			}
			if(revisionSeleccionada.getPrimerEnvioRealizado()) {
				utilErrores.addError("No puede borrar una revision con primer envio");
				return;
			}
			
			inspecciones = revisionSeleccionada.getReinspecciones();
			if(inspecciones.size() > 0){
				utilErrores.addInfo("La revision tiene inspecciones asociadas que se van a borrar");
			}
			
			noAutorizarBorrado = false;
		}catch(Exception exc){
			logger.log(Level.SEVERE," Error borrando la revision",exc);
			utilErrores.addError("Borrar revision");
		}
	}
	
	public void borrarRevisionDefinitivamente(){
		try{
			revisionService.borrarRevision(revisionSeleccionada);
			logger.info("Revision " + revisionSeleccionada.getRevisionId() + " placa : " + revisionSeleccionada.getVehiculo().getPlaca() + " borrada");
			noAutorizarBorrado = true;
			utilErrores.addInfo("Revision borrada");
		}catch(Exception exc){
			logger.log(Level.SEVERE,"Error borrando revision ",exc);
			utilErrores.addError(" Error borrando revision" );
		}
		
	}
	
	
	
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public List<Revision> getRevisiones() {
		return revisiones;
	}
	public void setRevisiones(List<Revision> revisiones) {
		this.revisiones = revisiones;
	}
	public Revision getRevisionSeleccionada() {
		return revisionSeleccionada;
	}
	public void setRevisionSeleccionada(Revision revisionSeleccionada) {
		this.revisionSeleccionada = revisionSeleccionada;
	}

	public List<Inspeccion> getInspecciones() {
		return inspecciones;
	}

	public void setInspecciones(List<Inspeccion> inspecciones) {
		this.inspecciones = inspecciones;
	}

	public boolean isAutorizarBorrado() {
		return noAutorizarBorrado;
	}

	public void setAutorizarBorrado(boolean autorizarBorrado) {
		this.noAutorizarBorrado = autorizarBorrado;
	}

	public String getTest() {
		return test;
	}

	public void setTest(String test) {
		this.test = test;
	}
	
	
	
}
