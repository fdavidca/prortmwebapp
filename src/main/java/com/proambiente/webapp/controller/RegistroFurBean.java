package com.proambiente.webapp.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.Revision;
import com.proambiente.modelo.RegistroFur;
import com.proambiente.webapp.dao.RegistroFurFacade;
import com.proambiente.webapp.service.RegistrarFurService;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.util.UtilErrores;

@Named
@ViewScoped
public class RegistroFurBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private static final Logger logger = Logger.getLogger(RegistroFurBean.class.getName());
	
	@Inject
	RevisionService revisionService;
	
	@Inject
	UtilErrores utilErrores;
	
	Revision revision;
	
	
	
	@Inject
	RegistroFurFacade registroFurDAO;
	
	private List<RegistroFur> registrosDeFur;
	
	@PostConstruct
	public void init() {
		try{
			Integer id = (Integer) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("revision_id");
			revision = revisionService.cargarRevision(id);
			registrosDeFur = registroFurDAO.buscarPorRevisionId(revision.getRevisionId()*1L);
			logger.log(Level.INFO,"Pruebas cargadas para {0}",revision.getVehiculo().getPlaca());
		}catch(Exception exc){
			utilErrores.addError("No se puede restablecer la vista" + exc.getMessage());			
			logger.log(Level.SEVERE, "Error no se puede cargar la vista de pruebas ", exc);
		}
	}
	
	
	

	public List<RegistroFur> getRegistrosDeFur() {
		return registrosDeFur;
	}

	public void setRegistrosDeFur(List<RegistroFur> registrosDeFur) {
		this.registrosDeFur = registrosDeFur;
	}




	public Revision getRevision() {
		return revision;
	}

	public void imprimirReporte(Long registroFurId){
		try {
		
			
				FacesContext.getCurrentInstance().getExternalContext().redirect("/prortmwebapp/ServletFurRegistrado?revision_id="+registroFurId);
			
		
		} catch (IOException e) {
			utilErrores.addError("Error imprimiendo reporte");
		}
		
	}


	public void setRevision(Revision revision) {
		this.revision = revision;
	}
	
	
	
	
	
	
}
