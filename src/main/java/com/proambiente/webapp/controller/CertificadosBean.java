package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;

import com.proambiente.modelo.Certificado;

import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.NivelAlerta;
import com.proambiente.modelo.Revision;
import com.proambiente.webapp.dao.InformacionCdaFacade;
import com.proambiente.webapp.service.CertificadoService;
import com.proambiente.webapp.service.FechaService;
import com.proambiente.webapp.service.FurCi2Codificador;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.service.ci2.ManejadorCi2SicovService2V2;
import com.proambiente.webapp.service.notification.NotificationPrimariaService;
import com.proambiente.webapp.service.sicov.OperacionesSicovProducer;
import com.proambiente.webapp.util.UtilErrores;

/**
 * Clase que sirve como controlador para 
 * la vista de imprimir certificado o imprimir
 * duplicado de certificado
 * @author FABIAN
 *
 */
@Named
@ViewScoped
public class CertificadosBean implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final Logger logger = Logger
			.getLogger(CertificadosBean.class.getName());
	
	private boolean anularImprimirNuevoCertificado;
	
	private boolean certificadoImpresoPorPrimeraVez;

	private String consecutivoPreimpreso;
	
	private String motivoAnulacion;

	Revision revision;
	
	Certificado certificadoNuevo;
	
	Certificado certificadoParaAnular;
	
	InformacionCda informacionCDA;
	
	private Integer diaExpedicion,mesExpedicion,anioExpedicion;
	
	private Integer diaVencimiento,mesVencimiento,anioVencimiento;
	
	@Inject
	RevisionService revisionService;

	@Inject
	ManejadorCi2SicovService2V2 manejadorOperacionesCi2;	
	@Inject
	transient FurCi2Codificador formularioService;

	@Inject
	UtilErrores utilErrores;

	@Inject
	InformacionCdaFacade informacionDAO;

	@Inject
	CertificadoService certificadoService;

	@Inject
	FechaService fechaService;

	@Inject
	OperacionesSicovProducer operacionesSicovProducer;

	@Resource
	ManagedExecutorService executor;

	@Inject
	NotificationPrimariaService notificacionService;
	
	
	private Calendar calFechaExpedicion;

	private Calendar calFechaVencimiento;

	private Boolean deshabilitarImprimir = false;

	@PostConstruct
	public void init() {

		try {
			calFechaVencimiento = Calendar.getInstance();
			calFechaExpedicion = Calendar.getInstance();
			Integer revisionId = (Integer) FacesContext.getCurrentInstance().getExternalContext().getFlash()
					.get("revision_id");
			revision = revisionService.cargarRevision(revisionId);
			informacionCDA = informacionDAO.find(1);
			Long numeroCertificadosDeRevision = certificadoService
					.consultarCuantosCertificadosTieneRevision(revisionId);
			logger.info("Numero de certificados " + numeroCertificadosDeRevision);
			certificadoImpresoPorPrimeraVez = numeroCertificadosDeRevision == 0;
			if (certificadoImpresoPorPrimeraVez) {
				logger.info("Certificado impreso por primera vez");
				calFechaExpedicion = fechaService.obtenerInicioHoy();
				calFechaVencimiento = calcularFechaVencimiento(revision, calFechaExpedicion);
				cargarFechas(calFechaExpedicion, calFechaVencimiento);
			}

			Long numeroCertificadosNoAnulados = certificadoService.consultarNumeroCertificadosNoAnulados(revisionId);
			anularImprimirNuevoCertificado = numeroCertificadosNoAnulados > 0;
			if (anularImprimirNuevoCertificado) {
				certificadoParaAnular = certificadoService.consultarUltimoCertificado(revisionId);
				cargarFechas(certificadoParaAnular);
			}
			// cagar informacion del siguiente consecutivo de preimpreso
			consecutivoPreimpreso = revision.getConsecutivoRunt();

			// Formulario formulario =
			// formularioService.generarFormularioSegundaEtapa(revisionId);
			// manejadorOperacionesCi2.reportarFur(formulario);

		} catch (Exception exc) {
			utilErrores.addError("No se puede mostrar  la pagina" + exc.getMessage());
			logger.log(Level.SEVERE, "Error no se puede cargar vista certificados", exc);
			throw exc;
		}
	}

	private void cargarFechas(Calendar calFechaExpedicion, Calendar calFechaVencimiento) {
		diaExpedicion = calFechaExpedicion.get(Calendar.DAY_OF_MONTH);
		mesExpedicion = calFechaExpedicion.get(Calendar.MONTH) + 1;
		anioExpedicion = calFechaExpedicion.get(Calendar.YEAR);

		diaVencimiento = calFechaVencimiento.get(Calendar.DAY_OF_MONTH);
		mesVencimiento = calFechaVencimiento.get(Calendar.MONTH) + 1;
		anioVencimiento = calFechaVencimiento.get(Calendar.YEAR);

	}

	private Calendar calcularFechaVencimiento(Revision revision2, Calendar calFechaExpedicion) {
		Calendar calFechaVencimientoCalculado = Calendar.getInstance();
		calFechaVencimientoCalculado.setTime(calFechaExpedicion.getTime());
		calFechaVencimientoCalculado.add(Calendar.YEAR, 1);
		return calFechaVencimientoCalculado;
	}

	private void cargarFechas(Certificado certificadoParaAnular2) {
		Timestamp tsFechaExpedicion = certificadoParaAnular2.getFechaExpedicion();
		Timestamp tsFechaVencimiento = certificadoParaAnular2.getFechaVencimiento();
		Date fechaExpedicion = new Date(tsFechaExpedicion.getTime());
		Date fechaVencimiento = new Date(tsFechaVencimiento.getTime());

		calFechaExpedicion.setTime(fechaExpedicion);
		calFechaVencimiento.setTime(fechaVencimiento);

		diaExpedicion = calFechaExpedicion.get(Calendar.DAY_OF_MONTH);
		mesExpedicion = calFechaExpedicion.get(Calendar.MONTH) + 1;
		anioExpedicion = calFechaExpedicion.get(Calendar.YEAR);

		diaVencimiento = calFechaVencimiento.get(Calendar.DAY_OF_MONTH);
		mesVencimiento = calFechaVencimiento.get(Calendar.MONTH) + 1;
		anioVencimiento = calFechaVencimiento.get(Calendar.YEAR);
	}


	public void imprimirCertificado(ActionEvent ae){
		if(revision != null){
			  

			try {	
				if (revision.getConsecutivoRunt() == null || revision.getConsecutivoRunt().isEmpty()){
					utilErrores.addError("Ingrese consecutivo runt a la revision");					
					return;
				}
				
				long numeroCertificadosDuplicados = certificadoService.consultarCertificadoConsecutivoSustratoExistente(consecutivoPreimpreso);
				if(numeroCertificadosDuplicados > 0) {
					utilErrores.addError("Consecutivo impreso en papel esta registrado ya");
					return;
				}
				
				long numeroCertificadosConsecutivoRuntDuplicados = certificadoService.consultarCertificadoConsecutivoRuntExistente(revision.getConsecutivoRunt());
				if(numeroCertificadosConsecutivoRuntDuplicados > 0) {
					utilErrores.addError("Consecutivo runt de la revision ya esta registrado");
					return;
				}
				if(! deshabilitarImprimir) {
					certificadoNuevo = new Certificado();
					certificadoNuevo.setConsecutivoRunt(revision.getConsecutivoRunt());
					certificadoNuevo.setConsecutivoSustrato(consecutivoPreimpreso);
					certificadoNuevo.setRevision(revision);
					certificadoNuevo.setFechaImpresion(new Timestamp(new Date().getTime()));
					certificadoNuevo.setAnulado(false);
					certificadoNuevo.setImpreso(true);
				
					Date fechaExpedicion = calFechaExpedicion.getTime();
					Timestamp tsFechaExpedicion = new Timestamp(fechaExpedicion.getTime());
				
					Date fechaVencimiento = calFechaVencimiento.getTime();
					Timestamp tsFechaVencimiento = new Timestamp(fechaVencimiento.getTime());				
					certificadoNuevo.setFechaVencimiento(tsFechaVencimiento);
					certificadoNuevo.setFechaExpedicion(tsFechaExpedicion);				
					certificadoService.crearCertificado(certificadoNuevo,revision,consecutivoPreimpreso);				
					logger.log(Level.INFO, "Imprimiendo certificado por primera vez para placa {0} revisionid {1}",
										new Object[]{revision.getVehiculo().getPlaca(),revision.getRevisionId()});						
					
					deshabilitarImprimir = true;

					// generar el pdf y guardarlo en la base de datos

					Integer revisionId = 0;
					try {
						String placa = revision.getVehiculo().getPlaca();
						revisionId = revision.getRevisionId();
						Integer intento = revision.getNumeroInspecciones();
						
						
						logger.log(Level.INFO, "Revision consolidada placa {0} revisionid {1} intento {3}",
								new Object[] { placa, revision.getRevisionId(), intento });

					} catch (Exception exc) {

						logger.log(Level.SEVERE, "Error generando fur ", exc);
						notificacionService.sendNotification(
								"Error, no se pudo guardar fur para la revision " + revisionId, NivelAlerta.ERROR_GRAVE,
								Boolean.TRUE);
					}

					utilErrores.addInfo("OK");
					
					
		        }else{
		        	utilErrores.addError("Certificado ya impreso");
		        }
				
		    } catch (Exception e) {
		    	logger.log(Level.SEVERE, "Error imprimiendo certificado ", e);
		    	utilErrores.addError("No se pudo registrar certificado " + e.getMessage());
		    } finally {
		        
		    }
		} else {
			utilErrores.addInfo("No hay revision");

		}
	}

	public void anularCertificadoImprimirDuplicado(ActionEvent ae) {
		if (revision != null) {
			

			try {
				
				String consecutivoAnterior = certificadoParaAnular.getConsecutivoSustrato();
				if(consecutivoAnterior.equals(consecutivoPreimpreso)) {
					utilErrores.addError("Error, digite un consecutivo runt diferente");
					return;
				}
				certificadoParaAnular.setAnulado(true);
				certificadoParaAnular.setFechaAnulacion(new Timestamp(new Date().getTime()));
				certificadoParaAnular.setMotivoAnulacion(motivoAnulacion);
				// certificadoService.actualizarCertificado(certificadoParaAnular);
				certificadoNuevo = new Certificado();
				certificadoNuevo.setConsecutivoRunt(revision.getConsecutivoRunt());
				certificadoNuevo.setConsecutivoSustrato(consecutivoPreimpreso);
				certificadoNuevo.setRevision(revision);
				certificadoNuevo.setFechaImpresion(new Timestamp(new Date().getTime()));
				certificadoNuevo.setAnulado(false);
				certificadoNuevo.setImpreso(true);

				Date fechaExpedicion = calFechaExpedicion.getTime();
				Timestamp tsFechaExpedicion = new Timestamp(fechaExpedicion.getTime());

				Date fechaVencimiento = calFechaVencimiento.getTime();
				Timestamp tsFechaVencimiento = new Timestamp(fechaVencimiento.getTime());
				certificadoNuevo.setFechaVencimiento(tsFechaVencimiento);
				certificadoNuevo.setFechaExpedicion(tsFechaExpedicion);
				certificadoService.anularCrearCertificado(certificadoNuevo, certificadoParaAnular);

				informacionCDA.setConsecutivoCert(consecutivoPreimpreso);
				informacionDAO.edit(informacionCDA);
				logger.log(Level.INFO, "Imprimiendo duplicado vez para placa {0} revisionid {1}",
						new Object[] { revision.getVehiculo().getPlaca(), revision.getRevisionId() });
				utilErrores.addInfo("Certificado anulado ok");
			} catch (Exception e) {
				utilErrores.addError("Error generando duplicado");
				logger.log(Level.SEVERE, "Error imprimiendo duplicado", e);
			} finally {
				
			}
		} else {
			utilErrores.addInfo("No hay revision");

		}
	}

	public InformacionCda getInformacionCDA() {
		return informacionCDA;
	}

	public void setInformacionCDA(InformacionCda informacionCDA) {
		this.informacionCDA = informacionCDA;
	}

	public Revision getRevision() {
		return revision;
	}

	public void setRevision(Revision revision) {
		this.revision = revision;
	}

	public String getConsecutivoPreimpreso() {
		return consecutivoPreimpreso;
	}

	public void setConsecutivoPreimpreso(String consecutivoPreimpreso) {
		this.consecutivoPreimpreso = consecutivoPreimpreso;
	}

	public String getMotivoAnulacion() {
		return motivoAnulacion;
	}

	public void setMotivoAnulacion(String motivoAnulacion) {
		this.motivoAnulacion = motivoAnulacion;
	}

	public boolean isAnularImprimirNuevoCertificado() {
		return anularImprimirNuevoCertificado;
	}

	public void setAnularImprimirNuevoCertificado(boolean anularImprimirNuevoCertificado) {
		this.anularImprimirNuevoCertificado = anularImprimirNuevoCertificado;
	}

	public boolean isCertificadoImpresoPorPrimeraVez() {
		return certificadoImpresoPorPrimeraVez;
	}

	public void setCertificadoImpresoPorPrimeraVez(boolean certificadoImpresoPorPrimeraVez) {
		this.certificadoImpresoPorPrimeraVez = certificadoImpresoPorPrimeraVez;
	}

	public Certificado getCertificadoParaAnular() {
		return certificadoParaAnular;
	}

	public void setCertificadoParaAnular(Certificado certificadoParaAnular) {
		this.certificadoParaAnular = certificadoParaAnular;
	}

	public Integer getDiaExpedicion() {
		return diaExpedicion;
	}

	public void setDiaExpedicion(Integer diaExpedicion) {
		this.diaExpedicion = diaExpedicion;
	}

	public Integer getMesExpedicion() {
		return mesExpedicion;
	}

	public void setMesExpedicion(Integer mesExpedicion) {
		this.mesExpedicion = mesExpedicion;
	}

	public Integer getAnioExpedicion() {
		return anioExpedicion;
	}

	public void setAnioExpedicion(Integer anioExpedicion) {
		this.anioExpedicion = anioExpedicion;
	}

	public Integer getDiaVencimiento() {
		return diaVencimiento;
	}

	public void setDiaVencimiento(Integer diaVencimiento) {
		this.diaVencimiento = diaVencimiento;
	}

	public Integer getMesVencimiento() {
		return mesVencimiento;
	}

	public void setMesVencimiento(Integer mesVencimiento) {
		this.mesVencimiento = mesVencimiento;
	}

	public Integer getAnioVencimiento() {
		return anioVencimiento;
	}

	public void setAnioVencimiento(Integer anioVencimiento) {
		this.anioVencimiento = anioVencimiento;
	}

	public Boolean getDeshabilitarImprimir() {
		return deshabilitarImprimir;
	}

	public void setDeshabilitarImprimir(Boolean deshabilitarImprimir) {
		this.deshabilitarImprimir = deshabilitarImprimir;
	}

}
