package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import org.omnifaces.cdi.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.Bitacora;
import com.proambiente.modelo.Usuario;
import com.proambiente.webapp.dao.BitacoraFacade;
import com.proambiente.webapp.util.UtilErrores;

@Named
@ViewScoped
public class BitacoraBean implements Serializable{
	
	private static final Logger logger = Logger.getLogger(BitacoraBean.class
			.getName());
	private static final long serialVersionUID = 1L;
	
	@Inject
	BitacoraFacade bitacoraFacade;
	
	@Inject
	UtilErrores utilErrores;
	
	@Inject
	PrincipalService principalService;

	private Bitacora bitacora;
	private boolean deshabilitarBoton;
	private Integer bitacoraId;
	

	@PostConstruct
	public void init(){
		
		if(bitacoraId == null) {
			bitacora = new Bitacora();
			bitacora.setTipoEvento("Reporte de Error");
		}else {
			bitacora = bitacoraFacade.find(bitacoraId);
		}
		
	}


	


	public void guardarCambios(ActionEvent ae){
		try{			
			logger.log(Level.INFO,"Guardando cambios bitacora ");
			Date date = new Date();
			bitacora.setFechaEvento(new Timestamp(date.getTime()));
			
			Usuario usuarioAutenticado = principalService.getUsuarioAutenticado();
			bitacora.setUsuarioId( usuarioAutenticado.getUsuarioId() );
			bitacoraFacade.edit(bitacora);
			deshabilitarBoton = true;			
			utilErrores.addInfo("Evento registrado");
			
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error guardando cambios bitacora " ,exc);
			utilErrores.addError("Error guardando bitacora " );
		}		
	}
	
	public Bitacora getBitacora() {
		return bitacora;
	}

	public void setBitacora(Bitacora bitacora) {
		this.bitacora = bitacora;
	}
	
	public boolean isDeshabilitarBoton() {
		return deshabilitarBoton;
	}

	public void setDeshabilitarBoton(boolean deshabilitarBoton) {
		this.deshabilitarBoton = deshabilitarBoton;
	}

	public Integer getBitacoraId() {
		return bitacoraId;
	}

	public void setBitacoraId(Integer bitacoraId) {
		this.bitacoraId = bitacoraId;
	}

}
