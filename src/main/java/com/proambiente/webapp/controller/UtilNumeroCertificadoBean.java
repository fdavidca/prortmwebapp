package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import com.proambiente.modelo.Certificado;
import com.proambiente.webapp.service.CertificadoService;

public class UtilNumeroCertificadoBean implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1227833892102147093L;

	private static final Logger logger = Logger
			.getLogger(UtilNumeroCertificadoBean.class.getName());
	
	@Inject
	CertificadoService certificadoService;
	
	public String numeroCertificado(Integer revisionId){
		Certificado certificado = null;
		try{
			certificado = certificadoService.consultarUltimoCertificado(revisionId);
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error consultando certificado ", exc);
			return "xxxxx";
		}
		if(certificado != null){
		return certificado.getConsecutivoSustrato() != null ? certificado.getConsecutivoSustrato() : "no asign";
		} else return "----";
	}

}
