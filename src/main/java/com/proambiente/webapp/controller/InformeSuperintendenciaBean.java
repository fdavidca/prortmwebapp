package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

//import javax.enterprise.context.SessionScoped;
import javax.faces.event.ActionEvent;
import org.omnifaces.cdi.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.LazyDataModel;

import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.util.UtilErrores;
import com.proambiente.webapp.util.dto.InformeResultadoTotalDTO;

@Named
@ViewScoped
public class InformeSuperintendenciaBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final Logger logger = Logger.getLogger(InformeSuperintendenciaBean.class.getName());

	private Date fechaInicial;
	private Date fechaFinal;

	@Inject
	private UtilErrores utilErrores;

	@Inject
	@InformeSuperModel
	LazyDataModel<InformeResultadoTotalDTO> lazyDataModel;

	@Inject
	RevisionService revisionService;

	SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd hh:mm");

	public void cargarInformacion(ActionEvent ae) {
		try {
			Calendar calendarFechaFinal = Calendar.getInstance();
			calendarFechaFinal.setTime(fechaFinal);
			calendarFechaFinal.add(Calendar.MONTH, -1);

			Calendar calendarFechaInicial = Calendar.getInstance();
			calendarFechaInicial.setTime(fechaInicial);
			if (calendarFechaFinal.compareTo(calendarFechaInicial) > 0) {
				utilErrores.addInfo("No se puede consultar mas de un mes de informacion");
				return;
			}
			logger.log(Level.INFO, "Informe superintendencia {0}, fecha fin {1}",
					new Object[] { sdf.format(fechaInicial), sdf.format(fechaFinal) });
			LazyInformeSuperintendenciaDataModel lazy = (LazyInformeSuperintendenciaDataModel) lazyDataModel;
			lazy.setFechaFinal(fechaFinal);
			lazy.setFechaInicial(fechaInicial);
			
			

		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Error cargando informacion", exc);
			utilErrores.addError("Error cargando informacion");
		}
	}

	public Date getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public LazyDataModel<InformeResultadoTotalDTO> getLazyDataModel() {
		return lazyDataModel;
	}

	public void setLazyDataModel(LazyDataModel<InformeResultadoTotalDTO> lazyDataModel) {
		this.lazyDataModel = lazyDataModel;
	}
	
	

}
