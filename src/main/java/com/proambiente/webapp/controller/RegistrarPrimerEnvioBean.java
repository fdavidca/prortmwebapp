package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;

import com.proambiente.modelo.Revision;
import com.proambiente.webapp.service.RegistrarPrimerEnvioService;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.service.RolesService;
import com.proambiente.webapp.util.UtilErrores; 

@Named
@ViewScoped
public class RegistrarPrimerEnvioBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5986495108974178973L;


	private static final Logger logger = Logger.getLogger(RegistrarPrimerEnvioBean.class
			.getName());
	

	@Inject
	RevisionService revisionService;
	
	@Inject
	RolesService rolesService;
	
	@Inject
	RegistrarPrimerEnvioService registrarEnvioService;
	
	@Inject
	UtilErrores utilErrores;
	
	private String placa;
	private String test;
	private List<Revision> revisiones;
	private Revision revisionSeleccionada;
	private boolean noAutorizarBorrado = true;


	
	public void buscarRevisiones(ActionEvent ae){
		if(placa != null && !placa.isEmpty()){
			try{
				revisiones = revisionService.buscarTodasRevisionesPorPlaca(placa);
				logger.info("revisiones econtradas placa " + placa + " numero : " + revisiones.size());
				test = "revisiones econtradas placa " + placa + " numero : " + revisiones.size();

			}catch(Exception exc){
				utilErrores.addError("Error consultando inspecciones");
				logger.log(Level.SEVERE, "Error consultando inspecciones por placa", exc);
			}
		}else{
			utilErrores.addError("Ingrese placa");
		}
	}
	
	
	public void registrarPrimerEnvio(ActionEvent ae) {
		try {
			rolesService.checkAdministrador();
			if(revisionSeleccionada == null) {
				utilErrores.addError("Selecione revision");
				return;
			}
			registrarEnvioService.registrarPrimerEnvio(revisionSeleccionada.getRevisionId(), revisionSeleccionada.getPrimerEnvioRealizado());
			utilErrores.addInfo("Registro Ok");
			
		}catch(Exception exc) {
			utilErrores.addError("Error registrando primer envio");
			logger.log(Level.SEVERE, "Error registrando primer envio", exc);
		}
	}
	
	
	
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public List<Revision> getRevisiones() {
		return revisiones;
	}
	public void setRevisiones(List<Revision> revisiones) {
		this.revisiones = revisiones;
	}
	public Revision getRevisionSeleccionada() {
		return revisionSeleccionada;
	}
	public void setRevisionSeleccionada(Revision revisionSeleccionada) {
		this.revisionSeleccionada = revisionSeleccionada;
	}

	public boolean isAutorizarBorrado() {
		return noAutorizarBorrado;
	}

	public void setAutorizarBorrado(boolean autorizarBorrado) {
		this.noAutorizarBorrado = autorizarBorrado;
	}

	public String getTest() {
		return test;
	}

	public void setTest(String test) {
		this.test = test;
	}	
}
