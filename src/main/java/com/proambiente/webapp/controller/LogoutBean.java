package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Logger;

import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import com.proambiente.modelo.Rol;
import com.proambiente.modelo.Usuario;
import com.proambiente.webapp.service.FechaService;

/**
 * Clase de utilidad para implementar el cierre de sesion de la aplicacion
 * 
 * @author FABIAN
 */

@Named
@SessionScoped
public class LogoutBean implements Serializable {

	@Inject
	FechaService fechaService;

	@Inject
	PrincipalService principalService;

	private static final Logger logger = Logger.getLogger(LogoutBean.class.getName());
	private static final long serialVersionUID = 1L;

	public String logout() {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
		session.invalidate();
		logger.info("Session invalidada, usuario cierra sesion");
		return "/index.xhtml?faces-redirect=true";
	}

	public String isPasswordUsuarioCaducada() {

		Usuario usuario = principalService.getUsuarioAutenticado();
		if (usuario != null) {
			Calendar fechaHoy = fechaService.obtenerInicioHoy();
			if (checkPasswordExpired(usuario,fechaHoy)) {
				logger.info("Password caducado ");
				return "/pages/cambiopassword.xhtml?faces-redirect=true";
			} else
				return "";
		}
		return "";
	}

	public void isPasswordExpired(ComponentSystemEvent event) {

		Usuario usuario = principalService.getUsuarioAutenticado();
		if (usuario != null) {
			Calendar fechaHoy = fechaService.obtenerInicioHoy();
			if (checkPasswordExpired(usuario, fechaHoy)) {
				FacesContext fc = FacesContext.getCurrentInstance();
				logger.info("Password caducado ");
				ConfigurableNavigationHandler nav = (ConfigurableNavigationHandler) fc.getApplication()
						.getNavigationHandler();
				nav.performNavigation("/pages/cambiopassword.xhtml?faces-redirect=true");
			}
		}
	}

	public static boolean checkPasswordExpired(Usuario usuario, Calendar calendarFechaHoy) {

		if (usuario != null) {
			Boolean admin = false;
			for (Rol r : usuario.getRols()) {
				if (r.getNombreRol().equalsIgnoreCase("ADMINISTRADOR")) {
					admin = true;
				}
			}
			if (!admin) {
				long tiempoFechaExpiracion = usuario.getFechaExpiracionContrasenia().getTime();
				Date dateFechaExpiracion = new Date(tiempoFechaExpiracion);
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(dateFechaExpiracion);

				return calendarFechaHoy.compareTo(calendar) >= 0;
			} else {
				long tiempoFechaExpiracion = usuario.getFechaExpiracionContrasenia().getTime();
				Date dateFechaExpiracion = new Date(tiempoFechaExpiracion);
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(dateFechaExpiracion);
				calendar.add(Calendar.DAY_OF_YEAR, -15);
				return calendarFechaHoy.compareTo(calendar) >= 0;
			}
		}
		return false;

	}

}
