package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.event.ActionEvent;
import org.omnifaces.cdi.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.proambiente.modelo.Bitacora;
import com.proambiente.modelo.Usuario;
import com.proambiente.webapp.dao.BitacoraFacade;
import com.proambiente.webapp.dao.UsuarioFacade;


@Named
@ViewScoped
public class EntradasBitacoraBean implements Serializable{
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7410645805337351826L;

	private static final Logger logger = Logger
			.getLogger(EntradasBitacoraBean.class.getName());

	private Date fechaInicial;
	private Date fechaFinal;
	
	@Inject
	BitacoraFacade bitacoraDAO;
	
	@Inject
	UsuarioFacade usuarioDAO;
	
	private List<Bitacora> entradasBitacora;
	
	
	public void consultarEntradas(ActionEvent ae){
		try {
			logger.info("Consultando alertas");
			entradasBitacora = bitacoraDAO.buscarBitacoraPorFecha(fechaInicial, fechaFinal)	;	
		} catch (Exception e) {			
			logger.log(Level.SEVERE, "Error consultando pruebas de ruido", e);
		}
	}
	
	public String cargarNombresUsuario(Integer usuarioId) {
		try {
			Usuario usuario = usuarioDAO.find(usuarioId);
			return String.format("%s %s",usuario.getNombres(),usuario.getApellidos());
			
		}catch(Exception exc) {
			logger.log(Level.SEVERE, "Error cargando nombre de usuario", exc);
		}
		return "error";
	}
	
	public Date getFechaInicial() {
		return fechaInicial;
	}
	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	public Date getFechaFinal() {
		return fechaFinal;
	}
	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public List<Bitacora> getEntradasBitacora() {
		return entradasBitacora;
	}

	public void setEntradasBitacora(List<Bitacora> entradasBitacora) {
		this.entradasBitacora = entradasBitacora;
	}	
}

