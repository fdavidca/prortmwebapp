package com.proambiente.webapp.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.PruebaFuga;
import com.proambiente.modelo.Usuario;
import com.proambiente.webapp.dao.AnalizadorFacade;
import com.proambiente.webapp.dao.UsuarioFacade;
import com.proambiente.webapp.service.PruebaFugasService;
import com.proambiente.webapp.util.UtilErrores;

@Named
@ViewScoped
public class InformePruebaFugasBean implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8369252348485702933L;

	private static final Logger logger = Logger
			.getLogger(InformePruebaFugasBean.class.getName());
	
	@Inject
	transient PruebaFugasService pruebaFugaService;
	
	@Inject
	UtilErrores utilErrores;
	
	@Inject
	UsuarioFacade usuarioDAO;
	
	@Inject
	AnalizadorFacade analizadorDAO;
	
	private Date fechaInicial;
	private Date fechaFinal;
	
	private List<PruebaFuga> pruebaFugasConsultadas;

	public void consultarPruebas(ActionEvent ae){
		try{
			
			pruebaFugasConsultadas = pruebaFugaService.consultarPruebaFugaPorFecha(fechaInicial, fechaFinal);
			
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error consultando prueba de fugas", exc);
			utilErrores.addError("Error consultando prueba de fugas");
		}
	}
	
	public Double diferenciaPresion(PruebaFuga pruebaFuga){
		if(pruebaFuga.getPresionInicial() != null  && pruebaFuga.getPresionFinal() != null)
			return pruebaFuga.getPresionInicial() - pruebaFuga.getPresionFinal();
		else return 0.0;
	}
	
	public Long bucarUsuarioPrueba(PruebaFuga pruebaFuga){
		if(pruebaFuga.getUsuarioId() != null){
			Usuario usuario = usuarioDAO.find(pruebaFuga.getUsuarioId()); 
			return usuario.getCedula();
		}
		else {
			return 0L;
		}
	}
	
	public String buscarSerialEquipoCompleto(String serialBanco){
		return analizadorDAO.obtenerSerialEquipoCompleto(serialBanco);
	}
	
	
	public void verReporte(Integer pruebaFugaId) {
		try {
			logger.log(Level.INFO, "Ver reportede prueba de fugas para {0}", pruebaFugaId);
			FacesContext.getCurrentInstance().getExternalContext()
					.redirect("/prortmwebapp/ServletPruebaFugas?id_prueba_fugas=" + pruebaFugaId);
		} catch (IOException e) {
			utilErrores.addError("Erro cargando reporte de fugas");
			logger.log(Level.SEVERE, "Error cargando reporte de fugas", e);
		}
	}
	
	public List<PruebaFuga> getPruebaFugasConsultadas() {
		return pruebaFugasConsultadas;
	}





	public void setPruebaFugasConsultadas(List<PruebaFuga> pruebaFugasConsultadas) {
		this.pruebaFugasConsultadas = pruebaFugasConsultadas;
	}





	public Date getFechaInicial() {
		return fechaInicial;
	}
	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	public Date getFechaFinal() {
		return fechaFinal;
	}
	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}
	
}