package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import org.omnifaces.cdi.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.Pista;
import com.proambiente.modelo.TipoPista;
import com.proambiente.webapp.dao.PistaFacade;
import com.proambiente.webapp.dao.TipoPistaFacade;
import com.proambiente.webapp.util.UtilErrores;

@Named
@ViewScoped
public class PistasBean implements Serializable{
	
	private static final Logger logger = Logger.getLogger(PistasBean.class
			.getName());
	private static final long serialVersionUID = 1L;
	
	@Inject
	PistaFacade pistaFacade;
	
	@Inject
	TipoPistaFacade tipoPistaFacade;
	
	@Inject
	UtilErrores utilErrores;

	private List<Pista> listaPistas;
	
	private Pista pistaSeleccionada = new Pista();
	
	private List<TipoPista> listaTiposPista;


	@PostConstruct
	public void init(){
		listaPistas = pistaFacade.findAll();
		listaTiposPista = tipoPistaFacade.findAll();
	}


	public void guardarCambios(ActionEvent ae){
		try{
			logger.log(Level.INFO,"Guardando cambios para " + pistaSeleccionada.toString());
			pistaFacade.edit(pistaSeleccionada);
			init();
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error guardando cambios para " + pistaSeleccionada.toString(),exc);
			utilErrores.addError("Error guardando campos para " + pistaSeleccionada.toString());
		}
		
	}


	public List<Pista> getListaPistas() {
		return listaPistas;
	}


	public void setListaPistas(List<Pista> listaPistas) {
		this.listaPistas = listaPistas;
	}


	public Pista getPistaSeleccionada() {
		return pistaSeleccionada;
	}


	public void setPistaSeleccionada(Pista pistaSeleccionada) {
		this.pistaSeleccionada = pistaSeleccionada;
	}


	public List<TipoPista> getListaTiposPista() {
		return listaTiposPista;
	}


	public void setListaTiposPista(List<TipoPista> listaTiposPista) {
		this.listaTiposPista = listaTiposPista;
	}
	
	public void nuevaPista(ActionEvent ae){
		try{
			pistaSeleccionada = new Pista();
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error", exc);
			utilErrores.addError("Error"+ exc.getMessage());
		}
	}
	
	public void attachPista(ActionEvent ae){
		try{
			
		}catch(Exception exc){
			
		}
	}
	

}
