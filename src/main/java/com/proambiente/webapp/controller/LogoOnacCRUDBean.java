package com.proambiente.webapp.controller;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.view.ViewScoped;
import javax.imageio.ImageIO;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.proambiente.modelo.LogoOnac;
import com.proambiente.webapp.dao.LogoOnacFacade;
import com.proambiente.webapp.util.UtilErrores;

@Named
@SessionScoped
public class LogoOnacCRUDBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 31L;

	private static final Logger logger = Logger.getLogger(LogoOnacCRUDBean.class.getName());

	@Inject
	LogoOnacFacade logoOnacFacade;

	@Inject
	UtilErrores utilErrores;

	List<LogoOnac> listaLogos;

	LogoOnac logoOnac;

	@PostConstruct
	public void init() {
		logoOnac = new LogoOnac();
		listaLogos = logoOnacFacade.findAll();
	}
	
	public void consultar() {
		listaLogos = logoOnacFacade.findAll();
	}

	public void nuevoLogo(ActionEvent ae) {

		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("editarlogoonac.xhtml?faces-redirect=true");
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Error redirigiendo a pagina de pruebas", e);
			utilErrores.addError("Error");
		}
	}

	
	public void editarLogo(Long idLogo) {
		if (idLogo != null) {
			try {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("logo_onac_id", idLogo);
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("editarlogoonac.xhtml?faces-redirect=true");
			} catch (IOException e) {
				logger.log(Level.SEVERE, "Error redirigiendo a pagina de pruebas", e);
				utilErrores.addError("Error");
			}
		} else {
			utilErrores.addInfo("Seleccione una revision");
		}
	}

	public LogoOnac getLogoOnac() {
		return logoOnac;
	}

	public void setLogoOnac(LogoOnac logoOnac) {
		this.logoOnac = logoOnac;
	}

	public List<LogoOnac> getListaLogos() {
		return listaLogos;
	}

	public void setListaLogos(List<LogoOnac> listaLogos) {
		this.listaLogos = listaLogos;
	}

	
	public StreamedContent getGraphic(Long idLogo) {
        try {
        	
             DefaultStreamedContent content = DefaultStreamedContent.builder()
                    .contentType("image/png")
                    .stream(() -> {
                        try {
                        	logger.log(Level.INFO, "cargando imagen para " + idLogo);
                        	LogoOnac logo = logoOnacFacade.find(idLogo);
                        	if(logo!=null && logo.getImagen()!=null) {
	                        	BufferedImage bufferedImg = ImageIO.read(new ByteArrayInputStream(logo.getImagen()));                   
	                            ByteArrayOutputStream os = new ByteArrayOutputStream();
	                            ImageIO.write(bufferedImg, "png", os);
	                            return new ByteArrayInputStream(os.toByteArray());
                        	}else {
                        		logger.log(Level.INFO, "no se encuentra imagen para " + idLogo);
                        		BufferedImage bufferedImg = new BufferedImage(100, 25, BufferedImage.TYPE_INT_RGB);
                                Graphics2D g2 = bufferedImg.createGraphics();
                                g2.drawString("No asignado", 0, 10);
                                ByteArrayOutputStream os = new ByteArrayOutputStream();
                                ImageIO.write(bufferedImg, "png", os);
                                return new ByteArrayInputStream(os.toByteArray());
                        	}
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                            return null;
                        }
                    })
                    .build();
             return content;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
