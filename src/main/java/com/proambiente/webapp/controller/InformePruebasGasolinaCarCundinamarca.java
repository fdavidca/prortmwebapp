package com.proambiente.webapp.controller;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.PrintWriter;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.omnifaces.cdi.ViewScoped;

import com.proambiente.modelo.Analizador;
import com.proambiente.modelo.ClaseVehiculo;
import com.proambiente.modelo.Defecto;
import com.proambiente.modelo.Equipo;
import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.Inspeccion;
import com.proambiente.modelo.LineaVehiculo;
import com.proambiente.modelo.Marca;
import com.proambiente.modelo.Medida;
import com.proambiente.modelo.Pipeta;
import com.proambiente.modelo.Propietario;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.PruebaFuga;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Servicio;
import com.proambiente.modelo.TipoCombustible;
import com.proambiente.modelo.Usuario;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.modelo.VerificacionGasolina;
import com.proambiente.modelo.dto.ResultadoOttoVehiculosDTO;
import com.proambiente.webapp.dao.EquipoFacade;
import com.proambiente.webapp.dao.InformacionCdaFacade;
import com.proambiente.webapp.dao.PermisibleFacade;
import com.proambiente.webapp.dao.PipetaFacade;
import com.proambiente.webapp.dao.UsuarioFacade;
import com.proambiente.webapp.service.CertificadoService;
import com.proambiente.webapp.service.InspeccionService;
import com.proambiente.webapp.service.PruebaFugaAprobadaService;
import com.proambiente.webapp.service.PruebasService;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.service.VersionSoftwareService;
import com.proambiente.webapp.service.generadordto.GeneradorDTOResultados;
import com.proambiente.webapp.service.indra.UtilConvertirServicioSuperintendencia;
import com.proambiente.webapp.util.ConstantesMedidasKilometraje;
import com.proambiente.webapp.util.UtilErrores;
import com.proambiente.webapp.util.dto.InformeGasolinaCarDTO;
import com.proambiente.webapp.util.dto.ResultadoOttoInformeDTO;
import com.proambiente.webapp.util.dto.ResultadoSonometriaInformeDTO;
import com.proambiente.webapp.util.dto.sda.CausaRechazoVehiculosOttoCarCundinamarca;
import com.proambiente.webapp.util.informes.DeterminadorCausaRechazoVehiculosOttoCar;
import com.proambiente.webapp.util.informes.medellin.GeneradorInformeLivianosRes0762;
import com.proambiente.webapp.util.informes.medellin.InformeLivianosRes0762;

/**
 * Clase que sirve como controlador de la vista que muestra
 * la infomacion de las pruebas de gasolina
 * @author FABIAN 
 */


@Named
@ViewScoped
public class InformePruebasGasolinaCarCundinamarca implements Serializable {

	
	private static final Logger logger = Logger
			.getLogger(InformePruebasGasolinaCarCundinamarca.class.getName());
	
	private static final long serialVersionUID = 1L;
	private static final String SI = "SI";
	private static final String NO = "NO";


	
	@Inject
	CertificadoService certificadoService;
	
	@Inject
	private PruebasService pruebaService;
	
	@Inject
	InspeccionService inspeccionService;
	
	@Inject
	private EquipoFacade equipoFacade;
	
	@Inject
	VersionSoftwareService versionSoftwareService;

	@Inject
	private InformacionCdaFacade informacionCda;
	
	@Inject
	private PermisibleFacade permisibleFacade;
	
	@Inject
	PipetaFacade pipetaFacade;
	
	@Inject
	UsuarioFacade usuarioFacade;

	@Inject
	private UtilErrores utilErrores;

	private Integer progress = 1;

	@Resource
	private ManagedExecutorService managedExecutorService;

	private InformacionCda infoCda;

	private Date fechaInicial, fechaFinal;

	private List<Prueba> pruebasOtto;
	
	private List<InformeGasolinaCarDTO> resultados;

	private List<String[]> datos;

	private SimpleDateFormat sdfPrueba;

	private String nombreSoftware;

	private String versionSoftware;
	
	@Inject
	PruebaFugaAprobadaService pruebaFugaAprobadaService;
	
	@Inject
	RevisionService revisionService;
	
	@PostConstruct
	public void init() {
		nombreSoftware = "PRORTM";
		versionSoftware = versionSoftwareService.obtenerVersionSoftware();
	}
	
	
	

	public void consultarPruebas(ActionEvent ae) {
		try {
			resultados =  Collections.synchronizedList(new ArrayList<>());
			
			Calendar calendarFechaFinal = Calendar.getInstance();
			calendarFechaFinal.setTime(fechaFinal);
			calendarFechaFinal.add(Calendar.MONTH,-1);
			
			Calendar calendarFechaInicial = Calendar.getInstance();
			calendarFechaInicial.setTime(fechaInicial);
			if(calendarFechaFinal.compareTo(calendarFechaInicial) > 0){
				utilErrores.addInfo("No se puede consultar mas de un mes de informacion");
				return;
			}
			
			
			
			logger.log(Level.INFO, "Pruebas ciclo otto: fecha inicio {0}, fecha final{1}",
						new Object[]{fechaInicial,fechaFinal});
			pruebasOtto = inspeccionService.pruebasOttoDeInspeccionPorFecha(fechaInicial,
					fechaFinal);
			if (pruebasOtto.size() == 0)
				return;
			logger.info("Numero Pruebas:" + pruebasOtto.size());
			infoCda = informacionCda.find(1);
			
			sdfPrueba = new SimpleDateFormat("yyyy/MM/dd,HH:mm:ss");
			if (datos != null)
				datos.clear();

			int NUM_HILOS = 10;

			//crea un hilo que procesa cada prueba paralelamente, termina cuando todos los hilos 
			//han completado ejecucion
			if (pruebasOtto.size() < NUM_HILOS) {
				CountDownLatch sincronizador = new CountDownLatch(
						pruebasOtto.size());
				for (Prueba prueba : pruebasOtto) {
					Integer revisionId = prueba.getRevision().getRevisionId();
					List<Prueba> pruebas = pruebaService.pruebasRevision(revisionId);
					Optional<Prueba> pruebaSonometria = pruebas.stream().filter( p->p.getTipoPrueba().getTipoPruebaId().equals(7)).findFirst();
					managedExecutorService.submit(new ProcesadorPruebaInformeOttoCar(prueba,
							sincronizador,pruebaSonometria.orElse( null )));
				}
				sincronizador.await();
			} else {
				//Dividir el procesamiento en lotes de 10 pruebas
				CountDownLatch sincronizador = new CountDownLatch(NUM_HILOS);

				int contador = 1;
				for (Prueba prueba : pruebasOtto) {
					
					Integer revisionId = prueba.getRevision().getRevisionId();
					List<Prueba> pruebas = pruebaService.pruebasRevision(revisionId);
					Optional<Prueba> pruebaSonometria = pruebas.stream().filter( p->p.getTipoPrueba().getTipoPruebaId().equals(7)).findFirst();
					
					managedExecutorService.submit(new ProcesadorPruebaInformeOttoCar(prueba,
							sincronizador,pruebaSonometria.orElse(null)));
					
					
					//cada 10 hilos esperar a que finalice, o si ya termino de procesar todas las pruebas
					if (contador > 0
							&& (contador % NUM_HILOS == 0 || contador == (pruebasOtto
									.size()))) {
						sincronizador.await();
					}
					if (contador % NUM_HILOS == 0) {//cada 10 pruebas crea un nuevo sincronizador
						if ((pruebasOtto.size() - contador) > NUM_HILOS
								&& contador != pruebasOtto.size()) {
							sincronizador = new CountDownLatch(NUM_HILOS);
							progress = (int) ((contador * 1.0)
									/ pruebasOtto.size() * 100.0);
						} else {
							sincronizador = null;
							sincronizador = new CountDownLatch(
									(pruebasOtto.size() - contador));
						}
					}
					contador++;
				}
				progress = 100;
			}
			pruebasOtto.clear();

		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Error consultando datos pruebas de gasolina", exc);
			utilErrores.addError("Error" + exc.getMessage());
		}
	}

	
	private static final char DECIMAL_SEPARATOR_PUNTO = '.';

	public void establecerPuntoSimboloDecimal(DecimalFormat df) {
		DecimalFormatSymbols ds = DecimalFormatSymbols.getInstance();
		ds.setDecimalSeparator(DECIMAL_SEPARATOR_PUNTO);
		df.setDecimalFormatSymbols(ds);
	}
	
	

	
	
	
	public String exportarInforme(){
        try {

            

			FacesContext context = FacesContext.getCurrentInstance();
			HttpServletResponse res = (HttpServletResponse) context.getExternalContext().getResponse();
			res.setContentType("application/text");
			res.setHeader("Content-disposition", "attachment;filename=informe_gasolina.csv");

			ServletOutputStream out = res.getOutputStream();		
			PrintWriter pw = new PrintWriter(out);
			for (String[] lista : datos) {
				StringBuilder sb = new StringBuilder();
				for(String valor: lista) {
					sb.append(valor != null ? valor : "").append(";");
				}
				pw.println(sb.toString());
			}
			pw.flush();
            out.flush();
            out.close();
            FacesContext.getCurrentInstance().responseComplete();
            
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error exportando informe de pruebas ciclo otto", e);            
        }
        return null;
    }

	

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public Date getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public List<Prueba> getPruebasOtto() {
		return pruebasOtto;
	}

	public void setPruebasOtto(List<Prueba> pruebasOtto) {
		this.pruebasOtto = pruebasOtto;
	}

	public List<String[]> getDatos() {
		return datos;
	}

	public void setDatos(List<String[]> datos) {
		this.datos = datos;
	}

	public Integer getProgress() {
		return progress;
	}

	public void setProgress(Integer progress) {
		this.progress = progress;
	}

	public List<InformeGasolinaCarDTO> getResultados() {
		return resultados;
	}

	public void setResultados(List<InformeGasolinaCarDTO> resultados) {
		this.resultados = resultados;
	}



	class ProcesadorPruebaInformeOttoCar implements Runnable {

		private Prueba pruebaGases;
		private Prueba pruebaSonometria;
		private CountDownLatch contador;

		public ProcesadorPruebaInformeOttoCar(Prueba pruebaP, CountDownLatch countDownLatch,Prueba pruebaSonometria) {
			this.contador = countDownLatch;
			this.pruebaGases = pruebaP;
			this.pruebaSonometria = pruebaSonometria;
		}

		@Override
		public void run() {
			try {
				
				 Map<String,String> temperaturaPorCodigo= new HashMap<>();
				 temperaturaPorCodigo.put("1","Aceite");
				 temperaturaPorCodigo.put("2","Bloque");
				 temperaturaPorCodigo.put("3","Tiempo");
				InformeGasolinaCarDTO informe = new InformeGasolinaCarDTO();
				Integer pruebaId = pruebaGases.getPruebaId();
				String pruebaIdStr = String.valueOf(pruebaId);
				informe.setConsecutivoPrueba(pruebaIdStr);
				GeneradorInformeLivianosRes0762 generadorInforme = new GeneradorInformeLivianosRes0762();
				GeneradorDTOResultados generadorResultados = new GeneradorDTOResultados();
				generadorResultados.setPuntoComoSeparadorDecimales();
				try{
					String numeroCertificadoRevision = certificadoService.consultarUltimoConsecutivoCertificado(pruebaGases.getRevision().getRevisionId());				
					informe.setNumeroCertificadoRTM(numeroCertificadoRevision);
				}catch (RuntimeException rexc){
					informe.setNumeroCertificadoRTM("");
					logger.log(Level.INFO, "Error consultando numero de certificado de revision");
				}
				
				logger.log(Level.INFO, "Procesando prueba : " + pruebaGases.getPruebaId());
				String consecutivoCertificado = "";
				try {
					consecutivoCertificado = certificadoService
							.consultarUltimoConsecutivoCertificado(pruebaGases.getRevision().getRevisionId());
				} catch (Exception exc) {
					logger.log(Level.SEVERE, "Error cargando certificado" + pruebaGases.getRevision().getRevisionId());
				}

				InformeLivianosRes0762 subInforme = new InformeLivianosRes0762();
				informe.setNumeroCda(infoCda.getNumeroCda());
				informe.setCodigoCiudad(infoCda.getCodigoDivipo());
				generadorInforme.agregarInformacionCda(infoCda, subInforme);
				generadorInforme.agregarInformacionPrueba(subInforme, pruebaGases);
				generarFechaInicioFinPrueba(informe,pruebaGases);
				generadorInforme.agregarInformacionSoftware(subInforme, nombreSoftware, versionSoftware);
				generadorInforme.agregarInformacionCertificado(subInforme, consecutivoCertificado);

				Propietario propietario = pruebaGases.getRevision().getPropietario();

				if (propietario != null) {
					generadorInforme.agregarInformacionPropietario(propietario, subInforme);
					agregarCodigoCiudadPropietario(propietario,informe);
				}
				Vehiculo vehiculo = pruebaGases.getRevision().getVehiculo();
				if (vehiculo != null) {
					generadorInforme.agregarInformacionVehiculo(vehiculo, subInforme);
					agregarCodigoLinea(vehiculo,informe);
					agregarCodigoClase(vehiculo,informe);
					agregarCodigoCombustible(vehiculo,informe);
					agregarCodigoServicio(vehiculo,informe);
					agregarCodigoMarca(vehiculo,informe);
				}

				generadorInforme.agregarInformacionAnalizador(pruebaGases.getAnalizador(), subInforme);
				Double pef = pruebaGases.getAnalizador().getPef();

				VerificacionGasolina verificacion = pruebaGases.getVerificacionGasolina();

				if (verificacion.getSpanAltoPipetaId() != null && verificacion.getSpanBajoPipetaId() != null) {

					Pipeta pipetaAlta = pipetaFacade.find(verificacion.getSpanAltoPipetaId());
					Pipeta pipetaBaja = pipetaFacade.find(verificacion.getSpanBajoPipetaId());
					generadorInforme.agregarInformacionPipetas(subInforme, pipetaBaja, pipetaAlta, pef);

				}

				Usuario usuario = usuarioFacade.find(verificacion.getUsuarioId());

				generadorInforme.agregarResultadoVerificacion(subInforme, verificacion, usuario, pef);
				
				

				Revision revision = pruebaGases.getRevision();
				Date fechaRevision = revision.getFechaCreacionRevision();
				Integer jefeTecnicoId = revision.getUsuarioResponsableId();
				Usuario jefeTecnico = null;
				if (revision.getNumeroInspecciones().equals(2)) {

					List<Inspeccion> inspecciones = inspeccionService
							.obtenerInspeccionesPorRevisionId(revision.getRevisionId());
					if (inspecciones.size() > 1) {
						Inspeccion segunda = inspecciones.get(1);
						fechaRevision = segunda.getFechaInspeccionAnterior();
						jefeTecnico = segunda.getUsuarioResponsable();
					}

				} else {

					jefeTecnico = usuarioFacade.find(jefeTecnicoId);

				}

				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				String fechaFur = sdf.format(fechaRevision);

				generadorInforme.agregarInformacionRevision(subInforme, pruebaGases.getRevision(), fechaFur, jefeTecnico);

				List<Medida> medidas = pruebaService.obtenerMedidas(pruebaGases);

				List<Defecto> defectos = pruebaService.obtenerDefectos(pruebaGases);

				cargarKilometraje(revision.getRevisionId(), subInforme);
				List<Equipo> equipos = pruebaService.obtenerEquipos(pruebaGases);
				
				generadorInforme.agregarInformacionEquipos( equipos, subInforme);

				ResultadoOttoVehiculosDTO resultado = generadorResultados.generarResultadoOtto(medidas, defectos);
				

				generadorInforme.cargarInformacionResultado(subInforme, resultado);
				
				
				if(pruebaGases.isDilucion()) {
					subInforme.setPresenciaDilucion("SI");
				}else {
					subInforme.setPresenciaDilucion("NO");
				}
				subInforme.setLugarTomaTemperaturaVehiculo(pruebaGases.getMetodoTemperatura());
				if(pruebaGases.getMetodoTemperatura().equals("3")) {
					informe.setTemperaturaMotor("NA");
				}else {
					informe.setTemperaturaMotor(subInforme.getTemperaturaMotor());
				}

				ResultadoOttoInformeDTO r2 = generadorResultados.generarResultadoOttoInforme(medidas, defectos);

				generadorInforme.cargarInformacionResultadoAdicional(subInforme, r2);
				
				generadorInforme.cargarInformacionMedidasVehiculo(subInforme,medidas);

				String resultadoFinal = pruebaGases.isAprobada() ? "1" : "2";
				resultadoFinal = pruebaGases.isAbortada() ? "3" : resultadoFinal;

				subInforme.setResultadoFinalPrueba(resultadoFinal);
				logger.log(Level.INFO, "Terminada la prueba : " + pruebaGases.getPruebaId());
				
				CausaRechazoVehiculosOttoCarCundinamarca causaRechazo = new DeterminadorCausaRechazoVehiculosOttoCar(permisibleFacade).determinarCausaRechazo(resultado, r2, pruebaGases, medidas, vehiculo);
				if(  causaRechazo.equals(CausaRechazoVehiculosOttoCarCundinamarca.HC_CRUCERO_1984 )
						|| causaRechazo.equals(CausaRechazoVehiculosOttoCarCundinamarca.HC_CRUCERO_1998_2009)
						|| causaRechazo.equals(CausaRechazoVehiculosOttoCarCundinamarca.HC_CRUCERO_1985_1997)
						|| causaRechazo.equals(CausaRechazoVehiculosOttoCarCundinamarca.HC_CRUCERO_2010)
						|| causaRechazo.equals(CausaRechazoVehiculosOttoCarCundinamarca.HC_RALENTI_1984)
						|| causaRechazo.equals(CausaRechazoVehiculosOttoCarCundinamarca.HC_RALENTI_1998_2009)
						|| causaRechazo.equals(CausaRechazoVehiculosOttoCarCundinamarca.HC_RALENTI_1985_1997)
						|| causaRechazo.equals(CausaRechazoVehiculosOttoCarCundinamarca.HC_RALENTI_1998_2009)
						|| causaRechazo.equals(CausaRechazoVehiculosOttoCarCundinamarca.CO_CRUCERO_1984 )
						|| causaRechazo.equals(CausaRechazoVehiculosOttoCarCundinamarca.CO_CRUCERO_1998_2009)
						|| causaRechazo.equals(CausaRechazoVehiculosOttoCarCundinamarca.CO_CRUCERO_1985_1997)
						|| causaRechazo.equals(CausaRechazoVehiculosOttoCarCundinamarca.CO_CRUCERO_2010)
						|| causaRechazo.equals(CausaRechazoVehiculosOttoCarCundinamarca.CO_RALENTI_1984)
						|| causaRechazo.equals(CausaRechazoVehiculosOttoCarCundinamarca.CO_RALENTI_2010)
						|| causaRechazo.equals(CausaRechazoVehiculosOttoCarCundinamarca.CO_RALENTI_1985_1997)
						|| causaRechazo.equals(CausaRechazoVehiculosOttoCarCundinamarca.CO_RALENTI_1998_2009) )		{
					
					informe.setIncumplimientoNivelesEmision(SI);
					
				}else {
					informe.setIncumplimientoNivelesEmision(NO);
				}
				if(pruebaGases.isDilucion()) {
					informe.setDilucion(SI);
				}else {
					informe.setDilucion(NO);
				}
				informe.setCausalRechazo(String.valueOf(causaRechazo.getValue()));
				List<Medida> medidasSonometria = pruebaService.obtenerMedidas(pruebaSonometria);
				List<Equipo> equipos1 =equipoFacade.consultarEquiposPorPrueba( pruebaSonometria.getPruebaId() );
				ResultadoSonometriaInformeDTO resultadoSonometria = generadorResultados.generarInformacionSonometria(medidasSonometria, equipos1);
				informe.setResultadoSonometria(resultadoSonometria);
				
				BeanInfo beanInfo = Introspector.getBeanInfo(InformeGasolinaCarDTO.class);
				for (PropertyDescriptor propertyDesc : beanInfo.getPropertyDescriptors()) {
				    String propertyName = propertyDesc.getName();
				    Object value = propertyDesc.getReadMethod().invoke(informe);
				    if(value instanceof String) {
					    String valueStr = (String)value;
					    if(valueStr.equalsIgnoreCase("TRUE")) {
					    	propertyDesc.getWriteMethod().invoke(informe, "SI");
					    }else if( valueStr.equalsIgnoreCase("FALSE")) {
					    	propertyDesc.getWriteMethod().invoke(informe, "NO");
					    }
				    }
				}
				informe.setSubInforme(subInforme);
				
				resultados.add(informe);
				Thread.sleep(0, 500000);
				medidas.clear();
				defectos.clear();
				equipos.clear();
				medidasSonometria.clear();
				
			} catch (Exception exc) {
				logger.log(Level.SEVERE,"Excepcion :" + pruebaGases.getPruebaId(),exc);				
			} finally {
				contador.countDown();

			}
		}

		

		private void agregarCodigoMarca(Vehiculo vehiculo, InformeGasolinaCarDTO informe) {
			if(vehiculo.getMarca()!=null) {
				Marca marca = vehiculo.getMarca();
				if(marca.getMarcaId()!=null) {
					Integer marcaId = marca.getMarcaId();
					String marcaStr = String.valueOf(marcaId);
					informe.setCodigoMarca(marcaStr);
				}
			}
			
		}

		private void agregarCodigoServicio(Vehiculo vehiculo, InformeGasolinaCarDTO informe) {
			if(vehiculo.getServicio()!=null) {
				Servicio servicio = vehiculo.getServicio();
				if(servicio.getServicioId() != null) {
					Integer codigoServicio =
					UtilConvertirServicioSuperintendencia
					.convertirServicioAsuperintendencia(vehiculo.getServicio().getServicioId());
					String codigoServicioStr = String.valueOf(codigoServicio);
					informe.setCodigoServicio(codigoServicioStr);
				}
			}
			
		}

		private void agregarCodigoCombustible(Vehiculo vehiculo, InformeGasolinaCarDTO informe) {
			if(vehiculo.getTipoCombustible()!=null) {
				TipoCombustible tc = vehiculo.getTipoCombustible();
				if(tc.getCombustibleId() != null) {
					Integer combustibleId = tc.getCombustibleId();
					String combustibleStr = String.valueOf(combustibleId);
					informe.setCodigoCombustible(combustibleStr);
				}
			}
			
		}

		private void agregarCodigoClase(Vehiculo vehiculo, InformeGasolinaCarDTO informe) {
			if(vehiculo.getClaseVehiculo()!=null) {
				ClaseVehiculo clase = vehiculo.getClaseVehiculo();
				if(clase.getClaseVehiculoId() != null) {
					Integer claseId = clase.getClaseVehiculoId();
					String claseIdStr = String.valueOf(claseId);
					informe.setCodigoClaseVehiculo(claseIdStr);
				}
			}
			
		}

		private void agregarCodigoLinea(Vehiculo vehiculo, InformeGasolinaCarDTO informe) {
			if(vehiculo.getLineaVehiculo() != null  ) {
				LineaVehiculo linea = vehiculo.getLineaVehiculo();
				if(linea.getCodigoLinea() != null ) {
					Integer codigoLinea = linea.getCodigoLinea();
					String codigoLineaStr = String.valueOf(codigoLinea);
					informe.setCodigoLineaVehiculo(codigoLineaStr);
				}
			}			
		}

		private void agregarCodigoCiudadPropietario(Propietario propietario, InformeGasolinaCarDTO informe) {
			if(propietario.getCiudad() != null) {
				Integer ciudadId = propietario.getCiudad().getCiudadId();
				String codigoCiudad = ciudadId != null ? String.valueOf(ciudadId) : "error";
				informe.setCodigoCiudadPropietario(codigoCiudad);
			}
		}

		private void generarFechaInicioFinPrueba(InformeGasolinaCarDTO informe, Prueba prueba) {
			
			SimpleDateFormat sdf = new SimpleDateFormat( "yyyy/MM/dd, HH:mm:ss");
			informe.setFechaInicioPrueba( sdf.format( prueba.getFechaInicio() ));
			informe.setFechaFinPrueba( sdf.format( prueba.getFechaFinalizacion() ));
			
		}

		

	}
	
	@PreDestroy
	public void limpiarListaDatos() {
		
		if(pruebasOtto != null) {
			pruebasOtto.clear();
		}
		if(datos != null) {
			datos.clear();
		}
	}

	public void cargarResultadoFaltantes(ResultadoOttoInformeDTO re2, InformeGasolinaCarDTO informe) {
		informe.setHumedadRelativa( re2.getHumedadRelativa() );
		informe.setTemperaturaAmbiente( re2.getTemperaturaAmbiente() );
		informe.setIncumplimientoNivelesEmision( re2.getIncumplimientoNivelesDeEmision() );		
	}
	
	private String cargarPruebaFugas(Date fechaPrueba,String serial) {
		LocalDate fecha = Instant.ofEpochMilli(fechaPrueba.getTime()) .atZone(ZoneId.systemDefault()).toLocalDate();
		LocalDateTime startOfDay = fecha.atStartOfDay();
		LocalDateTime endOfDay = LocalTime.MAX.atDate(fecha);
		
		Instant instant = startOfDay.atZone(ZoneId.systemDefault()).toInstant();
		Date dateInicio = Date.from(instant);
		
		Instant instantEnd = endOfDay.atZone(ZoneId.systemDefault()).toInstant();
		Date dateFin = Date.from(instantEnd);
		PruebaFuga pf = pruebaFugaAprobadaService.consultarPruebaFugaPorFecha(dateInicio,dateFin,serial);
		if(pf != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			return sdf.format(pf.getFechaRealizacion());
		}else {
			return "";
		}
		
	}

	private void cargarKilometraje(Integer revisionId, InformeLivianosRes0762 informe) {

		DecimalFormat df = new DecimalFormat("#");
		Prueba pruebaIv = revisionService.buscarPruebaPorTipoPorRevision(revisionId, 1);
		if (pruebaIv != null) {

			List<Medida> medidasIv = pruebaService.obtenerMedidas(pruebaIv);
			Optional<Medida> medidaKilometraje = medidasIv.stream().filter(
					m -> m.getTipoMedida().getTipoMedidaId().equals(ConstantesMedidasKilometraje.KILOMETRAJE_MEDIDA))
					.findAny();
			if (medidaKilometraje.isPresent()) {

				Double kilometraje = medidaKilometraje.get().getValor();
				if (kilometraje >= 0) {
					informe.setKilometraje(df.format(kilometraje));
				} else {
					informe.setKilometraje("NO FUNCIONAL");
				}

			} else {

				Prueba pruebaFrenos = revisionService.buscarPruebaPorTipoPorRevision(revisionId, 5);
				if (pruebaFrenos != null) {

					List<Medida> medidas = pruebaService.obtenerMedidas(pruebaIv);
					Optional<Medida> medidaKilom = medidas.stream().filter(m -> m.getTipoMedida().getTipoMedidaId()
							.equals(ConstantesMedidasKilometraje.KILOMETRAJE_MEDIDA)).findAny();
					if (medidaKilom.isPresent()) {

						Double kilometraje = medidaKilom.get().getValor();
						if (kilometraje >= 0) {
							informe.setKilometraje(df.format(kilometraje));
						} else {
							informe.setKilometraje("NO FUNCIONAL");
						}

					} else {

					}

				}

			}

		}

	}

	

}
