package com.proambiente.webapp.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJBAccessException;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.inject.Inject;
import javax.inject.Named;

import org.omnifaces.util.Faces;
import org.primefaces.PrimeFaces;

import com.proambiente.modelo.NivelAlerta;
import com.proambiente.modelo.Propietario;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Revision_;

import com.proambiente.modelo.Vehiculo;
import com.proambiente.modelo.dto.PruebaDTO;
import com.proambiente.webapp.service.FechaService;
import com.proambiente.webapp.service.FurCi2Codificador;
import com.proambiente.webapp.service.PruebasService;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.service.RolesService;
import com.proambiente.webapp.service.ci2.ManejadorCi2SicovService2V2;

import com.proambiente.webapp.service.ci2.OperacionesSicovCi2V3;
import com.proambiente.webapp.service.guardarpdf.GuardarPdfCallback;
import com.proambiente.webapp.service.guardarpdf.GuardarPdfCallbackFactory;
import com.proambiente.webapp.service.indra.OperacionesSicovIndraV2;
import com.proambiente.webapp.service.notification.NotificationPrimariaService;
import com.proambiente.webapp.service.sicov.OperacionesSicov;
import com.proambiente.webapp.service.sicov.OperacionesSicov.Envio;
import com.proambiente.webapp.service.sicov.OperacionesSicovProducer;
import com.proambiente.webapp.util.RevisionComparator;
import com.proambiente.webapp.util.UtilErrores;

/**
 * Clase que sirve de controlador para la pagina inicio.xhtml
 * 
 * @author FABIAN
 *
 */

@Named
@SessionScoped
public class InicioBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = Logger.getLogger(InicioBean.class.getName());

	@Inject
	RevisionService revisionService;

	@Inject
	transient FechaService fechaService;

	@Inject
	UtilErrores utilErrores;

	@Inject
	transient UtilNumeroCertificadoBean utilNumeroCertificado;

	@Inject
	ManejadorCi2SicovService2V2 manejadorCi2;

	@Inject
	FurCi2Codificador furCi2Service;

	@Inject
	GuardarPdfCallbackFactory guardarPdfCallbackFactory;

	private List<Revision> revisionesPruebasTerminadas;
	private List<Revision> revisionesPruebasPendientes;
	private List<Revision> revisionesRegistradas;

	private Date fechaInicial, fechaFinal;

	private String numeroConsecutivoPreimpreso;
	private String numeroConsecutivoRunt;

	private boolean filtrarConsecutivoRunt;
	private boolean filtrarNumeroSolicitud;
	private boolean filtrarCertificado = true;

	private boolean mostrarDialogoCertificado;

	private boolean segundoEnvioRealizado;
	
	private boolean preventiva;

	private List<Revision> revisionesFiltradas;

	private List<PruebaDTO> pruebasDeRevisiones;

	@Inject
	PruebasService pruebaService;

	RevisionComparator revisionComparator;

	@Inject
	transient OperacionesSicovProducer sicovProducer;

	@Inject
	NotificationPrimariaService notificacionService;

	@Inject
	PrincipalService principalService;

	@Inject
	RolesService rolesService;

	OperacionesSicov operacionesSicov;

	private long numeroRevisionesIntento;

	private long numeroRevisionesIntentoFinalizadas;

	private String alerta;

	private Boolean resultadoEnvioVisible;

	public static final String INSPECCION_VISUAL = "INSPECCION_VISUAL";

	public InicioBean() {
		revisionesPruebasTerminadas = new ArrayList<>();
		revisionesPruebasPendientes = new ArrayList<>();
		revisionComparator = new RevisionComparator();

	}

	@PostConstruct
	public void init() {
		Calendar calFechaHoy = fechaService.obtenerInicioHoy();
		fechaInicial = calFechaHoy.getTime();
		calFechaHoy.add(Calendar.DAY_OF_YEAR, 1);
		fechaFinal = calFechaHoy.getTime();
		FacesContext.getCurrentInstance().getExternalContext().getFlash().clear();
		cargarInfoRevisiones();
	}

	public List<Revision> getRevisionesPruebasTerminadas() {
		return revisionesPruebasTerminadas;
	}

	public void setRevisionesPruebasTerminadas(List<Revision> revisionesAprobadas) {
		this.revisionesPruebasTerminadas = revisionesAprobadas;
	}

	public List<Revision> getRevisionesRegistradas() {
		return revisionesRegistradas;
	}

	public void setRevisionesRegistradas(List<Revision> revisionesRegistradas) {
		this.revisionesRegistradas = revisionesRegistradas;
	}

	public Date getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public boolean isMostrarDialogoCertificado() {
		return mostrarDialogoCertificado;
	}

	public void setMostrarDialogoCertificado(boolean mostrarDialogoCertificado) {
		this.mostrarDialogoCertificado = mostrarDialogoCertificado;
	}

	public List<Revision> getRevisionesPruebasPendientes() {
		return revisionesPruebasPendientes;
	}

	public void setRevisionesPruebasPendientes(List<Revision> revisionesPruebasPendientes) {
		this.revisionesPruebasPendientes = revisionesPruebasPendientes;
	}

	public String getNumeroConsecutivoPreimpreso() {
		return numeroConsecutivoPreimpreso;
	}

	public void setNumeroConsecutivoPreimpreso(String numeroConsecutivoPreimpreso) {
		this.numeroConsecutivoPreimpreso = numeroConsecutivoPreimpreso;
	}

	public String getNumeroConsecutivoRunt() {
		return numeroConsecutivoRunt;
	}

	public void setNumeroConsecutivoRunt(String numeroConsecutivoRunt) {
		this.numeroConsecutivoRunt = numeroConsecutivoRunt;
	}

	public List<Revision> getRevisionesFiltradas() {
		return revisionesFiltradas;
	}

	public void setRevisionesFiltradas(List<Revision> revisionesFiltradas) {
		this.revisionesFiltradas = revisionesFiltradas;
	}

	public void cargarInfoRevisiones() {

		try {
			revisionesPruebasPendientes.clear();
			revisionesPruebasTerminadas.clear();
			if (revisionesRegistradas != null)
				revisionesRegistradas.clear();

			Calendar calendarFechaFinal = Calendar.getInstance();
			calendarFechaFinal.setTime(fechaFinal);
			calendarFechaFinal.add(Calendar.MONTH, -1);

			Calendar calendarFechaInicial = Calendar.getInstance();
			calendarFechaInicial.setTime(fechaInicial);
			if (calendarFechaFinal.compareTo(calendarFechaInicial) > 0) {
				utilErrores.addInfo("No se puede consultar mas de un mes de informacion");
				return;
			}
			revisionesRegistradas = revisionService.consultarRevisionesInspecciones(fechaInicial, fechaFinal,
					segundoEnvioRealizado,Optional.of(preventiva));
			List<Integer> idRevisiones = new ArrayList<>();

			for (Revision revision : revisionesRegistradas) {
				if (revision.getAprobada()) {

					revisionesPruebasTerminadas.add(revision);
					idRevisiones.add(revision.getRevisionId());
					continue;
				}
				String estadoRevision = estadoRevision(revision.getRevisionId());
				if (estadoRevision.equalsIgnoreCase("no_finalizada")) {
					revisionesPruebasPendientes.add(revision);
					idRevisiones.add(revision.getRevisionId());
				} else {

					revisionesPruebasTerminadas.add(revision);
				}

			}
			pruebasDeRevisiones = revisionService.pruebasDTOPorIdRevisiones(idRevisiones);
			revisionesRegistradas.stream().map(r -> r.getRevisionId()).collect(Collectors.toList());
			Collections.sort(revisionesPruebasPendientes, revisionComparator);
			Collections.sort(revisionesPruebasTerminadas, revisionComparator);
			utilErrores.addInfo("Revisiones consultadas");
		} catch (Exception exc) {
			utilErrores.addError("Error" + exc.getCause());
			logger.log(Level.SEVERE, "Error consultando revisiones", exc);
		}
	}

	/**
	 * Redirige a la vista en donde se consolida la revision
	 * 
	 * @param revisionId
	 */
	public void imprimirCertificado(Integer revisionId) {
		try {
			if (!esReporteValido(revisionId)) {
				return;
			}

			if (revisionId != null) {

				Revision revision = revisionService.cargarRevisionDTO(revisionId, Revision_.consecutivoRunt,
						Revision_.primerEnvioRealizado, Revision_.numeroInspecciones, Revision_.preventiva);
				if (revision.getPreventiva()) {
					utilErrores.addError("No puede imprimirse certificado para revision preventiva");
					return;
				}
				if (revision.getConsecutivoRunt() == null || revision.getConsecutivoRunt().isEmpty()) {
					utilErrores.addError("Falta consecutivo runt de la revision ");
					return;
				}
				if (!revision.getPrimerEnvioRealizado()) {
					utilErrores.addError("No se puede consolidar la revision sin realizar primer envio ");
					return;
				}
//				if(revision.getPrimerEnvioRealizado() != null && revision.getNumeroInspecciones() != null) {
//					
//				}

				if (revisionService.evaluarRevision(revisionId).equalsIgnoreCase("aprobada")) {

					logger.log(Level.INFO, "Imprimir certificado para revision {0}", revisionId);
					FacesContext.getCurrentInstance().getExternalContext().getFlash().put("revision_id", revisionId);
					FacesContext.getCurrentInstance().getExternalContext()
							.redirect("pages/certificado.xhtml?faces-redirect=true");

				} else {
					utilErrores.addError("Revision no aprobada");
				}
			} else {
				utilErrores.addInfo("Seleccione revision de la tabla");
			}
		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Error imprimir certificado ", exc);
			utilErrores.addError("Error " + exc.getMessage());
		}

	}

	/**
	 * Metodo que se ejecuta cuando se recibe una notificacion asincrona por el
	 * canal notify ver NotificacionPrimariaService
	 * 
	 * @param event
	 */
	public void pushed(Object event) {

		String mensaje = Faces.getRequestParameter("mensaje");
		String nivel = Faces.getRequestParameter("nivel");

		if (nivel.equalsIgnoreCase("error") || nivel.equalsIgnoreCase("ERROR_GRAVE")) {
			this.alerta = "Alerta recibida " + mensaje;
			this.resultadoEnvioVisible = true;

			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_FATAL, nivel, mensaje);
			PrimeFaces.current().dialog().showMessageDynamic(message);

		} else {

			utilErrores.addInfo(mensaje);
		}

	}

	public Boolean getResultadoEnvioVisible() {
		return this.resultadoEnvioVisible;
	}

	public void ocultarResultadoEnvio() {
		this.resultadoEnvioVisible = false;
	}

	/**
	 * Metodo que se ejececuta cuando se da click en primer envio del boton
	 * 
	 * @param revisionId
	 */
	public void validarSicov(Integer revisionId) {
		try {
			rolesService.checkAdministrador();
			if (!esReporteValido(revisionId)) {

			} else {
				String evaluacionRevision = revisionService.evaluarRevision(revisionId);
				if (evaluacionRevision.equalsIgnoreCase("no_finalizada")) {
					utilErrores.addError("La revisión no ha sido finalizada aún");
					return;
				}
				Revision revision = revisionService.cargarRevisionDTO(revisionId, Revision_.preventiva,
						Revision_.primerEnvioRealizado, Revision_.numeroInspecciones);// solo consulta lo necesario

				if (revision.getPrimerEnvioRealizado()) {
					utilErrores.addError("Primer envio ya realizado ");
					return;
				}
				if (revision.getPreventiva()) {
					utilErrores.addError("Revision preventiva ");
					return;
				} else {
					operacionesSicov = sicovProducer.crearOperacionSicov();
					GuardarPdfCallback guardarPdfCallback = guardarPdfCallbackFactory.crearInstancia();
					operacionesSicov.registrarFUR(revisionId, revision.getNumeroInspecciones(), Envio.PRIMER_ENVIO,guardarPdfCallback);
					utilErrores.addInfo("Envio en proceso");
				}
			}
		} catch (EJBAccessException exc) {
			logger.log(Level.SEVERE, " Usuario no tiene privilegios ", exc);
			utilErrores.addError(" Usuario no tiene privilegios");
		} catch (Exception exc) {
			logger.log(Level.SEVERE, " Error al validar sicov", exc);
			utilErrores.addError(" Excepcion al validar sicov");
		}
	}

	

	/**
	 * Metodo que se ejecuta cuando se ca click en el boton segundo envio
	 * 
	 * @param revisionId
	 */
	public void validarSicov2Envio(Integer revisionId) {
		try {

			if (!esReporteValido(revisionId)) {

			} else {
				String evaluacionRevision = revisionService.evaluarRevision(revisionId);
				if (evaluacionRevision.equalsIgnoreCase("no_finalizada")) {
					utilErrores.addError("La revisión no ha sido finalizada aún");
					return;
				}
				Revision revision = revisionService.cargarRevisionDTO(revisionId, Revision_.preventiva,
						Revision_.numeroInspecciones, Revision_.cerrada, Revision_.revisionId,
						Revision_.consecutivoRunt, Revision_.primerEnvioRealizado);// solo consulta lo necesario
				// si no se ha enviado el primer envio
				if (!revision.getPrimerEnvioRealizado()) {
					utilErrores.addError("No ha realizado el primer envio");
					return;
				}

				if (revision.getConsecutivoRunt() == null || revision.getConsecutivoRunt().isEmpty()) {
					utilErrores.addError("Ingrese consecutivo runt de la revision");
					return;
				}
				if (revision.getNumeroInspecciones() == 2 && !revision.getCerrada()) {
					utilErrores.addError("Consolide la revision");
					return;
				}
				if (revision.getPreventiva()) {
					utilErrores.addError("Revision preventiva");
					return;
				} else if (evaluacionRevision.equalsIgnoreCase("aprobada")) {
					GuardarPdfCallback guardarPdfCallback = guardarPdfCallbackFactory.crearInstancia();
					String consecutivoPreimpreso = utilNumeroCertificado.numeroCertificado(revisionId);
					if (consecutivoPreimpreso.equalsIgnoreCase("----")) {
						utilErrores.addError("La revision no tiene certificado asociado");
						return;
					}
					OperacionesSicov opSicov = sicovProducer.crearOperacionSicov();
					if (opSicov instanceof OperacionesSicovCi2V3) {
						OperacionesSicovCi2V3 opCi2 = (OperacionesSicovCi2V3) opSicov;
						opCi2.registrarInformacionCertificado(revision.getRevisionId(), consecutivoPreimpreso,
								revision.getConsecutivoRunt(),guardarPdfCallback);

					} else if (opSicov instanceof OperacionesSicovIndraV2) {
						opSicov.registrarInformacionRunt(revisionId,guardarPdfCallback);
					} else {
						revisionService.registrarSegundoEnvio(revisionId);
					}
				} else if (evaluacionRevision.equalsIgnoreCase("reprobada")) {
					GuardarPdfCallback guardarPdfCallback = guardarPdfCallbackFactory.crearInstancia();
					try {
						OperacionesSicov opSicov = sicovProducer.crearOperacionSicov();
						if (opSicov instanceof OperacionesSicovCi2V3) {
							OperacionesSicovCi2V3 opCi2 = (OperacionesSicovCi2V3) opSicov;
							opCi2.enviarSegundoEnvioRevisionReprobada(revisionId, revision.getConsecutivoRunt(),guardarPdfCallback);

						} else if (opSicov instanceof OperacionesSicovIndraV2) {
							opSicov.registrarInformacionRunt(revisionId,guardarPdfCallback);
						} else {
							revisionService.registrarSegundoEnvio(revisionId);
						}

					} catch (Exception e) {
						logger.log(Level.SEVERE, "Error, no se pudo registrar formulario", e);
						notificacionService.sendNotification(
								"Error, no se pudo registrar formulario segunda vez " + e.getMessage(),
								NivelAlerta.ERROR_GRAVE, Boolean.TRUE);
					}

				}
			}
		} catch (EJBAccessException exc) {
			logger.log(Level.SEVERE, "Proceso restringido para usuario actual", exc);
			utilErrores.addError(" Excepcion al validar sicov");
		} catch (Exception exc) {
			logger.log(Level.SEVERE, " Error al validar sicov", exc);
			utilErrores.addError(" Excepcion al validar sicov");
		}
	}

	/**
	 * Metodo que se ejecuta cuando se oprime el boton pruebas
	 * 
	 * @param revisionId
	 */
	public void verPruebas(Integer revisionId) {
		if (revisionId != null) {
			try {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("revision_id", revisionId);
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("pages/pruebas.xhtml?faces-redirect=true");
			} catch (IOException e) {
				logger.log(Level.SEVERE, "Error redirigiendo a pagina de pruebas", e);
				utilErrores.addError("Error");
			}
		} else {
			utilErrores.addInfo("Seleccione una revision");
		}
	}

	public void abrirDialogoConsecutivos(Integer revisionId) {
		if (revisionId != null) {
			Map<String, Object> options = new HashMap<String, Object>();
			options.put("modal", true);
			options.put("draggable", true);
			options.put("resizable", true);
			options.put("contentWidth", 800);
			options.put("contentHeight", 600);
			options.put("includeViewParams", true);

			Map<String, List<String>> params = new HashMap<String, List<String>>();
			List<String> values = new ArrayList<String>();
			String revisionIdStr = String.valueOf(revisionId);
			values.add(revisionIdStr);
			params.put("revisionId", values);
			PrimeFaces.current().dialog().openDynamic("pages/consecutivofupa.xhtml", options, params);
		} else {
			utilErrores.addInfo("Seleccione una revision");
		}
	}

	public String getAlerta() {
		return this.alerta;
	}

	public void abrirDialogoJefeTecnico(Integer revisionId) {
		if (revisionId != null) {
			Map<String, Object> options = new HashMap<String, Object>();
			options.put("modal", true);
			options.put("draggable", true);
			options.put("resizable", true);
			options.put("contentWidth",800);
			options.put("contentHeight",400);
			options.put("includeViewParams", true);

			Map<String, List<String>> params = new HashMap<String, List<String>>();
			List<String> values = new ArrayList<String>();
			String revisionIdStr = String.valueOf(revisionId);
			values.add(revisionIdStr);
			params.put("revisionId", values);
			PrimeFaces.current().dialog().openDynamic("/pages/asignarusuarioresponsable.xhtml", options, params);
		} else {
			utilErrores.addInfo("Seleccione una revision");
		}
	}

	public String cargarNumeroSolicitud(Integer revisionId) {
		Revision rAux = revisionService.cargarRevisionDTO(revisionId, Revision_.numeroSolicitud);
		return rAux.getNumeroSolicitud();
	}

	public String cargarConsecutivoRunt(Integer revisionId) {
		Revision rAux = revisionService.cargarRevisionDTO(revisionId, Revision_.consecutivoRunt);
		return rAux.getConsecutivoRunt();
	}

	public void filtrarRevisionesConsecutivos() {

		try {
			if (!filtrarConsecutivoRunt && !filtrarNumeroSolicitud) {
				utilErrores.addInfo("No filtros especificados");
				cargarInfoRevisiones();
			} else {
				revisionesPruebasTerminadas = revisionService.consultarRevisionesRegistradas(fechaInicial, fechaFinal,
						true, filtrarConsecutivoRunt, filtrarNumeroSolicitud);
				utilErrores.addInfo("Filtrada");
			}
		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Error filtrando Revisiones consecutivos", exc);
			utilErrores.addError("Error filtrando revisiones");
		}
	}

	public String editarRevision() {
		String direccion = "pages/revisiones.xhtml?faces-redirect=true";
		return direccion;
	}

	public String numeroCertificado(int revisionId, Boolean aprobada) {
		return utilNumeroCertificado.numeroCertificado(revisionId);
	}

	public String estadoRevision(Integer revisionId) {
		if (revisionId != null) {
			Revision revAux = new Revision();
			revAux.setRevisionId(revisionId);
			int revIndex = Collections.binarySearch(revisionesPruebasTerminadas, revAux, revisionComparator);
			if (revIndex >= 0) {
				Revision rev = revisionesPruebasTerminadas.get(revIndex);
				if (rev.getAprobada())
					return "aprobada";
			}
			return revisionService.evaluarRevision(revisionId);
		} else {
			return "--";
		}
	}

	public Boolean esReporteValido(Integer revisionId) {
		try {
			if (revisionId != null) {
				Revision revision = revisionService.cargarRevision(revisionId);
				Propietario propietario = revision.getPropietario();
				Vehiculo vehiculo = revision.getVehiculo();
				Boolean propietarioInvalido = propietario == null;
				Boolean vinInvalido = vehiculo.getVin() == null || vehiculo.getVin().isEmpty();
				Boolean colorInvalido = vehiculo.getColor() == null;
				Boolean numeroMotorInvalido = vehiculo.getNumeroMotor() == null || vehiculo.getNumeroMotor().isEmpty();
				Boolean numeroSillasInvalido = vehiculo.getNumeroSillas() <= 0;
				Boolean cilindrajeInvalido = vehiculo.getCilindraje() < 0;
				

				if (propietarioInvalido || vinInvalido || colorInvalido || numeroMotorInvalido || numeroSillasInvalido
						|| cilindrajeInvalido) {
					StringBuilder sbMensaje = new StringBuilder();
					if (propietarioInvalido) {
						sbMensaje.append("Propietario no diligenciado");
					}
					if (vinInvalido) {
						sbMensaje.append("\n Vin no diligenciado");
					}
					if (colorInvalido) {
						sbMensaje.append("\n Color no diligenciado");
					}
					if (numeroMotorInvalido) {
						sbMensaje.append("\n Numero motor no diligenciado");
					}
					if (numeroSillasInvalido) {
						sbMensaje.append("\n Numero de sillas invalido");
					}
					if (cilindrajeInvalido) {
						sbMensaje.append("\n Cilindraje menor o igual a cero");
					}
					

					throw new RuntimeException(sbMensaje.toString());

				} else {
					System.out.println("Validación de Reporte OK");
					return true;

				}
			} else {
				utilErrores.addError("Parametro revisionId no especificado");
				return false;
			}

		} catch (Exception exc) {
			utilErrores.addError("Error" + exc.getMessage());
			return false;
		}

	}

	public void imprimirReporte(Integer revisionId) {
		try {
			if (esReporteValido(revisionId)) {

				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("ServletReporte?revision=" + revisionId);

			}
		} catch (IOException e) {
			utilErrores.addError("Error imprimiendo reporte");
		}

	}

	public String estadoPrueba(Revision r, Integer tipoPrueba) {
		Optional<PruebaDTO> p;
		p = pruebasDeRevisiones.stream().filter(pdto -> pdto.getRevisionId().equals(r.getRevisionId()))
				.filter(pdto -> pdto.getTipoPruebaId().equals(tipoPrueba)).findFirst();
		if (p.isPresent())
			return buscarPrueba(p.get());
		else
			return "---";
	}

	public String buscarPrueba(PruebaDTO p) {
		if(p.isAbortada()) {
			return "Abortada";
		}else if (!p.isFinalizada()) {
			return "Pendiente";
		} else {
			if (p.isAprobada()) {
				return "Aprobada";
			} else {
				return "Reprobada";
			}
		}

	}

	@PreDestroy
	public void cleanListas() {
		if (revisionesPruebasTerminadas != null) {
			revisionesPruebasTerminadas.clear();
		}
		if (revisionesPruebasPendientes != null) {
			revisionesPruebasPendientes.clear();
		}
		if (revisionesRegistradas != null) {
			revisionesRegistradas.clear();
		}
		if (revisionesFiltradas != null) {
			revisionesFiltradas.clear();
		}
		if (pruebasDeRevisiones != null) {
			pruebasDeRevisiones.clear();
		}
	}

	public void calcularTotal(Object o) {
		Integer valor = (Integer) o;
		numeroRevisionesIntento = revisionesPruebasPendientes.stream()
				.filter(r -> r.getNumeroInspecciones().equals(valor)).count();

	}

	public void calcularTotalFinalizadas(Object objectNumInspeccion) {
		Integer numeroInspeccion = (Integer) objectNumInspeccion;
		numeroRevisionesIntentoFinalizadas = revisionesPruebasTerminadas.stream()
				.filter(r -> r.getNumeroInspecciones().equals(numeroInspeccion)).count();
	}

	public Long getNumeroInspeccionesRevFinalizadas() {
		return numeroRevisionesIntentoFinalizadas;
	}

	public Long getNumeroRevisionesIntento() {
		return numeroRevisionesIntento;
	}

	public boolean isFiltrarConsecutivoRunt() {
		return filtrarConsecutivoRunt;
	}

	public void setFiltrarConsecutivoRunt(boolean filtrarConsecutivoRunt) {
		this.filtrarConsecutivoRunt = filtrarConsecutivoRunt;
	}

	public boolean isFiltrarNumeroSolicitud() {
		return filtrarNumeroSolicitud;
	}

	public void setFiltrarNumeroSolicitud(boolean filtrarNumeroSolicitud) {
		this.filtrarNumeroSolicitud = filtrarNumeroSolicitud;
	}

	public boolean isFiltrarCertificado() {
		return filtrarCertificado;
	}

	public void setFiltrarCertificado(boolean filtrarCertificado) {
		this.filtrarCertificado = filtrarCertificado;
	}

	public boolean isSegundoEnvioRealizado() {
		return segundoEnvioRealizado;
	}

	public void setSegundoEnvioRealizado(boolean segundoEnvioRealizado) {
		this.segundoEnvioRealizado = segundoEnvioRealizado;
	}
	
	

	public boolean isPreventiva() {
		return preventiva;
	}

	public void setPreventiva(boolean preventiva) {
		this.preventiva = preventiva;
	}

	public String finalizar() {
		return "/inicio.xhtml?faces-redirect=true";
	}

}
