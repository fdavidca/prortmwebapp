package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.faces.event.ActionEvent;
import org.omnifaces.cdi.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.LazyDataModel;

import com.proambiente.modelo.Prueba;
import com.proambiente.webapp.util.UtilErrores;
import com.proambiente.webapp.util.dto.InformacionRevisionDTO;
import com.proambiente.webapp.util.dto.InformeGasolinaMotocicletasCarDTO;
import com.proambiente.webapp.util.dto.sda.InformeMotocicletaDTO;



/**
 * Clase que sirve como controlador para el informe de pruebas de gases
 * de vehiculos tipo motocicleta
 */
@Named
@ViewScoped
public class InfoRevisionesMedellinBean implements Serializable{
	
	
	private static final Logger logger = Logger
			.getLogger(InfoRevisionesMedellinBean.class.getName());

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@Inject @PeubaGasesModelMedellin 
	private LazyDataModel<InformacionRevisionDTO> lazyDataModel;

	@Inject
	private UtilErrores utilErrores;

	private Integer progress = 1;

	@Resource
	private ManagedExecutorService managedExecutorService;

	private Date fechaInicial, fechaFinal;

	private List<Prueba> pruebas;

	private List<String[]> datos;

	

	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

	
	
	public void consultarPruebasMotos(ActionEvent ae) {
		try {
			
			//RESTRINGIR A UN MES LA INFORMACION QUE SE PUEDE CARGAR			
			Calendar calendarFechaFinal = Calendar.getInstance();
			calendarFechaFinal.setTime(fechaFinal);
			calendarFechaFinal.add(Calendar.MONTH,-1);
			
			Calendar calendarFechaInicial = Calendar.getInstance();
			calendarFechaInicial.setTime(fechaInicial);
			if(calendarFechaFinal.compareTo(calendarFechaInicial) > 0){
				utilErrores.addInfo("No se puede consultar mas de un mes de informacion");
				return;
			}			
			logger.log(Level.INFO, "Informe motos fecha inicio {0}, fecha fin {1}",
					new Object[]{sdf.format(fechaInicial),sdf.format(fechaFinal)});
			LazyInfoRevisionMedellinDataModel lazy = (LazyInfoRevisionMedellinDataModel)lazyDataModel;
			lazy.setFechaFinal(fechaFinal);
			lazy.setFechaInicial(fechaInicial);
			
		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Error consultando pruebas", exc);
		}
	}

	


	@PreDestroy
	public void limpiarListas() {
		if(pruebas != null) {
			pruebas.clear();
		}
		if(datos != null) {
			datos.clear();
		}
	}

	public List<Prueba> getPruebas() {
		return pruebas;
	}

	public void setPruebas(List<Prueba> pruebas) {
		this.pruebas = pruebas;
	}

	public List<String[]> getDatos() {
		return datos;
	}

	public void setDatos(List<String[]> datos) {
		this.datos = datos;
	}

	public Date getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public Integer getProgress() {
		return progress;
	}

	public void setProgress(Integer progress) {
		this.progress = progress;
	}




	public LazyDataModel<InformacionRevisionDTO> getLazyDataModel() {
		return lazyDataModel;
	}




	public void setLazyDataModel(LazyDataModel<InformacionRevisionDTO> lazyDataModel) {
		this.lazyDataModel = lazyDataModel;
	}

	
}
