package com.proambiente.webapp.controller;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

@Named
@SessionScoped
public class FechaBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1258769345L;
	
	public String getFechaHoraActual() {
		LocalDateTime ldt = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM);
		String formattedDateTime = ldt.format(formatter); 
		return formattedDateTime;
	}

}
