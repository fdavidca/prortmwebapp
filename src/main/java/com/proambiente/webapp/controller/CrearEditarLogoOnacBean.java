package com.proambiente.webapp.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.io.IOUtils;
import org.omnifaces.cdi.ViewScoped;
import org.primefaces.model.file.UploadedFile;

import com.proambiente.modelo.LogoOnac;
import com.proambiente.webapp.dao.LogoOnacFacade;
import com.proambiente.webapp.util.UtilErrores;

import javax.annotation.PostConstruct;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;



@Named
@ViewScoped
public class CrearEditarLogoOnacBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5986495108974178973L;


	private static final Logger logger = Logger.getLogger(CrearEditarLogoOnacBean.class
			.getName());
	

	@Inject
	UtilErrores utilErrores;
	
	@Inject
	LogoOnacFacade logoOnacDAO;
	
	private LogoOnac logoOnac;
	
	private Date fechaVigenciaDesde;
	
	private Date fechaVigenciaHasta;
	
	private UploadedFile uploadedFile;


	private boolean logoOnacSubido;
	
	@PostConstruct
	public void init(){
		
		try{
			Long id = (Long) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("logo_onac_id");
			if(id!=null) {
				logoOnac = logoOnacDAO.find(id);
				if(logoOnac==null) {
					logoOnac= new LogoOnac();
				}
				if(logoOnac.getFechaVigenciaDesde()!=null) {
					fechaVigenciaDesde = new Date(logoOnac.getFechaVigenciaDesde().getTime());
				}
				if(logoOnac.getFechaVigenciaHasta()!=null) {
					fechaVigenciaHasta = new Date(logoOnac.getFechaVigenciaHasta().getTime());
				}
				
				utilErrores.addInfo("Log onac cargado");
				
			}else {
				logoOnac= new LogoOnac();
			}
				
			logger.log(Level.INFO,"Prueba cargada {0}",id);
		}catch(Exception exc){
			utilErrores.addError("No se puede restablecer la vista" + exc.getMessage());			
			logger.log(Level.SEVERE, "Error no se puede cargar la vista de pruebas ", exc);
		}
	}
	
	public void upload() {
		if (uploadedFile != null) {
			InputStream is = null;
			try {
				String filename = uploadedFile.getFileName();
				if( filename == null  || filename.isEmpty() ) {
					utilErrores.addError("Especifique archivo");
					return;
				}
				logger.info("Archivo subido:" + filename);
				is = uploadedFile.getInputStream();
				byte[] bytes = IOUtils.toByteArray(is);
				logger.info("Tamani de archivo" + bytes.length);
				logoOnac.setImagen(bytes);				
				
				utilErrores.addInfo("Logo subido");

			} catch (Exception e) {
				logger.log(Level.SEVERE, "Error subiendo archivo", e);
				utilErrores.addError("Error subiendo archivo");
			} finally {
				try {
					if(is != null)
						is.close();
				} catch (IOException e) {
					logger.log(Level.SEVERE, "Error subiendo archivo logo para fur", e);
				}
			}

		}
	}
	
	

	public UploadedFile getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(UploadedFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

	public LogoOnac getLogoOnac() {
		return logoOnac;
	}

	public void setLogoOnac(LogoOnac logoOnac) {
		this.logoOnac = logoOnac;
	}
	
	
	
	public Date getFechaVigenciaDesde() {
		return fechaVigenciaDesde;
	}



	public void setFechaVigenciaDesde(Date fechaVigenciaDesde) {
		this.fechaVigenciaDesde = fechaVigenciaDesde;
	}



	public Date getFechaVigenciaHasta() {
		return fechaVigenciaHasta;
	}



	public void setFechaVigenciaHasta(Date fechaVigenciaHasta) {
		this.fechaVigenciaHasta = fechaVigenciaHasta;
	}



	public void guardarCambios(ActionEvent ae) {
		
		
		try {
			if(logoOnac.getImagen() == null) {
				utilErrores.addInfo("No se ha subido imagen para guardar");
				return;
			}
			logoOnac.setFechaVigenciaDesde(new Timestamp(fechaVigenciaDesde.getTime()));
			logoOnac.setFechaVigenciaHasta(new Timestamp(fechaVigenciaHasta.getTime()));
			logoOnacDAO.edit(logoOnac);
			logoOnacSubido = true;
			utilErrores.addInfo("Logo onac creado editado exitosamente ");
		}catch(Exception exc) {
			logger.log(Level.SEVERE, "Error guardando logo onac", exc);
			utilErrores.addError("Error guardando logo onac");
		}
	}

	public boolean isLogoOnacSubido() {
		return logoOnacSubido;
	}

	public void setLogoOnacSubido(boolean logoOnacSubido) {
		this.logoOnacSubido = logoOnacSubido;
	}
	
	
	
}
