package com.proambiente.webapp.controller;

import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.InformacionCda;
import com.proambiente.webapp.dao.InformacionCdaFacade;

@Named
@SessionScoped
public class InformacionCdaSessionBean {
	
	@Inject
	InformacionCdaFacade infoCdaDAO;

	
	public String getVersionSoftware() {
		
		InformacionCda infoCda = infoCdaDAO.find(1);
		
		return infoCda.getVersionSoftware(); 
	}
}
