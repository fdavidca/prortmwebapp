package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;

import com.proambiente.modelo.ClaseVehiculo;
import com.proambiente.modelo.Defecto;
import com.proambiente.modelo.Equipo;
import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.Inspeccion;
import com.proambiente.modelo.LineaVehiculo;
import com.proambiente.modelo.Marca;
import com.proambiente.modelo.Medida;
import com.proambiente.modelo.Propietario;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Servicio;
import com.proambiente.modelo.TipoCombustible;
import com.proambiente.modelo.Usuario;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.modelo.VerificacionLinealidad;
import com.proambiente.modelo.dto.ResultadoDieselDTO;
import com.proambiente.webapp.dao.InformacionCdaFacade;
import com.proambiente.webapp.dao.PermisibleFacade;
import com.proambiente.webapp.dao.UsuarioFacade;
import com.proambiente.webapp.service.CertificadoService;
import com.proambiente.webapp.service.InspeccionService;
import com.proambiente.webapp.service.PruebasService;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.service.VersionSoftwareService;
import com.proambiente.webapp.service.generadordto.GeneradorDTOResultados;
import com.proambiente.webapp.service.indra.UtilConvertirServicioSuperintendencia;
import com.proambiente.webapp.util.ConstantesMedidasKilometraje;
import com.proambiente.webapp.util.UtilErrores;
import com.proambiente.webapp.util.dto.InformeDieselCarRes0762;
import com.proambiente.webapp.util.dto.InformeDieselSDARes0762;
import com.proambiente.webapp.util.dto.ResultadoDieselDTOInformes;
import com.proambiente.webapp.util.dto.sda.CausaRechazoDieselCarCundinamarca;
import com.proambiente.webapp.util.informes.DeterminadorCausaRechazoDieselCar;
import com.proambiente.webapp.util.informes.medellin.GeneradorInformeDieselRes0762;
import com.proambiente.webapp.util.informes.medellin.InformeDieselRes0762;

/**
 * Clase que sirve de controlador para la vista de informes de pruebas diesel
 * 
 * @author FABIAN
 *
 */
@Named
@ViewScoped
public class InformePruebasDiesel implements Serializable {

	private static final Logger logger = Logger.getLogger(InformePruebasDiesel.class.getName());

	@Inject
	PruebasService pruebaService;

	@Inject
	InspeccionService inspeccionService;
	
	@Inject
	RevisionService revisionService;

	@Inject
	CertificadoService certificadoService;
	
	@Inject 
	UsuarioFacade usuarioDAO;

	@Inject
	PermisibleFacade permisibleService;

	private static final long serialVersionUID = 1L;
	private static final String SI = "SI";
	private static final String NO = "NO";

	@Inject
	private InformacionCdaFacade informacionCda;

	@Inject
	private VersionSoftwareService versionSoftwareService;

	@Inject
	private UtilErrores utilErrores;

	private InformacionCda infoCda;

	private Date fechaInicial, fechaFinal;

	private List<Prueba> pruebasDiesel;

	private List<InformeDieselSDARes0762> datos;

	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
	private SimpleDateFormat sdfPrueba = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");

	private String nombreSoftware;

	private String versionSoftware;

	private DecimalFormat formateadorSinCifras = new DecimalFormat("#");
	private DecimalFormat formateadorConUnaCifra = new DecimalFormat("#0.0");

	@PostConstruct
	public void init() {
		nombreSoftware = "PRORTM";
		versionSoftware = versionSoftwareService.obtenerVersionSoftware();
	}

	public void consultarPruebas(ActionEvent ae) {
		try {

			Calendar calendarFechaFinal = Calendar.getInstance();
			calendarFechaFinal.setTime(fechaFinal);
			calendarFechaFinal.add(Calendar.MONTH, -12);

			Calendar calendarFechaInicial = Calendar.getInstance();
			calendarFechaInicial.setTime(fechaInicial);
			if (calendarFechaFinal.compareTo(calendarFechaInicial) > 0) {
				utilErrores.addInfo("No se puede consultar mas de 12 meses de informacion");
				return;
			}
			logger.log(Level.INFO, "Diesel fecha inicial0 {0}, fecha final {1}",
					new Object[] { sdf.format(fechaInicial), sdf.format(fechaFinal) });
			pruebasDiesel = inspeccionService.pruebasDieselPorInspeccionPorFecha(fechaInicial, fechaFinal);
			if (pruebasDiesel.size() == 0) {
				utilErrores.addInfo("No hay pruebas Diesel");
				return;
			}
			logger.info("Numero Pruebas:" + pruebasDiesel.size());
			infoCda = informacionCda.find(1);
			sdf = new SimpleDateFormat("yyyy/MM/dd");

			datos = new ArrayList<>();

			GeneradorDTOResultados generadorResultados = new GeneradorDTOResultados();
			generadorResultados.setPuntoComoSeparadorDecimales();
			GeneradorInformeDieselRes0762 generadorInforme = new GeneradorInformeDieselRes0762();
			DeterminadorCausaRechazoDieselCar determinadorCausa = new DeterminadorCausaRechazoDieselCar(permisibleService);
			ResultadoDieselDTO resultadoDiesel = new ResultadoDieselDTO();
			ResultadoDieselDTOInformes resultadoDieselInformes = new ResultadoDieselDTOInformes();

			for (Prueba prueba : pruebasDiesel) {
				InformeDieselRes0762 informeDieselDTO = new InformeDieselRes0762();
				try {
					
					InformeDieselSDARes0762 informeDieselCarRes0762 = new InformeDieselSDARes0762();
					informeDieselCarRes0762.setNumeroCda( infoCda.getNumeroCda());
					generadorInforme.agregarInformacionCda(infoCda, informeDieselDTO);
					informeDieselCarRes0762.setCodigoCiudad(infoCda.getCodigoDivipo());
					generadorInforme.agregarInformacionAnalizador(informeDieselDTO, prueba.getAnalizador());
					generadorInforme.agregarInformacionSoftware(informeDieselDTO, nombreSoftware, versionSoftware);
					generadorInforme.agregarInformacionPrueba(informeDieselDTO, prueba);
					generarFechaInicioFinPrueba(informeDieselCarRes0762, prueba);
					informeDieselCarRes0762.setNumeroConsecutivoPrueba(String.valueOf(prueba.getPruebaId()));
					Propietario propietario = prueba.getRevision().getPropietario();
					generadorInforme.agregarInformacionPropietario( propietario,informeDieselDTO);
					agregarCodigoCiudadPropietario(propietario, informeDieselCarRes0762);
					Vehiculo vehiculo = prueba.getRevision().getVehiculo();
					
					agregarCodigoLinea(vehiculo,informeDieselCarRes0762);
					agregarCodigoClase(vehiculo,informeDieselCarRes0762);
					agregarCodigoCombustible(vehiculo,informeDieselCarRes0762);
					agregarCodigoServicio(vehiculo,informeDieselCarRes0762);
					agregarCodigoMarca(vehiculo,informeDieselCarRes0762);
					
					Integer potenciaInt = vehiculo.getPotencia();
					String potencia = potenciaInt != null ? String.valueOf(potenciaInt): "";
					informeDieselCarRes0762.setPotencia(potencia);
					generadorInforme.agregarInformacionVehiculo( vehiculo, informeDieselDTO);

					VerificacionLinealidad verificacionLinealidad = prueba.getVerificacionLinealidad();
					Usuario inspectorVerificacion = usuarioDAO.find(verificacionLinealidad.getUsuarioId());

					generadorInforme.ponerInformacionVerificacionLinealidad(informeDieselDTO,
							verificacionLinealidad,inspectorVerificacion);

					List<Medida> medidas = pruebaService.obtenerMedidas(prueba);
					List<Defecto> defectos = pruebaService.obtenerDefectos(prueba);

					
					resultadoDiesel = generadorResultados.generarResultadoDieselDTO(medidas,
							defectos);
					
					resultadoDieselInformes = generadorResultados
							.generarResultadoDieselDTOInformes(medidas, defectos);		
					informeDieselCarRes0762.setInestabilidadRpmCiclos(resultadoDieselInformes.getInestabilidadRevoluciones());
					generadorInforme.cargarInformacionResultado(informeDieselDTO, resultadoDiesel,
							resultadoDieselInformes);
					
					generadorInforme.cargarInformacionMedidasVehiculo(informeDieselDTO, medidas,vehiculo);
					
					generadorInforme.cargarInformacionMedidasDensidadHumo(informeDieselDTO,medidas);
					generadorInforme.cargarInformacionMedidasRpmRalenti(informeDieselDTO,medidas);
					
					Revision revision = prueba.getRevision();
					Date fechaRevision = revision.getFechaCreacionRevision();
					Integer jefeTecnicoId = revision.getUsuarioResponsableId();
					Usuario jefeTecnico = null;
					if (revision.getNumeroInspecciones().equals(2)) {

						List<Inspeccion> inspecciones = inspeccionService
								.obtenerInspeccionesPorRevisionId(revision.getRevisionId());
						if (inspecciones.size() > 1) {
							Inspeccion segunda = inspecciones.get(1);
							fechaRevision = segunda.getFechaInspeccionAnterior();
							jefeTecnico = segunda.getUsuarioResponsable();
						}

					} else {

						jefeTecnico = usuarioDAO.find(jefeTecnicoId);

					}

					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
					String fechaFur = sdf.format(fechaRevision);

					if (prueba.getRevision().getAprobada()) {
						String numeroRevisionTecnico = certificadoService
								.consultarUltimoConsecutivoCertificado(prueba.getRevision().getRevisionId());
						informeDieselDTO.setCertificadoEmitido(numeroRevisionTecnico);
					} else {
						informeDieselDTO.setCertificadoEmitido("");
					}
					
					generadorInforme.agregarInformacionRevision( informeDieselDTO,revision,fechaFur,jefeTecnico);

					cargarKilometraje(revision.getRevisionId(), informeDieselDTO);
					List<Equipo> equipos = pruebaService.obtenerEquipos(prueba);
					
					generadorInforme.agregarInformacionEquipos( equipos, informeDieselDTO);
					
					
					String resultado = prueba.isAprobada() ? "Aprobado" : "Rechazado";
					resultado = prueba.isAbortada() ? "Abortado" : resultado;
					informeDieselDTO.setResultadoFinalPruebaRealizada(resultado);
					
					
					
					
					informeDieselCarRes0762.setSubinformeDiesel(informeDieselDTO);
					
					if(prueba.isAbortada()) {
						informeDieselCarRes0762.setCausaRechazoDiesel("");
					}else {
						CausaRechazoDieselCarCundinamarca causaRechazo = determinadorCausa.determinarCausaRechazo(resultadoDiesel, resultadoDieselInformes, prueba, medidas, vehiculo);
						if(causaRechazo != null) {
							informeDieselCarRes0762.setCausaRechazoDiesel( String.valueOf( causaRechazo.getValue() )  );
						}
					}
					
					datos.add(informeDieselCarRes0762);

					
				} catch (Exception exc) {
					logger.log(Level.SEVERE, "Prueba inconsistente " + prueba.getPruebaId(), exc);
				}

			logger.info("Numero filas:" + datos.size());
			}

		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Error consultando luces", exc);
			utilErrores.addError("Error" + exc.getMessage());
		}
	}
		
		private void cargarKilometraje(Integer revisionId, InformeDieselRes0762 informe) {
			DecimalFormat df = new DecimalFormat("#");
			Prueba pruebaIv = revisionService.buscarPruebaPorTipoPorRevision(revisionId, 1);
			if (pruebaIv != null) {

				List<Medida> medidasIv = pruebaService.obtenerMedidas(pruebaIv);
				Optional<Medida> medidaKilometraje = medidasIv.stream().filter(
						m -> m.getTipoMedida().getTipoMedidaId().equals(ConstantesMedidasKilometraje.KILOMETRAJE_MEDIDA))
						.findAny();
				if (medidaKilometraje.isPresent()) {

					Double kilometraje = medidaKilometraje.get().getValor();
					if (kilometraje >= 0) {
						informe.setKilometraje(df.format(kilometraje));
					} else {
						informe.setKilometraje("NO FUNCIONAL");
					}

				} else {

					Prueba pruebaFrenos = revisionService.buscarPruebaPorTipoPorRevision(revisionId, 5);
					if (pruebaFrenos != null) {

						List<Medida> medidas = pruebaService.obtenerMedidas(pruebaIv);
						Optional<Medida> medidaKilom = medidas.stream().filter(m -> m.getTipoMedida().getTipoMedidaId()
								.equals(ConstantesMedidasKilometraje.KILOMETRAJE_MEDIDA)).findAny();
						if (medidaKilom.isPresent()) {

							Double kilometraje = medidaKilom.get().getValor();
							if (kilometraje >= 0) {
								informe.setKilometraje(df.format(kilometraje));
							} else {
								informe.setKilometraje("NO FUNCIONAL");
							}

						} else {

						}

					}

				}

			}

			
		}
		
		private void agregarCodigoLinea(Vehiculo vehiculo, InformeDieselSDARes0762 informe) {
			if(vehiculo.getLineaVehiculo() != null  ) {
				LineaVehiculo linea = vehiculo.getLineaVehiculo();
				if(linea.getCodigoLinea() != null ) {
					Integer codigoLinea = linea.getCodigoLinea();
					String codigoLineaStr = String.valueOf(codigoLinea);
					informe.setCodigoLineaVehiculo(codigoLineaStr);
				}
			}			
		}
		private void agregarCodigoClase(Vehiculo vehiculo, InformeDieselSDARes0762 informe) {
			if(vehiculo.getClaseVehiculo()!=null) {
				ClaseVehiculo clase = vehiculo.getClaseVehiculo();
				if(clase.getClaseVehiculoId() != null) {
					Integer claseId = clase.getClaseVehiculoId();
					String claseIdStr = String.valueOf(claseId);
					informe.setCodigoClaseVehiculo(claseIdStr);
				}
			}
			
		}
		private void agregarCodigoCombustible(Vehiculo vehiculo, InformeDieselSDARes0762 informe) {
			if(vehiculo.getTipoCombustible()!=null) {
				TipoCombustible tc = vehiculo.getTipoCombustible();
				if(tc.getCombustibleId() != null) {
					Integer combustibleId = tc.getCombustibleId();
					String combustibleStr = String.valueOf(combustibleId);
					informe.setCodigoCombustible(combustibleStr);
				}
			}
			
		}
		
		private void agregarCodigoMarca(Vehiculo vehiculo, InformeDieselSDARes0762 informe) {
			if(vehiculo.getMarca()!=null) {
				Marca marca = vehiculo.getMarca();
				if(marca.getMarcaId()!=null) {
					Integer marcaId = marca.getMarcaId();
					String marcaStr = String.valueOf(marcaId);
					informe.setCodigoMarca(marcaStr);
				}
			}
			
		}

		private void agregarCodigoServicio(Vehiculo vehiculo, InformeDieselSDARes0762 informe) {
			if(vehiculo.getServicio()!=null) {
				Servicio servicio = vehiculo.getServicio();
				if(servicio.getServicioId() != null) {
					Integer codigoServicio =
					UtilConvertirServicioSuperintendencia
					.convertirServicioAsuperintendencia(vehiculo.getServicio().getServicioId());
					String codigoServicioStr = String.valueOf(codigoServicio);
					informe.setCodigoServicio(codigoServicioStr);
				}
			}
			
		}

		
		
		
		private void generarFechaInicioFinPrueba(InformeDieselSDARes0762 informe, Prueba prueba) {
			
			SimpleDateFormat sdf = new SimpleDateFormat( "yyyy/MM/dd, HH:mm:ss");
			informe.setFechaInicioPrueba( sdf.format( prueba.getFechaInicio() ));
			informe.setFechaFinPrueba( sdf.format( prueba.getFechaFinalizacion() ));
			
		}
		
		private void agregarCodigoCiudadPropietario(Propietario propietario, InformeDieselSDARes0762 informe) {
			if(propietario.getCiudad() != null) {
				Integer ciudadId = propietario.getCiudad().getCiudadId();
				String codigoCiudad = ciudadId != null ? String.valueOf(ciudadId) : "error";
				informe.setCodigoCiudadPropietario(codigoCiudad);
			}
		}

	@PreDestroy
	public void limpiarListas() {
		if (pruebasDiesel != null) {
			pruebasDiesel.clear();
		}
		if (datos != null) {
			datos.clear();
		}
	}

	public Date getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public List<Prueba> getPruebasDiesel() {
		return pruebasDiesel;
	}

	public void setPruebasDiesel(List<Prueba> pruebasDiesel) {
		this.pruebasDiesel = pruebasDiesel;
	}

	public List<InformeDieselSDARes0762> getDatos() {
		return datos;
	}

	public void setDatos(List<InformeDieselSDARes0762> datos) {
		this.datos = datos;
	}

	

}
