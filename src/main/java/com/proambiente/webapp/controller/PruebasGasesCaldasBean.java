package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;

import com.proambiente.modelo.Analizador;
import com.proambiente.modelo.Defecto;
import com.proambiente.modelo.Equipo;
import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.Inspeccion;
import com.proambiente.modelo.Medida;
import com.proambiente.modelo.Pipeta;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.PruebaFuga;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Usuario;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.modelo.VerificacionGasolina;
import com.proambiente.modelo.VerificacionLinealidad;
import com.proambiente.modelo.dto.ResultadoDieselDTO;
import com.proambiente.modelo.dto.ResultadoOttoMotocicletasDTO;
import com.proambiente.modelo.dto.ResultadoOttoVehiculosDTO;
import com.proambiente.webapp.dao.EquipoFacade;
import com.proambiente.webapp.dao.InformacionCdaFacade;
import com.proambiente.webapp.dao.PermisibleFacade;
import com.proambiente.webapp.dao.PipetaFacade;
import com.proambiente.webapp.dao.UsuarioFacade;
import com.proambiente.webapp.service.CertificadoService;
import com.proambiente.webapp.service.InspeccionService;
import com.proambiente.webapp.service.PruebaFugaAprobadaService;
import com.proambiente.webapp.service.PruebasService;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.service.VersionSoftwareService;
import com.proambiente.webapp.service.generadordto.GeneradorDTOResultados;
import com.proambiente.webapp.service.generadordto.GeneradorDTOTolima;
import com.proambiente.webapp.util.ConstantesMedidasKilometraje;
import com.proambiente.webapp.util.LLenarInfoInformeCaldas;
import com.proambiente.webapp.util.UtilErrores;
import com.proambiente.webapp.util.dto.InformeCaldasDTO;
import com.proambiente.webapp.util.dto.InformeDieselCarRes0762;
import com.proambiente.webapp.util.dto.InformeGasolinaMotocicletasCarDTO;
import com.proambiente.webapp.util.dto.ResultadoDieselDTOInformes;
import com.proambiente.webapp.util.dto.ResultadoOttoInformeDTO;
import com.proambiente.webapp.util.dto.ResultadoSonometriaInformeDTO;
import com.proambiente.webapp.util.dto.sda.InformeMotocicletaDTO;
import com.proambiente.webapp.util.informes.GeneradorDTOInformes;
import com.proambiente.webapp.util.informes.medellin.GeneradorInformeDieselRes0762;
import com.proambiente.webapp.util.informes.medellin.GeneradorInformeLivianosRes0762;
import com.proambiente.webapp.util.informes.medellin.GeneradorInformeMotoRes0762;
import com.proambiente.webapp.util.informes.medellin.InformeDieselRes0762;
import com.proambiente.webapp.util.informes.medellin.InformeLivianosRes0762;
import com.proambiente.webapp.util.informes.medellin.InformeMotocicletasRes0762;


@Named
@ViewScoped
public class PruebasGasesCaldasBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String SI = "SI";
	private static final String NO = "NO";

	private static final Logger logger = Logger.getLogger(PruebasGasesCaldasBean.class.getName());

	@Inject
	PruebasService pruebaService;

	@Inject
	InspeccionService inspeccionService;

	@Inject
	CertificadoService certificadoService;

	@Inject
	PermisibleFacade permisibleFacade;

	private Date fechaInicial, fechaFinal;

	@Inject
	private InformacionCdaFacade informacionCda;

	@Inject
	private VersionSoftwareService versionSoftwareService;

	@Inject
	private UtilErrores utilErrores;

	@Resource
	private ManagedExecutorService managedExecutorService;

	private InformacionCda infoCda;

	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

	private SimpleDateFormat sdfPrueba = new SimpleDateFormat("yyyy/MM/dd,HH:mm:ss");

	private String versionSoftware;

	@Inject
	UsuarioFacade usuarioFacade;

	@Inject
	PipetaFacade pipetaFacade;

	@Inject
	RevisionService revisionService;



	@Inject
	PruebaFugaAprobadaService pruebaFugaAprobadaService;
	
	@Inject 
	UsuarioFacade usuarioDAO;
	
	@Inject
	private EquipoFacade equipoFacade;

	List<InformeCaldasDTO> datos = new ArrayList<>();

	private DecimalFormat df;
	
	private enum SeparadorDECIMAL {
		COMA, PUNTO
	};

	private SeparadorDECIMAL separador = SeparadorDECIMAL.PUNTO;

	public void establecerPuntoComoSeparadorDecimal() {
		separador = SeparadorDECIMAL.PUNTO;
	};

	public void establecerComaComoSeparadorDecimal() {
		separador = SeparadorDECIMAL.COMA;
	};

	public void consultarPruebas(ActionEvent ae) {

		Map<String, String> temperaturaPorCodigo = new HashMap<>();
		temperaturaPorCodigo.put("1", "Aceite");
		temperaturaPorCodigo.put("2", "Bloque");
		temperaturaPorCodigo.put("3", "Tiempo");
		establecerComaComoSeparadorDecimal();
		try {

			List<Prueba> pruebasMotocicleta = inspeccionService.consultarPruebasOttoMotocicletasPaginado(fechaInicial,
					fechaFinal, 0, 10000);

			long numeroPruebas = inspeccionService.contarPruebasOttoMotocicletasInspeccion(fechaInicial, fechaFinal);

			logger.info("Numero Pruebas Motocicleta Totales:" + numeroPruebas);
			infoCda = informacionCda.find(1);

			versionSoftware = versionSoftwareService.obtenerVersionSoftware();

			String nombreSoftware = "PRORTM";

			if (datos != null)
				datos.clear();
			datos = new ArrayList<>();
			GeneradorDTOResultados generadorResultados = new GeneradorDTOResultados();
			
			
			GeneradorInformeMotoRes0762 generadorResultadosExtra = new GeneradorInformeMotoRes0762();
			generadorResultadosExtra.establecerComaComoSeparadorDecimal();
             
			for (Prueba prueba : pruebasMotocicleta) {
				
				LocalDateTime fechaInicioPrueba = prueba.getFechaInicio().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
				LocalDateTime fechaInicioConsulta = fechaInicial.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
				LocalDateTime fechaFinConsulta = fechaFinal.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
				if(fechaInicioPrueba.isBefore(fechaInicioConsulta)) {
					DateTimeFormatter df =DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
					logger.log(Level.INFO, "Prueba : " + prueba.getPruebaId() + " es anterior a la fecha inicial de la consulta :" + df.format(fechaInicioPrueba) );
					continue;
				}
				if(fechaInicioPrueba.isAfter(fechaFinConsulta)) {
					DateTimeFormatter df =DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
					logger.log(Level.INFO, "Prueba : " + prueba.getPruebaId() + " es posterior a la fecha final de la consulta :" + df.format(fechaInicioPrueba) );
					continue;
				}

				logger.log(Level.INFO, "Procesando prueba : " + prueba.getPruebaId());
				String consecutivoCertificado = "";
				try {
					consecutivoCertificado = certificadoService
							.consultarUltimoConsecutivoCertificado(prueba.getRevision().getRevisionId());
				} catch (Exception exc) {
					logger.log(Level.SEVERE, "Error cargando certificado" + prueba.getRevision().getRevisionId());
				}

				InformeMotocicletaDTO informe = new InformeMotocicletaDTO();
				InformeMotocicletasRes0762 subInforme = new InformeMotocicletasRes0762();
				subInforme.setNumeroConsecutivoRunt(consecutivoCertificado);
				Revision revision = prueba.getRevision();
				if(revision != null) {
					String numeroFur = String.valueOf(revision.getRevisionId()) +" - " + String.valueOf(revision.getNumeroInspecciones());
					subInforme.setNumeroFUR(numeroFur);
				}
				informe.setSubInforme(subInforme);
				
				Usuario usuario = prueba.getUsuario();
				if(usuario != null && usuario.getCedula() != null) {
					
					subInforme.setNumeroIdentificacionInspectorPrueba(usuario.getCedula().toString());
				}

				GeneradorDTOInformes.ponerInformacionCda(informe, infoCda);
				

				GeneradorDTOInformes.ponerInformacionPrueba(informe, prueba);
				GeneradorDTOInformes.ponerInformacionSoftware(informe, nombreSoftware, versionSoftware);
				GeneradorDTOInformes.ponerInformacionCertificado(informe, consecutivoCertificado);

				

				informe.setCiudad(infoCda.getCiudad());

				
				Vehiculo vehiculo = prueba.getRevision().getVehiculo();
				if (vehiculo != null) {
					GeneradorDTOTolima.ponerInformacionVehiculo(informe, vehiculo);
				}

				cargarInformacionAnalizador(prueba, informe);
				cargarInformacionVerificacion(prueba, informe, sdfPrueba);

				List<Medida> medidas = pruebaService.obtenerMedidas(prueba);
				List<Defecto> defectos = pruebaService.obtenerDefectos(prueba);
				if(revision != null && revision.getRevisionId() != null) {
					cargarKilometrajeMotos(revision.getRevisionId(), informe);
				}

				Boolean dosTiempos = prueba.getRevision().getVehiculo().getTiemposMotor().equals(2);

				ResultadoOttoMotocicletasDTO resultado = generadorResultados.generarResultadoOttoMotos(medidas,
						defectos, dosTiempos);
				
				generadorResultadosExtra.cargarInformacionMedidasVehiculo(subInforme, medidas);

				cargarResultado(informe, resultado);

				ResultadoOttoInformeDTO r2 = generadorResultados.generarResultadoOttoInforme(medidas, defectos);

				cargarFaltante(informe, r2);

				String resultadoFinal = prueba.isAprobada() ? "APROBADO" : "NO APROBADO";
				resultadoFinal = prueba.isAbortada() ? "ABORTADO" : resultadoFinal;
				
				informe.setConceptoFinalDelVehiculo(resultadoFinal);
				logger.log(Level.INFO, "Terminada la prueba : " + prueba.getPruebaId());
				// datos.add(informe);
				
				ResultadoSonometriaInformeDTO resultadoSonometria = new ResultadoSonometriaInformeDTO();
				List<Prueba> pruebas = pruebaService.pruebasRevision(revision.getRevisionId());
				Optional<Prueba> pruebaSonometriaOpt = pruebas.stream()
						.filter(p -> p.getTipoPrueba().getTipoPruebaId().equals(7)).findFirst();
				if (pruebaSonometriaOpt.isPresent()) {
					Prueba pruebaSonometria = pruebaSonometriaOpt.get();
					List<Medida> medidasSonometria = pruebaService.obtenerMedidas(pruebaSonometria);
					List<Equipo> equipos1 = equipoFacade.consultarEquiposPorPrueba(pruebaSonometria.getPruebaId());
					 resultadoSonometria = generadorResultados
							.generarInformacionSonometria(medidasSonometria, equipos1);
					
				} 
				
				
				
				InformeCaldasDTO informeCaldas = new InformeCaldasDTO();
				LLenarInfoInformeCaldas.llenarInfoCda(informeCaldas,infoCda);
				LLenarInfoInformeCaldas.llenarInformacionMotos(informeCaldas, informe,resultadoSonometria);
				datos.add(informeCaldas);
			}
			
			if(pruebasMotocicleta != null) {
				pruebasMotocicleta.clear();
			}

			List<Prueba> pruebasOtto = inspeccionService.pruebasOttoDeInspeccionPorFechaPaginado(fechaInicial,
					fechaFinal, 0, 10000);

			numeroPruebas = inspeccionService.contarPruebasOttoInspeccion(fechaInicial, fechaFinal);

			logger.info("Numero Pruebas Totales Otto:" + numeroPruebas);




			GeneradorInformeLivianosRes0762 generadorInforme = new GeneradorInformeLivianosRes0762();

			for (Prueba prueba : pruebasOtto) {

				logger.log(Level.INFO, "Procesando prueba : " + prueba.getPruebaId());
				String consecutivoCertificado = "";
				try {
					consecutivoCertificado = certificadoService
							.consultarUltimoConsecutivoCertificado(prueba.getRevision().getRevisionId());
				} catch (Exception exc) {
					logger.log(Level.SEVERE, "Error cargando certificado" + prueba.getRevision().getRevisionId());
				}

				InformeLivianosRes0762 informe = new InformeLivianosRes0762();

				generadorInforme.agregarInformacionCda(infoCda, informe);
				generadorInforme.agregarInformacionPrueba(informe, prueba);
				generadorInforme.agregarInformacionSoftware(informe, nombreSoftware, versionSoftware);
				generadorInforme.agregarInformacionCertificado(informe, consecutivoCertificado);

				
				Vehiculo vehiculo = prueba.getRevision().getVehiculo();
				if (vehiculo != null) {
					generadorInforme.agregarInformacionVehiculo(vehiculo, informe);
				}

				generadorInforme.agregarInformacionAnalizador(prueba.getAnalizador(), informe);
				Double pef = prueba.getAnalizador().getPef();

				VerificacionGasolina verificacion = prueba.getVerificacionGasolina();

				if (verificacion.getSpanAltoPipetaId() != null && verificacion.getSpanBajoPipetaId() != null) {

					Pipeta pipetaAlta = pipetaFacade.find(verificacion.getSpanAltoPipetaId());
					Pipeta pipetaBaja = pipetaFacade.find(verificacion.getSpanBajoPipetaId());
					generadorInforme.agregarInformacionPipetas(informe, pipetaBaja, pipetaAlta, pef);

				}

				Usuario usuario = usuarioFacade.find(verificacion.getUsuarioId());

				generadorInforme.agregarResultadoVerificacion(informe, verificacion, usuario, pef);

				String fechaFugas = cargarPruebaFugas(prueba.getFechaInicio(), prueba.getAnalizador().getSerial());
				informe.setFechaHoraPruebaFugas(fechaFugas);

				Revision revision = prueba.getRevision();
				Date fechaRevision = revision.getFechaCreacionRevision();
				Integer jefeTecnicoId = revision.getUsuarioResponsableId();
				Usuario jefeTecnico = null;
				if (revision.getNumeroInspecciones().equals(2)) {

					List<Inspeccion> inspecciones = inspeccionService
							.obtenerInspeccionesPorRevisionId(revision.getRevisionId());
					if (inspecciones.size() > 1) {
						Inspeccion segunda = inspecciones.get(1);
						fechaRevision = segunda.getFechaInspeccionAnterior();
						jefeTecnico = segunda.getUsuarioResponsable();
					}

				} else {

					jefeTecnico = usuarioFacade.find(jefeTecnicoId);

				}

				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				String fechaFur = sdf.format(fechaRevision);

				generadorInforme.agregarInformacionRevision(informe, prueba.getRevision(), fechaFur, jefeTecnico);

				List<Medida> medidas = pruebaService.obtenerMedidas(prueba);

				List<Defecto> defectos = pruebaService.obtenerDefectos(prueba);

				cargarKilometraje(revision.getRevisionId(), informe);
				List<Equipo> equipos = pruebaService.obtenerEquipos(prueba);

				generadorInforme.agregarInformacionEquipos(equipos, informe);

				ResultadoOttoVehiculosDTO resultado = generadorResultados.generarResultadoOtto(medidas, defectos);

				generadorInforme.cargarInformacionResultado(informe, resultado);
				// estas no son medidas, deberian serlo por el momento se deja así
				if (prueba.isDilucion()) {
					informe.setPresenciaDilucion("SI");
				} else {
					informe.setPresenciaDilucion("NO");
				}
				informe.setLugarTomaTemperaturaVehiculo(temperaturaPorCodigo.get(prueba.getMetodoTemperatura()));

				ResultadoOttoInformeDTO r2 = generadorResultados.generarResultadoOttoInforme(medidas, defectos);

				generadorInforme.cargarInformacionResultadoAdicional(informe, r2);

				generadorInforme.cargarInformacionMedidasVehiculo(informe, medidas);

				String resultadoFinal = prueba.isAprobada() ? "APROBADO" : "REPROBADO";
				resultadoFinal = prueba.isAbortada() ? "ABORTADO" : resultadoFinal;

				informe.setResultadoFinalPrueba(resultadoFinal);
				
				ResultadoSonometriaInformeDTO resultadoSonometria = new ResultadoSonometriaInformeDTO();
				List<Prueba> pruebas = pruebaService.pruebasRevision(revision.getRevisionId());
				Optional<Prueba> pruebaSonometriaOpt = pruebas.stream()
						.filter(p -> p.getTipoPrueba().getTipoPruebaId().equals(7)).findFirst();
				if (pruebaSonometriaOpt.isPresent()) {
					Prueba pruebaSonometria = pruebaSonometriaOpt.get();
					List<Medida> medidasSonometria = pruebaService.obtenerMedidas(pruebaSonometria);
					List<Equipo> equipos1 = equipoFacade.consultarEquiposPorPrueba(pruebaSonometria.getPruebaId());
					 resultadoSonometria = generadorResultados
							.generarInformacionSonometria(medidasSonometria, equipos1);
					
				} 
					
				
				logger.log(Level.INFO, "Terminada la prueba OTTO : " + prueba.getPruebaId());
				
				InformeCaldasDTO informeCaldas = new InformeCaldasDTO();
				LLenarInfoInformeCaldas.llenarInfoCda(informeCaldas,infoCda);
				LLenarInfoInformeCaldas.llenarInfoOtto(informeCaldas,informe,resultadoSonometria);
				datos.add(informeCaldas);
			}
			if(pruebasOtto != null) {
				pruebasOtto.clear();
			}

			List<Prueba> pruebasDiesel = inspeccionService.pruebasDieselPorInspeccionPorFecha(fechaInicial, fechaFinal);

			logger.info("Numero Pruebas:" + pruebasDiesel.size());
			infoCda = informacionCda.find(1);
			sdf = new SimpleDateFormat("yyyy/MM/dd");

			List<InformeDieselCarRes0762> listaResultados = new ArrayList<>();
			

			//generadorResultados.setComaComoSeparadorDecimales();
			GeneradorInformeDieselRes0762 generadorInformeDiesel = new GeneradorInformeDieselRes0762();
			//coma como separador decimal

			ResultadoDieselDTO resultadoDiesel = new ResultadoDieselDTO();
			ResultadoDieselDTOInformes resultadoDieselInformes = new ResultadoDieselDTOInformes();
			for (Prueba prueba : pruebasDiesel) {
				InformeDieselRes0762 informeDieselDTO = new InformeDieselRes0762();
				try {

					InformeDieselCarRes0762 informeDieselCarRes0762 = new InformeDieselCarRes0762();
					informeDieselCarRes0762.setNumeroCda(infoCda.getNumeroCda());

					informeDieselCarRes0762.setCodigoCiudad(infoCda.getCodigoDivipo());

					generarFechaInicioFinPrueba(informeDieselCarRes0762, prueba);
					informeDieselCarRes0762.setNumeroConsecutivoPrueba(String.valueOf(prueba.getPruebaId()));
					

					Vehiculo vehiculo = prueba.getRevision().getVehiculo();
					Integer potenciaInt = vehiculo.getPotencia();
					String potencia = potenciaInt != null ? String.valueOf(potenciaInt) : "";
					informeDieselCarRes0762.setPotencia(potencia);
					generadorInformeDiesel.agregarInformacionVehiculo(vehiculo, informeDieselDTO);
					Analizador analizador = prueba.getAnalizador();
					generadorInformeDiesel.agregarInformacionAnalizador(informeDieselDTO, analizador);

					VerificacionLinealidad verificacionLinealidad = prueba.getVerificacionLinealidad();
					Usuario inspectorVerificacion = usuarioDAO.find(verificacionLinealidad.getUsuarioId());

					generadorInformeDiesel.ponerInformacionVerificacionLinealidad(informeDieselDTO,
							verificacionLinealidad, inspectorVerificacion);

					List<Medida> medidas = pruebaService.obtenerMedidas(prueba);
					List<Defecto> defectos = pruebaService.obtenerDefectos(prueba);

					resultadoDiesel = generadorResultados.generarResultadoDieselDTO(medidas, defectos);

					resultadoDieselInformes = generadorResultados.generarResultadoDieselDTOInformes(medidas, defectos);
					informeDieselCarRes0762
							.setInestabilidadRpmCiclos(resultadoDieselInformes.getInestabilidadRevoluciones());
					generadorInformeDiesel.cargarInformacionResultado(informeDieselDTO, resultadoDiesel,
							resultadoDieselInformes);

					generadorInformeDiesel.cargarInformacionMedidasVehiculo(informeDieselDTO, medidas, vehiculo);

					generadorInformeDiesel.cargarInformacionMedidasDensidadHumo(informeDieselDTO, medidas);
					generadorInformeDiesel.cargarInformacionMedidasRpmRalenti(informeDieselDTO, medidas);

					Revision revision = prueba.getRevision();
					Date fechaRevision = revision.getFechaCreacionRevision();
					Integer jefeTecnicoId = revision.getUsuarioResponsableId();
					Usuario jefeTecnico = null;
					if (revision.getNumeroInspecciones().equals(2)) {

						List<Inspeccion> inspecciones = inspeccionService
								.obtenerInspeccionesPorRevisionId(revision.getRevisionId());
						if (inspecciones.size() > 1) {
							Inspeccion segunda = inspecciones.get(1);
							fechaRevision = segunda.getFechaInspeccionAnterior();
							jefeTecnico = segunda.getUsuarioResponsable();
						}

					} else {

						jefeTecnico = usuarioDAO.find(jefeTecnicoId);

					}

					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
					String fechaFur = sdf.format(fechaRevision);

					if (prueba.getRevision().getAprobada()) {
						String numeroRevisionTecnico = certificadoService
								.consultarUltimoConsecutivoCertificado(prueba.getRevision().getRevisionId());
						informeDieselDTO.setCertificadoEmitido(numeroRevisionTecnico);
					} else {
						informeDieselDTO.setCertificadoEmitido("");
					}

					cargarKilometrajeDiesel(revision.getRevisionId(),informeDieselDTO);
					
					generadorInformeDiesel.agregarInformacionRevision(informeDieselDTO, revision, fechaFur,
							jefeTecnico);

					
					

					

					String resultado = prueba.isAprobada() ? "APROBADO" : "RECHAZADO";
					resultado = prueba.isAbortada() ? "Abortado" : resultado;
					informeDieselDTO.setResultadoFinalPruebaRealizada(resultado);
					

					Integer revisionId = prueba.getRevision().getRevisionId();

					List<Prueba> pruebas = pruebaService.pruebasRevision(revisionId);
					Optional<Prueba> pruebaSonometriaOpt = pruebas.stream()
							.filter(p -> p.getTipoPrueba().getTipoPruebaId().equals(7)).findFirst();
					if (pruebaSonometriaOpt.isPresent()) {
						Prueba pruebaSonometria = pruebaSonometriaOpt.get();
						List<Medida> medidasSonometria = pruebaService.obtenerMedidas(pruebaSonometria);
						List<Equipo> equipos1 = equipoFacade.consultarEquiposPorPrueba(pruebaSonometria.getPruebaId());
						ResultadoSonometriaInformeDTO resultadoSonometria = generadorResultados
								.generarInformacionSonometria(medidasSonometria, equipos1);
						informeDieselCarRes0762.setResultadoSonometria(resultadoSonometria);
					} else {
						ResultadoSonometriaInformeDTO resultadoSonometria = new ResultadoSonometriaInformeDTO();
						resultadoSonometria.setMarcaMedidor("");
						resultadoSonometria.setFechaUltimaCalibracion("");
						resultadoSonometria.setSerialMedidor("");
						resultadoSonometria.setValorMedicion("");
					}
					informeDieselCarRes0762.setSubinformeDiesel(informeDieselDTO);

					

					listaResultados.add(informeDieselCarRes0762);
					InformeCaldasDTO informeCaldas = new InformeCaldasDTO();
					
					LLenarInfoInformeCaldas.llenarInfoCda(informeCaldas , infoCda);
					LLenarInfoInformeCaldas.llenarInfoDiesel(informeCaldas,informeDieselCarRes0762);
					
					datos.add(informeCaldas);
				} catch (Exception exc) {
					logger.log(Level.SEVERE, "Prueba inconsistente " + prueba.getPruebaId(), exc);
				}

			}
			listaResultados.clear();

		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Error cargando prueba metodo dos", exc);

		}

	}

	private String cargarPruebaFugas(Date fechaPrueba, String serial) {
		LocalDate fecha = Instant.ofEpochMilli(fechaPrueba.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
		LocalDateTime startOfDay = fecha.atStartOfDay();
		LocalDateTime endOfDay = LocalTime.MAX.atDate(fecha);

		Instant instant = startOfDay.atZone(ZoneId.systemDefault()).toInstant();
		Date dateInicio = Date.from(instant);

		Instant instantEnd = endOfDay.atZone(ZoneId.systemDefault()).toInstant();
		Date dateFin = Date.from(instantEnd);
		PruebaFuga pf = pruebaFugaAprobadaService.consultarPruebaFugaPorFecha(dateInicio, dateFin, serial);
		if (pf != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			return sdf.format(pf.getFechaRealizacion());
		} else {
			return "";
		}

	}

	public Date getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	private void cargarResultado(InformeGasolinaMotocicletasCarDTO informe, ResultadoOttoMotocicletasDTO resultado) {

		informe.setTemperaturaDeMotor(resultado.getTempRalenti().equals("0") ? "NA" : resultado.getTempRalenti());
		informe.setRpmRalenti(resultado.getRpmRalenti());
		informe.setHcRalenti(resultado.getHcRalenti());
		informe.setCoRalenti(resultado.getCoRalenti());
		informe.setCo2Ralenti(resultado.getCo2Ralenti());
		informe.setO2Ralenti(resultado.getO2Ralenti());

		informe.setRevolucionesFueraRango(resultado.getRevolucionesFueraRango().equalsIgnoreCase("TRUE") ? "SI" : "NO");
		informe.setFugasTuboEscape(resultado.getFugasTuboEscape().equalsIgnoreCase("TRUE") ? "SI" : "NO");
		informe.setFugasSilenciador(resultado.getFugasSilenciador().equalsIgnoreCase("TRUE") ? "SI" : "NO");
		informe.setFugasTapaCombustible(resultado.getTapaCombustible().equalsIgnoreCase("TRUE") ? "SI" : "NO");
		informe.setFugasTapaAceite(resultado.getTapaAceite().equalsIgnoreCase("TRUE") ? "SI" : "NO");
		informe.setSalidasAdicionalesDiseno(resultado.getSalidasAdicionales().equalsIgnoreCase("TRUE") ? "SI" : "NO");
		informe.setPresenciaHumoNegroAzul(resultado.getPresenciaHumos().equalsIgnoreCase("TRUE") ? "SI" : "NO");
		informe.setAccesoriosDeformacionesImpiden(NO);

	}

	private void cargarFaltante(InformeGasolinaMotocicletasCarDTO informe, ResultadoOttoInformeDTO r2) {
		informe.setTemperaturaAmbiente(r2.getTemperaturaAmbiente());
		informe.setHumedadRelativa(r2.getHumedadRelativa());
		informe.setIncumplimientoNivelesEmision(r2.getIncumplimientoNivelesDeEmision().equalsIgnoreCase("TRUE") ? "SI" : "NO");
	}

	private void cargarInformacionAnalizador(Prueba prueba, InformeGasolinaMotocicletasCarDTO informe) {
		Analizador analizador = pruebaService.consultarAnalizadorPrueba(prueba);
		DecimalFormat df = new DecimalFormat("0.000");
		estableceSimboloDecimal(df);
		if (analizador != null) {
			informe.setVlrPEf(df.format(analizador.getPef()));
			informe.setNumeroSerieBanco(analizador.getSerial());
			informe.setNumeroSerieAnalizador(analizador.getSerialAnalizador());
			informe.setMarcaAnalizador(analizador.getMarca());

		} else {
			informe.setVlrPEf("No config");
			informe.setNumeroSerieBanco("No config");
			informe.setNumeroSerieAnalizador("No config");
			informe.setMarcaAnalizador("No config");
		}

	}

	private static final char DECIMAL_SEPARATOR_PUNTO = '.';

	private static final char DECIMAL_SEPARATOR_COMA = ',';

	public void estableceSimboloDecimal(DecimalFormat df) {
		DecimalFormatSymbols ds = DecimalFormatSymbols.getInstance();
		if(separador.equals(SeparadorDECIMAL.PUNTO)) {
			ds.setDecimalSeparator(DECIMAL_SEPARATOR_PUNTO);
		}else {
			ds.setDecimalSeparator(DECIMAL_SEPARATOR_COMA);
		}
		df.setDecimalFormatSymbols(ds);
	}

	private void cargarInformacionVerificacion(Prueba prueba, InformeMotocicletaDTO informe, SimpleDateFormat sdf) {
		VerificacionGasolina verif = pruebaService.consultarVerificacionPrueba(prueba);
		
		if (verif != null) {

			df = new DecimalFormat("#");
			String refBajaHC = verif.getValorGasRefBajaHC() != null ? df.format(verif.getValorGasRefBajaHC()) : "";
			informe.setVlrSpanBajoHC(refBajaHC);

			String resultadoValorBajaHc = verif.getValorBajoHc() != null ? df.format(verif.getValorBajoHc()) : "";
			informe.setResultadoValorSpanBajoHC(resultadoValorBajaHc);

			df = new DecimalFormat("0.00");
			estableceSimboloDecimal(df);
			String refBajaCO = verif.getValorGasRefBajaCO() != null ? df.format(verif.getValorGasRefBajaCO()) : "";
			informe.setVlrSpanBajoCO(refBajaCO);

			String valorBajaCO = verif.getValorBajoCo() != null ? df.format(verif.getValorBajoCo()) : "";
			informe.setResultadoValorSpanBajoCO(valorBajaCO);

			df = new DecimalFormat("0.0");
			estableceSimboloDecimal(df);
			String referenciaBajaCO2 = verif.getValorGasRefBajaCO2() != null ? df.format(verif.getValorGasRefBajaCO2())
					: "";
			informe.setVlrSpanBajoCO2(referenciaBajaCO2);

			String valorBajaCO2 = verif.getValorBajoCo2() != null ? df.format(verif.getValorBajoCo2()) : "";
			informe.setResultadoValorSpanBajoCO2(valorBajaCO2);

			df = new DecimalFormat("#");
			String refAltaHC = verif.getValorGasRefAltaHC() != null ? df.format(verif.getValorGasRefAltaHC()) : "";
			informe.setVlrSpanAltoHC(refAltaHC);

			String valorAltaHC = verif.getValorAltoHc() != null ? df.format(verif.getValorAltoHc()) : "";
			informe.setResultadoValorSpanAltoHC(valorAltaHC);

			df = new DecimalFormat("0.00");
			estableceSimboloDecimal(df);
			String refAltaCO = verif.getValorGasRefAltaCO() != null ? df.format(verif.getValorGasRefAltaCO()) : "";
			informe.setVlrSpanAltoCO(refAltaCO);

			String valorAltoCO = verif.getValorAltoCo() != null ? df.format(verif.getValorAltoCo()) : "";
			informe.setResultadoValorSpanAltoCO(valorAltoCO);

			df = new DecimalFormat("0.0");
			estableceSimboloDecimal(df);
			String refAltaCO2 = verif.getValorGasRefAltaCO2() != null ? df.format(verif.getValorGasRefAltaCO2()) : "";
			informe.setVlrSpanAltoCO2(refAltaCO2);

			String valorAltaCO2 = verif.getValorAltoCo2() != null ? df.format(verif.getValorAltoCo2()) : "";
			informe.setResultadoValorSpanAltoCO2(valorAltaCO2);

			String fechaVerif = verif.getFechaVerificacion() != null ? sdf.format(verif.getFechaVerificacion()) : "";
			informe.setFechaHoraUltimaCalibracion(fechaVerif);
		} else {
			informe.setVlrSpanBajoHC("");
			informe.setVlrSpanBajoCO("");
			informe.setVlrSpanBajoCO2("");

			informe.setVlrSpanAltoHC("");
			informe.setVlrSpanAltoCO("");
			informe.setVlrSpanAltoCO2("");
			informe.setFechaHoraUltimaCalibracion("");

		}

	}
	
	private void cargarKilometrajeMotos(Integer revisionId, InformeGasolinaMotocicletasCarDTO informe) {

		DecimalFormat df = new DecimalFormat("#");
		Prueba pruebaIv = revisionService.buscarPruebaPorTipoPorRevision(revisionId, 1);
		if (pruebaIv != null) {

			List<Medida> medidasIv = pruebaService.obtenerMedidas(pruebaIv);
			Optional<Medida> medidaKilometraje = medidasIv.stream().filter(
					m -> m.getTipoMedida().getTipoMedidaId().equals(ConstantesMedidasKilometraje.KILOMETRAJE_MEDIDA))
					.findAny();
			if (medidaKilometraje.isPresent()) {

				Double kilometraje = medidaKilometraje.get().getValor();
				if (kilometraje >= 0) {
					informe.setKilometraje(df.format(kilometraje));
				} else {
					informe.setKilometraje("NO FUNCIONAL");
				}

			} else {

				Prueba pruebaFrenos = revisionService.buscarPruebaPorTipoPorRevision(revisionId, 5);
				if (pruebaFrenos != null) {

					List<Medida> medidas = pruebaService.obtenerMedidas(pruebaIv);
					Optional<Medida> medidaKilom = medidas.stream().filter(m -> m.getTipoMedida().getTipoMedidaId()
							.equals(ConstantesMedidasKilometraje.KILOMETRAJE_MEDIDA)).findAny();
					if (medidaKilom.isPresent()) {

						Double kilometraje = medidaKilom.get().getValor();
						if (kilometraje >= 0) {
							informe.setKilometraje(df.format(kilometraje));
						} else {
							informe.setKilometraje("NO FUNCIONAL");
						}

					} else {

					}

				}

			}

		}

	}

	private void cargarKilometraje(Integer revisionId, InformeLivianosRes0762 informe) {

		DecimalFormat df = new DecimalFormat("#");
		Prueba pruebaIv = revisionService.buscarPruebaPorTipoPorRevision(revisionId, 1);
		if (pruebaIv != null) {

			List<Medida> medidasIv = pruebaService.obtenerMedidas(pruebaIv);
			Optional<Medida> medidaKilometraje = medidasIv.stream().filter(
					m -> m.getTipoMedida().getTipoMedidaId().equals(ConstantesMedidasKilometraje.KILOMETRAJE_MEDIDA))
					.findAny();
			if (medidaKilometraje.isPresent()) {

				Double kilometraje = medidaKilometraje.get().getValor();
				if (kilometraje >= 0) {
					informe.setKilometraje(df.format(kilometraje));
				} else {
					informe.setKilometraje("NO FUNCIONAL");
				}

			} else {

				Prueba pruebaFrenos = revisionService.buscarPruebaPorTipoPorRevision(revisionId, 5);
				if (pruebaFrenos != null) {

					List<Medida> medidas = pruebaService.obtenerMedidas(pruebaIv);
					Optional<Medida> medidaKilom = medidas.stream().filter(m -> m.getTipoMedida().getTipoMedidaId()
							.equals(ConstantesMedidasKilometraje.KILOMETRAJE_MEDIDA)).findAny();
					if (medidaKilom.isPresent()) {

						Double kilometraje = medidaKilom.get().getValor();
						if (kilometraje >= 0) {
							informe.setKilometraje(df.format(kilometraje));
						} else {
							informe.setKilometraje("NO FUNCIONAL");
						}

					} else {

					}

				}

			}

		}

	}
	
	private void cargarKilometrajeDiesel(Integer revisionId, InformeDieselRes0762 informe) {

		DecimalFormat df = new DecimalFormat("#");
		Prueba pruebaIv = revisionService.buscarPruebaPorTipoPorRevision(revisionId, 1);
		if (pruebaIv != null) {

			List<Medida> medidasIv = pruebaService.obtenerMedidas(pruebaIv);
			Optional<Medida> medidaKilometraje = medidasIv.stream().filter(
					m -> m.getTipoMedida().getTipoMedidaId().equals(ConstantesMedidasKilometraje.KILOMETRAJE_MEDIDA))
					.findAny();
			if (medidaKilometraje.isPresent()) {

				Double kilometraje = medidaKilometraje.get().getValor();
				if (kilometraje >= 0) {
					informe.setKilometraje(df.format(kilometraje));
				} else {
					informe.setKilometraje("NO FUNCIONAL");
				}

			} else {

				Prueba pruebaFrenos = revisionService.buscarPruebaPorTipoPorRevision(revisionId, 5);
				if (pruebaFrenos != null) {

					List<Medida> medidas = pruebaService.obtenerMedidas(pruebaIv);
					Optional<Medida> medidaKilom = medidas.stream().filter(m -> m.getTipoMedida().getTipoMedidaId()
							.equals(ConstantesMedidasKilometraje.KILOMETRAJE_MEDIDA)).findAny();
					if (medidaKilom.isPresent()) {

						Double kilometraje = medidaKilom.get().getValor();
						if (kilometraje >= 0) {
							informe.setKilometraje(df.format(kilometraje));
						} else {
							informe.setKilometraje("NO FUNCIONAL");
						}

					} else {

					}

				}

			}

		}

	}

	private void generarFechaInicioFinPrueba(InformeDieselCarRes0762 informe, Prueba prueba) {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd, HH:mm:ss");
		informe.setFechaInicioPrueba(sdf.format(prueba.getFechaInicio()));
		informe.setFechaFinPrueba(sdf.format(prueba.getFechaFinalizacion()));

	}

	public List<InformeCaldasDTO> getDatos() {
		return datos;
	}

	public void setDatos(List<InformeCaldasDTO> datos) {
		this.datos = datos;
	}
	
	
	
	

}
