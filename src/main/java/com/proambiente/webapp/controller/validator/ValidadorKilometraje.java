package com.proambiente.webapp.controller.validator;

import javax.inject.Named;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@Named
public class ValidadorKilometraje implements Validator{

	@Override
	public void validate(FacesContext arg0, UIComponent arg1, Object arg2) throws ValidatorException {
		
		
		
		String cadena = (String)(arg2);
		
		
		try {
			
			Integer kilometraje = Integer.valueOf(cadena);
			if(kilometraje < 0 || kilometraje >1000000) {
				throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_FATAL,"El kilometraje no puede ser menor a 0 o mayor a un millon","El kilometraje no puede ser menor a 0 o mayor a un millon") );
			}
			
		}catch(NumberFormatException nexc) {
			
			if(cadena.equals("NO FUNCIONAL")) {
				
				
			} else {
				throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_FATAL,"En caso de no ser numerico el kilometraje debe ser NO FUNCIONAL","En caso de no ser numerico el kilometraje debe ser NO FUNCIONAL") );
			}			
		}
		
		
	}

}
