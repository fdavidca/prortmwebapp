package com.proambiente.webapp.controller.validator;

import com.proambiente.modelo.Pipeta;
import com.proambiente.modelo.TipoMuestra;

public class PipetaValidator {
	
	
	public Boolean validarPipeta(Pipeta pipeta,StringBuilder mensajesError) {
		TipoMuestra tipoMuestra = pipeta.getTipoMuestra();
		
	 if(tipoMuestra.equals(TipoMuestra.BAJA)) {
		 
		 Boolean hcValido = Math.abs(300 - pipeta.getValorHC()) < 300*0.15; 
		 Boolean coValido = pipeta.getValorCO()>= 0.85 && pipeta.getValorCO() <= 1.15; 
		 Boolean co2Valido = pipeta.getValorCO2() >= 5.1 && pipeta.getValorCO2() <= 6.9;  
		 
		 if(!hcValido) {
			 mensajesError.append("Valor de HC fuera de rango").append(" ");
		 }
		 if(!coValido) {
			 mensajesError.append("Valor de CO fuera de rango").append(" ");
		 }
		 if(!co2Valido) {
			 mensajesError.append("Valor de CO2 fuera de rango").append(" ");
		 }
		 
		 return hcValido && coValido && co2Valido;
		 
	 }else if(tipoMuestra.equals(TipoMuestra.MEDIA)) {
		 
		 Boolean hcValido = Math.abs(1200 - pipeta.getValorHC()) < 1200*0.15;
		 Boolean coValido = pipeta.getValorCO()>= 3.4 && pipeta.getValorCO() <= 4.6; 
		 Boolean co2Valido = pipeta.getValorCO2() >= 10.2 && pipeta.getValorCO2() <= 13.8;  
		 
		 if(!hcValido) {
			 mensajesError.append("Valor de HC fuera de rango").append(" ");
		 }
		 if(!coValido) {
			 mensajesError.append("Valor de CO fuera de rango").append(" ");
		 }
		 if(!co2Valido) {
			 mensajesError.append("Valor de CO2 fuera de rango").append(" ");
		 }
		 
		 return hcValido && coValido && co2Valido;
		 
	 } else {
		 
		 Boolean hcValido = Math.abs(3200 - pipeta.getValorHC()) < 3200*0.15;
		 Boolean coValido = pipeta.getValorCO()>= 6.8 && pipeta.getValorCO() <= 9.2; 
		 Boolean co2Valido = pipeta.getValorCO2() >= 10.2 && pipeta.getValorCO2() <= 13.8;  
		 
		 if(!hcValido) {
			 mensajesError.append("Valor de HC fuera de rango").append(" ");
		 }
		 if(!coValido) {
			 mensajesError.append("Valor de CO fuera de rango").append(" ");
		 }
		 if(!co2Valido) {
			 mensajesError.append("Valor de CO2 fuera de rango").append(" ");
		 }
		 return hcValido && coValido && co2Valido;
		 
	 }
		
		
		
	}

}
