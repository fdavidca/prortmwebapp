package com.proambiente.webapp.controller.validator;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Named;

@Named
public class FechaMatriculaValidator implements Validator{

	LocalDate ldMinima = LocalDate.of(1900, 01, 01);
	LocalDate ldMaxima = LocalDate.now();
	
	@Override
	public void validate(FacesContext arg0, UIComponent uiComponent, Object value) throws ValidatorException {
			Date fechaMatricula = (Date) value;
			
			LocalDate date = fechaMatricula.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();		
			if(date.isBefore(ldMinima) || date.isAfter(ldMaxima) ){
				throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_FATAL, " Fecha de matricula menor a 1900 o mayor a la fecha de hoy", "La fecha de matricula no debe ser menor a 1900 ni posterior a la fecha de hoy"));
			}
			
	}

}
