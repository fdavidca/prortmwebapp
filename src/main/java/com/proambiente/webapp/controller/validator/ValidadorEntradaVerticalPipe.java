package com.proambiente.webapp.controller.validator;

import javax.inject.Named;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@Named
public class ValidadorEntradaVerticalPipe implements Validator{

	@Override
	public void validate(FacesContext arg0, UIComponent arg1, Object arg2) throws ValidatorException {
		String id = "";
		if(arg1 != null) {
			id = arg1.getId();
		}
		
		String cadena = (String)(arg2);
		
		if(cadena == null || cadena.isEmpty()) {
			StringBuilder sb = new StringBuilder();
			sb.append("El campo ").append(id).append(" no puede estar vacio");
			
			throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_FATAL,"El campo no puede estar vacio  ","El campo no puede estar vacio ") );
		}	
		if(cadena.contains("|")) {
			StringBuilder sb = new StringBuilder();
			sb.append("El campo ").append(id).append(" no puede contener el caracter | ");
			throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_FATAL,sb.toString(),sb.toString()) );		
		}		
	}

}
