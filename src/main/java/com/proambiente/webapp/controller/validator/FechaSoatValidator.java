package com.proambiente.webapp.controller.validator;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.webapp.service.FechaService;

@Named
public class FechaSoatValidator implements Validator {

	@Inject
	FechaService fechaService;
	
	@Override
	public void validate(FacesContext arg0, UIComponent arg1, Object value) throws ValidatorException {
		Date fechaSoat = (Date) value;		
		LocalDateTime dtFechaSoat =fechaSoat.toInstant()
			      .atZone(ZoneId.systemDefault())
			      .toLocalDateTime();
		
		//Calcular la fecha de maniana
		Calendar cHoy = fechaService.obtenerInicioHoy();
		LocalDateTime dtHoy = LocalDateTime.ofInstant(cHoy.toInstant(),ZoneId.systemDefault());	
		LocalDateTime dtManiana = dtHoy.plusDays(1);
		
		LocalDateTime dtHoyMenosDiezAnio =  LocalDateTime.ofInstant(cHoy.toInstant(),ZoneId.systemDefault()).minusYears(10);
		
		
		if(dtFechaSoat.isAfter(dtHoy)) {
			throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_FATAL, " Fecha de soat no puede ser posterior a la fecha de hoy", "La fecha de soat no puede ser posterior a la fecha de hoy"));
		}
		
		if(dtHoyMenosDiezAnio.isAfter(dtFechaSoat)) {
			throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_FATAL," Fecha de soat no puede ser anterior a la fecha de hoy menos 10 anios",
					"La fecha de soat no puede ser posterior a la fecha de hoy"));			
		}
	}

}
