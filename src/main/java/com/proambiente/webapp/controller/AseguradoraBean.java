package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.event.ActionEvent;
import org.omnifaces.cdi.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.Aseguradora;
import com.proambiente.webapp.dao.AseguradoraFacade;
import com.proambiente.webapp.util.UtilErrores; 

@Named
@ViewScoped
public class AseguradoraBean implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8419116536696867200L;

	private static final Logger logger = Logger.getLogger(AseguradoraBean.class
			.getName());
	
	@Inject
	AseguradoraFacade aseguradoraDAO;

	@Inject
	UtilErrores utilErrores;
	
	private Aseguradora nuevaAseguradora = new Aseguradora();	


	public Aseguradora getNuevaAseguradora() {
		return nuevaAseguradora;
	}

	public void setNuevaAseguradora(Aseguradora nuevaAseguradora) {
		this.nuevaAseguradora = nuevaAseguradora;
	}
	
	public void crearAseguradora(ActionEvent ae){
		try{
			aseguradoraDAO.create(nuevaAseguradora);
			utilErrores.addInfo("Aseguradora creada");			
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error creando aseguradora", exc);
			utilErrores.addError("Error creando aseguradora");			
		}	
	}
		
}
