package com.proambiente.webapp.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PreDestroy;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.PrimeFaces;
import org.tempuri.DatosSoap;

import com.proambiente.modelo.Revision;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.service.ci2.DatosSoapConfigurado;
import com.proambiente.webapp.util.UtilErrores;


@Named
@ViewScoped
public class RevisionPlacaBean implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4509826282703272360L;

	private static final Logger logger = Logger
			.getLogger(RevisionPlacaBean.class.getName());
	
	@Inject
	RevisionService revisionService;
	
	@Inject @DatosSoapConfigurado
	DatosSoap datosSoap;
	
	@Inject
	UtilErrores utilErrores;
	
	
	
	@Inject
	transient UtilNumeroCertificadoBean utilNumeroCertificado;
	
	private String placa;
	private List<Revision> revisiones;
	private Revision revisionSeleccionada;

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	
	
	public List<Revision> getRevisiones() {
		return revisiones;
	}

	public void setRevisiones(List<Revision> revisiones) {
		this.revisiones = revisiones;
	}
	
	public void buscarRevisiones(ActionEvent ae){
		if(placa != null && !placa.isEmpty()){
			try{
				revisiones = revisionService.buscarTodasRevisionesPorPlaca(placa);
				if(revisiones == null || revisiones.isEmpty()) {
					utilErrores.addInfo("No se encontraron revisiones");
				}else {
					utilErrores.addInfo("Revisiones encontradas: " + revisiones.size());
				}
			}catch(Exception exc){
				utilErrores.addError("Error consultando inspecciones");
				logger.log(Level.SEVERE, "Error consultando inspecciones por placa", exc);
				
			}
		}else{
			utilErrores.addError("Ingrese placa");
		}
	}

	public Revision getRevisionSeleccionada() {
		return revisionSeleccionada;
	}

	public void setRevisionSeleccionada(Revision revisionSeleccionada) {
		this.revisionSeleccionada = revisionSeleccionada;
	}

	public void imprimirReporte(ActionEvent ae){
		if(revisionSeleccionada != null){
			FacesContext context = FacesContext.getCurrentInstance();
			String baseURL = context.getExternalContext().getRequestContextPath();
		    String url = baseURL + "/ServletReporte?revision="+revisionSeleccionada.getRevisionId();
		    try {
		        String encodeURL = context.getExternalContext().encodeResourceURL(url);
		        context.getExternalContext().redirect(encodeURL);
		    } catch (Exception e) {
		    	logger.log(Level.SEVERE, "Error imprimir reporte", e);
		    } finally {
		        context.responseComplete();
		    }
		    
		} else {
			utilErrores.addInfo("Seleccione revison de la tabla");
			
		}
	}
	
	
	public void abrirDialogoPruebas(){
		if(revisionSeleccionada != null){
			try {				       
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("revision_id",revisionSeleccionada.getRevisionId() );	        
				FacesContext.getCurrentInstance().getExternalContext().redirect("pruebas.xhtml?faces-redirect=true");
			} catch (IOException e) {
				logger.log(Level.SEVERE, "Error redirigiendo a pagina de pruebas", e);
				utilErrores.addError("Error");
			}
		}else{
			utilErrores.addInfo("Seleccione una revision");
		}
	}	
	
	
	public void abrirDialogoConsecutivos(){
		if(revisionSeleccionada != null){
			Map<String, Object> options = new HashMap<String, Object>();
	        options.put("modal", true);
	        options.put("draggable", false);
	        options.put("resizable", false);
	        options.put("contentWidth", 800);
	        options.put("contentHeight", 600);
	        options.put("includeViewParams", true);
	 
	        Map<String, List<String>> params = new HashMap<String, List<String>>();
	        List<String> values = new ArrayList<String>();
	        String revisionId = String.valueOf(revisionSeleccionada.getRevisionId());
	        values.add(revisionId);
	        params.put("revisionId", values);
	        PrimeFaces.current().dialog().openDynamic("/pages/consecutivofupa.xhtml",options,params);
		}else{
			utilErrores.addInfo("Seleccione una revision");
		}
	}	
	
	
	public String imprimirCertificado(){
		try{
			if(revisionSeleccionada != null){
					String evaluacion = revisionService.evaluarRevision(revisionSeleccionada.getRevisionId());
					if(evaluacion.equalsIgnoreCase("aprobada")){
						logger.log(Level.INFO, "Imprimir certificado para placa {0}",revisionSeleccionada.getVehiculo().getPlaca());
						FacesContext.getCurrentInstance().getExternalContext().getFlash().put("revision_id",revisionSeleccionada.getRevisionId() );
						FacesContext.getCurrentInstance().getExternalContext().redirect("certificado.xhtml?faces-redirect=true");
				} else{
					utilErrores.addError("Revision no aprobada");
				}
			} else {
				utilErrores.addInfo("Seleccione revision de la tabla");				
			}			
		}catch(Exception exc){			
			logger.log(Level.SEVERE, "Error imprimir certificado ", exc);
		}
		return "";		
	}
	
	
	public String verPruebas(){
		if(revisionSeleccionada != null){			
			logger.log(Level.INFO, "Ver pruebas para {0}",revisionSeleccionada.getVehiculo().getPlaca());	
			FacesContext.getCurrentInstance().getExternalContext().getFlash().put("revision_id",revisionSeleccionada.getRevisionId() );
			
			return "/pages/pruebas.xhtml?faces-redirect=true";//EL SLASH AL INICIO DE "/PAGES/PRUEBAS"ES MUY IMPORTANTE
		} else {
			utilErrores.addInfo("Seleccione revision de la tabla");
			return "";
		}
	}
	
	
	public String editarRevision(){
		if(revisionSeleccionada != null){	
			return "/pages/revisiones?faces-redirect=true&revisionId=" + revisionSeleccionada.getRevisionId();			
		}else {
			utilErrores.addInfo("Seleccione revision de la tabla");
			return "";
		}
	}
	
	public String furRegistrados(){
		if(revisionSeleccionada != null){			
			logger.log(Level.INFO, "Ver registros de fur para {0}",revisionSeleccionada.getVehiculo().getPlaca());	
			FacesContext.getCurrentInstance().getExternalContext().getFlash().put("revision_id",revisionSeleccionada.getRevisionId() );
			return "/pages/furregistrados.xhtml?faces-redirect=true";//EL SLASH AL INICIO DE "/PAGES/PRUEBAS"ES MUY IMPORTANTE
		} else {
			utilErrores.addInfo("Seleccione revision de la tabla");
			return "";
		}
	}
	
	@PreDestroy
	public void limpiarListas() {
		if(revisiones != null) {
			revisiones.clear();
		}
	
	}
	
	public String numeroCertificado(int revisionId){
		return utilNumeroCertificado.numeroCertificado(revisionId);	
	}
	
	
}