package com.proambiente.webapp.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import org.omnifaces.cdi.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.Revision;
import com.proambiente.webapp.service.FechaService;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.util.UtilErrores;



@Named
@ViewScoped
public class ResumenBean implements Serializable{
	
	
	private static final Logger logger = Logger.getLogger(ResumenBean.class.getName());
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Date fechaInicial,fechaFinal;
	
	@Inject
	RevisionService revisionService;
	
	@Inject
	transient FechaService fechaService;
	
	@Inject
	UtilErrores utilErrores;
	
	@Inject
	transient UtilNumeroCertificadoBean utilNumeroCertificado;
	
	
	private List<Revision> revisionesConsultadas;
	
	@PostConstruct
	public void init(){
		Calendar calFechaHoy = fechaService.obtenerInicioHoy();
		fechaInicial = calFechaHoy.getTime();
		calFechaHoy.add(Calendar.DAY_OF_YEAR,1);		
		fechaFinal = calFechaHoy.getTime();	
		revisionesConsultadas = new ArrayList<>();
	}
	
	
	public void consultarRevisiones(AjaxBehaviorEvent ae){
		try{
			
			revisionesConsultadas = revisionService.consultarRevisionesVencidas();
			
		}catch(Exception exc){
			exc.printStackTrace();
			utilErrores.addError("Error consultando revisiones" + exc.getMessage());
		}
	}
	
	public void verPruebas(Integer revisionId){		
		if(revisionId != null){
			try {				       
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("revision_id",revisionId );	        
				FacesContext.getCurrentInstance().getExternalContext().redirect("pruebas.xhtml?faces-redirect=true");
			} catch (IOException e) {
				logger.log(Level.SEVERE, "Error redirigiendo a pagina de pruebas", e);
				utilErrores.addError("Error");
			}
		}else{
			utilErrores.addInfo("Seleccione una revision");
		}
	}
	
	public String numeroCertificado(int revisionId,Boolean aprobada){					
		return utilNumeroCertificado.numeroCertificado(revisionId);
	}
	
	public String estadoRevision(Integer revisionId){
		if(revisionId != null){
			return revisionService.evaluarRevision(revisionId);
		}else {
			return "--";
		}
	}
	
	
	

	public List<Revision> getRevisionesConsultadas() {
		return revisionesConsultadas;
	}




	public void setRevisionesConsultadas(List<Revision> revisionesConsultadas) {
		this.revisionesConsultadas = revisionesConsultadas;
	}




	public Date getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}
	
	
	
	

}
