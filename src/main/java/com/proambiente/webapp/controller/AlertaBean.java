package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.event.ActionEvent;
import org.omnifaces.cdi.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.Alerta;

import com.proambiente.webapp.service.AlertasDeHoyService;
import com.proambiente.webapp.service.ReconocerAlertaService;
import com.proambiente.webapp.util.UtilErrores;


@Named
@ViewScoped
public class AlertaBean implements Serializable{
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7410645805337351826L;

	private static final Logger logger = Logger
			.getLogger(AlertaBean.class.getName());

	private Date fechaInicial;
	private Date fechaFinal;
	
	@Inject
	AlertasDeHoyService alertaService;
	
	@Inject
	ReconocerAlertaService reconocerAlertaService;
	
	@Inject
	UtilErrores utilErrores;
	
	private List<Alerta> alertasGeneradas;
	
	
	public void consultarAlertas(ActionEvent ae){
		try {
			logger.info("Consultando alertas");
			alertasGeneradas = alertaService.consultarAlertasHoy();		
		} catch (Exception e) {			
			logger.log(Level.SEVERE, "Error consultando alertas", e);
			utilErrores.addError("error consultando alertas");
		}
	}
	
	
	
	public Date getFechaInicial() {
		return fechaInicial;
	}
	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	public Date getFechaFinal() {
		return fechaFinal;
	}
	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}



	public List<Alerta> getAlertasGeneradas() {
		return alertasGeneradas;
	}



	public void setAlertasGeneradas(List<Alerta> alertasGeneradas) {
		this.alertasGeneradas = alertasGeneradas;
	}
	
	
	public void reconocerAlerta(Integer alertaId) {
		try {
			reconocerAlertaService.reconocerAlerta(alertaId);
			utilErrores.addInfo("alerta reconocida");
		}catch(Exception exc) {
			logger.log(Level.SEVERE, "Error reconociendo alerta", exc);
			utilErrores.addError("Error reconociendo alertas");
		}
	}
	
}
