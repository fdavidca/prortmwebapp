package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.faces.event.ActionEvent;
import org.omnifaces.cdi.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.LazyDataModel;

import com.proambiente.modelo.Prueba;
import com.proambiente.webapp.util.UtilErrores;
import com.proambiente.webapp.util.dto.InformeGasolinaMotocicletasSDADTO;
import com.proambiente.webapp.util.dto.sda.InformeMotocicletaDTO;



/**
 * Clase que sirve como controlador para el informe de pruebas de gases
 * de vehiculos tipo motocicleta
 */
@Named
@ViewScoped
public class InfoPruebaGasesMotoBean implements Serializable{
	
	
	private static final Logger logger = Logger
			.getLogger(InfoPruebaGasesMotoBean.class.getName());

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@Inject @PeubaGasesModel
	private LazyDataModel<InformeGasolinaMotocicletasSDADTO> lazyDataModel;

	@Inject
	private UtilErrores utilErrores;

	private Integer progress = 1;

	@Resource
	private ManagedExecutorService managedExecutorService;

	private Date fechaInicial, fechaFinal;

	private List<Prueba> pruebas;

	private List<String[]> datos;

	private String[] encabezado = {
			"No. CDA",
			"Nombre CDA",
			"NIT CDA",
			"Dirección CDA",
			"Telefono 1  CDA",
			"Telefono 2 (celular)",
			"Ciudad CDA",
			"No Resolución CDA",
			"Fecha Resolución CDA",
			"Vr PEF",
			"Número de serie del banco",
			"No. serie  analizador",
			"Marca analizador",
			"Valor gas referencia bajo  HC verificacion y ajuste",
			"Valor gas referencia bajo CO verificacion y ajuste",
			"Valor gas referencia bajo CO2 verificacion y ajuste",
			"Valor gas referencia alto HC verificacion y ajuste",
			"Valor gas referencia  alto CO verificacion y ajuste",
			"Valor gas referencia  alto CO2 verificacion y ajuste",
			"Fecha  y hora ultima verificación y ajuste",
			"Nombre del software de aplicacion",
			"Version software de aplicacion",
			"No de consecutivo prueba",
			"Fecha y hora inicio de la prueba",
			"Fecha y hora final de la prueba",
			"Fecha y hora aborto de la prueba",
			"Inspector que realiza la prueba",
			"Temperatura ambiente",
			"Humedad Relativa",
			"Causal de aborto de la prueba",
			"Nombre o razón social propietario",
			"Tpo documento",
			"No. Documento de identificacion",
			"Direccion",
			"Telefono 1",
			"Telefono 2 (celular)",
			"Ciudad",
			"Marca",
			"Tipo de motor",
			"Linea",
			"Diseño",
			"Año modelo",
			"Placa",
			"Cilindraje",
			"Clase",
			"Servicio",
			"Combustible",
			"No Motor",
			"No VIN / Serie",
			"No licencia transito",
			"Kilometraje",
			"fugas tubo  escape",
			"fugas silenciador",
			"Accesorios o deformaciones en el tubo de escape que no permitan la instalacion sistema de muestreo",
			"tapa combustible o fugas", "tapa aceite o fugas",
			"Sistema de adimision de aire", "salidas adicionales diseño",
			"PCV (Sistema recirculacion de gases del carter)",
			"presencia humo negro, azul (solo motos 4T)",
			"Revoluciones fuera de rango", "Falla sistema de refrigeracion",
			"Temperatura de motor", "rpm ralenti", "hc ralenti", "co ralenti",
			"co2 ralenti", "o2 ralenti",
			"Incumplimiento de niveles de emision",
			"Concepto final del vehículo" };

	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

	
	
	public void consultarPruebasMotos(ActionEvent ae) {
		try {
			
			//RESTRINGIR A UN MES LA INFORMACION QUE SE PUEDE CARGAR			
			Calendar calendarFechaFinal = Calendar.getInstance();
			calendarFechaFinal.setTime(fechaFinal);
			calendarFechaFinal.add(Calendar.MONTH,-12);
			
			Calendar calendarFechaInicial = Calendar.getInstance();
			calendarFechaInicial.setTime(fechaInicial);
			if(calendarFechaFinal.compareTo(calendarFechaInicial) > 0){
				utilErrores.addInfo("No se puede consultar mas de 12 meses de informacion");
				return;
			}			
			logger.log(Level.INFO, "Informe motos fecha inicio {0}, fecha fin {1}",
					new Object[]{sdf.format(fechaInicial),sdf.format(fechaFinal)});
			LazyPruebaMotosGasolinaDataModel lazy = (LazyPruebaMotosGasolinaDataModel)lazyDataModel;
			lazy.setFechaFinal(fechaFinal);
			lazy.setFechaInicial(fechaInicial);
			
		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Error consultando pruebas", exc);
		}
	}

	


	@PreDestroy
	public void limpiarListas() {
		if(pruebas != null) {
			pruebas.clear();
		}
		if(datos != null) {
			datos.clear();
		}
	}




	public String[] getEncabezado() {
		return encabezado;
	}

	public void setEncabezado(String[] encabezado) {
		this.encabezado = encabezado;
	}

	public List<Prueba> getPruebas() {
		return pruebas;
	}

	public void setPruebas(List<Prueba> pruebas) {
		this.pruebas = pruebas;
	}

	public List<String[]> getDatos() {
		return datos;
	}

	public void setDatos(List<String[]> datos) {
		this.datos = datos;
	}

	public Date getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public Integer getProgress() {
		return progress;
	}

	public void setProgress(Integer progress) {
		this.progress = progress;
	}




	public LazyDataModel<InformeGasolinaMotocicletasSDADTO> getLazyDataModel() {
		return lazyDataModel;
	}




	public void setLazyDataModel(LazyDataModel<InformeGasolinaMotocicletasSDADTO> lazyDataModel) {
		this.lazyDataModel = lazyDataModel;
	}

	


}
