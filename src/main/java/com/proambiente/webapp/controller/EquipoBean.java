package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import org.omnifaces.cdi.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.Equipo;
import com.proambiente.modelo.TipoEquipo;
import com.proambiente.webapp.dao.EquipoFacade;
import com.proambiente.webapp.dao.TipoEquipoFacade;
import com.proambiente.webapp.util.UtilErrores;

@Named
@ViewScoped
public class EquipoBean implements Serializable{
	
	private static final Logger logger = Logger.getLogger(EquipoBean.class
			.getName());
	private static final long serialVersionUID = 1L;
	
	@Inject
	EquipoFacade equipoFacade;
	
	@Inject
	TipoEquipoFacade tipoEquipoFacade;
	
	@Inject
	UtilErrores utilErrores;

	private List<Equipo> listaEquipos;
	
	private Equipo equipoSeleccionado;
	
	private List<TipoEquipo> listaTiposEquipo;


	@PostConstruct
	public void init(){
		listaEquipos = equipoFacade.findAll();
		listaTiposEquipo = tipoEquipoFacade.findAll();
	}


	public List<Equipo> getListaEquipos() {
		return listaEquipos;
	}


	public void setListaEquipos(List<Equipo> listaEquipos) {
		this.listaEquipos = listaEquipos;
	}


	public Equipo getEquipoSeleccionado() {
		return equipoSeleccionado;
	}


	public void setEquipoSeleccionado(Equipo equipoSeleccionado) {
		this.equipoSeleccionado = equipoSeleccionado;
	}

	
	
	public List<TipoEquipo> getListaTiposEquipo() {
		return listaTiposEquipo;
	}


	public void setListaTiposEquipo(List<TipoEquipo> listaTiposEquipo) {
		this.listaTiposEquipo = listaTiposEquipo;
	}


	public void guardarCambios(ActionEvent ae){
		try{
			logger.log(Level.INFO,"Guardando cambios para " + equipoSeleccionado.toString());
			equipoFacade.edit(equipoSeleccionado);
			init();
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error guardando cambios para " + equipoSeleccionado.toString(),exc);
			utilErrores.addError("Error guardando campos para " + equipoSeleccionado.toString());
		}
		
	}
	
	public void atachEquipo(ActionEvent ae){
		
			
	}
	
	public void nuevoEquipo(ActionEvent ae){
		
		equipoSeleccionado = new Equipo();
		
	}

}
