package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.NivelAlerta;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Usuario;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.webapp.dao.InformacionCdaFacade;
import com.proambiente.webapp.dao.VehiculoFacade;
import com.proambiente.webapp.service.PinCi2Service;
import com.proambiente.webapp.service.PinNoEncontradoException;
import com.proambiente.webapp.service.RevisionMismoDiaService;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.service.notification.NotificationPrimariaService;
import com.proambiente.webapp.service.sicov.OperacionesSicov;
import com.proambiente.webapp.service.sicov.OperacionesSicovProducer;
import com.proambiente.webapp.util.UtilCopiarInformacionVehiculo;
import com.proambiente.webapp.util.UtilErrores;

@Named
@ConversationScoped
public class IngresoDatosRapidoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = Logger.getLogger(IngresoDatosRapidoBean.class.getName());

	private static final String PRIMERA_INSPECCION = "1";
	
	private static final String PIN_NO_UTILIZADO = "PIN_NO_UTILIZADO";

	@Inject
	RevisionService revisionService;
	
	@Inject
	RevisionMismoDiaService revisionesParaVehiculoHoy;

	@Inject
	@UsuarioDefault
	Usuario usuarioAutenticado;

	@Inject
	UtilErrores utilErrores;

	@Inject
	VehiculoFacade vehiculoDAO;

	@Inject
	@VehiculoDefault
	Vehiculo vehiculo;

	@Inject
	private Conversation conversation;

	@Inject
	transient OperacionesSicovProducer sicovProducer;

	Revision revision = new Revision();

	Boolean deshabilitarBoton = false;
	
	Boolean deshabilitarInputPlaca = false;

	Boolean mostrarOpcionTiemposMotor = false;

	Boolean mostrarOpcionTaximetro = false;

	Boolean crearPruebaTaximetro = false;

	OperacionesSicov operacionesSicov;

	private String pin;

	private Boolean preventiva = false;

	@Inject
	NotificationPrimariaService notificacionService;

	@Inject
	PinCi2Service pinCi2Service;
	
	@Inject
	InformacionCdaFacade infoCdaFacade;

	@PostConstruct
	public void init() {
		deshabilitarBoton = false;
		deshabilitarInputPlaca = false;
		mostrarOpcionTiemposMotor = vehiculoNoEsLivianoPesado();
		mostrarOpcionTaximetro = vehiculoEsServicioPublico();
		crearPruebaTaximetro = false;
		operacionesSicov = sicovProducer.crearOperacionSicov();
	}

	public void initConversation() {
		if (!FacesContext.getCurrentInstance().isPostback() && conversation.isTransient()) {

			conversation.begin();
		}
	}

	private Boolean vehiculoEsServicioPublico() {
		if (vehiculo.getServicio() != null)
			return vehiculo.getServicio().getServicioId() == 2;
		else
			return false;
	}

	public void blurPlaca(AjaxBehaviorEvent abe) {

		try {
			try {
				if (preventiva) {

				} else {
					if (vehiculo.getPlaca().isEmpty()) {
						utilErrores.addError("Placa de vehiculo vacia");
						return;
					} else {
						pin = operacionesSicov.consultarPinPlaca(vehiculo.getPlaca());
						utilErrores.addInfo("PIN : " + pin);
						revision.setPin(pin);
					}
				}
			} catch (PinNoEncontradoException pexc) {
				pin = PIN_NO_UTILIZADO;
				revision.setPin(pin);
				utilErrores.addError("No se pudo consultar pin");
				logger.log(Level.SEVERE, "No existe pin", pexc);
				notificacionService.sendNotification("No se pudo consultar Pin " + pexc.getMessage(),
						NivelAlerta.ERROR_GRAVE, Boolean.TRUE);
			} catch (Exception exc) {
				pin = PIN_NO_UTILIZADO;
				revision.setPin(pin);
				logger.log(Level.SEVERE, "Error al consultar pin", exc);
				notificacionService.sendNotification("Error al consultar el Pin" + exc.getMessage(),
						NivelAlerta.ERROR_GRAVE, Boolean.TRUE);
			}

			Vehiculo v = vehiculoDAO.findVehicuoByPlaca(vehiculo.getPlaca());
			if (v == null) {
				// utilErrores.add("Vehiculo no preexistente en bd");
			} else {
				vehiculo = null;
				vehiculo = UtilCopiarInformacionVehiculo.copiarVehiculo(v);
				mostrarOpcionTiemposMotor = vehiculoNoEsLivianoPesado();
				mostrarOpcionTaximetro = vehiculoEsServicioPublico();
				logger.log(Level.INFO, "Informacion de vehiculo {0} cargada", vehiculo.getPlaca());
				utilErrores.addInfo("Informacion de vehiculo cargada");
				
			}
			deshabilitarInputPlaca = true;
		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Error en evento blur del campo de texto placa", exc);
			utilErrores.addError("Error" + exc.getMessage());
		}

	}

	public void registrarRevisionSinInformacionPropietario(ActionEvent ae) {
		try {
			//si se pretende crear una revision oficial
			if(!preventiva) {
				long revisionesOficialesCreadasHoy = revisionesParaVehiculoHoy.revisionesMismoDiaPorPlaca(vehiculo.getPlaca());
				if(revisionesOficialesCreadasHoy>0) {
					String mensaje =String.format("Error, ya existe una REVISION OFICIAL CREADA para %s , intente crear revision preventiva",vehiculo.getPlaca());
					utilErrores.addError(mensaje);
					return;
				}
			}			
			revision.setPreventiva(preventiva);
			revision.setVehiculo(vehiculo);
			revision.setTaximetro(crearPruebaTaximetro);// para indicar que se cree la prueba de taximetro
			if(vehiculo.getCilindraje() == 0 && (vehiculo.getTipoCombustible().getCombustibleId().equals(3)
					                             || vehiculo.getTipoCombustible().getCombustibleId().equals(11)) ) {
				utilErrores.addError("Cilindraje de vehiculo diesel no puede ser 0");
				return;
			}
			if (revision.getPreventiva()) {

				revisionService.registrarRevisionSinPropietario(revision, vehiculo, usuarioAutenticado);
				deshabilitarBoton = true;

			} else {
				try {
					if (pin == null || pin.equalsIgnoreCase(PIN_NO_UTILIZADO)) {
						utilErrores.addError("El pin no es válido, UTILICE EL PIN MEDIANTE WEB DE SICOV");
						InformacionCda infoCda = infoCdaFacade.find(1);
						if(infoCda.getBloquearIngresoPorPin()) {
							return;
						}
					}
					operacionesSicov = sicovProducer.crearOperacionSicov();

					if (revision.getRevisionId() == null) {// Es la primera
															// vez,se crea
															// la revision
						Revision revisionCreada = revisionService.registrarRevisionSinPropietario(revision, vehiculo,
								usuarioAutenticado);
						utilErrores.addInfo("Revision registrada con exito");
						try {
							operacionesSicov.utilizarPin(pin, PRIMERA_INSPECCION, vehiculo.getPlaca());
							pinCi2Service.registrarPinUtilizado(revisionCreada.getRevisionId());
						} catch (Exception exc) {
							logger.log(Level.SEVERE, "Error utilizando pin", exc);
							notificacionService.sendNotification("no se pudo utilizar pin" + exc.getMessage(),
									NivelAlerta.ERROR_GRAVE, Boolean.TRUE);
							utilErrores.addError("No se pudo utilizar pin");
						}
						deshabilitarBoton = true;
					}

				} catch (Exception e) {
					logger.log(Level.SEVERE, "Error ", e);
					utilErrores.addError("No se pudo crear revisión");
				}

			} // final del else

		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Error registro rapido de revision", exc);
			utilErrores.addError("Error al registrar revision " + exc.getMessage());
		} finally {
			if (!conversation.isTransient()) {
				conversation.end();
			}
		}
	}

	public String finalizar() {
		if (!conversation.isTransient()) {
			conversation.end();
		}
		return "/inicio.xhtml?faces-redirect=true";
	}

	public void comboTipoVehiculoListener(ActionEvent ae) {
		mostrarOpcionTiemposMotor = vehiculoNoEsLivianoPesado();
	}

	public void comboServicioVehiculoListener(ActionEvent ae) {
		mostrarOpcionTaximetro = vehiculoEsServicioPublico();
	}

	private Boolean vehiculoNoEsLivianoPesado() {
		Integer tipoVehiculoId = vehiculo.getTipoVehiculo().getTipoVehiculoId();
		return ( tipoVehiculoId!= 1 && tipoVehiculoId != 3);

	}

	public Vehiculo getVehiculo() {
		return vehiculo;
	}

	public void setVehiculo(Vehiculo vehiculo) {
		this.vehiculo = vehiculo;
	}

	public Boolean getDeshabilitarBoton() {
		return deshabilitarBoton;
	}

	public void setDeshabilitarBoton(Boolean deshabilitarBoton) {
		this.deshabilitarBoton = deshabilitarBoton;
	}

	public Boolean getMostrarOpcionTiemposMotor() {
		return mostrarOpcionTiemposMotor;
	}

	public void setMostrarOpcionTiemposMotor(Boolean mostrarOpcionTiemposMotor) {
		this.mostrarOpcionTiemposMotor = mostrarOpcionTiemposMotor;
	}

	public Boolean getCrearPruebaTaximetro() {
		return crearPruebaTaximetro;
	}

	public void setCrearPruebaTaximetro(Boolean crearPruebaTaximetro) {
		this.crearPruebaTaximetro = crearPruebaTaximetro;
	}

	public Boolean getMostrarOpcionTaximetro() {
		return mostrarOpcionTaximetro;
	}

	public void setMostrarOpcionTaximetro(Boolean mostrarOpcionTaximetro) {
		this.mostrarOpcionTaximetro = mostrarOpcionTaximetro;
	}

	public Boolean getPreventiva() {
		return preventiva;
	}

	public void setPreventiva(Boolean preventiva) {
		this.preventiva = preventiva;
	}

	public Boolean getDeshabilitarInputPlaca() {
		return deshabilitarInputPlaca;
	}

	public void setDeshabilitarInputPlaca(Boolean deshabilitarInputPlaca) {
		this.deshabilitarInputPlaca = deshabilitarInputPlaca;
	}
	
	

}
