package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.omnifaces.cdi.ViewScoped;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;




import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Revision_;
import com.proambiente.webapp.service.ConsecutivoRuntRevisionService;
import com.proambiente.webapp.service.RevisionIdPorConsecutivoRuntService;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.service.VehiculoService;
import com.proambiente.webapp.util.UtilErrores;


@Named
@ViewScoped
public class ConsecutivoFupaBean implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3507818342204007194L;

	private static final Logger logger = Logger
			.getLogger(ConsecutivoFupaBean.class.getName());
	
	@Inject
	UtilErrores utilErrores;
	
	@Inject
	RevisionService revisionService;
	
	@Inject
	VehiculoService vehiculoService;
	
	@Inject 
	transient ConsecutivoRuntRevisionService consecutivoRuntService;
	
	@Inject
	transient RevisionIdPorConsecutivoRuntService revisionIdPorCRService;
	
	
	private String revisionId;

	private Revision revision;

	private String placa;
	
	
	
	public void guardar(){		
		try{
			//consultar si existe una revision con el mismo consecutivo runt
			Optional<Integer> optRevisionId = revisionIdPorCRService.buscarRevisionIdPorConsecutivoRuntService(revision.getConsecutivoRunt());
			if(optRevisionId.isPresent()) {//en caso de que existe una revision con el  consecutivo runt
				Integer revisionId = optRevisionId.get();
				if(!revisionId.equals(revision.getRevisionId())) {//si no es la misma revision que se está editando
					FacesContext.getCurrentInstance().addMessage("form1:consecutivo_runt", new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error: Consecutivo Runt Ya registrado para revision : " + revisionId,""));
					return;
				}				
			}			
			revisionService.registrarConsecutivoRuntFupa(revision.getRevisionId(), revision.getConsecutivoRunt(),revision.getNumeroSolicitud());
			FacesContext.getCurrentInstance().addMessage("form1:consecutivo_runt", new FacesMessage(FacesMessage.SEVERITY_INFO,"OK operacion exitosa ","Ok operacion exitosa"));
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error editando revision", exc);
			FacesContext.getCurrentInstance().addMessage("form1:consecutivo_runt", new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error: Consecutivo Runt Duplicado","Error"));
		}
	}
	
	
	public void cancelar(){
		try{
			//RequestContext.getCurrentInstance().closeDialog(null);
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error editando revision", exc);
			utilErrores.addError("Error guardando cambios");
		}
	}
	
	
	
	public Revision getRevision() {
		return revision;
	}

	public void setRevision(Revision revision) {
		this.revision = revision;
	}

	public String getRevisionId() {
		return revisionId;
	}

	public void setRevisionId(String revisionId) {
		this.revisionId = revisionId;
		Integer revisionIdInt = Integer.valueOf(revisionId);
		revision = revisionService.cargarRevisionDTO(revisionIdInt,Revision_.consecutivoRunt,Revision_.numeroSolicitud,Revision_.revisionId);
		placa = vehiculoService.placaDesdeRevisionId(revisionIdInt);
	}

	public String getPlaca() {
		return placa;
	}


	public void setPlaca(String placa) {
		this.placa = placa;
	}

}

