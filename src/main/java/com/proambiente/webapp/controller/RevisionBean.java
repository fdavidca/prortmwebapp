package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.SelectEvent;

import com.proambiente.modelo.Ciudad;
import com.proambiente.modelo.ClaseVehiculo;
import com.proambiente.modelo.Color;
import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.LineaVehiculo;
import com.proambiente.modelo.Marca;
import com.proambiente.modelo.NivelAlerta;
import com.proambiente.modelo.Propietario;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.TipoVehiculo;
import com.proambiente.modelo.Usuario;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.webapp.dao.CiudadFacade;
import com.proambiente.webapp.dao.ClaseVehiculoFacade;
import com.proambiente.webapp.dao.ColorFacade;
import com.proambiente.webapp.dao.InformacionCdaFacade;
import com.proambiente.webapp.dao.MarcaFacade;
import com.proambiente.webapp.dao.PropietarioFacade;
import com.proambiente.webapp.dao.VehiculoFacade;
import com.proambiente.webapp.service.PinCi2Service;
import com.proambiente.webapp.service.PinNoEncontradoException;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.service.ci2.OperacionesSicovCi2V3;
import com.proambiente.webapp.service.notification.NotificationPrimariaService;
import com.proambiente.webapp.service.sicov.OperacionesSicov;
import com.proambiente.webapp.service.sicov.OperacionesSicovProducer;
import com.proambiente.webapp.util.ConstantesClaseVehiculo;
import com.proambiente.webapp.util.ConstantesTiposVehiculo;
import com.proambiente.webapp.util.UtilCopiarInformacionVehiculo;
import com.proambiente.webapp.util.UtilErrores;


/**
 * Clase que sirve de controlador para la pagina en la que se ingresan o se
 * editan los datos de la revision, vehiculo y propietario
 * 
 * @author FABIAN
 */
@Named
@ConversationScoped
public class RevisionBean implements Serializable {

	private static final Logger logger = Logger.getLogger(RevisionBean.class.getName());
	private static final long serialVersionUID = 1L;
	private static final String PIN_NO_UTILIZADO = "PIN_NO_UTILIZADO";

	@Inject
	@VehiculoDefault
	Vehiculo vehiculo;

	@Inject
	InformacionCdaFacade infoCdaFacade;

	@Inject
	MarcaFacade marcaDAO;

	@Inject
	ColorFacade colorDAO;

	@Inject
	CiudadFacade ciudadFacade;

	@Inject
	ClaseVehiculoFacade claseVehiculoDAO;

	@Inject
	VehiculoFacade vehiculoDAO;

	@Inject
	PropietarioFacade propietarioDAO;

	@Inject
	Propietario propietario;

	@Inject
	Propietario conductor;

	@Inject
	UtilErrores utilErrores;

	@Inject
	Revision revision;

	@Inject
	PrincipalService principalService;

	@Inject
	RevisionService revisionService;

	@Inject
	transient OperacionesSicovProducer sicovProducer;

	@Inject
	NotificationPrimariaService notificacionService;
	
	@Inject
	PinCi2Service pinCi2Service;

	@Resource
	ManagedExecutorService executor;
	
	@Inject
	Conversation conversation;

	transient OperacionesSicov operacionesSicov;

	private Propietario propietarioAnterior;

	Date fechaMatriculaAuxiliar, fechaSoatAuxiliar,fechaVencimientoGnvAuxiliar;

	private boolean propietarioDiferenteConductor, vehiculoPreexistente, propietarioPreexistente, initEjecutado;

	String revisionId;

	private String pin;

	private Boolean crearRevision = false;

	private Boolean desHabilitarBoton = false;
	
	private List<LineaVehiculo> listaLineasVehiculo;

	private boolean mostrarCheckBoxPruebaTaximetro;

	private String mensajeCheckBoxTaximetro;
	private boolean mostrarFechaConversionGnv = false;

	public RevisionBean() {
		fechaSoatAuxiliar = new Date();
		fechaMatriculaAuxiliar = new Date();
		fechaVencimientoGnvAuxiliar = new Date();
	}
	
	public void initConversation() {
		
	}


	public void init() {
		try {
			
			if (!FacesContext.getCurrentInstance().isPostback() && conversation.isTransient()) {
				conversation.begin();
			}

			if (!initEjecutado) {
				initEjecutado = true;
				InformacionCda infoCda = infoCdaFacade.find(1);
				if (revisionId == null) {
					if (vehiculo.getMarca() != null) {
						String codigoDivipoStr = infoCda.getCodigoDivipo();
						Integer codigoDivipo = Integer.valueOf(codigoDivipoStr);
						Ciudad ciudad = ciudadFacade.find(codigoDivipo);
						propietario.setCiudad(ciudad);
						listaLineasVehiculo = marcaDAO.findLineaVehiculosByMarca(vehiculo.getMarca());
					}
					return;
				}
				Integer id = Integer.valueOf(revisionId);

				if (id != null) {// en caso de que la revision ya exista y se
									// quiera cargar
					revision = revisionService.cargarRevision(id);
					logger.log(Level.INFO, "Revision cargada para {0}, revision id {1}",
							new Object[] { revision.getVehiculo().getPlaca(), revision.getRevisionId() });
					vehiculo = revision.getVehiculo();
					propietario = revision.getPropietario();
					if (propietario == null) {
						propietario = new Propietario();
					}
					if (vehiculo.getMarca() != null) {
						listaLineasVehiculo = marcaDAO.findLineaVehiculosByMarca(vehiculo.getMarca());
					}
					if (vehiculo.getFechaMatricula() != null) {
						fechaMatriculaAuxiliar = new Date(vehiculo.getFechaMatricula().getTime());
					}
					if (vehiculo.getFechaSoat() != null) {
						fechaSoatAuxiliar = new Date(vehiculo.getFechaSoat().getTime());
					}
					mostrarFechaConversionGnv = vehiculo.getConversionGnv().equals("SI");
					if(vehiculo.getFechaVenicimientoGNV() != null){
						fechaVencimientoGnvAuxiliar = new Date(vehiculo.getFechaVenicimientoGNV().getTime());
					}

				} else {
					crearRevision = true;
				}
			}

		} catch (Exception exc) {
			logger.log(Level.SEVERE, "No se puede cargar vista de ingreso edicion de revision", exc);
			utilErrores.addError("No se puede restablecer la vista" + exc.getMessage());
		}finally {
			if (!FacesContext.getCurrentInstance().isPostback() && !(conversation == null) && conversation.isTransient()) {
				conversation.end();
			}
		}
	}

	public Vehiculo getVehiculo() {
		return vehiculo;
	}

	public Propietario getPropietario() {
		return propietario;
	}

	public Propietario getConductor() {
		return conductor;
	}

	public Revision getRevision() {
		return revision;
	}



	public List<Marca> getListaMarcasByNombre(String como) {
		List<Marca> lista = null;
		if (como == null || como.isEmpty()) {
			// lista = marcaDAO.findMarcasByNombre("%");
			lista = new ArrayList<Marca>();
		} else {
			lista = marcaDAO.findMarcasByNombre(como);
		}
		return lista;

	}

	public List<Color> findColoresByNombre(String como) {

		List<Color> lista = null;
		if (como == null || como.isEmpty()) {
			lista = new ArrayList<Color>();
		} else {
			lista = colorDAO.findColoresByNombre(como);
		}
		return lista;
	}

	public List<Ciudad> findCiudadByNombre(String como) {
		List<Ciudad> lista = null;
		if (como == null || como.isEmpty()) {
			lista = new ArrayList<Ciudad>();
		} else {
			lista = ciudadFacade.findCiudadByNombre(como);
		}
		return lista;
	}

	public void cambioMarca(ValueChangeEvent vce) {
		Marca m = (Marca) vce.getNewValue();
		if (m != null) {
			listaLineasVehiculo = marcaDAO.findLineaVehiculosByMarca(m);
		}

	}

	public void cambioTipoVehiculo(ValueChangeEvent vce) {
		TipoVehiculo tipoVehiculo = (TipoVehiculo) vce.getNewValue();
		if (tipoVehiculo != null) {
			if (tipoVehiculo.getTipoVehiculoId().equals(ConstantesTiposVehiculo.MOTO)) {// MOTOCICLETA
				ClaseVehiculo claseMotocicleta = claseVehiculoDAO.find(ConstantesClaseVehiculo.MOTOCICLETA);
				vehiculo.setClaseVehiculo(claseMotocicleta);//
			} else if (tipoVehiculo.getTipoVehiculoId().equals(ConstantesTiposVehiculo.LIVIANO)) {
				ClaseVehiculo claseAutomovil = claseVehiculoDAO.find(ConstantesClaseVehiculo.AUTOMOVIL);
				vehiculo.setClaseVehiculo(claseAutomovil);
			} else if (tipoVehiculo.getTipoVehiculoId().equals(ConstantesTiposVehiculo.MOTO_CARRO)) {
				ClaseVehiculo claseMotocarro = claseVehiculoDAO.find(ConstantesClaseVehiculo.MOTOCARRO);
				vehiculo.setClaseVehiculo(claseMotocarro);
			}
		}
	}

	public List<LineaVehiculo> getLineasVehiculo() {
		if (this.listaLineasVehiculo == null) {
			return new ArrayList<LineaVehiculo>();
		} else
			return this.listaLineasVehiculo;
	}

	public void comboServicioListener(AjaxBehaviorEvent abe) {
		if (vehiculo.getServicio().getNombre().equalsIgnoreCase("Publico")) {
			this.mostrarCheckBoxPruebaTaximetro = true;
			mensajeCheckBoxTaximetro = "Crear Prueba Taximetro";
		}
	}
	
	public void comboConversionGNVListener(AjaxBehaviorEvent abe) {
		if(vehiculo.getConversionGnv().equals("SI")) {
			this.mostrarFechaConversionGnv = true;			
		} else {
			this.mostrarFechaConversionGnv = false;
		}
	}

	
	/**
	 * Consulta el pin por placa 
	 * en caso de no ser el pin correcto
	 * asigna constante PIN_NO_UTILIZADO
	 * 
	 * Carga la información del vehiculo en caso de que ya esté registrado
	 * @param abe
	 */
	public void blurPlaca(AjaxBehaviorEvent abe) {

		try {

			if (revisionId == null) {
				try {
					if (revision.getPreventiva()) {

					} else {
						operacionesSicov = sicovProducer.crearOperacionSicov();
						pin = operacionesSicov.consultarPinPlaca(vehiculo.getPlaca());
						utilErrores.addInfo("PIN: " + pin);
						revision.setPin(pin);
					}
				} catch (PinNoEncontradoException exc) {
					pin = PIN_NO_UTILIZADO;
					revision.setPin(pin);
					utilErrores.addError("No se pudo consultar pin");
					logger.log(Level.SEVERE, "No existe pin", exc);
					notificacionService.sendNotification("No se pudo consultar Pin " + exc.getMessage(),
							NivelAlerta.ERROR_GRAVE, Boolean.TRUE);
				} catch (Exception e) {
					pin = PIN_NO_UTILIZADO;
					revision.setPin(pin);
					logger.log(Level.SEVERE, "Error al consultar pin", e);
					notificacionService.sendNotification("Error al consultar el Pin" + e.getMessage(),
							NivelAlerta.ERROR_GRAVE, Boolean.TRUE);
				}

			}

			if (!vehiculoPreexistente) {
				Vehiculo v = vehiculoDAO.findVehicuoByPlaca(vehiculo.getPlaca());
				if (v == null) {
					utilErrores.addInfo("Vehiculo no preexistente en bd");
				} else {
					vehiculo = null;				
					vehiculo = UtilCopiarInformacionVehiculo.copiarVehiculo(v);
					if (vehiculo.getMarca() != null) {
						listaLineasVehiculo = marcaDAO.findLineaVehiculosByMarca(vehiculo.getMarca());
					}
					vehiculoPreexistente = true;
					mostrarFechaConversionGnv = vehiculo.getConversionGnv().equals("SI");
					fechaMatriculaAuxiliar = vehiculo.getFechaMatricula();
					if(vehiculo.getFechaVenicimientoGNV() != null)
						fechaVencimientoGnvAuxiliar = vehiculo.getFechaVenicimientoGNV();
					if(vehiculo.getFechaSoat() != null)
						fechaSoatAuxiliar = vehiculo.getFechaSoat();
					logger.log(Level.INFO, "Informacion de vehiculo {0} cargada", vehiculo.getPlaca());
					utilErrores.addInfo("Informacion de vehiculo cargada");
					// propietario = vehiculo.getPropietario();
				}
			}
		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Error en evento blur del campo de texto placa", exc);
			utilErrores.addError("Error" + exc.getMessage());
		}

	}

	public void blurIdentificacion(AjaxBehaviorEvent abe) {

		try {
			if (!propietarioPreexistente) {
				Propietario p = propietarioDAO.find(propietario.getPropietarioId());
				if (p == null) {
					utilErrores.addInfo("Propietario no preexistente en bd");
				} else {

					propietarioAnterior = new Propietario();
					propietarioAnterior.setApellidos(p.getApellidos());
					propietarioAnterior.setNombres(p.getNombres());
					propietarioAnterior.setCiudad(p.getCiudad());
					propietarioAnterior.setDireccion(p.getDireccion());
					propietarioAnterior.setFechaRegistro(p.getFechaRegistro());
					propietarioAnterior.setNumeroLicencia(p.getNumeroLicencia());
					propietarioAnterior.setPropietarioId(p.getPropietarioId());
					propietarioAnterior.setTelefono1(p.getTelefono1());
					propietarioAnterior.setTelefono2(p.getTelefono2());
					propietarioAnterior.setTipoIdentificacion(p.getTipoIdentificacion());
					propietarioAnterior.setTipoLicencia(p.getTipoLicencia());
					propietario = null;
					propietario = p;
					propietarioPreexistente = true;
					utilErrores.addInfo("Informacion de propietario cargada");
				}
			}

		} catch (Exception exc) {

			logger.log(Level.SEVERE, "Error tratando de cargar informacion propietario blur", exc);
			utilErrores.addError("Error" + exc.getMessage());

		}

	}

	public void blurIdentificacionConductor(AjaxBehaviorEvent abe) {

		try {
			Propietario p = propietarioDAO.find(conductor.getPropietarioId());
			if (p == null) {
				utilErrores.addInfo("No preexistente en base de datos");

			} else {
				conductor = p;
			}

		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Error blur campo de identificacion conductor", exc);
			utilErrores.addError("Error " + exc.getCause());
		}

	}

	public boolean isVehiculoPreexistente() {
		return vehiculoPreexistente;
	}

	public void setVehiculoPreexistente(boolean vehiculoPreexistente) {
		this.vehiculoPreexistente = vehiculoPreexistente;
	}

	public boolean isMostrarCheckBoxPruebaTaximetro() {
		return mostrarCheckBoxPruebaTaximetro;
	}

	public void setMostrarCheckBoxPruebaTaximetro(boolean mostrarCheckBoxPruebaTaximetro) {
		this.mostrarCheckBoxPruebaTaximetro = mostrarCheckBoxPruebaTaximetro;
	}

	public boolean isPropietarioPreexistente() {
		return propietarioPreexistente;
	}

	public String getMensajeCheckBoxTaximetro() {
		return mensajeCheckBoxTaximetro;
	}

	public void setMensajeCheckBoxTaximetro(String mensajeCheckBoxTaximetro) {
		this.mensajeCheckBoxTaximetro = mensajeCheckBoxTaximetro;
	}

	public boolean isPropietarioDiferenteConductor() {
		return propietarioDiferenteConductor;
	}

	public void setPropietarioDiferenteConductor(boolean propietarioIgualConductor) {
		this.propietarioDiferenteConductor = propietarioIgualConductor;
	}

	public Date getFechaMatriculaAuxiliar() {
		return fechaMatriculaAuxiliar;
	}

	public void setFechaMatriculaAuxiliar(Date fechaMatriculaAuxiliar) {
		this.fechaMatriculaAuxiliar = fechaMatriculaAuxiliar;
	}

	public Date getFechaSoatAuxiliar() {
		return fechaSoatAuxiliar;
	}

	public void setFechaSoatAuxiliar(Date fechaSoatAuxiliar) {
		this.fechaSoatAuxiliar = fechaSoatAuxiliar;
	}

	public void registrarRevision(ActionEvent ae) {
		//si la revision no es preventiva y el pin es nulo o pin no utilizado
		if (!revision.getPreventiva() && ( revision.getPin() == null || revision.getPin().equalsIgnoreCase(PIN_NO_UTILIZADO) ) ) {
			utilErrores.addError("El pin no es válido, no se puede crear revisión");
			return;
		}

		if (propietario.getTipoDocumentoIdentidad() == null) {
			utilErrores.addError("Seleccione de nuevo un tipo de documento del propietario ");
			return;
		}

		vehiculo.setFechaMatricula(new Timestamp(fechaMatriculaAuxiliar.getTime()));
		vehiculo.setFechaSoat(new Timestamp(fechaSoatAuxiliar.getTime()));
		if(fechaVencimientoGnvAuxiliar != null) {
			vehiculo.setFechaVenicimientoGNV( new Timestamp( fechaVencimientoGnvAuxiliar.getTime() ) );
		}
		if (!propietarioDiferenteConductor) {
			conductor = propietario;
		} else {

		}
		try {

			Usuario usuarioAutenticado = principalService.getUsuarioAutenticado();
			Revision revisionCreada = revisionService.registrarRevision(revision, vehiculo, propietario, conductor, usuarioAutenticado);
			logger.log(Level.INFO, "Revision creada // editada {0} propietario {1} revisionId {2}",
					new Object[] { revision.getVehiculo().getPlaca(), propietario.getPropietarioId(),revisionCreada.getRevisionId() });

			executor.submit(() -> {
				try {
					operacionesSicov = sicovProducer.crearOperacionSicov();
					if (revision.getPreventiva()) {

						logger.log(Level.INFO, "Revision  se crea preventiva " + vehiculo.getPlaca());
					} else {
						if (revision.getRevisionId() == null) {// Es la primera
																// vez,se crea
																// la revision
							operacionesSicov.utilizarPin(pin, OperacionesSicovCi2V3.PRIMERA_INSPECCION, vehiculo.getPlaca());							
							pinCi2Service.registrarPinUtilizado(revisionCreada.getRevisionId());
						}
					}
				} catch (Exception exc) {
					logger.log(Level.SEVERE, "Error en sicov utilizar pin para placa", exc);
					notificacionService.sendNotification("Error, no se puede utilizar pin para placa", NivelAlerta.ERROR_GRAVE, Boolean.TRUE);
				}
			});

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error registrando revision", e);
			utilErrores.addError("Error registrando revision" + e.getMessage());
		}finally {
			if (!FacesContext.getCurrentInstance().isPostback() && !(conversation == null) && conversation.isTransient()) {
				conversation.end();
			}
		}
		utilErrores.addInfo("Revision creada");
		desHabilitarBoton = true;
	}

	public void editarVehiculo(ActionEvent ae) {

		try {

			if (vehiculo.getPlaca() != null) {
				vehiculo = revisionService.editarVehiculo(vehiculo);
				utilErrores.addInfo("Vehiculo editado correctamente");
				vehiculoPreexistente = false;
			}
		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Error editando vehiculo " + vehiculo.getPlaca(), exc);
			utilErrores.addError("Error editando vehiculo " + exc.getCause());
		}
	}

	public void editarPropietario(ActionEvent ae) {

		try {
			if (propietario.getPropietarioId() != null) {
				propietario = revisionService.editarPropietario(propietario);
				utilErrores.addInfo("Propietario editado correctamente");
			}
		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Error editando propietario", exc);
			utilErrores.addError("Error editando propietario " + exc.getMessage());

		}
	}

	public String finalizar() {
		if(!conversation.isTransient()) {
			conversation.end();
		}
		return "/inicio.xhtml?faces-redirect=true";
	}

	public String limpiar() {
		return null;
	}

	public void mostrarDialogoCrearMarca() {
		//RequestContext.getCurrentInstance().openDialog("/pages/marcas.xhtml");
	}

	public void onDialogReturn(SelectEvent selectionEvent) {
		LineaVehiculo linea = (LineaVehiculo) selectionEvent.getObject();
		if (linea != null) {
			listaLineasVehiculo = marcaDAO.findLineaVehiculosByMarca(linea.getMarca());

			System.out.println("Linea creada:" + linea.getNombre());
			vehiculo.setLineaVehiculo(linea);
			vehiculo.setMarca(linea.getMarca());
		} else {
			System.out.println("No se creo linea");
		}
	}

	public void mostrarDialogoCrearLinea() {

	}

	public String getRevisionId() {
		return revisionId;
	}

	public void setRevisionId(String revisionId) {
		this.revisionId = revisionId;
	}

	public Boolean getDesHabilitarBoton() {
		return desHabilitarBoton;
	}

	public void setDesHabilitarBoton(Boolean desHabilitarBoton) {
		this.desHabilitarBoton = desHabilitarBoton;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public Date getFechaVencimientoGnvAuxiliar() {
		return fechaVencimientoGnvAuxiliar;
	}

	public void setFechaVencimientoGnvAuxiliar(Date fechaVencimientoGnvAuxiliar) {
		this.fechaVencimientoGnvAuxiliar = fechaVencimientoGnvAuxiliar;
	}

	public boolean isMostrarFechaConversionGnv() {
		return mostrarFechaConversionGnv;
	}

	public void setMostrarFechaConversionGnv(boolean mostrarFechaConversionGnv) {
		this.mostrarFechaConversionGnv = mostrarFechaConversionGnv;
	}
	
	

}
