package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.omnifaces.cdi.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.Inspeccion;
import com.proambiente.webapp.service.InspeccionService;
import com.proambiente.webapp.service.guardarpdf.GuardarPdfCallback;
import com.proambiente.webapp.service.guardarpdf.GuardarPdfCallbackFactory;
import com.proambiente.webapp.service.sicov.OperacionesSicov;
import com.proambiente.webapp.service.sicov.OperacionesSicovProducer;
import com.proambiente.webapp.util.UtilErrores;

@Named
@ViewScoped
public class InspeccionBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5489325609896167121L;

	private static final Logger logger = Logger.getLogger(InspeccionBean.class.getName());

	@Inject
	InspeccionService inspeccionService;

	@Inject
	UtilErrores utilErrores;

	@Inject
	transient OperacionesSicovProducer sicovProducer;

	OperacionesSicov operacionesSicov;

	private Inspeccion inspeccionSeleccionada;
	
	@Inject
	GuardarPdfCallbackFactory guardarPdfCallbackFactory;

	private String placa;
	private List<Inspeccion> inspecciones;

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public void buscarInspecciones(ActionEvent ae) {
		if (placa != null && !placa.isEmpty()) {
			try {
				inspecciones = inspeccionService.inspeccionesPorPlaca(placa);
			} catch (Exception exc) {
				utilErrores.addError("Error consultando inspecciones");
				logger.log(Level.SEVERE, "Error consultando inspecciones " + placa, exc);
			}
		} else {
			utilErrores.addError("Ingrese placa");
		}
	}

	public void validarSicov() {
		try {
			
			if (inspeccionSeleccionada == null) {
				utilErrores.addError("Seleccione inspeccion");
				return;
			}
			String inspeccionIdStr = String.valueOf(inspeccionSeleccionada.getInspeccionId());
			Integer inspeccionId = Integer.valueOf(inspeccionIdStr);
			operacionesSicov = sicovProducer.crearOperacionSicov();
			operacionesSicov.registrarFURInspeccionPrimerEnvio(inspeccionId);
			

		} catch (Exception exc) {
			logger.log(Level.SEVERE, " Error al validar sicov", exc);
			utilErrores.addError(" Excepcion al validar sicov");
		}
	}
	
	public void segundoEnvioInspeccion() {
		try {
			if (inspeccionSeleccionada == null) {
				utilErrores.addError("Seleccione inspeccion");
				return;
			}
			GuardarPdfCallback guardarPdfCallback = guardarPdfCallbackFactory.crearInstancia();
			String inspeccionIdStr = String.valueOf(inspeccionSeleccionada.getInspeccionId());
			Integer inspeccionId = Integer.valueOf(inspeccionIdStr);
			Integer revisionId = inspeccionService.revisionIdDesdeInspeccionId(inspeccionId);
			operacionesSicov = sicovProducer.crearOperacionSicov();
			operacionesSicov.registrarInformacionRunt(revisionId,guardarPdfCallback);
			
			
		}catch(Exception exc) {
			logger.log(Level.SEVERE, " Error al validar sicov", exc);
			utilErrores.addError(" Excepcion al validar sicov");
		}
	}

	public List<Inspeccion> getInspecciones() {
		return inspecciones;
	}

	public void setInspecciones(List<Inspeccion> inspecciones) {
		this.inspecciones = inspecciones;
	}

	public Inspeccion getInspeccionSeleccionada() {
		return inspeccionSeleccionada;
	}

	public void setInspeccionSeleccionada(Inspeccion inspeccionSeleccionada) {
		this.inspeccionSeleccionada = inspeccionSeleccionada;
	}

	public void imprimirReporte() {
		if (inspeccionSeleccionada == null) {
			utilErrores.addError("Seleccione inspeccion");
		}
		String inspeccionId = String.valueOf(inspeccionSeleccionada.getInspeccionId());

		FacesContext context = FacesContext.getCurrentInstance();

		String baseURL = context.getExternalContext().getRequestContextPath();

		String url = baseURL + "/ServletReporteInspeccion?inspeccion_id=" + inspeccionId;

		try {

			String encodeURL = context.getExternalContext().encodeResourceURL(url);
			context.getExternalContext().redirect(encodeURL);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error imprimiendo reporte", e);
		} finally {
			context.responseComplete();
		}
	}

	public String verDetalles() {
		try {
			if (inspeccionSeleccionada != null) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("inspeccion_id",
						inspeccionSeleccionada.getInspeccionId());
				return "detallesinspeccion?faces-redirect=true";
			} else {
				utilErrores.addInfo(" SELECCIONE INSPECCION DE LA TABLA");
				return "";
			}
		}catch(Exception exc) {
			logger.log(Level.SEVERE, "Error", exc);
		}
		return "";
	}

}
