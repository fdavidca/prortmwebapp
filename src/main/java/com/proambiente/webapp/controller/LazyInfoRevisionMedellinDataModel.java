package com.proambiente.webapp.controller;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.inject.Inject;

import org.primefaces.model.FilterMeta;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortMeta;
import org.primefaces.model.SortOrder;

import com.proambiente.modelo.Analizador;
import com.proambiente.modelo.Certificado;
import com.proambiente.modelo.Defecto;
import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.Medida;
import com.proambiente.modelo.Propietario;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.modelo.VerificacionGasolina;
import com.proambiente.modelo.dto.ResultadoOttoMotocicletasDTO;
import com.proambiente.webapp.dao.InformacionCdaFacade;
import com.proambiente.webapp.dao.PermisibleFacade;
import com.proambiente.webapp.service.CertificadoService;
import com.proambiente.webapp.service.InspeccionService;
import com.proambiente.webapp.service.PruebasService;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.service.RevisionesPorFechaService;
import com.proambiente.webapp.service.VersionSoftwareService;
import com.proambiente.webapp.service.generadordto.GeneradorDTOResultados;
import com.proambiente.webapp.service.generadordto.GeneradorDTOTolima;
import com.proambiente.webapp.util.UtilErrores;
import com.proambiente.webapp.util.dto.InformacionRevisionDTO;
import com.proambiente.webapp.util.dto.InformeGasolinaMotocicletasCarDTO;
import com.proambiente.webapp.util.dto.ResultadoOttoInformeDTO;
import com.proambiente.webapp.util.dto.sda.CausaRechazoMotocicletas;
import com.proambiente.webapp.util.dto.sda.InformeMotocicletaDTO;
import com.proambiente.webapp.util.informes.DeterminadorCausaRechazoMotos;
import com.proambiente.webapp.util.informes.GeneradorDTOInformes;
import com.proambiente.webapp.util.informes.medellin.GeneradorInformeRevisiones;

@PeubaGasesModelMedellin 
public class LazyInfoRevisionMedellinDataModel extends LazyDataModel<InformacionRevisionDTO> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String SI = "SI";
	private static final String NO = "NO";

	private static final Logger logger = Logger.getLogger(LazyInfoRevisionMedellinDataModel.class.getName());

	@Inject
	PruebasService pruebaService;
	
	@Inject
	InspeccionService inspeccionService;
	
	@Inject
	CertificadoService certificadoService;
	
	@Inject 
	RevisionesPorFechaService revisionService;
	
	@Inject
	PermisibleFacade permisibleFacade;

	private Date fechaInicial, fechaFinal;

	@Inject
	private InformacionCdaFacade informacionCda;

	@Inject
	private VersionSoftwareService versionSoftwareService;

	@Inject
	private UtilErrores utilErrores;

	@Resource
	private ManagedExecutorService managedExecutorService;

	private InformacionCda infoCda;

	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

	private SimpleDateFormat sdfPrueba = new SimpleDateFormat("yyyy/MM/dd,HH:mm:ss");

	private String versionSoftware;

@Override
	public List<InformacionRevisionDTO> load(int first, int pageSize, Map<String, SortMeta> sortBy, Map<String, FilterMeta> filterBy)  {
		List<InformacionRevisionDTO> datos = new ArrayList<>();

		try {

			List<Revision> revisiones = revisionService.consultarRevisionesPorFecha(fechaInicial, fechaFinal,false);
			long numeroPruebas = revisiones.size();			
			this.setRowCount((int) numeroPruebas);
			if (revisiones.isEmpty()) {
				utilErrores.addError("No se encontraron revisiones");
				return null;
			}
			logger.info("Numero Pruebas Totales:" + numeroPruebas);
			infoCda = informacionCda.find(1);

			versionSoftware = versionSoftwareService.obtenerVersionSoftware();

			String nombreSoftware = "PRORTM";

			if (datos != null)
				datos.clear();
			datos = new ArrayList<>();
			
			
			
			for (Revision revision : revisiones) {
				
				logger.log(Level.INFO, "Procesando revision : " + revision.getRevisionId() );
				String consecutivoCertificado = "";
				Optional<Certificado> optCertificado = Optional.empty();
				try {
					  Certificado certificado = certificadoService.consultarUltimoCertificado( revision.getRevisionId() );
					  if(certificado != null) {
						  optCertificado = Optional.of(certificado);
					  }
				}catch(Exception exc) {
					logger.log(Level.SEVERE, "Error cargando certificado" + revision.getRevisionId() );
				}

				GeneradorInformeRevisiones generador = new GeneradorInformeRevisiones();
				
				InformacionRevisionDTO informe = generador.generarInforme(revision, optCertificado, infoCda);
				
				datos.add(informe);
			}

			return datos;
		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Error cargando prueba metodo dos", exc);
			return null;
		}

	}

	public Date getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	private void cargarResultado(InformeGasolinaMotocicletasCarDTO informe, ResultadoOttoMotocicletasDTO resultado) {

		informe.setTemperaturaDeMotor( resultado.getTempRalenti().equals("0") ? "NA" : resultado.getTempRalenti() );
		informe.setRpmRalenti(resultado.getRpmRalenti());
		informe.setHcRalenti(resultado.getHcRalenti());
		informe.setCoRalenti(resultado.getCoRalenti());
		informe.setCo2Ralenti(resultado.getCo2Ralenti());
		informe.setO2Ralenti(resultado.getO2Ralenti());

		informe.setRevolucionesFueraRango( resultado.getRevolucionesFueraRango().equalsIgnoreCase("TRUE") ? "SI":"NO" );
		informe.setFugasTuboEscape(resultado.getFugasTuboEscape().equalsIgnoreCase("TRUE") ? "SI":"NO");
		informe.setFugasSilenciador(resultado.getFugasSilenciador().equalsIgnoreCase("TRUE") ? "SI":"NO");
		informe.setFugasTapaCombustible(resultado.getTapaCombustible().equalsIgnoreCase("TRUE") ? "SI":"NO");
		informe.setFugasTapaAceite(resultado.getTapaAceite().equalsIgnoreCase("TRUE") ? "SI":"NO");
		informe.setSalidasAdicionalesDiseno(resultado.getSalidasAdicionales().equalsIgnoreCase("TRUE") ? "SI":"NO");
		informe.setPresenciaHumoNegroAzul(resultado.getPresenciaHumos().equalsIgnoreCase("TRUE") ? "SI":"NO");
		informe.setAccesoriosDeformacionesImpiden(NO);

	}

	private void cargarFaltante(InformeGasolinaMotocicletasCarDTO informe, ResultadoOttoInformeDTO r2) {
		informe.setTemperaturaAmbiente(r2.getTemperaturaAmbiente());
		informe.setHumedadRelativa(r2.getHumedadRelativa());
	}

	private void cargarInformacionAnalizador(Prueba prueba, InformeGasolinaMotocicletasCarDTO informe) {
		Analizador analizador = pruebaService.consultarAnalizadorPrueba(prueba);
		DecimalFormat df = new DecimalFormat("0.000");
		if (analizador != null) {
			informe.setVlrPEf(df.format(analizador.getPef()));
			informe.setNumeroSerieBanco(analizador.getSerial());
			informe.setNumeroSerieAnalizador(analizador.getSerialAnalizador());
			informe.setMarcaAnalizador(analizador.getMarca());

		} else {
			informe.setVlrPEf("No config");
			informe.setNumeroSerieBanco("No config");
			informe.setNumeroSerieAnalizador("No config");
			informe.setMarcaAnalizador("No config");
		}

	}

	private void cargarInformacionVerificacion(Prueba prueba, InformeMotocicletaDTO informe, SimpleDateFormat sdf) {
		VerificacionGasolina verif = pruebaService.consultarVerificacionPrueba(prueba);
		DecimalFormat df = new DecimalFormat();
		if (verif != null) {

			df = new DecimalFormat("#");
			String refBajaHC = verif.getValorGasRefBajaHC() != null ? df.format(verif.getValorGasRefBajaHC()) : "";
			informe.setVlrSpanBajoHC(refBajaHC);

			String resultadoValorBajaHc = verif.getValorBajoHc() != null ? df.format(verif.getValorBajoHc()) : "";
			informe.setResultadoValorSpanBajoHC(resultadoValorBajaHc);

			df = new DecimalFormat("0.00");
			String refBajaCO = verif.getValorGasRefBajaCO() != null ? df.format(verif.getValorGasRefBajaCO()) : "";
			informe.setVlrSpanBajoCO(refBajaCO);

			String valorBajaCO = verif.getValorBajoCo() != null ? df.format(verif.getValorBajoCo()) : "";
			informe.setResultadoValorSpanBajoCO(valorBajaCO);

			df = new DecimalFormat("0.0");
			String referenciaBajaCO2 = verif.getValorGasRefBajaCO2() != null ? df.format(verif.getValorGasRefBajaCO2())
					: "";
			informe.setVlrSpanBajoCO2(referenciaBajaCO2);

			String valorBajaCO2 = verif.getValorBajoCo2() != null ? df.format(verif.getValorBajoCo2()) : "";
			informe.setResultadoValorSpanBajoCO2(valorBajaCO2);

			df = new DecimalFormat("#");
			String refAltaHC = verif.getValorGasRefAltaHC() != null ? df.format(verif.getValorGasRefAltaHC()) : "";
			informe.setVlrSpanAltoHC(refAltaHC);

			String valorAltaHC = verif.getValorAltoHc() != null ? df.format(verif.getValorAltoHc()) : "";
			informe.setResultadoValorSpanAltoHC(valorAltaHC);

			df = new DecimalFormat("0.00");
			String refAltaCO = verif.getValorGasRefAltaCO() != null ? df.format(verif.getValorGasRefAltaCO()) : "";
			informe.setVlrSpanAltoCO(refAltaCO);
			
			String valorAltoCO = verif.getValorAltoCo() != null ? df.format(verif.getValorAltoCo()): "";
			informe.setResultadoValorSpanAltoCO(valorAltoCO);

			df = new DecimalFormat("0.0");
			String refAltaCO2 = verif.getValorGasRefAltaCO2() != null ? df.format(verif.getValorGasRefAltaCO2()) : "";
			informe.setVlrSpanAltoCO2(refAltaCO2);

			String valorAltaCO2 = verif.getValorAltoCo2() != null ? df.format(verif.getValorAltoCo2()) : "";
			informe.setResultadoValorSpanAltoCO2(valorAltaCO2);

			String fechaVerif = verif.getFechaVerificacion() != null ? sdf.format(verif.getFechaVerificacion()) : "";
			informe.setFechaHoraUltimaCalibracion(fechaVerif);
		} else {
			informe.setVlrSpanBajoHC("");
			informe.setVlrSpanBajoCO("");
			informe.setVlrSpanBajoCO2("");

			informe.setVlrSpanAltoHC("");
			informe.setVlrSpanAltoCO("");
			informe.setVlrSpanAltoCO2("");
			informe.setFechaHoraUltimaCalibracion("");

		}

	}

	@Override
	public int count(Map<String, FilterMeta> filterBy) {		
		return 0;
	}

	
	
	
}
