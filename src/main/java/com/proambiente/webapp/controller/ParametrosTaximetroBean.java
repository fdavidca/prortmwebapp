package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;

import com.proambiente.modelo.ParametroPruebaTaximetro;
import com.proambiente.webapp.dao.ParametroTaximetroFacade;
import com.proambiente.webapp.dao.UsuarioFacade;
import com.proambiente.webapp.util.UtilErrores;

/**
 * Clase que sirve como controlador para la vista de editar usuarios
 * @author FABIAN
 *
 */

@Named
@ViewScoped
public class ParametrosTaximetroBean implements Serializable{
	
	
	private static final Logger logger = Logger.getLogger(ParametrosTaximetroBean.class
			.getName());
	private static final long serialVersionUID = 1L;

	@Inject
	UsuarioFacade usuarioDAO;
	
	@Inject
	ParametroTaximetroFacade parametrosPruebaTaximetroDAO;
	
	@Inject 
	UtilErrores utilErrores;	
	
	ParametroPruebaTaximetro parametroSeleccionado;
	
	private Boolean deshabilitarGuardar;
	
	private List<ParametroPruebaTaximetro> listaParametrosPruebaTaximetro;
	

	
	public ParametrosTaximetroBean(){
		parametroSeleccionado = new ParametroPruebaTaximetro();		
	}
	
	@PostConstruct
	public void init(){
		listaParametrosPruebaTaximetro = parametrosPruebaTaximetroDAO.findAll();		
	}

	
	
	
	private void addError(String string) {
		FacesContext fc = FacesContext.getCurrentInstance();
		FacesMessage fm = new FacesMessage();
		fm.setSummary(string);
		fm.setSeverity(FacesMessage.SEVERITY_ERROR);
		fc.addMessage(string,fm);		
	}
	
	public void nuevaConfiguracion() {
		parametroSeleccionado = new ParametroPruebaTaximetro();
		deshabilitarGuardar = false;
	}

	public Boolean getDeshabilitarGuardar() {
		return deshabilitarGuardar;
	}

	public void setDeshabilitarGuardar(Boolean deshabilitarGuardar) {
		this.deshabilitarGuardar = deshabilitarGuardar;
	}

	public List<ParametroPruebaTaximetro> getListaParametrosPruebaTaximetro() {
		return listaParametrosPruebaTaximetro;
	}

	public void setListaParametrosPruebaTaximetro(List<ParametroPruebaTaximetro> listaParametrosPruebaTaximetro) {
		this.listaParametrosPruebaTaximetro = listaParametrosPruebaTaximetro;
	}

	public ParametroPruebaTaximetro getParametroSeleccionado() {
		return parametroSeleccionado;
	}

	public void setParametroSeleccionado(ParametroPruebaTaximetro parametroSeleccionado) {
		this.parametroSeleccionado = parametroSeleccionado;
	}
	
	public void guardarCambios(ActionEvent ae){
		try {
		if(parametroSeleccionado.getParametrosTaximetroId() == null){
			if( parametroSeleccionado.getCiudad() == null){				
				utilErrores.addError("Ingrese ciudad");
				return;
			}else if ( parametroSeleccionado.getCiudad().isEmpty() ){
				addError("Ingrese ciudad");
				return;
			}		
		}
		
		if(parametroSeleccionado.getParametrosTaximetroId() == null){
			if( parametroSeleccionado.getDistanciaPorUnidad() == null){				
				utilErrores.addError("Ingrese distancia por unidad");
				return;
			}else if ( parametroSeleccionado.getDistanciaPorUnidad()<0 ){
				addError("Distancia por unidad no puede ser negativa");
				return;
			}		
		}
		
		if(parametroSeleccionado.getParametrosTaximetroId() == null){
			if( parametroSeleccionado.getTiempoPorUnidad() == null){				
				utilErrores.addError("Ingrese tiempo por unidad");
				return;
			}else if ( parametroSeleccionado.getTiempoPorUnidad()<0 ){
				addError("Tiempo por unidad no puede ser negativa");
				return;
			}		
		}
		
			parametrosPruebaTaximetroDAO.edit(parametroSeleccionado);
			deshabilitarGuardar = true;
		} catch(Exception exc){
			logger.log(Level.SEVERE, "Error guardando cambios de usuario", exc);
			utilErrores.addError("Error creando usuario");
		}
		listaParametrosPruebaTaximetro = parametrosPruebaTaximetroDAO.findAll();		
	}
	
	public void editarActionListener(ActionEvent ae) {
		deshabilitarGuardar = false;
	}
	
}
