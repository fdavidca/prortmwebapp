package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import org.omnifaces.cdi.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.dto.MedicionGasesDTO;
import com.proambiente.webapp.dao.PruebaFacade;
import com.proambiente.webapp.util.MedicionGasesDTODecodificador;
import com.proambiente.webapp.util.UtilErrores;

@Named
@ViewScoped
public class LogPruebaGasesBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7088786374632658877L;

	private static final Logger logger = Logger.getLogger(LogPruebaGasesBean.class.getName());
	
	@Inject
	PruebaFacade pruebaFacade;
	
	@Inject
	UtilErrores utilErrores;
	
	List<List<MedicionGasesDTO>> muestras = new ArrayList<>();
	
	
	@PostConstruct
	public void init(){
		
		try{
			Integer id = (Integer) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("prueba_id");
			prueba = pruebaFacade.find(id);
			try {
				String medidasTexto = prueba.getMedidasTexto();
				if(medidasTexto != null && !medidasTexto.isEmpty()) {
					muestras = MedicionGasesDTODecodificador.decodificarDesdeCadenaVariasListas(medidasTexto);
				}
			}catch(Exception exc) {
				logger.log(Level.SEVERE,"Error cargando muestras de prueba " + id);
			}
			
			logger.log(Level.INFO,"Prueba cargada {0}",id);
		}catch(Exception exc){
			utilErrores.addError("No se puede restablecer la vista" + exc.getMessage());			
			logger.log(Level.SEVERE, "Error no se puede cargar la vista de pruebas ", exc);
		}
	}
	
	
	
	
	private Integer pruebaId;
	
	private Prueba prueba;

	public Prueba getPrueba() {
		return prueba;
	}

	public void setPrueba(Prueba prueba) {
		this.prueba = prueba;
	}

	public Integer getPruebaId() {
		return pruebaId;
	}

	public void setPruebaId(Integer pruebaId) {
		this.pruebaId = pruebaId;
	}




	public List<MedicionGasesDTO> getMuestrasBaja() {
				
		if(muestras != null && muestras.size() >= 1)
		return muestras.get(0);
		else 
			return new ArrayList<>();
	}

	
	public List<MedicionGasesDTO> getMuestrasAlta(){
		if(muestras != null && muestras.size() >= 2)
			return muestras.get(1);
			else 
				return new ArrayList<>();
	}
}