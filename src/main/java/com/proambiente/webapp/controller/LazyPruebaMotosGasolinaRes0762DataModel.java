package com.proambiente.webapp.controller;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.inject.Inject;

import org.primefaces.model.FilterMeta;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortMeta;

import com.proambiente.modelo.Defecto;
import com.proambiente.modelo.Equipo;
import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.Inspeccion;
import com.proambiente.modelo.Medida;
import com.proambiente.modelo.Pipeta;
import com.proambiente.modelo.Propietario;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.PruebaFuga;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Usuario;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.modelo.VerificacionGasolina;
import com.proambiente.modelo.dto.ResultadoOttoMotocicletasDTO;
import com.proambiente.webapp.dao.InformacionCdaFacade;
import com.proambiente.webapp.dao.PermisibleFacade;
import com.proambiente.webapp.dao.PipetaFacade;
import com.proambiente.webapp.dao.UsuarioFacade;
import com.proambiente.webapp.service.CertificadoService;
import com.proambiente.webapp.service.InspeccionService;
import com.proambiente.webapp.service.PruebaFugaAprobadaService;
import com.proambiente.webapp.service.PruebasService;
import com.proambiente.webapp.service.RevisionService;
import com.proambiente.webapp.service.VersionSoftwareService;
import com.proambiente.webapp.service.generadordto.GeneradorDTOResultados;
import com.proambiente.webapp.util.ConstantesMedidasKilometraje;
import com.proambiente.webapp.util.UtilErrores;

import com.proambiente.webapp.util.dto.ResultadoOttoInformeDTO;
import com.proambiente.webapp.util.dto.sda.CausaRechazoMotocicletas;

import com.proambiente.webapp.util.informes.DeterminadorCausaRechazoMotos;
import com.proambiente.webapp.util.informes.medellin.GeneradorInformeMotoRes0762;

import com.proambiente.webapp.util.informes.medellin.InformeMotocicletasRes0762;

@PruebaGasesMotosModelRes0762
public class LazyPruebaMotosGasolinaRes0762DataModel extends LazyDataModel<InformeMotocicletasRes0762> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String SI = "SI";
	private static final String NO = "NO";

	private static final Logger logger = Logger.getLogger(LazyPruebaMotosGasolinaRes0762DataModel.class.getName());

	@Inject
	PruebasService pruebaService;

	@Inject
	RevisionService revisionService;

	@Inject
	InspeccionService inspeccionService;

	@Inject
	CertificadoService certificadoService;

	@Inject
	PermisibleFacade permisibleFacade;

	@Inject
	UsuarioFacade usuarioFacade;

	@Inject
	PipetaFacade pipetaFacade;

	private Date fechaInicial, fechaFinal;

	@Inject
	private InformacionCdaFacade informacionCda;

	@Inject
	private VersionSoftwareService versionSoftwareService;

	@Inject
	private UtilErrores utilErrores;

	@Resource
	private ManagedExecutorService managedExecutorService;

	private InformacionCda infoCda;

	@Inject
	PruebaFugaAprobadaService pruebaFugaAprobadaService;

	private String versionSoftware;

	@Override
	public List<InformeMotocicletasRes0762> load(int first, int pageSize, Map<String, SortMeta> sortBy,
			Map<String, FilterMeta> filterBy) {
		List<InformeMotocicletasRes0762> datos = new ArrayList<>();

		try {

			List<Prueba> pruebas = inspeccionService.consultarPruebasOttoMotocicletasPaginado(fechaInicial, fechaFinal,
					first, pageSize);
			long numeroPruebas = inspeccionService.contarPruebasOttoMotocicletasInspeccion(fechaInicial, fechaFinal);
			this.setRowCount((int) numeroPruebas);
			if (pruebas.isEmpty()) {
				utilErrores.addError("No se encontraron pruebas");
				return null;
			}
			logger.info("Numero Pruebas Totales:" + numeroPruebas);
			infoCda = informacionCda.find(1);

			versionSoftware = versionSoftwareService.obtenerVersionSoftware();

			String nombreSoftware = "PRORTM";

			if (datos != null)
				datos.clear();
			datos = new ArrayList<>();
			GeneradorDTOResultados generadorResultados = new GeneradorDTOResultados();
			generadorResultados.setPuntoComoSeparadorDecimales();
			GeneradorInformeMotoRes0762 generadorInforme = new GeneradorInformeMotoRes0762();

			DeterminadorCausaRechazoMotos determinadorRechazo = new DeterminadorCausaRechazoMotos(permisibleFacade);
			for (Prueba prueba : pruebas) {

				logger.log(Level.INFO, "Procesando prueba : " + prueba.getPruebaId());
				String consecutivoCertificado = "";
				try {
					consecutivoCertificado = certificadoService
							.consultarUltimoConsecutivoCertificado(prueba.getRevision().getRevisionId());
				} catch (Exception exc) {
					logger.log(Level.SEVERE, "Error cargando certificado" + prueba.getRevision().getRevisionId());
				}

				InformeMotocicletasRes0762 informe = new InformeMotocicletasRes0762();

				generadorInforme.agregarInformacionCda(infoCda, informe);
				generadorInforme.agregarInformacionPrueba(informe, prueba);
				generadorInforme.agregarInformacionSoftware(informe, nombreSoftware, versionSoftware);
				generadorInforme.agregarInformacionCertificado(informe, consecutivoCertificado);

				Propietario propietario = prueba.getRevision().getPropietario();

				if (propietario != null) {
					generadorInforme.agregarInformacionPropietario(propietario, informe);
				}
				Vehiculo vehiculo = prueba.getRevision().getVehiculo();
				if (vehiculo != null) {
					generadorInforme.agregarInformacionVehiculo(vehiculo, informe);
				}

				generadorInforme.agregarInformacionAnalizador(prueba.getAnalizador(), informe);
				Double pef = prueba.getAnalizador().getPef();

				VerificacionGasolina verificacion = prueba.getVerificacionGasolina();

				Pipeta pipetaAlta = pipetaFacade.find(verificacion.getSpanAltoPipetaId());
				Pipeta pipetaBaja = pipetaFacade.find(verificacion.getSpanBajoPipetaId());

				Usuario usuario = usuarioFacade.find(verificacion.getUsuarioId());

				generadorInforme.agregarInformacionPipetas(informe, pipetaBaja, pipetaAlta, pef);
				generadorInforme.agregarResultadoVerificacion(informe, verificacion, usuario, pef);

				String fechaFugas = cargarPruebaFugas(prueba.getFechaInicio(), prueba.getAnalizador().getSerial());
				informe.setFechaHoraPruebaFugas(fechaFugas);

				Revision revision = prueba.getRevision();
				Date fechaRevision = revision.getFechaCreacionRevision();
				Integer jefeTecnicoId = revision.getUsuarioResponsableId();
				Usuario jefeTecnico = null;
				if (revision.getNumeroInspecciones().equals(2)) {

					List<Inspeccion> inspecciones = inspeccionService
							.obtenerInspeccionesPorRevisionId(revision.getRevisionId());
					if (inspecciones.size() > 1) {
						Inspeccion segunda = inspecciones.get(1);
						fechaRevision = segunda.getFechaInspeccionAnterior();
						jefeTecnico = segunda.getUsuarioResponsable();
					}

				} else {

					jefeTecnico = usuarioFacade.find(jefeTecnicoId);

				}

				SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				String fechaFur = sdf.format(fechaRevision);

				generadorInforme.agregarInformacionRevision(informe, prueba.getRevision(), fechaFur, jefeTecnico);

				List<Medida> medidas = pruebaService.obtenerMedidas(prueba);
				List<Defecto> defectos = pruebaService.obtenerDefectos(prueba);

				cargarKilometraje(revision.getRevisionId(), informe);
				List<Equipo> equipos = pruebaService.obtenerEquipos(prueba);

				generadorInforme.agregarInformacionEquipos(equipos, informe);

				Boolean dosTiempos = prueba.getRevision().getVehiculo().getTiemposMotor().equals(2);

				ResultadoOttoMotocicletasDTO resultado = generadorResultados.generarResultadoOttoMotos(medidas,
						defectos, dosTiempos);

				generadorInforme.cargarInformacionResultado(informe, resultado);

				ResultadoOttoInformeDTO r2 = generadorResultados.generarResultadoOttoInforme(medidas, defectos);

				generadorInforme.cargarInformacionResultadoAdicional(informe, r2);

				generadorInforme.cargarInformacionMedidasVehiculo(informe, medidas);

				if (prueba.getMotivoAborto() != null) {
					generadorInforme.cargarMedicionesNoCorregidas(informe, prueba.getMotivoAborto());
				}

				CausaRechazoMotocicletas causa = null;
				try {
					if (!prueba.isAbortada() && !prueba.isAprobada()) {
						causa = determinadorRechazo.determinarCausaRechazo(resultado, prueba, medidas, vehiculo);
						if (causa != null) {
							if (causa.equals(CausaRechazoMotocicletas.HC_CUATRO_TIEMPOS)
									|| causa.equals(CausaRechazoMotocicletas.CO_CUATRO_TIEMPOS)
									|| causa.equals(CausaRechazoMotocicletas.HC_DOS_TIEMPOS_2010_MAYOR)
									|| causa.equals(CausaRechazoMotocicletas.CO_DOS_TIEMPOS_MENOR_2009)
									|| causa.equals(CausaRechazoMotocicletas.HC_DOS_TIEMPOS_MENOR_2009)
									|| causa.equals(CausaRechazoMotocicletas.CO_DOS_TIEMPOS_2010_MAYOR)) {
								informe.setIncumpimientoNivelesEmisiones("SI");
							} else {// el rechazo no es por emisiones contamintantes
								informe.setIncumpimientoNivelesEmisiones("NO");
								generadorInforme.cargarMedicionesNoCorregidas(informe, "");
							}

						}

					} else {
						informe.setIncumpimientoNivelesEmisiones("NO");
					}
				} catch (Exception exc) {
					logger.log(Level.SEVERE, "Error determinando causa de rechaxo puega id" + prueba.getPruebaId(),
							exc);
					continue;
				}

				String resultadoFinal = prueba.isAprobada() ? "Aprobado" : "Reprobado";
				resultadoFinal = prueba.isAbortada() ? "Abortado" : resultadoFinal;

				informe.setResultadoFinalPrueba(resultadoFinal);
				logger.log(Level.INFO, "Terminada la prueba : " + prueba.getPruebaId());
				datos.add(informe);
			}
			return datos;
		} catch (Exception exc) {
			exc.printStackTrace();
			logger.log(Level.SEVERE, "Error cargando prueba metodo dos", exc);
			return null;
		}

	}

	public Date getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	private String cargarPruebaFugas(Date fechaPrueba, String serial) {
		LocalDate fecha = Instant.ofEpochMilli(fechaPrueba.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
		LocalDateTime startOfDay = fecha.atStartOfDay();
		LocalDateTime endOfDay = LocalTime.MAX.atDate(fecha);

		Instant instant = startOfDay.atZone(ZoneId.systemDefault()).toInstant();
		Date dateInicio = Date.from(instant);

		Instant instantEnd = endOfDay.atZone(ZoneId.systemDefault()).toInstant();
		Date dateFin = Date.from(instantEnd);
		PruebaFuga pf = pruebaFugaAprobadaService.consultarPruebaFugaPorFecha(dateInicio, dateFin, serial);
		if (pf != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			return sdf.format(pf.getFechaRealizacion());
		} else {
			return "";
		}

	}

	private void cargarKilometraje(Integer revisionId, InformeMotocicletasRes0762 informe) {

		DecimalFormat df = new DecimalFormat("#");
		Prueba pruebaIv = revisionService.buscarPruebaPorTipoPorRevision(revisionId, 1);
		if (pruebaIv != null) {

			List<Medida> medidasIv = pruebaService.obtenerMedidas(pruebaIv);
			Optional<Medida> medidaKilometraje = medidasIv.stream().filter(
					m -> m.getTipoMedida().getTipoMedidaId().equals(ConstantesMedidasKilometraje.KILOMETRAJE_MEDIDA))
					.findAny();
			if (medidaKilometraje.isPresent()) {

				Double kilometraje = medidaKilometraje.get().getValor();
				if (kilometraje >= 0) {
					informe.setKilometraje(df.format(kilometraje));
				} else {
					informe.setKilometraje("NO FUNCIONAL");
				}

			} else {

				Prueba pruebaFrenos = revisionService.buscarPruebaPorTipoPorRevision(revisionId, 5);
				if (pruebaFrenos != null) {

					List<Medida> medidas = pruebaService.obtenerMedidas(pruebaIv);
					Optional<Medida> medidaKilom = medidas.stream().filter(m -> m.getTipoMedida().getTipoMedidaId()
							.equals(ConstantesMedidasKilometraje.KILOMETRAJE_MEDIDA)).findAny();
					if (medidaKilom.isPresent()) {

						Double kilometraje = medidaKilom.get().getValor();
						if (kilometraje >= 0) {
							informe.setKilometraje(df.format(kilometraje));
						} else {
							informe.setKilometraje("NO FUNCIONAL");
						}

					} else {

					}

				}

			}

		}

	}

	@Override
	public int count(Map<String, FilterMeta> filterBy) {
		return 0;
	}

}
