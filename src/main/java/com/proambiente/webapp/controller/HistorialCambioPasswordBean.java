package com.proambiente.webapp.controller;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import org.omnifaces.cdi.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.proambiente.modelo.Bitacora;
import com.proambiente.modelo.CambioPassword;
import com.proambiente.modelo.Usuario;
import com.proambiente.webapp.dao.BitacoraFacade;
import com.proambiente.webapp.dao.CambioPasswordFacade;
import com.proambiente.webapp.dao.UsuarioFacade;
import com.proambiente.webapp.util.UtilErrores;

@Named
@ViewScoped
public class HistorialCambioPasswordBean implements Serializable{
	
	private static final Logger logger = Logger.getLogger(HistorialCambioPasswordBean.class
			.getName());
	private static final long serialVersionUID = 1L;
	
	@Inject
	CambioPasswordFacade cambioPasswordFacade;
	
	@Inject
	UtilErrores utilErrores;
	
	@Inject
	UsuarioFacade usuarioFacade;

	
	private List<CambioPassword> historial ;
	private Long documentoUsuario;	

	@PostConstruct
	public void init(){
		historial = new ArrayList<>();
	}


	


	public void consultar(ActionEvent ae){
		try{			
			logger.log(Level.INFO,"Consultar cambio de password para " + documentoUsuario);
			Date date = new Date();
			
			
			Optional<Usuario> optUsuario = usuarioFacade.buscarUsuarioPorDocumento(documentoUsuario);
			if(optUsuario.isPresent()) {
				historial = cambioPasswordFacade.consultarHistorial( optUsuario.get().getUsuarioId() );				
			}else {
				utilErrores.addInfo("Usuario no encontrado");
			}						
			utilErrores.addInfo("Consulta ok");
			
		}catch(Exception exc){
			logger.log(Level.SEVERE, "Error historial cambio password " ,exc);
			utilErrores.addError("Error historial cambio password " );
		}		
	}





	public List<CambioPassword> getHistorial() {
		return historial;
	}





	public void setHistorial(List<CambioPassword> historial) {
		this.historial = historial;
	}





	public Long getDocumentoUsuario() {
		return documentoUsuario;
	}





	public void setDocumentoUsuario(Long documentoUsuario) {
		this.documentoUsuario = documentoUsuario;
	}
	
	

}
