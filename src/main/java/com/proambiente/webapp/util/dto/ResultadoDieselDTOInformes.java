package com.proambiente.webapp.util.dto;


public class ResultadoDieselDTOInformes {
	
	protected String rpmRalenti;
	protected String temperaturaFinal;
	
	protected String rpmGobernadaCiclo0;
	protected String rpmGobernadaCiclo1;
	protected String rpmGobernadaCiclo3;
	protected String rpmGobernadaCiclo2;
	
	protected String rpmRalentiCiclo0;
	protected String rpmRalentiCiclo1;
	protected String rpmRalentiCiclo2;
	protected String rpmRalentiCiclo3;
	
	protected String densidadHumoCiclo0;
	protected String densidadHumoCiclo1;
	protected String densidadHumoCiclo2;
	protected String densidadHumoCiclo3;
	protected String densidadHumoFinal;
	
	protected String temperaturaAmbiental;
	protected String humedadRelativa;
	
	protected String velocidadNoAlcanzada5Seg;
	protected String malFuncionamientoMotor;
	protected String incumplimientoNiveles;
	protected String fugasTuvoUnionesMultipleSilenciador;
	protected String salidasAdicionalesDisenio;
	protected String ausenciaTaponesDeAceite;
	protected String ausenciaTaponCombustible;
	protected String instalacionAccesoriosODeformaciones;
	protected String incorrectaOperacionSistemaRefrigeracion;
	protected String ausenciaDeFiltroAire;
	protected String instalacionDispositivosAlteranRpms;
	protected String diferenciasAritmeticas;
	protected String fallaSubitaMotor;
	protected String gobernadorNoLimitaRevoluciones;
	protected String inestabilidadRevoluciones;
	protected String diferenciaTemperaturas;
	
	
	
	
	public String getRpmRalenti() {
		return rpmRalenti;
	}
	public void setRpmRalenti(String rpmRalenti) {
		this.rpmRalenti = rpmRalenti;
	}
	public String getTemperaturaFinal() {
		return temperaturaFinal;
	}
	public void setTemperaturaFinal(String temperaturaFinal) {
		this.temperaturaFinal = temperaturaFinal;
	}
	public String getRpmGobernadaCiclo0() {
		return rpmGobernadaCiclo0;
	}
	public void setRpmGobernadaCiclo0(String rpmGobernadaCiclo0) {
		this.rpmGobernadaCiclo0 = rpmGobernadaCiclo0;
	}
	public String getRpmGobernadaCiclo1() {
		return rpmGobernadaCiclo1;
	}
	public void setRpmGobernadaCiclo1(String rpmGobernadaCiclo1) {
		this.rpmGobernadaCiclo1 = rpmGobernadaCiclo1;
	}
	public String getRpmGobernadaCiclo3() {
		return rpmGobernadaCiclo3;
	}
	public void setRpmGobernadaCiclo3(String rpmGobernadaCiclo3) {
		this.rpmGobernadaCiclo3 = rpmGobernadaCiclo3;
	}
	public String getRpmGobernadaCiclo2() {
		return rpmGobernadaCiclo2;
	}
	public void setRpmGobernadaCiclo2(String rpmGobernadaCiclo4) {
		this.rpmGobernadaCiclo2 = rpmGobernadaCiclo4;
	}
	public String getMalFuncionamientoMotor() {
		return malFuncionamientoMotor;
	}
	public void setMalFuncionamientoMotor(String malFuncionamientoMotor) {
		this.malFuncionamientoMotor = malFuncionamientoMotor;
	}
	public String getIncumplimientoNiveles() {
		return incumplimientoNiveles;
	}
	public void setIncumplimientoNiveles(String incumplimientoNiveles) {
		this.incumplimientoNiveles = incumplimientoNiveles;
	}
	public String getFugasTuvoUnionesMultipleSilenciador() {
		return fugasTuvoUnionesMultipleSilenciador;
	}
	public void setFugasTuvoUnionesMultipleSilenciador(String fugasTuvoUnionesMultipleSilenciador) {
		this.fugasTuvoUnionesMultipleSilenciador = fugasTuvoUnionesMultipleSilenciador;
	}
	public String getSalidasAdicionalesDisenio() {
		return salidasAdicionalesDisenio;
	}
	public void setSalidasAdicionalesDisenio(String salidasAdicionalesDisenio) {
		this.salidasAdicionalesDisenio = salidasAdicionalesDisenio;
	}
	public String getAusenciaTaponesDeAceite() {
		return ausenciaTaponesDeAceite;
	}
	public void setAusenciaTaponesDeAceite(String ausenciaTaponesDeAceite) {
		this.ausenciaTaponesDeAceite = ausenciaTaponesDeAceite;
	}
	public String getAusenciaTaponCombustible() {
		return ausenciaTaponCombustible;
	}
	public void setAusenciaTaponCombustible(String ausenciaTaponCombustible) {
		this.ausenciaTaponCombustible = ausenciaTaponCombustible;
	}
	public String getInstalacionAccesoriosODeformaciones() {
		return instalacionAccesoriosODeformaciones;
	}
	public void setInstalacionAccesoriosODeformaciones(String instalacionAccesoriosODeformaciones) {
		this.instalacionAccesoriosODeformaciones = instalacionAccesoriosODeformaciones;
	}
	public String getIncorrectaOperacionSistemaRefrigeracion() {
		return incorrectaOperacionSistemaRefrigeracion;
	}
	public void setIncorrectaOperacionSistemaRefrigeracion(String incorrectaOperacionSistemaRefrigeracion) {
		this.incorrectaOperacionSistemaRefrigeracion = incorrectaOperacionSistemaRefrigeracion;
	}
	public String getAusenciaDeFiltroAire() {
		return ausenciaDeFiltroAire;
	}
	public void setAusenciaDeFiltroAire(String ausenciaDeFiltroAire) {
		this.ausenciaDeFiltroAire = ausenciaDeFiltroAire;
	}
	public String getInstalacionDispositivosAlteranRpms() {
		return instalacionDispositivosAlteranRpms;
	}
	public void setInstalacionDispositivosAlteranRpms(String instalacionDispositivosAlteranRpms) {
		this.instalacionDispositivosAlteranRpms = instalacionDispositivosAlteranRpms;
	}
	public String getDiferenciasAritmeticas() {
		return diferenciasAritmeticas;
	}
	public void setDiferenciasAritmeticas(String diferenciasAritmeticas) {
		this.diferenciasAritmeticas = diferenciasAritmeticas;
	}
	public String getFallaSubitaMotor() {
		return fallaSubitaMotor;
	}
	public void setFallaSubitaMotor(String fallaSubitaMotor) {
		this.fallaSubitaMotor = fallaSubitaMotor;
	}
	public String getVelocidadNoAlcanzada5Seg() {
		return velocidadNoAlcanzada5Seg;
	}
	public void setVelocidadNoAlcanzada5Seg(String velocidadNoAlcanzada5Seg) {
		this.velocidadNoAlcanzada5Seg = velocidadNoAlcanzada5Seg;
	}
	public String getTemperaturaAmbiental() {
		return temperaturaAmbiental;
	}
	public void setTemperaturaAmbiental(String temperaturaAmbiental) {
		this.temperaturaAmbiental = temperaturaAmbiental;
	}
	public String getHumedadRelativa() {
		return humedadRelativa;
	}
	public void setHumedadRelativa(String humedadRelativa) {
		this.humedadRelativa = humedadRelativa;
	}
	public String getGobernadorNoLimitaRevoluciones() {
		return gobernadorNoLimitaRevoluciones;
	}
	public void setGobernadorNoLimitaRevoluciones(String gobernadorNoLimitaRevoluciones) {
		this.gobernadorNoLimitaRevoluciones = gobernadorNoLimitaRevoluciones;
	}
	public String getInestabilidadRevoluciones() {
		return inestabilidadRevoluciones;
	}
	public void setInestabilidadRevoluciones(String inestabilidadRevoluciones) {
		this.inestabilidadRevoluciones = inestabilidadRevoluciones;
	}
	public String getDiferenciaTemperaturas() {
		return diferenciaTemperaturas;
	}
	public void setDiferenciaTemperaturas(String diferenciaTemperaturas) {
		this.diferenciaTemperaturas = diferenciaTemperaturas;
	}
	public String getRpmRalentiCiclo0() {
		return rpmRalentiCiclo0;
	}
	public void setRpmRalentiCiclo0(String rpmRalentiCiclo0) {
		this.rpmRalentiCiclo0 = rpmRalentiCiclo0;
	}
	public String getRpmRalentiCiclo1() {
		return rpmRalentiCiclo1;
	}
	public void setRpmRalentiCiclo1(String rpmRalentiCiclo1) {
		this.rpmRalentiCiclo1 = rpmRalentiCiclo1;
	}
	public String getRpmRalentiCiclo2() {
		return rpmRalentiCiclo2;
	}
	public void setRpmRalentiCiclo2(String rpmRalentiCiclo2) {
		this.rpmRalentiCiclo2 = rpmRalentiCiclo2;
	}
	public String getRpmRalentiCiclo3() {
		return rpmRalentiCiclo3;
	}
	public void setRpmRalentiCiclo3(String rpmRalentiCiclo3) {
		this.rpmRalentiCiclo3 = rpmRalentiCiclo3;
	}
	public String getDensidadHumoCiclo0() {
		return densidadHumoCiclo0;
	}
	public void setDensidadHumoCiclo0(String densidadHumoCiclo0) {
		this.densidadHumoCiclo0 = densidadHumoCiclo0;
	}
	public String getDensidadHumoCiclo1() {
		return densidadHumoCiclo1;
	}
	public void setDensidadHumoCiclo1(String densidadHumoCiclo1) {
		this.densidadHumoCiclo1 = densidadHumoCiclo1;
	}
	public String getDensidadHumoCiclo2() {
		return densidadHumoCiclo2;
	}
	public void setDensidadHumoCiclo2(String densidadHumoCiclo2) {
		this.densidadHumoCiclo2 = densidadHumoCiclo2;
	}
	public String getDensidadHumoCiclo3() {
		return densidadHumoCiclo3;
	}
	public void setDensidadHumoCiclo3(String densidadHumoCiclo3) {
		this.densidadHumoCiclo3 = densidadHumoCiclo3;
	}
	public String getDensidadHumoFinal() {
		return densidadHumoFinal;
	}
	public void setDensidadHumoFinal(String densidadHumoFinal) {
		this.densidadHumoFinal = densidadHumoFinal;
	}
	
	

}
