package com.proambiente.webapp.util.dto;

public class InformacionRevisionDTO {
	
	private String numeroCertificado;
	private String fechaExpedicion;
	private String fechaVencimiento;
	private String tipoIdentificacionCda;
	private String identificacionCda;
	private String divipo;
	private String sucursal;
	private String claseCda;
	
	private String placa;
	private String claseVehiculo;
	private String servicio;
	private String marca;
	private String linea;
	private String modelo;
	private String color;
	private String tipoCombustible;
	private String motor;
	private String vin;
	private String cilindraje;
	private String numeroSoat;
	private String fechaExpedicionSoat;
	private String fechaVencimientoSoat;
	private String aseguradora;
	private String numeroLicenciaTransito;
	private String tipoIdentificacionPropietario;
	private String identificacionPropietario;
	private String nombrePropietario;
	private String direccionPropietario;
	private String telefonoPropietario;
	private String numeroLicenciaConduccion;
	private String categoriaLicenciaConduccion;
	private String tipoIdentificacionConductor;
	private String identificacionConductor;
	private String nombreConductor;
	private String direccionConductor;
	private String telefonoConductor;
	private String estadoRevision;
	private String fechaCargue;
	public String getNumeroCertificado() {
		return numeroCertificado;
	}
	public void setNumeroCertificado(String numeroCertificado) {
		this.numeroCertificado = numeroCertificado;
	}
	public String getFechaExpedicion() {
		return fechaExpedicion;
	}
	public void setFechaExpedicion(String fechaExpedicion) {
		this.fechaExpedicion = fechaExpedicion;
	}
	public String getFechaVencimiento() {
		return fechaVencimiento;
	}
	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
	public String getTipoIdentificacionCda() {
		return tipoIdentificacionCda;
	}
	public void setTipoIdentificacionCda(String tipoIdentificacionCda) {
		this.tipoIdentificacionCda = tipoIdentificacionCda;
	}
	public String getIdentificacionCda() {
		return identificacionCda;
	}
	public void setIdentificacionCda(String identificacionCda) {
		this.identificacionCda = identificacionCda;
	}
	public String getDivipo() {
		return divipo;
	}
	public void setDivipo(String divipo) {
		this.divipo = divipo;
	}
	public String getSucursal() {
		return sucursal;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}
	public String getClaseCda() {
		return claseCda;
	}
	public void setClaseCda(String claseCda) {
		this.claseCda = claseCda;
	}
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public String getClaseVehiculo() {
		return claseVehiculo;
	}
	public void setClaseVehiculo(String claseVehiculo) {
		this.claseVehiculo = claseVehiculo;
	}
	public String getServicio() {
		return servicio;
	}
	public void setServicio(String servicio) {
		this.servicio = servicio;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getLinea() {
		return linea;
	}
	public void setLinea(String linea) {
		this.linea = linea;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getTipoCombustible() {
		return tipoCombustible;
	}
	public void setTipoCombustible(String tipoCombustible) {
		this.tipoCombustible = tipoCombustible;
	}
	public String getMotor() {
		return motor;
	}
	public void setMotor(String motor) {
		this.motor = motor;
	}
	public String getVin() {
		return vin;
	}
	public void setVin(String vin) {
		this.vin = vin;
	}
	public String getCilindraje() {
		return cilindraje;
	}
	public void setCilindraje(String cilindraje) {
		this.cilindraje = cilindraje;
	}
	public String getNumeroSoat() {
		return numeroSoat;
	}
	public void setNumeroSoat(String numeroSoat) {
		this.numeroSoat = numeroSoat;
	}
	public String getFechaExpedicionSoat() {
		return fechaExpedicionSoat;
	}
	public void setFechaExpedicionSoat(String fechaExpedicionSoat) {
		this.fechaExpedicionSoat = fechaExpedicionSoat;
	}
	public String getFechaVencimientoSoat() {
		return fechaVencimientoSoat;
	}
	public void setFechaVencimientoSoat(String fechaVencimientoSoat) {
		this.fechaVencimientoSoat = fechaVencimientoSoat;
	}
	public String getAseguradora() {
		return aseguradora;
	}
	public void setAseguradora(String aseguradora) {
		this.aseguradora = aseguradora;
	}
	public String getNumeroLicenciaTransito() {
		return numeroLicenciaTransito;
	}
	public void setNumeroLicenciaTransito(String numeroLicenciaTransito) {
		this.numeroLicenciaTransito = numeroLicenciaTransito;
	}
	public String getTipoIdentificacionPropietario() {
		return tipoIdentificacionPropietario;
	}
	public void setTipoIdentificacionPropietario(String tipoIdentificacionPropietario) {
		this.tipoIdentificacionPropietario = tipoIdentificacionPropietario;
	}
	public String getIdentificacionPropietario() {
		return identificacionPropietario;
	}
	public void setIdentificacionPropietario(String identificacionPropietario) {
		this.identificacionPropietario = identificacionPropietario;
	}
	public String getNombrePropietario() {
		return nombrePropietario;
	}
	public void setNombrePropietario(String nombrePropietario) {
		this.nombrePropietario = nombrePropietario;
	}
	public String getDireccionPropietario() {
		return direccionPropietario;
	}
	public void setDireccionPropietario(String direccionPropietario) {
		this.direccionPropietario = direccionPropietario;
	}
	public String getTelefonoPropietario() {
		return telefonoPropietario;
	}
	public void setTelefonoPropietario(String telefonoPropietario) {
		this.telefonoPropietario = telefonoPropietario;
	}
	public String getNumeroLicenciaConduccion() {
		return numeroLicenciaConduccion;
	}
	public void setNumeroLicenciaConduccion(String numeroLicenciaConduccion) {
		this.numeroLicenciaConduccion = numeroLicenciaConduccion;
	}
	public String getCategoriaLicenciaConduccion() {
		return categoriaLicenciaConduccion;
	}
	public void setCategoriaLicenciaConduccion(String categoriaLicenciaConduccion) {
		this.categoriaLicenciaConduccion = categoriaLicenciaConduccion;
	}
	public String getTipoIdentificacionConductor() {
		return tipoIdentificacionConductor;
	}
	public void setTipoIdentificacionConductor(String tipoIdentificacionConductor) {
		this.tipoIdentificacionConductor = tipoIdentificacionConductor;
	}
	public String getNombreConductor() {
		return nombreConductor;
	}
	public void setNombreConductor(String nombreConductor) {
		this.nombreConductor = nombreConductor;
	}
	public String getDireccionConductor() {
		return direccionConductor;
	}
	public void setDireccionConductor(String direccionConductor) {
		this.direccionConductor = direccionConductor;
	}
	public String getTelefonoConductor() {
		return telefonoConductor;
	}
	public void setTelefonoConductor(String telefonoConductor) {
		this.telefonoConductor = telefonoConductor;
	}
	public String getEstadoRevision() {
		return estadoRevision;
	}
	public void setEstadoRevision(String estadoRevision) {
		this.estadoRevision = estadoRevision;
	}
	public String getFechaCargue() {
		return fechaCargue;
	}
	public void setFechaCargue(String fechaCargue) {
		this.fechaCargue = fechaCargue;
	}
	public String getIdentificacionConductor() {
		return identificacionConductor;
	}
	public void setIdentificacionConductor(String identificacionConductor) {
		this.identificacionConductor = identificacionConductor;
	}
	
	
	
	
	
	
	
	
	

}
