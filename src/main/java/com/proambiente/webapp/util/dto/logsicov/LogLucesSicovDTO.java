package com.proambiente.webapp.util.dto.logsicov;

public class LogLucesSicovDTO {

	  private String derBajaIntensidadValor1= ""; 
	  private String derBajaIntensidadValor2= ""; 
	  private String derBajaIntensidadValor3= ""; 
	  private String derBajaSimultaneas= ""; 
	  private String izqBajaIntensidadValor1= ""; 
	  private String izqBajaIntensidadValor2= ""; 
	  private String izqBajaIntensidaValor3= ""; 
	  private String IzqBajaSimultaneas= ""; 
	  private String derBajaInclinacionValor1= ""; 
	  private String derBajaInclinacionValor2= ""; 
	  private String derBajaInclinacionValor3= ""; 
	  private String izqBajaInclinacionValor1= ""; 
	  private String izqBajaInclinacionValor2= ""; 
	  private String izqBajaInclinacionValor3= ""; 
	  private String sumatoriaIntensidad= ""; 
	  private String derAltaIntensidadValor1= ""; 
	  private String derAltaIntensidadValor2= ""; 
	  private String derAltaIntensidadValor3= ""; 
	  private String derAltasSimultaneas= ""; 
	  private String izqAltaIntesidadValor1= ""; 
	  private String izqAltaIntesidadValor2= ""; 
	  private String izqAltaIntesidadValor3= ""; 
	  private String izqAltasSimultaneas= ""; 
	  private String derExplorardorasValor1= ""; 
	  private String derExplorardorasValor2= ""; 
	  private String derExplorardorasValor3= ""; 
	  private String derExploradorasSimultaneas= ""; 
	  private String izqExplorardorasValor1= ""; 
	  private String izqExplorardorasValor2= ""; 
	  private String izqExplorardorasValor3= ""; 
	  private String izqExploradorasSimultaneas= ""; 
	  private String tablaAfectada= ""; 
	  private String idRegistro= "";
	public String getDerBajaIntensidadValor1() {
		return derBajaIntensidadValor1;
	}
	public void setDerBajaIntensidadValor1(String derBajaIntensidadValor1) {
		this.derBajaIntensidadValor1 = derBajaIntensidadValor1;
	}
	public String getDerBajaIntensidadValor2() {
		return derBajaIntensidadValor2;
	}
	public void setDerBajaIntensidadValor2(String derBajaIntensidadValor2) {
		this.derBajaIntensidadValor2 = derBajaIntensidadValor2;
	}
	public String getDerBajaIntensidadValor3() {
		return derBajaIntensidadValor3;
	}
	public void setDerBajaIntensidadValor3(String derBajaIntensidadValor3) {
		this.derBajaIntensidadValor3 = derBajaIntensidadValor3;
	}
	public String getDerBajaSimultaneas() {
		return derBajaSimultaneas;
	}
	public void setDerBajaSimultaneas(String derBajaSimultaneas) {
		this.derBajaSimultaneas = derBajaSimultaneas;
	}
	public String getIzqBajaIntensidadValor1() {
		return izqBajaIntensidadValor1;
	}
	public void setIzqBajaIntensidadValor1(String izqBajaIntensidadValor1) {
		this.izqBajaIntensidadValor1 = izqBajaIntensidadValor1;
	}
	public String getIzqBajaIntensidadValor2() {
		return izqBajaIntensidadValor2;
	}
	public void setIzqBajaIntensidadValor2(String izqBajaIntensidadValor2) {
		this.izqBajaIntensidadValor2 = izqBajaIntensidadValor2;
	}
	public String getIzqBajaIntensidaValor3() {
		return izqBajaIntensidaValor3;
	}
	public void setIzqBajaIntensidaValor3(String izqBajaIntensidaValor3) {
		this.izqBajaIntensidaValor3 = izqBajaIntensidaValor3;
	}
	public String getIzqBajaSimultaneas() {
		return IzqBajaSimultaneas;
	}
	public void setIzqBajaSimultaneas(String izqBajaSimultaneas) {
		IzqBajaSimultaneas = izqBajaSimultaneas;
	}
	public String getDerBajaInclinacionValor1() {
		return derBajaInclinacionValor1;
	}
	public void setDerBajaInclinacionValor1(String derBajaInclinacionValor1) {
		this.derBajaInclinacionValor1 = derBajaInclinacionValor1;
	}
	public String getDerBajaInclinacionValor2() {
		return derBajaInclinacionValor2;
	}
	public void setDerBajaInclinacionValor2(String derBajaInclinacionValor2) {
		this.derBajaInclinacionValor2 = derBajaInclinacionValor2;
	}
	public String getDerBajaInclinacionValor3() {
		return derBajaInclinacionValor3;
	}
	public void setDerBajaInclinacionValor3(String derBajaInclinacionValor3) {
		this.derBajaInclinacionValor3 = derBajaInclinacionValor3;
	}
	public String getIzqBajaInclinacionValor1() {
		return izqBajaInclinacionValor1;
	}
	public void setIzqBajaInclinacionValor1(String izqBajaInclinacionValor1) {
		this.izqBajaInclinacionValor1 = izqBajaInclinacionValor1;
	}
	public String getIzqBajaInclinacionValor2() {
		return izqBajaInclinacionValor2;
	}
	public void setIzqBajaInclinacionValor2(String izqBajaInclinacionValor2) {
		this.izqBajaInclinacionValor2 = izqBajaInclinacionValor2;
	}
	public String getIzqBajaInclinacionValor3() {
		return izqBajaInclinacionValor3;
	}
	public void setIzqBajaInclinacionValor3(String izqBajaInclinacionValor3) {
		this.izqBajaInclinacionValor3 = izqBajaInclinacionValor3;
	}
	public String getSumatoriaIntensidad() {
		return sumatoriaIntensidad;
	}
	public void setSumatoriaIntensidad(String sumatoriaIntensidad) {
		this.sumatoriaIntensidad = sumatoriaIntensidad;
	}
	public String getDerAltaIntensidadValor1() {
		return derAltaIntensidadValor1;
	}
	public void setDerAltaIntensidadValor1(String derAltaIntensidadValor1) {
		this.derAltaIntensidadValor1 = derAltaIntensidadValor1;
	}
	public String getDerAltaIntensidadValor2() {
		return derAltaIntensidadValor2;
	}
	public void setDerAltaIntensidadValor2(String derAltaIntensidadValor2) {
		this.derAltaIntensidadValor2 = derAltaIntensidadValor2;
	}
	public String getDerAltaIntensidadValor3() {
		return derAltaIntensidadValor3;
	}
	public void setDerAltaIntensidadValor3(String derAltaIntensidadValor3) {
		this.derAltaIntensidadValor3 = derAltaIntensidadValor3;
	}
	public String getDerAltasSimultaneas() {
		return derAltasSimultaneas;
	}
	public void setDerAltasSimultaneas(String derAltasSimultaneas) {
		this.derAltasSimultaneas = derAltasSimultaneas;
	}
	public String getIzqAltaIntesidadValor1() {
		return izqAltaIntesidadValor1;
	}
	public void setIzqAltaIntesidadValor1(String izqAltaIntesidadValor1) {
		this.izqAltaIntesidadValor1 = izqAltaIntesidadValor1;
	}
	public String getIzqAltaIntesidadValor2() {
		return izqAltaIntesidadValor2;
	}
	public void setIzqAltaIntesidadValor2(String izqAltaIntesidadValor2) {
		this.izqAltaIntesidadValor2 = izqAltaIntesidadValor2;
	}
	public String getIzqAltaIntesidadValor3() {
		return izqAltaIntesidadValor3;
	}
	public void setIzqAltaIntesidadValor3(String izqAltaIntesidadValor3) {
		this.izqAltaIntesidadValor3 = izqAltaIntesidadValor3;
	}
	public String getIzqAltasSimultaneas() {
		return izqAltasSimultaneas;
	}
	public void setIzqAltasSimultaneas(String izqAltasSimultaneas) {
		this.izqAltasSimultaneas = izqAltasSimultaneas;
	}
	public String getDerExplorardorasValor1() {
		return derExplorardorasValor1;
	}
	public void setDerExplorardorasValor1(String derExplorardorasValor1) {
		this.derExplorardorasValor1 = derExplorardorasValor1;
	}
	public String getDerExplorardorasValor2() {
		return derExplorardorasValor2;
	}
	public void setDerExplorardorasValor2(String derExplorardorasValor2) {
		this.derExplorardorasValor2 = derExplorardorasValor2;
	}
	public String getDerExplorardorasValor3() {
		return derExplorardorasValor3;
	}
	public void setDerExplorardorasValor3(String derExplorardorasValor3) {
		this.derExplorardorasValor3 = derExplorardorasValor3;
	}
	public String getDerExploradorasSimultaneas() {
		return derExploradorasSimultaneas;
	}
	public void setDerExploradorasSimultaneas(String derExploradorasSimultaneas) {
		this.derExploradorasSimultaneas = derExploradorasSimultaneas;
	}
	public String getIzqExplorardorasValor1() {
		return izqExplorardorasValor1;
	}
	public void setIzqExplorardorasValor1(String izqExplorardorasValor1) {
		this.izqExplorardorasValor1 = izqExplorardorasValor1;
	}
	public String getIzqExplorardorasValor2() {
		return izqExplorardorasValor2;
	}
	public void setIzqExplorardorasValor2(String izqExplorardorasValor2) {
		this.izqExplorardorasValor2 = izqExplorardorasValor2;
	}
	public String getIzqExplorardorasValor3() {
		return izqExplorardorasValor3;
	}
	public void setIzqExplorardorasValor3(String izqExplorardorasValor3) {
		this.izqExplorardorasValor3 = izqExplorardorasValor3;
	}
	public String getIzqExploradorasSimultaneas() {
		return izqExploradorasSimultaneas;
	}
	public void setIzqExploradorasSimultaneas(String izqExploradorasSimultaneas) {
		this.izqExploradorasSimultaneas = izqExploradorasSimultaneas;
	}
	public String getTablaAfectada() {
		return tablaAfectada;
	}
	public void setTablaAfectada(String tablaAfectada) {
		this.tablaAfectada = tablaAfectada;
	}
	public String getIdRegistro() {
		return idRegistro;
	}
	public void setIdRegistro(String idRegistro) {
		this.idRegistro = idRegistro;
	} 
	  
	  
}
