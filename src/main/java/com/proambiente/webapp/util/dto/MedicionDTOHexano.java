package com.proambiente.webapp.util.dto;

import java.math.BigDecimal;
import java.util.Date;

public class MedicionDTOHexano {
	
	private Date fecha;
	private Integer HC;
	private Integer HCHexano;
	private BigDecimal CO;
	private BigDecimal CO2;
	private BigDecimal O2;
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public Integer getHC() {
		return HC;
	}
	public void setHC(Integer hC) {
		HC = hC;
	}
	public Integer getHCHexano() {
		return HCHexano;
	}
	public void setHCHexano(Integer hCHexano) {
		HCHexano = hCHexano;
	}
	public BigDecimal getCO() {
		return CO;
	}
	public void setCO(BigDecimal cO) {
		CO = cO;
	}
	public BigDecimal getCO2() {
		return CO2;
	}
	public void setCO2(BigDecimal cO2) {
		CO2 = cO2;
	}
	public BigDecimal getOxigeno() {
		return O2;
	}
	public void setOxigeno(BigDecimal o2) {
		O2 = o2;
	}
	
	
	
}
