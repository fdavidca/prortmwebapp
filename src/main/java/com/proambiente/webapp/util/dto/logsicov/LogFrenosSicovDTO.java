package com.proambiente.webapp.util.dto.logsicov;

public class LogFrenosSicovDTO {
	
	private String eficaciaTotal ="";
	private String eficaciaAuxiliar = "";
	
	//eje1
	private String fuerzaEje1Izquierdo = "";
	private String pesoEje1Izquierdo = "";	
	private String fuerzaEje1Derecho = "";
	private String pesoEje1Derecho = "";
	private String eje1Desequilibrio = "";
	
	//eje2
	private String fuerzaEje2Izquierdo = "";
	private String pesoEje2Izquierdo = "";
	private String fuerzaEje2Derecho = "";
	private String pesoEje2Derecho = "";
	private String eje2Desequilibrio = "";

	//eje3
	private String fuerzaEje3Izquierdo = "";
	private String pesoEje3Izquierdo = "";
	private String fuerzaEje3Derecho = "";
	private String pesoEje3Derecho = "";
	private String eje3Desequilibrio = "";
	
	//eje 4
	private String fuerzaEje4Izquierdo = "";
	private String pesoEje4Izquierdo = "";
	private String fuerzaEje4Derecho = "";
	private String pesoEje4Derecho = "";
	private String eje4Desequilibrio = "";
	
	//eje5
	private String fuerzaEje5Izquierdo = "";
	private String pesoEje5Izquierdo = "";
	private String fuerzaEje5Derecho = "";
	private String pesoEje5Derecho = "";
	private String eje5Desequilibrio = "";
	
	//eje 6
	private String fuerzaEje6Izquierdo = "";
	private String pesoEje6Izquierdo = "";
	private String fuerzaEje6Derecho = "";
	private String pesoEje6Derecho = "";
	private String eje6Desequilibrio = "";
	
	//eje auxiliar
	private String derFuerzaAuxiliar = "";
	private String derFuerzaPeso = "";
	private String izqFuerzaAuxiliar = "";
	private String izqFuerzaPeso = "";
	
	
	
	private String tablaAfectada = "";
	private String idRegistro = "";
	public String getEficaciaTotal() {
		return eficaciaTotal;
	}
	public void setEficaciaTotal(String eficaciaTotal) {
		this.eficaciaTotal = eficaciaTotal;
	}
	public String getEficaciaAuxiliar() {
		return eficaciaAuxiliar;
	}
	public void setEficaciaAuxiliar(String eficaciaAuxiliar) {
		this.eficaciaAuxiliar = eficaciaAuxiliar;
	}
	public String getFuerzaEje1Izquierdo() {
		return fuerzaEje1Izquierdo;
	}
	public void setFuerzaEje1Izquierdo(String fuerzaEje1Izquierdo) {
		this.fuerzaEje1Izquierdo = fuerzaEje1Izquierdo;
	}
	public String getPesoEje1Izquierdo() {
		return pesoEje1Izquierdo;
	}
	public void setPesoEje1Izquierdo(String pesoEje1Izquierdo) {
		this.pesoEje1Izquierdo = pesoEje1Izquierdo;
	}
	public String getFuerzaEje1Derecho() {
		return fuerzaEje1Derecho;
	}
	public void setFuerzaEje1Derecho(String fuerzaEje1Derecho) {
		this.fuerzaEje1Derecho = fuerzaEje1Derecho;
	}
	public String getPesoEje1Derecho() {
		return pesoEje1Derecho;
	}
	public void setPesoEje1Derecho(String pesoEje1Derecho) {
		this.pesoEje1Derecho = pesoEje1Derecho;
	}
	public String getEje1Desequilibrio() {
		return eje1Desequilibrio;
	}
	public void setEje1Desequilibrio(String eje1Desequilibrio) {
		this.eje1Desequilibrio = eje1Desequilibrio;
	}
	public String getFuerzaEje2Izquierdo() {
		return fuerzaEje2Izquierdo;
	}
	public void setFuerzaEje2Izquierdo(String fuerzaEje2Izquierdo) {
		this.fuerzaEje2Izquierdo = fuerzaEje2Izquierdo;
	}
	public String getPesoEje2Izquierdo() {
		return pesoEje2Izquierdo;
	}
	public void setPesoEje2Izquierdo(String pesoEje2Izquierdo) {
		this.pesoEje2Izquierdo = pesoEje2Izquierdo;
	}
	public String getFuerzaEje2Derecho() {
		return fuerzaEje2Derecho;
	}
	public void setFuerzaEje2Derecho(String fuerzaEje2Derecho) {
		this.fuerzaEje2Derecho = fuerzaEje2Derecho;
	}
	public String getPesoEje2Derecho() {
		return pesoEje2Derecho;
	}
	public void setPesoEje2Derecho(String pesoEje2Derecho) {
		this.pesoEje2Derecho = pesoEje2Derecho;
	}
	public String getEje2Desequilibrio() {
		return eje2Desequilibrio;
	}
	public void setEje2Desequilibrio(String eje2Desequilibrio) {
		this.eje2Desequilibrio = eje2Desequilibrio;
	}
	public String getFuerzaEje3Izquierdo() {
		return fuerzaEje3Izquierdo;
	}
	public void setFuerzaEje3Izquierdo(String fuerzaEje3Izquierdo) {
		this.fuerzaEje3Izquierdo = fuerzaEje3Izquierdo;
	}
	public String getPesoEje3Izquierdo() {
		return pesoEje3Izquierdo;
	}
	public void setPesoEje3Izquierdo(String pesoEje3Izquierdo) {
		this.pesoEje3Izquierdo = pesoEje3Izquierdo;
	}
	public String getFuerzaEje3Derecho() {
		return fuerzaEje3Derecho;
	}
	public void setFuerzaEje3Derecho(String fuerzaEje3Derecho) {
		this.fuerzaEje3Derecho = fuerzaEje3Derecho;
	}
	public String getPesoEje3Derecho() {
		return pesoEje3Derecho;
	}
	public void setPesoEje3Derecho(String pesoEje3Derecho) {
		this.pesoEje3Derecho = pesoEje3Derecho;
	}
	public String getEje3Desequilibrio() {
		return eje3Desequilibrio;
	}
	public void setEje3Desequilibrio(String eje3Desequilibrio) {
		this.eje3Desequilibrio = eje3Desequilibrio;
	}
	public String getFuerzaEje4Izquierdo() {
		return fuerzaEje4Izquierdo;
	}
	public void setFuerzaEje4Izquierdo(String fuerzaEje4Izquierdo) {
		this.fuerzaEje4Izquierdo = fuerzaEje4Izquierdo;
	}
	public String getPesoEje4Izquierdo() {
		return pesoEje4Izquierdo;
	}
	public void setPesoEje4Izquierdo(String pesoEje4Izquierdo) {
		this.pesoEje4Izquierdo = pesoEje4Izquierdo;
	}
	public String getFuerzaEje4Derecho() {
		return fuerzaEje4Derecho;
	}
	public void setFuerzaEje4Derecho(String fuerzaEje4Derecho) {
		this.fuerzaEje4Derecho = fuerzaEje4Derecho;
	}
	public String getPesoEje4Derecho() {
		return pesoEje4Derecho;
	}
	public void setPesoEje4Derecho(String pesoEje4Derecho) {
		this.pesoEje4Derecho = pesoEje4Derecho;
	}
	public String getEje4Desequilibrio() {
		return eje4Desequilibrio;
	}
	public void setEje4Desequilibrio(String eje4Desequilibrio) {
		this.eje4Desequilibrio = eje4Desequilibrio;
	}
	public String getFuerzaEje5Izquierdo() {
		return fuerzaEje5Izquierdo;
	}
	public void setFuerzaEje5Izquierdo(String fuerzaEje5Izquierdo) {
		this.fuerzaEje5Izquierdo = fuerzaEje5Izquierdo;
	}
	public String getPesoEje5Izquierdo() {
		return pesoEje5Izquierdo;
	}
	public void setPesoEje5Izquierdo(String pesoEje5Izquierdo) {
		this.pesoEje5Izquierdo = pesoEje5Izquierdo;
	}
	public String getFuerzaEje5Derecho() {
		return fuerzaEje5Derecho;
	}
	public void setFuerzaEje5Derecho(String fuerzaEje5Derecho) {
		this.fuerzaEje5Derecho = fuerzaEje5Derecho;
	}
	public String getPesoEje5Derecho() {
		return pesoEje5Derecho;
	}
	public void setPesoEje5Derecho(String pesoEje5Derecho) {
		this.pesoEje5Derecho = pesoEje5Derecho;
	}
	public String getEje5Desequilibrio() {
		return eje5Desequilibrio;
	}
	public void setEje5Desequilibrio(String eje5Desequilibrio) {
		this.eje5Desequilibrio = eje5Desequilibrio;
	}
	public String getFuerzaEje6Izquierdo() {
		return fuerzaEje6Izquierdo;
	}
	public void setFuerzaEje6Izquierdo(String fuerzaEje6Izquierdo) {
		this.fuerzaEje6Izquierdo = fuerzaEje6Izquierdo;
	}
	public String getPesoEje6Izquierdo() {
		return pesoEje6Izquierdo;
	}
	public void setPesoEje6Izquierdo(String pesoEje6Izquierdo) {
		this.pesoEje6Izquierdo = pesoEje6Izquierdo;
	}
	public String getFuerzaEje6Derecho() {
		return fuerzaEje6Derecho;
	}
	public void setFuerzaEje6Derecho(String fuerzaEje6Derecho) {
		this.fuerzaEje6Derecho = fuerzaEje6Derecho;
	}
	public String getPesoEje6Derecho() {
		return pesoEje6Derecho;
	}
	public void setPesoEje6Derecho(String pesoEje6Derecho) {
		this.pesoEje6Derecho = pesoEje6Derecho;
	}
	public String getEje6Desequilibrio() {
		return eje6Desequilibrio;
	}
	public void setEje6Desequilibrio(String eje6Desequilibrio) {
		this.eje6Desequilibrio = eje6Desequilibrio;
	}
	public String getTablaAfectada() {
		return tablaAfectada;
	}
	public void setTablaAfectada(String tablaAfectada) {
		this.tablaAfectada = tablaAfectada;
	}
	public String getIdRegistro() {
		return idRegistro;
	}
	public void setIdRegistro(String idRegistro) {
		this.idRegistro = idRegistro;
	}
	public String getDerFuerzaAuxiliar() {
		return derFuerzaAuxiliar;
	}
	public void setDerFuerzaAuxiliar(String derFuerzaAuxiliar) {
		this.derFuerzaAuxiliar = derFuerzaAuxiliar;
	}
	public String getDerFuerzaPeso() {
		return derFuerzaPeso;
	}
	public void setDerFuerzaPeso(String derFuerzaPeso) {
		this.derFuerzaPeso = derFuerzaPeso;
	}
	public String getIzqFuerzaAuxiliar() {
		return izqFuerzaAuxiliar;
	}
	public void setIzqFuerzaAuxiliar(String izqFuerzaAuxiliar) {
		this.izqFuerzaAuxiliar = izqFuerzaAuxiliar;
	}
	public String getIzqFuerzaPeso() {
		return izqFuerzaPeso;
	}
	public void setIzqFuerzaPeso(String izqFuerzaPeso) {
		this.izqFuerzaPeso = izqFuerzaPeso;
	}
	
	
	

}
