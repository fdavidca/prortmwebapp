package com.proambiente.webapp.util.dto.sda;

public enum CausaRechazoDiesel {
	
	 	SIN_RECHAZO(0),
	 	FUGAS_TUBO_ESCAPE(1),
	 	FUGAS_SILENCIADOR(2),
	 	AUSENCIA_TAPA_ACEITE(3),
		AUSENCIA_TAPA_COMBUSTIBLE(4),
		SALIDAS_ADICIONALES(5),
		FALLA_FILTRO_AIRE(6),
		DISPOSITIVOS_OBSTACULOS_IMPIDEN_ACELERAR(7),
		ACCESORIOS_DEFORMACIONES_IMPIDEN(8),
		FALLA_SISTEMA_REFRIGERACION(9),
		GOBERNADOR_NO_LIMITA(10),		
		NO_SE_ALCANZA_REVOLUCIONES_GOBERNADAS(11),
		MAL_FUNCIONAMIENTO_MOTOR(12),
		DIFERENCIA_ARITMETICA(14),
		INESTABILIDAD_RPMS_CICLOS_MEDICION(15),
		DIFERENCIA_TEMPERATURA(16),
		OPACIDAD_EXCEDE_2001(17),
		OPACIDAD_EXCEDE_1996_2000(18),
		OPACIDAD_EXCEDE_1991_1995(19),
		OPACIDAD_EXCEDE_1986_1990(20),
		OPACIDAD_EXCEDE_1981_1985(21),
		OPACIDAD_EXCEDE_1980(22);
	
	private final int value;

	private CausaRechazoDiesel(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}
}
