package com.proambiente.webapp.util.dto.logsicov;

import static com.proambiente.webapp.util.ConstantesMedidasLuces.INCLINACION_BAJA_DERECHA;
import static com.proambiente.webapp.util.ConstantesMedidasLuces.INCLINACION_BAJA_DERECHA_DOS;
import static com.proambiente.webapp.util.ConstantesMedidasLuces.INCLINACION_BAJA_DERECHA_MOTOS;
import static com.proambiente.webapp.util.ConstantesMedidasLuces.INCLINACION_BAJA_DERECHA_TRES;
import static com.proambiente.webapp.util.ConstantesMedidasLuces.INCLINACION_BAJA_IZQUIERDA;
import static com.proambiente.webapp.util.ConstantesMedidasLuces.INCLINACION_BAJA_IZQUIERDA_DOS;
import static com.proambiente.webapp.util.ConstantesMedidasLuces.INCLINACION_BAJA_IZQUIERDA_MOTOS;
import static com.proambiente.webapp.util.ConstantesMedidasLuces.INCLINACION_BAJA_IZQUIERDA_TRES;
import static com.proambiente.webapp.util.ConstantesMedidasLuces.INTENSIDAD_ALTA_DERECHA;
import static com.proambiente.webapp.util.ConstantesMedidasLuces.INTENSIDAD_ALTA_DERECHA_DOS;
import static com.proambiente.webapp.util.ConstantesMedidasLuces.INTENSIDAD_ALTA_DERECHA_MOTOS;
import static com.proambiente.webapp.util.ConstantesMedidasLuces.INTENSIDAD_ALTA_DERECHA_TRES;
import static com.proambiente.webapp.util.ConstantesMedidasLuces.INTENSIDAD_ALTA_IZQUIERDA;
import static com.proambiente.webapp.util.ConstantesMedidasLuces.INTENSIDAD_ALTA_IZQUIERDA_DOS;
import static com.proambiente.webapp.util.ConstantesMedidasLuces.INTENSIDAD_ALTA_IZQUIERDA_MOTOS;
import static com.proambiente.webapp.util.ConstantesMedidasLuces.INTENSIDAD_ALTA_IZQUIERDA_TRES;
import static com.proambiente.webapp.util.ConstantesMedidasLuces.INTENSIDAD_BAJA_DERECHA;
import static com.proambiente.webapp.util.ConstantesMedidasLuces.INTENSIDAD_BAJA_DERECHA_DOS;
import static com.proambiente.webapp.util.ConstantesMedidasLuces.INTENSIDAD_BAJA_DERECHA_MOTOS;
import static com.proambiente.webapp.util.ConstantesMedidasLuces.INTENSIDAD_BAJA_DERECHA_TRES;
import static com.proambiente.webapp.util.ConstantesMedidasLuces.INTENSIDAD_BAJA_IZQUIERDA;
import static com.proambiente.webapp.util.ConstantesMedidasLuces.INTENSIDAD_BAJA_IZQUIERDA_DOS;
import static com.proambiente.webapp.util.ConstantesMedidasLuces.INTENSIDAD_BAJA_IZQUIERDA_MOTOS;
import static com.proambiente.webapp.util.ConstantesMedidasLuces.INTENSIDAD_BAJA_IZQUIERDA_TRES;
import static com.proambiente.webapp.util.ConstantesMedidasLuces.INTENSIDAD_EXPLORADORA_DERECHA;
import static com.proambiente.webapp.util.ConstantesMedidasLuces.INTENSIDAD_EXPLORADORA_DERECHA_DOS;
import static com.proambiente.webapp.util.ConstantesMedidasLuces.INTENSIDAD_EXPLORADORA_DERECHA_TRES;
import static com.proambiente.webapp.util.ConstantesMedidasLuces.INTENSIDAD_EXPLORADORA_IZQUIERDA;
import static com.proambiente.webapp.util.ConstantesMedidasLuces.INTENSIDAD_EXPLORADORA_IZQUIERDA_DOS;
import static com.proambiente.webapp.util.ConstantesMedidasLuces.INTENSIDAD_EXPLORADORA_IZQUIERDA_TRES;
import static com.proambiente.webapp.util.ConstantesMedidasLuces.LUCES_BAJAS_ALTAS_EXPL_SIMULTANEAS;
import static com.proambiente.webapp.util.ConstantesMedidasLuces.SUMA_EXPLORADORAS;
import static com.proambiente.webapp.util.ConstantesMedidasLuces.SUMA_LUCES;

import java.text.DecimalFormat;
import java.util.List;

import com.proambiente.modelo.Medida;

public class GeneradorLogLucesSicov {
	
	public LogLucesSicovDTO generarLogSicov(List<Medida> medidas) {
		LogLucesSicovDTO log = new LogLucesSicovDTO();
		DecimalFormat df = new DecimalFormat("#");
		
		for (Medida m : medidas) {
			switch (m.getTipoMedida().getTipoMedidaId()) {
				
			case INTENSIDAD_ALTA_DERECHA_MOTOS:// luz alta derecha motocicletas
				log.setDerAltaIntensidadValor1(df.format(m.getValor()));
				break;
			case INTENSIDAD_ALTA_IZQUIERDA_MOTOS:// luz alta izquierda
				log.setIzqAltaIntesidadValor1(df.format(m.getValor()));
				break;
			case INCLINACION_BAJA_IZQUIERDA_MOTOS:// angulo baja izquierda
				log.setIzqBajaInclinacionValor1( df.format(m.getValor()));				
				break;
			case INCLINACION_BAJA_DERECHA:// angulo baja derecha para vehiculos
				log.setDerBajaInclinacionValor1(df.format(m.getValor()));
				break;
			case INCLINACION_BAJA_DERECHA_DOS:// angulo baja derecha para vehiculos
				log.setDerBajaInclinacionValor2(df.format(m.getValor()));
				break;
			case INCLINACION_BAJA_DERECHA_TRES:// angulo baja derecha para vehiculos
				log.setDerBajaInclinacionValor3(df.format(m.getValor()));
				break;
			case INCLINACION_BAJA_IZQUIERDA:// angulo baja izquierda para vehiculos
				log.setIzqBajaInclinacionValor1( df.format(m.getValor()));
				break;
			case INCLINACION_BAJA_IZQUIERDA_DOS:// angulo baja izquierda para vehiculos
				log.setIzqBajaInclinacionValor2( df.format(m.getValor()));
				break;
			case INCLINACION_BAJA_IZQUIERDA_TRES:// angulo baja izquierda para vehiculos
				log.setIzqBajaInclinacionValor3( df.format(m.getValor()));
				break;
			case INTENSIDAD_BAJA_DERECHA:// intensidad luz baja derecha para vehiculos
				log.setDerBajaIntensidadValor1(df.format(m.getValor()));
				break;
			case INTENSIDAD_BAJA_DERECHA_DOS:// intensidad luz baja derecha para vehiculos
				log.setDerBajaIntensidadValor2(df.format(m.getValor()));
				break;
			case INTENSIDAD_BAJA_DERECHA_TRES:// intensidad luz baja derecha para vehiculos
				log.setDerBajaIntensidadValor3(df.format(m.getValor()));
				break;
			case INTENSIDAD_ALTA_DERECHA:// intensidad de la luz alta derecha para
				// vehiculos
				log.setDerAltaIntensidadValor1( df.format(m.getValor()));
				//
				break;
			case INTENSIDAD_ALTA_DERECHA_DOS:// intensidad de la luz alta derecha para
				// vehiculos
				log.setDerAltaIntensidadValor2( df.format(m.getValor()));
				//
				break;
			case INTENSIDAD_ALTA_DERECHA_TRES:// intensidad de la luz alta derecha para
				// vehiculos
				log.setDerAltaIntensidadValor3( df.format(m.getValor()));
				//
				break;
			case SUMA_EXPLORADORAS:// intensidad de las luces exploradoras
				// esta medida no sale en el reporte impreso
				break;
			case INTENSIDAD_BAJA_IZQUIERDA:// intensidad de la luz baja izquierda para
				// vehiculos
				log.setIzqBajaIntensidadValor1(df.format(m.getValor()));
				break;
			case INTENSIDAD_BAJA_IZQUIERDA_DOS:// intensidad de la luz baja izquierda para
				// vehiculos
				log.setIzqBajaIntensidadValor2(df.format(m.getValor()));
				break;
			case INTENSIDAD_BAJA_IZQUIERDA_TRES:// intensidad de la luz baja izquierda para
				// vehiculos
				log.setIzqBajaIntensidaValor3(df.format(m.getValor()));
				break;
			case INTENSIDAD_ALTA_IZQUIERDA:// intensidad alta izquierda
				log.setDerAltaIntensidadValor1( df.format(m.getValor()));
				break;
			case INTENSIDAD_ALTA_IZQUIERDA_DOS:// intensidad alta izquierda
				log.setDerAltaIntensidadValor2( df.format(m.getValor()));
				break;
			case INTENSIDAD_ALTA_IZQUIERDA_TRES:// intensidad alta izquierda
				log.setDerAltaIntensidadValor3( df.format(m.getValor()));
				break;
			case SUMA_LUCES:
				log.setSumatoriaIntensidad(df.format(m.getValor()));
				break;
			case INCLINACION_BAJA_DERECHA_MOTOS:// angulo baja derecha
				log.setDerBajaInclinacionValor1(df.format(m.getValor()));
				break;
			case INTENSIDAD_BAJA_DERECHA_MOTOS:// luz baja derecha
				log.setDerBajaIntensidadValor1(df.format(m.getValor()));
				break;
			case INTENSIDAD_BAJA_IZQUIERDA_MOTOS:// luz baja izquierda
				log.setIzqBajaIntensidadValor1(df.format(m.getValor()));
				break;
			case LUCES_BAJAS_ALTAS_EXPL_SIMULTANEAS:
				if(m.getValor()>0) {
					log.setIzqBajaSimultaneas("TRUE");
					log.setIzqAltasSimultaneas("TRUE");
					log.setIzqExploradorasSimultaneas("TRUE");
					log.setDerBajaSimultaneas("TRUE");
					log.setDerAltasSimultaneas("TRUE");
					log.setDerExploradorasSimultaneas("TRUE");
				}
				break;
			case 2016:// intensidad alt derecha en motocarro
				// parametros.put("SumaLuces",String.valueOf(m.getValormedida()));//no
				// sale en el reporte
				break;
			case 2017:// intensidad alta izquierda en motocarro
				// parametros.put("SumaLuces",String.valueOf(m.getValormedida()));
				break;
			case 2018:// intensidad baja derecha motocarro
				log.setDerBajaIntensidadValor1(df.format(m.getValor()));
				break;
			case 2019:// intensidad baja izquierda en motocarro
				log.setIzqBajaIntensidadValor1(df.format(m.getValor()));
				break;
			case 2020://
				log.setDerBajaInclinacionValor1(df.format(m.getValor()));
				break;
			case 2021:// inclinacion baja derecha motocarro
				log.setDerBajaInclinacionValor1(df.format(m.getValor()));
				break;
			case INTENSIDAD_EXPLORADORA_DERECHA:// intensidad alta izquierda
				log.setDerExplorardorasValor1( df.format( m.getValor() ));
				break;
			case INTENSIDAD_EXPLORADORA_IZQUIERDA:// intensidad alta izquierda
				log.setIzqExplorardorasValor1( df.format( m.getValor() ));
				break;
			case INTENSIDAD_EXPLORADORA_DERECHA_DOS:// intensidad alta izquierda
				log.setDerExplorardorasValor2( df.format( m.getValor() ));
				break;
			case INTENSIDAD_EXPLORADORA_IZQUIERDA_DOS:// intensidad alta izquierda
				log.setIzqExplorardorasValor2( df.format( m.getValor() ));
				break;
			case INTENSIDAD_EXPLORADORA_DERECHA_TRES:// intensidad alta izquierda
				log.setDerExplorardorasValor3( df.format( m.getValor() ));
				break;
			case INTENSIDAD_EXPLORADORA_IZQUIERDA_TRES:// intensidad alta izquierda
				log.setIzqExplorardorasValor3( df.format( m.getValor() ));
				break;

			default:
				break;
			}// end of switch
		} // end of for ista de Medida
		
		return log;
		
	}

}
