package com.proambiente.webapp.util.dto;

public class ResultadoProfundidadLabradoDTO {
	
	private String profundidadLabradoEje1Der1 = "";
	private String profundidadLabradoEje1Der2 = "";
	private String profundidadLabradoEje1Der3 = "";
	private String profundidadLabradoEje1Der4 = "";
	
	
	private String profundidadLabradoEje2Der1 = "";
	private String profundidadLabradoEje2Der2 = "";
	private String profundidadLabradoEje2Der3 = "";
	private String profundidadLabradoEje2Der4 = "";
	
	private String profundidadLabradoEje3Der1 = "";
	private String profundidadLabradoEje3Der2 = "";
	private String profundidadLabradoEje3Der3 = "";
	private String profundidadLabradoEje3Der4 = "";
	
	private String profundidadLabradoEje4Der1 = "";
	private String profundidadLabradoEje4Der2 = "";
	private String profundidadLabradoEje4Der3 = "";
	private String profundidadLabradoEje4Der4 = "";
	
	private String profundidadLabradoEje5Der1 = "";
	private String profundidadLabradoEje5Der2 = "";
	private String profundidadLabradoEje5Der3 = "";
	private String profundidadLabradoEje5Der4 = "";
	
	private String profundidadLabradoEje1Izq1 = "";
	private String profundidadLabradoEje1Izq2 = "";
	private String profundidadLabradoEje1Izq3 = "";
	private String profundidadLabradoEje1Izq4 = "";
	
	
	private String profundidadLabradoEje2Izq1 = "";
	private String profundidadLabradoEje2Izq2 = "";
	private String profundidadLabradoEje2Izq3 = "";
	private String profundidadLabradoEje2Izq4 = "";
	
	private String profundidadLabradoEje3Izq1 = "";
	private String profundidadLabradoEje3Izq2 = "";
	private String profundidadLabradoEje3Izq3 = "";
	private String profundidadLabradoEje3Izq4 = "";
	
	private String profundidadLabradoEje4Izq1 = "";
	private String profundidadLabradoEje4Izq2 = "";
	private String profundidadLabradoEje4Izq3 = "";
	private String profundidadLabradoEje4Izq4 = "";
	
	private String profundidadLabradoEje5Izq1 = "";
	private String profundidadLabradoEje5Izq2 = "";
	private String profundidadLabradoEje5Izq3 = "";
	private String profundidadLabradoEje5Izq4 = "";
	
	private String profundidadLabradoRepuestoDerecha = "";
	private String profundidadLabradoRepuestoIzquierda = "";
	
	public String getProfundidadLabradoEje1Der1() {
		return profundidadLabradoEje1Der1;
	}
	public void setProfundidadLabradoEje1Der1(String profundidadLabradoEje1Der1) {
		this.profundidadLabradoEje1Der1 = profundidadLabradoEje1Der1;
	}
	public String getProfundidadLabradoEje1Der2() {
		return profundidadLabradoEje1Der2;
	}
	public void setProfundidadLabradoEje1Der2(String profundidadLabradoEje1Der2) {
		this.profundidadLabradoEje1Der2 = profundidadLabradoEje1Der2;
	}
	public String getProfundidadLabradoEje1Der3() {
		return profundidadLabradoEje1Der3;
	}
	public void setProfundidadLabradoEje1Der3(String profundidadLabradoEje1Der3) {
		this.profundidadLabradoEje1Der3 = profundidadLabradoEje1Der3;
	}
	public String getProfundidadLabradoEje1Der4() {
		return profundidadLabradoEje1Der4;
	}
	public void setProfundidadLabradoEje1Der4(String profundidadLabradoEje1Der4) {
		this.profundidadLabradoEje1Der4 = profundidadLabradoEje1Der4;
	}
	public String getProfundidadLabradoEje2Der1() {
		return profundidadLabradoEje2Der1;
	}
	public void setProfundidadLabradoEje2Der1(String profundidadLabradoEje2Der1) {
		this.profundidadLabradoEje2Der1 = profundidadLabradoEje2Der1;
	}
	public String getProfundidadLabradoEje2Der2() {
		return profundidadLabradoEje2Der2;
	}
	public void setProfundidadLabradoEje2Der2(String profundidadLabradoEje2Der2) {
		this.profundidadLabradoEje2Der2 = profundidadLabradoEje2Der2;
	}
	public String getProfundidadLabradoEje2Der3() {
		return profundidadLabradoEje2Der3;
	}
	public void setProfundidadLabradoEje2Der3(String profundidadLabradoEje2Der3) {
		this.profundidadLabradoEje2Der3 = profundidadLabradoEje2Der3;
	}
	public String getProfundidadLabradoEje2Der4() {
		return profundidadLabradoEje2Der4;
	}
	public void setProfundidadLabradoEje2Der4(String profundidadLabradoEje2Der4) {
		this.profundidadLabradoEje2Der4 = profundidadLabradoEje2Der4;
	}
	public String getProfundidadLabradoEje3Der1() {
		return profundidadLabradoEje3Der1;
	}
	public void setProfundidadLabradoEje3Der1(String profundidadLabradoEje3Der1) {
		this.profundidadLabradoEje3Der1 = profundidadLabradoEje3Der1;
	}
	public String getProfundidadLabradoEje3Der2() {
		return profundidadLabradoEje3Der2;
	}
	public void setProfundidadLabradoEje3Der2(String profundidadLabradoEje3Der2) {
		this.profundidadLabradoEje3Der2 = profundidadLabradoEje3Der2;
	}
	public String getProfundidadLabradoEje3Der3() {
		return profundidadLabradoEje3Der3;
	}
	public void setProfundidadLabradoEje3Der3(String profundidadLabradoEje3Der3) {
		this.profundidadLabradoEje3Der3 = profundidadLabradoEje3Der3;
	}
	public String getProfundidadLabradoEje3Der4() {
		return profundidadLabradoEje3Der4;
	}
	public void setProfundidadLabradoEje3Der4(String profundidadLabradoEje3Der4) {
		this.profundidadLabradoEje3Der4 = profundidadLabradoEje3Der4;
	}
	public String getProfundidadLabradoEje4Der1() {
		return profundidadLabradoEje4Der1;
	}
	public void setProfundidadLabradoEje4Der1(String profundidadLabradoEje4Der1) {
		this.profundidadLabradoEje4Der1 = profundidadLabradoEje4Der1;
	}
	public String getProfundidadLabradoEje4Der2() {
		return profundidadLabradoEje4Der2;
	}
	public void setProfundidadLabradoEje4Der2(String profundidadLabradoEje4Der2) {
		this.profundidadLabradoEje4Der2 = profundidadLabradoEje4Der2;
	}
	public String getProfundidadLabradoEje4Der3() {
		return profundidadLabradoEje4Der3;
	}
	public void setProfundidadLabradoEje4Der3(String profundidadLabradoEje4Der3) {
		this.profundidadLabradoEje4Der3 = profundidadLabradoEje4Der3;
	}
	public String getProfundidadLabradoEje4Der4() {
		return profundidadLabradoEje4Der4;
	}
	public void setProfundidadLabradoEje4Der4(String profundidadLabradoEje4Der4) {
		this.profundidadLabradoEje4Der4 = profundidadLabradoEje4Der4;
	}
	public String getProfundidadLabradoEje5Der1() {
		return profundidadLabradoEje5Der1;
	}
	public void setProfundidadLabradoEje5Der1(String profundidadLabradoEje5Der1) {
		this.profundidadLabradoEje5Der1 = profundidadLabradoEje5Der1;
	}
	public String getProfundidadLabradoEje5Der2() {
		return profundidadLabradoEje5Der2;
	}
	public void setProfundidadLabradoEje5Der2(String profundidadLabradoEje5Der2) {
		this.profundidadLabradoEje5Der2 = profundidadLabradoEje5Der2;
	}
	public String getProfundidadLabradoEje5Der3() {
		return profundidadLabradoEje5Der3;
	}
	public void setProfundidadLabradoEje5Der3(String profundidadLabradoEje5Der3) {
		this.profundidadLabradoEje5Der3 = profundidadLabradoEje5Der3;
	}
	public String getProfundidadLabradoEje5Der4() {
		return profundidadLabradoEje5Der4;
	}
	public void setProfundidadLabradoEje5Der4(String profundidadLabradoEje5Der4) {
		this.profundidadLabradoEje5Der4 = profundidadLabradoEje5Der4;
	}
	public String getProfundidadLabradoEje1Izq1() {
		return profundidadLabradoEje1Izq1;
	}
	public void setProfundidadLabradoEje1Izq1(String profundidadLabradoEje1Izq1) {
		this.profundidadLabradoEje1Izq1 = profundidadLabradoEje1Izq1;
	}
	public String getProfundidadLabradoEje1Izq2() {
		return profundidadLabradoEje1Izq2;
	}
	public void setProfundidadLabradoEje1Izq2(String profundidadLabradoEje1Izq2) {
		this.profundidadLabradoEje1Izq2 = profundidadLabradoEje1Izq2;
	}
	public String getProfundidadLabradoEje1Izq3() {
		return profundidadLabradoEje1Izq3;
	}
	public void setProfundidadLabradoEje1Izq3(String profundidadLabradoEje1Izq3) {
		this.profundidadLabradoEje1Izq3 = profundidadLabradoEje1Izq3;
	}
	public String getProfundidadLabradoEje1Izq4() {
		return profundidadLabradoEje1Izq4;
	}
	public void setProfundidadLabradoEje1Izq4(String profundidadLabradoEje1Izq4) {
		this.profundidadLabradoEje1Izq4 = profundidadLabradoEje1Izq4;
	}
	public String getProfundidadLabradoEje2Izq1() {
		return profundidadLabradoEje2Izq1;
	}
	public void setProfundidadLabradoEje2Izq1(String profundidadLabradoEje2Izq1) {
		this.profundidadLabradoEje2Izq1 = profundidadLabradoEje2Izq1;
	}
	public String getProfundidadLabradoEje2Izq2() {
		return profundidadLabradoEje2Izq2;
	}
	public void setProfundidadLabradoEje2Izq2(String profundidadLabradoEje2Izq2) {
		this.profundidadLabradoEje2Izq2 = profundidadLabradoEje2Izq2;
	}
	public String getProfundidadLabradoEje2Izq3() {
		return profundidadLabradoEje2Izq3;
	}
	public void setProfundidadLabradoEje2Izq3(String profundidadLabradoEje2Izq3) {
		this.profundidadLabradoEje2Izq3 = profundidadLabradoEje2Izq3;
	}
	public String getProfundidadLabradoEje2Izq4() {
		return profundidadLabradoEje2Izq4;
	}
	public void setProfundidadLabradoEje2Izq4(String profundidadLabradoEje2Izq4) {
		this.profundidadLabradoEje2Izq4 = profundidadLabradoEje2Izq4;
	}
	public String getProfundidadLabradoEje3Izq1() {
		return profundidadLabradoEje3Izq1;
	}
	public void setProfundidadLabradoEje3Izq1(String profundidadLabradoEje3Izq1) {
		this.profundidadLabradoEje3Izq1 = profundidadLabradoEje3Izq1;
	}
	public String getProfundidadLabradoEje3Izq2() {
		return profundidadLabradoEje3Izq2;
	}
	public void setProfundidadLabradoEje3Izq2(String profundidadLabradoEje3Izq2) {
		this.profundidadLabradoEje3Izq2 = profundidadLabradoEje3Izq2;
	}
	public String getProfundidadLabradoEje3Izq3() {
		return profundidadLabradoEje3Izq3;
	}
	public void setProfundidadLabradoEje3Izq3(String profundidadLabradoEje3Izq3) {
		this.profundidadLabradoEje3Izq3 = profundidadLabradoEje3Izq3;
	}
	public String getProfundidadLabradoEje3Izq4() {
		return profundidadLabradoEje3Izq4;
	}
	public void setProfundidadLabradoEje3Izq4(String profundidadLabradoEje3Izq4) {
		this.profundidadLabradoEje3Izq4 = profundidadLabradoEje3Izq4;
	}
	public String getProfundidadLabradoEje4Izq1() {
		return profundidadLabradoEje4Izq1;
	}
	public void setProfundidadLabradoEje4Izq1(String profundidadLabradoEje4Izq1) {
		this.profundidadLabradoEje4Izq1 = profundidadLabradoEje4Izq1;
	}
	public String getProfundidadLabradoEje4Izq2() {
		return profundidadLabradoEje4Izq2;
	}
	public void setProfundidadLabradoEje4Izq2(String profundidadLabradoEje4Izq2) {
		this.profundidadLabradoEje4Izq2 = profundidadLabradoEje4Izq2;
	}
	public String getProfundidadLabradoEje4Izq3() {
		return profundidadLabradoEje4Izq3;
	}
	public void setProfundidadLabradoEje4Izq3(String profundidadLabradoEje4Izq3) {
		this.profundidadLabradoEje4Izq3 = profundidadLabradoEje4Izq3;
	}
	public String getProfundidadLabradoEje4Izq4() {
		return profundidadLabradoEje4Izq4;
	}
	public void setProfundidadLabradoEje4Izq4(String profundidadLabradoEje4Izq4) {
		this.profundidadLabradoEje4Izq4 = profundidadLabradoEje4Izq4;
	}
	public String getProfundidadLabradoEje5Izq1() {
		return profundidadLabradoEje5Izq1;
	}
	public void setProfundidadLabradoEje5Izq1(String profundidadLabradoEje5Izq1) {
		this.profundidadLabradoEje5Izq1 = profundidadLabradoEje5Izq1;
	}
	public String getProfundidadLabradoEje5Izq2() {
		return profundidadLabradoEje5Izq2;
	}
	public void setProfundidadLabradoEje5Izq2(String profundidadLabradoEje5Izq2) {
		this.profundidadLabradoEje5Izq2 = profundidadLabradoEje5Izq2;
	}
	public String getProfundidadLabradoEje5Izq3() {
		return profundidadLabradoEje5Izq3;
	}
	public void setProfundidadLabradoEje5Izq3(String profundidadLabradoEje5Izq3) {
		this.profundidadLabradoEje5Izq3 = profundidadLabradoEje5Izq3;
	}
	public String getProfundidadLabradoEje5Izq4() {
		return profundidadLabradoEje5Izq4;
	}
	public void setProfundidadLabradoEje5Izq4(String profundidadLabradoEje5Izq4) {
		this.profundidadLabradoEje5Izq4 = profundidadLabradoEje5Izq4;
	}
	public String getProfundidadLabradoRepuestoDerecha() {
		return profundidadLabradoRepuestoDerecha;
	}
	public void setProfundidadLabradoRepuestoDerecha(String profundidadLabradoRepuestoDerecha) {
		this.profundidadLabradoRepuestoDerecha = profundidadLabradoRepuestoDerecha;
	}
	public String getProfundidadLabradoRepuestoIzquierda() {
		return profundidadLabradoRepuestoIzquierda;
	}
	public void setProfundidadLabradoRepuestoIzquierda(String profundidadLabradoRepuestoIzquierda) {
		this.profundidadLabradoRepuestoIzquierda = profundidadLabradoRepuestoIzquierda;
	}	
}
