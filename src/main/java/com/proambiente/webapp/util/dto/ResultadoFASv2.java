package com.proambiente.webapp.util.dto;
/**
 * Clase que representa
 * la informacion faltante 
 * resultado
 * 20203040003625
 * @author USUARIO
 *
 */
public class ResultadoFASv2 {
	
	private String fuerzaDerechaAuxiliar =  "";
	private String fuerzaIzquierdaAuxiliar = "";
	private String pesoDerechoTotal = "";
	private String pesoIzquierdoTotal = "";

	
	
	public String getFuerzaDerechaAuxiliar() {
		return fuerzaDerechaAuxiliar;
	}
	public void setFuerzaDerechaAuxiliar(String fuerzaDerechaAuxiliar) {
		this.fuerzaDerechaAuxiliar = fuerzaDerechaAuxiliar;
	}
	public String getFuerzaIzquierdaAuxiliar() {
		return fuerzaIzquierdaAuxiliar;
	}
	public void setFuerzaIzquierdaAuxiliar(String fuerzaIzquierdaAuxiliar) {
		this.fuerzaIzquierdaAuxiliar = fuerzaIzquierdaAuxiliar;
	}
	public String getPesoDerechoTotal() {
		return pesoDerechoTotal;
	}
	public void setPesoDerechoTotal(String pesoDerechoTotal) {
		this.pesoDerechoTotal = pesoDerechoTotal;
	}
	public String getPesoIzquierdoTotal() {
		return pesoIzquierdoTotal;
	}
	public void setPesoIzquierdoTotal(String pesoIzquierdoTotal) {
		this.pesoIzquierdoTotal = pesoIzquierdoTotal;
	}
	
}
