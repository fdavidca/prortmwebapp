package com.proambiente.webapp.util.dto;

public class EquipoPruebaDTO {
  private String modeloEquipo;
  
  private String fabricante;
  
  private String serieEquipo;
  
  private String tipoPrueba;
  
  private String tipoEquipo;
  
  private String ltoe;
  
  public EquipoPruebaDTO() {}
  
  public EquipoPruebaDTO(String modeloEquipo, String fabricante, String serieEquipo, String tipoPrueba, String tipoEquipo) {
    this.modeloEquipo = modeloEquipo;
    this.fabricante = fabricante;
    this.serieEquipo = serieEquipo;
    this.tipoPrueba = tipoPrueba;
    this.tipoEquipo = tipoEquipo;
  }
  
  public String getModeloEquipo() {
    return this.modeloEquipo;
  }
  
  public void setModeloEquipo(String modeloEquipo) {
    this.modeloEquipo = modeloEquipo;
  }
  
  public String getFabricante() {
    return this.fabricante;
  }
  
  public void setFabricante(String fabricante) {
    this.fabricante = fabricante;
  }
  
  public String getSerieEquipo() {
    return this.serieEquipo;
  }
  
  public void setSerieEquipo(String serieEquipo) {
    this.serieEquipo = serieEquipo;
  }
  
  public String getTipoPrueba() {
    return this.tipoPrueba;
  }
  
  public void setTipoPrueba(String tipoPrueba) {
    this.tipoPrueba = tipoPrueba;
  }
  
  public String getTipoEquipo() {
    return this.tipoEquipo;
  }
  
  public void setTipoEquipo(String tipoEquipo) {
    this.tipoEquipo = tipoEquipo;
  }
  
  public String getLtoe() {
    return this.ltoe;
  }
  
  public void setLtoe(String ltoe) {
    this.ltoe = ltoe;
  }
}
