package com.proambiente.webapp.util.dto;

public class MuestraFugasDTO{
	private String fecha;
	private Double valor;
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public MuestraFugasDTO(String fecha, Double valor) {
		super();
		this.fecha = fecha;
		this.valor = valor;
	}
	
	
}