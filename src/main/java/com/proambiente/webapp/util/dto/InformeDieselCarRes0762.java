package com.proambiente.webapp.util.dto;

import com.proambiente.webapp.util.informes.medellin.InformeDieselRes0762;

public class InformeDieselCarRes0762 {
	
	private InformeDieselRes0762 subinformeDiesel;
	
	ResultadoSonometriaInformeDTO resultadoSonometria;
	
	private String causaRechazoDiesel;
	private String resultadoPrueba;
	private String numeroCda;
	private String numeroConsecutivoPrueba;
	private String potencia;
	private String normaAplicada;
	private String codigoCiudad;
	protected String fechaInicioPrueba;
	protected String fechaFinPrueba;
	protected String fechaVerificacionGasolina;
	protected String codigoCiudadPropietario;
	protected String codigoLineaVehiculo;
	protected String codigoClaseVehiculo;
	protected String codigoCombustible;
	protected String codigoServicio;
	protected String codigoMarca;
	protected String temperaturaMotor;
	protected String inestabilidadRpmCiclos;
	
	
	public String getInestabilidadRpmCiclos() {
		return inestabilidadRpmCiclos;
	}
	public void setInestabilidadRpmCiclos(String inestabilidadRpmCiclos) {
		if(inestabilidadRpmCiclos.equalsIgnoreCase("TRUE")) {
			this.inestabilidadRpmCiclos = "SI";
		}else if(inestabilidadRpmCiclos.equalsIgnoreCase("FALSE")) {
			this.inestabilidadRpmCiclos = "NO";
		}else {
			this.inestabilidadRpmCiclos = inestabilidadRpmCiclos;
		}
		
		
	}
	public String getCodigoCiudad() {
		return codigoCiudad;
	}
	public void setCodigoCiudad(String codigoCiudad) {
		this.codigoCiudad = codigoCiudad;
	}
	public String getFechaInicioPrueba() {
		return fechaInicioPrueba;
	}
	public void setFechaInicioPrueba(String fechaInicioPrueba) {
		this.fechaInicioPrueba = fechaInicioPrueba;
	}
	public String getFechaFinPrueba() {
		return fechaFinPrueba;
	}
	public void setFechaFinPrueba(String fechaFinPrueba) {
		this.fechaFinPrueba = fechaFinPrueba;
	}
	public String getFechaVerificacionGasolina() {
		return fechaVerificacionGasolina;
	}
	public void setFechaVerificacionGasolina(String fechaVerificacionGasolina) {
		this.fechaVerificacionGasolina = fechaVerificacionGasolina;
	}
	public String getCodigoCiudadPropietario() {
		return codigoCiudadPropietario;
	}
	public void setCodigoCiudadPropietario(String codigoCiudadPropietario) {
		this.codigoCiudadPropietario = codigoCiudadPropietario;
	}
	public String getCodigoLineaVehiculo() {
		return codigoLineaVehiculo;
	}
	public void setCodigoLineaVehiculo(String codigoLineaVehiculo) {
		this.codigoLineaVehiculo = codigoLineaVehiculo;
	}
	public String getCodigoClaseVehiculo() {
		return codigoClaseVehiculo;
	}
	public void setCodigoClaseVehiculo(String codigoClaseVehiculo) {
		this.codigoClaseVehiculo = codigoClaseVehiculo;
	}
	public String getCodigoCombustible() {
		return codigoCombustible;
	}
	public void setCodigoCombustible(String codigoCombustible) {
		this.codigoCombustible = codigoCombustible;
	}
	public String getCodigoServicio() {
		return codigoServicio;
	}
	public void setCodigoServicio(String codigoServicio) {
		this.codigoServicio = codigoServicio;
	}
	public String getCodigoMarca() {
		return codigoMarca;
	}
	public void setCodigoMarca(String codigoMarca) {
		this.codigoMarca = codigoMarca;
	}
	public String getTemperaturaMotor() {
		return temperaturaMotor;
	}
	public void setTemperaturaMotor(String temperaturaMotor) {
		this.temperaturaMotor = temperaturaMotor;
	}
	public InformeDieselRes0762 getSubinformeDiesel() {
		return subinformeDiesel;
	}
	public void setSubinformeDiesel(InformeDieselRes0762 subinformeDiesel) {
		this.subinformeDiesel = subinformeDiesel;
	}
	public ResultadoSonometriaInformeDTO getResultadoSonometria() {
		return resultadoSonometria;
	}
	public void setResultadoSonometria(ResultadoSonometriaInformeDTO resultadoSonometria) {
		this.resultadoSonometria = resultadoSonometria;
	}
	public String getCausaRechazoDiesel() {
		return causaRechazoDiesel;
	}
	public void setCausaRechazoDiesel(String causaRechazoDiesel) {
		this.causaRechazoDiesel = causaRechazoDiesel;
	}
	public String getResultadoPrueba() {
		return resultadoPrueba;
	}
	public void setResultadoPrueba(String resultadoPrueba) {
		this.resultadoPrueba = resultadoPrueba;
	}
	public String getNumeroCda() {
		return numeroCda;
	}
	public void setNumeroCda(String numeroCda) {
		this.numeroCda = numeroCda;
	}
	public String getNumeroConsecutivoPrueba() {
		return numeroConsecutivoPrueba;
	}
	public void setNumeroConsecutivoPrueba(String numeroConsecutivoPrueba) {
		this.numeroConsecutivoPrueba = numeroConsecutivoPrueba;
	}
	public String getPotencia() {
		return potencia;
	}
	public void setPotencia(String potencia) {
		this.potencia = potencia;
	}
	public String getNormaAplicada() {
		return normaAplicada;
	}
	public void setNormaAplicada(String normaAplicada) {
		this.normaAplicada = normaAplicada;
	}
	
	

}
