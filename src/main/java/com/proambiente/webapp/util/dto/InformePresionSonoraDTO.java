package com.proambiente.webapp.util.dto;

import java.sql.Timestamp;


public class InformePresionSonoraDTO {
	
	public String nombreCDA;
	public String numeroCDA;
	public String nitCDA;
	
	public String marcaEquipo;
	public String modeloEquipo;
	public String serieEquipo;
	public Timestamp fechaMedicion;
	public String placaVehiculo;
	public String tipoVehiculo;
	public Integer modeloVehiculo;
	public String combustibleVehiculo;
	public Integer cilindradaVehiculo;
	public String medicionEscape;
	public String getNombreCDA() {
		return nombreCDA;
	}
	public void setNombreCDA(String nombreCDA) {
		this.nombreCDA = nombreCDA;
	}
	public String getNumeroCDA() {
		return numeroCDA;
	}
	public void setNumeroCDA(String numeroCDA) {
		this.numeroCDA = numeroCDA;
	}
	public String getNitCDA() {
		return nitCDA;
	}
	public void setNitCDA(String nitCDA) {
		this.nitCDA = nitCDA;
	}
	public String getMarcaEquipo() {
		return marcaEquipo;
	}
	public void setMarcaEquipo(String marcaEquipo) {
		this.marcaEquipo = marcaEquipo;
	}
	public String getModeloEquipo() {
		return modeloEquipo;
	}
	public void setModeloEquipo(String modeloEquipo) {
		this.modeloEquipo = modeloEquipo;
	}
	public String getSerieEquipo() {
		return serieEquipo;
	}
	public void setSerieEquipo(String serieEquipo) {
		this.serieEquipo = serieEquipo;
	}
	public Timestamp getFechaMedicion() {
		return fechaMedicion;
	}
	public void setFechaMedicion(Timestamp fechaMedicion) {
		this.fechaMedicion = fechaMedicion;
	}
	public String getPlacaVehiculo() {
		return placaVehiculo;
	}
	public void setPlacaVehiculo(String placaVehiculo) {
		this.placaVehiculo = placaVehiculo;
	}
	public String getTipoVehiculo() {
		return tipoVehiculo;
	}
	public void setTipoVehiculo(String tipoVehiculo) {
		this.tipoVehiculo = tipoVehiculo;
	}
	public Integer getModeloVehiculo() {
		return modeloVehiculo;
	}
	public void setModeloVehiculo(Integer modeloVehiculo) {
		this.modeloVehiculo = modeloVehiculo;
	}
	public String getCombustibleVehiculo() {
		return combustibleVehiculo;
	}
	public void setCombustibleVehiculo(String combustibleVehiculo) {
		this.combustibleVehiculo = combustibleVehiculo;
	}
	public Integer getCilindradaVehiculo() {
		return cilindradaVehiculo;
	}
	public void setCilindradaVehiculo(Integer cilindradaVehiculo) {
		this.cilindradaVehiculo = cilindradaVehiculo;
	}
	public String getMedicionEscape() {
		return medicionEscape;
	}
	public void setMedicionEscape(String medicionEscape) {
		this.medicionEscape = medicionEscape;
	}
	
	

}
