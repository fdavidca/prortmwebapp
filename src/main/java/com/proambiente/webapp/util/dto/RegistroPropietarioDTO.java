package com.proambiente.webapp.util.dto;


import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RegistroPropietarioDTO {

	@NotNull
	@JsonProperty("tipo_documento")
	private String tipoDocumento;
	
	@NotNull
	@JsonProperty("numero_documento")
	private String numeroDocumento;
	
	@NotNull
	private String tel;
	private String tel2;
	
	@NotNull
	private String nombres;
	@NotNull
	private String apellidos;
	
	
	private String correo;
	private String direccion;
	private String departamento;
	private String ciudad;
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getTel2() {
		return tel2;
	}
	public void setTel2(String tel2) {
		this.tel2 = tel2;
	}	
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getDepartamento() {
		return departamento;
	}
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RegistroPropietarioDTO [tipoDocumento=");
		builder.append(tipoDocumento);
		builder.append(", numeroDocumento=");
		builder.append(numeroDocumento);
		builder.append(", tel=");
		builder.append(tel);
		builder.append(", tel2=");
		builder.append(tel2);
		builder.append(", nombres=");
		builder.append(nombres);
		builder.append(", apellidos=");
		builder.append(apellidos);
		builder.append(", correo=");
		builder.append(correo);
		builder.append(", direccion=");
		builder.append(direccion);
		builder.append(", departamento=");
		builder.append(departamento);
		builder.append(", ciudad=");
		builder.append(ciudad);
		builder.append("]");
		return builder.toString();
	}
	
	
	
}
