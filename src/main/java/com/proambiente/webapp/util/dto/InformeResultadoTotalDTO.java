package com.proambiente.webapp.util.dto;

public class InformeResultadoTotalDTO {
	
	private String numeroFormato;
	private String fechaPrueba;
	private String resultadoDeTodaLaPrueba;
	private String numeroDeControl;
	private String numeroConsecutivoRunt;
	private String placa;
	private String servicio;
	private String clase;
	private String cilindraje;
	private String marca;
	private String linea;
	private String modelo;
	private String fechaMatricula;
	private String combustible;
	private String tipoMotor;
	private String ruidoEscape;
	private String intensidadBajaDerecha;
	private String intensidadBajaIzquierda;
	private String inclinacionBajaDerecha;
	private String inclinacionBajaIzquierda;
	private String intensidadTotal;
	private String delanteraDerecha;
	private String delanteraIzquierda;
	private String traseraDerecha;
	private String traseraIzquierda;
	private String eficaciaTotal;
	private String eficaciaAuxiliar;
	private String fuerzaEje1Derecho;
	private String fuerzaEje2Derecho;
	private String fuerzaEje3Derecho;
	private String fuerzaEje4Derecho;
	private String fuerzaEje5Derecho;
	private String fuerzaEje1Izquierdo;
	private String fuerzaEje2Izquierdo;
	private String fuerzaEje3Izquierdo;
	private String fuerzaEje4Izquierdo;
	private String fuerzaEje5Izquierdo;
	private String pesoEje1Derecho;
	private String pesoEje2Derecho;
	private String pesoEje3Derecho;
	private String pesoEje4Derecho;
	private String pesoEje5Derecho;
	private String pesoEje1Izquierdo;
	private String pesoEje2Izquierdo;
	private String pesoEje3Izquierdo;
	private String pesoEje4Izquierdo;
	private String pesoEje5Izquierdo;
	
	private String desequilibrioEje1;
	private String desequilibrioEje2;
	private String desequilibrioEje3;
	private String desequilibrioEje4;
	private String desequilibrioEje5;
	private String desviacionEje1;
	private String desviacionEje2;
	private String desviacionEje3;
	private String desviacionEje4;
	private String desviacionEje5;
	private String referenciaComercialLlanta;
	private String errorDistancia;
	private String errorTiempo;
	private String temperatura;
	private String rpmCrucero;
	private String rpmRalenti;
	private String tempRalenti;
	private String tempCrucero;
	
	private String coRalenti;
	private String coCrucero;
	private String co2Ralenti;
	private String co2Crucero;
	private String o2Ralenti;
	private String o2Crucero;
	private String hcRalenti;
	private String hcCrucero;
	private String rpmRalentiDiesel;
	private String rpmGobernadaDiesel;
	private String temperaturaDiesel;
	private String opacidadCiclo1;
	private String opacidadCiclo2;
	private String opacidadCiclo3;
	private String opacidadCiclo4;
	private String densidadHumoCiclo1;
	private String densidadHumoCiclo2;
	private String densidadHumoCiclo3;
	private String densidadHumoCiclo4;
	private String densdiadHumoResultado;
	private String resultado;
	
	
	private String profEje1Der;
	private String profEje1Izq;
	
	private String profEje2Der1;
	private String profEje2Der2;
	
	private String profEje2Izq1;
	private String profEje2Izq2;
	
	private String profEje3Der1;
	private String profEje3Der2;
	
	private String profEje3Izq1;
	private String profEje3Izq2;
	
	private String profEje4Der1;
	private String profEje4Der2;
	
	private String profEje4Izq1;
	private String profEje4Izq2;
	
	private String profEje5Der1;
	private String profEje5Der2;
	
	private String profEje5Izq1;
	private String profEje5Izq2;
	
	private String profRepDer;
	private String profRepIzq;
	
	
	
	
	public String getDensidadHumoCiclo1() {
		return densidadHumoCiclo1;
	}
	public void setDensidadHumoCiclo1(String densidadHumoCiclo1) {
		this.densidadHumoCiclo1 = densidadHumoCiclo1;
	}
	public String getDensidadHumoCiclo2() {
		return densidadHumoCiclo2;
	}
	public void setDensidadHumoCiclo2(String densidadHumoCiclo2) {
		this.densidadHumoCiclo2 = densidadHumoCiclo2;
	}
	public String getDensidadHumoCiclo3() {
		return densidadHumoCiclo3;
	}
	public void setDensidadHumoCiclo3(String densidadHumoCiclo3) {
		this.densidadHumoCiclo3 = densidadHumoCiclo3;
	}
	public String getDensidadHumoCiclo4() {
		return densidadHumoCiclo4;
	}
	public void setDensidadHumoCiclo4(String densidadHumoCiclo4) {
		this.densidadHumoCiclo4 = densidadHumoCiclo4;
	}
	public String getDensdiadHumoResultado() {
		return densdiadHumoResultado;
	}
	public void setDensdiadHumoResultado(String densdiadHumoResultado) {
		this.densdiadHumoResultado = densdiadHumoResultado;
	}
	public String getNumeroFormato() {
		return numeroFormato;
	}
	public void setNumeroFormato(String numeroFormato) {
		this.numeroFormato = numeroFormato;
	}
	public String getFechaPrueba() {
		return fechaPrueba;
	}
	public void setFechaPrueba(String fechaPrueba) {
		this.fechaPrueba = fechaPrueba;
	}
	public String getResultadoDeTodaLaPrueba() {
		return resultadoDeTodaLaPrueba;
	}
	public void setResultadoDeTodaLaPrueba(String resultadoDeTodaLaPrueba) {
		this.resultadoDeTodaLaPrueba = resultadoDeTodaLaPrueba;
	}
	public String getNumeroDeControl() {
		return numeroDeControl;
	}
	public void setNumeroDeControl(String numeroDeControl) {
		this.numeroDeControl = numeroDeControl;
	}
	public String getNumeroConsecutivoRunt() {
		return numeroConsecutivoRunt;
	}
	public void setNumeroConsecutivoRunt(String numeroConsecutivoRunt) {
		this.numeroConsecutivoRunt = numeroConsecutivoRunt;
	}
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public String getServicio() {
		return servicio;
	}
	public void setServicio(String servicio) {
		this.servicio = servicio;
	}
	public String getClase() {
		return clase;
	}
	public void setClase(String clase) {
		this.clase = clase;
	}
	public String getCilindraje() {
		return cilindraje;
	}
	public void setCilindraje(String cilindraje) {
		this.cilindraje = cilindraje;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getLinea() {
		return linea;
	}
	public void setLinea(String linea) {
		this.linea = linea;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getFechaMatricula() {
		return fechaMatricula;
	}
	public void setFechaMatricula(String fechaMatricula) {
		this.fechaMatricula = fechaMatricula;
	}
	public String getCombustible() {
		return combustible;
	}
	public void setCombustible(String combustible) {
		this.combustible = combustible;
	}
	public String getTipoMotor() {
		return tipoMotor;
	}
	public void setTipoMotor(String tipoMotor) {
		this.tipoMotor = tipoMotor;
	}
	public String getRuidoEscape() {
		return ruidoEscape;
	}
	public void setRuidoEscape(String ruidoEscape) {
		this.ruidoEscape = ruidoEscape;
	}
	public String getIntensidadBajaDerecha() {
		return intensidadBajaDerecha;
	}
	public void setIntensidadBajaDerecha(String intensidadBajaDerecha) {
		this.intensidadBajaDerecha = intensidadBajaDerecha;
	}
	public String getIntensidadBajaIzquierda() {
		return intensidadBajaIzquierda;
	}
	public void setIntensidadBajaIzquierda(String intensidadBajaIzquierda) {
		this.intensidadBajaIzquierda = intensidadBajaIzquierda;
	}
	public String getInclinacionBajaDerecha() {
		return inclinacionBajaDerecha;
	}
	public void setInclinacionBajaDerecha(String inclinacionBajaDerecha) {
		this.inclinacionBajaDerecha = inclinacionBajaDerecha;
	}
	public String getInclinacionBajaIzquierda() {
		return inclinacionBajaIzquierda;
	}
	public void setInclinacionBajaIzquierda(String inclinacionBajaIzquierda) {
		this.inclinacionBajaIzquierda = inclinacionBajaIzquierda;
	}
	public String getIntensidadTotal() {
		return intensidadTotal;
	}
	public void setIntensidadTotal(String intensidadTotal) {
		this.intensidadTotal = intensidadTotal;
	}
	public String getDelanteraDerecha() {
		return delanteraDerecha;
	}
	public void setDelanteraDerecha(String delanteraDerecha) {
		this.delanteraDerecha = delanteraDerecha;
	}
	public String getDelanteraIzquierda() {
		return delanteraIzquierda;
	}
	public void setDelanteraIzquierda(String delanteraIzquierda) {
		this.delanteraIzquierda = delanteraIzquierda;
	}
	public String getTraseraDerecha() {
		return traseraDerecha;
	}
	public void setTraseraDerecha(String traseraDerecha) {
		this.traseraDerecha = traseraDerecha;
	}
	public String getTraseraIzquierda() {
		return traseraIzquierda;
	}
	public void setTraseraIzquierda(String traseraIzquierda) {
		this.traseraIzquierda = traseraIzquierda;
	}
	public String getEficaciaTotal() {
		return eficaciaTotal;
	}
	public void setEficaciaTotal(String eficaciaTotal) {
		this.eficaciaTotal = eficaciaTotal;
	}
	public String getEficaciaAuxiliar() {
		return eficaciaAuxiliar;
	}
	public void setEficaciaAuxiliar(String eficaciaAuxiliar) {
		this.eficaciaAuxiliar = eficaciaAuxiliar;
	}
	public String getFuerzaEje1Derecho() {
		return fuerzaEje1Derecho;
	}
	public void setFuerzaEje1Derecho(String fuerzaEje1Derecho) {
		this.fuerzaEje1Derecho = fuerzaEje1Derecho;
	}
	public String getFuerzaEje2Derecho() {
		return fuerzaEje2Derecho;
	}
	public void setFuerzaEje2Derecho(String fuerzaEje2Derecho) {
		this.fuerzaEje2Derecho = fuerzaEje2Derecho;
	}
	public String getFuerzaEje4Derecho() {
		return fuerzaEje4Derecho;
	}
	public void setFuerzaEje4Derecho(String fuerzaEje4Derecho) {
		this.fuerzaEje4Derecho = fuerzaEje4Derecho;
	}
	public String getFuerzaEje5Derecho() {
		return fuerzaEje5Derecho;
	}
	public void setFuerzaEje5Derecho(String fuerzaEje5Derecho) {
		this.fuerzaEje5Derecho = fuerzaEje5Derecho;
	}
	public String getFuerzaEje1Izquierdo() {
		return fuerzaEje1Izquierdo;
	}
	public void setFuerzaEje1Izquierdo(String fuerzaEje1Izquierdo) {
		this.fuerzaEje1Izquierdo = fuerzaEje1Izquierdo;
	}
	public String getFuerzaEje2Izquierdo() {
		return fuerzaEje2Izquierdo;
	}
	public void setFuerzaEje2Izquierdo(String fuerzaEje2Izquierdo) {
		this.fuerzaEje2Izquierdo = fuerzaEje2Izquierdo;
	}
	public String getFuerzaEje3Izquierdo() {
		return fuerzaEje3Izquierdo;
	}
	public void setFuerzaEje3Izquierdo(String fuerzaEje3Izquierdo) {
		this.fuerzaEje3Izquierdo = fuerzaEje3Izquierdo;
	}
	public String getFuerzaEje4Izquierdo() {
		return fuerzaEje4Izquierdo;
	}
	public void setFuerzaEje4Izquierdo(String fuerzaEje4Izquierdo) {
		this.fuerzaEje4Izquierdo = fuerzaEje4Izquierdo;
	}
	public String getFuerzaEje5Izquierdo() {
		return fuerzaEje5Izquierdo;
	}
	public void setFuerzaEje5Izquierdo(String fuerzaEje5Izquierdo) {
		this.fuerzaEje5Izquierdo = fuerzaEje5Izquierdo;
	}
	public String getPesoEje1Derecho() {
		return pesoEje1Derecho;
	}
	public void setPesoEje1Derecho(String pesoEje1Derecho) {
		this.pesoEje1Derecho = pesoEje1Derecho;
	}
	public String getPesoEje2Derecho() {
		return pesoEje2Derecho;
	}
	public void setPesoEje2Derecho(String pesoEje2Derecho) {
		this.pesoEje2Derecho = pesoEje2Derecho;
	}
	public String getPesoEje3Derecho() {
		return pesoEje3Derecho;
	}
	public void setPesoEje3Derecho(String pesoEje3Derecho) {
		this.pesoEje3Derecho = pesoEje3Derecho;
	}
	public String getPesoEje4Derecho() {
		return pesoEje4Derecho;
	}
	public void setPesoEje4Derecho(String pesoEje4Derecho) {
		this.pesoEje4Derecho = pesoEje4Derecho;
	}
	public String getPesoEje5Derecho() {
		return pesoEje5Derecho;
	}
	public void setPesoEje5Derecho(String pesoEje5Derecho) {
		this.pesoEje5Derecho = pesoEje5Derecho;
	}
	public String getDesequilibrioEje1() {
		return desequilibrioEje1;
	}
	public void setDesequilibrioEje1(String desequilibrioEje1) {
		this.desequilibrioEje1 = desequilibrioEje1;
	}
	public String getDesequilibrioEje2() {
		return desequilibrioEje2;
	}
	public void setDesequilibrioEje2(String desequilibrioEje2) {
		this.desequilibrioEje2 = desequilibrioEje2;
	}
	public String getDesequilibrioEje3() {
		return desequilibrioEje3;
	}
	public void setDesequilibrioEje3(String desequilibrioEje3) {
		this.desequilibrioEje3 = desequilibrioEje3;
	}
	public String getDesequilibrioEje4() {
		return desequilibrioEje4;
	}
	public void setDesequilibrioEje4(String desequilibrioEje4) {
		this.desequilibrioEje4 = desequilibrioEje4;
	}
	public String getDesequilibrioEje5() {
		return desequilibrioEje5;
	}
	public void setDesequilibrioEje5(String desequilibrioEje5) {
		this.desequilibrioEje5 = desequilibrioEje5;
	}
	public String getDesviacionEje1() {
		return desviacionEje1;
	}
	public void setDesviacionEje1(String desviacionEje1) {
		this.desviacionEje1 = desviacionEje1;
	}
	public String getDesviacionEje2() {
		return desviacionEje2;
	}
	public void setDesviacionEje2(String desviacionEje2) {
		this.desviacionEje2 = desviacionEje2;
	}
	public String getDesviacionEje3() {
		return desviacionEje3;
	}
	public void setDesviacionEje3(String desviacionEje3) {
		this.desviacionEje3 = desviacionEje3;
	}
	public String getDesviacionEje4() {
		return desviacionEje4;
	}
	public void setDesviacionEje4(String desviacionEje4) {
		this.desviacionEje4 = desviacionEje4;
	}
	public String getDesviacionEje5() {
		return desviacionEje5;
	}
	public void setDesviacionEje5(String desviacionEje5) {
		this.desviacionEje5 = desviacionEje5;
	}
	public String getReferenciaComercialLlanta() {
		return referenciaComercialLlanta;
	}
	public void setReferenciaComercialLlanta(String referenciaComercialLlanta) {
		this.referenciaComercialLlanta = referenciaComercialLlanta;
	}
	public String getErrorDistancia() {
		return errorDistancia;
	}
	public void setErrorDistancia(String errorDistancia) {
		this.errorDistancia = errorDistancia;
	}
	public String getErrorTiempo() {
		return errorTiempo;
	}
	public void setErrorTiempo(String errorTiempo) {
		this.errorTiempo = errorTiempo;
	}
	public String getTemperatura() {
		return temperatura;
	}
	public void setTemperatura(String temperatura) {
		this.temperatura = temperatura;
	}
	public String getRpmCrucero() {
		return rpmCrucero;
	}
	public void setRpmCrucero(String rpmCrucero) {
		this.rpmCrucero = rpmCrucero;
	}
	public String getRpmRalenti() {
		return rpmRalenti;
	}
	public void setRpmRalenti(String rpmRalenti) {
		this.rpmRalenti = rpmRalenti;
	}
	public String getCoRalenti() {
		return coRalenti;
	}
	public void setCoRalenti(String coRalenti) {
		this.coRalenti = coRalenti;
	}
	public String getCoCrucero() {
		return coCrucero;
	}
	public void setCoCrucero(String coCrucero) {
		this.coCrucero = coCrucero;
	}
	public String getCo2Ralenti() {
		return co2Ralenti;
	}
	public void setCo2Ralenti(String co2Ralenti) {
		this.co2Ralenti = co2Ralenti;
	}
	public String getCo2Crucero() {
		return co2Crucero;
	}
	public void setCo2Crucero(String co2Crucero) {
		this.co2Crucero = co2Crucero;
	}
	public String getO2Ralenti() {
		return o2Ralenti;
	}
	public void setO2Ralenti(String o2Ralenti) {
		this.o2Ralenti = o2Ralenti;
	}
	public String getO2Crucero() {
		return o2Crucero;
	}
	public void setO2Crucero(String o2Crucero) {
		this.o2Crucero = o2Crucero;
	}
	public String getHcRalenti() {
		return hcRalenti;
	}
	public void setHcRalenti(String hcRalenti) {
		this.hcRalenti = hcRalenti;
	}
	public String getHcCrucero() {
		return hcCrucero;
	}
	public void setHcCrucero(String hcCrucero) {
		this.hcCrucero = hcCrucero;
	}
	public String getRpmRalentiDiesel() {
		return rpmRalentiDiesel;
	}
	public void setRpmRalentiDiesel(String rpmRalentiDiesel) {
		this.rpmRalentiDiesel = rpmRalentiDiesel;
	}
	public String getRpmGobernadaDiesel() {
		return rpmGobernadaDiesel;
	}
	public void setRpmGobernadaDiesel(String rpmGobernadaDiesel) {
		this.rpmGobernadaDiesel = rpmGobernadaDiesel;
	}
	public String getTemperaturaDiesel() {
		return temperaturaDiesel;
	}
	public void setTemperaturaDiesel(String temperaturaDiesel) {
		this.temperaturaDiesel = temperaturaDiesel;
	}
	public String getOpacidadCiclo1() {
		return opacidadCiclo1;
	}
	public void setOpacidadCiclo1(String opacidadCiclo1) {
		this.opacidadCiclo1 = opacidadCiclo1;
	}
	public String getOpacidadCiclo2() {
		return opacidadCiclo2;
	}
	public void setOpacidadCiclo2(String opacidadCiclo2) {
		this.opacidadCiclo2 = opacidadCiclo2;
	}
	public String getOpacidadCiclo3() {
		return opacidadCiclo3;
	}
	public void setOpacidadCiclo3(String opacidadCiclo3) {
		this.opacidadCiclo3 = opacidadCiclo3;
	}
	public String getOpacidadCiclo4() {
		return opacidadCiclo4;
	}
	public void setOpacidadCiclo4(String opacidadCiclo4) {
		this.opacidadCiclo4 = opacidadCiclo4;
	}
	public String getResultado() {
		return resultado;
	}
	public void setResultado(String resultado) {
		this.resultado = resultado;
	}
	public String getFuerzaEje3Derecho() {
		return fuerzaEje3Derecho;
	}
	public void setFuerzaEje3Derecho(String fuerzaEje3Derecho) {
		this.fuerzaEje3Derecho = fuerzaEje3Derecho;
	}
	public String getPesoEje1Izquierdo() {
		return pesoEje1Izquierdo;
	}
	public void setPesoEje1Izquierdo(String pesoEje1Izquierdo) {
		this.pesoEje1Izquierdo = pesoEje1Izquierdo;
	}
	public String getPesoEje2Izquierdo() {
		return pesoEje2Izquierdo;
	}
	public void setPesoEje2Izquierdo(String pesoEje2Izquierdo) {
		this.pesoEje2Izquierdo = pesoEje2Izquierdo;
	}
	public String getPesoEje3Izquierdo() {
		return pesoEje3Izquierdo;
	}
	public void setPesoEje3Izquierdo(String pesoEje3Izquierdo) {
		this.pesoEje3Izquierdo = pesoEje3Izquierdo;
	}
	public String getPesoEje4Izquierdo() {
		return pesoEje4Izquierdo;
	}
	public void setPesoEje4Izquierdo(String pesoEje4Izquierdo) {
		this.pesoEje4Izquierdo = pesoEje4Izquierdo;
	}
	public String getPesoEje5Izquierdo() {
		return pesoEje5Izquierdo;
	}
	public void setPesoEje5Izquierdo(String pesoEje5Izquierdo) {
		this.pesoEje5Izquierdo = pesoEje5Izquierdo;
	}
	public String getTempRalenti() {
		return tempRalenti;
	}
	public void setTempRalenti(String tempRalenti) {
		this.tempRalenti = tempRalenti;
	}
	public String getTempCrucero() {
		return tempCrucero;
	}
	public void setTempCrucero(String tempCrucero) {
		this.tempCrucero = tempCrucero;
	}
	public String getProfEje1Der() {
		return profEje1Der;
	}
	public void setProfEje1Der(String profEje1Der) {
		this.profEje1Der = profEje1Der;
	}
	public String getProfEje1Izq() {
		return profEje1Izq;
	}
	public void setProfEje1Izq(String profEje1Izq) {
		this.profEje1Izq = profEje1Izq;
	}
	public String getProfEje2Der1() {
		return profEje2Der1;
	}
	public void setProfEje2Der1(String profEje2Der1) {
		this.profEje2Der1 = profEje2Der1;
	}
	public String getProfEje2Der2() {
		return profEje2Der2;
	}
	public void setProfEje2Der2(String profEje2Der2) {
		this.profEje2Der2 = profEje2Der2;
	}
	public String getProfEje2Izq1() {
		return profEje2Izq1;
	}
	public void setProfEje2Izq1(String profEje2Izq1) {
		this.profEje2Izq1 = profEje2Izq1;
	}
	public String getProfEje2Izq2() {
		return profEje2Izq2;
	}
	public void setProfEje2Izq2(String profEje2Izq2) {
		this.profEje2Izq2 = profEje2Izq2;
	}
	public String getProfEje3Der1() {
		return profEje3Der1;
	}
	public void setProfEje3Der1(String profEje3Der1) {
		this.profEje3Der1 = profEje3Der1;
	}
	public String getProfEje3Der2() {
		return profEje3Der2;
	}
	public void setProfEje3Der2(String profEje3Der2) {
		this.profEje3Der2 = profEje3Der2;
	}
	public String getProfEje3Izq1() {
		return profEje3Izq1;
	}
	public void setProfEje3Izq1(String profEje3Izq1) {
		this.profEje3Izq1 = profEje3Izq1;
	}
	public String getProfEje3Izq2() {
		return profEje3Izq2;
	}
	public void setProfEje3Izq2(String profEje3Izq2) {
		this.profEje3Izq2 = profEje3Izq2;
	}
	public String getProfEje4Der1() {
		return profEje4Der1;
	}
	public void setProfEje4Der1(String profEje4Der1) {
		this.profEje4Der1 = profEje4Der1;
	}
	public String getProfEje4Der2() {
		return profEje4Der2;
	}
	public void setProfEje4Der2(String profEje4Der2) {
		this.profEje4Der2 = profEje4Der2;
	}
	public String getProfEje4Izq1() {
		return profEje4Izq1;
	}
	public void setProfEje4Izq1(String profEje4Izq1) {
		this.profEje4Izq1 = profEje4Izq1;
	}
	public String getProfEje4Izq2() {
		return profEje4Izq2;
	}
	public void setProfEje4Izq2(String profEje4Izq2) {
		this.profEje4Izq2 = profEje4Izq2;
	}
	public String getProfEje5Der1() {
		return profEje5Der1;
	}
	public void setProfEje5Der1(String profEje5Der1) {
		this.profEje5Der1 = profEje5Der1;
	}
	public String getProfEje5Der2() {
		return profEje5Der2;
	}
	public void setProfEje5Der2(String profEje5Der2) {
		this.profEje5Der2 = profEje5Der2;
	}
	public String getProfEje5Izq1() {
		return profEje5Izq1;
	}
	public void setProfEje5Izq1(String profEje5Izq1) {
		this.profEje5Izq1 = profEje5Izq1;
	}
	public String getProfEje5Izq2() {
		return profEje5Izq2;
	}
	public void setProfEje5Izq2(String profEje5Izq2) {
		this.profEje5Izq2 = profEje5Izq2;
	}
	public String getProfRepDer() {
		return profRepDer;
	}
	public void setProfRepDer(String profRepDer) {
		this.profRepDer = profRepDer;
	}
	public String getProfRepIzq() {
		return profRepIzq;
	}
	public void setProfRepIzq(String profRepIzq) {
		this.profRepIzq = profRepIzq;
	}
	
	
	

}
