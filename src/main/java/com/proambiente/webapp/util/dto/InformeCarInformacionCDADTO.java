package com.proambiente.webapp.util.dto;

public class InformeCarInformacionCDADTO {

	protected String noCda;
	protected String nombreCda;
	protected String nitCda;
	protected String direccionCda;
	protected String telefono1;
	protected String telefono2;
	protected String ciudad;
	protected String numeroResolucion;
	protected String fechaResolucion;
	
	protected String numeroCertificadoRTM;
	
	protected String consecutivoPrueba;
	protected String fechaInicioPrueba;
	protected String fechaFinalPrueba;
	protected String fechaAbortoPrueba;
	protected String inspectorRealizaPrueba;
	
	protected String causaAbortoPrueba;
	protected String resultadoPrueba;
	
	protected String nombreDelSoftware;
	protected String versionDelPrograma;
	
	
	protected String marca;
	protected String linea;
	protected String modelo;
	protected String placa;
	
	protected String tiemposMotor;
	protected String disenio;
	
	protected String cilindraje;
	protected String claseVehiculo;
	protected String servicio;
	protected String combustible;
	protected String numeroMotor;
	protected String vin;
	protected String licenciaTransito;
	protected String modificacionesMotor;
	protected String kilometraje;
	protected String potenciaDelMotor;
	protected String ltoeDiametro;
	
	
	protected String nombreRazonSocial;
	protected String tipoDocumento;
	protected String numeroDocumento;
	protected String direccion;
	protected String telefono;
	protected String ciudadPropietario;
	
	
	
	public InformeCarInformacionCDADTO() {
		super();
	}

	public String getNoCda() {
		return noCda;
	}

	public String getNombreCda() {
		return nombreCda;
	}

	public void setNombreCda(String nombreCda) {
		this.nombreCda = nombreCda;
	}

	public String getNitCda() {
		return nitCda;
	}

	public void setNitCda(String nitCda) {
		this.nitCda = nitCda;
	}

	public String getDireccionCda() {
		return direccionCda;
	}

	public void setDireccionCda(String direccionCda) {
		this.direccionCda = direccionCda;
	}

	public String getTelefono1() {
		return telefono1;
	}

	public void setTelefono1(String telefono1) {
		this.telefono1 = telefono1;
	}

	public String getTelefono2() {
		return telefono2;
	}

	public void setTelefono2(String telefono2) {
		this.telefono2 = telefono2;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getNumeroResolucion() {
		return numeroResolucion;
	}

	public void setNumeroResolucion(String numeroResolucion) {
		this.numeroResolucion = numeroResolucion;
	}

	public String getFechaResolucion() {
		return fechaResolucion;
	}

	public void setFechaResolucion(String fechaResolucion) {
		this.fechaResolucion = fechaResolucion;
	}

	public void setNoCda(String noCda) {
		this.noCda = noCda;
	}

	public String getConsecutivoPrueba() {
		return consecutivoPrueba;
	}

	public void setConsecutivoPrueba(String consecutivoPrueba) {
		this.consecutivoPrueba = consecutivoPrueba;
	}

	public String getFechaInicioPrueba() {
		return fechaInicioPrueba;
	}

	public void setFechaInicioPrueba(String fechaInicioPrueba) {
		this.fechaInicioPrueba = fechaInicioPrueba;
	}

	public String getFechaFinalPrueba() {
		return fechaFinalPrueba;
	}

	public void setFechaFinalPrueba(String fechaFinalPrueba) {
		this.fechaFinalPrueba = fechaFinalPrueba;
	}

	public String getFechaAbortoPrueba() {
		return fechaAbortoPrueba;
	}

	public void setFechaAbortoPrueba(String fechaAbortoPrueba) {
		this.fechaAbortoPrueba = fechaAbortoPrueba;
	}

	public String getInspectorRealizaPrueba() {
		return inspectorRealizaPrueba;
	}

	public void setInspectorRealizaPrueba(String inspectorRealizaPrueba) {
		this.inspectorRealizaPrueba = inspectorRealizaPrueba;
	}
	
	public String getCausaAbortoPrueba() {
		return causaAbortoPrueba;
	}
	public void setCausaAbortoPrueba(String causaAbortoPrueba) {
		this.causaAbortoPrueba = causaAbortoPrueba;
	}

	public String getResultadoPrueba() {
		return resultadoPrueba;
	}

	public void setResultadoPrueba(String resultadoPrueba) {
		this.resultadoPrueba = resultadoPrueba;
	}

	public String getNombreDelSoftware() {
		return nombreDelSoftware;
	}

	public void setNombreDelSoftware(String nombreDelSoftware) {
		this.nombreDelSoftware = nombreDelSoftware;
	}

	public String getVersionDelPrograma() {
		return versionDelPrograma;
	}

	public void setVersionDelPrograma(String versionDelPrograma) {
		this.versionDelPrograma = versionDelPrograma;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getLinea() {
		return linea;
	}

	public void setLinea(String linea) {
		this.linea = linea;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getCilindraje() {
		return cilindraje;
	}

	public void setCilindraje(String cilindraje) {
		this.cilindraje = cilindraje;
	}

	public String getClaseVehiculo() {
		return claseVehiculo;
	}

	public void setClaseVehiculo(String claseVehiculo) {
		this.claseVehiculo = claseVehiculo;
	}

	public String getServicio() {
		return servicio;
	}

	public void setServicio(String servicio) {
		this.servicio = servicio;
	}

	public String getCombustible() {
		return combustible;
	}

	public void setCombustible(String combustible) {
		this.combustible = combustible;
	}

	public String getNumeroMotor() {
		return numeroMotor;
	}

	public void setNumeroMotor(String numeroMotor) {
		this.numeroMotor = numeroMotor;
	}

	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public String getLicenciaTransito() {
		return licenciaTransito;
	}

	public void setLicenciaTransito(String licenciaTransito) {
		this.licenciaTransito = licenciaTransito;
	}

	public String getModificacionesMotor() {
		return modificacionesMotor;
	}

	public void setModificacionesMotor(String modificacionesMotor) {
		this.modificacionesMotor = modificacionesMotor;
	}

	public String getKilometraje() {
		return kilometraje;
	}

	public void setKilometraje(String kilometraje) {
		this.kilometraje = kilometraje;
	}

	public String getPotenciaDelMotor() {
		return potenciaDelMotor;
	}

	public void setPotenciaDelMotor(String potenciaDelMotor) {
		this.potenciaDelMotor = potenciaDelMotor;
	}

	public String getLtoeDiametro() {
		return ltoeDiametro;
	}

	public void setLtoeDiametro(String ltoeDiametro) {
		this.ltoeDiametro = ltoeDiametro;
	}

	public String getNombreRazonSocial() {
		return nombreRazonSocial;
	}

	public void setNombreRazonSocial(String nombreRazonSocial) {
		this.nombreRazonSocial = nombreRazonSocial;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCiudadPropietario() {
		return ciudadPropietario;
	}

	public void setCiudadPropietario(String ciudadPropietario) {
		this.ciudadPropietario = ciudadPropietario;
	}

	public String getTiemposMotor() {
		return tiemposMotor;
	}

	public void setTiemposMotor(String tiemposMotor) {
		this.tiemposMotor = tiemposMotor;
	}

	public String getDisenio() {
		return disenio;
	}

	public void setDisenio(String disenio) {
		this.disenio = disenio;
	}

	public String getNumeroCertificadoRTM() {
		return numeroCertificadoRTM;
	}

	public void setNumeroCertificadoRTM(String numeroCertificadoRTM) {
		this.numeroCertificadoRTM = numeroCertificadoRTM;
	}
	
	

}