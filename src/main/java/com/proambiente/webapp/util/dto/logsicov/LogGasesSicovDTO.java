package com.proambiente.webapp.util.dto.logsicov;

public class LogGasesSicovDTO {
	
	  private String temperaturaAmbiente= "" ; 
	  private String rpmRalenti= "" ; 
	  private String tempRalenti= "" ; 
	  private String humedadRelativa= "" ; 
	  private String velocidadGobernada0= "" ; 
	  private String velocidadGobernada1= "" ; 
	  private String velocidadGobernada2= "" ; 
	  private String velocidadGobernada3= "" ; 
	  private String opacidad0= "" ; 
	  private String opacidad1= "" ; 
	  private String opacidad2= "" ; 
	  private String opacidad3= "" ; 
	  private String valorFinal= "" ; 
	  private String temperaturaInicial= "" ; 
	  private String temperaturaFinal= "" ; 
	  private String LTOEStandar= "" ; 
	  private String HCRalenti= "" ; 
	  private String CORalenti= "" ; 
	  private String CO2Ralenti= "" ; 
	  private String O2Ralenti= "" ; 
	  private String rpmCrucero= "" ; 
	  private String HCCrucero= "" ; 
	  private String COCrucero= "" ; 
	  private String CO2Crucero= "" ; 
	  private String O2Crucero= "" ; 
	  private String dilucion= "" ; 
	  private String catalizador= "" ; 
	  private String temperaturaPrueba= "" ; 
	  private String tablaAfectada= "" ; 
	  private String idRegistro= "" ;  
	
	
	public String getTemperaturaAmbiente() {
		return temperaturaAmbiente;
	}
	public void setTemperaturaAmbiente(String temperaturaAmbiente) {
		this.temperaturaAmbiente = temperaturaAmbiente;
	}
	public String getRpmRalenti() {
		return rpmRalenti;
	}
	public void setRpmRalenti(String rpmRalenti) {
		this.rpmRalenti = rpmRalenti;
	}
	public String getTempRalenti() {
		return tempRalenti;
	}
	public void setTempRalenti(String tempRalenti) {
		this.tempRalenti = tempRalenti;
	}
	public String getHumedadRelativa() {
		return humedadRelativa;
	}
	public void setHumedadRelativa(String humedadRelativa) {
		this.humedadRelativa = humedadRelativa;
	}
	public String getVelocidadGobernada0() {
		return velocidadGobernada0;
	}
	public void setVelocidadGobernada0(String velocidadGobernada0) {
		this.velocidadGobernada0 = velocidadGobernada0;
	}
	public String getVelocidadGobernada1() {
		return velocidadGobernada1;
	}
	public void setVelocidadGobernada1(String velocidadGobernada1) {
		this.velocidadGobernada1 = velocidadGobernada1;
	}
	public String getVelocidadGobernada2() {
		return velocidadGobernada2;
	}
	public void setVelocidadGobernada2(String velocidadGobernada2) {
		this.velocidadGobernada2 = velocidadGobernada2;
	}
	public String getVelocidadGobernada3() {
		return velocidadGobernada3;
	}
	public void setVelocidadGobernada3(String velocidadGobernada3) {
		this.velocidadGobernada3 = velocidadGobernada3;
	}
	public String getOpacidad0() {
		return opacidad0;
	}
	public void setOpacidad0(String opacidad0) {
		this.opacidad0 = opacidad0;
	}
	public String getOpacidad1() {
		return opacidad1;
	}
	public void setOpacidad1(String opacidad1) {
		this.opacidad1 = opacidad1;
	}
	public String getOpacidad2() {
		return opacidad2;
	}
	public void setOpacidad2(String opacidad2) {
		this.opacidad2 = opacidad2;
	}
	public String getOpacidad3() {
		return opacidad3;
	}
	public void setOpacidad3(String opacidad3) {
		this.opacidad3 = opacidad3;
	}
	public String getValorFinal() {
		return valorFinal;
	}
	public void setValorFinal(String valorFinal) {
		this.valorFinal = valorFinal;
	}
	public String getTemperaturaInicial() {
		return temperaturaInicial;
	}
	public void setTemperaturaInicial(String temperaturaInicial) {
		this.temperaturaInicial = temperaturaInicial;
	}
	public String getTemperaturaFinal() {
		return temperaturaFinal;
	}
	public void setTemperaturaFinal(String temperaturaFinal) {
		this.temperaturaFinal = temperaturaFinal;
	}
	public String getLTOEStandar() {
		return LTOEStandar;
	}
	public void setLTOEStandar(String lTOEStandar) {
		LTOEStandar = lTOEStandar;
	}
	public String getHCRalenti() {
		return HCRalenti;
	}
	public void setHCRalenti(String hCRalenti) {
		HCRalenti = hCRalenti;
	}
	public String getCORalenti() {
		return CORalenti;
	}
	public void setCORalenti(String cORalenti) {
		CORalenti = cORalenti;
	}
	public String getCO2Ralenti() {
		return CO2Ralenti;
	}
	public void setCO2Ralenti(String cO2Ralenti) {
		CO2Ralenti = cO2Ralenti;
	}
	public String getO2Ralenti() {
		return O2Ralenti;
	}
	public void setO2Ralenti(String o2Ralenti) {
		O2Ralenti = o2Ralenti;
	}
	public String getRpmCrucero() {
		return rpmCrucero;
	}
	public void setRpmCrucero(String rpmCrucero) {
		this.rpmCrucero = rpmCrucero;
	}
	public String getHCCrucero() {
		return HCCrucero;
	}
	public void setHCCrucero(String hCCrucero) {
		HCCrucero = hCCrucero;
	}
	public String getCOCrucero() {
		return COCrucero;
	}
	public void setCOCrucero(String cOCrucero) {
		COCrucero = cOCrucero;
	}
	public String getCO2Crucero() {
		return CO2Crucero;
	}
	public void setCO2Crucero(String cO2Crucero) {
		CO2Crucero = cO2Crucero;
	}
	public String getO2Crucero() {
		return O2Crucero;
	}
	public void setO2Crucero(String o2Crucero) {
		O2Crucero = o2Crucero;
	}
	public String getDilucion() {
		return dilucion;
	}
	public void setDilucion(String dilucion) {
		this.dilucion = dilucion;
	}
	public String getCatalizador() {
		return catalizador;
	}
	public void setCatalizador(String catalizador) {
		this.catalizador = catalizador;
	}
	public String getTemperaturaPrueba() {
		return temperaturaPrueba;
	}
	public void setTemperaturaPrueba(String temperaturaPrueba) {
		this.temperaturaPrueba = temperaturaPrueba;
	}
	public String getTablaAfectada() {
		return tablaAfectada;
	}
	public void setTablaAfectada(String tablaAfectada) {
		this.tablaAfectada = tablaAfectada;
	}
	public String getIdRegistro() {
		return idRegistro;
	}
	public void setIdRegistro(String idRegistro) {
		this.idRegistro = idRegistro;
	}
	  
	  

}
