package com.proambiente.webapp.util.dto.sda;

public enum CausaRechazoMotocicletas {
	
	SIN_RECHAZO(0),
	FUGAS_TUBO_ESCAPE(1),
	FUGAS_SILENCIADOR(2),
	AUSENCIA_TAPA_ACEITE(3),
	AUSENCIA_TAPA_COMBUSTIBLE(4),
	SALIDAS_ADICIONALES(5),
	HUMO_NEGRO_AZUL(7),
	REVOLUCIONES_FUERA_RANGO(8),
	CO_CUATRO_TIEMPOS(18),
	HC_CUATRO_TIEMPOS(17),
	CO_DOS_TIEMPOS_MENOR_2009(13),
	HC_DOS_TIEMPOS_MENOR_2009(14),
	CO_DOS_TIEMPOS_2010_MAYOR(15),
	HC_DOS_TIEMPOS_2010_MAYOR(16);
	
	private final int value;

	private CausaRechazoMotocicletas(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}
}
