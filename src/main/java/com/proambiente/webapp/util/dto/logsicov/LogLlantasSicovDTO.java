package com.proambiente.webapp.util.dto.logsicov;

public class LogLlantasSicovDTO {
	
	  private String derProfundidadEje1= "";
	  private String derProfundidadExternaEje2= "";
	  private String derProfundidadExternaEje3= "";
	  private String derProfundidadExternaEje4= "";
	  private String derProfundidadExternaEje5= "";
	  private String izqProfundidadEje1= "";
	  private String izqProfundidadExternaEje2= "";
	  private String izqProfundidadExternaEje3= "";
	  private String izqProfundidadExternaEje4= "";
	  private String izqProfundidadExternaEje5= "";
	  private String derProfundidadInternaEje2= "";
	  private String derProfundidadInternaEje3= "";
	  private String derProfundidadInternaEje4= "";
	  private String derProfundidadInternaEje5= "";
	  private String izqProfundidadInternaEje2= "";
	  private String izqProfundidadInternaEje3= "";
	  private String izqProfundidadInternaEje4= "";
	  private String izqProfundidadInternaEje5= "";
	  private String repuestoProfundidad= "";
	  private String repuesto2Profundidad= "";
	  private String derPresionEje1= "";
	  private String derPresionExternaEje2= "";
	  private String derPresionExternaEje3= "";
	  private String derPresionExternaEje4= "";
	  private String derPresionExternaEje5= "";
	  private String IzqPresionEje1= "";
	  private String IzqPresionExternaEje2= "";
	  private String IzqPresionExternaEje3= "";
	  private String IzqPresionExternaEje4= "";
	  private String IzqPresionExternaEje5= "";
	  private String derPresionInternaEje2= "";
	  private String derPresionInternaEje3= "";
	  private String derPresionInternaEje4= "";
	  private String derPresionInternaEje5= "";
	  private String IzqPresionInternaEje2= "";
	  private String IzqPresionInternaEje3= "";
	  private String IzqPresionInternaEje4= "";
	  private String IzqPresionInternaEje5= "";
	  private String RepuestoPresion= "";
	  private String Repuesto2Presion= "";
	  private String tablaAfectada= "";
	  private String idRegistro = "";
	public String getDerProfundidadEje1() {
		return derProfundidadEje1;
	}
	public void setDerProfundidadEje1(String derProfundidadEje1) {
		this.derProfundidadEje1 = derProfundidadEje1;
	}
	public String getDerProfundidadExternaEje2() {
		return derProfundidadExternaEje2;
	}
	public void setDerProfundidadExternaEje2(String derProfundidadExternaEje2) {
		this.derProfundidadExternaEje2 = derProfundidadExternaEje2;
	}
	public String getDerProfundidadExternaEje3() {
		return derProfundidadExternaEje3;
	}
	public void setDerProfundidadExternaEje3(String derProfundidadExternaEje3) {
		this.derProfundidadExternaEje3 = derProfundidadExternaEje3;
	}
	public String getDerProfundidadExternaEje4() {
		return derProfundidadExternaEje4;
	}
	public void setDerProfundidadExternaEje4(String derProfundidadExternaEje4) {
		this.derProfundidadExternaEje4 = derProfundidadExternaEje4;
	}
	public String getDerProfundidadExternaEje5() {
		return derProfundidadExternaEje5;
	}
	public void setDerProfundidadExternaEje5(String derProfundidadExternaEje5) {
		this.derProfundidadExternaEje5 = derProfundidadExternaEje5;
	}
	public String getIzqProfundidadEje1() {
		return izqProfundidadEje1;
	}
	public void setIzqProfundidadEje1(String izqProfundidadEje1) {
		this.izqProfundidadEje1 = izqProfundidadEje1;
	}
	public String getIzqProfundidadExternaEje2() {
		return izqProfundidadExternaEje2;
	}
	public void setIzqProfundidadExternaEje2(String izqProfundidadExternaEje2) {
		this.izqProfundidadExternaEje2 = izqProfundidadExternaEje2;
	}
	public String getIzqProfundidadExternaEje3() {
		return izqProfundidadExternaEje3;
	}
	public void setIzqProfundidadExternaEje3(String izqProfundidadExternaEje3) {
		this.izqProfundidadExternaEje3 = izqProfundidadExternaEje3;
	}
	public String getIzqProfundidadExternaEje4() {
		return izqProfundidadExternaEje4;
	}
	public void setIzqProfundidadExternaEje4(String izqProfundidadExternaEje4) {
		this.izqProfundidadExternaEje4 = izqProfundidadExternaEje4;
	}
	public String getIzqProfundidadExternaEje5() {
		return izqProfundidadExternaEje5;
	}
	public void setIzqProfundidadExternaEje5(String izqProfundidadExternaEje5) {
		this.izqProfundidadExternaEje5 = izqProfundidadExternaEje5;
	}
	public String getDerProfundidadInternaEje2() {
		return derProfundidadInternaEje2;
	}
	public void setDerProfundidadInternaEje2(String derProfundidadInternaEje2) {
		this.derProfundidadInternaEje2 = derProfundidadInternaEje2;
	}
	public String getDerProfundidadInternaEje3() {
		return derProfundidadInternaEje3;
	}
	public void setDerProfundidadInternaEje3(String derProfundidadInternaEje3) {
		this.derProfundidadInternaEje3 = derProfundidadInternaEje3;
	}
	public String getDerProfundidadInternaEje4() {
		return derProfundidadInternaEje4;
	}
	public void setDerProfundidadInternaEje4(String derProfundidadInternaEje4) {
		this.derProfundidadInternaEje4 = derProfundidadInternaEje4;
	}
	public String getDerProfundidadInternaEje5() {
		return derProfundidadInternaEje5;
	}
	public void setDerProfundidadInternaEje5(String derProfundidadInternaEje5) {
		this.derProfundidadInternaEje5 = derProfundidadInternaEje5;
	}
	public String getIzqProfundidadInternaEje2() {
		return izqProfundidadInternaEje2;
	}
	public void setIzqProfundidadInternaEje2(String izqProfundidadInternaEje2) {
		this.izqProfundidadInternaEje2 = izqProfundidadInternaEje2;
	}
	public String getIzqProfundidadInternaEje3() {
		return izqProfundidadInternaEje3;
	}
	public void setIzqProfundidadInternaEje3(String izqProfundidadInternaEje3) {
		this.izqProfundidadInternaEje3 = izqProfundidadInternaEje3;
	}
	public String getIzqProfundidadInternaEje4() {
		return izqProfundidadInternaEje4;
	}
	public void setIzqProfundidadInternaEje4(String izqProfundidadInternaEje4) {
		this.izqProfundidadInternaEje4 = izqProfundidadInternaEje4;
	}
	public String getIzqProfundidadInternaEje5() {
		return izqProfundidadInternaEje5;
	}
	public void setIzqProfundidadInternaEje5(String izqProfundidadInternaEje5) {
		this.izqProfundidadInternaEje5 = izqProfundidadInternaEje5;
	}
	public String getRepuestoProfundidad() {
		return repuestoProfundidad;
	}
	public void setRepuestoProfundidad(String repuestoProfundidad) {
		this.repuestoProfundidad = repuestoProfundidad;
	}
	public String getRepuesto2Profundidad() {
		return repuesto2Profundidad;
	}
	public void setRepuesto2Profundidad(String repuesto2Profundidad) {
		this.repuesto2Profundidad = repuesto2Profundidad;
	}
	public String getDerPresionEje1() {
		return derPresionEje1;
	}
	public void setDerPresionEje1(String derPresionEje1) {
		this.derPresionEje1 = derPresionEje1;
	}
	public String getDerPresionExternaEje2() {
		return derPresionExternaEje2;
	}
	public void setDerPresionExternaEje2(String derPresionExternaEje2) {
		this.derPresionExternaEje2 = derPresionExternaEje2;
	}
	public String getDerPresionExternaEje3() {
		return derPresionExternaEje3;
	}
	public void setDerPresionExternaEje3(String derPresionExternaEje3) {
		this.derPresionExternaEje3 = derPresionExternaEje3;
	}
	public String getDerPresionExternaEje4() {
		return derPresionExternaEje4;
	}
	public void setDerPresionExternaEje4(String derPresionExternaEje4) {
		this.derPresionExternaEje4 = derPresionExternaEje4;
	}
	public String getDerPresionExternaEje5() {
		return derPresionExternaEje5;
	}
	public void setDerPresionExternaEje5(String derPresionExternaEje5) {
		this.derPresionExternaEje5 = derPresionExternaEje5;
	}
	public String getIzqPresionEje1() {
		return IzqPresionEje1;
	}
	public void setIzqPresionEje1(String izqPresionEje1) {
		IzqPresionEje1 = izqPresionEje1;
	}
	public String getIzqPresionExternaEje2() {
		return IzqPresionExternaEje2;
	}
	public void setIzqPresionExternaEje2(String izqPresionExternaEje2) {
		IzqPresionExternaEje2 = izqPresionExternaEje2;
	}
	public String getIzqPresionExternaEje3() {
		return IzqPresionExternaEje3;
	}
	public void setIzqPresionExternaEje3(String izqPresionExternaEje3) {
		IzqPresionExternaEje3 = izqPresionExternaEje3;
	}
	public String getIzqPresionExternaEje4() {
		return IzqPresionExternaEje4;
	}
	public void setIzqPresionExternaEje4(String izqPresionExternaEje4) {
		IzqPresionExternaEje4 = izqPresionExternaEje4;
	}
	public String getIzqPresionExternaEje5() {
		return IzqPresionExternaEje5;
	}
	public void setIzqPresionExternaEje5(String izqPresionExternaEje5) {
		IzqPresionExternaEje5 = izqPresionExternaEje5;
	}
	public String getDerPresionInternaEje2() {
		return derPresionInternaEje2;
	}
	public void setDerPresionInternaEje2(String derPresionInternaEje2) {
		this.derPresionInternaEje2 = derPresionInternaEje2;
	}
	public String getDerPresionInternaEje3() {
		return derPresionInternaEje3;
	}
	public void setDerPresionInternaEje3(String derPresionInternaEje3) {
		this.derPresionInternaEje3 = derPresionInternaEje3;
	}
	public String getDerPresionInternaEje4() {
		return derPresionInternaEje4;
	}
	public void setDerPresionInternaEje4(String derPresionInternaEje4) {
		this.derPresionInternaEje4 = derPresionInternaEje4;
	}
	public String getDerPresionInternaEje5() {
		return derPresionInternaEje5;
	}
	public void setDerPresionInternaEje5(String derPresionInternaEje5) {
		this.derPresionInternaEje5 = derPresionInternaEje5;
	}
	public String getIzqPresionInternaEje2() {
		return IzqPresionInternaEje2;
	}
	public void setIzqPresionInternaEje2(String izqPresionInternaEje2) {
		IzqPresionInternaEje2 = izqPresionInternaEje2;
	}
	public String getIzqPresionInternaEje3() {
		return IzqPresionInternaEje3;
	}
	public void setIzqPresionInternaEje3(String izqPresionInternaEje3) {
		IzqPresionInternaEje3 = izqPresionInternaEje3;
	}
	public String getIzqPresionInternaEje4() {
		return IzqPresionInternaEje4;
	}
	public void setIzqPresionInternaEje4(String izqPresionInternaEje4) {
		IzqPresionInternaEje4 = izqPresionInternaEje4;
	}
	public String getIzqPresionInternaEje5() {
		return IzqPresionInternaEje5;
	}
	public void setIzqPresionInternaEje5(String izqPresionInternaEje5) {
		IzqPresionInternaEje5 = izqPresionInternaEje5;
	}
	public String getRepuestoPresion() {
		return RepuestoPresion;
	}
	public void setRepuestoPresion(String repuestoPresion) {
		RepuestoPresion = repuestoPresion;
	}
	public String getRepuesto2Presion() {
		return Repuesto2Presion;
	}
	public void setRepuesto2Presion(String repuesto2Presion) {
		Repuesto2Presion = repuesto2Presion;
	}
	public String getTablaAfectada() {
		return tablaAfectada;
	}
	public void setTablaAfectada(String tablaAfectada) {
		this.tablaAfectada = tablaAfectada;
	}
	public String getIdRegistro() {
		return idRegistro;
	}
	public void setIdRegistro(String idRegistro) {
		this.idRegistro = idRegistro;
	}
	  
	  

}
