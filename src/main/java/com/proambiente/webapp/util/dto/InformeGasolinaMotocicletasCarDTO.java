package com.proambiente.webapp.util.dto;

import com.proambiente.webapp.util.informes.medellin.InformeMotocicletasRes0762;

public class InformeGasolinaMotocicletasCarDTO extends InformeCarInformacionCDADTO {
	
	
	private ResultadoSonometriaInformeDTO resultadoSonometriaInformeDTO;
	private InformeMotocicletasRes0762 subInforme;
	private String numeroCda;
	private String nivelEmisiones;
	
	
	private String vlrPEf;
	private String numeroSerieBanco;
	private String numeroSerieAnalizador;
	private String marcaAnalizador;
	private String vlrSpanBajoHC;
	private String resultadoVlrSpanBajoHC;
	private String vlrSpanBajoCO;
	private String resultadoVlrSpanBajoCO;
	private String vlrSpanBajoCO2;
	private String resultadoVlrSpanBajoCO2;
	private String vlrSpanAltoHC;
	private String resultadoVlrSpanAltoHC;
	private String vlrSpanAltoCO;
	private String resultadoVlrSpanAltoCO;
	private String vlrSpanAltoCO2;
	private String resultadoVlrSpanAltoCO2;
	private String fechaHoraUltimaCalibracion;
	private String nombreSoftware;
	private String versionSoftware;
	
	
	
	private String fugasTuboEscape;
	private String fugasSilenciador;
	
	private String accesoriosDeformacionesImpiden;
	private String fugasTapaCombustible;
	private String fugasTapaAceite;
	private String sistemaAdmisionAire;
	private String salidasAdicionalesDiseno;
	private String pcv;
	private String presenciaHumoNegroAzul;
	private String revolucionesFueraRango;
	private String fallaSistemaRefrigeracion;
	
	private String temperaturaDeMotor;
	private String rpmRalenti;
	private String hcRalenti;
	private String coRalenti;
	private String co2Ralenti;
	private String o2Ralenti;
	
	private String dilucion;
	private String incumplimientoNivelesEmision;
	private String conceptoFinalDelVehiculo;
	
	private String humedadRelativa;
	private String temperaturaAmbiente;
	private String causaRechazo;
	private String temperaturaTapaEmbrague;
	
	private String fechaInicioPrueba;
	private String fechaFinPrueba;
	protected String fechaVerificacionGasolina;
	protected String codigoCiudadPropietario;
	protected String codigoLineaVehiculo;
	protected String codigoClaseVehiculo;
	protected String codigoCombustible;
	protected String codigoServicio;
	protected String codigoMarca;
	protected String temperaturaMotor;
	
	
	public String getFechaInicioPrueba() {
		return fechaInicioPrueba;
	}
	public void setFechaInicioPrueba(String fechaInicioPrueba) {
		this.fechaInicioPrueba = fechaInicioPrueba;
	}
	public String getFechaFinPrueba() {
		return fechaFinPrueba;
	}
	public void setFechaFinPrueba(String fechaFinPrueba) {
		this.fechaFinPrueba = fechaFinPrueba;
	}
	public String getFechaVerificacionGasolina() {
		return fechaVerificacionGasolina;
	}
	public void setFechaVerificacionGasolina(String fechaVerificacionGasolina) {
		this.fechaVerificacionGasolina = fechaVerificacionGasolina;
	}
	public String getCodigoCiudadPropietario() {
		return codigoCiudadPropietario;
	}
	public void setCodigoCiudadPropietario(String codigoCiudadPropietario) {
		this.codigoCiudadPropietario = codigoCiudadPropietario;
	}
	public String getCodigoLineaVehiculo() {
		return codigoLineaVehiculo;
	}
	public void setCodigoLineaVehiculo(String codigoLineaVehiculo) {
		this.codigoLineaVehiculo = codigoLineaVehiculo;
	}
	public String getCodigoClaseVehiculo() {
		return codigoClaseVehiculo;
	}
	public void setCodigoClaseVehiculo(String codigoClaseVehiculo) {
		this.codigoClaseVehiculo = codigoClaseVehiculo;
	}
	public String getCodigoCombustible() {
		return codigoCombustible;
	}
	public void setCodigoCombustible(String codigoCombustible) {
		this.codigoCombustible = codigoCombustible;
	}
	public String getCodigoServicio() {
		return codigoServicio;
	}
	public void setCodigoServicio(String codigoServicio) {
		this.codigoServicio = codigoServicio;
	}
	public String getCodigoMarca() {
		return codigoMarca;
	}
	public void setCodigoMarca(String codigoMarca) {
		this.codigoMarca = codigoMarca;
	}
	public String getTemperaturaMotor() {
		return temperaturaMotor;
	}
	public void setTemperaturaMotor(String temperaturaMotor) {
		this.temperaturaMotor = temperaturaMotor;
	}
	public String getVlrPEf() {
		return vlrPEf;
	}
	public void setVlrPEf(String vlrPEf) {
		this.vlrPEf = vlrPEf;
	}
	public String getNumeroSerieBanco() {
		return numeroSerieBanco;
	}
	public void setNumeroSerieBanco(String numeroSerieBanco) {
		this.numeroSerieBanco = numeroSerieBanco;
	}
	public String getNumeroSerieAnalizador() {
		return numeroSerieAnalizador;
	}
	public void setNumeroSerieAnalizador(String numeroSerieAnalizador) {
		this.numeroSerieAnalizador = numeroSerieAnalizador;
	}
	public String getMarcaAnalizador() {
		return marcaAnalizador;
	}
	public void setMarcaAnalizador(String marcaAnalizador) {
		this.marcaAnalizador = marcaAnalizador;
	}
	public String getVlrSpanBajoHC() {
		return vlrSpanBajoHC;
	}
	public void setVlrSpanBajoHC(String vlrSpanBajoHC) {
		this.vlrSpanBajoHC = vlrSpanBajoHC;
	}
	public String getVlrSpanBajoCO() {
		return vlrSpanBajoCO;
	}
	public void setVlrSpanBajoCO(String vlrSpanBajoCO) {
		this.vlrSpanBajoCO = vlrSpanBajoCO;
	}
	public String getVlrSpanBajoCO2() {
		return vlrSpanBajoCO2;
	}
	public void setVlrSpanBajoCO2(String vlrSpanBajoCO2) {
		this.vlrSpanBajoCO2 = vlrSpanBajoCO2;
	}
	public String getVlrSpanAltoHC() {
		return vlrSpanAltoHC;
	}
	public void setVlrSpanAltoHC(String vlrSpanAltoHC) {
		this.vlrSpanAltoHC = vlrSpanAltoHC;
	}
	public String getVlrSpanAltoCO() {
		return vlrSpanAltoCO;
	}
	public void setVlrSpanAltoCO(String vlrSpanAltoCO) {
		this.vlrSpanAltoCO = vlrSpanAltoCO;
	}
	public String getVlrSpanAltoCO2() {
		return vlrSpanAltoCO2;
	}
	public void setVlrSpanAltoCO2(String vlrSpanAltoCO2) {
		this.vlrSpanAltoCO2 = vlrSpanAltoCO2;
	}
	public String getFechaHoraUltimaCalibracion() {
		return fechaHoraUltimaCalibracion;
	}
	public void setFechaHoraUltimaCalibracion(String fechaHoraUltimaCalibracion) {
		this.fechaHoraUltimaCalibracion = fechaHoraUltimaCalibracion;
	}
	public String getNombreSoftware() {
		return nombreSoftware;
	}
	public void setNombreSoftware(String nombreSoftware) {
		this.nombreSoftware = nombreSoftware;
	}
	public String getVersionSoftware() {
		return versionSoftware;
	}
	public void setVersionSoftware(String versionSoftware) {
		this.versionSoftware = versionSoftware;
	}
	public String getFugasTuboEscape() {
		return fugasTuboEscape;
	}
	public void setFugasTuboEscape(String fugasTuboEscape) {
		this.fugasTuboEscape = fugasTuboEscape;
	}
	public String getFugasSilenciador() {
		return fugasSilenciador;
	}
	public void setFugasSilenciador(String fugasSilenciador) {
		this.fugasSilenciador = fugasSilenciador;
	}
	public String getAccesoriosDeformacionesImpiden() {
		return accesoriosDeformacionesImpiden;
	}
	public void setAccesoriosDeformacionesImpiden(String accesoriosDeformacionesImpiden) {
		this.accesoriosDeformacionesImpiden = accesoriosDeformacionesImpiden;
	}
	public String getFugasTapaCombustible() {
		return fugasTapaCombustible;
	}
	public void setFugasTapaCombustible(String fugasTapaCombustible) {
		this.fugasTapaCombustible = fugasTapaCombustible;
	}
	public String getFugasTapaAceite() {
		return fugasTapaAceite;
	}
	public void setFugasTapaAceite(String fugasTapaAceite) {
		this.fugasTapaAceite = fugasTapaAceite;
	}
	public String getSistemaAdmisionAire() {
		return sistemaAdmisionAire;
	}
	public void setSistemaAdmisionAire(String sistemaAdmisionAire) {
		this.sistemaAdmisionAire = sistemaAdmisionAire;
	}
	public String getSalidasAdicionalesDiseno() {
		return salidasAdicionalesDiseno;
	}
	public void setSalidasAdicionalesDiseno(String salidasAdicionalesDiseno) {
		this.salidasAdicionalesDiseno = salidasAdicionalesDiseno;
	}
	public String getPcv() {
		return pcv;
	}
	public void setPcv(String pcv) {
		this.pcv = pcv;
	}
	public String getPresenciaHumoNegroAzul() {
		return presenciaHumoNegroAzul;
	}
	public void setPresenciaHumoNegroAzul(String presenciaHumoNegroAzul) {
		this.presenciaHumoNegroAzul = presenciaHumoNegroAzul;
	}
	public String getRevolucionesFueraRango() {
		return revolucionesFueraRango;
	}
	public void setRevolucionesFueraRango(String revolucionesFueraRango) {
		this.revolucionesFueraRango = revolucionesFueraRango;
	}
	public String getFallaSistemaRefrigeracion() {
		return fallaSistemaRefrigeracion;
	}
	public void setFallaSistemaRefrigeracion(String fallaSistemaRefrigeracion) {
		this.fallaSistemaRefrigeracion = fallaSistemaRefrigeracion;
	}
	public String getTemperaturaDeMotor() {
		return temperaturaDeMotor;
	}
	public void setTemperaturaDeMotor(String temperaturaDeMotor) {
		this.temperaturaDeMotor = temperaturaDeMotor;
	}
	public String getRpmRalenti() {
		return rpmRalenti;
	}
	public void setRpmRalenti(String rpmRalenti) {
		this.rpmRalenti = rpmRalenti;
	}
	public String getHcRalenti() {
		return hcRalenti;
	}
	public void setHcRalenti(String hcRalenti) {
		this.hcRalenti = hcRalenti;
	}
	public String getCoRalenti() {
		return coRalenti;
	}
	public void setCoRalenti(String coRalenti) {
		this.coRalenti = coRalenti;
	}
	public String getCo2Ralenti() {
		return co2Ralenti;
	}
	public void setCo2Ralenti(String co2Ralenti) {
		this.co2Ralenti = co2Ralenti;
	}
	public String getO2Ralenti() {
		return o2Ralenti;
	}
	public void setO2Ralenti(String o2Ralenti) {
		this.o2Ralenti = o2Ralenti;
	}
	public String getConceptoFinalDelVehiculo() {
		return conceptoFinalDelVehiculo;
	}
	public void setConceptoFinalDelVehiculo(String conceptoFinalDelVehiculo) {
		this.conceptoFinalDelVehiculo = conceptoFinalDelVehiculo;
	}
	public String getDilucion() {
		return dilucion;
	}
	public void setDilucion(String dilucion) {
		this.dilucion = dilucion;
	}
	public String getIncumplimientoNivelesEmision() {
		return incumplimientoNivelesEmision;
	}
	public void setIncumplimientoNivelesEmision(String incumplimientoNivelesEmision) {
		this.incumplimientoNivelesEmision = incumplimientoNivelesEmision;
	}
	public String getHumedadRelativa() {
		return humedadRelativa;
	}
	public void setHumedadRelativa(String humedadRelativa) {
		this.humedadRelativa = humedadRelativa;
	}
	public String getTemperaturaAmbiente() {
		return temperaturaAmbiente;
	}
	public void setTemperaturaAmbiente(String temperaturaAmbiente) {
		this.temperaturaAmbiente = temperaturaAmbiente;
	}
	public String getResultadoVlrSpanBajoHC() {
		return resultadoVlrSpanBajoHC;
	}
	public void setResultadoVlrSpanBajoHC(String resultadoVlrSpanBajoHC) {
		this.resultadoVlrSpanBajoHC = resultadoVlrSpanBajoHC;
	}
	public String getResultadoVlrSpanBajoCO() {
		return resultadoVlrSpanBajoCO;
	}
	public void setResultadoVlrSpanBajoCO(String resultadoVlrSpanBajoCO) {
		this.resultadoVlrSpanBajoCO = resultadoVlrSpanBajoCO;
	}
	public String getResultadoVlrSpanBajoCO2() {
		return resultadoVlrSpanBajoCO2;
	}
	public void setResultadoVlrSpanBajoCO2(String resultadoVlrSpanBajoCO2) {
		this.resultadoVlrSpanBajoCO2 = resultadoVlrSpanBajoCO2;
	}
	public String getResultadoVlrSpanAltoHC() {
		return resultadoVlrSpanAltoHC;
	}
	public void setResultadoVlrSpanAltoHC(String resultadoVlrSpanAltoHC) {
		this.resultadoVlrSpanAltoHC = resultadoVlrSpanAltoHC;
	}
	public String getResultadoVlrSpanAltoCO() {
		return resultadoVlrSpanAltoCO;
	}
	public void setResultadoVlrSpanAltoCO(String resultadoVlrSpanAltoCO) {
		this.resultadoVlrSpanAltoCO = resultadoVlrSpanAltoCO;
	}
	public String getResultadoVlrSpanAltoCO2() {
		return resultadoVlrSpanAltoCO2;
	}
	public void setResultadoVlrSpanAltoCO2(String resultadoVlrSpanAltoCO2) {
		this.resultadoVlrSpanAltoCO2 = resultadoVlrSpanAltoCO2;
	}
	public String getCausaRechazo() {
		return causaRechazo;
	}
	public void setCausaRechazo(String causaRechazo) {
		this.causaRechazo = causaRechazo;
	}
	public ResultadoSonometriaInformeDTO getResultadoSonometriaInformeDTO() {
		return resultadoSonometriaInformeDTO;
	}
	public void setResultadoSonometriaInformeDTO(ResultadoSonometriaInformeDTO resultadoSonometriaInformeDTO) {
		this.resultadoSonometriaInformeDTO = resultadoSonometriaInformeDTO;
	}
	public String getTemperaturaTapaEmbrague() {
		return temperaturaTapaEmbrague;
	}
	public void setTemperaturaTapaEmbrague(String temperaturaTapaEmbrague) {
		this.temperaturaTapaEmbrague = temperaturaTapaEmbrague;
	}
	public InformeMotocicletasRes0762 getSubInforme() {
		return subInforme;
	}
	public void setSubInforme(InformeMotocicletasRes0762 subInforme) {
		this.subInforme = subInforme;
	}
	public String getNumeroCda() {
		return numeroCda;
	}
	public void setNumeroCda(String numeroCda) {
		this.numeroCda = numeroCda;
	}
	public String getNivelEmisiones() {
		return nivelEmisiones;
	}
	public void setNivelEmisiones(String nivelEmisiones) {
		this.nivelEmisiones = nivelEmisiones;
	}
	
	

}
