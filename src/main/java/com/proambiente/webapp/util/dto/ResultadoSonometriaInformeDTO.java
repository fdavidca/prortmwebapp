package com.proambiente.webapp.util.dto;

public class ResultadoSonometriaInformeDTO {
	
	private String serialMedidor;
	private String marcaMedidor;
	private String fechaUltimaCalibracion;
	private String valorMedicion;
	
	
	public String getSerialMedidor() {
		return serialMedidor;
	}
	public void setSerialMedidor(String serialMedidor) {
		this.serialMedidor = serialMedidor;
	}
	public String getMarcaMedidor() {
		return marcaMedidor;
	}
	public void setMarcaMedidor(String marcaMedidor) {
		this.marcaMedidor = marcaMedidor;
	}
	public String getFechaUltimaCalibracion() {
		return fechaUltimaCalibracion;
	}
	public void setFechaUltimaCalibracion(String fechaUltimaCalibracion) {
		this.fechaUltimaCalibracion = fechaUltimaCalibracion;
	}
	public String getValorMedicion() {
		return valorMedicion;
	}
	public void setValorMedicion(String valorMedicion) {
		this.valorMedicion = valorMedicion;
	}
	
	

}
