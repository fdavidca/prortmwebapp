package com.proambiente.webapp.util.dto;


/**
 * Complemento del resultado otto de reporte 
 * Para  diversas entidades ambientales
 * 
 * @author fdavi
 *
 */
public class ResultadoOttoInformeDTO {
	
	private String temperaturaAmbiente;
	private String humedadRelativa;
	private String incumplimientoNivelesDeEmision;
	private String valorMedicion;
	private String fallaFiltroAire;
	private String presenciaAccesoriosImpiden;
	
	public String getTemperaturaAmbiente() {
		return temperaturaAmbiente;
	}
	public void setTemperaturaAmbiente(String temperaturaAmbiente) {
		this.temperaturaAmbiente = temperaturaAmbiente;
	}
	public String getHumedadRelativa() {
		return humedadRelativa;
	}
	public void setHumedadRelativa(String humedadRelativa) {
		this.humedadRelativa = humedadRelativa;
	}
	public String getIncumplimientoNivelesDeEmision() {
		return incumplimientoNivelesDeEmision;
	}
	public void setIncumplimientoNivelesDeEmision(String incumplimientoNivelesDeEmision) {
		this.incumplimientoNivelesDeEmision = incumplimientoNivelesDeEmision;
	}
	public String getValorMedicion() {
		return valorMedicion;
	}
	public void setValorMedicion(String valorMedicion) {
		this.valorMedicion = valorMedicion;
	}
	public String getFallaFiltroAire() {
		return fallaFiltroAire;
	}
	public void setFallaFiltroAire(String fallaFiltroAire) {
		this.fallaFiltroAire = fallaFiltroAire;
	}
	public String getPresenciaAccesoriosImpiden() {
		return presenciaAccesoriosImpiden;
	}
	public void setPresenciaAccesoriosImpiden(String presenciaAccesoriosImpiden) {
		this.presenciaAccesoriosImpiden = presenciaAccesoriosImpiden;
	}
	
	

}
