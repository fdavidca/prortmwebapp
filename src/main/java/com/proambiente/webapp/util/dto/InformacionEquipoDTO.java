package com.proambiente.webapp.util.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InformacionEquipoDTO {
	
	private String marca = "";
	private String nombre = "";
	private String noserie = "";
	private String ltoe = "";
	private String pef = "";
	
	
	private String NoSerieBench = "";
	
	
	private String Esperiferico = "";
	
	
	private String Prueba = "";
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getNoserie() {
		return noserie;
	}
	public void setNoserie(String noserie) {
		this.noserie = noserie;
	}
	public String getPef() {
		return pef;
	}
	public void setPef(String pef) {
		this.pef = pef;
	}
	
	@JsonProperty("NoSerieBench")
	public String getNoSerieBench() {
		return NoSerieBench;
	}
	public void setNoSerieBench(String noSerieBench) {
		NoSerieBench = noSerieBench;
	}
	
	@JsonProperty("Esperiferico")
	public String getEsperiferico() {
		return Esperiferico;
	}
	public void setEsperiferico(String esperiferico) {
		Esperiferico = esperiferico;
	}
	
	@JsonProperty("Prueba")
	public String getPrueba() {
		return Prueba;
	}
	public void setPrueba(String prueba) {
		Prueba = prueba;
	}
	public String getLtoe() {
		return ltoe;
	}
	public void setLtoe(String ltoe) {
		this.ltoe = ltoe;
	}
	
	

}
