package com.proambiente.webapp.util.dto;

public class ResultadoPresionLlantasDTO {
	
	private String presionLlantaEje1Der1 = "";
	private String presionLlantaEje1Izq1= "";
	
	private String presionLlantaEje2Der1= "";
	private String presionLlantaEje2Der2= "";
	private String presionLlantaEje2Der3= "";
	private String presionLlantaEje2Der4= "";
	
	private String presionLlantaEje2Izq1= "";
	private String presionLlantaEje2Izq2= "";
	private String presionLlantaEje2Izq3= "";
	private String presionLlantaEje2Izq4= "";

	private String presionLlantaEje3Der1= "";
	private String presionLlantaEje3Der2= "";
	private String presionLlantaEje3Der3= "";
	private String presionLlantaEje3Der4= "";

	private String presionLlantaEje3Izq1= "";
	private String presionLlantaEje3Izq2= "";
	private String presionLlantaEje3Izq3= "";
	private String presionLlantaEje3Izq4= "";

	private String presionLlantaEje4Der1= "";
	private String presionLlantaEje4Der2= "";
	private String presionLlantaEje4Der3= "";
	private String presionLlantaEje4Der4= "";
	
	private String presionLlantaEje4Izq1= "";
	private String presionLlantaEje4Izq2= "";
	private String presionLlantaEje4Izq3= "";
	private String presionLlantaEje4Izq4= "";

	private String presionLlantaEje5Der1= "";
	private String presionLlantaEje5Der2= "";
	private String presionLlantaEje5Der3= "";
	private String presionLlantaEje5Der4= "";

	private String presionLlantaEje5Izq1= "";
	private String presionLlantaEje5Izq2= "";
	private String presionLlantaEje5Izq3= "";
	private String presionLlantaEje5Izq4= "";
	
	private String presionLlantaRepuestoDer = "";
	private String presionLLantaRepuestoIzq = "";
	
	
	public String getPresionLlantaEje1Der1() {
		return presionLlantaEje1Der1;
	}
	public void setPresionLlantaEje1Der1(String presionLlantaEje1Der1) {
		this.presionLlantaEje1Der1 = presionLlantaEje1Der1;
	}
	public String getPresionLlantaEje1Izq1() {
		return presionLlantaEje1Izq1;
	}
	public void setPresionLlantaEje1Izq1(String presionLlantaEje1Izq1) {
		this.presionLlantaEje1Izq1 = presionLlantaEje1Izq1;
	}
	public String getPresionLlantaEje2Der1() {
		return presionLlantaEje2Der1;
	}
	public void setPresionLlantaEje2Der1(String presionLlantaEje2Der1) {
		this.presionLlantaEje2Der1 = presionLlantaEje2Der1;
	}
	public String getPresionLlantaEje2Der2() {
		return presionLlantaEje2Der2;
	}
	public void setPresionLlantaEje2Der2(String presionLlantaEje2Der2) {
		this.presionLlantaEje2Der2 = presionLlantaEje2Der2;
	}
	public String getPresionLlantaEje2Der3() {
		return presionLlantaEje2Der3;
	}
	public void setPresionLlantaEje2Der3(String presionLlantaEje2Der3) {
		this.presionLlantaEje2Der3 = presionLlantaEje2Der3;
	}
	public String getPresionLlantaEje2Der4() {
		return presionLlantaEje2Der4;
	}
	public void setPresionLlantaEje2Der4(String presionLlantaEje2Der4) {
		this.presionLlantaEje2Der4 = presionLlantaEje2Der4;
	}
	public String getPresionLlantaEje3Der1() {
		return presionLlantaEje3Der1;
	}
	public void setPresionLlantaEje3Der1(String presionLlantaEje3Der1) {
		this.presionLlantaEje3Der1 = presionLlantaEje3Der1;
	}
	public String getPresionLlantaEje3Der2() {
		return presionLlantaEje3Der2;
	}
	public void setPresionLlantaEje3Der2(String presionLlantaEje3Der2) {
		this.presionLlantaEje3Der2 = presionLlantaEje3Der2;
	}
	public String getPresionLlantaEje3Der3() {
		return presionLlantaEje3Der3;
	}
	public void setPresionLlantaEje3Der3(String presionLlantaEje3Der3) {
		this.presionLlantaEje3Der3 = presionLlantaEje3Der3;
	}
	public String getPresionLlantaEje3Der4() {
		return presionLlantaEje3Der4;
	}
	public void setPresionLlantaEje3Der4(String presionLlantaEje3Der4) {
		this.presionLlantaEje3Der4 = presionLlantaEje3Der4;
	}
	public String getPresionLlantaEje3Izq1() {
		return presionLlantaEje3Izq1;
	}
	public void setPresionLlantaEje3Izq1(String presionLlantaEje3Izq1) {
		this.presionLlantaEje3Izq1 = presionLlantaEje3Izq1;
	}
	public String getPresionLlantaEje3Izq2() {
		return presionLlantaEje3Izq2;
	}
	public void setPresionLlantaEje3Izq2(String presionLlantaEje3Izq2) {
		this.presionLlantaEje3Izq2 = presionLlantaEje3Izq2;
	}
	public String getPresionLlantaEje3Izq3() {
		return presionLlantaEje3Izq3;
	}
	public void setPresionLlantaEje3Izq3(String presionLlantaEje3Izq3) {
		this.presionLlantaEje3Izq3 = presionLlantaEje3Izq3;
	}
	public String getPresionLlantaEje3Izq4() {
		return presionLlantaEje3Izq4;
	}
	public void setPresionLlantaEje3Izq4(String presionLlantaEje3Izq4) {
		this.presionLlantaEje3Izq4 = presionLlantaEje3Izq4;
	}
	public String getPresionLlantaEje4Der1() {
		return presionLlantaEje4Der1;
	}
	public void setPresionLlantaEje4Der1(String presionLlantaEje4Der1) {
		this.presionLlantaEje4Der1 = presionLlantaEje4Der1;
	}
	public String getPresionLlantaEje4Der2() {
		return presionLlantaEje4Der2;
	}
	public void setPresionLlantaEje4Der2(String presionLlantaEje4Der2) {
		this.presionLlantaEje4Der2 = presionLlantaEje4Der2;
	}
	public String getPresionLlantaEje4Der3() {
		return presionLlantaEje4Der3;
	}
	public void setPresionLlantaEje4Der3(String presionLlantaEje4Der3) {
		this.presionLlantaEje4Der3 = presionLlantaEje4Der3;
	}
	public String getPresionLlantaEje4Der4() {
		return presionLlantaEje4Der4;
	}
	public void setPresionLlantaEje4Der4(String presionLlantaEje4Der4) {
		this.presionLlantaEje4Der4 = presionLlantaEje4Der4;
	}
	public String getPresionLlantaEje4Izq1() {
		return presionLlantaEje4Izq1;
	}
	public void setPresionLlantaEje4Izq1(String presionLlantaEje4Izq1) {
		this.presionLlantaEje4Izq1 = presionLlantaEje4Izq1;
	}
	public String getPresionLlantaEje4Izq2() {
		return presionLlantaEje4Izq2;
	}
	public void setPresionLlantaEje4Izq2(String presionLlantaEje4Izq2) {
		this.presionLlantaEje4Izq2 = presionLlantaEje4Izq2;
	}
	public String getPresionLlantaEje4Izq3() {
		return presionLlantaEje4Izq3;
	}
	public void setPresionLlantaEje4Izq3(String presionLlantaEje4Izq3) {
		this.presionLlantaEje4Izq3 = presionLlantaEje4Izq3;
	}
	public String getPresionLlantaEje4Izq4() {
		return presionLlantaEje4Izq4;
	}
	public void setPresionLlantaEje4Izq4(String presionLlantaEje4Izq4) {
		this.presionLlantaEje4Izq4 = presionLlantaEje4Izq4;
	}
	public String getPresionLlantaEje5Der1() {
		return presionLlantaEje5Der1;
	}
	public void setPresionLlantaEje5Der1(String presionLlantaEje5Der1) {
		this.presionLlantaEje5Der1 = presionLlantaEje5Der1;
	}
	public String getPresionLlantaEje5Der2() {
		return presionLlantaEje5Der2;
	}
	public void setPresionLlantaEje5Der2(String presionLlantaEje5Der2) {
		this.presionLlantaEje5Der2 = presionLlantaEje5Der2;
	}
	public String getPresionLlantaEje5Der3() {
		return presionLlantaEje5Der3;
	}
	public void setPresionLlantaEje5Der3(String presionLlantaEje5Der3) {
		this.presionLlantaEje5Der3 = presionLlantaEje5Der3;
	}
	public String getPresionLlantaEje5Der4() {
		return presionLlantaEje5Der4;
	}
	public void setPresionLlantaEje5Der4(String presionLlantaEje5Der4) {
		this.presionLlantaEje5Der4 = presionLlantaEje5Der4;
	}
	public String getPresionLlantaEje5Izq1() {
		return presionLlantaEje5Izq1;
	}
	public void setPresionLlantaEje5Izq1(String presionLlantaEje5Izq1) {
		this.presionLlantaEje5Izq1 = presionLlantaEje5Izq1;
	}
	public String getPresionLlantaEje5Izq2() {
		return presionLlantaEje5Izq2;
	}
	public void setPresionLlantaEje5Izq2(String presionLlantaEje5Izq2) {
		this.presionLlantaEje5Izq2 = presionLlantaEje5Izq2;
	}
	public String getPresionLlantaEje5Izq3() {
		return presionLlantaEje5Izq3;
	}
	public void setPresionLlantaEje5Izq3(String presionLlantaEje5Izq3) {
		this.presionLlantaEje5Izq3 = presionLlantaEje5Izq3;
	}
	public String getPresionLlantaEje5Izq4() {
		return presionLlantaEje5Izq4;
	}
	public void setPresionLlantaEje5Izq4(String presionLlantaEje5Izq4) {
		this.presionLlantaEje5Izq4 = presionLlantaEje5Izq4;
	}
	public String getPresionLlantaRepuestoDer() {
		return presionLlantaRepuestoDer;
	}
	public void setPresionLlantaRepuestoDer(String presionLlantaRepuestoDer) {
		this.presionLlantaRepuestoDer = presionLlantaRepuestoDer;
	}
	public String getPresionLLantaRepuestoIzq() {
		return presionLLantaRepuestoIzq;
	}
	public void setPresionLLantaRepuestoIzq(String presionLLantaRepuestoIzq) {
		this.presionLLantaRepuestoIzq = presionLLantaRepuestoIzq;
	}
	public String getPresionLlantaEje2Izq1() {
		return presionLlantaEje2Izq1;
	}
	public void setPresionLlantaEje2Izq1(String presionLlantaEje2Izq1) {
		this.presionLlantaEje2Izq1 = presionLlantaEje2Izq1;
	}
	public String getPresionLlantaEje2Izq2() {
		return presionLlantaEje2Izq2;
	}
	public void setPresionLlantaEje2Izq2(String presionLlantaEje2Izq2) {
		this.presionLlantaEje2Izq2 = presionLlantaEje2Izq2;
	}
	public String getPresionLlantaEje2Izq3() {
		return presionLlantaEje2Izq3;
	}
	public void setPresionLlantaEje2Izq3(String presionLlantaEje2Izq3) {
		this.presionLlantaEje2Izq3 = presionLlantaEje2Izq3;
	}
	public String getPresionLlantaEje2Izq4() {
		return presionLlantaEje2Izq4;
	}
	public void setPresionLlantaEje2Izq4(String presionLlantaEje2Izq4) {
		this.presionLlantaEje2Izq4 = presionLlantaEje2Izq4;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ResultadoPresionLlantasDTO [presionLlantaEje1Der1=");
		builder.append(presionLlantaEje1Der1);
		builder.append(", presionLlantaEje1Izq1=");
		builder.append(presionLlantaEje1Izq1);
		builder.append(", presionLlantaEje2Der1=");
		builder.append(presionLlantaEje2Der1);
		builder.append(", presionLlantaEje2Der2=");
		builder.append(presionLlantaEje2Der2);
		builder.append(", presionLlantaEje2Der3=");
		builder.append(presionLlantaEje2Der3);
		builder.append(", presionLlantaEje2Der4=");
		builder.append(presionLlantaEje2Der4);
		builder.append(", presionLlantaEje2Izq1=");
		builder.append(presionLlantaEje2Izq1);
		builder.append(", presionLlantaEje2Izq2=");
		builder.append(presionLlantaEje2Izq2);
		builder.append(", presionLlantaEje2Izq3=");
		builder.append(presionLlantaEje2Izq3);
		builder.append(", presionLlantaEje2Izq4=");
		builder.append(presionLlantaEje2Izq4);
		builder.append(", presionLlantaEje3Der1=");
		builder.append(presionLlantaEje3Der1);
		builder.append(", presionLlantaEje3Der2=");
		builder.append(presionLlantaEje3Der2);
		builder.append(", presionLlantaEje3Der3=");
		builder.append(presionLlantaEje3Der3);
		builder.append(", presionLlantaEje3Der4=");
		builder.append(presionLlantaEje3Der4);
		builder.append(", presionLlantaEje3Izq1=");
		builder.append(presionLlantaEje3Izq1);
		builder.append(", presionLlantaEje3Izq2=");
		builder.append(presionLlantaEje3Izq2);
		builder.append(", presionLlantaEje3Izq3=");
		builder.append(presionLlantaEje3Izq3);
		builder.append(", presionLlantaEje3Izq4=");
		builder.append(presionLlantaEje3Izq4);
		builder.append(", presionLlantaEje4Der1=");
		builder.append(presionLlantaEje4Der1);
		builder.append(", presionLlantaEje4Der2=");
		builder.append(presionLlantaEje4Der2);
		builder.append(", presionLlantaEje4Der3=");
		builder.append(presionLlantaEje4Der3);
		builder.append(", presionLlantaEje4Der4=");
		builder.append(presionLlantaEje4Der4);
		builder.append(", presionLlantaEje4Izq1=");
		builder.append(presionLlantaEje4Izq1);
		builder.append(", presionLlantaEje4Izq2=");
		builder.append(presionLlantaEje4Izq2);
		builder.append(", presionLlantaEje4Izq3=");
		builder.append(presionLlantaEje4Izq3);
		builder.append(", presionLlantaEje4Izq4=");
		builder.append(presionLlantaEje4Izq4);
		builder.append(", presionLlantaEje5Der1=");
		builder.append(presionLlantaEje5Der1);
		builder.append(", presionLlantaEje5Der2=");
		builder.append(presionLlantaEje5Der2);
		builder.append(", presionLlantaEje5Der3=");
		builder.append(presionLlantaEje5Der3);
		builder.append(", presionLlantaEje5Der4=");
		builder.append(presionLlantaEje5Der4);
		builder.append(", presionLlantaEje5Izq1=");
		builder.append(presionLlantaEje5Izq1);
		builder.append(", presionLlantaEje5Izq2=");
		builder.append(presionLlantaEje5Izq2);
		builder.append(", presionLlantaEje5Izq3=");
		builder.append(presionLlantaEje5Izq3);
		builder.append(", presionLlantaEje5Izq4=");
		builder.append(presionLlantaEje5Izq4);
		builder.append(", presionLlantaRepuestoDer=");
		builder.append(presionLlantaRepuestoDer);
		builder.append(", presionLLantaRepuestoIzq=");
		builder.append(presionLLantaRepuestoIzq);
		builder.append("]");
		return builder.toString();
	}

	
	
}
