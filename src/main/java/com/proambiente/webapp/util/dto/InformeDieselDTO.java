package com.proambiente.webapp.util.dto;

public class InformeDieselDTO extends InformeCarInformacionCDADTO {
	
	private String serieMedidor;
	private String marcaMedidor;

	private String temperaturaAmbiente;
	private String humedadRelativa;
	


	
	private String comprobacionInyeccionCombustible;
	private String rpmGobernadaMedida;
	private String temperaturaInicialMotor;
	private String rpmRalenti;
	private String resultadoCicloPreliminar;
	private String densidadHumoCicloPreliminar;
	private String rpmGobernadaCicloPreliminar;
	private String resultadoPrimerCiclo;
	private String densidadHumoPrimerCiclo;
	private String rpmGobernadaPrimerCiclo;
	private String resultadoOpacidadSegundoCiclo;
	private String densidadHumoSegundoCiclo;
	private String rpmGobernadaSegundoCiclo;
	private String resultadoOpacidadTercerCiclo;
	private String densidadHumoTercerciclo;
	private String rpmGobernadaTercerCiclo;
	private String resultadoFinal;
	private String densidadHumoFinal;

	private String tempFinalMotor;
	private String fugasTuboEscape;
	private String fugasSilenciador;
	private String presenciaTapaCombustible;
	private String presenciaTapaAceite;
	private String accesoriosImpiden;
	private String salidasAdicionales;
	private String filtroAire;
	private String sistemaRegrigeracion;
	private String revolucionesInestables;
	private String velocidadNoAlcanzada5Seg;
	private String malFuncionamientoMotor;
	private String incumplimientoNiveles;
	private String fugasTuvoUnionesMultipleSilenciador;
	private String salidasAdicionalesDisenio;
	private String ausenciaTaponesDeAceite;
	private String ausenciaTaponCombustible;
	private String instalacionAccesoriosODeformaciones;
	private String incorrectaOperacionSistemaRefrigeracion;
	private String ausenciaDeFiltroAire;
	private String instalacionDispositivosAlteranRpms;
	private String diferenciasAritmeticas;
	private String diferenciasTemperaturaMotor;
	private String fallaSubitaMotor;
	private String gobernadorNoLimitaRevoluciones;
	
	private String numeroCertificadoRevision;
	
	private String valorPrimerPuntoLinealidad;
	private String resultadoValorPrimerPuntoLinealidad;
	private String valorSegundoPuntoLinealidad;
	private String resultadoValorSegundoPuntoLinealidad;
	private String valorTercerPuntoLinealidad;
	private String resultadoValorTercerPuntoLinealidad;
	private String valorCuartoPuntoLinealidad;
	private String resultadoCuartoPuntoLinealidad;
	
	private String causaRechazoDiesel;
	private String resultadoPrueba;
	
	
	ResultadoSonometriaInformeDTO resultadoSonometria;
	
	public ResultadoSonometriaInformeDTO getResultadoSonometria() {
		return resultadoSonometria;
	}
	public void setResultadoSonometria(ResultadoSonometriaInformeDTO resultadoSonometria) {
		this.resultadoSonometria = resultadoSonometria;
	}
	
	
	public String getNoCda() {
		return noCda;
	}
	public void setNoCda(String noCda) {
		this.noCda = noCda;
	}
	public String getNombreCda() {
		return nombreCda;
	}
	public void setNombreCda(String nombreCda) {
		this.nombreCda = nombreCda;
	}
	public String getNitCda() {
		return nitCda;
	}
	public void setNitCda(String nitCda) {
		this.nitCda = nitCda;
	}
	public String getDireccionCda() {
		return direccionCda;
	}
	public void setDireccionCda(String direccionCda) {
		this.direccionCda = direccionCda;
	}
	public String getTelefono1() {
		return telefono1;
	}
	public void setTelefono1(String telefono1) {
		this.telefono1 = telefono1;
	}
	public String getTelefono2() {
		return telefono2;
	}
	public void setTelefono2(String telefono2) {
		this.telefono2 = telefono2;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getNumeroResolucion() {
		return numeroResolucion;
	}
	public void setNumeroResolucion(String numeroResolucion) {
		this.numeroResolucion = numeroResolucion;
	}
	public String getFechaResolucion() {
		return fechaResolucion;
	}
	public void setFechaResolucion(String fechaResolucion) {
		this.fechaResolucion = fechaResolucion;
	}
	public String getSerieMedidor() {
		return serieMedidor;
	}
	public void setSerieMedidor(String serieMedidor) {
		this.serieMedidor = serieMedidor;
	}
	public String getMarcaMedidor() {
		return marcaMedidor;
	}
	public void setMarcaMedidor(String marcaMedidor) {
		this.marcaMedidor = marcaMedidor;
	}
	
	public String getTemperaturaAmbiente() {
		return temperaturaAmbiente;
	}
	public void setTemperaturaAmbiente(String temperaturaAmbiente) {
		this.temperaturaAmbiente = temperaturaAmbiente;
	}
	public String getHumedadRelativa() {
		return humedadRelativa;
	}
	public void setHumedadRelativa(String humedadRelativa) {
		this.humedadRelativa = humedadRelativa;
	}
	
	
	
	public String getComprobacionInyeccionCombustible() {
		return comprobacionInyeccionCombustible;
	}
	public void setComprobacionInyeccionCombustible(String comprobacionInyeccionCombustible) {
		this.comprobacionInyeccionCombustible = comprobacionInyeccionCombustible;
	}
	public String getRpmGobernadaMedida() {
		return rpmGobernadaMedida;
	}
	public void setRpmGobernadaMedida(String rpmGobernadaMedida) {
		this.rpmGobernadaMedida = rpmGobernadaMedida;
	}
	public String getTemperaturaInicialMotor() {
		return temperaturaInicialMotor;
	}
	public void setTemperaturaInicialMotor(String temperaturaInicialMotor) {
		this.temperaturaInicialMotor = temperaturaInicialMotor;
	}
	public String getRpmRalenti() {
		return rpmRalenti;
	}
	public void setRpmRalenti(String rpmRalenti) {
		this.rpmRalenti = rpmRalenti;
	}
	public String getResultadoCicloPreliminar() {
		return resultadoCicloPreliminar;
	}
	public void setResultadoCicloPreliminar(String resultadoCicloPreliminar) {
		this.resultadoCicloPreliminar = resultadoCicloPreliminar;
	}
	public String getRpmGobernadaCicloPreliminar() {
		return rpmGobernadaCicloPreliminar;
	}
	public void setRpmGobernadaCicloPreliminar(String rpmGobernadaCicloPreliminar) {
		this.rpmGobernadaCicloPreliminar = rpmGobernadaCicloPreliminar;
	}
	public String getResultadoPrimerCiclo() {
		return resultadoPrimerCiclo;
	}
	public void setResultadoPrimerCiclo(String resultadoPrimerCiclo) {
		this.resultadoPrimerCiclo = resultadoPrimerCiclo;
	}
	public String getRpmGobernadaPrimerCiclo() {
		return rpmGobernadaPrimerCiclo;
	}
	public void setRpmGobernadaPrimerCiclo(String rpmGobernadaPrimerCiclo) {
		this.rpmGobernadaPrimerCiclo = rpmGobernadaPrimerCiclo;
	}
	public String getResultadoOpacidadSegundoCiclo() {
		return resultadoOpacidadSegundoCiclo;
	}
	public void setResultadoOpacidadSegundoCiclo(String resultadoOpacidadSegundoCiclo) {
		this.resultadoOpacidadSegundoCiclo = resultadoOpacidadSegundoCiclo;
	}
	public String getRpmGobernadaSegundoCiclo() {
		return rpmGobernadaSegundoCiclo;
	}
	public void setRpmGobernadaSegundoCiclo(String rpmGobernadaSegundoCiclo) {
		this.rpmGobernadaSegundoCiclo = rpmGobernadaSegundoCiclo;
	}
	public String getResultadoOpacidadTercerCiclo() {
		return resultadoOpacidadTercerCiclo;
	}
	public void setResultadoOpacidadTercerCiclo(String resultadoOpacidadTercerCiclo) {
		this.resultadoOpacidadTercerCiclo = resultadoOpacidadTercerCiclo;
	}
	public String getRpmGobernadaTercerCiclo() {
		return rpmGobernadaTercerCiclo;
	}
	public void setRpmGobernadaTercerCiclo(String rpmGobernadaTercerCiclo) {
		this.rpmGobernadaTercerCiclo = rpmGobernadaTercerCiclo;
	}
	public String getResultadoFinal() {
		return resultadoFinal;
	}
	public void setResultadoFinal(String resultadoFinal) {
		this.resultadoFinal = resultadoFinal;
	}
	public String getLtoeDiametro() {
		return ltoeDiametro;
	}
	public void setLtoeDiametro(String ltoeDiametro) {
		this.ltoeDiametro = ltoeDiametro;
	}
	public String getTempFinalMotor() {
		return tempFinalMotor;
	}
	public void setTempFinalMotor(String tempFinalMotor) {
		this.tempFinalMotor = tempFinalMotor;
	}
	public String getFugasTuboEscape() {
		return fugasTuboEscape;
	}
	public void setFugasTuboEscape(String fugasTuboEscape) {
		this.fugasTuboEscape = fugasTuboEscape;
	}
	public String getFugasSilenciador() {
		return fugasSilenciador;
	}
	public void setFugasSilenciador(String fugasSilenciador) {
		this.fugasSilenciador = fugasSilenciador;
	}
	public String getPresenciaTapaCombustible() {
		return presenciaTapaCombustible;
	}
	public void setPresenciaTapaCombustible(String presenciaTapaCombustible) {
		this.presenciaTapaCombustible = presenciaTapaCombustible;
	}
	public String getPresenciaTapaAceite() {
		return presenciaTapaAceite;
	}
	public void setPresenciaTapaAceite(String presenciaTapaAceite) {
		this.presenciaTapaAceite = presenciaTapaAceite;
	}
	public String getAccesoriosImpiden() {
		return accesoriosImpiden;
	}
	public void setAccesoriosImpiden(String accesoriosImpiden) {
		this.accesoriosImpiden = accesoriosImpiden;
	}
	public String getSalidasAdicionales() {
		return salidasAdicionales;
	}
	public void setSalidasAdicionales(String salidasAdicionales) {
		this.salidasAdicionales = salidasAdicionales;
	}
	public String getFiltroAire() {
		return filtroAire;
	}
	public void setFiltroAire(String filtroAire) {
		this.filtroAire = filtroAire;
	}
	public String getSistemaRegrigeracion() {
		return sistemaRegrigeracion;
	}
	public void setSistemaRegrigeracion(String sistemaRegrigeracion) {
		this.sistemaRegrigeracion = sistemaRegrigeracion;
	}
	public String getRevolucionesInestables() {
		return revolucionesInestables;
	}
	public void setRevolucionesInestables(String revolucionesInestables) {
		this.revolucionesInestables = revolucionesInestables;
	}
	public String getVelocidadNoAlcanzada5Seg() {
		return velocidadNoAlcanzada5Seg;
	}
	public void setVelocidadNoAlcanzada5Seg(String velocidadNoAlcanzada5Seg) {
		this.velocidadNoAlcanzada5Seg = velocidadNoAlcanzada5Seg;
	}
	public String getMalFuncionamientoMotor() {
		return malFuncionamientoMotor;
	}
	public void setMalFuncionamientoMotor(String malFuncionamientoMotor) {
		this.malFuncionamientoMotor = malFuncionamientoMotor;
	}
	public String getIncumplimientoNiveles() {
		return incumplimientoNiveles;
	}
	public void setIncumplimientoNiveles(String incumplimientoNiveles) {
		this.incumplimientoNiveles = incumplimientoNiveles;
	}
	public String getFugasTuvoUnionesMultipleSilenciador() {
		return fugasTuvoUnionesMultipleSilenciador;
	}
	public void setFugasTuvoUnionesMultipleSilenciador(String fugasTuvoUnionesMultipleSilenciador) {
		this.fugasTuvoUnionesMultipleSilenciador = fugasTuvoUnionesMultipleSilenciador;
	}
	public String getSalidasAdicionalesDisenio() {
		return salidasAdicionalesDisenio;
	}
	public void setSalidasAdicionalesDisenio(String salidasAdicionalesDisenio) {
		this.salidasAdicionalesDisenio = salidasAdicionalesDisenio;
	}
	public String getAusenciaTaponesDeAceite() {
		return ausenciaTaponesDeAceite;
	}
	public void setAusenciaTaponesDeAceite(String ausenciaTaponesDeAceite) {
		this.ausenciaTaponesDeAceite = ausenciaTaponesDeAceite;
	}
	public String getAusenciaTaponCombustible() {
		return ausenciaTaponCombustible;
	}
	public void setAusenciaTaponCombustible(String ausenciaTaponCombustible) {
		this.ausenciaTaponCombustible = ausenciaTaponCombustible;
	}
	public String getInstalacionAccesoriosODeformaciones() {
		return instalacionAccesoriosODeformaciones;
	}
	public void setInstalacionAccesoriosODeformaciones(String instalacionAccesoriosODeformaciones) {
		this.instalacionAccesoriosODeformaciones = instalacionAccesoriosODeformaciones;
	}
	public String getIncorrectaOperacionSistemaRefrigeracion() {
		return incorrectaOperacionSistemaRefrigeracion;
	}
	public void setIncorrectaOperacionSistemaRefrigeracion(String incorrectaOperacionSistemaRefrigeracion) {
		this.incorrectaOperacionSistemaRefrigeracion = incorrectaOperacionSistemaRefrigeracion;
	}
	public String getAusenciaDeFiltroAire() {
		return ausenciaDeFiltroAire;
	}
	public void setAusenciaDeFiltroAire(String ausenciaDeFiltroAire) {
		this.ausenciaDeFiltroAire = ausenciaDeFiltroAire;
	}
	public String getInstalacionDispositivosAlteranRpms() {
		return instalacionDispositivosAlteranRpms;
	}
	public void setInstalacionDispositivosAlteranRpms(String instalacionDispositivosAlteranRpms) {
		this.instalacionDispositivosAlteranRpms = instalacionDispositivosAlteranRpms;
	}
	public String getDiferenciasAritmeticas() {
		return diferenciasAritmeticas;
	}
	public void setDiferenciasAritmeticas(String diferenciasAritmeticas) {
		this.diferenciasAritmeticas = diferenciasAritmeticas;
	}
	public String getFallaSubitaMotor() {
		return fallaSubitaMotor;
	}
	public void setFallaSubitaMotor(String fallaSubitaMotor) {
		this.fallaSubitaMotor = fallaSubitaMotor;
	}
	public String getValorPrimerPuntoLinealidad() {
		return valorPrimerPuntoLinealidad;
	}
	public void setValorPrimerPuntoLinealidad(String valorPrimerPuntoLinealidad) {
		this.valorPrimerPuntoLinealidad = valorPrimerPuntoLinealidad;
	}
	public String getResultadoValorPrimerPuntoLinealidad() {
		return resultadoValorPrimerPuntoLinealidad;
	}
	public void setResultadoValorPrimerPuntoLinealidad(String resultadoValorPrimerPuntoLinealidad) {
		this.resultadoValorPrimerPuntoLinealidad = resultadoValorPrimerPuntoLinealidad;
	}
	public String getValorSegundoPuntoLinealidad() {
		return valorSegundoPuntoLinealidad;
	}
	public void setValorSegundoPuntoLinealidad(String valorSegundoPuntoLinealidad) {
		this.valorSegundoPuntoLinealidad = valorSegundoPuntoLinealidad;
	}
	public String getResultadoValorSegundoPuntoLinealidad() {
		return resultadoValorSegundoPuntoLinealidad;
	}
	public void setResultadoValorSegundoPuntoLinealidad(String resultadoValorSegundoPuntoLinealidad) {
		this.resultadoValorSegundoPuntoLinealidad = resultadoValorSegundoPuntoLinealidad;
	}
	public String getValorTercerPuntoLinealidad() {
		return valorTercerPuntoLinealidad;
	}
	public void setValorTercerPuntoLinealidad(String valorTercerPuntoLinealidad) {
		this.valorTercerPuntoLinealidad = valorTercerPuntoLinealidad;
	}
	public String getResultadoValorTercerPuntoLinealidad() {
		return resultadoValorTercerPuntoLinealidad;
	}
	public void setResultadoValorTercerPuntoLinealidad(String resultadoValorTercerPuntoLinealidad) {
		this.resultadoValorTercerPuntoLinealidad = resultadoValorTercerPuntoLinealidad;
	}
	public String getValorCuartoPuntoLinealidad() {
		return valorCuartoPuntoLinealidad;
	}
	public void setValorCuartoPuntoLinealidad(String valorCuartoPuntoLinealidad) {
		this.valorCuartoPuntoLinealidad = valorCuartoPuntoLinealidad;
	}
	public String getResultadoCuartoPuntoLinealidad() {
		return resultadoCuartoPuntoLinealidad;
	}
	public void setResultadoCuartoPuntoLinealidad(String resultadoCuartoPuntoLinealidad) {
		this.resultadoCuartoPuntoLinealidad = resultadoCuartoPuntoLinealidad;
	}
	public String getNumeroCertificadoRevision() {
		return numeroCertificadoRevision;
	}
	public void setNumeroCertificadoRevision(String numeroCertificadoRevision) {
		this.numeroCertificadoRevision = numeroCertificadoRevision;
	}
	public String getCausaRechazoDiesel() {
		return causaRechazoDiesel;
	}
	public void setCausaRechazoDiesel(String causaRechazoDiesel) {
		this.causaRechazoDiesel = causaRechazoDiesel;
	}
	public String getDiferenciasTemperaturaMotor() {
		return diferenciasTemperaturaMotor;
	}
	public void setDiferenciasTemperaturaMotor(String diferenciasTemperaturaMotor) {
		this.diferenciasTemperaturaMotor = diferenciasTemperaturaMotor;
	}
	public String getGobernadorNoLimitaRevoluciones() {
		return gobernadorNoLimitaRevoluciones;
	}
	public void setGobernadorNoLimitaRevoluciones(String gobernadorNoLimitaRevoluciones) {
		this.gobernadorNoLimitaRevoluciones = gobernadorNoLimitaRevoluciones;
	}
	public String getResultadoPrueba() {
		return resultadoPrueba;
	}
	public void setResultadoPrueba(String resultadoPrueba) {
		this.resultadoPrueba = resultadoPrueba;
	}
	public String getDensidadHumoCicloPreliminar() {
		return densidadHumoCicloPreliminar;
	}
	public void setDensidadHumoCicloPreliminar(String densidadHumoCicloPreliminar) {
		this.densidadHumoCicloPreliminar = densidadHumoCicloPreliminar;
	}
	public String getDensidadHumoPrimerCiclo() {
		return densidadHumoPrimerCiclo;
	}
	public void setDensidadHumoPrimerCiclo(String densidadHumoPrimerCiclo) {
		this.densidadHumoPrimerCiclo = densidadHumoPrimerCiclo;
	}
	public String getDensidadHumoSegundoCiclo() {
		return densidadHumoSegundoCiclo;
	}
	public void setDensidadHumoSegundoCiclo(String densidadHumoSegundoCiclo) {
		this.densidadHumoSegundoCiclo = densidadHumoSegundoCiclo;
	}
	public String getDensidadHumoTercerciclo() {
		return densidadHumoTercerciclo;
	}
	public void setDensidadHumoTercerciclo(String densidadHumoTercerciclo) {
		this.densidadHumoTercerciclo = densidadHumoTercerciclo;
	}
	public String getDensidadHumoFinal() {
		return densidadHumoFinal;
	}
	public void setDensidadHumoFinal(String densidadHumoFinal) {
		this.densidadHumoFinal = densidadHumoFinal;
	}
	
	 
	
}
