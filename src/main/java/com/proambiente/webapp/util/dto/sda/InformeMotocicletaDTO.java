package com.proambiente.webapp.util.dto.sda;

import com.proambiente.webapp.util.dto.InformeGasolinaMotocicletasCarDTO;

public class InformeMotocicletaDTO extends InformeGasolinaMotocicletasCarDTO{
	
	protected String resultadoValorSpanBajoHC;
	protected String resultadoValorSpanBajoCO;
	protected String resultadoValorSpanBajoCO2;
	protected String resultadoValorSpanAltoHC;
	protected String resultadoValorSpanAltoCO;
	protected String resultadoValorSpanAltoCO2;
	protected String causaRechazo;
	
	
	public String getResultadoValorSpanBajoHC() {
		return resultadoValorSpanBajoHC;
	}
	public void setResultadoValorSpanBajoHC(String resultadoValorSpanBajoHC) {
		this.resultadoValorSpanBajoHC = resultadoValorSpanBajoHC;
	}
	public String getResultadoValorSpanBajoCO() {
		return resultadoValorSpanBajoCO;
	}
	public void setResultadoValorSpanBajoCO(String resultadoValorSpanBajoCO) {
		this.resultadoValorSpanBajoCO = resultadoValorSpanBajoCO;
	}
	public String getResultadoValorSpanBajoCO2() {
		return resultadoValorSpanBajoCO2;
	}
	public void setResultadoValorSpanBajoCO2(String resultadoValorSpanBajoCO2) {
		this.resultadoValorSpanBajoCO2 = resultadoValorSpanBajoCO2;
	}
	public String getResultadoValorSpanAltoHC() {
		return resultadoValorSpanAltoHC;
	}
	public void setResultadoValorSpanAltoHC(String resultadoValorSpanAltoHC) {
		this.resultadoValorSpanAltoHC = resultadoValorSpanAltoHC;
	}
	public String getResultadoValorSpanAltoCO() {
		return resultadoValorSpanAltoCO;
	}
	public void setResultadoValorSpanAltoCO(String resultadoValorSpanAltoCO) {
		this.resultadoValorSpanAltoCO = resultadoValorSpanAltoCO;
	}
	public String getResultadoValorSpanAltoCO2() {
		return resultadoValorSpanAltoCO2;
	}
	public void setResultadoValorSpanAltoCO2(String resultadoValorSpanAltoCO2) {
		this.resultadoValorSpanAltoCO2 = resultadoValorSpanAltoCO2;
	}
	public String getCausaRechazo() {
		return causaRechazo;
	}
	public void setCausaRechazo(String causaRechazo) {
		this.causaRechazo = causaRechazo;
	}
	
	
	
	

}
