package com.proambiente.webapp.util.dto;

public class ComentarioRevisionDTO {
	
	private Integer revisionId;
	private String comentario;
	public Integer getRevisionId() {
		return revisionId;
	}
	public void setRevisionId(Integer revisionId) {
		this.revisionId = revisionId;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	
	

}
