package com.proambiente.webapp.util.dto.logsicov;

public class LogAlineacionSicovDTO {
	
	private String eje1 = "";
	private String eje2 = "";
	private String eje3 = "";
	private String eje4 = "";
	private String eje5 = "";
	private String eje6 = "";
	private String tablaAfectada = "";
	private String idRegistro = "";
	public String getEje1() {
		return eje1;
	}
	public void setEje1(String eje1) {
		this.eje1 = eje1;
	}
	public String getEje2() {
		return eje2;
	}
	public void setEje2(String eje2) {
		this.eje2 = eje2;
	}
	public String getEje3() {
		return eje3;
	}
	public void setEje3(String eje3) {
		this.eje3 = eje3;
	}
	public String getEje4() {
		return eje4;
	}
	public void setEje4(String eje4) {
		this.eje4 = eje4;
	}
	public String getEje5() {
		return eje5;
	}
	public void setEje5(String eje5) {
		this.eje5 = eje5;
	}
	public String getEje6() {
		return eje6;
	}
	public void setEje6(String eje6) {
		this.eje6 = eje6;
	}
	public String getTablaAfectada() {
		return tablaAfectada;
	}
	public void setTablaAfectada(String tablaAfectada) {
		this.tablaAfectada = tablaAfectada;
	}
	public String getIdRegistro() {
		return idRegistro;
	}
	public void setIdRegistro(String idRegistro) {
		this.idRegistro = idRegistro;
	}
	
	

}
