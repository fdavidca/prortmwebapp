package com.proambiente.webapp.util.dto;

public class ResultadoLucesDtoV2 {
	
	private  String intensidadBajaDerecha1 = "";
	private  String intensidadBajaDerecha2 = "";
	private  String intensidadBajaDerecha3 = "";
	
	private String derBajaSimultaneas = "";
	
	private String intensidadBajaIzquierda1 = "";
	private String intensidadBajaIzquierda2 = "";
	private String intensidadBajaIzquierda3 = "";
	
	private String izqBajaSimultaneas = "";
	
	private String inclinacionBajaDerecha1 = "";
	private String inclinacionBajaDerecha2 = "";
	private String inclinacionBajaDerecha3 = "";
	
	private String inclinacionBajaIzquierda1 = "";
	private String inclinacionBajaIzquierda2 = "";
	private String inclinacionBajaIzquierda3 = "";
	
	
	
	private String intensidadAltaDerecha1 = "";
	private String intensidadAltaDerecha2 = "";
	private String intensidadAltaDerecha3 = "";
	
	private String derAltaSimultaneas = "";
	
	private String intensidadAltaIzquierda1 = "";
	private String intensidadAltaIzquierda2 = "";
	private String intensidadAltaIzquierda3 = "";
	
	private String izqAltaSimultaneas = "";
	
	private String intensidadExploradoraDerecha1 = "";
	private String intensidadExploradoraDerecha2 = "";
	private String intensidadExploradoraDerecha3 = "";
	
	private String derExploradoraSimultaneas = "";
	
	private String intensidadExploradoraIzquierda1 = "";
	private String intensidadExploradoraIzquierda2 = "";
	private String intensidadExploradoraIzquierda3 = "";
	
	private String izqExploradoraSimultaneas = "";
	
	private String sumatoriaIntensidad = "";

	public String getIntensidadBajaDerecha1() {
		return intensidadBajaDerecha1;
	}

	public void setIntensidadBajaDerecha1(String intensidadBajaDerecha1) {
		this.intensidadBajaDerecha1 = intensidadBajaDerecha1;
	}

	public String getIntensidadBajaDerecha2() {
		return intensidadBajaDerecha2;
	}

	public void setIntensidadBajaDerecha2(String intensidadBajaDerecha2) {
		this.intensidadBajaDerecha2 = intensidadBajaDerecha2;
	}

	public String getIntensidadBajaDerecha3() {
		return intensidadBajaDerecha3;
	}

	public void setIntensidadBajaDerecha3(String intensidadBajaDerecha3) {
		this.intensidadBajaDerecha3 = intensidadBajaDerecha3;
	}

	public String getDerBajaSimultaneas() {
		return derBajaSimultaneas;
	}

	public void setDerBajaSimultaneas(String derBajaSimultaneas) {
		this.derBajaSimultaneas = derBajaSimultaneas;
	}

	public String getIntensidadBajaIzquierda1() {
		return intensidadBajaIzquierda1;
	}

	public void setIntensidadBajaIzquierda1(String intensidadBajaIzquierda1) {
		this.intensidadBajaIzquierda1 = intensidadBajaIzquierda1;
	}

	public String getIntensidadBajaIzquierda2() {
		return intensidadBajaIzquierda2;
	}

	public void setIntensidadBajaIzquierda2(String intensidadBajaIzquierda2) {
		this.intensidadBajaIzquierda2 = intensidadBajaIzquierda2;
	}

	public String getIntensidadBajaIzquierda3() {
		return intensidadBajaIzquierda3;
	}

	public void setIntensidadBajaIzquierda3(String intensidadBajaIzquierda3) {
		this.intensidadBajaIzquierda3 = intensidadBajaIzquierda3;
	}

	public String getIzqBajaSimultaneas() {
		return izqBajaSimultaneas;
	}

	public void setIzqBajaSimultaneas(String izqBajaSimultaneas) {
		this.izqBajaSimultaneas = izqBajaSimultaneas;
	}

	public String getInclinacionBajaDerecha1() {
		return inclinacionBajaDerecha1;
	}

	public void setInclinacionBajaDerecha1(String inclinacionBajaDerecha1) {
		this.inclinacionBajaDerecha1 = inclinacionBajaDerecha1;
	}

	public String getInclinacionBajaDerecha2() {
		return inclinacionBajaDerecha2;
	}

	public void setInclinacionBajaDerecha2(String inclinacionBajaDerecha2) {
		this.inclinacionBajaDerecha2 = inclinacionBajaDerecha2;
	}

	public String getInclinacionBajaDerecha3() {
		return inclinacionBajaDerecha3;
	}

	public void setInclinacionBajaDerecha3(String inclinacionBajaDerecha3) {
		this.inclinacionBajaDerecha3 = inclinacionBajaDerecha3;
	}

	public String getInclinacionBajaIzquierda1() {
		return inclinacionBajaIzquierda1;
	}

	public void setInclinacionBajaIzquierda1(String inclinacionBajaIzquierda1) {
		this.inclinacionBajaIzquierda1 = inclinacionBajaIzquierda1;
	}

	public String getInclinacionBajaIzquierda2() {
		return inclinacionBajaIzquierda2;
	}

	public void setInclinacionBajaIzquierda2(String inclinacionBajaIzquierda2) {
		this.inclinacionBajaIzquierda2 = inclinacionBajaIzquierda2;
	}

	public String getInclinacionBajaIzquierda3() {
		return inclinacionBajaIzquierda3;
	}

	public void setInclinacionBajaIzquierda3(String inclinacionBajaIzquierda3) {
		this.inclinacionBajaIzquierda3 = inclinacionBajaIzquierda3;
	}

	public String getIntensidadAltaDerecha1() {
		return intensidadAltaDerecha1;
	}

	public void setIntensidadAltaDerecha1(String intensidadAltaDerecha1) {
		this.intensidadAltaDerecha1 = intensidadAltaDerecha1;
	}

	public String getIntensidadAltaDerecha2() {
		return intensidadAltaDerecha2;
	}

	public void setIntensidadAltaDerecha2(String intensidadAltaDerecha2) {
		this.intensidadAltaDerecha2 = intensidadAltaDerecha2;
	}

	public String getIntensidadAltaDerecha3() {
		return intensidadAltaDerecha3;
	}

	public void setIntensidadAltaDerecha3(String intensidadAltaDerecha3) {
		this.intensidadAltaDerecha3 = intensidadAltaDerecha3;
	}

	public String getDerAltaSimultaneas() {
		return derAltaSimultaneas;
	}

	public void setDerAltaSimultaneas(String derAltaSimultaneas) {
		this.derAltaSimultaneas = derAltaSimultaneas;
	}

	public String getIntensidadAltaIzquierda1() {
		return intensidadAltaIzquierda1;
	}

	public void setIntensidadAltaIzquierda1(String intensidadAltaIzquierda1) {
		this.intensidadAltaIzquierda1 = intensidadAltaIzquierda1;
	}

	public String getIntensidadAltaIzquierda2() {
		return intensidadAltaIzquierda2;
	}

	public void setIntensidadAltaIzquierda2(String intensidadAltaIzquierda2) {
		this.intensidadAltaIzquierda2 = intensidadAltaIzquierda2;
	}

	public String getIntensidadAltaIzquierda3() {
		return intensidadAltaIzquierda3;
	}

	public void setIntensidadAltaIzquierda3(String intensidadAltaIzquierda3) {
		this.intensidadAltaIzquierda3 = intensidadAltaIzquierda3;
	}

	public String getIzqAltaSimultaneas() {
		return izqAltaSimultaneas;
	}

	public void setIzqAltaSimultaneas(String izqAltaSimultaneas) {
		this.izqAltaSimultaneas = izqAltaSimultaneas;
	}

	public String getIntensidadExploradoraDerecha1() {
		return intensidadExploradoraDerecha1;
	}

	public void setIntensidadExploradoraDerecha1(String intensidadExploradoraDerecha1) {
		this.intensidadExploradoraDerecha1 = intensidadExploradoraDerecha1;
	}

	public String getIntensidadExploradoraDerecha2() {
		return intensidadExploradoraDerecha2;
	}

	public void setIntensidadExploradoraDerecha2(String intensidadExploradoraDerecha2) {
		this.intensidadExploradoraDerecha2 = intensidadExploradoraDerecha2;
	}

	public String getIntensidadExploradoraDerecha3() {
		return intensidadExploradoraDerecha3;
	}

	public void setIntensidadExploradoraDerecha3(String intensidadExploradoraDerecha3) {
		this.intensidadExploradoraDerecha3 = intensidadExploradoraDerecha3;
	}

	public String getDerExploradoraSimultaneas() {
		return derExploradoraSimultaneas;
	}

	public void setDerExploradoraSimultaneas(String derExploradoraSimultaneas) {
		this.derExploradoraSimultaneas = derExploradoraSimultaneas;
	}

	public String getIntensidadExploradoraIzquierda1() {
		return intensidadExploradoraIzquierda1;
	}

	public void setIntensidadExploradoraIzquierda1(String intensidadExploradoraIzquierda1) {
		this.intensidadExploradoraIzquierda1 = intensidadExploradoraIzquierda1;
	}

	public String getIntensidadExploradoraIzquierda2() {
		return intensidadExploradoraIzquierda2;
	}

	public void setIntensidadExploradoraIzquierda2(String intensidadExploradoraIzquierda2) {
		this.intensidadExploradoraIzquierda2 = intensidadExploradoraIzquierda2;
	}

	public String getIntensidadExploradoraIzquierda3() {
		return intensidadExploradoraIzquierda3;
	}

	public void setIntensidadExploradoraIzquierda3(String intensidadExploradoraIzquierda3) {
		this.intensidadExploradoraIzquierda3 = intensidadExploradoraIzquierda3;
	}

	public String getIzqExploradoraSimultaneas() {
		return izqExploradoraSimultaneas;
	}

	public void setIzqExploradoraSimultaneas(String izqExploradoraSimultaneas) {
		this.izqExploradoraSimultaneas = izqExploradoraSimultaneas;
	}

	public String getSumatoriaIntensidad() {
		return sumatoriaIntensidad;
	}

	public void setSumatoriaIntensidad(String sumatoriaIntensidad) {
		this.sumatoriaIntensidad = sumatoriaIntensidad;
	}
	
	

}
