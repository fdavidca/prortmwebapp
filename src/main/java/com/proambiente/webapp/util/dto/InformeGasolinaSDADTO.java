package com.proambiente.webapp.util.dto;

import com.proambiente.webapp.util.informes.medellin.InformeLivianosRes0762;

public class InformeGasolinaSDADTO extends InformeCarInformacionCDADTO{
	
	protected String numeroCda;
	protected String consecutivoPrueba;
	protected String codigoCiudad;
	protected String nivelEmisiones;
	protected String vlrPEf;
	protected String numeroSerieBanco;
	protected String numeroSerieAnalizador;
	protected String marcaAnalizador;
	
	protected String vlrSpanBajoHC;
	protected String vlrSpanBajoCO;
	protected String vlrSpanBajoCO2;
	protected String vlrSpanAltoHC;
	protected String vlrSpanAltoCO;
	protected String vlrSpanAltoCO2;
	
	
	protected String resultadoVlrSpanBajoHC;
	protected String resultadoVlrSpanBajoCO;
	protected String resultadoVlrSpanBajoCO2;
	protected String resultadoVlrSpanAltoHC;
	protected String resultadoVlrSpanAltoCO;
	protected String resultadoVlrSpanAltoCO2;
	
	
	protected String fechaHoraUltimaCalibracion;
	protected String nombreSoftware;
	protected String versionSoftware;
	
	
	protected String metodoMedicionTemperatura;
	protected String temperaturaAmbiente;
	protected String humedadRelativa;
	
	protected String fugasTuboEscape;
	protected String fugasSilenciador;
	
	protected String accesoriosDeformacionesImpiden;
	protected String fugasTapaCombustible;
	protected String fugasTapaAceite;
	protected String sistemaAdmisionAire;
	protected String salidasAdicionalesDiseno;
	protected String pcv;
	protected String presenciaHumoNegroAzul;
	protected String revolucionesFueraRango;
	protected String fallaSistemaRefrigeracion;
	
	protected String temperaturaDeMotor;
	protected String rpmRalenti;
	protected String hcRalenti;
	protected String coRalenti;
	protected String co2Ralenti;
	protected String o2Ralenti;
	protected String rpmCrucero;
	protected String hcCrucero;
	protected String coCrucero;
	protected String co2Crucero;
	protected String o2Crucero;
	protected String dilucion;
	protected String incumplimientoNivelesEmision;
	protected String causalRechazo;
	protected String conceptoFinalDelVehiculo;
	
	protected ResultadoSonometriaInformeDTO resultadoSonometria;
	protected InformeLivianosRes0762 subInforme;
	protected String fechaInicioPrueba;
	protected String fechaFinPrueba;
	protected String fechaVerificacionGasolina;
	protected String codigoCiudadPropietario;
	protected String codigoLineaVehiculo;
	protected String codigoClaseVehiculo;
	protected String codigoCombustible;
	protected String codigoServicio;
	protected String codigoMarca;
	protected String temperaturaMotor;
	
	
	
	public String getTemperaturaMotor() {
		return temperaturaMotor;
	}
	public void setTemperaturaMotor(String temperaturaMotor) {
		this.temperaturaMotor = temperaturaMotor;
	}
	public String getCodigoMarca() {
		return codigoMarca;
	}
	public void setCodigoMarca(String codigoMarca) {
		this.codigoMarca = codigoMarca;
	}
	public String getCodigoCombustible() {
		return codigoCombustible;
	}
	public void setCodigoCombustible(String codigoCombustible) {
		this.codigoCombustible = codigoCombustible;
	}
	public String getCodigoServicio() {
		return codigoServicio;
	}
	public void setCodigoServicio(String codigoServicio) {
		this.codigoServicio = codigoServicio;
	}
	public String getCodigoClaseVehiculo() {
		return codigoClaseVehiculo;
	}
	public void setCodigoClaseVehiculo(String codigoClaseVehiculo) {
		this.codigoClaseVehiculo = codigoClaseVehiculo;
	}
	public String getCodigoLineaVehiculo() {
		return codigoLineaVehiculo;
	}
	public void setCodigoLineaVehiculo(String codigoLineaVehiculo) {
		this.codigoLineaVehiculo = codigoLineaVehiculo;
	}
	public String getCodigoCiudadPropietario() {
		return codigoCiudadPropietario;
	}
	public void setCodigoCiudadPropietario(String codigoCiudadPropietario) {
		this.codigoCiudadPropietario = codigoCiudadPropietario;
	}
	public String getFechaVerificacionGasolina() {
		return fechaVerificacionGasolina;
	}
	public void setFechaVerificacionGasolina(String fechaVerificacionGasolina) {
		this.fechaVerificacionGasolina = fechaVerificacionGasolina;
	}
	public String getFechaInicioPrueba() {
		return fechaInicioPrueba;
	}
	public void setFechaInicioPrueba(String fechaInicioPrueba) {
		this.fechaInicioPrueba = fechaInicioPrueba;
	}
	public String getFechaFinPrueba() {
		return fechaFinPrueba;
	}
	public void setFechaFinPrueba(String fechaFinPrueba) {
		this.fechaFinPrueba = fechaFinPrueba;
	}
	public String getConsecutivoPrueba() {
		return consecutivoPrueba;
	}
	public void setConsecutivoPrueba(String consecutivoPrueba) {
		this.consecutivoPrueba = consecutivoPrueba;
	}
	public String getNumeroCda() {
		return numeroCda;
	}
	public void setNumeroCda(String numeroCda) {
		this.numeroCda = numeroCda;
	}
	public String getVlrPEf() {
		return vlrPEf;
	}
	public void setVlrPEf(String vlrPEf) {
		this.vlrPEf = vlrPEf;
	}
	public String getNumeroSerieBanco() {
		return numeroSerieBanco;
	}
	public void setNumeroSerieBanco(String numeroSerieBanco) {
		this.numeroSerieBanco = numeroSerieBanco;
	}
	public String getNumeroSerieAnalizador() {
		return numeroSerieAnalizador;
	}
	public void setNumeroSerieAnalizador(String numeroSerieAnalizador) {
		this.numeroSerieAnalizador = numeroSerieAnalizador;
	}
	public String getMarcaAnalizador() {
		return marcaAnalizador;
	}
	public void setMarcaAnalizador(String marcaAnalizador) {
		this.marcaAnalizador = marcaAnalizador;
	}
	public String getVlrSpanBajoHC() {
		return vlrSpanBajoHC;
	}
	public void setVlrSpanBajoHC(String vlrSpanBajoHC) {
		this.vlrSpanBajoHC = vlrSpanBajoHC;
	}
	public String getVlrSpanBajoCO() {
		return vlrSpanBajoCO;
	}
	public void setVlrSpanBajoCO(String vlrSpanBajoCO) {
		this.vlrSpanBajoCO = vlrSpanBajoCO;
	}
	public String getVlrSpanBajoCO2() {
		return vlrSpanBajoCO2;
	}
	public void setVlrSpanBajoCO2(String vlrSpanBajoCO2) {
		this.vlrSpanBajoCO2 = vlrSpanBajoCO2;
	}
	public String getVlrSpanAltoHC() {
		return vlrSpanAltoHC;
	}
	public void setVlrSpanAltoHC(String vlrSpanAltoHC) {
		this.vlrSpanAltoHC = vlrSpanAltoHC;
	}
	public String getVlrSpanAltoCO() {
		return vlrSpanAltoCO;
	}
	public void setVlrSpanAltoCO(String vlrSpanAltoCO) {
		this.vlrSpanAltoCO = vlrSpanAltoCO;
	}
	public String getVlrSpanAltoCO2() {
		return vlrSpanAltoCO2;
	}
	public void setVlrSpanAltoCO2(String vlrSpanAltoCO2) {
		this.vlrSpanAltoCO2 = vlrSpanAltoCO2;
	}
	public String getFechaHoraUltimaCalibracion() {
		return fechaHoraUltimaCalibracion;
	}
	public void setFechaHoraUltimaCalibracion(String fechaHoraUltimaCalibracion) {
		this.fechaHoraUltimaCalibracion = fechaHoraUltimaCalibracion;
	}
	public String getNombreSoftware() {
		return nombreSoftware;
	}
	public void setNombreSoftware(String nombreSoftware) {
		this.nombreSoftware = nombreSoftware;
	}
	public String getVersionSoftware() {
		return versionSoftware;
	}
	public void setVersionSoftware(String versionSoftware) {
		this.versionSoftware = versionSoftware;
	}
	public String getMetodoMedicionTemperatura() {
		return metodoMedicionTemperatura;
	}
	public void setMetodoMedicionTemperatura(String metodoMedicionTemperatura) {
		this.metodoMedicionTemperatura = metodoMedicionTemperatura;
	}
	public String getTemperaturaAmbiente() {
		return temperaturaAmbiente;
	}
	public void setTemperaturaAmbiente(String temperaturaAmbiente) {
		this.temperaturaAmbiente = temperaturaAmbiente;
	}
	public String getHumedadRelativa() {
		return humedadRelativa;
	}
	public void setHumedadRelativa(String humedadRelativa) {
		this.humedadRelativa = humedadRelativa;
	}
	public String getFugasTuboEscape() {
		return fugasTuboEscape;
	}
	public void setFugasTuboEscape(String fugasTuboEscape) {
		this.fugasTuboEscape = fugasTuboEscape;
	}
	public String getFugasSilenciador() {
		return fugasSilenciador;
	}
	public void setFugasSilenciador(String fugasSilenciador) {
		this.fugasSilenciador = fugasSilenciador;
	}
	public String getAccesoriosDeformacionesImpiden() {
		return accesoriosDeformacionesImpiden;
	}
	public void setAccesoriosDeformacionesImpiden(String accesoriosDeformacionesImpiden) {
		this.accesoriosDeformacionesImpiden = accesoriosDeformacionesImpiden;
	}
	public String getFugasTapaCombustible() {
		return fugasTapaCombustible;
	}
	public void setFugasTapaCombustible(String fugasTapaCombustible) {
		this.fugasTapaCombustible = fugasTapaCombustible;
	}
	public String getFugasTapaAceite() {
		return fugasTapaAceite;
	}
	public void setFugasTapaAceite(String fugasTapaAceite) {
		this.fugasTapaAceite = fugasTapaAceite;
	}
	public String getSistemaAdmisionAire() {
		return sistemaAdmisionAire;
	}
	public void setSistemaAdmisionAire(String sistemaAdmisionAire) {
		this.sistemaAdmisionAire = sistemaAdmisionAire;
	}
	public String getSalidasAdicionalesDiseno() {
		return salidasAdicionalesDiseno;
	}
	public void setSalidasAdicionalesDiseno(String salidasAdicionalesDiseno) {
		this.salidasAdicionalesDiseno = salidasAdicionalesDiseno;
	}
	public String getPcv() {
		return pcv;
	}
	public void setPcv(String pcv) {
		this.pcv = pcv;
	}
	public String getPresenciaHumoNegroAzul() {
		return presenciaHumoNegroAzul;
	}
	public void setPresenciaHumoNegroAzul(String presenciaHumoNegroAzul) {
		this.presenciaHumoNegroAzul = presenciaHumoNegroAzul;
	}
	public String getRevolucionesFueraRango() {
		return revolucionesFueraRango;
	}
	public void setRevolucionesFueraRango(String revolucionesFueraRango) {
		this.revolucionesFueraRango = revolucionesFueraRango;
	}
	public String getFallaSistemaRefrigeracion() {
		return fallaSistemaRefrigeracion;
	}
	public void setFallaSistemaRefrigeracion(String fallaSistemaRefrigeracion) {
		this.fallaSistemaRefrigeracion = fallaSistemaRefrigeracion;
	}
	public String getTemperaturaDeMotor() {
		return temperaturaDeMotor;
	}
	public void setTemperaturaDeMotor(String temperaturaDeMotor) {
		this.temperaturaDeMotor = temperaturaDeMotor;
	}
	public String getRpmRalenti() {
		return rpmRalenti;
	}
	public void setRpmRalenti(String rpmRalenti) {
		this.rpmRalenti = rpmRalenti;
	}
	public String getHcRalenti() {
		return hcRalenti;
	}
	public void setHcRalenti(String hcRalenti) {
		this.hcRalenti = hcRalenti;
	}
	public String getCoRalenti() {
		return coRalenti;
	}
	public void setCoRalenti(String coRalenti) {
		this.coRalenti = coRalenti;
	}
	public String getCo2Ralenti() {
		return co2Ralenti;
	}
	public void setCo2Ralenti(String co2Ralenti) {
		this.co2Ralenti = co2Ralenti;
	}
	public String getO2Ralenti() {
		return o2Ralenti;
	}
	public void setO2Ralenti(String o2Ralenti) {
		this.o2Ralenti = o2Ralenti;
	}
	public String getRpmCrucero() {
		return rpmCrucero;
	}
	public void setRpmCrucero(String rpmCrucero) {
		this.rpmCrucero = rpmCrucero;
	}
	public String getHcCrucero() {
		return hcCrucero;
	}
	public void setHcCrucero(String hcCrucero) {
		this.hcCrucero = hcCrucero;
	}
	public String getCoCrucero() {
		return coCrucero;
	}
	public void setCoCrucero(String coCrucero) {
		this.coCrucero = coCrucero;
	}
	public String getCo2Crucero() {
		return co2Crucero;
	}
	public void setCo2Crucero(String co2Crucero) {
		this.co2Crucero = co2Crucero;
	}
	public String getO2Crucero() {
		return o2Crucero;
	}
	public void setO2Crucero(String o2Crucero) {
		this.o2Crucero = o2Crucero;
	}
	public String getDilucion() {
		return dilucion;
	}
	public void setDilucion(String dilucion) {
		this.dilucion = dilucion;
	}
	public String getIncumplimientoNivelesEmision() {
		return incumplimientoNivelesEmision;
	}
	public void setIncumplimientoNivelesEmision(String incumplimientoNivelesEmision) {
		this.incumplimientoNivelesEmision = incumplimientoNivelesEmision;
	}
	public String getConceptoFinalDelVehiculo() {
		return conceptoFinalDelVehiculo;
	}
	public void setConceptoFinalDelVehiculo(String conceptoFinalDelVehiculo) {
		this.conceptoFinalDelVehiculo = conceptoFinalDelVehiculo;
	}
	public ResultadoSonometriaInformeDTO getResultadoSonometria() {
		return resultadoSonometria;
	}
	public void setResultadoSonometria(ResultadoSonometriaInformeDTO resultadoSonometria) {
		this.resultadoSonometria = resultadoSonometria;
	}
	public String getResultadoVlrSpanBajoHC() {
		return resultadoVlrSpanBajoHC;
	}
	public void setResultadoVlrSpanBajoHC(String resultadoVlrSpanBajoHC) {
		this.resultadoVlrSpanBajoHC = resultadoVlrSpanBajoHC;
	}
	public String getResultadoVlrSpanBajoCO() {
		return resultadoVlrSpanBajoCO;
	}
	public void setResultadoVlrSpanBajoCO(String resultadoVlrSpanBajoCO) {
		this.resultadoVlrSpanBajoCO = resultadoVlrSpanBajoCO;
	}
	public String getResultadoVlrSpanBajoCO2() {
		return resultadoVlrSpanBajoCO2;
	}
	public void setResultadoVlrSpanBajoCO2(String resultadoVlrSpanBajoCO2) {
		this.resultadoVlrSpanBajoCO2 = resultadoVlrSpanBajoCO2;
	}
	public String getResultadoVlrSpanAltoHC() {
		return resultadoVlrSpanAltoHC;
	}
	public void setResultadoVlrSpanAltoHC(String resultadoVlrSpanAltoHC) {
		this.resultadoVlrSpanAltoHC = resultadoVlrSpanAltoHC;
	}
	public String getResultadoVlrSpanAltoCO() {
		return resultadoVlrSpanAltoCO;
	}
	public void setResultadoVlrSpanAltoCO(String resultadoVlrSpanAltoCO) {
		this.resultadoVlrSpanAltoCO = resultadoVlrSpanAltoCO;
	}
	public String getResultadoVlrSpanAltoCO2() {
		return resultadoVlrSpanAltoCO2;
	}
	public void setResultadoVlrSpanAltoCO2(String resultadoVlrSpanAltoCO2) {
		this.resultadoVlrSpanAltoCO2 = resultadoVlrSpanAltoCO2;
	}
	public String getCausalRechazo() {
		return causalRechazo;
	}
	public void setCausalRechazo(String causalRechazo) {
		this.causalRechazo = causalRechazo;
	}
	public InformeLivianosRes0762 getSubInforme() {
		return subInforme;
	}
	public void setSubInforme(InformeLivianosRes0762 subInforme) {
		this.subInforme = subInforme;
	}
	public String getNivelEmisiones() {
		return nivelEmisiones;
	}
	public void setNivelEmisiones(String nivelEmisiones) {
		this.nivelEmisiones = nivelEmisiones;
	}
	public String getCodigoCiudad() {
		return codigoCiudad;
	}
	public void setCodigoCiudad(String codigoCiudad) {
		this.codigoCiudad = codigoCiudad;
	}
	
		
	
}
