package com.proambiente.webapp.util.dto;

public class ResultadoOttoMotocicletasInformeDTO {
	
	private String temperaturaAmbiente;
	private String humedadRelativa;
	public String getTemperaturaAmbiente() {
		return temperaturaAmbiente;
	}
	public void setTemperaturaAmbiente(String temperaturaAmbiente) {
		this.temperaturaAmbiente = temperaturaAmbiente;
	}
	public String getHumedadRelativa() {
		return humedadRelativa;
	}
	public void setHumedadRelativa(String humedadRelativa) {
		this.humedadRelativa = humedadRelativa;
	}
	
	

}
