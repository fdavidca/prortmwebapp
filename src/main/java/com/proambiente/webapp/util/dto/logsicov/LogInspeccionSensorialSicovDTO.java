package com.proambiente.webapp.util.dto.logsicov;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LogInspeccionSensorialSicovDTO {
	
	@JsonProperty("Observaciones")
	private String observaciones = "";
	
	@JsonProperty("CodigoRechazo")
	private String codigoRechazo = "";
	
	
	private String tablaAfectada = "";
	private String idRegistro = "";
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public String getCodigoRechazo() {
		return codigoRechazo;
	}
	public void setCodigoRechazo(String codigoRechazo) {
		this.codigoRechazo = codigoRechazo;
	}
	public String getTablaAfectada() {
		return tablaAfectada;
	}
	public void setTablaAfectada(String tablaAfectada) {
		this.tablaAfectada = tablaAfectada;
	}
	public String getIdRegistro() {
		return idRegistro;
	}
	public void setIdRegistro(String idRegistro) {
		this.idRegistro = idRegistro;
	}	

}
