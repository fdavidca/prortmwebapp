package com.proambiente.webapp.util.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RegistroVehiculoDTO {
	
	private String placa;
	
	@Min(value = 1900, message = "El modelo no puede ser menor a  1900")
	@Max(value = 2030, message = "El modelo no puede ser mayor a 2030")
	private Integer modelo;
	private Integer tipoVehiculoId;
	
	private String numeroMotor;
	private String vin;
	private String numeroSerie;
	private String numeroChasis;
	private String marca;
	private String linea;
	private String combustible;
	private String color;
	private String servicio;
	private String clase;
	private String numeroLicencia;
	private String fechaMatricula;
	private String cilindraje;
	private String kilometraje;
	private String numeroSoat;
	private String fechaSoat;
	private String tiemposMotor;
	private String potencia;
	
	@JsonProperty("numero_sillas")
	private String numeroSillas;
	@JsonProperty("vidrios_polarizados")
	private boolean vidriosPolarizados;
	private boolean blindado;
	
	private String conversionGNV;
	private String fechaConversionGNV;
	
	private Integer idTipoServicio;//obligatorio
	private Integer idTipoCombustible;//obligatorio
	private Integer idClaseVehiculo;//obligatorio
	

	private Integer idMarca;
	private Integer idLinea;
	private Integer idColor;
	
	private Integer numeroPasajerosSentados;
	
	private RegistroPropietarioDTO propietario;
	
	private RegistroPropietarioDTO conductor;
	
	
	

	public Integer getNumeroPasajerosSentados() {
		return numeroPasajerosSentados;
	}
	public void setNumeroPasajerosSentados(Integer numeroPasajerosSentados) {
		this.numeroPasajerosSentados = numeroPasajerosSentados;
	}
	public Integer getIdColor() {
		return idColor;
	}
	public void setIdColor(Integer idColor) {
		this.idColor = idColor;
	}
	public Integer getIdMarca() {
		return idMarca;
	}
	public void setIdMarca(Integer idMarca) {
		this.idMarca = idMarca;
	}
	public Integer getIdLinea() {
		return idLinea;
	}
	public void setIdLinea(Integer idLinea) {
		this.idLinea = idLinea;
	}
	public Integer getIdTipoServicio() {
		return idTipoServicio;
	}
	public void setIdTipoServicio(Integer idTipoServicio) {
		this.idTipoServicio = idTipoServicio;
	}
	public Integer getIdTipoCombustible() {
		return idTipoCombustible;
	}
	public void setIdTipoCombustible(Integer idTipoCombustible) {
		this.idTipoCombustible = idTipoCombustible;
	}
	public Integer getIdClaseVehiculo() {
		return idClaseVehiculo;
	}
	public void setIdClaseVehiculo(Integer idClaseVehiculo) {
		this.idClaseVehiculo = idClaseVehiculo;
	}
	public Integer getTipoVehiculoId() {
		return tipoVehiculoId;
	}
	public void setTipoVehiculoId(Integer tipoVehiculoId) {
		this.tipoVehiculoId = tipoVehiculoId;
	}
	public String getPotencia() {
		return potencia;
	}
	public void setPotencia(String potencia) {
		this.potencia = potencia;
	}
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public Integer getModelo() {
		return modelo;
	}
	public void setModelo(Integer modelo) {
		this.modelo = modelo;
	}
	public String getNumeroMotor() {
		return numeroMotor;
	}
	public void setNumeroMotor(String numeroMotor) {
		this.numeroMotor = numeroMotor;
	}
	public String getVin() {
		return vin;
	}
	public void setVin(String vin) {
		this.vin = vin;
	}
	public String getNumeroSerie() {
		return numeroSerie;
	}
	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}
	public String getNumeroChasis() {
		return numeroChasis;
	}
	public void setNumeroChasis(String numeroChasis) {
		this.numeroChasis = numeroChasis;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getLinea() {
		return linea;
	}
	public void setLinea(String linea) {
		this.linea = linea;
	}
	public String getCombustible() {
		return combustible;
	}
	public void setCombustible(String combustible) {
		this.combustible = combustible;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getServicio() {
		return servicio;
	}
	public void setServicio(String servicio) {
		this.servicio = servicio;
	}
	public String getClase() {
		return clase;
	}
	public void setClase(String clase) {
		this.clase = clase;
	}
	public String getNumeroLicencia() {
		return numeroLicencia;
	}
	public void setNumeroLicencia(String numeroLicencia) {
		this.numeroLicencia = numeroLicencia;
	}
	public String getFechaMatricula() {
		return fechaMatricula;
	}
	public void setFechaMatricula(String fechaMatricula) {
		this.fechaMatricula = fechaMatricula;
	}
	public String getCilindraje() {
		return cilindraje;
	}
	public void setCilindraje(String cilindraje) {
		this.cilindraje = cilindraje;
	}
	public String getKilometraje() {
		return kilometraje;
	}
	public void setKilometraje(String kilometraje) {
		this.kilometraje = kilometraje;
	}
	public String getNumeroSoat() {
		return numeroSoat;
	}
	public void setNumeroSoat(String numeroSoat) {
		this.numeroSoat = numeroSoat;
	}
	public String getFechaSoat() {
		return fechaSoat;
	}
	public void setFechaSoat(String fechaSoat) {
		this.fechaSoat = fechaSoat;
	}
	public String getTiemposMotor() {
		return tiemposMotor;
	}
	public void setTiemposMotor(String tiemposMotor) {
		this.tiemposMotor = tiemposMotor;
	}
	public String getNumeroSillas() {
		return numeroSillas;
	}
	public void setNumeroSillas(String numeroSillas) {
		this.numeroSillas = numeroSillas;
	}
	public RegistroPropietarioDTO getPropietario() {
		return propietario;
	}
	public void setPropietario(RegistroPropietarioDTO propietario) {
		this.propietario = propietario;
	}
	public RegistroPropietarioDTO getConductor() {
		return conductor;
	}
	public void setConductor(RegistroPropietarioDTO conductor) {
		this.conductor = conductor;
	}
	
	
	
	public boolean isVidriosPolarizados() {
		return vidriosPolarizados;
	}
	public void setVidriosPolarizados(boolean vidriosPolarizados) {
		this.vidriosPolarizados = vidriosPolarizados;
	}
	public boolean isBlindado() {
		return blindado;
	}
	public void setBlindado(boolean blindaddo) {
		this.blindado = blindaddo;
	}
	
	public String getConversionGNV() {
		return conversionGNV;
	}
	public void setConversionGNV(String conversionGNV) {
		this.conversionGNV = conversionGNV;
	}
	public String getFechaConversionGNV() {
		return fechaConversionGNV;
	}
	public void setFechaConversionGNV(String fechaConversionGNV) {
		this.fechaConversionGNV = fechaConversionGNV;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RegistroVehiculoDTO [placa=");
		builder.append(placa);
		builder.append(", modelo=");
		builder.append(modelo);
		builder.append(", tipoVehiculoId=");
		builder.append(tipoVehiculoId);
		builder.append(", numeroMotor=");
		builder.append(numeroMotor);
		builder.append(", vin=");
		builder.append(vin);
		builder.append(", numeroSerie=");
		builder.append(numeroSerie);
		builder.append(", numeroChasis=");
		builder.append(numeroChasis);
		builder.append(", marca=");
		builder.append(marca);
		builder.append(", linea=");
		builder.append(linea);
		builder.append(", combustible=");
		builder.append(combustible);
		builder.append(", color=");
		builder.append(color);
		builder.append(", servicio=");
		builder.append(servicio);
		builder.append(", clase=");
		builder.append(clase);
		builder.append(", numeroLicencia=");
		builder.append(numeroLicencia);
		builder.append(", fechaMatricula=");
		builder.append(fechaMatricula);
		builder.append(", cilindraje=");
		builder.append(cilindraje);
		builder.append(", kilometraje=");
		builder.append(kilometraje);
		builder.append(", numeroSoat=");
		builder.append(numeroSoat);
		builder.append(", fechaSoat=");
		builder.append(fechaSoat);
		builder.append(", tiemposMotor=");
		builder.append(tiemposMotor);
		builder.append(", potencia=");
		builder.append(potencia);
		builder.append(", numeroSillas=");
		builder.append(numeroSillas);
		builder.append(", vidriosPolarizados=");
		builder.append(vidriosPolarizados);
		builder.append(", blindado=");
		builder.append(blindado);
		builder.append(", conversionGNV=");
		builder.append(conversionGNV);
		builder.append(", fechaConversionGNV=");
		builder.append(fechaConversionGNV);
		builder.append(", propietario=");
		builder.append(propietario);
		builder.append(", conductor=");
		builder.append(conductor);
		builder.append("]");
		return builder.toString();
	}
	

	
	
}