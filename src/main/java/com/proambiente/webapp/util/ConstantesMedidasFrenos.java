package com.proambiente.webapp.util;

public class ConstantesMedidasFrenos {
	
	public static final int PESO_DERECHO_EJE_UNO = 5000;
    public static final int PESO_DERECHO_EJE_DOS = 5001;
    public static final int PESO_DERECHO_EJE_TRES = 5002;
    public static final int PESO_DERECHO_EJE_CUATRO = 5003;
    public static final int PESO_IZQUIERDO_EJE_UNO = 5004;
    public static final int PESO_IZQUIERDO_EJE_DOS = 5005;
    public static final int PESO_IZQUIERDO_EJE_TRES = 5006;
    public static final int PESO_IZQUIERDO_EJE_CUATRO = 5007;

    
    
    public static final int FUERZA_DERECHA_EJE_UNO = 5008;
    public static final int FUERZA_DERECHA_EJE_DOS = 5009;
    public static final int FUERZA_DERECHA_EJE_TRES = 5010;
    public static final int FUERZA_DERECHA_EJE_CUATRO = 5011;
    public static final int FUERZA_IZQUIERDA_EJE_UNO = 5012;
    public static final int FUERZA_IZQUIERDA_EJE_DOS = 5013;
    public static final int FUERZA_IZQUIERDA_EJE_TRES = 5014;
    public static final int FUERZA_IZQUIERDA_EJE_CUATRO = 5015;
    
    public static final int FUERZA_DERECHA_FRENO_MANO = 5016;
    public static final int FUERZA_IZQUIERDA_FRENO_MANO = 5020;
    
    public static final int PESO_DERECHO_TOTAL = 5025;
    public static final int PESO_IZQUIERDO_TOTAL = 5026;  
    
    public static final int FUERZA_TOTAL_FRENO_MANO_DERECHA = 5027;
    public static final int FUERZA_TOTAL_FRENO_MANO_IZQUIERDA = 5028;
    
    public static final int EFICACIA_FRENADO_TOTAL = 5024;
    public static final int EFICACIA_FRENADO_AUXILIAR = 5036;
    public static final int DESEQUILIBRIO_EJE_UNO = 5032;
    public static final int DESEQUILIBRIO_EJE_DOS = 5033;
    public static final int DESEQUILIBRIO_EJE_TRES = 5034;
    public static final int DESEQUILIBRIO_EJE_CUATRO = 5035;
	public static final int DESEQUILIBRIO_EJE_CINCO = 5114;//TODO implementar estos codigos
	public static final int DESEQUILIBRIO_EJE_SEIS = 0;//TODO implementar estos codigos
	public static final int FUERZA_IZQUIERDA_EJE_CINCO = 5113;//TODO implementar estos codigos
	public static final int FUERZA_IZQUIERDA_EJE_SEIS = 0;//TODO implementar estos codigos
	public static final int FUERZA_DERECHA_EJE_CINCO = 5112;//TODO implementar estos codigos
	public static final int FUERZA_DERECHA_EJE_SEIS = 0;//TODO implementar estos codigos
	
	public static final int PESO_DERECHO_EJE_CINCO = 5110;//TODO implementar estos codigos
	public static final int PESO_DERECHO_EJE_SEIS = -1;//TODO implementar estos codigos
	public static final int PESO_IZQUIERDO_EJE_CINCO = 5111;
	public static final int PESO_IZQUIERDO_EJE_SEIS = -99;
	
	 public static final int PESO_DERECHO_FREN_ESTA_CICLO = 5120;
	 public static final int FUERZA_DERECHA_FREN_ESTA_CICLO = 5121;
	 public static final int EFICACIA_ESTACIONAMIENTO = 5124;
	
	

}
