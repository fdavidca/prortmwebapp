package com.proambiente.webapp.util.resultadototal;

import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE1_DER;
import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE1_IZQ;
import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE2_DER_1;
import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE2_DER_2;
import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE2_IZQ_1;
import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE2_IZQ_2;
import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE3_DER_1;
import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE3_DER_2;
import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE3_IZQ_1;
import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE3_IZQ_2;
import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE4_DER_1;
import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE4_DER_2;
import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE4_IZQ_1;
import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE4_IZQ_2;
import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE5_DER_1;
import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE5_DER_2;
import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE5_IZQ_1;
import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_EJE5_IZQ_2;
import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_REPUESTO_DER;
import static com.proambiente.webapp.util.ConstantesMedidasProfundidaLabrado.PROFUNDIDAD_LABRADO_REPUESTO_IZQ;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.proambiente.modelo.Medida;
import com.proambiente.modelo.Prueba;
import com.proambiente.webapp.service.util.FormateadorMedidasInterface;
import com.proambiente.webapp.util.dto.InformeResultadoTotalDTO;

public class UtilColocarProfundidadLabradoInforme {

	public void colocarInformacionProfundidadLabrado(List<Prueba> pruebas, Boolean isMoto, Boolean isPesado,
			InformeResultadoTotalDTO informe, FormateadorMedidasInterface formateadorMedidas,
			List<Medida> medidasProfundidadLabrado) {

		Optional<Medida> medidaDelanteraDerecha = medidasProfundidadLabrado.stream()
				.filter(m -> m.getTipoMedida().getTipoMedidaId().equals(5060)).findAny();
		Optional<Medida> medidaDelanteraIzquierda = medidasProfundidadLabrado.stream()
				.filter(m -> m.getTipoMedida().getTipoMedidaId().equals(5061)).findAny();
		Optional<Medida> medidaTraseraDerecha = medidasProfundidadLabrado.stream()
				.filter(m -> m.getTipoMedida().getTipoMedidaId().equals(5062)).findAny();

		Optional<Medida> medidaTraseraIzquierda = medidasProfundidadLabrado.stream()
				.filter(m -> m.getTipoMedida().getTipoMedidaId().equals(5063)).findAny();

		Optional<Medida> valorRepuesto = medidasProfundidadLabrado.stream()
				.filter(m -> m.getTipoMedida().getTipoMedidaId().equals(5064)).findAny();

		if (isMoto) {
			colocarMedidasProfundidadLabradoMoto(medidaDelanteraDerecha, medidaTraseraDerecha, medidaTraseraIzquierda,
					valorRepuesto, informe, formateadorMedidas);
		} else if (!isPesado) {
			colocarMedidasProfundidadLabrado(medidaDelanteraDerecha, medidaDelanteraIzquierda, medidaTraseraDerecha,
					medidaTraseraIzquierda, valorRepuesto, informe, formateadorMedidas);
		} else {
			colocarProfundidadLabradoPesado(medidasProfundidadLabrado, informe, formateadorMedidas);
		}

	}

	public void colocarProfundidadLabradoPesado(List<Medida> medidasProfundidadLabrado, InformeResultadoTotalDTO informe,
			FormateadorMedidasInterface formateadorMedidas) {

		for (Medida m : medidasProfundidadLabrado) {
			switch (m.getTipoMedida().getTipoMedidaId()) {
			case PROFUNDIDAD_LABRADO_EJE1_IZQ:
				informe.setProfEje1Izq(formateadorMedidas.formatearMedida(m));				
				break;
			case PROFUNDIDAD_LABRADO_EJE1_DER:
				informe.setProfEje1Der(formateadorMedidas.formatearMedida(m));				
				break;
			case PROFUNDIDAD_LABRADO_EJE2_IZQ_2:
				informe.setProfEje2Izq2(formateadorMedidas.formatearMedida(m));				
				break;
			case PROFUNDIDAD_LABRADO_EJE2_IZQ_1:
				informe.setProfEje2Izq1(formateadorMedidas.formatearMedida(m));				
				break;
			case PROFUNDIDAD_LABRADO_EJE2_DER_1:
				informe.setProfEje2Der1(formateadorMedidas.formatearMedida(m));
				break;
			case PROFUNDIDAD_LABRADO_EJE2_DER_2:
				informe.setProfEje2Der2(formateadorMedidas.formatearMedida(m));
				break;
			case PROFUNDIDAD_LABRADO_EJE3_IZQ_2:
				informe.setProfEje3Izq2(formateadorMedidas.formatearMedida(m));
				break;
			case PROFUNDIDAD_LABRADO_EJE3_IZQ_1:
				informe.setProfEje3Izq1(formateadorMedidas.formatearMedida(m));				
				break;
			case PROFUNDIDAD_LABRADO_EJE3_DER_1:
				informe.setProfEje3Der1(formateadorMedidas.formatearMedida(m));
				break;
			case PROFUNDIDAD_LABRADO_EJE3_DER_2:
				informe.setProfEje3Der2(formateadorMedidas.formatearMedida(m));
				break;
			case PROFUNDIDAD_LABRADO_EJE4_IZQ_2:
				informe.setProfEje4Izq2(formateadorMedidas.formatearMedida(m));				
				break;
			case PROFUNDIDAD_LABRADO_EJE4_IZQ_1:
				informe.setProfEje4Izq1(formateadorMedidas.formatearMedida(m));				
				break;
			case PROFUNDIDAD_LABRADO_EJE4_DER_1:
				informe.setProfEje4Der1(formateadorMedidas.formatearMedida(m));				
				break;
			case PROFUNDIDAD_LABRADO_EJE4_DER_2:
				informe.setProfEje4Der2(formateadorMedidas.formatearMedida(m));				
				break;
			case PROFUNDIDAD_LABRADO_EJE5_IZQ_2:
				informe.setProfEje5Izq2(formateadorMedidas.formatearMedida(m));
				break;
			case PROFUNDIDAD_LABRADO_EJE5_IZQ_1:
				informe.setProfEje5Izq1(formateadorMedidas.formatearMedida(m));
				break;
			case PROFUNDIDAD_LABRADO_EJE5_DER_1:
				informe.setProfEje5Der1(formateadorMedidas.formatearMedida(m));
				break;
			case PROFUNDIDAD_LABRADO_EJE5_DER_2:
				informe.setProfEje5Der2(formateadorMedidas.formatearMedida(m));
				break;
			case PROFUNDIDAD_LABRADO_REPUESTO_DER:
				informe.setProfRepDer(formateadorMedidas.formatearMedida(m));
				break;
			case PROFUNDIDAD_LABRADO_REPUESTO_IZQ:
				informe.setProfRepIzq(formateadorMedidas.formatearMedida(m));
				break;
			}
		}
	}
	
	public void colocarMedidasProfundidadLabradoMoto(Optional<Medida> valorDelanteraDerecha,
			Optional<Medida> valorTraseraDerecha, Optional<Medida> valorTraseraIzquierda,Optional<Medida> valorRepuesto,
			InformeResultadoTotalDTO informe,FormateadorMedidasInterface formateadorMedidas) {

		
		String strDD = valorDelanteraDerecha.isPresent() ? formateadorMedidas.formatearMedida(valorDelanteraDerecha.get()) : "   ";
		String strTD = valorTraseraDerecha.isPresent() ? formateadorMedidas.formatearMedida(valorTraseraDerecha.get()) : "   ";
		String strTI = valorTraseraIzquierda.isPresent() ? formateadorMedidas.formatearMedida(valorTraseraIzquierda.get()) : "";
		String strRepuesto = valorRepuesto.isPresent() ? formateadorMedidas.formatearMedida(valorRepuesto.get()): " ";

		informe.setProfEje1Der(strDD);
		informe.setProfEje2Der1(strTD);
		informe.setProfEje2Izq1(strTI);
		informe.setProfRepDer(strRepuesto);
		
	}

	public void colocarMedidasProfundidadLabrado(Optional<Medida> valorDelanteraDerecha,
			Optional<Medida> valorDelanteraIzquierda, Optional<Medida> valorTraseraDerecha,
			Optional<Medida> valorTraseraIzquierda, Optional<Medida> valorRepuesto,
			InformeResultadoTotalDTO informe,FormateadorMedidasInterface formateadorMedidas) {	

		String strDD = valorDelanteraDerecha.isPresent() ? formateadorMedidas.formatearMedida(valorDelanteraDerecha.get()) : "   ";
		String strDI = valorDelanteraIzquierda.isPresent() ? formateadorMedidas.formatearMedida(valorDelanteraIzquierda.get()) : "  ";
		String strTD = valorTraseraDerecha.isPresent() ? formateadorMedidas.formatearMedida(valorTraseraDerecha.get()) : "   ";
		String strTI = valorTraseraIzquierda.isPresent() ? formateadorMedidas.formatearMedida(valorTraseraIzquierda.get()) : "   ";
		String strRepuesto = valorRepuesto.isPresent() ? formateadorMedidas.formatearMedida(valorRepuesto.get()) : "   ";
		
		informe.setProfEje1Der(strDD);
		informe.setProfEje1Izq(strDI);
		informe.setProfEje2Der1(strTD);
		informe.setProfEje2Izq1(strTI);
		informe.setProfRepDer(strRepuesto);
		
		
	}

}
