package com.proambiente.webapp.util.resultadototal;

import com.proambiente.modelo.dto.ResultadoDieselDTO;
import com.proambiente.webapp.util.dto.InformeResultadoTotalDTO;
import com.proambiente.webapp.util.dto.ResultadoDieselDTOInformes;

public class ColocadorInformacionDiesel {

	public void colocarInformacionDiesel( ResultadoDieselDTO resultadoDiesel,InformeResultadoTotalDTO informe,ResultadoDieselDTOInformes resultadoAdicional){
		
		informe.setOpacidadCiclo1( resultadoDiesel.getAceleracionCero() );
		informe.setOpacidadCiclo2( resultadoDiesel.getAceleracionUno() );
		informe.setOpacidadCiclo3( resultadoDiesel.getAceleracionDos() );
		informe.setOpacidadCiclo4( resultadoDiesel.getAceleracionTres() );
		
		informe.setTemperaturaDiesel( resultadoDiesel.getTemperaturaInicial() );
		informe.setRpmGobernadaDiesel( resultadoDiesel.getRpmGobernada() );
		
		informe.setDensidadHumoCiclo1(resultadoAdicional.getDensidadHumoCiclo0());
		informe.setDensidadHumoCiclo2(resultadoAdicional.getDensidadHumoCiclo1());
		informe.setDensidadHumoCiclo3(resultadoAdicional.getDensidadHumoCiclo2());
		informe.setDensidadHumoCiclo4(resultadoAdicional.getDensidadHumoCiclo3());
		
		informe.setDensdiadHumoResultado(resultadoAdicional.getDensidadHumoFinal());
		
	}
}
