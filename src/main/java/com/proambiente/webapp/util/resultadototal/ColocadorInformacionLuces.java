package com.proambiente.webapp.util.resultadototal;

import com.proambiente.modelo.dto.ResultadoLucesDTO;
import com.proambiente.webapp.util.dto.InformeResultadoTotalDTO;

public class ColocadorInformacionLuces {
	
	public void colocarInformacionLuces(ResultadoLucesDTO resultado,InformeResultadoTotalDTO informe) {
		informe.setInclinacionBajaDerecha(resultado.getDerBajaInclinacion());
		informe.setInclinacionBajaIzquierda(resultado.getIzqBajaInclinacion());
		informe.setIntensidadBajaDerecha( resultado.getDerBajaIntensidad());
		informe.setIntensidadBajaIzquierda( resultado.getIzqBajaIntensidad() );
		informe.setIntensidadTotal( resultado.getSumatoriaIntensidad() );
	}

}
