package com.proambiente.webapp.util.resultadototal;

import com.proambiente.modelo.dto.ResultadoFasDTO;
import com.proambiente.webapp.util.dto.InformeResultadoTotalDTO;

public class ColocadorInformacionDesviacion {

	public void colocarInformacionDesviacion(ResultadoFasDTO resultado,InformeResultadoTotalDTO informe) {
		
		informe.setDesviacionEje1( resultado.getDesviacionLateralEje1() );
		informe.setDesviacionEje2( resultado.getDesviacionLateralEje2() );
		informe.setDesviacionEje3( resultado.getDesviacionLateralEje3() );
		informe.setDesviacionEje4( resultado.getDesviacionLateralEje4() );
		informe.setDesviacionEje5( resultado.getDesviacionLateralEje5() );
		
	}
}
