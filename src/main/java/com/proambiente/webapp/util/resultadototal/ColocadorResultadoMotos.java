package com.proambiente.webapp.util.resultadototal;

import com.proambiente.modelo.dto.ResultadoOttoMotocicletasDTO;
import com.proambiente.webapp.util.dto.InformeResultadoTotalDTO;

public class ColocadorResultadoMotos {

	public void colocarResultadoMotos(InformeResultadoTotalDTO informe,ResultadoOttoMotocicletasDTO resultado) {
		
		informe.setTempRalenti( resultado.getTempRalenti() );
		informe.setRpmRalenti( resultado.getRpmRalenti() );
		informe.setHcRalenti(resultado.getHcRalenti());
		informe.setCoRalenti( resultado.getCoRalenti() );
		informe.setCo2Ralenti( resultado.getCo2Ralenti() );
		informe.setO2Ralenti( resultado.getO2Ralenti() );
		
	}
}
