package com.proambiente.webapp.util.resultadototal;

import java.text.SimpleDateFormat;

import com.google.common.base.Preconditions;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.webapp.util.dto.InformeResultadoTotalDTO;

public class UtilColocarInformacionVehiculo {
	
	public void colocarInformacionVehiculo(Vehiculo vehiculo,InformeResultadoTotalDTO informe) {
		//informe no debe ser null
		Preconditions.checkNotNull(informe,"Referencia a informe null");
		//vehiculo no debe ser null
		Preconditions.checkNotNull(vehiculo,"Vehiculo no debe ser null");
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		informe.setPlaca(vehiculo.getPlaca());
		informe.setServicio( vehiculo.getServicio() !=  null ? vehiculo.getServicio().getNombre() : "");        
        informe.setClase( vehiculo.getClaseVehiculo() != null ? vehiculo.getClaseVehiculo().getNombre() : "" );
        informe.setMarca( vehiculo.getMarca() != null ? vehiculo.getMarca().getNombre() : "");
        informe.setLinea( vehiculo.getLineaVehiculo() != null ? vehiculo.getLineaVehiculo().getNombre() : "");
        informe.setCilindraje( vehiculo.getCilindraje() == null ? "0":String.valueOf( vehiculo.getCilindraje() )  );
        informe.setModelo( String.valueOf(vehiculo.getModelo() )	 );
        informe.setFechaMatricula( sdf.format(vehiculo.getFechaMatricula() ) );
        informe.setCombustible( vehiculo.getTipoCombustible() != null ? vehiculo.getTipoCombustible().getNombre() : "" );
        informe.setTipoMotor( String.valueOf(vehiculo.getTiemposMotor() ) );
	}

}
