package com.proambiente.webapp.util.resultadototal;

import com.google.common.base.Preconditions;
import com.proambiente.modelo.Certificado;
import com.proambiente.webapp.util.dto.InformeResultadoTotalDTO;

public class UtilColocarInformacionCertificado {
	
	public void colocarInformacionCertificado(Certificado certificado,InformeResultadoTotalDTO informe) {
		Preconditions.checkNotNull(informe,"Referencia a informe null");
		if(certificado == null) {
			
			
		}else {
			String consecutivoRunt = certificado.getConsecutivoRunt() != null ? certificado.getConsecutivoRunt():  "";
			informe.setNumeroConsecutivoRunt(consecutivoRunt);
			String numeroSustrato = certificado.getConsecutivoSustrato() != null ? certificado.getConsecutivoSustrato(): "";
			informe.setNumeroDeControl(numeroSustrato);
 		}
	}

}
