package com.proambiente.webapp.util.resultadototal;

import java.text.SimpleDateFormat;

import com.proambiente.modelo.Revision;
import com.proambiente.webapp.util.dto.InformeResultadoTotalDTO;

public class ColocadorInformacionRevisin {

	public void colocarInformacionRevision(InformeResultadoTotalDTO informe,Revision revision) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		informe.setFechaPrueba( sdf.format( revision.getFechaCreacionRevision() ) );
		informe.setNumeroFormato( String.valueOf( revision.getRevisionId() ) );
		
	}
}
