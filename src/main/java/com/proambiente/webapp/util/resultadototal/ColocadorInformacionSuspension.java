package com.proambiente.webapp.util.resultadototal;

import com.proambiente.modelo.dto.ResultadoFasDTO;
import com.proambiente.webapp.util.dto.InformeResultadoTotalDTO;

public class ColocadorInformacionSuspension {
	
	public void colocarInformacionSuspension( ResultadoFasDTO resultadoFas, InformeResultadoTotalDTO informe) {
		
		informe.setDelanteraDerecha( resultadoFas.getSuspDerEje1());
		informe.setDelanteraIzquierda( resultadoFas.getSuspIzqEje1() );
		informe.setTraseraDerecha( resultadoFas.getSuspIzqEje1() );
		informe.setTraseraIzquierda( resultadoFas.getSuspIzqEje2() );
		
	}

}
