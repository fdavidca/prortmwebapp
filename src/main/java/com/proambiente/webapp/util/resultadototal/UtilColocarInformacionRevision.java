package com.proambiente.webapp.util.resultadototal;

import java.text.SimpleDateFormat;

import com.google.common.base.Preconditions;
import com.proambiente.modelo.Revision;
import com.proambiente.webapp.util.dto.InformeResultadoTotalDTO;

public class UtilColocarInformacionRevision {
	
	public void colocarInformacionRevision(Revision revision,InformeResultadoTotalDTO informe) {
		Preconditions.checkNotNull(revision,"Referencia a revision null");
		Preconditions.checkNotNull(informe, "Referencia a informe null");
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		String strResultadoRevision = revision.getAprobada() ? "Aprobado" : "Rechazado";
		informe.setResultadoDeTodaLaPrueba(strResultadoRevision);
		informe.setNumeroFormato( String.valueOf(revision.getRevisionId()) );
        informe.setFechaPrueba( sdf.format(revision.getFechaCreacionRevision() ) );//fecha de revision
		
	}

}
