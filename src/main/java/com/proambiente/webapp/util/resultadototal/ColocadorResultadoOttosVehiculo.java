package com.proambiente.webapp.util.resultadototal;

import com.proambiente.modelo.dto.ResultadoOttoVehiculosDTO;
import com.proambiente.webapp.util.dto.InformeResultadoTotalDTO;

public class ColocadorResultadoOttosVehiculo {
	
	public void colocarResultadoOtto(ResultadoOttoVehiculosDTO resultado,InformeResultadoTotalDTO informe) {
		
		informe.setHcRalenti( resultado.getHcRalenti() );
		informe.setCoRalenti( resultado.getCoRalenti() );
		informe.setCo2Ralenti( resultado.getCo2Ralenti() );
		informe.setO2Ralenti( resultado.getO2Ralenti() );
		
		informe.setHcCrucero( resultado.getHcCrucero() );
		informe.setCoCrucero( resultado.getCoCrucero() );
		informe.setCo2Crucero( resultado.getCo2Crucero() );
		informe.setO2Crucero( resultado.getO2Crucero() );
		
		informe.setRpmCrucero( resultado.getRpmCrucero() );
		informe.setRpmRalenti( resultado.getRpmRalenti() );
		
		informe.setTempRalenti( resultado.getTempRalenti() );
		informe.setTempCrucero( resultado.getTempCrucero() );
		
		
	}
}
