package com.proambiente.webapp.util.resultadototal;

import com.proambiente.modelo.dto.ResultadoFasDTO;
import com.proambiente.webapp.util.dto.InformeResultadoTotalDTO;

public class ColocadorResultadoFrenos {

	public void colocarResultadoFrenos(ResultadoFasDTO resultado,InformeResultadoTotalDTO informe ) {
		
		informe.setPesoEje1Derecho( resultado.getPesoDerEje1() );
		informe.setPesoEje2Derecho( resultado.getPesoDerEje2() );
		informe.setPesoEje3Derecho( resultado.getPesoDerEje3() );
		informe.setPesoEje4Derecho( resultado.getPesoDerEje4() );
		informe.setPesoEje5Derecho( resultado.getPesoDerEje5() );
		
		informe.setPesoEje1Izquierdo( resultado.getPesoIzqEje1() );
		informe.setPesoEje2Izquierdo( resultado.getPesoIzqEje2() );
		informe.setPesoEje3Izquierdo( resultado.getPesoIzqEje3() );
		informe.setPesoEje4Izquierdo( resultado.getPesoIzqEje4() );
		informe.setPesoEje5Izquierdo( resultado.getPesoIzqEje5() );
		
		informe.setFuerzaEje1Derecho( resultado.getFuerzaFrenadoDerEje1() );
		informe.setFuerzaEje2Derecho( resultado.getFuerzaFrenadoDerEje2() );
		informe.setFuerzaEje3Derecho( resultado.getFuerzaFrenadoDerEje3() );
		informe.setFuerzaEje4Derecho( resultado.getFuerzaFrenadoDerEje4() );
		informe.setFuerzaEje5Derecho( resultado.getFuerzaFrenadoDerEje5() );
		
		informe.setFuerzaEje1Izquierdo( resultado.getFuerzaFrenadoIzqEje1() );
		informe.setFuerzaEje2Izquierdo( resultado.getFuerzaFrenadoIzqEje2() );
		informe.setFuerzaEje3Izquierdo( resultado.getFuerzaFrenadoIzqEje3() );
		informe.setFuerzaEje4Izquierdo( resultado.getFuerzaFrenadoIzqEje4() );
		informe.setFuerzaEje5Izquierdo( resultado.getFuerzaFrenadoIzqEje5() );
		
		
		
		
		informe.setDesequilibrioEje1( resultado.getDesequilibrioEje1() );
		informe.setDesequilibrioEje2( resultado.getDesequilibrioEje2() );
		informe.setDesequilibrioEje3( resultado.getDesequilibrioEje3() );
		informe.setDesequilibrioEje4( resultado.getDesequilibrioEje4() );
		informe.setDesequilibrioEje5( resultado.getDesequilibrioEje5() );
		
		informe.setEficaciaAuxiliar( resultado.getEficaciaAuxiliar() );
		informe.setEficaciaTotal( resultado.getEficaciaTotal() );
		
		
	}
}
