package com.proambiente.webapp.util;


import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 *
 * @author Usuario
 */
@Named
@RequestScoped
public class UtilErrores implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * Creates a new instance of UtilErrores
     */
    public UtilErrores() {
    }
    
    protected FacesContext getCurrentContext() {
		return FacesContext.getCurrentInstance();
	}
	
	public void addInfo(String msg) {
		addMessage(msg, FacesMessage.SEVERITY_INFO);
	}
	
	public void addError(String msg) {
		addMessage(msg, FacesMessage.SEVERITY_ERROR);
	}
	
	private void addMessage(String msg,Severity severity) {
		FacesMessage message=new FacesMessage(msg);
		message.setSeverity(severity);
		FacesContext ctx=getCurrentContext();
		ctx.addMessage(null, message);
	}
	
	
}
