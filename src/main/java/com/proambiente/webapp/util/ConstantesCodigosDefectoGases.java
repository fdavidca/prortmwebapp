package com.proambiente.webapp.util;

public class ConstantesCodigosDefectoGases {

	public static final int CODIGO_AUSENCIA_TAPA_ACEITE_DIESEL = 84031;
	public static final int CODIGO_AUSENCIA_TAPA_COMBUSTIBLE_DIESEL = 84032;
	public static final int CODIGO_FUGAS_SILENCIADOR_DIESEL = 84035;
	public static final int CODIGO_FUGAS_TUBO_ESCAPE_DIESEL = 84027;
	public static final int CODIGO_SALIDAS_ADICIONALES_DIESEL = 84030;
	public static final int CODIGO_HUMO_NEGRO_AZUL = 84004;
	public static final int CODIGO_REVOLUCIONES_FUERA_RANGO = 84045;
	public static final int REVOLUCIONES_INESTABLES_MOTOS = 80046;
	public static final int CODIGO_INESTABILIDAD_RPM_CICLOS = 84019;
	
	//OTTO
	public static final int CODIGO_AUSENCIA_TAPA_COMBUSTIBLE_OTTO = 84000;
	public static final int CODIGO_AUSENCIA_TAPA_ACEITE_OTTO = 84001;
	public static final int CODIGO_FUGAS_TUBO_SILENCIADOR = 84002;
	public static final int CODIGO_SALIDAS_ADICIONALES_OTTO = 84003;
	public static final int CODIGO_FALLA_SISTEMA_AD_AIRE_OTTO = 84005;
	public static final int CODIGO_ACCESORIOS_IMPIDEN_OTTO = 84008;
	
	
	public static final int CODIGO_FUGAS_TUBO_ESCAPE_MOTO = 85001;
	public static final int CODIGO_SALIDAS_ADICIONALES_MOTO = 85002;
	public static final int CODIGO_AUSENCIA_TAPA_ACEITE_MOTO = 85003;
	
	
	public static final int CODIGO_ACCESORIOS_DEFORMACIONES_IMPIDEN =  84011;
	public static final int CODIGO_INCORRECTA_OP_REFRIGERACION_OTTO = 84012;
	public static final int CODIGO_AUSENCIA_FILTRO_AIRE_DIESEL = 84009;
	public static final int CODIGO_DESCONEXION_PCV = 84010;
	public static final int CODIGO_FALLA_SISTEMA_REFRIGERACION_DIESEL = 84021;
	
	//defecto niveles de emision
	public static final int CODIGO_INCUMPLIMIENTO_NIVELES_EMISION = 80000;
	public static final int CODIGO_INCUMPLIMIENTO_EMISIONES_MOTO = 84018;
	
	//diesel
	public static final int CODIGO_OP_INADECUADA_DISPOSITIVOS_IMPIDEN_ACELERAR_DIESEL = 84023;
	
	public static final int CODIGO_FALLA_SUBITA_VEHICULO = 84029;
	
	public static final int CODIGO_DIFERENCIA_TEMPERATURA_MOTOR = 84037;
	public static final int CODIGO_DIFERENCIA_ARITMETICA_CICLOS = 84028;
	public static final int CODIGO_MAL_FUNCIONAMIENTO_CONTROL_VELOCIDAD = 84040;
	public static final int CODIGO_SISTEMA_INYECCION_NO_LIMITA = 84042;
	public static final int CODIGO_INDICIO_DANIO_MOTOR = 84043;
	public static final int CODIGO_INDICIO_CONDICION_INSEGURA = 84044;
	public static final int CODIGO_NO_ALCANZA_GOBERNADA = 84024;
	//DOS NUEVOS

	public static final int CODIGO_NO_SATISFACIERON_CRITERIOS = 84047;
	//normativa especial bogota
	public static final int CODIGO_INCUMPLIMIENTO_DECRETO_1552 = 84050;
	public static final int CODIGO_FALLA_SISTEMA_SILENCIADOR = 84051;
	public static final int CODIGO_PRESENCIA_GENERADORES_RUIDO = 84052;
	public static final int PRESENCIA_HUMO_NEGRO_AZUL_MOTOS = 80048;
	
}
;