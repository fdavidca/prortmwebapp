package com.proambiente.webapp.util;

public class ConstantesMedidasGases {
	
	public static final int CODIGO_MEDIDA_TEMPERATURA_AMBIENTE = 8031;
	public static final int CODIGO_MEDIDA_HUMEDAD_RELATIVA = 8032;
	
	public static final int CODIGO_MEDIDA_HC_RALENTI = 8001;
	public static final int CODIGO_MEDIDA_CO_RALENTI = 8002;
	public static final int CODIGO_MEDIDA_CO2_RALENTI = 8003;
	public static final int CODIGO_MEDIDA_O2_RALENTI = 8004;
	public static final int CODIGO_MEDIDA_RPM_RALENTI = 8005;
	public static final int CODIGO_MEDIDA_TEMP_RALENTI = 8006;
	
	public static final int CODIGO_MEDIDA_HC_CRUCERO = 8007;
	public static final int CODIGO_MEDIDA_CO_CRUCERO = 8008;
	public static final int CODIGO_MEDIDA_CO2_CRUCERO = 8009;
	public static final int CODIGO_MEDIDA_O2_CRUCERO = 8010;
	public static final int CODIGO_MEDIDA_RPM_CRUCERO = 8011;
	public static final int CODIGO_MEDIDA_TEMP_CRUCERO = 8012;
	
	public static final int CODIGO_MEDIDA_HC_RALENTI_2T = 8018;
	public static final int CODIGO_MEDIDA_CO_RALENTI_2T = 8020;
	public static final int CODIGO_MEDIDA_CO2_RALENTI_2T = 8019;
	public static final int CODIGO_MEDIDA_O2_RALENTI_2T = 8021;
	public static final int CODIGO_MEDIDA_RPM_RALENTI_2T = 8028;
	public static final int CODIGO_MEDIDA_TEMP_RALENTI_2T = 8022;
	
	public static final int CODIGO_TEMPERATURA_MOTOR_DIESEL = 8034;
	public static final int CODIGO_VELOCIDAD_GOBERNADA = 8036;
	public static final int CODIGO_VELOCIDAD_GOBERNADA_PROMEDIO = 8051;
	public static final int CODIGO_VELOCIDAD_RALENTI_DIESEL = 8035;
	public static final int CODIGO_VELOCIDAD_RALENTI_PROMEDIO = 8050;
	
	
	public static final int CODIGO_REV_RAL_CICLO_PRELIMINAR = 8037;
	public static final int CODIGO_REV_RAL_PRIMER_CICLO = 8038;
	public static final int CODIGO_REV_RAL_SEGUNDO_CICLO = 8039;
	public static final int CODIGO_REV_RAL_TERCER_CICLO = 8040;
	
	public static final int CODIGO_REV_GOB_CICLO_PRELIMINAR = 8041;
	public static final int CODIGO_REV_GOB_PRIMER_CICLO = 8042;
	public static final int CODIGO_REV_GOB_SEGUNDO_CICLO = 8043;
	public static final int CODIGO_REV_GOB_TERCER_CICLO = 8044;
	
	public static final int CODIGO_OPACIDAD_CICLO_PRELIMINAR = 8033;
	public static final int CODIGO_OPACIDAD_PRIMER_CICLO = 8013;
	public static final int CODIGO_OPACIDAD_SEGUNDO_CICLO = 8014;
	public static final int CODIGO_OPACIDAD_TERCER_CICLO = 8015;
	
	public static final int CODIGO_RESULTADO_OPACIDAD = 8017;
							
	public static final int CODIGO_DIAMETRO_TUBO_ESCAPE = 8052;
	public static final int CODIGO_MODIFICACIONES_MOTOR = 8066;
	
	public static final int CODIGO_CONOCE_RPM_FABRICANTE = 8060;
	public static final int CODIGO_RPM_RAL_MIN_FABRICANTE = 8061;
	public static final int CODIGO_RPM_RAL_MAX_FABRICANTE = 8062;
	public static final int CODIGO_NUM_TUBO_ESCAPES = 8063;
	public static final int CODIGO_CATALIZADOR = 8064;
	public static final int CODIGO_CORRECCION_O2 = 8065;
	public static final int CODIGO_SCOOTER = 8075;
	
	public static final int CODIGO_RPM_MIN_RAL_FABRICANTE = 8067;
	public static final int CODIGO_RPM_MAX_RAL_FABRICANTE = 8068;
	public static final int CODIGO_RPM_MIN_GOB_FABRICANTE = 8069;
	public static final int CODIGO_RPM_MAX_GOB_FABRICANTE = 8070;
	
	
	
	public static final int CODIGO_DIESEL_TEMPERATURA_INICIAL = 8034;
	public static final int CODIGO_DIESEL_TEMPERATURA_FINAL = 8048;
	
	public static final int CODIGO_DENSIDAD_HUMO_CICLO_PRELIMINAR = 8071 ;
	public static final int CODIGO_DENSIDAD_HUMO_CICLO_UNO = 8072;
	public static final int CODIGO_DENSIDAD_HUMO_CICLO_DOS = 8073;
	public static final int CODIGO_DENSIDAD_HUMO_CICLO_TRES = 8074;
	public static final int CODIGO_RESULTADO_DENSIDAD_HUMO = 8077;
	public static final int CODIGO_EVALUACION_DENSIDAD_HUMO = 8078;
	
	public static final int CODIGO_MODELO_REPOTENCIACION = 8085;
	public static final Integer KILOMETRAJE = null;
	
	

}
