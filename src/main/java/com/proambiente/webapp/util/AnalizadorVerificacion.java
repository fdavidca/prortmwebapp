package com.proambiente.webapp.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.apache.commons.math3.stat.regression.SimpleRegression;

import com.proambiente.modelo.TipoVerificacion;
import com.proambiente.modelo.VerificacionGasolina;
import com.proambiente.modelo.dto.MedicionGasesDTO;

/**
 *
 * @author fdavi
 */
public class AnalizadorVerificacion {
    
    public SummaryStatistics obtenerEstadisticasHC(List<MedicionGasesDTO> resultadoVerificacion){
        SummaryStatistics sumary = new SummaryStatistics();
        resultadoVerificacion.stream().forEach(md->{
                                                    sumary.addValue(md.getHC());
                                                    });
        return sumary;
    }

    public SummaryStatistics obtenerEstadisticasCO(List<MedicionGasesDTO> resultadoVerificacion) {
        SummaryStatistics sS = new SummaryStatistics();
        resultadoVerificacion.stream().forEach(md->{
            sS.addValue(md.getCO().setScale(2, RoundingMode.UP).doubleValue());
            });
        return sS;
    }
    
    public SummaryStatistics obtenerEstadisticasCO2(List<MedicionGasesDTO> resultadoVerificacion){
        SummaryStatistics s = new SummaryStatistics();
        resultadoVerificacion.stream().forEach( md->{
                                                        Double valor  = md.getCO2().setScale(2, RoundingMode.HALF_EVEN).doubleValue();
                                                        s.addValue(valor);
                                                    });
        return s;       
    }

    public Double calcularError(Double valorEsperado, Double valorMedido) {
        return valorEsperado - valorMedido;
    }
    
    public SimpleRegression calcularRectaHC(List<MedicionGasesDTO> listaMedicionesBaja,List<MedicionGasesDTO> listaMedicionesAlta,
                                    Double valorPipetaHCBaja,Double valorPipetaHCAlta,Double pef){
        
        SimpleRegression simpleRegresion = new SimpleRegression();
        
        double mediaHCBaja = obtenerEstadisticasHC(listaMedicionesBaja).getMean();
        
        double mediaHCAlta = obtenerEstadisticasHC(listaMedicionesAlta).getMean(); 
        
        BigDecimal bdMediaHCBaja = new BigDecimal(mediaHCBaja*pef).setScale(0,RoundingMode.HALF_EVEN);
        
        BigDecimal bdMediaHCAlta = new BigDecimal(mediaHCAlta*pef).setScale(0,RoundingMode.HALF_EVEN);
        
        
        BigDecimal bdPipetaBaja = new BigDecimal(valorPipetaHCBaja*pef).setScale(0, RoundingMode.HALF_EVEN);
        
        BigDecimal bdPipetaAlta = new BigDecimal(valorPipetaHCAlta*pef).setScale(0, RoundingMode.HALF_EVEN);
       
        simpleRegresion.addData( bdPipetaBaja.doubleValue() , bdMediaHCBaja.doubleValue() );       
        
        simpleRegresion.addData( bdPipetaAlta.doubleValue() , bdMediaHCAlta.doubleValue() );
    
        return simpleRegresion;
    }
    
    public SimpleRegression calcularRectaCO(List<MedicionGasesDTO> listaMedicionesBaja,List<MedicionGasesDTO> listaMedicionesAlta,
                                    Double valorPipetaCOBaja,Double valorPipetaCOAlta){
        
        SimpleRegression simpleRegresion = new SimpleRegression();      
        Double valorCOBajaPromedio  = obtenerEstadisticasCO(listaMedicionesBaja).getMean();        
        Double valorCOAltaPromedio =  obtenerEstadisticasCO(listaMedicionesAlta).getMean();
        
        BigDecimal bdValorCOBaja = new BigDecimal(valorCOBajaPromedio).setScale(2,RoundingMode.HALF_EVEN);
        Double valorCOBaja = bdValorCOBaja.doubleValue();
        
        BigDecimal bdValorCOAlta = new BigDecimal(valorCOAltaPromedio).setScale(2,RoundingMode.HALF_EVEN);
        Double valorCOAlta = bdValorCOAlta.doubleValue();
        
        simpleRegresion.addData(valorPipetaCOBaja, valorCOBaja );       
        simpleRegresion.addData( valorPipetaCOAlta, valorCOAlta );
       
        return simpleRegresion;
    }
    
    public SimpleRegression calcularRectaCO2(List<MedicionGasesDTO> listaMedicionesBaja,List<MedicionGasesDTO> listaMedicionesAlta,
                                    Double valorPipetaCO2Baja,Double valorPipetaCO2Alta){
        
        SimpleRegression simpleRegresion = new SimpleRegression();
        Double valorCO2BajaPromedio  = obtenerEstadisticasCO2(listaMedicionesBaja).getMean();        
        Double valorCO2AltaPromedio =  obtenerEstadisticasCO2(listaMedicionesAlta).getMean();
        
        BigDecimal bdBaja = new BigDecimal(valorCO2BajaPromedio).setScale(1, RoundingMode.HALF_EVEN);
        BigDecimal bdAlta = new BigDecimal(valorCO2AltaPromedio).setScale(1,RoundingMode.HALF_EVEN);
        
        Double valorCO2Baja = bdBaja.doubleValue();
        Double valorCO2Alta = bdAlta.doubleValue();
        
        simpleRegresion.addData( valorPipetaCO2Baja, valorCO2Baja );       
        simpleRegresion.addData( valorPipetaCO2Alta, valorCO2Alta );
        
        
        return simpleRegresion;
    }
    
    
    public Double[][] tablaRangosHCVehiculo = {{0.0, 150.0, 12.0},
    {401.0, 600.0, 30.0}
    };

    public Double[][] tablaRangosCOVehiculo = {{0.0, 1.0, 0.06},
    {1.01, 4.0, 0.15},
    };

    public Double[][] tablaRangosCO2Vehiculo = {{0.0, 6.0, 0.60},
    {4.1, 12.0, 0.60}
    };

    public Double[][] tablaRangosHCMotocicletas4T = {{0.0, 150.0, 50.0},
    {151.0, 600.0, 50.0}
   };

    public Double[][] tablaRangosCOMotocicletas4T = {
    	{0.0, 1.0, 0.05},   
        {1.01, 2.0, 0.10},
        {2.01,4.0 , 0.20},
        {4.01,10.0 , 0.50}        
    };

    public Double[][] tablaRangosCO2Motocicletas4T = {/**{0.0, 2.0, 0.19},**/
    {3.75, 4.0, 0.2},    		
    {4.1, 6.0, 0.4},
    {8.1, 12.0, 0.8}
    };

    public Double[][] tablaRangosHCMotocicletas2T = {{0.0, 150.0, 100.0},
    {1511.0, 1600.0, 100.0}
    };

    public Double[][] tablaRangosCOMotocicletas2T = {{0.0, 1.0, 0.05},    
    {4.01, 8.0, 0.50}};

    public Double[][] tablaRangosCO2Motocicletas2T ={  /**{0.0, 2.0, 0.1},**/
    /**{2.1, 4.0, 0.2},**/ {4.1, 6.0, 0.4}, {8.1, 12.0, 0.8}};

    public Double obtenerValorRangoHCVehiculo(Double valorGas) {
        BigDecimal bd = new BigDecimal(valorGas);
        bd = bd.setScale(0, RoundingMode.HALF_EVEN);
        valorGas = bd.doubleValue();
        for (Double[] rango : tablaRangosHCVehiculo) {
            //primera columna es el limite bajo, segunda columna es el limite alto, tercera columna es el valor que se retorna
            if (valorGas >= rango[0] && valorGas <= rango[1]) {
                Double limiteRango = rango[2];
                return limiteRango;
            }
        }
        return -1.0;
    }

    public Double obtenerValorRangoHCMotocicleta4Tiempos(Double valorGas) {
        BigDecimal bd = new BigDecimal(valorGas);
        bd = bd.setScale(0, RoundingMode.HALF_EVEN);
        valorGas = bd.doubleValue();
        for (Double[] rango : tablaRangosHCMotocicletas4T) {
            //primera columna es el limite bajo, segunda columna es el limite alto, tercera columna es el valor que se retorna
            if (valorGas >= rango[0] && valorGas <= rango[1]) {
                Double limiteRango = rango[2];
                return limiteRango;
            }
        }
        return -1.0;
    }

    public Double obtenerValorRangoHCMotocicleta2Tiempos(Double valorGas) {
        BigDecimal bd = new BigDecimal(valorGas);
        bd = bd.setScale(0, RoundingMode.HALF_EVEN);
        valorGas = bd.doubleValue();
        for (Double[] rango : tablaRangosHCMotocicletas4T) {
            //primera columna es el limite bajo, segunda columna es el limite alto, tercera columna es el valor que se retorna
            if (valorGas >= rango[0] && valorGas <= rango[1]) {
                Double limiteRango = rango[2];
                return limiteRango;
            }
        }
        return -1.0;
    }

    public Double obtenerValorRangoCOVehiculo(Double valorGas) {
        BigDecimal bd = new BigDecimal(valorGas);
        bd = bd.setScale(0, RoundingMode.HALF_EVEN);
        valorGas = bd.doubleValue();
        for (Double[] rango : tablaRangosCOVehiculo) {
            //primera columna es el limite bajo, segunda columna es el limite alto, tercera columna es el valor que se retorna
            if (valorGas >= rango[0] && valorGas <= rango[1]) {
                Double limiteRango = rango[2];
                return limiteRango;
            }
        }
        return -1.0;
    }

    public Double obtenerValorRangoCOMotocicleta4Tiempos(Double valorGas) {
        BigDecimal bd = new BigDecimal(valorGas);
        bd = bd.setScale(0, RoundingMode.HALF_EVEN);
        valorGas = bd.doubleValue();
        for (Double[] rango : tablaRangosCOMotocicletas4T) {
            //primera columna es el limite bajo, segunda columna es el limite alto, tercera columna es el valor que se retorna
            if (valorGas >= rango[0] && valorGas <= rango[1]) {
                Double limiteRango = rango[2];
                return limiteRango;
            }
        }
        return -1.0;
    }

    public Double obtenerValorRangoCOMotocicleta2Tiempos(Double valorGas) {
        BigDecimal bd = new BigDecimal(valorGas);
        bd = bd.setScale(0, RoundingMode.HALF_EVEN);
        valorGas = bd.doubleValue();
        for (Double[] rango : tablaRangosCO2Motocicletas2T) {
            //primera columna es el limite bajo, segunda columna es el limite alto, tercera columna es el valor que se retorna
            if (valorGas >= rango[0] && valorGas <= rango[1]) {
                Double limiteRango = rango[2];
                return limiteRango;
            }
        }
        return -1.0;
    }

    public Double obtenerValorRangoCO2Vehiculo(Double valorGas) {
        BigDecimal bd = new BigDecimal(valorGas);
        bd = bd.setScale(0, RoundingMode.HALF_EVEN);
        valorGas = bd.doubleValue();
        for (Double[] rango : tablaRangosCO2Vehiculo) {
            //primera columna es el limite bajo, segunda columna es el limite alto, tercera columna es el valor que se retorna
            if (valorGas >= rango[0] && valorGas <= rango[1]) {
                Double limiteRango = rango[2];
                return limiteRango;
            }
        }
        return -1.0;
    }

    public Double obtenerValorRangoCO2Motocicleta4Tiempos(Double valorGas) {
        BigDecimal bd = new BigDecimal(valorGas);
        bd = bd.setScale(0, RoundingMode.HALF_EVEN);
        valorGas = bd.doubleValue();
        for (Double[] rango : tablaRangosCO2Motocicletas4T) {
            //primera columna es el limite bajo, segunda columna es el limite alto, tercera columna es el valor que se retorna
            if (valorGas >= rango[0] && valorGas <= rango[1]) {
                Double limiteRango = rango[2];
                return limiteRango;
            }
        }
        return -1.0;
    }

    public Double obtenerValorRantoCO2Motocicleta2Tiempos(Double valorGas) {
        BigDecimal bd = new BigDecimal(valorGas);
        bd = bd.setScale(0, RoundingMode.HALF_EVEN);
        valorGas = bd.doubleValue();
        for (Double[] rango : tablaRangosCO2Motocicletas2T) {
            //primera columna es el limite bajo, segunda columna es el limite alto, tercera columna es el valor que se retorna
            if (valorGas >= rango[0] && valorGas <= rango[1]) {
                Double limiteRango = rango[2];
                return limiteRango;
            }
        }
        return -1.0;
    }
    
    public boolean validarTablaHC(double pendiente, double intercepto,TipoVerificacion tipoVerificacion,
    								StringBuilder sb,Double porcentaje){
        Double pendienteDouble = pendiente;
        if(pendienteDouble.equals(Double.NaN) || pendiente == Double.POSITIVE_INFINITY || pendiente == Double.NEGATIVE_INFINITY
                || pendiente == Double.MAX_VALUE || pendiente == Double.MIN_VALUE ){
            throw new RuntimeException("Error pendiente HC invalida");
        }
        
        
        
        Double[][] tabla ;
        if(tipoVerificacion.equals(TipoVerificacion.VEHICULO_OTTO)){
           tabla = tablaRangosHCVehiculo;
        }else if(tipoVerificacion.equals(TipoVerificacion.MOTOCICLETA_4T)){
            tabla = tablaRangosHCMotocicletas4T;
        }else {
        	tabla = tablaRangosHCMotocicletas2T;
        }
        boolean validoGlobal = true;
        for(Double[] rangos: tabla){
            double x = rangos[1];//x representa la variable independiente
            double valorCalculado = x*pendiente + intercepto;
            valorCalculado = new BigDecimal(valorCalculado).setScale(0, RoundingMode.HALF_EVEN).doubleValue();
            double diferencia = Math.abs( valorCalculado - x);
            diferencia = new BigDecimal(diferencia).setScale(0,RoundingMode.HALF_EVEN).doubleValue();
            Double diferenciaPermitida = new BigDecimal(rangos[2]*porcentaje).setScale(0, RoundingMode.HALF_EVEN).doubleValue();
            Boolean valido = Math.abs(diferencia) <= diferenciaPermitida ;
            validoGlobal = valido&validoGlobal;
            if(!valido) {
                String formato = "Rango HC hexano NO VÁLIDO, verificando punto : %.0f,valor calculado : %.0f,"
                                + "\n máxima diferencia : %.0f, diferencia encontrada: %.0f\n";
                sb.append(String.format(formato, x,valorCalculado,diferenciaPermitida,diferencia));
               
            }else {
                String formato = "Rango HC hexano válido, verificando punto : %.0f, valor calculado : %.0f,"
                                + "\n máxima diferencia : %.0f, diferencia encontrada: %.0f\n";
                sb.append(String.format(formato, x,valorCalculado,diferenciaPermitida,diferencia));
            }
        }
        return validoGlobal;
    }
    
    public boolean validarTablaCO(double pendiente,double intercepto,TipoVerificacion tipoVerificacion,StringBuilder sb,Double porcentaje){
        Double[][] tabla ;
        if( tipoVerificacion.equals(TipoVerificacion.VEHICULO_OTTO) ){
           tabla = tablaRangosCOVehiculo;
        }else if(tipoVerificacion.equals(TipoVerificacion.MOTOCICLETA_4T)){
            tabla = tablaRangosCOMotocicletas4T;
        }else {
        	tabla = tablaRangosCOMotocicletas2T;
        }
        boolean validoGlobal = true;
        for(Double[] rangos: tabla){
            double x = rangos[1];//valor de la variable independiente
            double valorCalculado = x*pendiente + intercepto;
            valorCalculado = new BigDecimal(valorCalculado).setScale(2, RoundingMode.HALF_EVEN).doubleValue();            
            double diferencia = Math.abs( valorCalculado - x);
            diferencia = new BigDecimal(diferencia).setScale(2,RoundingMode.HALF_EVEN).doubleValue();
            //se aplica un porcentaje a el rango[2] que corresponde a la diferencia permitida
            Double diferenciaPermitida = new BigDecimal(rangos[2]*porcentaje).setScale(2,RoundingMode.HALF_EVEN).doubleValue();
            Boolean valido = Math.abs(diferencia) <= diferenciaPermitida ;
            validoGlobal = valido && validoGlobal;//en caso de false validoGlobal sera false aun cuando validoCiclo sea true
            if(!valido) {
                String formato = "Rango CO NO VÁLIDO,verificando punto : %.2f , el valor calculado : %.2f,"
                                + "\n máxima diferencia : %.2f, diferencia encontrada: %.2f\n";
                sb.append(String.format(formato, x,valorCalculado,diferenciaPermitida,diferencia));
                //break;
            }else {
                String formato = "Rango CO válido, verificando punto : %.2f, el valor calculado : %.2f,"
                                + "\n máxima diferencia : %.2f, diferencia encontrada: %.2f\n";
                sb.append(String.format(formato, x,valorCalculado,diferenciaPermitida,diferencia));
            }
        }
        return validoGlobal;
    }
    
    public boolean validarTablaCO2(double pendiente,double intercepto, TipoVerificacion tipoVerificacion,StringBuilder sb,Double porcentaje){
        Double[][] tabla ;
        if( tipoVerificacion.equals(TipoVerificacion.VEHICULO_OTTO) ){
           tabla = tablaRangosCO2Vehiculo;
           
        }else if(tipoVerificacion.equals(TipoVerificacion.MOTOCICLETA_4T)){
        	
            tabla = tablaRangosCO2Motocicletas4T;
        }else {
        	
        	tabla = tablaRangosCO2Motocicletas2T;
        }
        boolean validoGlobal = true;
        for(Double[] rangos: tabla){
            double x = rangos[1];//variable independiente
            double valorCalculado = x*pendiente + intercepto;
            valorCalculado = new BigDecimal(valorCalculado).setScale(1,RoundingMode.HALF_EVEN).doubleValue();
            double diferencia = new BigDecimal(Math.abs( valorCalculado - x)).setScale(1, RoundingMode.HALF_EVEN).doubleValue();
            double diferenciaPermitida = new BigDecimal(rangos[2]*porcentaje).setScale(1, RoundingMode.HALF_EVEN).doubleValue();
            
            boolean valido = Math.abs(diferencia) <= diferenciaPermitida ;
            validoGlobal = valido&validoGlobal;
            diferenciaPermitida = new BigDecimal(rangos[2]).multiply(new BigDecimal(porcentaje)).setScale(1, RoundingMode.HALF_EVEN).doubleValue();
            if(!valido) {
                String formato = "Rango CO2 NO VÁLIDO, verificando punto : %.1f el valor calculado : %.1f,"
                                + "\n máxima diferencia : %.1f, diferencia encontrada: %.1f\n";
                sb.append(String.format(formato, x,valorCalculado,diferenciaPermitida,diferencia));
                
            }else {
                String formato = "Rango CO2 válido, verificando punto : %.1f  el valor calculado : %.1f, "
                                + "\n máxima diferencia : %.2f, diferencia encontrada: %.2f\n";
                sb.append(String.format(formato, x,valorCalculado,diferenciaPermitida,diferencia));
            }
        }
        return validoGlobal;
    }
    
    
    public Boolean validarConformidadVehiculo(  SimpleRegression regresionHC,
    											SimpleRegression regresionCO,
    											SimpleRegression regresionCO2,    											
    											VerificacionGasolina vg,
    											Double pef,
    											StringBuilder sbGlobal,
    											TipoVerificacion tipoVerificacion) {
    	
    	
        Double pendienteHC = regresionHC.getSlope();
        Double interceptoHC = regresionHC.getIntercept();
        
        BigDecimal bdPendienteHC = new BigDecimal(pendienteHC);
        bdPendienteHC = bdPendienteHC.setScale(3, RoundingMode.HALF_EVEN);
        pendienteHC = bdPendienteHC.doubleValue();
        
        
        interceptoHC = new BigDecimal(interceptoHC).setScale(3,RoundingMode.HALF_EVEN).doubleValue(); 
           
        System.out.println( String.format("Pendiente HC: %.3f, intercepto %.3f", pendienteHC,interceptoHC) );        
        
        StringBuilder sb2 = new StringBuilder();
        
        
        Double pendienteCO = regresionCO.getSlope();
        pendienteCO = new BigDecimal(pendienteCO).setScale(3,RoundingMode.HALF_EVEN).doubleValue();
        
        
        Double interceptoCO = regresionCO.getIntercept();
        interceptoCO = new BigDecimal(interceptoCO).setScale(3,RoundingMode.HALF_EVEN).doubleValue();
        
        System.out.println( String.format("Pendiente CO: %.3f, intercepto %.3f", pendienteCO,interceptoCO) );
        
       
        
        Double pendienteCO2 = regresionCO2.getSlope();
        Double interceptoCO2 = regresionCO2.getIntercept();
        
        pendienteCO2 = new BigDecimal(pendienteCO2).setScale(3,RoundingMode.HALF_EVEN).doubleValue();
        interceptoCO2 = new BigDecimal(interceptoCO2).setScale(3,RoundingMode.HALF_EVEN).doubleValue();
        
        
        System.out.println( String.format("Pendiente CO2: %.3f, intercepto %.3f", pendienteCO2,interceptoCO2) );
		
        StringBuilder sbRangosCienPorciento = new StringBuilder();
        
        
        boolean hcValidoCienPorciento = validarTablaHC(pendienteHC, interceptoHC, tipoVerificacion, sbRangosCienPorciento,1.0);//porcentaje expresado en valor 
        boolean coValidoCienPorciento = validarTablaCO(pendienteCO, interceptoCO, tipoVerificacion, sbRangosCienPorciento,1.0);//porcentaje expresado en valor
        boolean co2ValidoCienPorciento = validarTablaCO2(pendienteCO2, interceptoCO2, tipoVerificacion, sbRangosCienPorciento,1.0);//porcentaje expresado en valor

        boolean aprobadaCienPorciento = hcValidoCienPorciento&& coValidoCienPorciento&&co2ValidoCienPorciento;
        if (!hcValidoCienPorciento || !coValidoCienPorciento || !co2ValidoCienPorciento) {
        	sbRangosCienPorciento.append("VERIFICACION VEHICULO NO APROBADA \n ES NECESARIO REALIZAR ALGÚN \n AJUSTE O RUTINA DE MANTENIMIENTO AL EQUIPO\n");
        }
        
        
        sb2.append("\n");
        sb2.append(sbRangosCienPorciento);
        
        
        StringBuilder sbRangos95Porciento = new StringBuilder();
        
        boolean hcValido95Porciento = validarTablaHC(pendienteHC, interceptoHC, tipoVerificacion, sbRangos95Porciento, 0.95);
        boolean coValido95Porciento = validarTablaCO(pendienteCO,interceptoCO,tipoVerificacion,sbRangos95Porciento,0.95);
        boolean co2Valido95Porciento = validarTablaCO2(pendienteCO2,interceptoCO2,tipoVerificacion,sbRangos95Porciento,0.95);
        
        if (!hcValido95Porciento || !coValido95Porciento || !co2Valido95Porciento) {
        	sbRangos95Porciento.append("VERIFICACION 95 % NO APROBADA \n SE RECOMIENDA AJUSTE DE MANTENIMIENTO AL EQUIPO\n");
        }else {
        	sbRangos95Porciento.append("VERIFICACION 95 %  APROBADA \n");
        }
		
        StringBuilder sbSeparador = new StringBuilder("\r\n Comprobacion usando 95 % del valor de la tabla \n\r");
        
        
        
       sbGlobal.append(sbRangosCienPorciento).append(sbSeparador).append( sbRangos95Porciento);
           	
    	
    	return aprobadaCienPorciento;
    }

}