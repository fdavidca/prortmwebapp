package com.proambiente.webapp.util;

public class ConstantesMedidasTaximetro {
	public static final int ID_DEFECTO_ERROR_DISTANCIA = 90001;
	public static final int ID_DEFECTO_ERROR_TIEMPO = 90002;
	public static final int ID_MEDIDA_ERROR_DISTANCIA = 9002;
	public static final int ID_MEDIDA_ERROR_TIEMPO = 9003;
	public static final int ID_MEDIDA_UNIDADES_DISTANCIA = 9004;
	public static final int ID_MEDIDA_UNIDADES_TIEMPO = 9005;
}
