package com.proambiente.webapp.util;

import com.proambiente.modelo.InformacionCda;
import com.proambiente.webapp.util.dto.InformeCaldasDTO;
import com.proambiente.webapp.util.dto.InformeCarInformacionCDADTO;
import com.proambiente.webapp.util.dto.InformeDieselCarRes0762;
import com.proambiente.webapp.util.dto.ResultadoSonometriaInformeDTO;
import com.proambiente.webapp.util.dto.sda.InformeMotocicletaDTO;
import com.proambiente.webapp.util.informes.medellin.InformeLivianosRes0762;

public class LLenarInfoInformeCaldas {

	public static void llenarInfoCda(InformeCaldasDTO informeCaldas, InformacionCda infoCda) {
		
		informeCaldas.nombreRazonSocial = infoCda.getNombreRazonSocialCda();
		informeCaldas.tipoIdentificacionCda = infoCda.getTipoIdentificacionCda();
		informeCaldas.numeroIdentificacionCda = infoCda.getNit();
		informeCaldas.personaDeContacto = "";//infoCda.getPersonaDeContacto();
		informeCaldas.correoElectronicoCda = infoCda.getCorreoElectronico();
		informeCaldas.telefonoCda = infoCda.getTelefono1();
		informeCaldas.departamentoCda = "CALDAS";
		informeCaldas.municipioCda = infoCda.getCiudad();
		informeCaldas.resolucionAutoridadAmbiental = infoCda.getNumeroResolucionSDA();
		informeCaldas.fechaResolucionVigente = infoCda.getFechaResolucionSDA();
		informeCaldas.numeroExpedienteCda = infoCda.getNumeroExpedienteCda();
		informeCaldas.claseCda = infoCda.getClaseCda();
		if(infoCda.isEsPistaMotos()) {
			informeCaldas.totalOpacimetros = "";
			informeCaldas.totalAnalizadoresLivianos = "";
		}else {
			informeCaldas.totalOpacimetros = infoCda.getTotalOpacimetros() == null ? "0":infoCda.getTotalOpacimetros().toString();
			informeCaldas.totalAnalizadoresLivianos = infoCda.getTotalAnalizadoresLivianos() == null ? "0":infoCda.getTotalAnalizadoresLivianos().toString();
		}
		informeCaldas.totalAnalizadores4T= infoCda.getTotalAnalizadores4tMotos() == null ? "0" : infoCda.getTotalAnalizadores4tMotos().toString();
		informeCaldas.totalAnalizadores2T = infoCda.getTotalAnalizadores2tMotos() == null ? "0" : infoCda.getTotalAnalizadores2tMotos().toString();
		informeCaldas.direccionCda = infoCda.getDireccion();
		informeCaldas.municipioDeInspeccion = infoCda.getCiudad();
		informeCaldas.versionSoftware = "'"+infoCda.getVersionSoftware()+"'";
		informeCaldas.nombreSoftware = "PRORTM";
		informeCaldas.nombreProveedorSoftware = "Proambiente s.a.s";
		
		
	}

	public static void llenarInformacionMotos(InformeCaldasDTO informeCaldas, InformeMotocicletaDTO infoM, ResultadoSonometriaInformeDTO resultadoSonometria) {
		
		informeCaldas.norma = " NTC 4983 ";
		
		informeCaldas.fechaInicioPrueba = ((InformeCarInformacionCDADTO)infoM).getFechaInicioPrueba();
		informeCaldas.fechaFinPrueba =((InformeCarInformacionCDADTO)infoM).getFechaFinalPrueba();
		
			if(infoM.getSubInforme() != null) {
		
				informeCaldas.numeroInspeccion = infoM.getSubInforme().getNumeroFUR();			
			
				informeCaldas.numeroCertificado = infoM.getSubInforme().getNumeroConsecutivoRunt();				
				
				informeCaldas.idInspector = infoM.getSubInforme().getNumeroIdentificacionInspectorPrueba();
				
				//informeCaldas.incumplimientoNivelesEmision = infoM.getSubInforme().getIncumpimientoNivelesEmisiones();
				
				informeCaldas.incumplimientoNivelesEmision = "";
			
			}
			
			informeCaldas.valorPef = infoM.getVlrPEf();
			
			informeCaldas.numeroDeSerieDelBanco = infoM.getNumeroSerieAnalizador();
			informeCaldas.marcaAnalizador = infoM.getMarcaAnalizador();
			
			informeCaldas.vlrSpanBajoHC =  infoM.getVlrSpanBajoHC();
		    informeCaldas.resultadoVlrSpanBajoHC =  infoM.getResultadoValorSpanBajoHC();
		    
			informeCaldas.vlrSpanBajoCO =  infoM.getVlrSpanBajoCO();
			informeCaldas.resultadoVlrSpanBajoCO =  infoM.getResultadoValorSpanBajoCO();
			
			informeCaldas.vlrSpanBajoCO2 =  infoM.getVlrSpanBajoCO2();
			informeCaldas.resultadoVlrSpanBajoCO2 =  infoM.getResultadoValorSpanBajoCO2();
			
			informeCaldas.vlrSpanAltoHC =  infoM.getVlrSpanAltoHC();
			informeCaldas.resultadoVlrSpanAltoHC =  infoM.getResultadoValorSpanAltoHC();
			
			informeCaldas.vlrSpanAltoCO =  infoM.getVlrSpanAltoCO();
			informeCaldas.resultadoVlrSpanAltoCO =  infoM.getResultadoValorSpanAltoCO();
			
			informeCaldas.vlrSpanAltoCO2 =  infoM.getVlrSpanAltoCO2();
			informeCaldas.resultadoVlrSpanAltoCO2 =  infoM.getResultadoValorSpanAltoCO2();
			informeCaldas.fechaHoraUltimaCalibracion =  infoM.getFechaHoraUltimaCalibracion();
			
			
			informeCaldas.placa = infoM.getPlaca();
			informeCaldas.modelo = infoM.getModelo();			
			informeCaldas.cilindraje = infoM.getCilindraje();			
			informeCaldas.kilometraje = infoM.getKilometraje();
			informeCaldas.marca = infoM.getMarca();
			informeCaldas.linea = infoM.getLinea();
			informeCaldas.clase = infoM.getClaseVehiculo();
			informeCaldas.servicio = infoM.getServicio();
			informeCaldas.combustible = infoM.getCombustible();
			
			informeCaldas.tipoMotor = infoM.getTiemposMotor();

			informeCaldas.disenioMotor = infoM.getSubInforme().getDisenioMotor();
			informeCaldas.numeroTubosEscape = infoM.getSubInforme().getNumeroTubosEscape();
			
			informeCaldas.temperaturaAmbiente = infoM.getTemperaturaAmbiente();
			informeCaldas.humedadRelativa = infoM.getHumedadRelativa();
			informeCaldas.temperaturaInicialMotor = infoM.getTemperaturaDeMotor();
			
			informeCaldas.rpmRalenti = infoM.getRpmRalenti();
			informeCaldas.hcRalenti = infoM.getHcRalenti();
			informeCaldas.coRalenti = infoM.getCoRalenti();
			informeCaldas.co2Ralenti = infoM.getCo2Ralenti();
			informeCaldas.o2Ralenti = infoM.getO2Ralenti();
			
			informeCaldas.conceptoFinal = infoM.getConceptoFinalDelVehiculo();
			
			informeCaldas.presenciaHumoNegroAzul = infoM.getPresenciaHumoNegroAzul();
			informeCaldas.revolucionesFueraRango = infoM.getRevolucionesFueraRango();
			informeCaldas.ausenciaFugasTapaAceite = infoM.getFugasTapaAceite();
			informeCaldas.ausenciaFugasTapaCombustible = infoM.getFugasTapaCombustible();
			informeCaldas.salidasAdicionales = infoM.getSalidasAdicionalesDiseno();
			informeCaldas.nivelEmisionAplicable = "Resolucion 0762" ;
			informeCaldas.conceptoFinal = infoM.getConceptoFinalDelVehiculo();
			
			//no aplican
			
			informeCaldas.presenciaDilucion = "";
			informeCaldas.ausenciaMalEstadoFiltroAire = "";
			informeCaldas.desconexionRecirculacion= "";
			informeCaldas.instalacionDispositivosImpidenIntroducirSonda = ""; 
			informeCaldas.incorrectaOperacionRefrigeracion = "";
			informeCaldas.fallaSubita = "";
			informeCaldas.incorrectaOperacionGobernador = "";
			informeCaldas.ejecucionIncorrecta = "";
			informeCaldas.diferenciaAritmetica = "";
			informeCaldas.diferenciaTemperatura = "";
			informeCaldas.activacionDispositivos = "";
			
			
			
		
			if(resultadoSonometria != null) {
				informeCaldas.decibeles = resultadoSonometria.getValorMedicion();
			}
		
		
	}

	public static void llenarInfoOtto(InformeCaldasDTO informeCaldas, InformeLivianosRes0762 infoM, ResultadoSonometriaInformeDTO resultadoSonometria) {
		informeCaldas.norma = "NTC 5365";
		
		informeCaldas.numeroInspeccion = infoM.getNumeroFUR();
		
		
		informeCaldas.numeroCertificado = infoM.getNumeroConsecutivoRunt(); 
		
		informeCaldas.fechaInicioPrueba = infoM.getFechaHoraInicioInspeccion();
		informeCaldas.fechaFinPrueba = infoM.getFechaHoraFinInspeccion();
	
	
		//informeCaldas.numeroDeSerieEquipo = infoM.getSubInforme().getSerialAnalizadorGases();
	
	
		//informeCaldas.marcaDelMedidor = infoM.getSubInforme().getMarcaAnalizadorGases();
		
		informeCaldas.idInspector = infoM.getNumeroIdentificacionInspectorPrueba();
		
		informeCaldas.valorPef = infoM.getPefBancoGases();
		
		informeCaldas.numeroDeSerieDelBanco = infoM.getSerialBancoGases();
		informeCaldas.marcaAnalizador = infoM.getMarcaAnalizadorGases();
		
		informeCaldas.vlrSpanBajoHC =  infoM.getPatronHcBajaPropano();
	    informeCaldas.resultadoVlrSpanBajoHC =  infoM.getResultadoHcBajaPropano();
		informeCaldas.vlrSpanBajoCO =  infoM.getPatronCoBaja();
		informeCaldas.resultadoVlrSpanBajoCO =  infoM.getResultadoCoBajo();
		informeCaldas.vlrSpanBajoCO2 =  infoM.getPatronCo2Baja();
		informeCaldas.resultadoVlrSpanBajoCO2 =  infoM.getResultadoCo2Bajo();
		informeCaldas.vlrSpanAltoHC =  infoM.getPatronHcAltaPropano();
		informeCaldas.resultadoVlrSpanAltoHC =  infoM.getResultadoHcAltaPropano();
		informeCaldas.vlrSpanAltoCO =  infoM.getPatronCoAlta();
		informeCaldas.resultadoVlrSpanAltoCO =  infoM.getResultadoCoAlto();
		informeCaldas.vlrSpanAltoCO2 =  infoM.getPatronCo2Alta();
		informeCaldas.resultadoVlrSpanAltoCO2 =  infoM.getResultadoCo2Alto();
		informeCaldas.fechaHoraUltimaCalibracion =  infoM.getFechaHoraVerificacionGasolina();
		
		informeCaldas.placa = infoM.getPlaca();
		informeCaldas.modelo = infoM.getModelo();			
		informeCaldas.cilindraje = infoM.getCilindraje();			
		informeCaldas.kilometraje = infoM.getKilometraje();
		informeCaldas.marca = infoM.getMarca();
		informeCaldas.linea = infoM.getLinea();
		informeCaldas.clase = infoM.getClase();
		informeCaldas.servicio = infoM.getServicio();
		informeCaldas.combustible = infoM.getCombustible();
		informeCaldas.tipoMotor = "CHISPA";
		
		informeCaldas.temperaturaAmbiente = infoM.getTemperaturaAmbiente();
		informeCaldas.humedadRelativa = infoM.getHumedadRelativa();
		
		informeCaldas.rpmRalenti = infoM.getRpmRalenti();
		informeCaldas.hcRalenti = infoM.getResultadoHcRalenti();
		informeCaldas.coRalenti = infoM.getResultadoCoRalenti();
		informeCaldas.co2Ralenti = infoM.getResultadoCo2Ralenti();
		informeCaldas.o2Ralenti = infoM.getResultadoO2Ralenti();
		
		informeCaldas.rpmCrucero = infoM.getRpmCrucero();
		
		informeCaldas.hcCrucero = infoM.getResultadoHcCrucero();
		informeCaldas.coCrucero = infoM.getResultadoCoCrucero();
		informeCaldas.co2Crucero = infoM.getResultadoCo2Crucero();
		informeCaldas.o2Crucero = infoM.getResultadoO2Crucero();
		
		informeCaldas.conceptoFinal = infoM.getResultadoFinalPrueba();
		
		informeCaldas.presenciaHumoNegroAzul = infoM.getPresenciaHumoNegroAzul();
		informeCaldas.revolucionesFueraRango = infoM.getRpmFueraRango();
		informeCaldas.ausenciaFugasTapaAceite = infoM.getAusenciaFugasTapaAceite();
		informeCaldas.ausenciaFugasTapaCombustible = infoM.getAusenciaFugasTapaCombustible();
		informeCaldas.salidasAdicionales = infoM.getSalidasAdicionales();
		informeCaldas.presenciaDilucion = infoM.getPresenciaDilucion();
		informeCaldas.fugasTubo = infoM.getFugaTuboEscapes();
		informeCaldas.ausenciaMalEstadoFiltroAire = infoM.getSistemaAdmisionAire();
		informeCaldas.desconexionRecirculacion = infoM.getDesconexionPCV();
		informeCaldas.incorrectaOperacionRefrigeracion = infoM.getIncorrectaOperacionRefrigeracion();
		informeCaldas.instalacionDispositivosImpidenIntroducirSonda = infoM.getInstalacionAccesoriosTubo();
		informeCaldas.incumplimientoNivelesEmision = infoM.getIncumplimientoNivelesEmisiones();
		informeCaldas.nivelEmisionAplicable = "Resolucion 0762 de 2022";
		informeCaldas.conceptoFinal = infoM.getResultadoFinalPrueba();
		
		if(resultadoSonometria != null) {
			informeCaldas.decibeles = resultadoSonometria.getValorMedicion();
		}
	}

	public static void llenarInfoDiesel(InformeCaldasDTO informeCaldas,
			InformeDieselCarRes0762 infoM) {
		
		informeCaldas.norma = " NTC 4231";
		
		informeCaldas.numeroInspeccion = infoM.getSubinformeDiesel().getNumeroFUR();
		
		
		informeCaldas.numeroCertificado = infoM.getSubinformeDiesel().getNumeroConsecutivoRunt(); 
	
	
		informeCaldas.serialOpacimetro = infoM.getSubinformeDiesel().getSerialOpacimetro();
	
	
		informeCaldas.marcaOpacimero = infoM.getSubinformeDiesel().getMarcaOpacimero();
		
		informeCaldas.ltoe = "430";
		
		
		
		informeCaldas.fechaInicioPrueba = infoM.getFechaInicioPrueba();
		informeCaldas.fechaFinPrueba = infoM.getFechaFinPrueba();
		
		informeCaldas.placa = infoM.getSubinformeDiesel().getPlaca();
		informeCaldas.modelo = infoM.getSubinformeDiesel().getModelo();			
		informeCaldas.cilindraje = infoM.getSubinformeDiesel().getCilindraje();			
		informeCaldas.kilometraje = infoM.getSubinformeDiesel().getKilometraje();
		informeCaldas.marca = infoM.getSubinformeDiesel().getMarca();
		informeCaldas.linea = infoM.getSubinformeDiesel().getLinea();
		informeCaldas.clase = infoM.getSubinformeDiesel().getClase();
		informeCaldas.servicio = infoM.getSubinformeDiesel().getServicio();
		informeCaldas.combustible = infoM.getSubinformeDiesel().getCombustible();
		informeCaldas.tipoMotor = infoM.getSubinformeDiesel().getTipoMotor();
		
		informeCaldas.temperaturaAmbiente = infoM.getSubinformeDiesel().getTemperaturaAmbiente();
		informeCaldas.humedadRelativa = infoM.getSubinformeDiesel().getHumedadRelativa();
		
		informeCaldas.rpmRalentiCicloPreliminar = infoM.getSubinformeDiesel().getRpmRalentiCicloPreliminar();
	    informeCaldas.rpmGobernadaCicloPreliminar = infoM.getSubinformeDiesel().getRpmGobernadaCicloPreliminar();
		informeCaldas.opacidadCicloPreliminar = infoM.getSubinformeDiesel().getOpacidadCicloPreliminar();
		informeCaldas.densidadHumoCicloPreliminar = infoM.getSubinformeDiesel().getDensidadHumoCicloPreliminar();
		
		informeCaldas.rpmRalentiCicloUno = infoM.getSubinformeDiesel().getRpmRalentiCicloUno();
		informeCaldas.rpmGobernadaCicloUno = infoM.getSubinformeDiesel().getRpmGobernadaCicloUno();
		informeCaldas.opacidadCicloUno = infoM.getSubinformeDiesel().getOpacidadCicloUno();
		informeCaldas.densidadHumoCicloUno = infoM.getSubinformeDiesel().getDensidadHumoCicloUno();
		
		informeCaldas.rpmRalentiCicloDos = infoM.getSubinformeDiesel().getRpmRalentiCicloDos();
		informeCaldas.rpmGobernadaCicloDos = infoM.getSubinformeDiesel().getRpmGobernadaCicloDos();
		informeCaldas.opacidadCicloDos = infoM.getSubinformeDiesel().getOpacidadCicloDos();
		informeCaldas.densidadHumoCicloDos = infoM.getSubinformeDiesel().getDensidadHumoCicloDos();
		
		informeCaldas.rpmRalentiCicloTres = infoM.getSubinformeDiesel().getRpmRalentiCicloTres();
		informeCaldas.rpmGobernadaCicloTres = infoM.getSubinformeDiesel().getRpmGobernadaCicloTres();
		informeCaldas.opacidadCicloTres = infoM.getSubinformeDiesel().getOpacidadCicloTres();
		informeCaldas.densidadHumoCicloTres = infoM.getSubinformeDiesel().getDensidadHumoCicloTres();
		
		informeCaldas.resultadoFinalOpacidad = infoM.getSubinformeDiesel().getResultadoFinalOpacidad();
		informeCaldas.resultadoFinalDensidadHumo = infoM.getSubinformeDiesel().getResultadoFinalDensidadHumo();
		informeCaldas.resultadoFinalPruebaRealizada = infoM.getSubinformeDiesel().getResultadoFinalPruebaRealizada();
		
		//defectos					
		
		informeCaldas.ausenciaFugasTapaAceite = infoM.getSubinformeDiesel().getAusenciaFugasTapaAceite();
		informeCaldas.ausenciaFugasTapaCombustible = infoM.getSubinformeDiesel().getAusenciaFugasTapaCombustible();
		informeCaldas.salidasAdicionales = infoM.getSubinformeDiesel().getSalidasAdicionales();
		
		informeCaldas.fugasTubo = infoM.getSubinformeDiesel().getFugasTubo();
		informeCaldas.ausenciaMalEstadoFiltroAire = infoM.getSubinformeDiesel().getIncorrectaInstalacionAusenciaFiltroAire();
		
		informeCaldas.incorrectaOperacionRefrigeracion = infoM.getSubinformeDiesel().getIncorrectaOperacionRefrigeracion();
		informeCaldas.instalacionDispositivosImpidenIntroducirSonda = infoM.getSubinformeDiesel().getInstalacionDispositivosImpidenIntroducirSonda();
		
		informeCaldas.incorrectaOperacionGobernador = infoM.getSubinformeDiesel().getIncorrectaOperacionGobernador();
		informeCaldas.fallaSubita = infoM.getSubinformeDiesel().getFallaSubitaMotor();
		informeCaldas.ejecucionIncorrecta = infoM.getSubinformeDiesel().getNoSeAlcanzaGobernadas();
		informeCaldas.diferenciaAritmetica = infoM.getSubinformeDiesel().getRechazoPorDiferenciaAritmetica();
		informeCaldas.diferenciaTemperatura = infoM.getSubinformeDiesel().getDiferenciaTemperaturaMayor10();
		informeCaldas.activacionDispositivos = infoM.getSubinformeDiesel().getDispositivosInstaladosAlteraVelocidad();
		informeCaldas.tipoMotor = "COMPRESION";
		informeCaldas.kilometraje = infoM.getSubinformeDiesel().getKilometraje();
		
		informeCaldas.temperaturaInicialMotor = infoM.getSubinformeDiesel().getTemperaturaInicialMotor();
		informeCaldas.temperaturaFinalMotor = infoM.getSubinformeDiesel().getTemperaturaFinalMotor();
		informeCaldas.incumplimientoNivelesEmision = infoM.getSubinformeDiesel().getIncumplimientoNivelesEmisiones();
		informeCaldas.conceptoFinal =infoM.getSubinformeDiesel().getResultadoFinalPruebaRealizada();
		
		//sonido
		
		if(infoM.getResultadoSonometria() != null) {
			informeCaldas.decibeles = infoM.getResultadoSonometria().getValorMedicion();
		}
		
	}
	
	
}
