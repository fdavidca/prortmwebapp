package com.proambiente.webapp.util;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;
import com.proambiente.modelo.dto.Muestra;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author fdavi
 */
public class MuestrasJsonCodificador {
    
    public static String codificarListaMuestas(List<Muestra> listaMuestras) throws JsonProcessingException{
        ObjectMapper mapper = new ObjectMapper();
            mapper.disable(WRITE_DATES_AS_TIMESTAMPS);
            mapper.setDateFormat(new ISO8601DateFormat());
            String datosDeMuestras = mapper.writeValueAsString(listaMuestras);
            return datosDeMuestras;
    }
    
    public static List<Muestra> decodificarListaMuestras(String jsonSerializado) throws JsonParseException, JsonMappingException, IOException{
    	ObjectMapper mapper = new ObjectMapper();
        mapper.disable(WRITE_DATES_AS_TIMESTAMPS);
        mapper.setDateFormat(new ISO8601DateFormat());
        Muestra[] muestras = mapper.readValue(jsonSerializado, Muestra[].class);
        return Arrays.asList(muestras);
        
    }
}
