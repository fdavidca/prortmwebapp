package com.proambiente.webapp.util;

public class ConstantesMedidasSuspension {
		
	public static final int ADHERENCIA_DERECHA_EJE_UNO = 6016;
    public static final int ADHERENCIA_DERECHA_EJE_DOS = 6017;
    public static final int ADHERENCIA_DERECHA_EJE_TRES = 6018;
    public static final int ADHERENCIA_DERECHA_EJE_CUATRO = 6019;
    
    public static final int ADHERENCIA_IZQUIERDA_EJE_UNO = 6020;
    public static final int ADHERENCIA_IZQUIERDA_EJE_DOS = 6021;
    public static final int ADHERENCIA_IZQUIERDA_EJE_TRES = 6022;
    public static final int ADHERENCIA_IZQUIERDA_EJE_CUATRO = 6023;
    
}
