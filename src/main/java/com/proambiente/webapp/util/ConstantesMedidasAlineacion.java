package com.proambiente.webapp.util;

public class ConstantesMedidasAlineacion {
	
	public static final int DESVIACION_EJE_UNO = 4000;
    public static final int DESVIACION_EJE_DOS = 4001;
    public static final int DESVIACION_EJE_TRES = 4002;
    public static final int DESVIACION_EJE_CUATRO = 4003;
	public static final int DESVIACION_EJE_CINCO = 4004;
	public static final int DESVIACION_EJE_SEIS = -99;

}
