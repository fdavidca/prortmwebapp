package com.proambiente.webapp.util.informes.medellin;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;

import com.proambiente.modelo.Analizador;
import com.proambiente.modelo.Equipo;
import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.Medida;
import com.proambiente.modelo.Pipeta;
import com.proambiente.modelo.Propietario;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Usuario;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.modelo.VerificacionGasolina;

import com.proambiente.modelo.dto.ResultadoOttoVehiculosDTO;
import com.proambiente.webapp.service.util.UtilInformesMedidas;
import com.proambiente.webapp.util.ConstantesMedidasGases;
import com.proambiente.webapp.util.dto.ResultadoOttoInformeDTO;


public class GeneradorInformeLivianosRes0762 {
	
	private static final char DECIMAL_SEPARATOR_PUNTO = '.';
	
	public void agregarInformacionCda(InformacionCda infoCda, InformeLivianosRes0762 informe) {
		
		informe.setNumeroExpedienteCda( infoCda.getNumeroExpedienteCda() );
		informe.setTipoIdentificacionCda(infoCda.getTipoIdentificacionCda());
		informe.setNumeroIdentificacionCda( infoCda.getNit() );
		informe.setNombreRazonSocial( infoCda.getNombreRazonSocialCda());
		informe.setNombreCdaEstablecimientoComercio( infoCda.getNombre() );
		informe.setClaseCda(infoCda.getClaseCda());
		informe.setDireccionCda( infoCda.getDireccion() );
		informe.setCorreoElectronicoCda( infoCda.getCorreoElectronico() );
		informe.setTelefonoCda( infoCda.getTelefono1() );
		informe.setTelefonoSecundarioCda( infoCda.getTelefono2() );
		informe.setMunicipioCda(infoCda.getCiudad());
		informe.setResolucionAutoridadAmbiental(infoCda.getNumeroResolucionSDA());
		informe.setFechaResolucionVigente(infoCda.getFechaResolucionSDA());
		if(infoCda.getTotalAnalizadoresLivianos() != null) {
			informe.setTotalEquiposAnalizadoresGases( String.valueOf( infoCda.getTotalAnalizadoresLivianos() ) );
		}else {
			informe.setTotalEquiposAnalizadoresGases("");
		}
		
		
	}
	
	public void agregarInformacionPropietario(Propietario propietario,InformeLivianosRes0762 informe) {
		if(propietario != null) {
			
			informe.setTipoIdentificacionPropietario(propietario.getTipoDocumentoIdentidad().getCodigoParametricoRunt());
			informe.setNumeroIdentificacionPropietario( String.valueOf( propietario.getPropietarioId()));
			informe.setNombresApellidosPropietario( propietario.getNombres() + " "
					+ propietario.getApellidos() );
			informe.setDireccionPropietario( propietario.getDireccion() );
			informe.setTelefonoPropietario( propietario.getTelefono1() );
			informe.setTelefonoSecundarioPropietario( propietario.getTelefono2() );
			informe.setMunicipioPropietario( propietario.getCiudad().getNombre() );
			informe.setCorreoElectronicoPropietario(propietario.getCorreoElectronico());
			
		}
		
	}
	
	public void agregarInformacionEquipos(List<Equipo> equipos,InformeLivianosRes0762 informe) {
		
		Optional<Equipo> termohigrometroOpt = equipos.stream().filter( e-> e.getTipoEquipo().getTipoEquipoId().equals(2 )).findAny();
		if(termohigrometroOpt.isPresent()) {
			
			Equipo termohigrometro = termohigrometroOpt.get();
			informe.setSerialSensorTemperaturaAmbiente( termohigrometro.getSerieEquipo() );
			informe.setMarcaSensorTemperaturaAmbiente ( termohigrometro.getFabricante() );
			informe.setSerialSensorHumedadRelativa( termohigrometro.getSerieEquipo() );
			informe.setMarcaSensorHumedadRelativa( termohigrometro.getFabricante() );
			
		}
		
		Optional<Equipo> sondaTemperaturaOpt = equipos.stream().filter( e-> e.getTipoEquipo().getTipoEquipoId().equals( 3 )).findAny();
		if(sondaTemperaturaOpt.isPresent()) {
			Equipo sonda = sondaTemperaturaOpt.get();
			informe.setMarcaSensorTemperaturaMotor( sonda.getFabricante() );
			informe.setSerialSensorTemperaturaMotor( sonda.getSerieEquipo() );
			
		}
		
		Optional<Equipo> tacometroOpt = equipos.stream().filter( e-> e.getTipoEquipo().getTipoEquipoId().equals( 4 )).findAny();
		if(tacometroOpt.isPresent()) {
			Equipo tacometro = tacometroOpt.get();
			informe.setMarcaCaptadorRpm(tacometro.getFabricante());
			informe.setSerialCaptadorRpm(tacometro.getSerieEquipo() );
		}
		
	}
	
	public void agregarInformacionVehiculo(Vehiculo vehiculo,InformeLivianosRes0762 informe) {
		
		informe.setPlaca( vehiculo.getPlaca());
		informe.setModelo( String.valueOf( vehiculo.getModelo()) );
		informe.setNumeroMotor( String.valueOf( vehiculo.getNumeroMotor() ));
		informe.setVin( vehiculo.getVin());
		informe.setCilindraje( vehiculo.getCilindraje()==null? "0":vehiculo.getCilindraje().toString() );
		informe.setLicenciaTransito( vehiculo.getNumeroLicencia());
		informe.setMarca( vehiculo.getMarca().getNombre() );
		informe.setLinea( vehiculo.getLineaVehiculo().getNombre() );
		informe.setClase( vehiculo.getClaseVehiculo().getNombre() );
		informe.setServicio( vehiculo.getServicio().getNombre() );
		informe.setCombustible( vehiculo.getTipoCombustible().getNombre() );
		
		if(vehiculo.getTiemposMotor() != null)
			informe.setTipoMotor( vehiculo.getTiemposMotor().toString() );
		
		informe.setConversionGNV( vehiculo.getConversionGnv() );
		if( vehiculo.getConversionGnv() != null && vehiculo.getConversionGnv().equalsIgnoreCase("SI") ) {
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
			if(vehiculo.getFechaVenicimientoGNV() != null) {
				try {
					informe.setFechaConversionGNV( sdf.format( vehiculo.getFechaVenicimientoGNV() ));
				}catch(Exception exc) {
					
				}
			}
			
		}
		
		
	}
	
	public void cargarInformacionResultado( InformeLivianosRes0762 informe,ResultadoOttoVehiculosDTO resultado) {
		
		informe.setRpmRalenti( resultado.getRpmRalenti() );
		informe.setTemperaturaMotor( resultado.getTempRalenti() );
		informe.setRpmCrucero( resultado.getRpmCrucero() );
		
		informe.setResultadoHcRalenti( resultado.getHcRalenti() );
		informe.setResultadoCoRalenti( resultado.getCoRalenti() );
		informe.setResultadoCo2Ralenti( resultado.getCo2Ralenti() );
		informe.setResultadoO2Ralenti( resultado.getO2Ralenti() );
		
		informe.setResultadoHcCrucero( resultado.getHcCrucero() );
		informe.setResultadoCoCrucero( resultado.getCoCrucero() );
		informe.setResultadoCo2Crucero( resultado.getCo2Crucero() );
		informe.setResultadoO2Crucero( resultado.getO2Crucero() );
		
		informe.setRpmFueraRango( convertirTrueFalseASiNo( resultado.getRevolucionesFueraRango() ) );
		informe.setFugaTuboEscapes( convertirTrueFalseASiNo( resultado.getFugasTuboEscape() ) );
		informe.setSalidasAdicionales( convertirTrueFalseASiNo( resultado.getSalidasAdicionales() ) );
		informe.setAusenciaFugasTapaAceite( convertirTrueFalseASiNo( resultado.getTapaAceite() ) );
		informe.setAusenciaFugasTapaCombustible( convertirTrueFalseASiNo( resultado.getTapaCombustible() ) );
		informe.setPresenciaDilucion( convertirTrueFalseASiNo( resultado.getDilucion() ) );
		informe.setDesconexionPCV( convertirTrueFalseASiNo( resultado.getDesconexionPCV() ) );
		informe.setPresenciaHumoNegroAzul( convertirTrueFalseASiNo( resultado.getPresenciaHumos() ) );
		informe.setIncorrectaOperacionRefrigeracion( convertirTrueFalseASiNo( resultado.getFallaSistemaRefrigeracion() ) ) ;
		
	}
	

	
	public void cargarInformacionResultadoAdicional( InformeLivianosRes0762 informe,ResultadoOttoInformeDTO  resultado) {
		
		informe.setTemperaturaAmbiente(  resultado.getTemperaturaAmbiente()  );
		informe.setHumedadRelativa(  resultado.getHumedadRelativa()  );
		informe.setInstalacionAccesoriosTubo( convertirTrueFalseASiNo( resultado.getPresenciaAccesoriosImpiden() ) );
		informe.setSistemaAdmisionAire( convertirTrueFalseASiNo( resultado.getFallaFiltroAire() ) );
		informe.setIncumplimientoNivelesEmisiones( convertirTrueFalseASiNo(  resultado.getIncumplimientoNivelesDeEmision() ) );
		
		
	}
	
	public void establecerPuntoSimboloDecimal(DecimalFormat df) {
		DecimalFormatSymbols ds = DecimalFormatSymbols.getInstance();
		ds.setDecimalSeparator(DECIMAL_SEPARATOR_PUNTO);
		df.setDecimalFormatSymbols(ds);
	}
	
	public void agregarResultadoVerificacion( InformeLivianosRes0762 informe, VerificacionGasolina verificacion, Usuario usuario, Double pef) {
		
		informe.setTipoIdentificacionInspectorVerificacionGasolina( usuario.getTipoIdentificacion());
		informe.setNumeroIdentificacionInspectorVerificacionGasolina( String.valueOf ( usuario.getCedula() ) );
		String nombres = usuario.getNombres() + " " + usuario.getApellidos();
		informe.setNombreInspectorVerificacionGasolina( nombres );
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String fechaVerificacionGasolina = sdf.format( verificacion.getFechaVerificacion());
		informe.setFechaHoraVerificacionGasolina( fechaVerificacionGasolina );
		
		DecimalFormat dfHc = new DecimalFormat("#");
		
		informe.setResultadoHcAltaPropano( dfHc.format( verificacion.getValorAltoHc() ) );
		Double hcPropanoAlta = verificacion.getValorAltoHc() * pef;
		BigDecimal bdHcAltaHexano = new BigDecimal(hcPropanoAlta).setScale(0,RoundingMode.HALF_EVEN);
		informe.setResultadoHcAltaHexano( bdHcAltaHexano.toString() );
		
		informe.setResultadoHcBajaPropano( dfHc.format( verificacion.getValorBajoHc() ));
		Double hcHexanoBaja = verificacion.getValorBajoHc() * pef;
		BigDecimal bdHcBajaHexano = new BigDecimal(hcHexanoBaja).setScale(0,RoundingMode.HALF_EVEN);
		informe.setResultadoHcBajaHexano( bdHcBajaHexano.toString() );
		
		
		
		DecimalFormat dfCo = new DecimalFormat("0.00");
		establecerPuntoSimboloDecimal(dfCo);
		informe.setResultadoCoAlto( dfCo.format( verificacion.getValorAltoCo() ) );
		informe.setResultadoCoBajo ( dfCo.format( verificacion.getValorBajoCo() ));
		
		DecimalFormat dfCo2 = new DecimalFormat("0.0");
		establecerPuntoSimboloDecimal(dfCo2);
		informe.setResultadoCo2Alto( dfCo2.format( verificacion.getValorAltoCo2()));
		informe.setResultadoCo2Bajo( dfCo2.format( verificacion.getValorBajoCo2()));
		
		if( verificacion.getValorAltoO2() != null) {
			informe.setResultadoO2Alto( dfCo2.format( verificacion.getValorAltoO2() ));
		}
		
		if( verificacion.getValorBajoO2() != null) {
			informe.setResultadoO2Bajo( dfCo2.format( verificacion.getValorBajoO2()) );
		}
		
		informe.setCriterioVerificacionGas("NTC 4983");
		
		informe.setResultadoVerificacionGas( verificacion.getAprobada() ? "APROBADA" : "RECHAZADA");
	}
	
	public void agregarInformacionPipetas( InformeLivianosRes0762 informe, Pipeta pipetaBaja, Pipeta pipetaAlta, Double pef ) {
		
		informe.setLaboratorioEmiteCertificadoAlta( pipetaAlta.getLaboratorioEmisorCertificado());
		informe.setCilindroGasAlta( pipetaAlta.getNumeroCilindro() );
		informe.setCertificadoGasAlta( pipetaAlta.getNumeroCertificado() );
		
		informe.setPatronHcAltaPropano( String.valueOf( pipetaAlta.getValorHC() ) );
		Double hcHexano = pipetaAlta.getValorHC() * pef;
		BigDecimal bdHcHexano = new BigDecimal( hcHexano).setScale(0,RoundingMode.HALF_EVEN);
		informe.setPatronHcAltaHexano(bdHcHexano.toString());
		
		DecimalFormat dfCo = new DecimalFormat("0.00");
		establecerPuntoSimboloDecimal(dfCo);
		informe.setPatronCoAlta( dfCo.format( pipetaAlta.getValorCO() ) );
		DecimalFormat dfCo2 = new DecimalFormat("0.0");
		establecerPuntoSimboloDecimal(dfCo2);
		informe.setPatronCo2Alta ( dfCo2.format( pipetaAlta.getValorCO2() ));
		if(pipetaAlta.getValorO2() != null)
			informe.setPatronO2Alta( dfCo2.format( pipetaAlta.getValorO2()));
		
		informe.setLaboratorioEmiteCertificadoGasBaja( pipetaBaja.getLaboratorioEmisorCertificado() );
		informe.setCilindroGasBaja( pipetaBaja.getNumeroCilindro() );
		informe.setCertificadoGasBaja( pipetaBaja.getNumeroCertificado() );
		
		informe.setPatronHcBajaPropano( String.valueOf( pipetaBaja.getValorHC() ) );
		Double hcHexanoBaja = pipetaBaja.getValorHC() * pef;
		BigDecimal bdHcHexanoBaja = new BigDecimal( hcHexanoBaja).setScale(0,RoundingMode.HALF_EVEN);
		informe.setPatronHcBajaHexano(bdHcHexanoBaja.toString());
		
		
		informe.setPatronCoAlta( dfCo.format( pipetaAlta.getValorCO() ) );
		
		informe.setPatronCo2Alta ( dfCo2.format( pipetaAlta.getValorCO2() ));
		
		
		
		informe.setPatronCoBaja( dfCo.format( pipetaBaja.getValorCO() ) );
		
		informe.setPatronCo2Baja( dfCo.format( pipetaBaja.getValorCO2() ));
		
		if(pipetaBaja.getValorO2() != null)		
			informe.setPatronO2Baja( dfCo.format( pipetaBaja.getValorO2() ) );
		
	}
	
	
	public  void cargarInformacionMedidasVehiculo(InformeLivianosRes0762 informe,List<Medida> medidas) {
		UtilInformesMedidas utilInformesMedidas = new UtilInformesMedidas();
		
		OptionalDouble conoceRpmFabricante = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,ConstantesMedidasGases.CODIGO_CONOCE_RPM_FABRICANTE);
		String conoceRpmFabricanteStr = "NO";
		if(conoceRpmFabricante.isPresent()) {
			
			conoceRpmFabricanteStr = conoceRpmFabricante.getAsDouble() > 0.0 ? "SI" : "NO";
		}else {
			conoceRpmFabricanteStr = "NO";
		}	
		informe.setConoceRpmFabricante(conoceRpmFabricanteStr);
		
		DecimalFormat df = new DecimalFormat("0");
		if( conoceRpmFabricante.isPresent() &&  conoceRpmFabricante.getAsDouble() > 0.0 ) {
			
			OptionalDouble rpmMinRalFabricante = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,ConstantesMedidasGases.CODIGO_RPM_RAL_MIN_FABRICANTE);
			Double rpmMinRalFabricanteVlr = rpmMinRalFabricante.isPresent() ? rpmMinRalFabricante.getAsDouble() : 0.0 ;		
			informe.setRpmMinRalFabricante( df.format(rpmMinRalFabricanteVlr));
			
			OptionalDouble rpmMaxRalFabricante = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,ConstantesMedidasGases.CODIGO_RPM_RAL_MAX_FABRICANTE);
			Double rpmMaxRalFabricanteVlr = rpmMaxRalFabricante.isPresent() ? rpmMaxRalFabricante.getAsDouble() : 0.0 ;		
			informe.setRpmMaxRalFabricante( df.format(rpmMaxRalFabricanteVlr));
			
		}
		
		OptionalDouble tubosEscape = utilInformesMedidas.obtenerMedidaPorCodigo( medidas, ConstantesMedidasGases.CODIGO_NUM_TUBO_ESCAPES);
		String tubosEscapeStr = tubosEscape.isPresent() ? df.format( tubosEscape.getAsDouble() ) : "1";
		informe.setNumeroTubosEscape( tubosEscapeStr );
		
		OptionalDouble catalizador = utilInformesMedidas.obtenerMedidaPorCodigo( medidas, ConstantesMedidasGases.CODIGO_CATALIZADOR);
		String catalizadorStr = "NO";
		if( catalizador.isPresent() ) {
			catalizadorStr = catalizador.getAsDouble() == 1.0 ? "SI" : catalizador.getAsDouble() < 0.0 ? "NA": "NO"  ;
		}else {
			catalizadorStr = "NO";
		}
		informe.setVehiculoConCatalizador(catalizadorStr);	

		
	}
	
	
	
	public void agregarInformacionRevision(InformeLivianosRes0762 informe, Revision revision, String fechaFur,Usuario jefeTecnico ) {
		
		String numeroFur = String.valueOf( revision.getRevisionId() )  +"-" +String.valueOf( revision.getNumeroInspecciones() );
		informe.setNumeroFUR( numeroFur );
		informe.setNumeroConsecutivoRunt( revision.getConsecutivoRunt() != null ? revision.getConsecutivoRunt() : " " );
		informe.setFechaFUR(fechaFur);
		
		if( revision.getNumeroInspecciones() > 1 ) {			
			
		    StringBuilder sb  = new StringBuilder().append( revision.getRevisionId()).append("- 1").append(",");
		    sb.append( revision.getRevisionId() ).append(" -2");
		    String furAsociados = sb.toString();
			informe.setFurAsociados( furAsociados );
			
		}else {
			
			String furAsociados = new StringBuilder().append( revision.getRevisionId()).append("-").append(1).toString();
			informe.setFurAsociados( furAsociados );
		}
		
		if(jefeTecnico != null) {
			informe.setTipoIdentificacionDirectorTecnico( jefeTecnico.getTipoIdentificacion() );
			informe.setNumeroIdentificacionDirectorTecnico( String.valueOf( jefeTecnico.getCedula() ));
			informe.setNombreDirectorTecnico( jefeTecnico.getNombres() + " " + jefeTecnico.getApellidos() );
		}
		
		
	}
	
public void agregarInformacionPrueba(InformeLivianosRes0762 informe, Prueba prueba) {
		
		SimpleDateFormat sdf = new SimpleDateFormat( "dd/MM/yyyy HH:mm:ss");
		informe.setFechaHoraInicioInspeccion( sdf.format( prueba.getFechaInicio() ));
		informe.setFechaHoraFinInspeccion( sdf.format( prueba.getFechaFinalizacion() ));
		informe.setFechaHoraAbortoInspeccion( prueba.getFechaAborto() != null ? sdf.format( prueba.getFechaAborto()) : "");
		
		if ( prueba.getCausaAborto() != null) {
			informe.setCausaAbortoInspeccion( String.valueOf ( prueba.getCausaAborto().getCausaAbortoId() ) );
		}
		

		
		Usuario usuario = prueba.getUsuario();
		if(usuario != null) {
			
			informe.setTipoIdentificacionInspectorPrueba( usuario.getTipoIdentificacion() );
			informe.setNumeroIdentificacionInspectorPrueba( String.valueOf ( usuario.getCedula() ) );
			informe.setNombreInspectorPrueba( usuario.getNombres() + " " + usuario.getApellidos() );
			
		}
		
		
		
		
	}

public void agregarInformacionSoftware(InformeLivianosRes0762 informe, String nombreSoftware,
		String versionSoftware) {
	
	
	informe.setNombreSoftware(nombreSoftware);
	informe.setVersionSoftware(versionSoftware);
	informe.setNombreProveedorSoftware( "Proambiente s.a.s.");
	
	
}

public void agregarInformacionCertificado(InformeLivianosRes0762 informe, String consecutivoCertificado) {
	informe.setCertificadoEmitido(consecutivoCertificado);
	
}

public void agregarInformacionAnalizador(Analizador analizador, InformeLivianosRes0762 informe) {
	
	informe.setMarcaBancoGases( analizador.getMarca() );
	informe.setModeloBancoGases( analizador.getModeloBancoGases() );
	informe.setSerialBancoGases( analizador.getSerialBancoGases() );
	
	informe.setMarcaAnalizadorGases( analizador.getMarcaAnalizadorGases() );
	informe.setModeloAnalizadorGases( analizador.getModelo() );
	informe.setSerialAnalizadorGases(analizador.getSerialAnalizador());
	
	DecimalFormat df = new DecimalFormat("0.000");
	establecerPuntoSimboloDecimal(df);
	informe.setPefBancoGases( df.format( analizador.getPef() )) ;
	informe.setSerialElectronicoAnalizadorGases( analizador.getSerial() );	
}

	
private String convertirTrueFalseASiNo(String valor) {
	if( valor == null) return "";
	if(valor.equalsIgnoreCase("TRUE")) {
		return "SI";
	}else if(valor.equalsIgnoreCase("FALSE")){
		return "NO";
	}else {
		return valor;
	}
}	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
