package com.proambiente.webapp.util.informes.medellin;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;

import com.ibm.icu.text.SimpleDateFormat;
import com.proambiente.modelo.Analizador;
import com.proambiente.modelo.Equipo;
import com.proambiente.modelo.FiltroDensidadNeutra;
import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.Medida;
import com.proambiente.modelo.Propietario;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Usuario;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.modelo.VerificacionLinealidad;
import com.proambiente.modelo.dto.ResultadoDieselDTO;
import com.proambiente.webapp.service.util.UtilInformesMedidas;
import com.proambiente.webapp.util.ConstantesMedidasGases;
import com.proambiente.webapp.util.dto.ResultadoDieselDTOInformes;

public class GeneradorInformeDieselRes0762 {
	
	private static final char DECIMAL_SEPARATOR_PUNTO = '.';
	private static final char DECIMAL_SEPARATOR_COMA = ',';
	
	private enum SeparadorDECIMAL{ COMA, PUNTO};

	private SeparadorDECIMAL separador = SeparadorDECIMAL.PUNTO;
	
	public void establecerPuntoComoSeparadorDecimal() {
		separador = SeparadorDECIMAL.PUNTO;
	};
	
	public void establecerComaComoSeparadorDecimal() {
		separador = SeparadorDECIMAL.COMA;
	};
	
	public void agregarInformacionCda(InformacionCda infoCda, InformeDieselRes0762 informe) {

		informe.setNumeroExpedienteCda(infoCda.getNumeroExpedienteCda());
		informe.setTipoIdentificacionCda(infoCda.getTipoIdentificacionCda());
		informe.setNumeroIdentificacionCda(infoCda.getNit());
		informe.setNombreRazonSocial(infoCda.getNombreRazonSocialCda());
		informe.setNombreCdaEstablecimientoComercio(infoCda.getNombre());
		informe.setClaseCda(infoCda.getClaseCda());
		informe.setDireccionCda(infoCda.getDireccion());
		informe.setCorreoElectronicoCda(infoCda.getCorreoElectronico());
		informe.setTelefonoCda(infoCda.getTelefono1());
		informe.setTelefonoSecundarioCda(infoCda.getTelefono2());
		informe.setMunicipioCda(infoCda.getCiudad());
		informe.setResolucionAutoridadAmbiental(infoCda.getNumeroResolucionSDA());
		informe.setFechaResolucionVigente(infoCda.getFechaResolucionSDA());
		if (infoCda.getTotalOpacimetros() != null) {
			informe.setTotalOpacimetros(infoCda.getTotalOpacimetros().toString());
		}

	}

	public void agregarInformacionPropietario(Propietario propietario, InformeDieselRes0762 informe) {
		if (propietario != null) {

			informe.setTipoIdentificacionPropietario(
					propietario.getTipoDocumentoIdentidad().getCodigoParametricoRunt());
			informe.setNumeroIdentificacionPropietario(String.valueOf(propietario.getPropietarioId()));
			informe.setNombresApellidosPropietario(propietario.getNombres() + " " + propietario.getApellidos());
			informe.setDireccionPropietario(propietario.getDireccion());
			informe.setTelefonoPropietario(propietario.getTelefono1());
			informe.setTelefonoSecundarioPropietario(propietario.getTelefono2());
			informe.setMunicipioPropietario(propietario.getCiudad().getNombre());
			informe.setCorreoElectronicoPropietario(propietario.getCorreoElectronico());

		}

	}

	public void agregarInformacionVehiculo(Vehiculo vehiculo, InformeDieselRes0762 informe) {

		informe.setPlaca(vehiculo.getPlaca());
		informe.setModelo(String.valueOf(vehiculo.getModelo()));
		informe.setNumeroMotor(String.valueOf(vehiculo.getNumeroMotor()));
		informe.setVin(vehiculo.getVin());
		informe.setCilindraje( vehiculo.getCilindraje()==null ? "0": vehiculo.getCilindraje().toString() );
		informe.setLicenciaTransito(vehiculo.getNumeroLicencia());
		informe.setMarca(vehiculo.getMarca().getNombre());
		informe.setLinea(vehiculo.getLineaVehiculo().getNombre());
		informe.setClase(vehiculo.getClaseVehiculo().getNombre());
		informe.setServicio(vehiculo.getServicio().getNombre());
		informe.setCombustible(vehiculo.getTipoCombustible().getNombre());

		if (vehiculo.getTiemposMotor() != null)
			informe.setTipoMotor(vehiculo.getTiemposMotor().toString());

	}

	public void cargarInformacionResultado(InformeDieselRes0762 informeDieselDTO, ResultadoDieselDTO resultado,
			ResultadoDieselDTOInformes r2) {

		informeDieselDTO.setTemperaturaAmbiente(r2.getTemperaturaAmbiental());
		informeDieselDTO.setHumedadRelativa(r2.getHumedadRelativa());

		informeDieselDTO.setTemperaturaInicialMotor(resultado.getTemperaturaInicial());

		informeDieselDTO.setRpmRalentiCicloPreliminar(r2.getRpmRalenti());
		informeDieselDTO.setOpacidadCicloPreliminar(resultado.getAceleracionCero());
		informeDieselDTO.setRpmGobernadaCicloPreliminar(r2.getRpmGobernadaCiclo0());

		informeDieselDTO.setOpacidadCicloUno(resultado.getAceleracionUno());
		informeDieselDTO.setRpmGobernadaCicloUno(r2.getRpmGobernadaCiclo1());

		informeDieselDTO.setOpacidadCicloDos(resultado.getAceleracionDos());
		informeDieselDTO.setRpmGobernadaCicloDos(r2.getRpmGobernadaCiclo2());

		informeDieselDTO.setOpacidadCicloTres(resultado.getAceleracionTres());
		informeDieselDTO.setRpmGobernadaCicloTres(r2.getRpmGobernadaCiclo3());

		informeDieselDTO.setResultadoFinalOpacidad(resultado.getValorFinal());
		informeDieselDTO.setTemperaturaFinalMotor(r2.getTemperaturaFinal());

		informeDieselDTO.setFugasTubo(resultado.getFugasTuboEscape().equalsIgnoreCase("TRUE") ? "SI" : "NO");

		informeDieselDTO
				.setAusenciaFugasTapaCombustible(resultado.getTapaCombustible().equalsIgnoreCase("TRUE") ? "SI" : "NO");
		informeDieselDTO.setAusenciaFugasTapaAceite(resultado.getTapaAceite().equalsIgnoreCase("TRUE") ? "SI" : "NO");

		informeDieselDTO
				.setSalidasAdicionales(resultado.getSalidasAdicionales().equalsIgnoreCase("TRUE") ? "SI" : "NO");
		informeDieselDTO.setIncorrectaInstalacionAusenciaFiltroAire(
				resultado.getFiltroAire().equalsIgnoreCase("TRUE") ? "SI" : "NO");
		informeDieselDTO.setIncorrectaOperacionRefrigeracion(
				resultado.getSistemaRefrigeracion().equalsIgnoreCase("TRUE") ? "SI" : "NO");
		informeDieselDTO.setRpmFueraRango(resultado.getRevolucionesFueraRango().equalsIgnoreCase("TRUE") ? "SI" : "NO");
		informeDieselDTO
				.setNoSeAlcanzaGobernadas(r2.getVelocidadNoAlcanzada5Seg().equalsIgnoreCase("TRUE") ? "SI" : "NO");
		informeDieselDTO.setIndicadorMalFuncionamientoMotor(
				r2.getMalFuncionamientoMotor().equalsIgnoreCase("TRUE") ? "SI" : "NO");
		informeDieselDTO.setIncumplimientoNivelesEmisiones(
				r2.getIncumplimientoNiveles().equalsIgnoreCase("TRUE") ? "SI" : "NO");

		informeDieselDTO
				.setSalidasAdicionales(resultado.getSalidasAdicionales().equalsIgnoreCase("TRUE") ? "SI" : "NO");

		informeDieselDTO.setInstalacionDispositivosImpidenIntroducirSonda(
				resultado.getSistemaMuestreo().equalsIgnoreCase("TRUE") ? "SI" : "NO");

		informeDieselDTO.setDispositivosInstaladosAlteraVelocidad(
				r2.getInstalacionDispositivosAlteranRpms().equalsIgnoreCase("TRUE") ? "SI" : "NO");
		informeDieselDTO.setRechazoPorDiferenciaAritmetica(
				r2.getDiferenciasAritmeticas().equalsIgnoreCase("TRUE") ? "SI" : "NO");
		informeDieselDTO.setFallaSubitaMotor(r2.getFallaSubitaMotor().equalsIgnoreCase("TRUE") ? "SI" : "NO");
		informeDieselDTO
				.setDiferenciaTemperaturaMayor10(r2.getDiferenciaTemperaturas().equalsIgnoreCase("TRUE") ? "SI" : "NO");
		informeDieselDTO.setIncorrectaOperacionGobernador(
				r2.getGobernadorNoLimitaRevoluciones().equalsIgnoreCase("TRUE") ? "SI" : "NO");

	}
	
	public void cargarInformacionMedidasRpmRalenti( InformeDieselRes0762 informe,List<Medida> medidas) {

		UtilInformesMedidas utilInformesMedidas = new UtilInformesMedidas();
//		public static final int CODIGO_VELOCIDAD_RALENTI_PROMEDIO = 8050;
//		
//		
//		public static final int CODIGO_REV_RAL_CICLO_PRELIMINAR = 8037;
//		public static final int CODIGO_REV_RAL_PRIMER_CICLO = 8038;
//		public static final int CODIGO_REV_RAL_SEGUNDO_CICLO = 8039;
//		public static final int CODIGO_REV_RAL_TERCER_CICLO = 8040;
		
		DecimalFormat df = new DecimalFormat("0");
		
		OptionalDouble revRalentiPreliminar = utilInformesMedidas.obtenerMedidaPorCodigo( medidas, 
								ConstantesMedidasGases.CODIGO_REV_RAL_CICLO_PRELIMINAR);
		Double revRalntiPreliminarVlr = revRalentiPreliminar.isPresent() ? revRalentiPreliminar.getAsDouble() : 0.0 ;
		informe.setRpmRalentiCicloPreliminar(df.format( revRalntiPreliminarVlr ));
		
		OptionalDouble revRalentiUno = utilInformesMedidas.obtenerMedidaPorCodigo( medidas, 
								ConstantesMedidasGases.CODIGO_REV_RAL_PRIMER_CICLO);
		Double revRalentiUnoVlr = revRalentiUno.isPresent() ? revRalentiUno.getAsDouble() : 0.0;
		informe.setRpmRalentiCicloUno( df.format( revRalentiUnoVlr ) );
		
		
		OptionalDouble revRalentiDos = utilInformesMedidas.obtenerMedidaPorCodigo( medidas,
								ConstantesMedidasGases.CODIGO_REV_RAL_SEGUNDO_CICLO);
		Double revRalentiDosVlr = revRalentiDos.isPresent() ? revRalentiDos.getAsDouble() : 0.0 ;
		informe.setRpmRalentiCicloDos( df.format( revRalentiDosVlr ));
		
		
		OptionalDouble revRalentiTres = utilInformesMedidas.obtenerMedidaPorCodigo( medidas, 
								ConstantesMedidasGases.CODIGO_REV_RAL_TERCER_CICLO);
		
		Double revRalentiTresVlr = revRalentiTres.isPresent() ? revRalentiTres.getAsDouble() : 0.0 ;
		informe.setRpmRalentiCicloTres( df.format( revRalentiTresVlr ));
		
		
		OptionalDouble revRalentiPromedio = utilInformesMedidas.obtenerMedidaPorCodigo( medidas, 
								ConstantesMedidasGases.CODIGO_VELOCIDAD_RALENTI_PROMEDIO);
		
		Double revRalentiPromedioVlr = revRalentiPromedio.isPresent() ? revRalentiPromedio.getAsDouble() : 0.0;
		informe.setRpmRalentiRegistrada( df.format( revRalentiPromedioVlr ) );
		
		
		
		OptionalDouble revCruceroPromedio = utilInformesMedidas.obtenerMedidaPorCodigo( medidas,
				                ConstantesMedidasGases.CODIGO_VELOCIDAD_GOBERNADA_PROMEDIO);
		Double revCruceroPromedioVlr = revCruceroPromedio.isPresent() ? revCruceroPromedio.getAsDouble() : 0.0 ;
		informe.setRpmGobernadaRegistrada( df.format( revCruceroPromedioVlr ) );
		
	}
	
	public void cargarInformacionMedidasDensidadHumo(InformeDieselRes0762 informe,List<Medida> medidas) {
		
		UtilInformesMedidas utilInformesMedidas = new UtilInformesMedidas();
//		public static final int CODIGO_DENSIDAD_HUMO_CICLO_PRELIMINAR = 8071;
//		public static final int CODIGO_DENSIDAD_HUMO_CICLO_UNO = 8072;
//		public static final int CODIGO_DENSIDAD_HUMO_CICLO_DOS = 8073;
//		public static final int CODIGO_DENSIDAD_HUMO_CICLO_TRES = 8074;
//		public static final int CODIGO_RESULTADO_DENSIDAD_HUMO = 8077;
		DecimalFormat df = new DecimalFormat("0.000");
		establecerSimboloDecimal(df);
		OptionalDouble densidadHumoCicloPreliminar = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_DENSIDAD_HUMO_CICLO_PRELIMINAR);
		Double desidadHumoPreliminarVlr = densidadHumoCicloPreliminar.isPresent() ? densidadHumoCicloPreliminar.getAsDouble() : 0.0;
		informe.setDensidadHumoCicloPreliminar(df.format(desidadHumoPreliminarVlr));
		
		OptionalDouble densidadHumoPrimerCiclo = utilInformesMedidas.obtenerMedidaPorCodigo( medidas, 
				                                 ConstantesMedidasGases.CODIGO_DENSIDAD_HUMO_CICLO_UNO);
		Double densidadHumoUno = densidadHumoPrimerCiclo.isPresent() ? densidadHumoPrimerCiclo.getAsDouble() : 0.0;
		informe.setDensidadHumoCicloUno( df.format( densidadHumoUno ));
		
		OptionalDouble densidadHumoSegundoCiclo = utilInformesMedidas.obtenerMedidaPorCodigo( medidas, 
                ConstantesMedidasGases.CODIGO_DENSIDAD_HUMO_CICLO_DOS);
		Double densidadHumoDos = densidadHumoSegundoCiclo.isPresent() ? densidadHumoSegundoCiclo.getAsDouble() : 0.0;
		informe.setDensidadHumoCicloDos( df.format( densidadHumoDos ));
		
		OptionalDouble densidadHumoTercerCiclo = utilInformesMedidas.obtenerMedidaPorCodigo( medidas, 
                ConstantesMedidasGases.CODIGO_DENSIDAD_HUMO_CICLO_TRES);
		Double densidadHumoTres = densidadHumoTercerCiclo.isPresent() ? densidadHumoTercerCiclo.getAsDouble() : 0.0;
		informe.setDensidadHumoCicloTres( df.format( densidadHumoTres ));
		
		OptionalDouble densidadHumoResultado = utilInformesMedidas.obtenerMedidaPorCodigo( medidas,
				ConstantesMedidasGases.CODIGO_RESULTADO_DENSIDAD_HUMO );
		Double densidadHumoVlr = densidadHumoResultado.isPresent() ? densidadHumoResultado.getAsDouble() : 0.0;
		informe.setResultadoFinalDensidadHumo(df.format(densidadHumoVlr));
		
	}

	public void cargarInformacionMedidasVehiculo(InformeDieselRes0762 informeDieselDTO, List<Medida> medidas,Vehiculo vehiculo) {
		UtilInformesMedidas utilInformesMedidas = new UtilInformesMedidas();

		OptionalDouble modificacionesMotor = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_MODIFICACIONES_MOTOR);
		String modificacionesMotorStr = "NO";
		if (modificacionesMotor.isPresent()) {
			modificacionesMotorStr = modificacionesMotor.getAsDouble() > 0.0 ? "SI" : "NO";
		} else {
			modificacionesMotorStr = "NO";
		}

		informeDieselDTO.setModificacionesMotor(modificacionesMotorStr);
		Integer diametroVehiculo = vehiculo.getDiametroExosto();
		if(diametroVehiculo == null) {
			diametroVehiculo = 0;
		}
		OptionalDouble diametroTubo = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_DIAMETRO_TUBO_ESCAPE);
		Double diametro = diametroTubo.isPresent() ? diametroTubo.getAsDouble() : diametroVehiculo;
		DecimalFormat df = new DecimalFormat("#");
		informeDieselDTO.setDiametroTuboEscape(df.format(diametro));

//		public static final int CODIGO_RPM_MIN_RAL_FABRICANTE = 8067;
//		public static final int CODIGO_RPM_MAX_RAL_FABRICANTE = 8068;
//		public static final int CODIGO_RPM_MIN_GOB_FABRICANTE = 8069;
//		public static final int CODIGO_RPM_MAX_GOB_FABRICANTE = 8070;

		OptionalDouble conoceRpmFabricante = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_CONOCE_RPM_FABRICANTE);
		String conoceRpmFabricanteStr = "NO";
		if (conoceRpmFabricante.isPresent()) {
			conoceRpmFabricanteStr = conoceRpmFabricante.getAsDouble() > 0.0 ? "SI" : "NO";
		} else {
			conoceRpmFabricanteStr = "NO";
		}

		informeDieselDTO.setConoceRpmFabricante(conoceRpmFabricanteStr);

		OptionalDouble rpmMinRalFabricante = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_RPM_MIN_RAL_FABRICANTE);
		Double rpmMinRalFabricanteVlr = rpmMinRalFabricante.isPresent() ? rpmMinRalFabricante.getAsDouble() : 0.0;
		informeDieselDTO.setRpmMinRalFabricante(df.format(rpmMinRalFabricanteVlr));

		OptionalDouble rpmMaxRalFabricante = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_RPM_MAX_RAL_FABRICANTE);
		Double rpmMaxRalFabricanteVlr = rpmMaxRalFabricante.isPresent() ? rpmMaxRalFabricante.getAsDouble() : 0.0;
		informeDieselDTO.setRpmMaxRalFabricante(df.format(rpmMaxRalFabricanteVlr));

		OptionalDouble rpmMinGobFabricante = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_RPM_MIN_GOB_FABRICANTE);
		Double rpmMinGobFabricanteVlr = rpmMinGobFabricante.isPresent() ? rpmMinGobFabricante.getAsDouble() : 0.0;
		informeDieselDTO.setRpmMinGobFabricante(df.format(rpmMinGobFabricanteVlr));

		OptionalDouble rpmMaxGobFabricante = utilInformesMedidas.obtenerMedidaPorCodigo(medidas,
				ConstantesMedidasGases.CODIGO_RPM_MAX_GOB_FABRICANTE);
		Double rpmMaxGobFabricanteVlr = rpmMaxGobFabricante.isPresent() ? rpmMaxGobFabricante.getAsDouble() : 0.0;
		informeDieselDTO.setRpmMaxGobFabricante(df.format(rpmMaxGobFabricanteVlr));

	}

	public void agregarInformacionEquipos(List<Equipo> equipos, InformeDieselRes0762 informe) {

		Optional<Equipo> termohigrometroOpt = equipos.stream()
				.filter(e -> e.getTipoEquipo().getTipoEquipoId().equals(2)).findAny();
		if (termohigrometroOpt.isPresent()) {

			Equipo termohigrometro = termohigrometroOpt.get();
			informe.setSerialSensorTemperaturaAmbiente(termohigrometro.getSerieEquipo());
			informe.setMarcaSensorTemperaturaAmbiente(termohigrometro.getFabricante());
			informe.setSerialSensorHumedad(termohigrometro.getSerieEquipo());
			informe.setMarcaSensorHumedad(termohigrometro.getFabricante());

		}

		Optional<Equipo> sondaTemperaturaOpt = equipos.stream()
				.filter(e -> e.getTipoEquipo().getTipoEquipoId().equals(3)).findAny();
		if (sondaTemperaturaOpt.isPresent()) {
			Equipo sonda = sondaTemperaturaOpt.get();
			informe.setMarcaSensorTemperaturaMotor(sonda.getFabricante());
			informe.setSerialSensorTemperaturaMotor(sonda.getSerieEquipo());

		}

		Optional<Equipo> tacometroOpt = equipos.stream().filter(e -> e.getTipoEquipo().getTipoEquipoId().equals(4))
				.findAny();
		if (tacometroOpt.isPresent()) {
			Equipo tacometro = tacometroOpt.get();
			informe.setMarcaCaptadorRpm(tacometro.getFabricante());
			informe.setSerialCaptadorRpm(tacometro.getSerieEquipo());
		}

	}

	public void agregarInformacionRevision(InformeDieselRes0762 informe, Revision revision, String fechaFur,
			Usuario jefeTecnico) {

		String numeroFur = new StringBuilder().append(revision.getRevisionId()).append("-").append(revision.getNumeroInspecciones()).toString();
		informe.setNumeroFUR(numeroFur);
		informe.setNumeroConsecutivoRunt(revision.getConsecutivoRunt() != null ? revision.getConsecutivoRunt() : " ");

		if (revision.getNumeroInspecciones() > 1) {

			StringBuilder sb = new StringBuilder().append(revision.getRevisionId()).append("- 1").append(",");
			sb.append(revision.getRevisionId()).append(" -2");
			String furAsociados = sb.toString();
			informe.setFurAsociados(furAsociados);

		} else {

			String furAsociados = new StringBuilder().append(revision.getRevisionId()).append("-").append(1).toString();
			informe.setFurAsociados(furAsociados);
		}

		if (jefeTecnico != null) {
			informe.setTipoIdentificacionDirectorTecnico(jefeTecnico.getTipoIdentificacion());
			informe.setNumeroIdentificacionDirectorTecnico(String.valueOf(jefeTecnico.getCedula()));
			informe.setNombreDirectorTecnico(jefeTecnico.getNombres() + " " + jefeTecnico.getApellidos());
		}
		
		informe.setFechaFUR(fechaFur);

	}

	public void agregarInformacionPrueba(InformeDieselRes0762 informe, Prueba prueba) {

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		informe.setFechaHoraInicioInspeccion(sdf.format(prueba.getFechaInicio()));
		informe.setFechaHoraFinInspeccion(sdf.format(prueba.getFechaFinalizacion()));
		informe.setFechaHoraAbortoInspeccion(
				prueba.getFechaAborto() != null ? sdf.format(prueba.getFechaAborto()) : "");

		if (prueba.getCausaAborto() != null) {
			informe.setCausaAbortoInspeccion(String.valueOf(prueba.getCausaAborto().getCausaAbortoId()));
		}

//		public String tipoIdentificacionInspectorPrueba;
//		public String numeroIdentificacionInspectorPrueba;
//		public String nombreInspectorPrueba;

		Usuario usuario = prueba.getUsuario();
		if (usuario != null) {

			informe.setTipoIdentificacionInspectorPrueba(usuario.getTipoIdentificacion());
			informe.setNumeroIdentificacionInspectorPrueba(String.valueOf(usuario.getCedula()));
			informe.setNombreInspectorPrueba(usuario.getNombres() + " " + usuario.getApellidos());

		}

	}

	public void agregarInformacionSoftware(InformeDieselRes0762 informe, String nombreSoftware,
			String versionSoftware) {

		informe.setNombreSoftware(nombreSoftware);
		informe.setVersionSoftware(versionSoftware);
		informe.setNombreProveedorSoftware("Proambiente s.a.s.");

	}
	
	public void establecerSimboloDecimal(DecimalFormat df) {
		DecimalFormatSymbols ds = DecimalFormatSymbols.getInstance();
		if(separador.equals(SeparadorDECIMAL.PUNTO)) {
			ds.setDecimalSeparator(DECIMAL_SEPARATOR_PUNTO);
		}else {
			ds.setDecimalSeparator(DECIMAL_SEPARATOR_COMA);
		}
		df.setDecimalFormatSymbols(ds);
	}

	public void ponerInformacionVerificacionLinealidad(InformeDieselRes0762 informe,
			VerificacionLinealidad verificacion, Usuario inspector) {

		informe.setTipoIdentificacionInspectorVerificacion(inspector.getTipoIdentificacion());
		informe.setNumeroIdentificacionInspectorVerificacion(String.valueOf(inspector.getCedula()));
		String nombres = inspector.getNombres() + " " + inspector.getApellidos();
		informe.setNombreInspectorVerificacion(nombres);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		informe.setFechaVerificacionLinealidad(sdf.format(verificacion.getFechaVerificacion()));

		FiltroDensidadNeutra filtroMenor = verificacion.getFiltroUno();
		informe.setLaboratorioFiltroMenor(filtroMenor.getCalibradoPor());
		informe.setSerialFiltroMenor(filtroMenor.getSerie());
		informe.setCertificadoFiltroMenor(filtroMenor.getNumeroCertificado());
		informe.setValorFiltroMenor(filtroMenor.getValor().toString());

		DecimalFormat df = new DecimalFormat("0.0");
        establecerSimboloDecimal(df);
		FiltroDensidadNeutra filtroMayor = verificacion.getFiltroDos();
		informe.setCertificadoFiltroMayor(filtroMayor.getCalibradoPor());
		informe.setSerialFiltroMayor(filtroMayor.getSerie());
		informe.setCertificadoFiltroMayor(filtroMayor.getNumeroCertificado());

		informe.setValorFiltroMayor(df.format(filtroMayor.getValor().doubleValue()));
		informe.setResultadoVerificacionPuntoCero(df.format(verificacion.getResutadoFiltro1()));
		informe.setResultadoVerificacionPuntoCien(df.format(verificacion.getResutadoFiltro4()));
		informe.setResultadoVerificacionFiltroMenor(df.format(verificacion.getResultadoFiltro2()));
		informe.setResultadoVerificacionFiltroMayor(df.format(verificacion.getResultadoFiltro3()));
		informe.setResultadoVerificacionLinealidad(verificacion.isAprobada() ? "APROBADA" : "REPROBADA");

	}

	public void agregarInformacionAnalizador(InformeDieselRes0762 informe, Analizador analizador) {

		informe.setMarcaOpacimero(analizador.getMarcaAnalizadorGases());
		informe.setModeloOpacimetro(analizador.getModelo());
		informe.setSerialOpacimetro(analizador.getSerialAnalizador());

		informe.setMarcaBancoGases(analizador.getMarca());
		informe.setModeloBancoGases(analizador.getModeloBancoGases());
		informe.setSerialBancoGases(analizador.getSerialBancoGases());

		informe.setLtoeOpacimetro(analizador.getLtoe() == null ? "0" : analizador.getLtoe().toString());

		informe.setSerialElectronicoOpacimetro(analizador.getSerial());

	}
}
