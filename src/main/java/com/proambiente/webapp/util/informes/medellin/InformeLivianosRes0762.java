package com.proambiente.webapp.util.informes.medellin;

public class InformeLivianosRes0762 {
	
	private String numeroExpedienteCda;
	private String tipoIdentificacionCda;
	private String numeroIdentificacionCda;
	private String nombreRazonSocial;
	private String nombreCdaEstablecimientoComercio;
	private String claseCda;
	private String direccionCda;
	private String correoElectronicoCda;
	private String telefonoCda;
	private String telefonoSecundarioCda;
	private String municipioCda;
	private String resolucionAutoridadAmbiental;
	private String fechaResolucionVigente;
	private String totalEquiposAnalizadoresGases;
	private String tipoIdentificacionPropietario;
	private String numeroIdentificacionPropietario;
	private String nombresApellidosPropietario;
	private String direccionPropietario;
	private String telefonoPropietario;
	private String telefonoSecundarioPropietario;
	private String municipioPropietario;
	private String correoElectronicoPropietario;
	private String placa;
	private String modelo;
	private String numeroMotor;
	private String vin;
	private String cilindraje;
	private String licenciaTransito;
	private String kilometraje;
	private String conversionGNV;
	private String fechaConversionGNV;
	private String marca;
	private String linea;
	private String clase;
	private String servicio;
	private String combustible;
	private String tipoMotor;
	private String conoceRpmFabricante;
	private String rpmMinRalFabricante;
	private String rpmMaxRalFabricante;
	private String numeroTubosEscape;
	private String marcaAnalizadorGases;
	private String modeloAnalizadorGases;
	private String serialAnalizadorGases;
	private String marcaBancoGases;
	private String modeloBancoGases;
	private String serialBancoGases;
	private String pefBancoGases;
	private String serialElectronicoAnalizadorGases;
	private String marcaCaptadorRpm;
	private String serialCaptadorRpm;
	private String marcaSensorTemperaturaMotor;
	private String serialSensorTemperaturaMotor;
	private String marcaSensorTemperaturaAmbiente;
	private String serialSensorTemperaturaAmbiente;
	private String marcaSensorHumedadRelativa;
	private String serialSensorHumedadRelativa;
	private String nombreSoftware;
	private String versionSoftware;
	private String nombreProveedorSoftware;
	private String tipoIdentificacionInspectorVerificacionGasolina;
	private String numeroIdentificacionInspectorVerificacionGasolina;
	private String nombreInspectorVerificacionGasolina;
	private String fechaHoraPruebaFugas;
	private String fechaHoraVerificacionGasolina;
	private String laboratorioEmiteCertificadoAlta;
	private String cilindroGasAlta;
	private String certificadoGasAlta;
	private String laboratorioEmiteCertificadoGasBaja;
	private String cilindroGasBaja;
	private String certificadoGasBaja;
	private String patronHcAltaPropano;
	private String patronHcAltaHexano;
	private String patronHcBajaPropano;
	private String patronHcBajaHexano;
	private String patronCoAlta;
	private String patronCoBaja;
	private String patronCo2Alta;
	private String patronCo2Baja;
	private String patronO2Alta;
	private String patronO2Baja;
	private String resultadoHcAltaPropano;
	private String resultadoHcAltaHexano;
	private String resultadoHcBajaPropano;
	private String resultadoHcBajaHexano;
	private String resultadoCoAlto;
	private String resultadoCoBajo;
	private String resultadoCo2Alto;
	private String resultadoCo2Bajo;
	private String resultadoO2Alto;
	private String resultadoO2Bajo;
	private String criterioVerificacionGas;
	private String resultadoVerificacionGas;
	private String tipoIdentificacionDirectorTecnico;
	private String numeroIdentificacionDirectorTecnico;
	private String nombreDirectorTecnico;
	private String tipoIdentificacionInspectorPrueba;
	private String numeroIdentificacionInspectorPrueba;
	private String nombreInspectorPrueba;
	private String numeroFUR;
	private String fechaFUR;
	private String numeroConsecutivoRunt;
	private String furAsociados;
	private String certificadoEmitido;
	private String fechaHoraInicioInspeccion;
	private String fechaHoraFinInspeccion;
	private String fechaHoraAbortoInspeccion;
	private String causaAbortoInspeccion;
	private String vehiculoConCatalizador;
	private String lugarTomaTemperaturaVehiculo;
	private String temperaturaAmbiente;
	private String humedadRelativa;
	private String temperaturaMotor;
	private String rpmRalenti;
	private String rpmCrucero;
	private String presenciaHumoNegroAzul;
	private String presenciaDilucion;
	private String rpmFueraRango;
	private String fugaTuboEscapes;
	private String salidasAdicionales;
	private String ausenciaFugasTapaAceite;
	private String ausenciaFugasTapaCombustible;
	private String sistemaAdmisionAire;
	private String desconexionPCV;
	private String instalacionAccesoriosTubo;
	private String incorrectaOperacionRefrigeracion;
	private String resultadoHcRalenti;
	private String resultadoHcCrucero;
	private String resultadoCoRalenti;
	private String resultadoCoCrucero;
	private String resultadoCo2Ralenti;
	private String resultadoCo2Crucero;
	private String resultadoO2Ralenti;
	private String resultadoO2Crucero;
	private String incumplimientoNivelesEmisiones;
	private String resultadoFinalPrueba;
	public String getNumeroExpedienteCda() {
		return numeroExpedienteCda;
	}
	public void setNumeroExpedienteCda(String numeroExpedienteCda) {
		this.numeroExpedienteCda = numeroExpedienteCda;
	}
	public String getTipoIdentificacionCda() {
		return tipoIdentificacionCda;
	}
	public void setTipoIdentificacionCda(String tipoIdentificacionCda) {
		this.tipoIdentificacionCda = tipoIdentificacionCda;
	}
	public String getNumeroIdentificacionCda() {
		return numeroIdentificacionCda;
	}
	public void setNumeroIdentificacionCda(String numeroIdentificacionCda) {
		this.numeroIdentificacionCda = numeroIdentificacionCda;
	}
	public String getNombreRazonSocial() {
		return nombreRazonSocial;
	}
	public void setNombreRazonSocial(String nombreRazonSocial) {
		this.nombreRazonSocial = nombreRazonSocial;
	}
	public String getNombreCdaEstablecimientoComercio() {
		return nombreCdaEstablecimientoComercio;
	}
	public void setNombreCdaEstablecimientoComercio(String nombreCdaEstablecimientoComercio) {
		this.nombreCdaEstablecimientoComercio = nombreCdaEstablecimientoComercio;
	}
	public String getClaseCda() {
		return claseCda;
	}
	public void setClaseCda(String claseCda) {
		this.claseCda = claseCda;
	}
	public String getDireccionCda() {
		return direccionCda;
	}
	public void setDireccionCda(String direccionCda) {
		this.direccionCda = direccionCda;
	}
	public String getCorreoElectronicoCda() {
		return correoElectronicoCda;
	}
	public void setCorreoElectronicoCda(String correoElectronicoCda) {
		this.correoElectronicoCda = correoElectronicoCda;
	}
	public String getTelefonoCda() {
		return telefonoCda;
	}
	public void setTelefonoCda(String telefonoCda) {
		this.telefonoCda = telefonoCda;
	}
	public String getTelefonoSecundarioCda() {
		return telefonoSecundarioCda;
	}
	public void setTelefonoSecundarioCda(String telefonoSecundarioCda) {
		this.telefonoSecundarioCda = telefonoSecundarioCda;
	}
	public String getMunicipioCda() {
		return municipioCda;
	}
	public void setMunicipioCda(String municipioCda) {
		this.municipioCda = municipioCda;
	}
	public String getResolucionAutoridadAmbiental() {
		return resolucionAutoridadAmbiental;
	}
	public void setResolucionAutoridadAmbiental(String resolucionAutoridadAmbiental) {
		this.resolucionAutoridadAmbiental = resolucionAutoridadAmbiental;
	}
	public String getFechaResolucionVigente() {
		return fechaResolucionVigente;
	}
	public void setFechaResolucionVigente(String fechaResolucionVigente) {
		this.fechaResolucionVigente = fechaResolucionVigente;
	}
	public String getTotalEquiposAnalizadoresGases() {
		return totalEquiposAnalizadoresGases;
	}
	public void setTotalEquiposAnalizadoresGases(String totalEquiposAnalizadoresGases) {
		this.totalEquiposAnalizadoresGases = totalEquiposAnalizadoresGases;
	}
	public String getTipoIdentificacionPropietario() {
		return tipoIdentificacionPropietario;
	}
	public void setTipoIdentificacionPropietario(String tipoIdentificacionPropietario) {
		this.tipoIdentificacionPropietario = tipoIdentificacionPropietario;
	}
	public String getNumeroIdentificacionPropietario() {
		return numeroIdentificacionPropietario;
	}
	public void setNumeroIdentificacionPropietario(String numeroIdentificacionPropietario) {
		this.numeroIdentificacionPropietario = numeroIdentificacionPropietario;
	}
	public String getNombresApellidosPropietario() {
		return nombresApellidosPropietario;
	}
	public void setNombresApellidosPropietario(String nombresApellidosPropietario) {
		this.nombresApellidosPropietario = nombresApellidosPropietario;
	}
	public String getDireccionPropietario() {
		return direccionPropietario;
	}
	public void setDireccionPropietario(String direccionPropietario) {
		this.direccionPropietario = direccionPropietario;
	}
	public String getTelefonoPropietario() {
		return telefonoPropietario;
	}
	public void setTelefonoPropietario(String telefonoPropietario) {
		this.telefonoPropietario = telefonoPropietario;
	}
	public String getTelefonoSecundarioPropietario() {
		return telefonoSecundarioPropietario;
	}
	public void setTelefonoSecundarioPropietario(String telefonoSecundarioPropietario) {
		this.telefonoSecundarioPropietario = telefonoSecundarioPropietario;
	}
	public String getMunicipioPropietario() {
		return municipioPropietario;
	}
	public void setMunicipioPropietario(String municipioPropietario) {
		this.municipioPropietario = municipioPropietario;
	}
	public String getCorreoElectronicoPropietario() {
		return correoElectronicoPropietario;
	}
	public void setCorreoElectronicoPropietario(String correoElectronicoPropietario) {
		this.correoElectronicoPropietario = correoElectronicoPropietario;
	}
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getNumeroMotor() {
		return numeroMotor;
	}
	public void setNumeroMotor(String numeroMotor) {
		this.numeroMotor = numeroMotor;
	}
	public String getVin() {
		return vin;
	}
	public void setVin(String vin) {
		this.vin = vin;
	}
	public String getCilindraje() {
		return cilindraje;
	}
	public void setCilindraje(String cilindraje) {
		this.cilindraje = cilindraje;
	}
	public String getLicenciaTransito() {
		return licenciaTransito;
	}
	public void setLicenciaTransito(String licenciaTransito) {
		this.licenciaTransito = licenciaTransito;
	}
	public String getKilometraje() {
		return kilometraje;
	}
	public void setKilometraje(String kilometraje) {
		this.kilometraje = kilometraje;
	}
	public String getConversionGNV() {
		return conversionGNV;
	}
	public void setConversionGNV(String conversionGNV) {
		this.conversionGNV = conversionGNV;
	}
	public String getFechaConversionGNV() {
		return fechaConversionGNV;
	}
	public void setFechaConversionGNV(String fechaConversionGNV) {
		this.fechaConversionGNV = fechaConversionGNV;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getLinea() {
		return linea;
	}
	public void setLinea(String linea) {
		this.linea = linea;
	}
	
	public String getClase() {
		return clase;
	}
	public void setClase(String clase) {
		this.clase = clase;
	}
	public String getServicio() {
		return servicio;
	}
	public void setServicio(String servicio) {
		this.servicio = servicio;
	}
	public String getCombustible() {
		return combustible;
	}
	public void setCombustible(String combustible) {
		this.combustible = combustible;
	}
	public String getTipoMotor() {
		return tipoMotor;
	}
	public void setTipoMotor(String tipoMotor) {
		this.tipoMotor = tipoMotor;
	}
	public String getConoceRpmFabricante() {
		return conoceRpmFabricante;
	}
	public void setConoceRpmFabricante(String conoceRpmFabricante) {
		this.conoceRpmFabricante = conoceRpmFabricante;
	}
	public String getRpmMinRalFabricante() {
		return rpmMinRalFabricante;
	}
	public void setRpmMinRalFabricante(String rpmMinRalFabricante) {
		this.rpmMinRalFabricante = rpmMinRalFabricante;
	}
	public String getRpmMaxRalFabricante() {
		return rpmMaxRalFabricante;
	}
	public void setRpmMaxRalFabricante(String rpmMaxRalFabricante) {
		this.rpmMaxRalFabricante = rpmMaxRalFabricante;
	}
	public String getNumeroTubosEscape() {
		return numeroTubosEscape;
	}
	public void setNumeroTubosEscape(String numeroTubosEscape) {
		this.numeroTubosEscape = numeroTubosEscape;
	}
	public String getMarcaAnalizadorGases() {
		return marcaAnalizadorGases;
	}
	public void setMarcaAnalizadorGases(String marcaAnalizadorGases) {
		this.marcaAnalizadorGases = marcaAnalizadorGases;
	}
	public String getModeloAnalizadorGases() {
		return modeloAnalizadorGases;
	}
	public void setModeloAnalizadorGases(String modeloAnalizadorGases) {
		this.modeloAnalizadorGases = modeloAnalizadorGases;
	}
	public String getSerialAnalizadorGases() {
		return serialAnalizadorGases;
	}
	public void setSerialAnalizadorGases(String serialAnalizadorGases) {
		this.serialAnalizadorGases = serialAnalizadorGases;
	}
	public String getMarcaBancoGases() {
		return marcaBancoGases;
	}
	public void setMarcaBancoGases(String marcaBancoGases) {
		this.marcaBancoGases = marcaBancoGases;
	}
	public String getModeloBancoGases() {
		return modeloBancoGases;
	}
	public void setModeloBancoGases(String modeloBancoGases) {
		this.modeloBancoGases = modeloBancoGases;
	}
	public String getSerialBancoGases() {
		return serialBancoGases;
	}
	public void setSerialBancoGases(String serialBancoGases) {
		this.serialBancoGases = serialBancoGases;
	}
	public String getPefBancoGases() {
		return pefBancoGases;
	}
	public void setPefBancoGases(String pefBancoGases) {
		this.pefBancoGases = pefBancoGases;
	}
	public String getSerialElectronicoAnalizadorGases() {
		return serialElectronicoAnalizadorGases;
	}
	public void setSerialElectronicoAnalizadorGases(String serialElectronicoAnalizadorGases) {
		this.serialElectronicoAnalizadorGases = serialElectronicoAnalizadorGases;
	}
	public String getMarcaCaptadorRpm() {
		return marcaCaptadorRpm;
	}
	public void setMarcaCaptadorRpm(String marcaCaptadorRpm) {
		this.marcaCaptadorRpm = marcaCaptadorRpm;
	}
	public String getSerialCaptadorRpm() {
		return serialCaptadorRpm;
	}
	public void setSerialCaptadorRpm(String serialCaptadorRpm) {
		this.serialCaptadorRpm = serialCaptadorRpm;
	}
	public String getMarcaSensorTemperaturaMotor() {
		return marcaSensorTemperaturaMotor;
	}
	public void setMarcaSensorTemperaturaMotor(String marcaSensorTemperaturaMotor) {
		this.marcaSensorTemperaturaMotor = marcaSensorTemperaturaMotor;
	}
	public String getSerialSensorTemperaturaMotor() {
		return serialSensorTemperaturaMotor;
	}
	public void setSerialSensorTemperaturaMotor(String serialSensorTemperaturaMotor) {
		this.serialSensorTemperaturaMotor = serialSensorTemperaturaMotor;
	}
	public String getMarcaSensorTemperaturaAmbiente() {
		return marcaSensorTemperaturaAmbiente;
	}
	public void setMarcaSensorTemperaturaAmbiente(String marcaSensorTemperaturaAmbiente) {
		this.marcaSensorTemperaturaAmbiente = marcaSensorTemperaturaAmbiente;
	}
	public String getSerialSensorTemperaturaAmbiente() {
		return serialSensorTemperaturaAmbiente;
	}
	public void setSerialSensorTemperaturaAmbiente(String serialSensorTemperaturaAmbiente) {
		this.serialSensorTemperaturaAmbiente = serialSensorTemperaturaAmbiente;
	}
	public String getMarcaSensorHumedadRelativa() {
		return marcaSensorHumedadRelativa;
	}
	public void setMarcaSensorHumedadRelativa(String marcaSensorHumedadRelativa) {
		this.marcaSensorHumedadRelativa = marcaSensorHumedadRelativa;
	}
	public String getSerialSensorHumedadRelativa() {
		return serialSensorHumedadRelativa;
	}
	public void setSerialSensorHumedadRelativa(String serialSensorHumedadRelativa) {
		this.serialSensorHumedadRelativa = serialSensorHumedadRelativa;
	}
	public String getNombreSoftware() {
		return nombreSoftware;
	}
	public void setNombreSoftware(String nombreSoftware) {
		this.nombreSoftware = nombreSoftware;
	}
	public String getVersionSoftware() {
		return versionSoftware;
	}
	public void setVersionSoftware(String versionSoftware) {
		this.versionSoftware = versionSoftware;
	}
	public String getNombreProveedorSoftware() {
		return nombreProveedorSoftware;
	}
	public void setNombreProveedorSoftware(String nombreProveedorSoftware) {
		this.nombreProveedorSoftware = nombreProveedorSoftware;
	}
	public String getTipoIdentificacionInspectorVerificacionGasolina() {
		return tipoIdentificacionInspectorVerificacionGasolina;
	}
	public void setTipoIdentificacionInspectorVerificacionGasolina(String tipoIdentificacionInspectorVerificacionGasolina) {
		this.tipoIdentificacionInspectorVerificacionGasolina = tipoIdentificacionInspectorVerificacionGasolina;
	}
	public String getNumeroIdentificacionInspectorVerificacionGasolina() {
		return numeroIdentificacionInspectorVerificacionGasolina;
	}
	public void setNumeroIdentificacionInspectorVerificacionGasolina(
			String numeroIdentificacionInspectorVerificacionGasolina) {
		this.numeroIdentificacionInspectorVerificacionGasolina = numeroIdentificacionInspectorVerificacionGasolina;
	}
	public String getNombreInspectorVerificacionGasolina() {
		return nombreInspectorVerificacionGasolina;
	}
	public void setNombreInspectorVerificacionGasolina(String nombreInspectorVerificacionGasolina) {
		this.nombreInspectorVerificacionGasolina = nombreInspectorVerificacionGasolina;
	}
	public String getFechaHoraPruebaFugas() {
		return fechaHoraPruebaFugas;
	}
	public void setFechaHoraPruebaFugas(String fechaHoraPruebaFugas) {
		this.fechaHoraPruebaFugas = fechaHoraPruebaFugas;
	}
	public String getFechaHoraVerificacionGasolina() {
		return fechaHoraVerificacionGasolina;
	}
	public void setFechaHoraVerificacionGasolina(String fechaHoraVerificacionGasolina) {
		this.fechaHoraVerificacionGasolina = fechaHoraVerificacionGasolina;
	}
	public String getLaboratorioEmiteCertificadoAlta() {
		return laboratorioEmiteCertificadoAlta;
	}
	public void setLaboratorioEmiteCertificadoAlta(String laboratorioEmiteCertificadoAlta) {
		this.laboratorioEmiteCertificadoAlta = laboratorioEmiteCertificadoAlta;
	}
	public String getCilindroGasAlta() {
		return cilindroGasAlta;
	}
	public void setCilindroGasAlta(String cilindroGasAlta) {
		this.cilindroGasAlta = cilindroGasAlta;
	}
	public String getCertificadoGasAlta() {
		return certificadoGasAlta;
	}
	public void setCertificadoGasAlta(String certificadoGasAlta) {
		this.certificadoGasAlta = certificadoGasAlta;
	}
	public String getLaboratorioEmiteCertificadoGasBaja() {
		return laboratorioEmiteCertificadoGasBaja;
	}
	public void setLaboratorioEmiteCertificadoGasBaja(String laboratorioEmiteCertificadoGasBaja) {
		this.laboratorioEmiteCertificadoGasBaja = laboratorioEmiteCertificadoGasBaja;
	}
	public String getCilindroGasBaja() {
		return cilindroGasBaja;
	}
	public void setCilindroGasBaja(String cilindroGasBaja) {
		this.cilindroGasBaja = cilindroGasBaja;
	}
	public String getCertificadoGasBaja() {
		return certificadoGasBaja;
	}
	public void setCertificadoGasBaja(String certificadoGasBaja) {
		this.certificadoGasBaja = certificadoGasBaja;
	}
	
	public String getPatronHcAltaPropano() {
		return patronHcAltaPropano;
	}
	public void setPatronHcAltaPropano(String patronHcAltaPropano) {
		this.patronHcAltaPropano = patronHcAltaPropano;
	}
	public String getPatronHcAltaHexano() {
		return patronHcAltaHexano;
	}
	public void setPatronHcAltaHexano(String patronHcAltaHexano) {
		this.patronHcAltaHexano = patronHcAltaHexano;
	}
	public String getPatronHcBajaPropano() {
		return patronHcBajaPropano;
	}
	public void setPatronHcBajaPropano(String patronHcBajaPropano) {
		this.patronHcBajaPropano = patronHcBajaPropano;
	}
	public String getPatronHcBajaHexano() {
		return patronHcBajaHexano;
	}
	public void setPatronHcBajaHexano(String patronHcBajaHexano) {
		this.patronHcBajaHexano = patronHcBajaHexano;
	}
	public String getPatronCoAlta() {
		return patronCoAlta;
	}
	public void setPatronCoAlta(String patronCoAlta) {
		this.patronCoAlta = patronCoAlta;
	}
	public String getPatronCoBaja() {
		return patronCoBaja;
	}
	public void setPatronCoBaja(String patronCoBaja) {
		this.patronCoBaja = patronCoBaja;
	}
	public String getPatronCo2Alta() {
		return patronCo2Alta;
	}
	public void setPatronCo2Alta(String patronCo2Alta) {
		this.patronCo2Alta = patronCo2Alta;
	}
	public String getPatronCo2Baja() {
		return patronCo2Baja;
	}
	public void setPatronCo2Baja(String patronCo2Baja) {
		this.patronCo2Baja = patronCo2Baja;
	}
	public String getPatronO2Alta() {
		return patronO2Alta;
	}
	public void setPatronO2Alta(String patronO2Alta) {
		this.patronO2Alta = patronO2Alta;
	}
	public String getPatronO2Baja() {
		return patronO2Baja;
	}
	public void setPatronO2Baja(String patronO2Baja) {
		this.patronO2Baja = patronO2Baja;
	}
	public String getResultadoHcAltaPropano() {
		return resultadoHcAltaPropano;
	}
	public void setResultadoHcAltaPropano(String resultadoHcAltaPropano) {
		this.resultadoHcAltaPropano = resultadoHcAltaPropano;
	}
	public String getResultadoHcAltaHexano() {
		return resultadoHcAltaHexano;
	}
	public void setResultadoHcAltaHexano(String resultadoHcAltaHexano) {
		this.resultadoHcAltaHexano = resultadoHcAltaHexano;
	}
	public String getResultadoHcBajaPropano() {
		return resultadoHcBajaPropano;
	}
	public void setResultadoHcBajaPropano(String resultadoHcBajaPropano) {
		this.resultadoHcBajaPropano = resultadoHcBajaPropano;
	}
	public String getResultadoHcBajaHexano() {
		return resultadoHcBajaHexano;
	}
	public void setResultadoHcBajaHexano(String resultadoHcBajaHexano) {
		this.resultadoHcBajaHexano = resultadoHcBajaHexano;
	}
	public String getResultadoCoAlto() {
		return resultadoCoAlto;
	}
	public void setResultadoCoAlto(String resultadoCoAlto) {
		this.resultadoCoAlto = resultadoCoAlto;
	}
	public String getResultadoCoBajo() {
		return resultadoCoBajo;
	}
	public void setResultadoCoBajo(String resultadoCoBajo) {
		this.resultadoCoBajo = resultadoCoBajo;
	}
	public String getResultadoCo2Alto() {
		return resultadoCo2Alto;
	}
	public void setResultadoCo2Alto(String resultadoCo2Alto) {
		this.resultadoCo2Alto = resultadoCo2Alto;
	}
	public String getResultadoCo2Bajo() {
		return resultadoCo2Bajo;
	}
	public void setResultadoCo2Bajo(String resultadoCo2Bajo) {
		this.resultadoCo2Bajo = resultadoCo2Bajo;
	}
	public String getResultadoO2Alto() {
		return resultadoO2Alto;
	}
	public void setResultadoO2Alto(String resultadoO2Alto) {
		this.resultadoO2Alto = resultadoO2Alto;
	}
	public String getResultadoO2Bajo() {
		return resultadoO2Bajo;
	}
	public void setResultadoO2Bajo(String resultadoO2Bajo) {
		this.resultadoO2Bajo = resultadoO2Bajo;
	}
	public String getCriterioVerificacionGas() {
		return criterioVerificacionGas;
	}
	public void setCriterioVerificacionGas(String criterioVerificacionGas) {
		this.criterioVerificacionGas = criterioVerificacionGas;
	}
	public String getResultadoVerificacionGas() {
		return resultadoVerificacionGas;
	}
	public void setResultadoVerificacionGas(String resultadoVerificacionGas) {
		this.resultadoVerificacionGas = resultadoVerificacionGas;
	}
	
	public String getNombreDirectorTecnico() {
		return nombreDirectorTecnico;
	}
	public void setNombreDirectorTecnico(String nombreDirectorTecnico) {
		this.nombreDirectorTecnico = nombreDirectorTecnico;
	}
	
	
	public String getTipoIdentificacionDirectorTecnico() {
		return tipoIdentificacionDirectorTecnico;
	}
	public void setTipoIdentificacionDirectorTecnico(String tipoIdentificacionDirectorTecnico) {
		this.tipoIdentificacionDirectorTecnico = tipoIdentificacionDirectorTecnico;
	}
	public String getNumeroIdentificacionDirectorTecnico() {
		return numeroIdentificacionDirectorTecnico;
	}
	public void setNumeroIdentificacionDirectorTecnico(String numeroIdentificacionDirectorTecnico) {
		this.numeroIdentificacionDirectorTecnico = numeroIdentificacionDirectorTecnico;
	}
	public String getTipoIdentificacionInspectorPrueba() {
		return tipoIdentificacionInspectorPrueba;
	}
	public void setTipoIdentificacionInspectorPrueba(String tipoIdentificacionInspectorPrueba) {
		this.tipoIdentificacionInspectorPrueba = tipoIdentificacionInspectorPrueba;
	}
	public String getNumeroIdentificacionInspectorPrueba() {
		return numeroIdentificacionInspectorPrueba;
	}
	public void setNumeroIdentificacionInspectorPrueba(String numeroIdentificacionInspectorPrueba) {
		this.numeroIdentificacionInspectorPrueba = numeroIdentificacionInspectorPrueba;
	}
	public String getNombreInspectorPrueba() {
		return nombreInspectorPrueba;
	}
	public void setNombreInspectorPrueba(String nombreInspectorPrueba) {
		this.nombreInspectorPrueba = nombreInspectorPrueba;
	}
	public String getNumeroFUR() {
		return numeroFUR;
	}
	public void setNumeroFUR(String numeroFUR) {
		this.numeroFUR = numeroFUR;
	}
	public String getFechaFUR() {
		return fechaFUR;
	}
	public void setFechaFUR(String fechaFUR) {
		this.fechaFUR = fechaFUR;
	}
	public String getNumeroConsecutivoRunt() {
		return numeroConsecutivoRunt;
	}
	public void setNumeroConsecutivoRunt(String numeroConsecutivoRunt) {
		this.numeroConsecutivoRunt = numeroConsecutivoRunt;
	}
	public String getFurAsociados() {
		return furAsociados;
	}
	public void setFurAsociados(String furAsociados) {
		this.furAsociados = furAsociados;
	}
	public String getCertificadoEmitido() {
		return certificadoEmitido;
	}
	public void setCertificadoEmitido(String certificadoEmitido) {
		this.certificadoEmitido = certificadoEmitido;
	}
	public String getFechaHoraInicioInspeccion() {
		return fechaHoraInicioInspeccion;
	}
	public void setFechaHoraInicioInspeccion(String fechaHoraInicioInspeccion) {
		this.fechaHoraInicioInspeccion = fechaHoraInicioInspeccion;
	}
	public String getFechaHoraFinInspeccion() {
		return fechaHoraFinInspeccion;
	}
	public void setFechaHoraFinInspeccion(String fechaHoraFinInspeccion) {
		this.fechaHoraFinInspeccion = fechaHoraFinInspeccion;
	}
	public String getFechaHoraAbortoInspeccion() {
		return fechaHoraAbortoInspeccion;
	}
	public void setFechaHoraAbortoInspeccion(String fechaHoraAbortoInspeccion) {
		this.fechaHoraAbortoInspeccion = fechaHoraAbortoInspeccion;
	}
	public String getCausaAbortoInspeccion() {
		return causaAbortoInspeccion;
	}
	public void setCausaAbortoInspeccion(String causaAbortoInspeccion) {
		this.causaAbortoInspeccion = causaAbortoInspeccion;
	}
	public String getVehiculoConCatalizador() {
		return vehiculoConCatalizador;
	}
	public void setVehiculoConCatalizador(String vehiculoConCatalizador) {
		this.vehiculoConCatalizador = vehiculoConCatalizador;
	}
	public String getLugarTomaTemperaturaVehiculo() {
		return lugarTomaTemperaturaVehiculo;
	}
	public void setLugarTomaTemperaturaVehiculo(String lugarTomaTemperaturaVehiculo) {
		this.lugarTomaTemperaturaVehiculo = lugarTomaTemperaturaVehiculo;
	}
	public String getTemperaturaAmbiente() {
		return temperaturaAmbiente;
	}
	public void setTemperaturaAmbiente(String temperaturaAmbiente) {
		this.temperaturaAmbiente = temperaturaAmbiente;
	}
	public String getHumedadRelativa() {
		return humedadRelativa;
	}
	public void setHumedadRelativa(String humedadRelativa) {
		this.humedadRelativa = humedadRelativa;
	}
	public String getTemperaturaMotor() {
		return temperaturaMotor;
	}
	public void setTemperaturaMotor(String temperaturaMotor) {
		this.temperaturaMotor = temperaturaMotor;
	}
	public String getRpmRalenti() {
		return rpmRalenti;
	}
	public void setRpmRalenti(String rpmRalenti) {
		this.rpmRalenti = rpmRalenti;
	}
	public String getRpmCrucero() {
		return rpmCrucero;
	}
	public void setRpmCrucero(String rpmCrucero) {
		this.rpmCrucero = rpmCrucero;
	}
	public String getPresenciaHumoNegroAzul() {
		return presenciaHumoNegroAzul;
	}
	public void setPresenciaHumoNegroAzul(String presenciaHumoNegroAzul) {
		this.presenciaHumoNegroAzul = presenciaHumoNegroAzul;
	}
	public String getPresenciaDilucion() {
		return presenciaDilucion;
	}
	public void setPresenciaDilucion(String presenciaDilucion) {
		this.presenciaDilucion = presenciaDilucion;
	}
	public String getRpmFueraRango() {
		return rpmFueraRango;
	}
	public void setRpmFueraRango(String rpmFueraRango) {
		this.rpmFueraRango = rpmFueraRango;
	}
	public String getFugaTuboEscapes() {
		return fugaTuboEscapes;
	}
	public void setFugaTuboEscapes(String fugaTuboEscapes) {
		this.fugaTuboEscapes = fugaTuboEscapes;
	}
	public String getSalidasAdicionales() {
		return salidasAdicionales;
	}
	public void setSalidasAdicionales(String salidasAdicionales) {
		this.salidasAdicionales = salidasAdicionales;
	}
	public String getAusenciaFugasTapaAceite() {
		return ausenciaFugasTapaAceite;
	}
	public void setAusenciaFugasTapaAceite(String ausenciaFugasTapaAceite) {
		this.ausenciaFugasTapaAceite = ausenciaFugasTapaAceite;
	}
	public String getAusenciaFugasTapaCombustible() {
		return ausenciaFugasTapaCombustible;
	}
	public void setAusenciaFugasTapaCombustible(String ausenciaFugasTapaCombustible) {
		this.ausenciaFugasTapaCombustible = ausenciaFugasTapaCombustible;
	}
	public String getSistemaAdmisionAire() {
		return sistemaAdmisionAire;
	}
	public void setSistemaAdmisionAire(String sistemaAdmisionAire) {
		this.sistemaAdmisionAire = sistemaAdmisionAire;
	}
	public String getDesconexionPCV() {
		return desconexionPCV;
	}
	public void setDesconexionPCV(String desconexionPCV) {
		this.desconexionPCV = desconexionPCV;
	}
	public String getInstalacionAccesoriosTubo() {
		return instalacionAccesoriosTubo;
	}
	public void setInstalacionAccesoriosTubo(String instalacionAccesoriosTubo) {
		this.instalacionAccesoriosTubo = instalacionAccesoriosTubo;
	}
	public String getIncorrectaOperacionRefrigeracion() {
		return incorrectaOperacionRefrigeracion;
	}
	public void setIncorrectaOperacionRefrigeracion(String incorrectaOperacionRefrigeracion) {
		this.incorrectaOperacionRefrigeracion = incorrectaOperacionRefrigeracion;
	}
	public String getResultadoHcRalenti() {
		return resultadoHcRalenti;
	}
	public void setResultadoHcRalenti(String resultadoHcRalenti) {
		this.resultadoHcRalenti = resultadoHcRalenti;
	}
	public String getResultadoHcCrucero() {
		return resultadoHcCrucero;
	}
	public void setResultadoHcCrucero(String resultadoHcCrucero) {
		this.resultadoHcCrucero = resultadoHcCrucero;
	}
	public String getResultadoCoRalenti() {
		return resultadoCoRalenti;
	}
	public void setResultadoCoRalenti(String resultadoCoRalenti) {
		this.resultadoCoRalenti = resultadoCoRalenti;
	}
	public String getResultadoCoCrucero() {
		return resultadoCoCrucero;
	}
	public void setResultadoCoCrucero(String resultadoCoCrucero) {
		this.resultadoCoCrucero = resultadoCoCrucero;
	}
	public String getResultadoCo2Ralenti() {
		return resultadoCo2Ralenti;
	}
	public void setResultadoCo2Ralenti(String resultadoCo2Ralenti) {
		this.resultadoCo2Ralenti = resultadoCo2Ralenti;
	}
	public String getResultadoCo2Crucero() {
		return resultadoCo2Crucero;
	}
	public void setResultadoCo2Crucero(String resultadoCo2Crucero) {
		this.resultadoCo2Crucero = resultadoCo2Crucero;
	}
	public String getResultadoO2Ralenti() {
		return resultadoO2Ralenti;
	}
	public void setResultadoO2Ralenti(String resultadoO2Ralenti) {
		this.resultadoO2Ralenti = resultadoO2Ralenti;
	}
	public String getResultadoO2Crucero() {
		return resultadoO2Crucero;
	}
	public void setResultadoO2Crucero(String resultadoO2Crucero) {
		this.resultadoO2Crucero = resultadoO2Crucero;
	}
	public String getIncumplimientoNivelesEmisiones() {
		return incumplimientoNivelesEmisiones;
	}
	public void setIncumplimientoNivelesEmisiones(String incumplimientoNivelesEmisiones) {
		this.incumplimientoNivelesEmisiones = incumplimientoNivelesEmisiones;
	}
	public String getResultadoFinalPrueba() {
		return resultadoFinalPrueba;
	}
	public void setResultadoFinalPrueba(String resultadoFinalPrueba) {
		this.resultadoFinalPrueba = resultadoFinalPrueba;
	}

	






}
