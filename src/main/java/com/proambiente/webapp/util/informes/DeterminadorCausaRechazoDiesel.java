package com.proambiente.webapp.util.informes;

import java.util.Collections;
import java.util.List;

import com.proambiente.modelo.Medida;
import com.proambiente.modelo.Permisible;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.TipoMedida;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.modelo.dto.ResultadoDieselDTO;
import com.proambiente.webapp.dao.PermisibleFacade;
import com.proambiente.webapp.util.ConstantesMedidasGases;
import com.proambiente.webapp.util.dto.ResultadoDieselDTOInformes;
import com.proambiente.webapp.util.dto.sda.CausaRechazoDiesel;

/**
 * Codifica la causa de rechazo que requiere la secretaria de ambiente
 * @author fdavi
 *
 */
public class DeterminadorCausaRechazoDiesel {
	
	

	private PermisibleFacade permisibleService;
	private boolean aprobado;
	private boolean dilucion;
	
	
	
	
	public DeterminadorCausaRechazoDiesel(PermisibleFacade permisibleService) {
		super();
		this.permisibleService = permisibleService;
	}

	public CausaRechazoDiesel determinarCausaRechazo(ResultadoDieselDTO resultado, 
															ResultadoDieselDTOInformes resultadosInforme, Prueba prueba,
															List<Medida> medidas,Vehiculo vehiculo) {
		if(prueba.isAprobada()) {
			return CausaRechazoDiesel.SIN_RECHAZO;
		}else {
			if(resultado.getFugasTuboEscape().equalsIgnoreCase("TRUE")) {
				return CausaRechazoDiesel.FUGAS_TUBO_ESCAPE;
			}
			if(resultado.getFugasSilenciador().equalsIgnoreCase("TRUE")) {
				return CausaRechazoDiesel.FUGAS_SILENCIADOR;
			}
			if(resultado.getTapaAceite().equalsIgnoreCase("TRUE")) {
				return CausaRechazoDiesel.AUSENCIA_TAPA_ACEITE;
			}
			if(resultado.getTapaCombustible().equalsIgnoreCase("TRUE")) {
				return CausaRechazoDiesel.AUSENCIA_TAPA_COMBUSTIBLE;				
			}
			if(resultado.getSalidasAdicionales().equalsIgnoreCase("TRUE")) {
				return CausaRechazoDiesel.SALIDAS_ADICIONALES;
			}
			if(resultadosInforme.getAusenciaDeFiltroAire().equalsIgnoreCase("TRUE")) {
				return CausaRechazoDiesel.FALLA_FILTRO_AIRE;
			}
			if(resultadosInforme.getInstalacionAccesoriosODeformaciones().equals("TRUE")) {
				return CausaRechazoDiesel.ACCESORIOS_DEFORMACIONES_IMPIDEN;
			}
			if(resultado.getSalidasAdicionales().equalsIgnoreCase("TRUE")) {
				return CausaRechazoDiesel.FALLA_SISTEMA_REFRIGERACION;
			}
			if(resultadosInforme.getInstalacionDispositivosAlteranRpms().equalsIgnoreCase("TRUE")) {
				return CausaRechazoDiesel.DISPOSITIVOS_OBSTACULOS_IMPIDEN_ACELERAR;
			}			
			if(resultadosInforme.getGobernadorNoLimitaRevoluciones().equalsIgnoreCase("TRUE")) {
				return CausaRechazoDiesel.GOBERNADOR_NO_LIMITA;
			}
			if(resultadosInforme.getVelocidadNoAlcanzada5Seg().equalsIgnoreCase("TRUE")) {
				return CausaRechazoDiesel.NO_SE_ALCANZA_REVOLUCIONES_GOBERNADAS;
			}
			if(resultadosInforme.getMalFuncionamientoMotor().equalsIgnoreCase("TRUE")) {
				return CausaRechazoDiesel.MAL_FUNCIONAMIENTO_MOTOR;
			}
			if(resultadosInforme.getFallaSubitaMotor().equalsIgnoreCase("TRUE")) {
				return CausaRechazoDiesel.MAL_FUNCIONAMIENTO_MOTOR;
			}
			if(resultadosInforme.getDiferenciasAritmeticas().equalsIgnoreCase("TRUE")) {
				return CausaRechazoDiesel.DIFERENCIA_ARITMETICA;
			}
			if(resultadosInforme.getInestabilidadRevoluciones().equalsIgnoreCase("TRUE")) {
				return CausaRechazoDiesel.INESTABILIDAD_RPMS_CICLOS_MEDICION;
			}
			if(resultadosInforme.getDiferenciaTemperaturas().equalsIgnoreCase("TRUE")) {
				return CausaRechazoDiesel.DIFERENCIA_TEMPERATURA;
			}			
			return determinarCausalPorMedida(prueba, vehiculo, medidas);			
		}		
			
	}
	
	
	
	
	private CausaRechazoDiesel  determinarCausalPorMedida(Prueba prueba, Vehiculo v, List<Medida> medidas) {
		
		 
		 Medida MEDIDA_MAXIMO_CICLOS = new Medida(); 
		 TipoMedida tipoMedidaMaximoCiclos = new TipoMedida();
		 tipoMedidaMaximoCiclos.setTipoMedidaId( ConstantesMedidasGases.CODIGO_RESULTADO_OPACIDAD);
		 MEDIDA_MAXIMO_CICLOS.setTipoMedida(tipoMedidaMaximoCiclos);
		
		
		 	     
	     
        CausaRechazoDiesel causaRechazo = null ;
        
        Permisible permisible = permisibleService.buscarPermisible(MEDIDA_MAXIMO_CICLOS.getTipoMedida().getTipoMedidaId(), v.getTipoVehiculo().getTipoVehiculoId(), v.getModelo());
		
        
        Collections.sort(medidas, (m1,m2)->{return m1.getTipoMedida().getTipoMedidaId().compareTo( m2.getTipoMedida().getTipoMedidaId() );});
        
        Medida medidaMaximoCiclos = buscarMedida(medidas, MEDIDA_MAXIMO_CICLOS,false);
        if(medidaMaximoCiclos == null)
        	return null;
        if ( medidaMaximoCiclos.getValor() >= permisible.getValorMinimo() && medidaMaximoCiclos.getValor() <= permisible.getValorMaximo()) {

        } else {
            aprobado = false;
            causaRechazo = seleccionarCausaRechazoPorModelo(v.getModelo());
            
        }   
        
		return causaRechazo;
        
    }

	private CausaRechazoDiesel seleccionarCausaRechazoPorModelo(Integer modelo) {
		CausaRechazoDiesel causaRechazo = null;
		if(modelo < 1980) {
			causaRechazo = CausaRechazoDiesel.OPACIDAD_EXCEDE_1980;
		}else if( modelo >= 1981 && modelo < 1986 ) {
			causaRechazo = CausaRechazoDiesel.OPACIDAD_EXCEDE_1981_1985;
		}else if( modelo >= 1986 && modelo < 1991 ) {
			causaRechazo = CausaRechazoDiesel.OPACIDAD_EXCEDE_1991_1995;
		}else if ( modelo >= 1996 && modelo < 2001 ) {
			causaRechazo = CausaRechazoDiesel.OPACIDAD_EXCEDE_1996_2000;
		}else {
			causaRechazo = CausaRechazoDiesel.OPACIDAD_EXCEDE_2001;
		}
		return causaRechazo;		
	}
	
	private Medida buscarMedida(List<Medida> medidas, Medida medidaD, boolean requerida) throws RuntimeException {

        int indice = Collections.binarySearch(medidas, medidaD,(m1,m2)->{return m1.getTipoMedida().getTipoMedidaId().compareTo( m2.getTipoMedida().getTipoMedidaId() );});
        if (indice < 0 && requerida) {
            throw new RuntimeException("No se encuentra medida para evaluar la prueba" + medidaD.getTipoMedida().getTipoMedidaId() );
        } else {
            if (indice >= 0) {
                Medida medida = medidas.get(indice);
                return medida;
            } else {
            	System.out.println( "Tamanio medias" + medidas.size() );
                return null;
            }
        }
    }
	
}
