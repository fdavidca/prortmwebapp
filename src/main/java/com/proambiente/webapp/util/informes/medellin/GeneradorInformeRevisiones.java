package com.proambiente.webapp.util.informes.medellin;

import java.text.SimpleDateFormat;
import java.util.Optional;

import com.proambiente.modelo.Certificado;
import com.proambiente.modelo.ClaseVehiculo;
import com.proambiente.modelo.Color;
import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.LineaVehiculo;
import com.proambiente.modelo.Marca;
import com.proambiente.modelo.Propietario;
import com.proambiente.modelo.Revision;
import com.proambiente.modelo.Servicio;
import com.proambiente.modelo.TipoCombustible;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.webapp.util.dto.InformacionRevisionDTO;

public class GeneradorInformeRevisiones {
	
	
	public InformacionRevisionDTO generarInforme(Revision revision,Optional<Certificado> optCertificado,InformacionCda infoCda) {
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");	
		
		InformacionRevisionDTO ir = new InformacionRevisionDTO();
		if(optCertificado.isPresent()) {
			Certificado c = optCertificado.get();
			ir.setNumeroCertificado(c.getConsecutivoSustrato());
			String fechaCertificado = sdf.format( c.getFechaImpresion());
			ir.setFechaExpedicion(fechaCertificado);
			String fechaVencimientoCertificado = sdf.format( c.getFechaVencimiento() );
			ir.setFechaVencimiento(fechaVencimientoCertificado);
		}else {
			ir.setNumeroCertificado("");
			ir.setFechaExpedicion("");
			ir.setFechaVencimiento("");
		}
		ir.setTipoIdentificacionCda("NIT");
		ir.setIdentificacionCda( infoCda.getNit() );
		ir.setDivipo( infoCda.getCodigoDivipo() );
		ir.setSucursal( infoCda.getNombre() );
		ir.setClaseCda("A");
		Vehiculo v = revision.getVehiculo();
		if(v != null) {
			ir.setPlaca( v.getPlaca() );
			ClaseVehiculo clase = v.getClaseVehiculo();
			if(clase != null) {
				ir.setClaseVehiculo( clase.getNombre() );
			}
			Servicio servicio = v.getServicio();
			if(servicio != null) {
				ir.setServicio( servicio.getNombre() );
			}
			Marca m = v.getMarca();
			if( m != null) {
				ir.setMarca( m.getNombre() );
			}
			LineaVehiculo lv = v.getLineaVehiculo();
			if( lv != null) {
				ir.setLinea( lv.getNombre() );
			}
			ir.setModelo( v.getModelo().toString() );
			Color color = v.getColor();
			if(color != null) {
				ir.setColor( color.getNombre() );
			}
			TipoCombustible tc = v.getTipoCombustible();
			if(tc != null) {
				ir.setTipoCombustible( tc.getNombre() );
			}
			ir.setMotor( v.getNumeroMotor() );
			ir.setVin( v.getVin() );
			ir.setCilindraje( v.getCilindraje() == null ? "0": v.getCilindraje().toString() );
			ir.setNumeroSoat( v.getNumeroSoat() );
			ir.setFechaExpedicionSoat( sdf.format( v.getFechaSoat() ) );
			ir.setFechaVencimientoSoat( sdf.format(v.getFechaExpiracionSoat() ) );
			if(v.getAseguradora() != null) {
				ir.setAseguradora( v.getAseguradora().getNombre());
			}
			ir.setNumeroLicenciaTransito( v.getNumeroLicencia() );
		}
			
			//seccion de propietario
			
			Propietario p = v.getPropietario();
			if( p != null) {
				
				ir.setTipoIdentificacionPropietario( p.getTipoDocumentoIdentidad().getNombreTipoDocumento() );
				ir.setIdentificacionPropietario( p.getPropietarioId().toString() );
				ir.setNombrePropietario(  p.getNombres() + " " + p.getApellidos() );
				ir.setDireccionPropietario( p.getDireccion() );
				ir.setTelefonoPropietario( p.getTelefono1() );
				
			}
			ir.setNumeroLicenciaConduccion("");
			ir.setCategoriaLicenciaConduccion("");
			
			Propietario c = v.getConductor();
			if( c != null ) {
				ir.setTipoIdentificacionConductor( p.getTipoDocumentoIdentidad().getNombreTipoDocumento() );
				ir.setIdentificacionConductor( p.getPropietarioId().toString() );
				ir.setNombreConductor(  p.getNombres() + " " + p.getApellidos() );
				ir.setDireccionConductor( p.getDireccion() );
				ir.setTelefonoConductor( p.getTelefono1() );
			}
			
			ir.setFechaCargue(sdf.format(revision.getFechaCreacionRevision()));
			
			if( revision.getAprobada() ) {
				ir.setEstadoRevision("APROBADA");
			}else {
				ir.setEstadoRevision("REPROBADA");
			}
			
				
		return ir;
		}
}



