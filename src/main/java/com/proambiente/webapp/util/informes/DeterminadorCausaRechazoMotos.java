package com.proambiente.webapp.util.informes;

import java.util.Collections;
import java.util.List;

import com.proambiente.modelo.Medida;
import com.proambiente.modelo.Permisible;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.TipoMedida;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.modelo.dto.ResultadoOttoMotocicletasDTO;
import com.proambiente.webapp.dao.PermisibleFacade;
import com.proambiente.webapp.util.ConstantesMedidasGases;
import com.proambiente.webapp.util.dto.sda.CausaRechazoMotocicletas;

/**
 * Codifica la causa de rechazo que requiere la secretaria de ambiente
 * @author fdavi
 *
 */
public class DeterminadorCausaRechazoMotos {
	
	

	private PermisibleFacade permisibleService;
	
	
	
	
	public DeterminadorCausaRechazoMotos(PermisibleFacade permisibleService) {
		super();
		this.permisibleService = permisibleService;
	}

	public CausaRechazoMotocicletas determinarCausaRechazo(ResultadoOttoMotocicletasDTO resultadoMotocicleta, Prueba prueba,List<Medida> medidas,Vehiculo vehiculo) {
		if(prueba.isAprobada()) {
			return CausaRechazoMotocicletas.SIN_RECHAZO;
		}else {
			if(resultadoMotocicleta.getFugasTuboEscape().equalsIgnoreCase("TRUE")) {
				return CausaRechazoMotocicletas.FUGAS_TUBO_ESCAPE;
			}
			if(resultadoMotocicleta.getFugasSilenciador().equalsIgnoreCase("TRUE")) {
				return CausaRechazoMotocicletas.FUGAS_SILENCIADOR;
			}
			if(resultadoMotocicleta.getTapaAceite().equalsIgnoreCase("TRUE")) {
				return CausaRechazoMotocicletas.AUSENCIA_TAPA_ACEITE;
			}
			if(resultadoMotocicleta.getTapaCombustible().equalsIgnoreCase("TRUE")) {
				return CausaRechazoMotocicletas.AUSENCIA_TAPA_COMBUSTIBLE;				
			}
			if(resultadoMotocicleta.getSalidasAdicionales().equalsIgnoreCase("TRUE")) {
				return CausaRechazoMotocicletas.SALIDAS_ADICIONALES;
			}
			if(resultadoMotocicleta.getPresenciaHumos().equalsIgnoreCase("TRUE")) {
				return CausaRechazoMotocicletas.HUMO_NEGRO_AZUL;
			}
			if(resultadoMotocicleta.getRevolucionesFueraRango().equalsIgnoreCase("TRUE")) {
				return CausaRechazoMotocicletas.REVOLUCIONES_FUERA_RANGO;
			}
			return determinarCausalPorMedida(prueba, vehiculo, medidas);			
		}		
			
	}
	
	
	
	
	private CausaRechazoMotocicletas  determinarCausalPorMedida(Prueba prueba, Vehiculo vehiculo, List<Medida> medidas) {
		
		 Medida MEDIDA_HC_RALENTI_4T = new Medida();
		 TipoMedida tipoMedidaHcRalenti4t = new TipoMedida();
		 tipoMedidaHcRalenti4t.setTipoMedidaId( ConstantesMedidasGases.CODIGO_MEDIDA_HC_RALENTI);
		 MEDIDA_HC_RALENTI_4T.setTipoMedida(tipoMedidaHcRalenti4t);
		 
	     Medida MEDIDA_CO_RALENTI_4T = new Medida();
	     TipoMedida tipoMedidaCORalenti4t = new TipoMedida();
	     tipoMedidaCORalenti4t.setTipoMedidaId( ConstantesMedidasGases.CODIGO_MEDIDA_CO_RALENTI);
	     MEDIDA_CO_RALENTI_4T.setTipoMedida(tipoMedidaCORalenti4t);

	     Medida MEDIDA_HC_RALENTI_2T = new Medida();
	     TipoMedida tipoMedidaHcRalenti2t = new TipoMedida();
	     tipoMedidaHcRalenti2t.setTipoMedidaId( ConstantesMedidasGases.CODIGO_MEDIDA_HC_RALENTI_2T );
	     MEDIDA_HC_RALENTI_2T.setTipoMedida(tipoMedidaHcRalenti2t);
	     
	     Medida MEDIDA_CO_RALENTI_2T = new Medida();
	     TipoMedida tipoMedidaCoRalenti2t = new TipoMedida();
	     tipoMedidaCoRalenti2t.setTipoMedidaId( ConstantesMedidasGases.CODIGO_MEDIDA_CO_RALENTI_2T);
	     MEDIDA_CO_RALENTI_2T.setTipoMedida(tipoMedidaCoRalenti2t);

        CausaRechazoMotocicletas causaRechazo = null ;
		
        
        Collections.sort(medidas, (m1,m2)->{return m1.getTipoMedida().getTipoMedidaId().compareTo( m2.getTipoMedida().getTipoMedidaId() );});
        
        if (vehiculo.getTiemposMotor() == 4) {
            Medida medidaHCRalenti = buscarMedida(medidas, MEDIDA_HC_RALENTI_4T, true);
            Permisible permisible = permisibleService.buscarPermisible(MEDIDA_HC_RALENTI_4T.getTipoMedida().getTipoMedidaId() , vehiculo.getTipoVehiculo().getTipoVehiculoId(), vehiculo.getModelo());
            if (medidaHCRalenti.getValor() >= permisible.getValorMinimo() 
                    && medidaHCRalenti.getValor() <= permisible.getValorMaximo()) {

            } else {
                causaRechazo =  CausaRechazoMotocicletas.HC_CUATRO_TIEMPOS;
            }
            Medida medidaCORalenti = buscarMedida(medidas,MEDIDA_CO_RALENTI_4T,true);
            permisible = permisibleService.buscarPermisible(MEDIDA_CO_RALENTI_4T.getTipoMedida().getTipoMedidaId(), 
                                                                vehiculo.getTipoVehiculo().getTipoVehiculoId(), vehiculo.getModelo());
            if(medidaCORalenti.getValor() >= permisible.getValorMinimo()
                    && medidaCORalenti.getValor() <= permisible.getValorMaximo()){
                
            }else {
                
                causaRechazo =  CausaRechazoMotocicletas.CO_CUATRO_TIEMPOS;
            }
        }
        if (vehiculo.getTiemposMotor() == 2) {
            Medida medidaHCRalenti = buscarMedida(medidas, MEDIDA_HC_RALENTI_2T, true);
            Permisible permisible = permisibleService.buscarPermisible(MEDIDA_HC_RALENTI_2T.getTipoMedida().getTipoMedidaId(), 
            														vehiculo.getTipoVehiculo().getTipoVehiculoId(), vehiculo.getModelo());
            if (medidaHCRalenti.getValor() >= permisible.getValorMinimo() 
                    && medidaHCRalenti.getValor() <= permisible.getValorMaximo()) {

            } else {
                if(vehiculo.getModelo()>=2010) {
                	causaRechazo = CausaRechazoMotocicletas.HC_DOS_TIEMPOS_2010_MAYOR;
                }else {
                	causaRechazo = CausaRechazoMotocicletas.HC_DOS_TIEMPOS_MENOR_2009;
                }
                
            }
            Medida medidaCORalenti = buscarMedida(medidas,MEDIDA_CO_RALENTI_2T,true);
            permisible = permisibleService.buscarPermisible(MEDIDA_CO_RALENTI_2T.getTipoMedida().getTipoMedidaId(), 
                                                                vehiculo.getTipoVehiculo().getTipoVehiculoId(), vehiculo.getModelo());
            if(medidaCORalenti.getValor() >= permisible.getValorMinimo()
                    && medidaCORalenti.getValor() <= permisible.getValorMaximo()){
                
            }else {
                if(vehiculo.getModelo() >= 2010) {
                	causaRechazo = CausaRechazoMotocicletas.CO_DOS_TIEMPOS_2010_MAYOR;
                }else{
                	causaRechazo = CausaRechazoMotocicletas.CO_DOS_TIEMPOS_MENOR_2009;
                }
                
            }

        }
		return causaRechazo;
        
    }
	
	private Medida buscarMedida(List<Medida> medidas, Medida medidaD, boolean requerida) throws RuntimeException {

        int indice = Collections.binarySearch(medidas, medidaD,(m1,m2)->{return m1.getTipoMedida().getTipoMedidaId().compareTo( m2.getTipoMedida().getTipoMedidaId() );});
        if (indice < 0 && requerida) {
            throw new RuntimeException("No se encuentra medida para evaluar la prueba : " + medidaD.getTipoMedida().getTipoMedidaId() );
        } else {
            if (indice >= 0) {
                Medida medida = medidas.get(indice);
                return medida;
            } else {
                throw new RuntimeException("Erro en busqueda de medida");
            }
        }
    }
	
	
}
