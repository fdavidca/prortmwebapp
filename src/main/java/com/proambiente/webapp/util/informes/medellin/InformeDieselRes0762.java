package com.proambiente.webapp.util.informes.medellin;

public class InformeDieselRes0762 {
	public String numeroExpedienteCda;
	public String tipoIdentificacionCda;
	public String numeroIdentificacionCda;
	public String nombreRazonSocial;
	public String nombreCdaEstablecimientoComercio;
	public String claseCda;
	public String direccionCda;
	public String correoElectronicoCda;
	public String telefonoCda;
	public String telefonoSecundarioCda;
	public String municipioCda;
	public String resolucionAutoridadAmbiental;
	public String fechaResolucionVigente;
	public String totalOpacimetros;
	public String tipoIdentificacionPropietario;
	public String numeroIdentificacionPropietario;
	public String nombresApellidosPropietario;
	public String direccionPropietario;
	public String telefonoPropietario;
	public String telefonoSecundarioPropietario;
	public String municipioPropietario;
	public String correoElectronicoPropietario;
	public String placa;
	public String modelo;
	public String numeroMotor;
	public String vin;
	public String modificacionesMotor;
	public String diametroTuboEscape;
	public String cilindraje;
	public String licenciaTransito;
	public String kilometraje;
	public String marca;
	public String linea;
	public String clase;
	public String servicio;
	public String combustible;
	public String tipoMotor;
	public String conoceRpmFabricante;
	public String rpmMinRalFabricante;
	public String rpmMaxRalFabricante;
	public String rpmMinGobFabricante;
	public String rpmMaxGobFabricante;
	public String marcaOpacimero;
	public String modeloOpacimetro;
	public String serialOpacimetro;
	public String marcaBancoGases;
	public String modeloBancoGases;
	public String serialBancoGases;
	public String ltoeOpacimetro;
	public String serialElectronicoOpacimetro;
	public String marcaCaptadorRpm;
	public String serialCaptadorRpm;
	public String marcaSensorTemperaturaMotor;
	public String serialSensorTemperaturaMotor;
	public String marcaSensorTemperaturaAmbiente;
	public String serialSensorTemperaturaAmbiente;
	public String marcaSensorHumedad;
	public String serialSensorHumedad;
	public String nombreSoftware;
	public String versionSoftware;
	public String nombreProveedorSoftware;
	public String tipoIdentificacionInspectorVerificacion;
	public String numeroIdentificacionInspectorVerificacion;
	public String nombreInspectorVerificacion;
	public String fechaVerificacionLinealidad;
	public String laboratorioFiltroMayor;
	public String serialFiltroMayor;
	public String certificadoFiltroMayor;
	public String valorFiltroMayor;
	public String laboratorioFiltroMenor;
	public String serialFiltroMenor;
	public String certificadoFiltroMenor;
	public String valorFiltroMenor;
	public String resultadoVerificacionPuntoCero;
	public String resultadoVerificacionFiltroMenor;
	public String resultadoVerificacionFiltroMayor;
	public String resultadoVerificacionPuntoCien;
	public String resultadoVerificacionLinealidad;
	public String tipoIdentificacionDirectorTecnico;
	public String numeroIdentificacionDirectorTecnico;
	public String nombreDirectorTecnico;
	public String tipoIdentificacionInspectorPrueba;
	public String numeroIdentificacionInspectorPrueba;
	public String nombreInspectorPrueba;
	public String numeroFUR;
	public String fechaFUR;
	public String numeroConsecutivoRunt;
	public String furAsociados;
	public String certificadoEmitido;
	public String fechaHoraInicioInspeccion;
	public String fechaHoraFinInspeccion;
	public String fechaHoraAbortoInspeccion;
	public String causaAbortoInspeccion;
	public String temperaturaAmbiente;
	public String humedadRelativa;
	public String temperaturaInicialMotor;
	public String temperaturaFinalMotor;
	public String rpmRalentiRegistrada;
	public String rpmGobernadaRegistrada;
	public String rpmFueraRango;
	public String fugasTubo;
	public String salidasAdicionales;
	public String ausenciaFugasTapaAceite;
	public String ausenciaFugasTapaCombustible;
	public String incorrectaInstalacionAusenciaFiltroAire;
	public String dispositivosInstaladosAlteraVelocidad;
	public String instalacionDispositivosImpidenIntroducirSonda;
	public String incorrectaOperacionRefrigeracion;
	public String diferenciaTemperaturaMayor10;
	public String incorrectaOperacionGobernador;
	public String indicadorMalFuncionamientoMotor;
	public String noSeAlcanzaGobernadas;
	public String fallaSubitaMotor;
	public String rechazoPorDiferenciaAritmetica;
	public String incumplimientoNivelesEmisiones;
	
	public String rpmRalentiCicloPreliminar;
	public String rpmGobernadaCicloPreliminar;
	public String opacidadCicloPreliminar;
	public String densidadHumoCicloPreliminar;
	
	public String rpmRalentiCicloUno;
	public String rpmGobernadaCicloUno;
	public String opacidadCicloUno;
	public String densidadHumoCicloUno;
	
	public String rpmRalentiCicloDos;
	public String rpmGobernadaCicloDos;
	public String opacidadCicloDos;
	public String densidadHumoCicloDos;
	
	public String rpmRalentiCicloTres;
	public String rpmGobernadaCicloTres;
	public String opacidadCicloTres;
	public String densidadHumoCicloTres;
	
	public String resultadoFinalOpacidad;
	public String resultadoFinalDensidadHumo;
	public String resultadoFinalPruebaRealizada;
	
	public String getNumeroExpedienteCda() {
		return numeroExpedienteCda;
	}
	public void setNumeroExpedienteCda(String numeroExpedienteCda) {
		this.numeroExpedienteCda = numeroExpedienteCda;
	}
	public String getTipoIdentificacionCda() {
		return tipoIdentificacionCda;
	}
	public void setTipoIdentificacionCda(String tipoIdentificacionCda) {
		this.tipoIdentificacionCda = tipoIdentificacionCda;
	}
	public String getNumeroIdentificacionCda() {
		return numeroIdentificacionCda;
	}
	public void setNumeroIdentificacionCda(String numeroIdentificacionCda) {
		this.numeroIdentificacionCda = numeroIdentificacionCda;
	}
	public String getNombreRazonSocial() {
		return nombreRazonSocial;
	}
	public void setNombreRazonSocial(String nombreRazonSocial) {
		this.nombreRazonSocial = nombreRazonSocial;
	}
	public String getNombreCdaEstablecimientoComercio() {
		return nombreCdaEstablecimientoComercio;
	}
	public void setNombreCdaEstablecimientoComercio(String nombreCdaEstablecimientoComercio) {
		this.nombreCdaEstablecimientoComercio = nombreCdaEstablecimientoComercio;
	}
	public String getClaseCda() {
		return claseCda;
	}
	public void setClaseCda(String claseCda) {
		this.claseCda = claseCda;
	}
	public String getDireccionCda() {
		return direccionCda;
	}
	public void setDireccionCda(String direccionCda) {
		this.direccionCda = direccionCda;
	}
	public String getCorreoElectronicoCda() {
		return correoElectronicoCda;
	}
	public void setCorreoElectronicoCda(String correoElectronicoCda) {
		this.correoElectronicoCda = correoElectronicoCda;
	}
	public String getTelefonoCda() {
		return telefonoCda;
	}
	public void setTelefonoCda(String telefonoCda) {
		this.telefonoCda = telefonoCda;
	}
	public String getTelefonoSecundarioCda() {
		return telefonoSecundarioCda;
	}
	public void setTelefonoSecundarioCda(String telefonoSecundarioCda) {
		this.telefonoSecundarioCda = telefonoSecundarioCda;
	}
	public String getMunicipioCda() {
		return municipioCda;
	}
	public void setMunicipioCda(String municipioCda) {
		this.municipioCda = municipioCda;
	}
	public String getResolucionAutoridadAmbiental() {
		return resolucionAutoridadAmbiental;
	}
	public void setResolucionAutoridadAmbiental(String resolucionAutoridadAmbiental) {
		this.resolucionAutoridadAmbiental = resolucionAutoridadAmbiental;
	}
	public String getFechaResolucionVigente() {
		return fechaResolucionVigente;
	}
	public void setFechaResolucionVigente(String fechaResolucionVigente) {
		this.fechaResolucionVigente = fechaResolucionVigente;
	}
	public String getTotalOpacimetros() {
		return totalOpacimetros;
	}
	public void setTotalOpacimetros(String totalOpacimetros) {
		this.totalOpacimetros = totalOpacimetros;
	}
	public String getTipoIdentificacionPropietario() {
		return tipoIdentificacionPropietario;
	}
	public void setTipoIdentificacionPropietario(String tipoIdentificacionPropietario) {
		this.tipoIdentificacionPropietario = tipoIdentificacionPropietario;
	}
	public String getNumeroIdentificacionPropietario() {
		return numeroIdentificacionPropietario;
	}
	public void setNumeroIdentificacionPropietario(String numeroIdentificacionPropietario) {
		this.numeroIdentificacionPropietario = numeroIdentificacionPropietario;
	}
	public String getNombresApellidosPropietario() {
		return nombresApellidosPropietario;
	}
	public void setNombresApellidosPropietario(String nombresApellidosPropietario) {
		this.nombresApellidosPropietario = nombresApellidosPropietario;
	}
	public String getDireccionPropietario() {
		return direccionPropietario;
	}
	public void setDireccionPropietario(String direccionPropietario) {
		this.direccionPropietario = direccionPropietario;
	}
	public String getTelefonoPropietario() {
		return telefonoPropietario;
	}
	public void setTelefonoPropietario(String telefonoPropietario) {
		this.telefonoPropietario = telefonoPropietario;
	}
	public String getTelefonoSecundarioPropietario() {
		return telefonoSecundarioPropietario;
	}
	public void setTelefonoSecundarioPropietario(String telefonoSecundarioPropietario) {
		this.telefonoSecundarioPropietario = telefonoSecundarioPropietario;
	}
	public String getMunicipioPropietario() {
		return municipioPropietario;
	}
	public void setMunicipioPropietario(String municipioPropietario) {
		this.municipioPropietario = municipioPropietario;
	}
	public String getCorreoElectronicoPropietario() {
		return correoElectronicoPropietario;
	}
	public void setCorreoElectronicoPropietario(String correoElectronicoPropietario) {
		this.correoElectronicoPropietario = correoElectronicoPropietario;
	}
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getNumeroMotor() {
		return numeroMotor;
	}
	public void setNumeroMotor(String numeroMotor) {
		this.numeroMotor = numeroMotor;
	}
	public String getVin() {
		return vin;
	}
	public void setVin(String vin) {
		this.vin = vin;
	}
	public String getModificacionesMotor() {
		return modificacionesMotor;
	}
	public void setModificacionesMotor(String modificacionesMotor) {
		this.modificacionesMotor = modificacionesMotor;
	}
	public String getDiametroTuboEscape() {
		return diametroTuboEscape;
	}
	public void setDiametroTuboEscape(String diametroTuboEscape) {
		this.diametroTuboEscape = diametroTuboEscape;
	}
	public String getCilindraje() {
		return cilindraje;
	}
	public void setCilindraje(String cilindraje) {
		this.cilindraje = cilindraje;
	}
	public String getLicenciaTransito() {
		return licenciaTransito;
	}
	public void setLicenciaTransito(String licenciaTransito) {
		this.licenciaTransito = licenciaTransito;
	}
	public String getKilometraje() {
		return kilometraje;
	}
	public void setKilometraje(String kilometraje) {
		this.kilometraje = kilometraje;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getLinea() {
		return linea;
	}
	public void setLinea(String linea) {
		this.linea = linea;
	}
	public String getClase() {
		return clase;
	}
	public void setClase(String clase) {
		this.clase = clase;
	}
	public String getServicio() {
		return servicio;
	}
	public void setServicio(String servicio) {
		this.servicio = servicio;
	}
	public String getCombustible() {
		return combustible;
	}
	public void setCombustible(String combustible) {
		this.combustible = combustible;
	}
	public String getTipoMotor() {
		return tipoMotor;
	}
	public void setTipoMotor(String tipoMotor) {
		this.tipoMotor = tipoMotor;
	}
	public String getConoceRpmFabricante() {
		return conoceRpmFabricante;
	}
	public void setConoceRpmFabricante(String conoceRpmFabricante) {
		this.conoceRpmFabricante = conoceRpmFabricante;
	}
	public String getRpmMinRalFabricante() {
		return rpmMinRalFabricante;
	}
	public void setRpmMinRalFabricante(String rpmMinRalFabricante) {
		this.rpmMinRalFabricante = rpmMinRalFabricante;
	}
	public String getRpmMaxRalFabricante() {
		return rpmMaxRalFabricante;
	}
	public void setRpmMaxRalFabricante(String rpmMaxRalFabricante) {
		this.rpmMaxRalFabricante = rpmMaxRalFabricante;
	}
	public String getRpmMinGobFabricante() {
		return rpmMinGobFabricante;
	}
	public void setRpmMinGobFabricante(String rpmMinGobFabricante) {
		this.rpmMinGobFabricante = rpmMinGobFabricante;
	}
	public String getRpmMaxGobFabricante() {
		return rpmMaxGobFabricante;
	}
	public void setRpmMaxGobFabricante(String rpmMaxGobFabricante) {
		this.rpmMaxGobFabricante = rpmMaxGobFabricante;
	}
	public String getMarcaOpacimero() {
		return marcaOpacimero;
	}
	public void setMarcaOpacimero(String marcaOpacimero) {
		this.marcaOpacimero = marcaOpacimero;
	}
	public String getModeloOpacimetro() {
		return modeloOpacimetro;
	}
	public void setModeloOpacimetro(String modeloOpacimetro) {
		this.modeloOpacimetro = modeloOpacimetro;
	}
	public String getSerialOpacimetro() {
		return serialOpacimetro;
	}
	public void setSerialOpacimetro(String serialOpacimetro) {
		this.serialOpacimetro = serialOpacimetro;
	}
	public String getMarcaBancoGases() {
		return marcaBancoGases;
	}
	public void setMarcaBancoGases(String marcaBancoGases) {
		this.marcaBancoGases = marcaBancoGases;
	}
	public String getModeloBancoGases() {
		return modeloBancoGases;
	}
	public void setModeloBancoGases(String modeloBancoGases) {
		this.modeloBancoGases = modeloBancoGases;
	}
	public String getSerialBancoGases() {
		return serialBancoGases;
	}
	public void setSerialBancoGases(String serialBancoGases) {
		this.serialBancoGases = serialBancoGases;
	}
	public String getLtoeOpacimetro() {
		return ltoeOpacimetro;
	}
	public void setLtoeOpacimetro(String ltoeOpacimetro) {
		this.ltoeOpacimetro = ltoeOpacimetro;
	}
	public String getSerialElectronicoOpacimetro() {
		return serialElectronicoOpacimetro;
	}
	public void setSerialElectronicoOpacimetro(String serialElectronicoOpacimetro) {
		this.serialElectronicoOpacimetro = serialElectronicoOpacimetro;
	}
	public String getMarcaCaptadorRpm() {
		return marcaCaptadorRpm;
	}
	public void setMarcaCaptadorRpm(String marcaCaptadorRpm) {
		this.marcaCaptadorRpm = marcaCaptadorRpm;
	}
	public String getSerialCaptadorRpm() {
		return serialCaptadorRpm;
	}
	public void setSerialCaptadorRpm(String serialCaptadorRpm) {
		this.serialCaptadorRpm = serialCaptadorRpm;
	}
	public String getMarcaSensorTemperaturaMotor() {
		return marcaSensorTemperaturaMotor;
	}
	public void setMarcaSensorTemperaturaMotor(String marcaSensorTemperaturaMotor) {
		this.marcaSensorTemperaturaMotor = marcaSensorTemperaturaMotor;
	}
	public String getSerialSensorTemperaturaMotor() {
		return serialSensorTemperaturaMotor;
	}
	public void setSerialSensorTemperaturaMotor(String serialSensorTemperaturaMotor) {
		this.serialSensorTemperaturaMotor = serialSensorTemperaturaMotor;
	}
	public String getMarcaSensorTemperaturaAmbiente() {
		return marcaSensorTemperaturaAmbiente;
	}
	public void setMarcaSensorTemperaturaAmbiente(String marcaSensorTemperaturaAmbiente) {
		this.marcaSensorTemperaturaAmbiente = marcaSensorTemperaturaAmbiente;
	}
	public String getSerialSensorTemperaturaAmbiente() {
		return serialSensorTemperaturaAmbiente;
	}
	public void setSerialSensorTemperaturaAmbiente(String serialSensorTemperaturaAmbiente) {
		this.serialSensorTemperaturaAmbiente = serialSensorTemperaturaAmbiente;
	}
	public String getMarcaSensorHumedad() {
		return marcaSensorHumedad;
	}
	public void setMarcaSensorHumedad(String marcaSensorHumedad) {
		this.marcaSensorHumedad = marcaSensorHumedad;
	}
	public String getSerialSensorHumedad() {
		return serialSensorHumedad;
	}
	public void setSerialSensorHumedad(String serialSensorHumedad) {
		this.serialSensorHumedad = serialSensorHumedad;
	}
	public String getNombreSoftware() {
		return nombreSoftware;
	}
	public void setNombreSoftware(String nombreSoftware) {
		this.nombreSoftware = nombreSoftware;
	}
	public String getVersionSoftware() {
		return versionSoftware;
	}
	public void setVersionSoftware(String versionSoftware) {
		this.versionSoftware = versionSoftware;
	}
	public String getNombreProveedorSoftware() {
		return nombreProveedorSoftware;
	}
	public void setNombreProveedorSoftware(String nombreProveedorSoftware) {
		this.nombreProveedorSoftware = nombreProveedorSoftware;
	}
	public String getTipoIdentificacionInspectorVerificacion() {
		return tipoIdentificacionInspectorVerificacion;
	}
	public void setTipoIdentificacionInspectorVerificacion(String tipoIdentificacionInspectorVerificacion) {
		this.tipoIdentificacionInspectorVerificacion = tipoIdentificacionInspectorVerificacion;
	}
	public String getNumeroIdentificacionInspectorVerificacion() {
		return numeroIdentificacionInspectorVerificacion;
	}
	public void setNumeroIdentificacionInspectorVerificacion(String numeroIdentificacionInspectorVerificacion) {
		this.numeroIdentificacionInspectorVerificacion = numeroIdentificacionInspectorVerificacion;
	}
	public String getNombreInspectorVerificacion() {
		return nombreInspectorVerificacion;
	}
	public void setNombreInspectorVerificacion(String nombreInspectorVerificacion) {
		this.nombreInspectorVerificacion = nombreInspectorVerificacion;
	}
	public String getFechaVerificacionLinealidad() {
		return fechaVerificacionLinealidad;
	}
	public void setFechaVerificacionLinealidad(String fechaVerificacionLinealidad) {
		this.fechaVerificacionLinealidad = fechaVerificacionLinealidad;
	}
	public String getLaboratorioFiltroMayor() {
		return laboratorioFiltroMayor;
	}
	public void setLaboratorioFiltroMayor(String laboratorioFiltroMayor) {
		this.laboratorioFiltroMayor = laboratorioFiltroMayor;
	}
	public String getSerialFiltroMayor() {
		return serialFiltroMayor;
	}
	public void setSerialFiltroMayor(String serialFiltroMayor) {
		this.serialFiltroMayor = serialFiltroMayor;
	}
	public String getCertificadoFiltroMayor() {
		return certificadoFiltroMayor;
	}
	public void setCertificadoFiltroMayor(String certificadoFiltroMayor) {
		this.certificadoFiltroMayor = certificadoFiltroMayor;
	}
	public String getValorFiltroMayor() {
		return valorFiltroMayor;
	}
	public void setValorFiltroMayor(String valorFiltroMayor) {
		this.valorFiltroMayor = valorFiltroMayor;
	}
	public String getLaboratorioFiltroMenor() {
		return laboratorioFiltroMenor;
	}
	public void setLaboratorioFiltroMenor(String laboratorioFiltroMenor) {
		this.laboratorioFiltroMenor = laboratorioFiltroMenor;
	}
	public String getSerialFiltroMenor() {
		return serialFiltroMenor;
	}
	public void setSerialFiltroMenor(String serialFiltroMenor) {
		this.serialFiltroMenor = serialFiltroMenor;
	}
	public String getCertificadoFiltroMenor() {
		return certificadoFiltroMenor;
	}
	public void setCertificadoFiltroMenor(String certificadoFiltroMenor) {
		this.certificadoFiltroMenor = certificadoFiltroMenor;
	}
	public String getValorFiltroMenor() {
		return valorFiltroMenor;
	}
	public void setValorFiltroMenor(String valorFiltroMenor) {
		this.valorFiltroMenor = valorFiltroMenor;
	}
	public String getResultadoVerificacionPuntoCero() {
		return resultadoVerificacionPuntoCero;
	}
	public void setResultadoVerificacionPuntoCero(String resultadoVerificacionPuntoCero) {
		this.resultadoVerificacionPuntoCero = resultadoVerificacionPuntoCero;
	}
	public String getResultadoVerificacionFiltroMenor() {
		return resultadoVerificacionFiltroMenor;
	}
	public void setResultadoVerificacionFiltroMenor(String resultadoVerificacionFiltroMenor) {
		this.resultadoVerificacionFiltroMenor = resultadoVerificacionFiltroMenor;
	}
	public String getResultadoVerificacionFiltroMayor() {
		return resultadoVerificacionFiltroMayor;
	}
	public void setResultadoVerificacionFiltroMayor(String resultadoVerificacionFiltroMayor) {
		this.resultadoVerificacionFiltroMayor = resultadoVerificacionFiltroMayor;
	}
	public String getResultadoVerificacionPuntoCien() {
		return resultadoVerificacionPuntoCien;
	}
	public void setResultadoVerificacionPuntoCien(String resultadoVerificacionPuntoCien) {
		this.resultadoVerificacionPuntoCien = resultadoVerificacionPuntoCien;
	}
	public String getResultadoVerificacionLinealidad() {
		return resultadoVerificacionLinealidad;
	}
	public void setResultadoVerificacionLinealidad(String resultadoVerificacionLinealidad) {
		this.resultadoVerificacionLinealidad = resultadoVerificacionLinealidad;
	}
	public String getTipoIdentificacionDirectorTecnico() {
		return tipoIdentificacionDirectorTecnico;
	}
	public void setTipoIdentificacionDirectorTecnico(String tipoIdentificacionDirectorTecnico) {
		this.tipoIdentificacionDirectorTecnico = tipoIdentificacionDirectorTecnico;
	}
	public String getNumeroIdentificacionDirectorTecnico() {
		return numeroIdentificacionDirectorTecnico;
	}
	public void setNumeroIdentificacionDirectorTecnico(String numeroIdentificacionDirectorTecnico) {
		this.numeroIdentificacionDirectorTecnico = numeroIdentificacionDirectorTecnico;
	}
	public String getNombreDirectorTecnico() {
		return nombreDirectorTecnico;
	}
	public void setNombreDirectorTecnico(String nombreDirectorTecnico) {
		this.nombreDirectorTecnico = nombreDirectorTecnico;
	}
	public String getTipoIdentificacionInspectorPrueba() {
		return tipoIdentificacionInspectorPrueba;
	}
	public void setTipoIdentificacionInspectorPrueba(String tipoIdentificacionInspectorPrueba) {
		this.tipoIdentificacionInspectorPrueba = tipoIdentificacionInspectorPrueba;
	}
	public String getNumeroIdentificacionInspectorPrueba() {
		return numeroIdentificacionInspectorPrueba;
	}
	public void setNumeroIdentificacionInspectorPrueba(String numeroIdentificacionInspectorPrueba) {
		this.numeroIdentificacionInspectorPrueba = numeroIdentificacionInspectorPrueba;
	}
	public String getNombreInspectorPrueba() {
		return nombreInspectorPrueba;
	}
	public void setNombreInspectorPrueba(String nombreInspectorPrueba) {
		this.nombreInspectorPrueba = nombreInspectorPrueba;
	}
	public String getNumeroFUR() {
		return numeroFUR;
	}
	public void setNumeroFUR(String numeroFUR) {
		this.numeroFUR = numeroFUR;
	}
	public String getFechaFUR() {
		return fechaFUR;
	}
	public void setFechaFUR(String fechaFUR) {
		this.fechaFUR = fechaFUR;
	}
	public String getNumeroConsecutivoRunt() {
		return numeroConsecutivoRunt;
	}
	public void setNumeroConsecutivoRunt(String numeroConsecutivoRunt) {
		this.numeroConsecutivoRunt = numeroConsecutivoRunt;
	}
	public String getFurAsociados() {
		return furAsociados;
	}
	public void setFurAsociados(String furAsociados) {
		this.furAsociados = furAsociados;
	}
	public String getCertificadoEmitido() {
		return certificadoEmitido;
	}
	public void setCertificadoEmitido(String certificadoEmitido) {
		this.certificadoEmitido = certificadoEmitido;
	}
	public String getFechaHoraInicioInspeccion() {
		return fechaHoraInicioInspeccion;
	}
	public void setFechaHoraInicioInspeccion(String fechaHoraInicioInspeccion) {
		this.fechaHoraInicioInspeccion = fechaHoraInicioInspeccion;
	}
	public String getFechaHoraFinInspeccion() {
		return fechaHoraFinInspeccion;
	}
	public void setFechaHoraFinInspeccion(String fechaHoraFinInspeccion) {
		this.fechaHoraFinInspeccion = fechaHoraFinInspeccion;
	}
	public String getFechaHoraAbortoInspeccion() {
		return fechaHoraAbortoInspeccion;
	}
	public void setFechaHoraAbortoInspeccion(String fechaHoraAbortoInspeccion) {
		this.fechaHoraAbortoInspeccion = fechaHoraAbortoInspeccion;
	}
	public String getCausaAbortoInspeccion() {
		return causaAbortoInspeccion;
	}
	public void setCausaAbortoInspeccion(String causaAbortoInspeccion) {
		this.causaAbortoInspeccion = causaAbortoInspeccion;
	}
	public String getTemperaturaAmbiente() {
		return temperaturaAmbiente;
	}
	public void setTemperaturaAmbiente(String temperaturaAmbiente) {
		this.temperaturaAmbiente = temperaturaAmbiente;
	}
	public String getHumedadRelativa() {
		return humedadRelativa;
	}
	public void setHumedadRelativa(String humedadRelativa) {
		this.humedadRelativa = humedadRelativa;
	}
	public String getTemperaturaInicialMotor() {
		return temperaturaInicialMotor;
	}
	public void setTemperaturaInicialMotor(String temperaturaInicialMotor) {
		this.temperaturaInicialMotor = temperaturaInicialMotor;
	}
	public String getTemperaturaFinalMotor() {
		return temperaturaFinalMotor;
	}
	public void setTemperaturaFinalMotor(String temperaturaFinalMotor) {
		this.temperaturaFinalMotor = temperaturaFinalMotor;
	}
	public String getRpmRalentiRegistrada() {
		return rpmRalentiRegistrada;
	}
	public void setRpmRalentiRegistrada(String rpmRalentiRegistrada) {
		this.rpmRalentiRegistrada = rpmRalentiRegistrada;
	}
	public String getRpmGobernadaRegistrada() {
		return rpmGobernadaRegistrada;
	}
	public void setRpmGobernadaRegistrada(String rpmGobernadaRegistrada) {
		this.rpmGobernadaRegistrada = rpmGobernadaRegistrada;
	}
	public String getRpmFueraRango() {
		return rpmFueraRango;
	}
	public void setRpmFueraRango(String rpmFueraRango) {
		this.rpmFueraRango = rpmFueraRango;
	}
	public String getFugasTubo() {
		return fugasTubo;
	}
	public void setFugasTubo(String fugasTubo) {
		this.fugasTubo = fugasTubo;
	}
	public String getSalidasAdicionales() {
		return salidasAdicionales;
	}
	public void setSalidasAdicionales(String salidasAdicionales) {
		this.salidasAdicionales = salidasAdicionales;
	}
	public String getAusenciaFugasTapaAceite() {
		return ausenciaFugasTapaAceite;
	}
	public void setAusenciaFugasTapaAceite(String ausenciaFugasTapaAceite) {
		this.ausenciaFugasTapaAceite = ausenciaFugasTapaAceite;
	}
	public String getAusenciaFugasTapaCombustible() {
		return ausenciaFugasTapaCombustible;
	}
	public void setAusenciaFugasTapaCombustible(String ausenciaFugasTapaCombustible) {
		this.ausenciaFugasTapaCombustible = ausenciaFugasTapaCombustible;
	}
	public String getIncorrectaInstalacionAusenciaFiltroAire() {
		return incorrectaInstalacionAusenciaFiltroAire;
	}
	public void setIncorrectaInstalacionAusenciaFiltroAire(String incorrectaInstalacionAusenciaFiltroAire) {
		this.incorrectaInstalacionAusenciaFiltroAire = incorrectaInstalacionAusenciaFiltroAire;
	}
	public String getDispositivosInstaladosAlteraVelocidad() {
		return dispositivosInstaladosAlteraVelocidad;
	}
	public void setDispositivosInstaladosAlteraVelocidad(String dispositivosInstaladosAlteraVelocidad) {
		this.dispositivosInstaladosAlteraVelocidad = dispositivosInstaladosAlteraVelocidad;
	}
	public String getInstalacionDispositivosImpidenIntroducirSonda() {
		return instalacionDispositivosImpidenIntroducirSonda;
	}
	public void setInstalacionDispositivosImpidenIntroducirSonda(String instalacionDispositivosImpidenIntroducirSonda) {
		this.instalacionDispositivosImpidenIntroducirSonda = instalacionDispositivosImpidenIntroducirSonda;
	}
	public String getIncorrectaOperacionRefrigeracion() {
		return incorrectaOperacionRefrigeracion;
	}
	public void setIncorrectaOperacionRefrigeracion(String incorrectaOperacionRefrigeracion) {
		this.incorrectaOperacionRefrigeracion = incorrectaOperacionRefrigeracion;
	}
	public String getDiferenciaTemperaturaMayor10() {
		return diferenciaTemperaturaMayor10;
	}
	public void setDiferenciaTemperaturaMayor10(String diferenciaTemperaturaMayor10) {
		this.diferenciaTemperaturaMayor10 = diferenciaTemperaturaMayor10;
	}
	public String getIncorrectaOperacionGobernador() {
		return incorrectaOperacionGobernador;
	}
	public void setIncorrectaOperacionGobernador(String incorrectaOperacionGobernador) {
		this.incorrectaOperacionGobernador = incorrectaOperacionGobernador;
	}
	public String getIndicadorMalFuncionamientoMotor() {
		return indicadorMalFuncionamientoMotor;
	}
	public void setIndicadorMalFuncionamientoMotor(String indicadorMalFuncionamientoMotor) {
		this.indicadorMalFuncionamientoMotor = indicadorMalFuncionamientoMotor;
	}
	public String getNoSeAlcanzaGobernadas() {
		return noSeAlcanzaGobernadas;
	}
	public void setNoSeAlcanzaGobernadas(String noSeAlcanzaGobernadas) {
		this.noSeAlcanzaGobernadas = noSeAlcanzaGobernadas;
	}
	public String getFallaSubitaMotor() {
		return fallaSubitaMotor;
	}
	public void setFallaSubitaMotor(String fallaSubitaMotor) {
		this.fallaSubitaMotor = fallaSubitaMotor;
	}
	public String getRechazoPorDiferenciaAritmetica() {
		return rechazoPorDiferenciaAritmetica;
	}
	public void setRechazoPorDiferenciaAritmetica(String rechazoPorDiferenciaAritmetica) {
		this.rechazoPorDiferenciaAritmetica = rechazoPorDiferenciaAritmetica;
	}
	public String getIncumplimientoNivelesEmisiones() {
		return incumplimientoNivelesEmisiones;
	}
	public void setIncumplimientoNivelesEmisiones(String incumplimientoNivelesEmisiones) {
		this.incumplimientoNivelesEmisiones = incumplimientoNivelesEmisiones;
	}
	public String getRpmRalentiCicloPreliminar() {
		return rpmRalentiCicloPreliminar;
	}
	public void setRpmRalentiCicloPreliminar(String rpmRalentiCicloPreliminar) {
		this.rpmRalentiCicloPreliminar = rpmRalentiCicloPreliminar;
	}
	public String getRpmGobernadaCicloPreliminar() {
		return rpmGobernadaCicloPreliminar;
	}
	public void setRpmGobernadaCicloPreliminar(String rpmGobernadaCicloPreliminar) {
		this.rpmGobernadaCicloPreliminar = rpmGobernadaCicloPreliminar;
	}
	public String getOpacidadCicloPreliminar() {
		return opacidadCicloPreliminar;
	}
	public void setOpacidadCicloPreliminar(String opacidadCicloPreliminar) {
		this.opacidadCicloPreliminar = opacidadCicloPreliminar;
	}
	public String getDensidadHumoCicloPreliminar() {
		return densidadHumoCicloPreliminar;
	}
	public void setDensidadHumoCicloPreliminar(String densidadHumoCicloPreliminar) {
		this.densidadHumoCicloPreliminar = densidadHumoCicloPreliminar;
	}
	public String getRpmRalentiCicloUno() {
		return rpmRalentiCicloUno;
	}
	public void setRpmRalentiCicloUno(String rpmRalentiCicloUno) {
		this.rpmRalentiCicloUno = rpmRalentiCicloUno;
	}
	public String getRpmGobernadaCicloUno() {
		return rpmGobernadaCicloUno;
	}
	public void setRpmGobernadaCicloUno(String rpmGobernadaCicloUno) {
		this.rpmGobernadaCicloUno = rpmGobernadaCicloUno;
	}
	public String getOpacidadCicloUno() {
		return opacidadCicloUno;
	}
	public void setOpacidadCicloUno(String opacidadCicloUno) {
		this.opacidadCicloUno = opacidadCicloUno;
	}
	public String getDensidadHumoCicloUno() {
		return densidadHumoCicloUno;
	}
	public void setDensidadHumoCicloUno(String densidadHumoCicloUno) {
		this.densidadHumoCicloUno = densidadHumoCicloUno;
	}
	public String getRpmRalentiCicloDos() {
		return rpmRalentiCicloDos;
	}
	public void setRpmRalentiCicloDos(String rpmRalentiCicloDos) {
		this.rpmRalentiCicloDos = rpmRalentiCicloDos;
	}
	public String getRpmGobernadaCicloDos() {
		return rpmGobernadaCicloDos;
	}
	public void setRpmGobernadaCicloDos(String rpmGobernadaCicloDos) {
		this.rpmGobernadaCicloDos = rpmGobernadaCicloDos;
	}
	public String getOpacidadCicloDos() {
		return opacidadCicloDos;
	}
	public void setOpacidadCicloDos(String opacidadCicloDos) {
		this.opacidadCicloDos = opacidadCicloDos;
	}
	public String getDensidadHumoCicloDos() {
		return densidadHumoCicloDos;
	}
	public void setDensidadHumoCicloDos(String densidadHumoCicloDos) {
		this.densidadHumoCicloDos = densidadHumoCicloDos;
	}
	public String getRpmRalentiCicloTres() {
		return rpmRalentiCicloTres;
	}
	public void setRpmRalentiCicloTres(String rpmRalentiCicloTres) {
		this.rpmRalentiCicloTres = rpmRalentiCicloTres;
	}
	public String getRpmGobernadaCicloTres() {
		return rpmGobernadaCicloTres;
	}
	public void setRpmGobernadaCicloTres(String rpmGobernadaCicloTres) {
		this.rpmGobernadaCicloTres = rpmGobernadaCicloTres;
	}
	public String getOpacidadCicloTres() {
		return opacidadCicloTres;
	}
	public void setOpacidadCicloTres(String opacidadCicloTres) {
		this.opacidadCicloTres = opacidadCicloTres;
	}
	public String getDensidadHumoCicloTres() {
		return densidadHumoCicloTres;
	}
	public void setDensidadHumoCicloTres(String densidadHumoCicloTres) {
		this.densidadHumoCicloTres = densidadHumoCicloTres;
	}
	public String getResultadoFinalOpacidad() {
		return resultadoFinalOpacidad;
	}
	public void setResultadoFinalOpacidad(String resultadoFinalOpacidad) {
		this.resultadoFinalOpacidad = resultadoFinalOpacidad;
	}
	public String getResultadoFinalDensidadHumo() {
		return resultadoFinalDensidadHumo;
	}
	public void setResultadoFinalDensidadHumo(String resultadoFinalDensidadHumo) {
		this.resultadoFinalDensidadHumo = resultadoFinalDensidadHumo;
	}
	public String getResultadoFinalPruebaRealizada() {
		return resultadoFinalPruebaRealizada;
	}
	public void setResultadoFinalPruebaRealizada(String resultadoFinalPruebaRealizada) {
		this.resultadoFinalPruebaRealizada = resultadoFinalPruebaRealizada;
	}

	

}
