package com.proambiente.webapp.util.informes;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import com.proambiente.modelo.Analizador;
import com.proambiente.modelo.InformacionCda;
import com.proambiente.modelo.Propietario;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.modelo.VerificacionGasolina;
import com.proambiente.modelo.VerificacionLinealidad;
import com.proambiente.modelo.dto.ResultadoDieselDTO;
import com.proambiente.webapp.util.dto.InformeCarInformacionCDADTO;
import com.proambiente.webapp.util.dto.InformeDieselDTO;
import com.proambiente.webapp.util.dto.InformeGasolinaCarDTO;
import com.proambiente.webapp.util.dto.ResultadoDieselDTOInformes;

public class GeneradorDTOInformes {
	
	
	
	private static final String NO = "NO";

	public static void ponerInformacionVerificacionLinealidad(InformeDieselDTO informe,VerificacionLinealidad verificacion) {
		
		DecimalFormat df = new DecimalFormat("0.0");
		DecimalFormatSymbols custom=new DecimalFormatSymbols();
		custom.setDecimalSeparator('.');
		df.setDecimalFormatSymbols(custom);
		informe.setValorPrimerPuntoLinealidad( df.format( verificacion.getValorFiltro1() ) );
		informe.setValorSegundoPuntoLinealidad( df.format( verificacion.getValorFiltro2() ));
		informe.setValorTercerPuntoLinealidad( df.format( verificacion.getValorFiltro3() ));
		informe.setValorCuartoPuntoLinealidad( df.format( verificacion.getValorFiltro4() ));
		
		informe.setResultadoValorPrimerPuntoLinealidad( df.format( verificacion.getResutadoFiltro1() ));
		informe.setResultadoValorSegundoPuntoLinealidad( df.format( verificacion.getResultadoFiltro2() ));
		informe.setResultadoValorTercerPuntoLinealidad( df.format( verificacion.getResultadoFiltro3() ));
		informe.setResultadoCuartoPuntoLinealidad( df.format( verificacion.getResutadoFiltro4() ));
		
		
		
	}
	
	
	public static void ponerInformacionCda(InformeCarInformacionCDADTO informeDieselDTO,InformacionCda informacionCda) {
		
		
		informeDieselDTO.setNoCda( informacionCda.getNumeroCda() );
		informeDieselDTO.setNombreCda( informacionCda.getNombre() );
		informeDieselDTO.setNitCda( informacionCda.getNit() );
		informeDieselDTO.setDireccionCda( informacionCda.getDireccion() );
		informeDieselDTO.setTelefono1( informacionCda.getTelefono1() );
		informeDieselDTO.setTelefono2( informacionCda.getTelefono2() );
		informeDieselDTO.setCiudad( informacionCda.getCodigoDivipo() );
		informeDieselDTO.setNumeroResolucion( informacionCda.getNumeroResolucionSDA() );
		informeDieselDTO.setFechaResolucion(  informacionCda.getFechaResolucionSDA() );
		
	}
	
	public static void ponerInformacionAnalizador(InformeDieselDTO informeDieselDTO,Analizador analizador) {
		
		informeDieselDTO.setSerieMedidor( analizador.getSerialAnalizador() );
		informeDieselDTO.setMarcaMedidor( analizador.getMarca() );
		
	}
	
	public static void ponerInformacionCertificado(InformeCarInformacionCDADTO informe,String numeroCertificado) {
		informe.setNumeroCertificadoRTM(numeroCertificado);
	}

	public static void ponerInformacionSoftware( InformeCarInformacionCDADTO informeDieselDTO, String nombreSoftware, String versionSoftware ) {
		
		informeDieselDTO.setVersionDelPrograma("  " +versionSoftware + "  ");
		informeDieselDTO.setNombreDelSoftware(nombreSoftware);
		
	}
	
	public static void ponerInformacionPrueba( InformeCarInformacionCDADTO informeDieselDTO, Prueba prueba) {
		
		SimpleDateFormat sdfPrueba = new SimpleDateFormat("yyyy/MM/dd,HH:mm:ss");		
		informeDieselDTO.setConsecutivoPrueba( String.valueOf( prueba.getPruebaId() ) );
		if (prueba.getFechaInicio() != null)
			informeDieselDTO.setFechaInicioPrueba(  sdfPrueba.format(prueba.getFechaInicio() ) );
		else
			informeDieselDTO.setFechaInicioPrueba("");

		if (prueba.getFechaFinalizacion() != null)
			informeDieselDTO.setFechaFinalPrueba( sdfPrueba.format(prueba.getFechaFinalizacion() ) );
		else
			informeDieselDTO.setFechaFinalPrueba("");

		if (prueba.getFechaAborto() != null)
			informeDieselDTO.setFechaAbortoPrueba( sdfPrueba.format(prueba.getFechaAborto() ) );
		else
			informeDieselDTO.setFechaAbortoPrueba("");
		if (prueba.isAbortada()) {
			if (prueba.getFechaAborto() == null)
				informeDieselDTO.setFechaAbortoPrueba( sdfPrueba.format( prueba.getFechaFinalizacion() ) );
		}

		if (prueba.getUsuario() != null && prueba.getUsuario().getCedula() != null)
			informeDieselDTO.setInspectorRealizaPrueba( String.valueOf(prueba.getUsuario().getCedula() ) );
		else
			informeDieselDTO.setInspectorRealizaPrueba("");

		if (prueba.getCausaAborto() != null)
			informeDieselDTO.setCausaAbortoPrueba( String.valueOf(prueba.getCausaAborto().getCausaAbortoId() ) );
		else
			informeDieselDTO.setCausaAbortoPrueba( "" );
		
		String resultado =  prueba.isAprobada() ? "1" : "2";
		resultado =	 prueba.isAbortada() ? "3" : resultado;
		informeDieselDTO.setResultadoPrueba(resultado);
		
	}
	
	public static void cargarInformacionPropietario(InformeCarInformacionCDADTO informeDieselDTO, Propietario propietario) {

		
		if (propietario != null) {
			informeDieselDTO.setNombreRazonSocial( propietario.getNombres() + " "
					+ propietario.getApellidos() );
			informeDieselDTO.setTipoDocumento( propietario.getTipoDocumentoIdentidad().getCodigoParametricoRunt() );
			informeDieselDTO.setNumeroDocumento( String.valueOf(propietario.getPropietarioId() ) );
			informeDieselDTO.setDireccion( propietario.getDireccion() );
			informeDieselDTO.setTelefono( propietario.getTelefono1() );
			informeDieselDTO.setCiudadPropietario( String.valueOf(propietario.getCiudad().getCiudadId() ) );
			
		}
	}
	
	public static void cargarInformacionVehiculo(InformeCarInformacionCDADTO informeDieselDTO, Vehiculo vehiculo) {
		
		informeDieselDTO.setMarca( String.valueOf(vehiculo.getMarca().getMarcaId() ) );
		
		informeDieselDTO.setLinea( String.valueOf(vehiculo.getLineaVehiculo().getCodigoLinea() ) );
		informeDieselDTO.setModelo( String.valueOf(vehiculo.getModelo() ) );
		informeDieselDTO.setPlaca( String.valueOf(vehiculo.getPlaca() ) );
		informeDieselDTO.setCilindraje( vehiculo.getCilindraje() == null ? "0":String.valueOf(vehiculo.getCilindraje()) );
		informeDieselDTO.setClaseVehiculo ( String.valueOf(vehiculo.getClaseVehiculo().getClaseVehiculoId() ) );
		informeDieselDTO.setServicio( String.valueOf(vehiculo.getServicio().getServicioId() ) );

		informeDieselDTO.setCombustible( String.valueOf(vehiculo.getTipoCombustible().getCombustibleId() ) );
		informeDieselDTO.setNumeroMotor( vehiculo.getNumeroMotor() != null ? vehiculo.getNumeroMotor() : "" );
		informeDieselDTO.setVin( vehiculo.getVin() != null ? vehiculo.getVin() : "" );
		informeDieselDTO.setLicenciaTransito( vehiculo.getNumeroLicencia() != null ? vehiculo.getNumeroLicencia() : "" );
		informeDieselDTO.setModificacionesMotor( NO );// vehiculo.getModificacionesMotor()? SI :
		// NO;// Modificaciones
		informeDieselDTO.setKilometraje( vehiculo.getKilometraje()==null ? "0": String.valueOf(vehiculo.getKilometraje() ) );
		

		informeDieselDTO.setPotenciaDelMotor( "" );// String.valueOf(vehiculo.getPotenciaMotor());//
									// Potencia del motor
		String diametroString = vehiculo.getDiametroExosto() != null ? String.valueOf(vehiculo.getDiametroExosto())
				: " ";
		informeDieselDTO.setLtoeDiametro(diametroString);
		
		
		informeDieselDTO.setTiemposMotor( String.valueOf( vehiculo.getTiemposMotor()  ) + "T" );
		informeDieselDTO.setDisenio( vehiculo.getDiseno().equalsIgnoreCase("SCOOTER") ? "2": "1" );

	}
	
	public static void cargarInformacionResultado(InformeDieselDTO informeDieselDTO, ResultadoDieselDTO resultado,ResultadoDieselDTOInformes r2) {
		
		informeDieselDTO.setTemperaturaAmbiente(r2.getTemperaturaAmbiental());
		informeDieselDTO.setHumedadRelativa(r2.getHumedadRelativa());
		
		informeDieselDTO.setTemperaturaInicialMotor( resultado.getTemperaturaInicial() );
		informeDieselDTO.setRpmRalenti( r2.getRpmRalenti() );
		informeDieselDTO.setRpmGobernadaMedida(resultado.getRpmGobernada());
		informeDieselDTO.setResultadoCicloPreliminar( resultado.getAceleracionCero() );	
		informeDieselDTO.setDensidadHumoCicloPreliminar( r2.getDensidadHumoCiclo0());
		informeDieselDTO.setRpmGobernadaCicloPreliminar( r2.getRpmGobernadaCiclo0() );
		
		informeDieselDTO.setResultadoPrimerCiclo( resultado.getAceleracionUno() );
		informeDieselDTO.setDensidadHumoPrimerCiclo( r2.getDensidadHumoCiclo1() );
		informeDieselDTO.setRpmGobernadaPrimerCiclo( r2.getRpmGobernadaCiclo1() );
		
		informeDieselDTO.setResultadoOpacidadSegundoCiclo( resultado.getAceleracionDos());
		informeDieselDTO.setRpmGobernadaSegundoCiclo( r2.getRpmGobernadaCiclo2() );
		informeDieselDTO.setDensidadHumoSegundoCiclo( r2.getDensidadHumoCiclo2() );
		
		informeDieselDTO.setResultadoOpacidadTercerCiclo( resultado.getAceleracionTres());
		informeDieselDTO.setRpmGobernadaTercerCiclo( r2.getRpmGobernadaCiclo3() );
		informeDieselDTO.setDensidadHumoTercerciclo( r2.getDensidadHumoCiclo3());
		
		informeDieselDTO.setResultadoFinal(resultado.getValorFinal());
		informeDieselDTO.setDensidadHumoFinal( r2.getDensidadHumoFinal());
		informeDieselDTO.setTempFinalMotor( r2.getTemperaturaFinal() );
		
		informeDieselDTO.setFugasTuboEscape(resultado.getFugasTuboEscape().equalsIgnoreCase("SI")?"SI":"NO");
		informeDieselDTO.setFugasSilenciador(resultado.getFugasSilenciador().equalsIgnoreCase("SI")?"SI":"NO");
		informeDieselDTO.setPresenciaTapaCombustible(resultado.getTapaCombustible().equalsIgnoreCase("SI")?"SI":"NO");
		informeDieselDTO.setPresenciaTapaAceite(resultado.getTapaAceite().equalsIgnoreCase("SI")?"SI":"NO");
		informeDieselDTO.setAccesoriosImpiden(resultado.getSistemaMuestreo().equalsIgnoreCase("SI")?"SI":"NO");
		informeDieselDTO.setSalidasAdicionales(resultado.getSalidasAdicionales().equalsIgnoreCase("SI")?"SI":"NO");
		informeDieselDTO.setFiltroAire(resultado.getFiltroAire().equalsIgnoreCase("SI")?"SI":"NO");
		informeDieselDTO.setSistemaRegrigeracion(resultado.getSistemaRefrigeracion().equalsIgnoreCase("SI")?"SI":"NO");
		informeDieselDTO.setRevolucionesInestables(resultado.getRevolucionesFueraRango().equalsIgnoreCase("SI")?"SI":"NO");
		informeDieselDTO.setVelocidadNoAlcanzada5Seg(r2.getVelocidadNoAlcanzada5Seg().equalsIgnoreCase("SI")?"SI":"NO");
		informeDieselDTO.setMalFuncionamientoMotor(r2.getMalFuncionamientoMotor().equalsIgnoreCase("SI")?"SI":"NO");
		informeDieselDTO.setIncumplimientoNiveles(r2.getIncumplimientoNiveles().equalsIgnoreCase("SI")?"SI":"NO");
		informeDieselDTO.setFugasTuvoUnionesMultipleSilenciador(r2.getFugasTuvoUnionesMultipleSilenciador().equalsIgnoreCase("SI")?"SI":"NO");
		informeDieselDTO.setSalidasAdicionales(resultado.getSalidasAdicionales().equalsIgnoreCase("SI")?"SI":"NO");
		informeDieselDTO.setAusenciaTaponesDeAceite(r2.getAusenciaTaponesDeAceite().equalsIgnoreCase("SI")?"SI":"NO");
		informeDieselDTO.setAusenciaTaponCombustible(r2.getAusenciaTaponCombustible().equalsIgnoreCase("SI")?"SI":"NO");
		informeDieselDTO.setInstalacionAccesoriosODeformaciones(resultado.getSistemaMuestreo().equalsIgnoreCase("SI")?"SI":"NO");
		informeDieselDTO.setIncorrectaOperacionSistemaRefrigeracion( resultado.getSistemaRefrigeracion().equalsIgnoreCase("SI")?"SI":"NO");
		informeDieselDTO.setAusenciaDeFiltroAire(resultado.getFiltroAire().equalsIgnoreCase("SI")?"SI":"NO");
		informeDieselDTO.setInstalacionDispositivosAlteranRpms(r2.getInstalacionDispositivosAlteranRpms().equalsIgnoreCase("SI")?"SI":"NO");
		informeDieselDTO.setDiferenciasAritmeticas(r2.getDiferenciasAritmeticas().equalsIgnoreCase("SI")?"SI":"NO");
		informeDieselDTO.setFallaSubitaMotor(r2.getFallaSubitaMotor().equalsIgnoreCase("SI")?"SI":"NO");
		informeDieselDTO.setDiferenciasTemperaturaMotor( r2.getDiferenciaTemperaturas().equalsIgnoreCase("SI")?"SI":"NO");
		informeDieselDTO.setGobernadorNoLimitaRevoluciones( r2.getGobernadorNoLimitaRevoluciones().equalsIgnoreCase("SI")?"SI":"NO");
		
		
	}


	public static void ponerInformacionVerificacionGasolina(InformeGasolinaCarDTO informe,
			VerificacionGasolina verificacionGasolina) throws ParseException {
		
		Locale locale = new Locale("en","US");
		
		BigDecimal bdHCBajo = new BigDecimal(verificacionGasolina.getValorBajoHc()).setScale(0,RoundingMode.HALF_EVEN);
		String valorHcBajo = NumberFormat.getInstance(locale).parse(bdHCBajo.toString()).toString(); 
		informe.setResultadoVlrSpanBajoHC( valorHcBajo );
		
		BigDecimal bdCOBajo = new BigDecimal(verificacionGasolina.getValorBajoCo()).setScale(2, RoundingMode.HALF_EVEN);
		String valorCOBajo = NumberFormat.getInstance(locale).format(bdCOBajo.doubleValue());
		
		informe.setResultadoVlrSpanBajoCO(valorCOBajo);
		
		BigDecimal bdCO2Bajo = new BigDecimal( verificacionGasolina.getValorBajoCo2()).setScale(1,RoundingMode.HALF_EVEN);
		String valorCO2Bajo = NumberFormat.getInstance(locale).format(bdCO2Bajo.doubleValue());
		informe.setResultadoVlrSpanBajoCO2(valorCO2Bajo);
		
		BigDecimal bdHCAlto = new BigDecimal( verificacionGasolina.getValorAltoHc()).setScale( 0, RoundingMode.HALF_EVEN);
		informe.setResultadoVlrSpanAltoHC( bdHCAlto.toString() );
		
		BigDecimal bdCOAlto = new BigDecimal( verificacionGasolina.getValorAltoCo()).setScale( 2, RoundingMode.HALF_EVEN);
		String valorCOAlto = NumberFormat.getInstance(locale).format(bdCOAlto.doubleValue());
		informe.setResultadoVlrSpanAltoCO( valorCOAlto );
		
		BigDecimal bdCO2Alto = new BigDecimal( verificacionGasolina.getValorAltoCo2()).setScale(1, RoundingMode.HALF_EVEN);
		String valorCO2Alto = NumberFormat.getInstance(locale).format(bdCO2Alto.doubleValue());
		informe.setResultadoVlrSpanAltoCO2( valorCO2Alto );
		
		
		
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
