package com.proambiente.webapp.util.informes;

import java.util.Collections;
import java.util.List;

import com.proambiente.modelo.Medida;
import com.proambiente.modelo.Permisible;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.TipoMedida;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.modelo.dto.ResultadoDieselDTO;
import com.proambiente.webapp.dao.PermisibleFacade;
import com.proambiente.webapp.util.ConstantesMedidasGases;
import com.proambiente.webapp.util.dto.ResultadoDieselDTOInformes;
import com.proambiente.webapp.util.dto.sda.CausaRechazoDieselCarCundinamarca;

/**
 * Codifica la causa de rechazo que requiere la secretaria de ambiente
 * @author fdavi
 *
 */
public class DeterminadorCausaRechazoDieselCar {
	
	

	private PermisibleFacade permisibleService;
	private boolean aprobado;
	private boolean dilucion;
	
	
	
	
	public DeterminadorCausaRechazoDieselCar(PermisibleFacade permisibleService) {
		super();
		this.permisibleService = permisibleService;
	}

	public CausaRechazoDieselCarCundinamarca determinarCausaRechazo(ResultadoDieselDTO resultado, 
															ResultadoDieselDTOInformes resultadosInforme, Prueba prueba,
															List<Medida> medidas,Vehiculo vehiculo) {
		if(prueba.isAprobada()) {
			return CausaRechazoDieselCarCundinamarca.SIN_RECHAZO;
		}else {
			if(resultado.getFugasTuboEscape().equalsIgnoreCase("TRUE")) {
				return CausaRechazoDieselCarCundinamarca.FUGAS_TUBO_ESCAPE;
			}
			if(resultado.getFugasSilenciador().equalsIgnoreCase("TRUE")) {
				return CausaRechazoDieselCarCundinamarca.FUGAS_SILENCIADOR;
			}
			if(resultado.getRevolucionesFueraRango().equalsIgnoreCase("TRUE")) {
				return CausaRechazoDieselCarCundinamarca.REVOLUCIONES_INESTABLES_FUERA_RANGO;
			}
			if(resultado.getTapaAceite().equalsIgnoreCase("TRUE")) {
				return CausaRechazoDieselCarCundinamarca.AUSENCIA_TAPA_ACEITE;
			}
			if(resultado.getTapaCombustible().equalsIgnoreCase("TRUE")) {
				return CausaRechazoDieselCarCundinamarca.AUSENCIA_TAPA_COMBUSTIBLE;				
			}
			if(resultado.getSalidasAdicionales().equalsIgnoreCase("TRUE")) {
				return CausaRechazoDieselCarCundinamarca.SALIDAS_ADICIONALES;
			}
			if(resultadosInforme.getAusenciaDeFiltroAire().equalsIgnoreCase("TRUE")) {
				return CausaRechazoDieselCarCundinamarca.FALLA_FILTRO_AIRE;
			}
			if(resultadosInforme.getInstalacionAccesoriosODeformaciones().equals("TRUE")) {
				return CausaRechazoDieselCarCundinamarca.ACCESORIOS_DEFORMACIONES_IMPIDEN;
			}
			if(resultado.getSalidasAdicionales().equalsIgnoreCase("TRUE")) {
				return CausaRechazoDieselCarCundinamarca.FALLA_SISTEMA_REFRIGERACION;
			}
			if(resultadosInforme.getInstalacionDispositivosAlteranRpms().equalsIgnoreCase("TRUE")) {
				return CausaRechazoDieselCarCundinamarca.DISPOSITIVOS_OBSTACULOS_IMPIDEN_ACELERAR;
			}			
			if(resultadosInforme.getGobernadorNoLimitaRevoluciones().equalsIgnoreCase("TRUE")) {
				return CausaRechazoDieselCarCundinamarca.GOBERNADOR_NO_LIMITA;
			}
			if(resultadosInforme.getVelocidadNoAlcanzada5Seg().equalsIgnoreCase("TRUE")) {
				return CausaRechazoDieselCarCundinamarca.NO_SE_ALCANZA_REVOLUCIONES_GOBERNADAS;
			}
			if(resultadosInforme.getMalFuncionamientoMotor().equalsIgnoreCase("TRUE")) {
				return CausaRechazoDieselCarCundinamarca.MAL_FUNCIONAMIENTO_MOTOR;
			}
			if(resultadosInforme.getFallaSubitaMotor().equalsIgnoreCase("TRUE")) {
				return CausaRechazoDieselCarCundinamarca.FALLA_SUBITA_MOTOR;
			}
			if(resultadosInforme.getDiferenciasAritmeticas().equalsIgnoreCase("TRUE")) {
				return CausaRechazoDieselCarCundinamarca.DIFERENCIA_ARITMETICA;
			}
			if(resultadosInforme.getInestabilidadRevoluciones().equalsIgnoreCase("TRUE")) {
				return CausaRechazoDieselCarCundinamarca.INESTABILIDAD_RPMS_CICLOS_MEDICION;
			}
			if(resultadosInforme.getDiferenciaTemperaturas().equalsIgnoreCase("TRUE")) {
				return CausaRechazoDieselCarCundinamarca.DIFERENCIA_TEMPERATURA;
			}			
			return determinarCausalPorMedida(prueba, vehiculo, medidas);			
		}		
			
	}
	
	
	
	
	private CausaRechazoDieselCarCundinamarca  determinarCausalPorMedida(Prueba prueba, Vehiculo v, List<Medida> medidas) {
		
		 
		 Medida MEDIDA_MAXIMO_CICLOS = new Medida(); 
		 TipoMedida tipoMedidaMaximoCiclos = new TipoMedida();
		 tipoMedidaMaximoCiclos.setTipoMedidaId( ConstantesMedidasGases.CODIGO_RESULTADO_DENSIDAD_HUMO);
		 MEDIDA_MAXIMO_CICLOS.setTipoMedida(tipoMedidaMaximoCiclos);
		
		
		 	     
	     
        CausaRechazoDieselCarCundinamarca causaRechazo = CausaRechazoDieselCarCundinamarca.Indeterminado ;
        
        Permisible permisible = permisibleService.buscarPermisible(MEDIDA_MAXIMO_CICLOS.getTipoMedida().getTipoMedidaId(), v.getTipoVehiculo().getTipoVehiculoId(), v.getModelo());
		
        
        Collections.sort(medidas, (m1,m2)->{return m1.getTipoMedida().getTipoMedidaId().compareTo( m2.getTipoMedida().getTipoMedidaId() );});
        
        Medida medidaMaximoCiclos = buscarMedida(medidas, MEDIDA_MAXIMO_CICLOS,false);
        if(medidaMaximoCiclos == null)
        	return null;
        if ( medidaMaximoCiclos.getValor() >= permisible.getValorMinimo() && medidaMaximoCiclos.getValor() <= permisible.getValorMaximo()) {

        } else {
            aprobado = false;
            causaRechazo = seleccionarCausaRechazoPorModeloPorCilindraje(v.getModelo(),v.getCilindraje());
            
        }   
        
		return causaRechazo;
        
    }

	private CausaRechazoDieselCarCundinamarca seleccionarCausaRechazoPorModeloPorCilindraje(Integer modelo,Integer cilindraje) {
		CausaRechazoDieselCarCundinamarca causaRechazo = null;
		if(cilindraje < 5000) {
			if(modelo <= 2000) {
				causaRechazo = CausaRechazoDieselCarCundinamarca.DENSIDAD_modelo_MENOR_2000_CC_menor_5000;
			}else if( modelo > 2000 && modelo < 2016 ) {
				causaRechazo = CausaRechazoDieselCarCundinamarca.DENSIDAD_modelo_entre_2001_2015_CC_menor_5000;
			}else if( modelo >= 2016 ) {
				causaRechazo = CausaRechazoDieselCarCundinamarca.DENSIDAD_modelo_MAYOR_2016_CC_MENOR_5000;
			}
			
		}else {
			if(modelo <= 2000) {
				causaRechazo = CausaRechazoDieselCarCundinamarca.DENSIDAD_modelo_MENOR_2000_CC_mayorigual_5000;
			}else if( modelo > 2000 && modelo < 2016 ) {
				causaRechazo = CausaRechazoDieselCarCundinamarca.DENSIDAD_modelo_entre_2001_2015_CC_mayorigual_5000;
			}else if( modelo >= 2016 ) {
				causaRechazo = CausaRechazoDieselCarCundinamarca.DENSIDAD_modelo_MAYOR_2016_CC_MAYORIGUAL_5000;
			}
		}
		
		
		return causaRechazo;		
	}
	
	private Medida buscarMedida(List<Medida> medidas, Medida medidaD, boolean requerida) throws RuntimeException {

        int indice = Collections.binarySearch(medidas, medidaD,(m1,m2)->{return m1.getTipoMedida().getTipoMedidaId().compareTo( m2.getTipoMedida().getTipoMedidaId() );});
        if (indice < 0 && requerida) {
            throw new RuntimeException("No se encuentra medida para evaluar la prueba" + medidaD.getTipoMedida().getTipoMedidaId() );
        } else {
            if (indice >= 0) {
                Medida medida = medidas.get(indice);
                return medida;
            } else {
            	System.out.println( "Tamanio medias" + medidas.size() );
                return null;
            }
        }
    }
	
}
