package com.proambiente.webapp.util.informes;

import java.util.Collections;
import java.util.List;

import com.proambiente.modelo.Medida;
import com.proambiente.modelo.Permisible;
import com.proambiente.modelo.Prueba;
import com.proambiente.modelo.TipoMedida;
import com.proambiente.modelo.Vehiculo;
import com.proambiente.modelo.dto.ResultadoOttoVehiculosDTO;
import com.proambiente.webapp.dao.PermisibleFacade;
import com.proambiente.webapp.util.ConstantesMedidasGases;
import com.proambiente.webapp.util.dto.ResultadoOttoInformeDTO;
import com.proambiente.webapp.util.dto.sda.CausaRechazoVehiculosOttoCarCundinamarca;

/**
 * Codifica la causa de rechazo que requiere la secretaria de ambiente
 * @author fdavi
 *
 */
public class DeterminadorCausaRechazoVehiculosOttoCar {
	
	

	private PermisibleFacade permisibleService;
	private boolean aprobado;
	private boolean dilucion;
	
	
	
	
	public DeterminadorCausaRechazoVehiculosOttoCar(PermisibleFacade permisibleService) {
		super();
		this.permisibleService = permisibleService;
	}

	public CausaRechazoVehiculosOttoCarCundinamarca determinarCausaRechazo(ResultadoOttoVehiculosDTO resultadoVehiculos, 
															ResultadoOttoInformeDTO resultadosInforme, Prueba prueba,
															List<Medida> medidas,Vehiculo vehiculo) {
		if(prueba.isAprobada()) {
			return CausaRechazoVehiculosOttoCarCundinamarca.SIN_RECHAZO;
		}else {
			if(resultadoVehiculos.getFugasTuboEscape().equalsIgnoreCase("TRUE")) {
				return CausaRechazoVehiculosOttoCarCundinamarca.FUGAS_TUBO_ESCAPE;
			}
			if(resultadoVehiculos.getFugasSilenciador().equalsIgnoreCase("TRUE")) {
				return CausaRechazoVehiculosOttoCarCundinamarca.FUGAS_SILENCIADOR;
			}
			if(resultadoVehiculos.getTapaAceite().equalsIgnoreCase("TRUE")) {
				return CausaRechazoVehiculosOttoCarCundinamarca.AUSENCIA_TAPA_ACEITE;
			}
			if(resultadoVehiculos.getTapaCombustible().equalsIgnoreCase("TRUE")) {
				return CausaRechazoVehiculosOttoCarCundinamarca.AUSENCIA_TAPA_COMBUSTIBLE;				
			}
			if(resultadoVehiculos.getSalidasAdicionales().equalsIgnoreCase("TRUE")) {
				return CausaRechazoVehiculosOttoCarCundinamarca.SALIDAS_ADICIONALES;
			}

			if(resultadosInforme.getFallaFiltroAire().equalsIgnoreCase("TRUE")) {
				return CausaRechazoVehiculosOttoCarCundinamarca.FALLA_FILTRO_AIRE;
			}
			if(resultadosInforme.getPresenciaAccesoriosImpiden().equals("TRUE")) {
				return CausaRechazoVehiculosOttoCarCundinamarca.ACCESORIOS_DEFORMACIONES_IMPIDEN;
			}
			
			if(resultadoVehiculos.getPresenciaHumos().equalsIgnoreCase("TRUE")) {
				return CausaRechazoVehiculosOttoCarCundinamarca.HUMO_NEGRO_AZUL;
			}
			if(resultadoVehiculos.getRevolucionesFueraRango().equalsIgnoreCase("TRUE")) {
				return CausaRechazoVehiculosOttoCarCundinamarca.REVOLUCIONES_FUERA_RANGO;
			}
			if(resultadoVehiculos.getDesconexionPCV().equalsIgnoreCase("TRUE")) {
				return CausaRechazoVehiculosOttoCarCundinamarca.DESCONEXION_PCV;
			}
			if(resultadoVehiculos.getSalidasAdicionales().equalsIgnoreCase("TRUE")) {
				return CausaRechazoVehiculosOttoCarCundinamarca.FALLA_SISTEMA_REFRIGERACION;
			}
			
			return determinarCausalPorMedida(prueba, vehiculo, medidas);			
		}		
			
	}
	
	
	
	
	private CausaRechazoVehiculosOttoCarCundinamarca  determinarCausalPorMedida(Prueba prueba, Vehiculo v, List<Medida> medidas) {
		
		 Medida MEDIDA_HC_RALENTI = new Medida();
		 TipoMedida tipoMedidaHcRalenti4t = new TipoMedida();
		 tipoMedidaHcRalenti4t.setTipoMedidaId( ConstantesMedidasGases.CODIGO_MEDIDA_HC_RALENTI);
		 MEDIDA_HC_RALENTI.setTipoMedida(tipoMedidaHcRalenti4t);
		 
	     Medida MEDIDA_CO_RALENTI = new Medida();
	     TipoMedida tipoMedidaCORalenti4t = new TipoMedida();
	     tipoMedidaCORalenti4t.setTipoMedidaId( ConstantesMedidasGases.CODIGO_MEDIDA_CO_RALENTI);
	     MEDIDA_CO_RALENTI.setTipoMedida(tipoMedidaCORalenti4t);

	     Medida MEDIDA_HC_CRUCERO = new Medida();
	     TipoMedida tipoMedidaHcCrucero = new TipoMedida();
	     tipoMedidaHcCrucero.setTipoMedidaId( ConstantesMedidasGases.CODIGO_MEDIDA_HC_CRUCERO );
	     MEDIDA_HC_CRUCERO.setTipoMedida(tipoMedidaHcCrucero);
	     
	     Medida MEDIDA_CO_CRUCERO = new Medida();
	     TipoMedida tipoMedidaCoCrucero = new TipoMedida();
	     tipoMedidaCoCrucero.setTipoMedidaId( ConstantesMedidasGases.CODIGO_MEDIDA_CO_CRUCERO);
	     MEDIDA_CO_CRUCERO.setTipoMedida(tipoMedidaCoCrucero);
	     
	     Medida MEDIDA_CO2_RALENTI = new Medida();
	     TipoMedida tipoMedidaCo2Ralenti = new TipoMedida();
	     tipoMedidaCo2Ralenti.setTipoMedidaId( ConstantesMedidasGases.CODIGO_MEDIDA_CO2_RALENTI);
	     MEDIDA_CO2_RALENTI.setTipoMedida(tipoMedidaCo2Ralenti);

	     Medida MEDIDA_CO2_CRUCERO = new Medida();
	     TipoMedida tipoMedidaCo2Crucero = new TipoMedida();
	     tipoMedidaCo2Crucero.setTipoMedidaId( ConstantesMedidasGases.CODIGO_MEDIDA_CO2_CRUCERO);
	     MEDIDA_CO2_CRUCERO.setTipoMedida(tipoMedidaCo2Crucero);
	     
	     Medida MEDIDA_O2_RALENTI = new Medida();
	     TipoMedida tipoMedidaO2Ralenti = new TipoMedida();
	     tipoMedidaO2Ralenti.setTipoMedidaId( ConstantesMedidasGases.CODIGO_MEDIDA_O2_RALENTI);
	     MEDIDA_O2_RALENTI.setTipoMedida(tipoMedidaO2Ralenti);
	     
	     Medida MEDIDA_O2_CRUCERO = new Medida();
	     TipoMedida tipoMedidaO2Crucero = new TipoMedida();
	     tipoMedidaO2Crucero.setTipoMedidaId( ConstantesMedidasGases.CODIGO_MEDIDA_O2_CRUCERO);
	     MEDIDA_O2_CRUCERO.setTipoMedida(tipoMedidaO2Crucero);
	     
	     
	     
	     
        CausaRechazoVehiculosOttoCarCundinamarca causaRechazo = null ;
        
        
		
        
        Collections.sort(medidas, (m1,m2)->{return m1.getTipoMedida().getTipoMedidaId().compareTo( m2.getTipoMedida().getTipoMedidaId() );});
        Permisible permisible = null;
        if (/*v.getTipoCombustible().getCombustibleId() == ConstantesTiposCombustible.GASOLINA
                || v.getTipoCombustible().getCombustibleId() == ConstantesTiposCombustible.GASOLINA_ELECTRICO*/ true) {
            Medida medidaCO2Ralenti = buscarMedida(medidas, MEDIDA_CO2_RALENTI,true);
            permisible = permisibleService.buscarPermisible(medidaCO2Ralenti.getTipoMedida().getTipoMedidaId(), v.getTipoVehiculo().getTipoVehiculoId(), v.getModelo());
            if (medidaCO2Ralenti.getValor() < permisible.getValorMinimo() || medidaCO2Ralenti.getValor() > permisible.getValorMaximo()) {
                aprobado = false;
                dilucion = true;
                causaRechazo = CausaRechazoVehiculosOttoCarCundinamarca.DILUCION;
                
            }
            Medida medidaCO2Crucero = buscarMedida(medidas, MEDIDA_CO2_CRUCERO,true);
            permisible = permisibleService.buscarPermisible(medidaCO2Crucero.getTipoMedida().getTipoMedidaId(), v.getTipoVehiculo().getTipoVehiculoId(), v.getModelo());
            if (medidaCO2Crucero.getValor() < permisible.getValorMinimo() || medidaCO2Ralenti.getValor() > permisible.getValorMaximo()) {
                aprobado = false;
                dilucion = true;
                causaRechazo = CausaRechazoVehiculosOttoCarCundinamarca.DILUCION;
                
            }

            Medida medidaO2Ralenti = buscarMedida(medidas, MEDIDA_O2_RALENTI,true);
            permisible = permisibleService.buscarPermisible(medidaO2Ralenti.getTipoMedida().getTipoMedidaId(), v.getTipoVehiculo().getTipoVehiculoId(), v.getModelo());
            if (medidaO2Ralenti.getValor() < permisible.getValorMinimo() || medidaO2Ralenti.getValor() > permisible.getValorMaximo()) {
                aprobado = false;
                dilucion = true;
                causaRechazo = CausaRechazoVehiculosOttoCarCundinamarca.DILUCION;
                
            }
            Medida medidaO2Crucero = buscarMedida(medidas, MEDIDA_O2_CRUCERO,true);
            permisible = permisibleService.buscarPermisible(medidaO2Crucero.getTipoMedida().getTipoMedidaId(), v.getTipoVehiculo().getTipoVehiculoId(), v.getModelo());
            if (medidaO2Crucero.getValor() < permisible.getValorMinimo() || medidaO2Crucero.getValor() > permisible.getValorMaximo()) {
                aprobado = false;
                dilucion = true;
                causaRechazo = CausaRechazoVehiculosOttoCarCundinamarca.DILUCION;
               
            }
        }
        
        Medida medidaHCRalenti = buscarMedida(medidas, MEDIDA_HC_RALENTI,true);
        permisible = permisibleService.buscarPermisible(MEDIDA_HC_RALENTI.getTipoMedida().getTipoMedidaId(), v.getTipoVehiculo().getTipoVehiculoId(), v.getModelo());
        if (medidaHCRalenti.getValor() >= permisible.getValorMinimo() && medidaHCRalenti.getValor() <= permisible.getValorMaximo()) {

        } else {
            aprobado = false;
            causaRechazo = seleccionarCausaRechazoHCRalenti(v.getModelo());
            
        }
        Medida medidaCORalenti = buscarMedida(medidas, MEDIDA_CO_RALENTI,true);
        permisible = permisibleService.buscarPermisible(medidaCORalenti.getTipoMedida().getTipoMedidaId(), v.getTipoVehiculo().getTipoVehiculoId(), v.getModelo());
        if (medidaCORalenti.getValor() < permisible.getValorMinimo() || medidaCORalenti.getValor() > permisible.getValorMaximo()) {
            aprobado = false;
            causaRechazo = seleccionarCausaRechazoCORalenti(v.getModelo());
            
        }
        Medida medidaHCCrucero = buscarMedida(medidas, MEDIDA_HC_CRUCERO,true);
        permisible = permisibleService.buscarPermisible(medidaHCCrucero.getTipoMedida().getTipoMedidaId(), v.getTipoVehiculo().getTipoVehiculoId(), v.getModelo());
        if (medidaHCCrucero.getValor() < permisible.getValorMinimo() || medidaHCCrucero.getValor() > permisible.getValorMaximo()) {
            aprobado = false;
            causaRechazo = seleccionarCausaRechazoHC(v.getModelo());
            
        }
        Medida medidaCOCrucero = buscarMedida(medidas, MEDIDA_CO_CRUCERO,true);
        permisible = permisibleService.buscarPermisible(medidaCOCrucero.getTipoMedida().getTipoMedidaId(), v.getTipoVehiculo().getTipoVehiculoId(), v.getModelo());
        if (medidaCOCrucero.getValor() < permisible.getValorMinimo() || medidaCOCrucero.getValor() > permisible.getValorMaximo()) {
            aprobado = false;
            causaRechazo = seleccionarCausaRechazoCO(v.getModelo());
            
        }

        
        
        
		return causaRechazo;
        
    }
	
	private CausaRechazoVehiculosOttoCarCundinamarca seleccionarCausaRechazoCORalenti(Integer modelo) {

		CausaRechazoVehiculosOttoCarCundinamarca causaRechazo = null;
		if(modelo < 1985) {
			causaRechazo = CausaRechazoVehiculosOttoCarCundinamarca.CO_RALENTI_1984;
		}else if (modelo >= 1985 && modelo <1998) {
			causaRechazo = CausaRechazoVehiculosOttoCarCundinamarca.CO_RALENTI_1985_1997;
		}else if (modelo >= 1998 && modelo < 2010) {
			causaRechazo = CausaRechazoVehiculosOttoCarCundinamarca.CO_RALENTI_1998_2009;
		}else {
			causaRechazo = CausaRechazoVehiculosOttoCarCundinamarca.CO_RALENTI_2010;
		}
		return causaRechazo;
	}

	private CausaRechazoVehiculosOttoCarCundinamarca seleccionarCausaRechazoHCRalenti(Integer modelo) {

		CausaRechazoVehiculosOttoCarCundinamarca causaRechazo = null;
		if(modelo < 1985) {
			causaRechazo = CausaRechazoVehiculosOttoCarCundinamarca.HC_RALENTI_1984;
		}else if (modelo >= 1985 && modelo <1998) {
			causaRechazo = CausaRechazoVehiculosOttoCarCundinamarca.HC_RALENTI_1985_1997;
		}else if (modelo >= 1998 && modelo<2010) {
			causaRechazo = CausaRechazoVehiculosOttoCarCundinamarca.HC_RALENTI_1998_2009;
		}else {
			causaRechazo = CausaRechazoVehiculosOttoCarCundinamarca.HC_RALENTI_2010;
		}
		return causaRechazo;
	}

	private CausaRechazoVehiculosOttoCarCundinamarca seleccionarCausaRechazoCO(Integer modelo) {
		CausaRechazoVehiculosOttoCarCundinamarca causaRechazo = null;
		if(modelo < 1985) {
			causaRechazo = CausaRechazoVehiculosOttoCarCundinamarca.CO_CRUCERO_1984;
		}else if (modelo >= 1985 && modelo <1998) {
			causaRechazo = CausaRechazoVehiculosOttoCarCundinamarca.CO_CRUCERO_1985_1997;
		}else if (modelo >= 1998 && modelo<2010) {
			causaRechazo = CausaRechazoVehiculosOttoCarCundinamarca.CO_CRUCERO_1998_2009;
		}else {
			causaRechazo = CausaRechazoVehiculosOttoCarCundinamarca.CO_CRUCERO_2010;
		}
		return causaRechazo;
	}

	private CausaRechazoVehiculosOttoCarCundinamarca seleccionarCausaRechazoHC(Integer modelo) {
		CausaRechazoVehiculosOttoCarCundinamarca causaRechazo = null;
		if(modelo < 1985) {
			causaRechazo = CausaRechazoVehiculosOttoCarCundinamarca.HC_CRUCERO_1984;
		}else if (modelo >= 1985 && modelo <1998) {
			causaRechazo = CausaRechazoVehiculosOttoCarCundinamarca.HC_CRUCERO_1985_1997;
		}else if (modelo >= 1998 && modelo < 2010) {
			causaRechazo = CausaRechazoVehiculosOttoCarCundinamarca.HC_CRUCERO_1998_2009;
		}else {
			causaRechazo = CausaRechazoVehiculosOttoCarCundinamarca.HC_CRUCERO_2010;
		}
		return causaRechazo;
	}

	private Medida buscarMedida(List<Medida> medidas, Medida medidaD, boolean requerida) throws RuntimeException {

        int indice = Collections.binarySearch(medidas, medidaD,(m1,m2)->{return m1.getTipoMedida().getTipoMedidaId().compareTo( m2.getTipoMedida().getTipoMedidaId() );});
        if (indice < 0 && requerida) {
            throw new RuntimeException("No se encuentra medida para evaluar la prueba" + medidaD.getTipoMedida().getTipoMedidaId() );
        } else {
            if (indice >= 0) {
                Medida medida = medidas.get(indice);
                return medida;
            } else {
                throw new RuntimeException("Erro en busqueda de medida");
            }
        }
    }
	
	
}
