package com.proambiente.webapp.util;

public class ConstantesMedidasPresionLlanta {

	public static final int P_EJE_1_LL1_DER = 1000;
    public static final int P_EJE_1_LL2_DER = 1001;
    public static final int P_EJE_1_LL3_DER = 1002;
    public static final int P_EJE_1_LL4_DER = 1003;
    public static final int P_EJE_1_LL5_DER = 1004;
    public static final int P_EJE_1_LL1_IZQ = 1005;
    public static final int P_EJE_1_LL2_IZQ = 1006;
    public static final int P_EJE_1_LL3_IZQ = 1007;
    public static final int P_EJE_1_LL4_IZQ = 1008;
    public static final int P_EJE_1_LL5_IZQ = 1009;

    public static final int P_EJE_2_LL1_DER = 1010;
    public static final int P_EJE_2_LL2_DER = 1011;
    public static final int P_EJE_2_LL3_DER = 1012;
    public static final int P_EJE_2_LL4_DER = 1013;
    public static final int P_EJE_2_LL5_DER = 1014;
    public static final int P_EJE_2_LL1_IZQ = 1015;
    public static final int P_EJE_2_LL2_IZQ = 1016;
    public static final int P_EJE_2_LL3_IZQ = 1017;
    public static final int P_EJE_2_LL4_IZQ = 1018;
    public static final int P_EJE_2_LL5_IZQ = 1019;

    public static final int P_EJE_3_LL1_DER = 1020;
    public static final int P_EJE_3_LL2_DER = 1021;
    public static final int P_EJE_3_LL3_DER = 1022;
    public static final int P_EJE_3_LL4_DER = 1023;
    public static final int P_EJE_3_LL5_DER = 1024;
    public static final int P_EJE_3_LL1_IZQ = 1025;
    public static final int P_EJE_3_LL2_IZQ = 1026;
    public static final int P_EJE_3_LL3_IZQ = 1027;
    public static final int P_EJE_3_LL4_IZQ = 1028;
    public static final int P_EJE_3_LL5_IZQ = 1029;

    public static final int P_EJE_4_LL1_DER = 1030;
    public static final int P_EJE_4_LL2_DER = 1031;
    public static final int P_EJE_4_LL3_DER = 1032;
    public static final int P_EJE_4_LL4_DER = 1033;
    public static final int P_EJE_4_LL5_DER = 1034;
    public static final int P_EJE_4_LL1_IZQ = 1035;
    public static final int P_EJE_4_LL2_IZQ = 1036;
    public static final int P_EJE_4_LL3_IZQ = 1037;
    public static final int P_EJE_4_LL4_IZQ = 1038;
    public static final int P_EJE_4_LL5_IZQ = 1039;

    public static final int P_EJE_5_LL1_DER = 1040;
    public static final int P_EJE_5_LL2_DER = 1041;
    public static final int P_EJE_5_LL3_DER = 1042;
    public static final int P_EJE_5_LL4_DER = 1043;
    public static final int P_EJE_5_LL5_DER = 1044;
    public static final int P_EJE_5_LL1_IZQ = 1045;
    public static final int P_EJE_5_LL2_IZQ = 1046;
    public static final int P_EJE_5_LL3_IZQ = 1047;
    public static final int P_EJE_5_LL4_IZQ = 1048;
    public static final int P_EJE_5_LL5_IZQ = 1049;

    public static final int  P_LLANTA_REPUESTO_1 = 1050;
    public static final int  P_LLANTA_REPUESTO_2 = 1051;

	
}
