package com.proambiente.webapp.util;

public class ConstantesMedidasLuces {
	
	public static final int INTENSIDAD_BAJA_DERECHA = 2006;
    public static final int INTENSIDAD_BAJA_IZQUIERDA = 2009;
    public static final int INTENSIDAD_ALTA_DERECHA = 2007;
    public static final int INTENSIDAD_ALTA_IZQUIERDA = 2010;
    public static final int INCLINACION_BAJA_DERECHA = 2003;
    public static final int INCLINACION_BAJA_IZQUIERDA = 2005;
    public static final int INTENSIDAD_EXPLORADORA_DERECHA = 2301;
    public static final int INTENSIDAD_EXPLORADORA_IZQUIERDA = 2302;
    
    
    public static final int INTENSIDAD_BAJA_DERECHA_DOS = 2022;
    public static final int INTENSIDAD_BAJA_IZQUIERDA_DOS = 2023;
    public static final int INTENSIDAD_ALTA_DERECHA_DOS = 2026;
    public static final int INTENSIDAD_ALTA_IZQUIERDA_DOS = 2027;
    public static final int INCLINACION_BAJA_DERECHA_DOS = 2024;
    public static final int INCLINACION_BAJA_IZQUIERDA_DOS = 2025;
    public static final int INTENSIDAD_EXPLORADORA_DERECHA_DOS = 2028;
    public static final int INTENSIDAD_EXPLORADORA_IZQUIERDA_DOS = 2029;
    
    public static final int INTENSIDAD_BAJA_DERECHA_TRES = 2030;
    public static final int INTENSIDAD_BAJA_IZQUIERDA_TRES = 2031;
    public static final int INTENSIDAD_ALTA_DERECHA_TRES = 2034;
    public static final int INTENSIDAD_ALTA_IZQUIERDA_TRES = 2035;
    public static final int INCLINACION_BAJA_DERECHA_TRES = 2032;
    public static final int INCLINACION_BAJA_IZQUIERDA_TRES = 2033;
    public static final int INTENSIDAD_EXPLORADORA_DERECHA_TRES = 2036;
    public static final int INTENSIDAD_EXPLORADORA_IZQUIERDA_TRES = 2037;
    
    public static final int LUCES_BAJAS_ALTAS_EXPL_SIMULTANEAS = 2038;//si si si
    public static final int LUCES_BAJAS_EXPL_SIMULTANEAS = 2039;//si no si
    public static final int LUCES_ALTAS_EXPL_SIMULTANEAS = 2040;//no si si
    public static final int LUCES_NINGUNA_SIMULTANEA = 2041;//no no no
    public static final int LUCES_BAJAS_ALTAS_SIMUL_NO_EXPL = 2042;//si si vacio
    public static final int LUCES_BAJAS_ALTAS_NO_SIMUL_NO_EXPL = 2043;// no no vacio
    
    public static final int INTENSIDAD_BAJA_DERECHA_CUATRIMOTO = 2119;
    public static final int INTENSIDAD_BAJA_IZQUIERDA_CUATRIMOTO = 2123;
    public static final int INTENSIDAD_ALTA_DERECHA_CUATRIMOTO = 2121;
    public static final int INTENSIDAD_ALTA_IZQUIERDA_CUATRIMOTO = 2125;
    public static final int INCLINACION_BAJA_DERECHA_CUATRIMOTO = 2120;
    public static final int INCLINACION_BAJA_IZQUIERDA_CUATRIMOTO = 2124;
    public static final int INTENSIDAD_EXPLORADORA_DERECHA_CUATRIMOTO = 2122;
    public static final int INTENSIDAD_EXPLORADORA_IZQUIERDA_CUATRIMOTO = 2126;
    public static final int SUMA_LUCES_CUATRIMOTO = 2127;
    
    public static final int INTENSIDAD_BAJA_DERECHA_CUADRICICLO = 2128;
    public static final int INTENSIDAD_BAJA_IZQUIERDA_CUADRICICLO = 2132;
    public static final int INTENSIDAD_ALTA_DERECHA_CUADRICICLO = 2130;
    public static final int INTENSIDAD_ALTA_IZQUIERDA_CUADRICICLO = 2134;
    public static final int INCLINACION_BAJA_DERECHA_CUADRICICLO = 2129;
    public static final int INCLINACION_BAJA_IZQUIERDA_CUADRICICLO = 2133;
    public static final int INTENSIDAD_EXPLORADORA_DERECHA_CUADRICICLO = 2131;
    public static final int INTENSIDAD_EXPLORADORA_IZQUIERDA_CUADRICICLO = 2135;
    public static final int SUMA_LUCES_CUADRICICLO = 2136;
    
    public static final int INTENSIDAD_BAJA_DERECHA_MOTOTRICICLO = 2137;
    public static final int INTENSIDAD_BAJA_IZQUIERDA_MOTOTRICICLO = 2141;
    public static final int INTENSIDAD_ALTA_DERECHA_MOTOTRICICLO = 2139;
    public static final int INTENSIDAD_ALTA_IZQUIERDA_MOTOTRICICLO = 2143;
    public static final int INCLINACION_BAJA_DERECHA_MOTOTRICICLO = 2138;
    public static final int INCLINACION_BAJA_IZQUIERDA_MOTOTRICICLO = 2142;
    public static final int INTENSIDAD_EXPLORADORA_DERECHA_MOTOTRICICLO = 2140;
    public static final int INTENSIDAD_EXPLORADORA_IZQUIERDA_MOTOTRICICLO = 2144;
    public static final int SUMA_LUCES_MOTOTRICICLO = 2145;
    
    
    
    
    
    public static final int SUMA_EXPLORADORAS = 2008;
    public static final int SUMA_LUCES = 2011;
    
    public static final int INCLINACION_BAJA_DERECHA_MOTOS = 2013;
    public static final int INTENSIDAD_BAJA_DERECHA_MOTOS = 2014;
    public static final int INTENSIDAD_ALTA_DERECHA_MOTOS = 2000;
    public static final int INTENSIDAD_BAJA_IZQUIERDA_MOTOS = 2015;
    public static final int INCLINACION_BAJA_IZQUIERDA_MOTOS = 2002;
    public static final int INTENSIDAD_ALTA_IZQUIERDA_MOTOS = 2001;
    
    public static final int INTENSIDAD_BAJA_DERECHA_MOTOS_DOS = 2060;
    public static final int INCLINACION_BAJA_DERECHA_MOTOS_DOS = 2061;    
    public static final int INTENSIDAD_BAJA_IZQUIERDA_MOTOS_DOS =  2062;
    public static final int INCLINACION_BAJA_IZQUIERDA_MOTOS_DOS = 2063;
    
    public static final int INTENSIDAD_BAJA_DERECHA_MOTOS_TRES = 2064;
    public static final int INCLINACION_BAJA_DERECHA_MOTOS_TRES = 2065;
    
    public static final int INTENSIDAD_BAJA_IZQUIERDA_MOTOS_TRES = 2066;
    public static final int INCLINACION_BAJA_IZQUIERDA_MOTOS_TRES = 2067;
    
    public static final int INTENSIDAD_ALTA_DER_MOTO_DOS = 2068;
    public static final int INTENSIDAD_ALTA_IZQ_MOTO_DOS = 2069;
    public static final int INTENSIDAD_ALTA_DER_MOTO_TRES = 2070;
    public static final int INTENSIDAD_ALTA_IZQ_MOTO_TRES = 2071;
    public static final int INTENSIDAD_EXP_DER_MOTO_DOS = 2072;
    public static final int INTENSIDAD_EXP_IZQ_MOTO_DOS = 2073;
    public static final int INTENSIDAD_EXP_DER_MOTO_TRES = 2074;
    public static final int INTENSIDAD_EXP_IZQ_MOTO_TRES = 2075;
    public static final int INTENSIDAD_EXP_DER_MOTO_UNO = 2076;
    public static final int INTENSIDAD_EXP_IZQ_MOTO_UNO = 2077;
    
    
    
    public static final int INTENSIDAD_BAJA_DERECHA_CICLOMOTOR = 2100;
    public static final int INTENSIDAD_BAJA_IZQUIERDA_CICLOMOTOR = 2104;
    public static final int INTENSIDAD_ALTA_DERECHA_CICLOMOTOR = 2102;
    public static final int INTENSIDAD_ALTA_IZQUIERDA_CICLOMOTOR = 2106;
    public static final int INTENSIDAD_EXPLORADORA_DERECHA_CICLOMOTOR = 2103;
    public static final int INTENSIDAD_EXPLORADORA_IZQUIERDA_CICLOMOTOR = 2107;
    public static final int SUMA_LUCES_CICLOMOTOR = 2108;
    
    public static final int INCLINACION_BAJA_DERECHA_CICLOMOTOR = 2101;
    public static final int INCLINACION_BAJA_IZQUIERDA_CICLOMOTOR = 2105;
    
    public static final int INTENSIDAD_BAJA_DERECHA_TRICIMOTO = 2110;
    public static final int INTENSIDAD_BAJA_IZQUIERDA_TRICIMOTO = 2114;
    public static final int INTENSIDAD_ALTA_DERECHA_TRICIMOTO = 2112;
    public static final int INTENSIDAD_ALTA_IZQUIERDA_TRICIMOTO = 2116;
    public static final int INTENSIDAD_EXPLORADORA_DERECHA_TRICIMOTO = 2113;
    public static final int INTENSIDAD_EXPLORADORA_IZQUIERDA_TRICIMOTO = 2117;
    public static final int SUMA_LUCES_TRICIMOTO = 2118;
    
    public static final int INCLINACION_BAJA_DERECHA_TRICIMOTO = 2111;
    public static final int INCLINACION_BAJA_IZQUIERDA_TRICIMOTO = 2115;
    
    public static final int INTENSIDAD_BAJA_DERECHA_CUATRIMOTO_DOS = 2150;
    public static final int INCLINACION_BAJA_DERECHA_CUATRIMOTO_DOS = 2151;    
    public static final int INTENSIDAD_BAJA_IZQUIERDA_CUATRIMOTO_DOS =  2152;
    public static final int INCLINACION_BAJA_IZQUIERDA_CUATRIMOTO_DOS = 2153;
    
    public static final int INTENSIDAD_BAJA_DERECHA_CUATRIMOTO_TRES = 2154;
    public static final int INCLINACION_BAJA_DERECHA_CUATRIMOTO_TRES = 2155;
    
    public static final int INTENSIDAD_BAJA_IZQUIERDA_CUATRIMOTO_TRES = 2156;
    public static final int INCLINACION_BAJA_IZQUIERDA_CUATRIMOTO_TRES = 2157;
    
    public static final int INTENSIDAD_ALTA_DER_CUATRIMOTO_DOS = 2158;
    public static final int INTENSIDAD_ALTA_IZQ_CUATRIMOTO_DOS = 2159;
    public static final int INTENSIDAD_ALTA_DER_CUATRIMOTO_TRES = 2160;
    public static final int INTENSIDAD_ALTA_IZQ_CUATRIMOTO_TRES = 2161;
    public static final int INTENSIDAD_EXP_DER_CUATRIMOTO_DOS = 2162;
    public static final int INTENSIDAD_EXP_IZQ_CUATRIMOTO_DOS = 2163;
    public static final int INTENSIDAD_EXP_DER_CUATRIMOTO_TRES = 2164;
    public static final int INTENSIDAD_EXP_IZQ_CUATRIMOTO_TRES = 2165;

    
    
    public static final int INTENSIDAD_BAJA_DERECHA_CUADRICICLO_DOS = 2170;
    public static final int INCLINACION_BAJA_DERECHA_CUADRICICLO_DOS = 2171;    
    public static final int INTENSIDAD_BAJA_IZQUIERDA_CUADRICICLO_DOS =  2172;
    public static final int INCLINACION_BAJA_IZQUIERDA_CUADRICICLO_DOS = 2173;
    
    public static final int INTENSIDAD_BAJA_DERECHA_CUADRICICLO_TRES = 2174;
    public static final int INCLINACION_BAJA_DERECHA_CUADRICICLO_TRES = 2175;
    
    public static final int INTENSIDAD_BAJA_IZQUIERDA_CUADRICICLO_TRES = 2176;
    public static final int INCLINACION_BAJA_IZQUIERDA_CUADRICICLO_TRES = 2177;
    
    public static final int INTENSIDAD_ALTA_DER_CUADRICICLO_DOS = 2178;
    public static final int INTENSIDAD_ALTA_IZQ_CUADRICICLO_DOS = 2179;
    public static final int INTENSIDAD_ALTA_DER_CUADRICICLO_TRES = 2180;
    public static final int INTENSIDAD_ALTA_IZQ_CUADRICICLO_TRES = 2181;
    public static final int INTENSIDAD_EXP_DER_CUADRICICLO_DOS = 2182;
    public static final int INTENSIDAD_EXP_IZQ_CUADRICICLO_DOS = 2183;
    public static final int INTENSIDAD_EXP_DER_CUADRICICLO_TRES = 2184;
    public static final int INTENSIDAD_EXP_IZQ_CUADRICICLO_TRES = 2185;

    
    public static final int INTENSIDAD_BAJA_DERECHA_MOTOTRICICLO_DOS = 2190;
    public static final int INCLINACION_BAJA_DERECHA_MOTOTRICICLO_DOS = 2191;    
    public static final int INTENSIDAD_BAJA_IZQUIERDA_MOTOTRICICLO_DOS =  2192;
    public static final int INCLINACION_BAJA_IZQUIERDA_MOTOTRICICLO_DOS = 2193;
    
    public static final int INTENSIDAD_BAJA_DERECHA_MOTOTRICICLO_TRES = 2194;
    public static final int INCLINACION_BAJA_DERECHA_MOTOTRICICLO_TRES = 2195;
    
    public static final int INTENSIDAD_BAJA_IZQUIERDA_MOTOTRICICLO_TRES = 2196;
    public static final int INCLINACION_BAJA_IZQUIERDA_MOTOTRICICLO_TRES = 2197;
    
    public static final int INTENSIDAD_ALTA_DER_MOTOTRICICLO_DOS = 2198;
    public static final int INTENSIDAD_ALTA_IZQ_MOTOTRICICLO_DOS = 2199;
    public static final int INTENSIDAD_ALTA_DER_MOTOTRICICLO_TRES = 2200;
    public static final int INTENSIDAD_ALTA_IZQ_MOTOTRICICLO_TRES = 2201;
    public static final int INTENSIDAD_EXP_DER_MOTOTRICICLO_DOS = 2202;
    public static final int INTENSIDAD_EXP_IZQ_MOTOTRICICLO_DOS = 2203;
    public static final int INTENSIDAD_EXP_DER_MOTOTRICICLO_TRES = 2204;
    public static final int INTENSIDAD_EXP_IZQ_MOTOTRICICLO_TRES = 2205;
    public static final int INTENSIDAD_EXP_DER_MOTOTRICICLO_UNO = 2206;
    public static final int INTENSIDAD_EXP_IZQ_MOTOTRICICLO_UNO = 2207;
    
    public static final int INTENSIDAD_BAJA_DERECHA_CICLOMOTOR_DOS = 2210;
    public static final int INCLINACION_BAJA_DERECHA_CICLOMOTOR_DOS = 2211;    
    public static final int INTENSIDAD_BAJA_IZQUIERDA_CICLOMOTOR_DOS =  2212;
    public static final int INCLINACION_BAJA_IZQUIERDA_CICLOMOTOR_DOS = 2213;
    
    public static final int INTENSIDAD_BAJA_DERECHA_CICLOMOTOR_TRES = 2214;
    public static final int INCLINACION_BAJA_DERECHA_CICLOMOTOR_TRES = 2215;
    
    public static final int INTENSIDAD_BAJA_IZQUIERDA_CICLOMOTOR_TRES = 2216;
    public static final int INCLINACION_BAJA_IZQUIERDA_CICLOMOTOR_TRES = 2217;
    
    public static final int INTENSIDAD_ALTA_DER_CICLOMOTOR_DOS = 2218;
    public static final int INTENSIDAD_ALTA_IZQ_CICLOMOTOR_DOS = 2219;
    public static final int INTENSIDAD_ALTA_DER_CICLOMOTOR_TRES = 2220;
    public static final int INTENSIDAD_ALTA_IZQ_CICLOMOTOR_TRES = 2221;
    public static final int INTENSIDAD_EXP_DER_CICLOMOTOR_DOS = 2222;
    public static final int INTENSIDAD_EXP_IZQ_CICLOMOTOR_DOS = 2223;
    public static final int INTENSIDAD_EXP_DER_CICLOMOTOR_TRES = 2224;
    public static final int INTENSIDAD_EXP_IZQ_CICLOMOTOR_TRES = 2225;
    
    public static final int INTENSIDAD_BAJA_DERECHA_TRICIMOTO_DOS = 2230;
    public static final int INCLINACION_BAJA_DERECHA_TRICIMOTO_DOS = 2231;    
    public static final int INTENSIDAD_BAJA_IZQUIERDA_TRICIMOTO_DOS =  2232;
    public static final int INCLINACION_BAJA_IZQUIERDA_TRICIMOTO_DOS = 2233;
    
    public static final int INTENSIDAD_BAJA_DERECHA_TRICIMOTO_TRES = 2234;
    public static final int INCLINACION_BAJA_DERECHA_TRICIMOTO_TRES = 2235;
    
    public static final int INTENSIDAD_BAJA_IZQUIERDA_TRICIMOTO_TRES = 2236;
    public static final int INCLINACION_BAJA_IZQUIERDA_TRICIMOTO_TRES = 2237;
    
    public static final int INTENSIDAD_ALTA_DER_TRICIMOTO_DOS = 2238;
    public static final int INTENSIDAD_ALTA_IZQ_TRICIMOTO_DOS = 2239;
    public static final int INTENSIDAD_ALTA_DER_TRICIMOTO_TRES = 2240;
    public static final int INTENSIDAD_ALTA_IZQ_TRICIMOTO_TRES = 2241;
    public static final int INTENSIDAD_EXP_DER_TRICIMOTO_DOS = 2242;
    public static final int INTENSIDAD_EXP_IZQ_TRICIMOTO_DOS = 2243;
    public static final int INTENSIDAD_EXP_DER_TRICIMOTO_TRES = 2244;
    public static final int INTENSIDAD_EXP_IZQ_TRICIMOTO_TRES = 2245;
    
    

    
    
    
        
}
