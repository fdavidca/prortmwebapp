package com.proambiente.webapp.util;

public class ConstantesTiposCombustible {
	
	public static final Integer DIESEL = 3;
	public static final Integer BIODIESEL = 8;
	public static final Integer DIESEL_ELECTRICO = 11;
	public static final Integer GASOLINA = 1;
	public static final Integer GAS_NATURAL = 2;
	public static final Integer GAS_GASOLINA = 4;
	public static final Integer GASOLINA_ELECTRICO = 10;
	public static final Integer ELECTRICO = 5;
	public static final Integer HIDROGENO = 6;
	
	
}
