package com.proambiente.webapp.util;

import com.proambiente.modelo.Vehiculo;

public class UtilCopiarInformacionVehiculo {

	
	public static Vehiculo copiarVehiculo(Vehiculo v) {
		
		Vehiculo vn = new Vehiculo();
		vn.setPlaca(v.getPlaca());
		vn.setModelo(v.getModelo());
		vn.setAseguradora(v.getAseguradora());
		vn.setBlindaje(v.getBlindaje());
		vn.setCilindraje(v.getCilindraje());
		vn.setClaseVehiculo(v.getClaseVehiculo());
		vn.setColor(v.getColor());
		vn.setConductor(v.getConductor());
		vn.setDiametroExosto(v.getDiametroExosto());
		vn.setDiseno(v.getDiseno());
		vn.setNumeroPasajeros(v.getNumeroPasajeros());
		vn.setFechaExpiracionSoat(v.getFechaExpiracionSoat());
		vn.setFechaMatricula(v.getFechaMatricula());
		vn.setFechaRegistro(v.getFechaRegistro());
		vn.setFechaSoat(v.getFechaSoat());
		vn.setKilometraje(v.getKilometraje());
		vn.setLineaVehiculo(v.getLineaVehiculo());
		vn.setLlanta(v.getLlanta());
		vn.setNumeroSillas(v.getNumeroSillas());
		vn.setMarca(v.getMarca());		
		vn.setNacionalidad(v.getNacionalidad());
		vn.setNumeroEjes(v.getNumeroEjes());
		vn.setNumeroExostos(v.getNumeroExostos());
		vn.setNumeroLicencia(v.getNumeroLicencia());
		vn.setNumeroMotor(v.getNumeroMotor());
		vn.setNumeroSerie(v.getNumeroSerie());
		vn.setNumeroSillas(v.getNumeroSillas());
		vn.setNumeroSoat(v.getNumeroSoat());
		vn.setPais(v.getPais());
		vn.setPotencia(v.getPotencia());
		vn.setPropietario(v.getPropietario());
		vn.setServicio(v.getServicio());
		vn.setServicioEspecial(v.getServicioEspecial());
		vn.setTiemposMotor(v.getTiemposMotor());
		vn.setTipoCombustible(v.getTipoCombustible());		
		vn.setTipoVehiculo(v.getTipoVehiculo());
		vn.setUsuario(v.getUsuario());
		vn.setVehiculoId(null);
		vn.setVelocidad(v.getVelocidad());
		vn.setVidriosPolarizados(v.getVidriosPolarizados());
		vn.setVin(v.getVin());	
		vn.setTipoCarroceria(v.getTipoCarroceria());
		String conversionGNV = v.getConversionGnv() == null ? "":v.getConversionGnv();
		vn.setConversionGnv(conversionGNV);
		vn.setFechaVenicimientoGNV(v.getFechaVenicimientoGNV());
		String catalizador = v.getCatalizador() == null ? "" : v.getCatalizador();
		vn.setCatalizador(catalizador);
		
		
		return vn;
	}
}
