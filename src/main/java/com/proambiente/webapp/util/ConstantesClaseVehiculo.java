package com.proambiente.webapp.util;

public class ConstantesClaseVehiculo {
	
	public static final Integer MOTOCICLETA = 10;
	public static final Integer MOTOCARRO = 14;
	public static final Integer AUTOMOVIL = 1;
	
}
