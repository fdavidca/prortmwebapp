package com.proambiente.webapp.util;

import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;
import com.proambiente.modelo.dto.MedicionGasesDTO;

public class MedicionGasesDTODecodificador {
	
	public static List<MedicionGasesDTO> decodificarDesdeCadena(String jsonSerializado) throws JsonParseException, JsonMappingException, IOException{
		ObjectMapper mapper = new ObjectMapper();
        mapper.disable(WRITE_DATES_AS_TIMESTAMPS);
        mapper.setDateFormat(new ISO8601DateFormat());
        MedicionGasesDTO[] muestras = mapper.readValue(jsonSerializado, MedicionGasesDTO[].class);
        return Arrays.asList(muestras);
	}
	
	public static List<List<MedicionGasesDTO>> decodificarDesdeCadenaVariasListas(String jsonSerializado) throws JsonParseException, JsonMappingException, IOException{
		ObjectMapper mapper = new ObjectMapper();
        mapper.disable(WRITE_DATES_AS_TIMESTAMPS);
        mapper.setDateFormat(new ISO8601DateFormat());
        MedicionGasesDTO[][] muestras = mapper.readValue(jsonSerializado, MedicionGasesDTO[][].class);
        List<List<MedicionGasesDTO>> listaDeLista = new ArrayList<>();
        for(MedicionGasesDTO[] arreglo: muestras) {
        	listaDeLista.add(Arrays.asList(arreglo));
        }
        return listaDeLista;
	}

}
