package com.proambiente.webapp.util;

import java.util.Comparator;

import com.proambiente.modelo.Revision;

public class RevisionComparator implements Comparator<Revision>{

	@Override
	public int compare(Revision o1, Revision o2) {
		if(o1.getRevisionId() != null && o2.getRevisionId() != null){
			return o1.getRevisionId().compareTo(o2.getRevisionId())*-1;
		}
		else{
			return 0;
		}	
	}

}
