package com.proambiente.webapp.util;

public class ConstantesTiposVehiculo {

	public static final Integer LIVIANO = 1;
	public static final Integer CUATROxCUATRO = 2;
	public static final Integer PESADO = 3;
	public static final Integer MOTO = 4;
	public static final Integer MOTO_CARRO = 5;
	public static final Integer REMOLQUE = 6;
	public static final Integer CICLOMOTOR = 7;
	public static final Integer TRICIMOTO = 8;
	public static final Integer CUATRIMOTO = 9;
	public static final Integer MOTOTRICICLO  = 10;
	public static final Integer CUADRICICLO = 11;
}
