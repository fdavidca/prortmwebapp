package com.proambiente.webapp.util;

public class ConstantesTiposEquipo {
	public static final Integer PROFUNDIMETRO = 1;
	public static final Integer TERMOHIGROMETRO = 2;
	public static final Integer SONDA_TEMPERATURA = 3;
	public static final Integer MEDIDOR_REVOLUCIONES = 4;
	public static final Integer LUXOMETRO = 5;
	public static final Integer SONOMETRO = 6;
	public static final Integer TAXIMETRO = 7;
	public static final Integer ALINEADOR = 8;
	public static final Integer ANALIZADOR_SUSPENSION = 9;
	public static final Integer FRENOMETRO = 10;
	public static final Integer ELEVADOR_MOTOCICLETAS = 11;
	public static final Integer DETECTOR_HOLGURAS = 12;
	public static final Integer SENSOR_DE_REVOLUCIONES_MEDIANTE_VIBRACION = 13;
	public static final Integer PIE_DE_REY = 14;
	public static final Integer MANOMETRO = 15;
	
}
