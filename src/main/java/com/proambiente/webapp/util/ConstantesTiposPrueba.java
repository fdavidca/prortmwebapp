package com.proambiente.webapp.util;

public class ConstantesTiposPrueba {

	public static final Integer TIPO_PRUEBA_FOTO = 3;
	public static final int TIPO_PRUEBA_GASES = 8;
	public static final int TIPO_PRUEBA_LUCES = 2;
	public static final int TIPO_PRUEBA_RUIDO = 7;
	public static final int TIPO_PRUEBA_FRENOS = 5;
	public static final int TIPO_PRUEBA_SUSPENSION = 6;
	public static final int TIPO_PRUEBA_DESVIACION = 4;
	public static final int TIPO_PRUEBA_INSPECCION_SENSORIAL = 1;
	public static final int TIPO_PRUEBA_TAXIMETRO = 9;
	
	
}
