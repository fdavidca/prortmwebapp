package com.proambiente.webapp.util;

import java.util.HashMap;
import java.util.Map;
import static com.proambiente.webapp.util.ConstantesCodigosDefectoGases.*;

public enum CausalesRechazoPruebasEmisionesContaminantes {
	
	
	AUSENCIA_TAPA_ACEITE_OTTO( CODIGO_AUSENCIA_TAPA_ACEITE_OTTO),
	AUSENCIA_TAPA_COMBUSTIBLE_OTTO( CODIGO_AUSENCIA_TAPA_COMBUSTIBLE_OTTO ),
	FUGAS_TUBO_SILENCIADOR_OTTO ( CODIGO_FUGAS_TUBO_SILENCIADOR ),
	SALIDAS_ADICIONALES_OTTO( CODIGO_SALIDAS_ADICIONALES_OTTO),
	INCORRECTA_OP_REFRIGERACION_OTTO(CODIGO_INCORRECTA_OP_REFRIGERACION_OTTO),
	FALLA_SISTEMA_AD_AIRE_OTTO (CODIGO_FALLA_SISTEMA_AD_AIRE_OTTO),
	ACCESORIOS_IMPIDEN_OTTO ( CODIGO_ACCESORIOS_IMPIDEN_OTTO ),
	
	FUGAS_TUBO_ESCAPE_MOTO( CODIGO_FUGAS_TUBO_ESCAPE_MOTO),
	SALIDAS_ADICIONALES_MOTO (CODIGO_SALIDAS_ADICIONALES_MOTO),
	AUSENCIA_TAPA_ACEITE_MOTO (CODIGO_AUSENCIA_TAPA_ACEITE_MOTO),
	
	AUSENCIA_TAPA_ACEITE_DIESEL( CODIGO_AUSENCIA_TAPA_ACEITE_DIESEL ),
	AUSENCIA_TAPA_COMBUSTIBLE_DIESEL( CODIGO_AUSENCIA_TAPA_COMBUSTIBLE_DIESEL  ), 
	FUGAS_SILENCIADOR_DIESEL( CODIGO_FUGAS_SILENCIADOR_DIESEL ),
	FUGAS_TUBO_ESCAPE_DIESEL( CODIGO_FUGAS_TUBO_ESCAPE_DIESEL ),
	SALIDAS_ADICIONALES_DISENIO_DIESEL( CODIGO_SALIDAS_ADICIONALES_DIESEL ),
	HUMO_NEGRO_O_AZUL( CODIGO_HUMO_NEGRO_AZUL ),
	REVOLUCIONES_FUERA_RANGO( CODIGO_REVOLUCIONES_FUERA_RANGO ),
	
	ACCESORIOS_DEFORMACIONES_IMPIDEN( CODIGO_ACCESORIOS_DEFORMACIONES_IMPIDEN ),
	AUSENCIA_O_INCORRECTA_OPERACION_FILTRO_AIRE_DIESEL( CODIGO_AUSENCIA_FILTRO_AIRE_DIESEL ),
	DESCONEXION_PCV( CODIGO_DESCONEXION_PCV ),
	FALLA_SISTEMA_REFRIGERACION_DIESEL( CODIGO_FALLA_SISTEMA_REFRIGERACION_DIESEL ),
	OP_INADECUADA_DISPOSITIVOS_IMPIDEN_ACELERAR(CODIGO_OP_INADECUADA_DISPOSITIVOS_IMPIDEN_ACELERAR_DIESEL),
	INCUMPLIMIENTO_NIVELES_EMISION(CODIGO_INCUMPLIMIENTO_NIVELES_EMISION),
	FALLA_SUBITA_VEHICULO( CODIGO_FALLA_SUBITA_VEHICULO ), 
	SISTEMA_INYECCION_NO_LIMITA(CODIGO_SISTEMA_INYECCION_NO_LIMITA),
	DIFERENCIA_TEMPERATURA_MOTOR(CODIGO_DIFERENCIA_TEMPERATURA_MOTOR),
	DIFERENCIA_ARITMETICA_CICLOS(CODIGO_DIFERENCIA_ARITMETICA_CICLOS),
	MAL_FUNCIONAMIENTO_CONTROL_VELOCIDAD(CODIGO_MAL_FUNCIONAMIENTO_CONTROL_VELOCIDAD),
	INDICIO_DANIO_MOTOR(CODIGO_INDICIO_DANIO_MOTOR),
	INDICIO_CONDICION_INSEGURA(CODIGO_INDICIO_CONDICION_INSEGURA),
	NO_ALCANZA_GOBERNADA(CODIGO_NO_ALCANZA_GOBERNADA),
	VELOCIDAD_FUERA_INTERVALO( CODIGO_REVOLUCIONES_FUERA_RANGO),
	NO_SATISFACIERON_CRITERIOS(CODIGO_NO_SATISFACIERON_CRITERIOS),
	INCUMPLIMIENTO_DECRETO_1552(CODIGO_INCUMPLIMIENTO_DECRETO_1552),
	FALLA_SISTEMA_SILENCIADOR(CODIGO_FALLA_SISTEMA_SILENCIADOR),
	PRESENCIA_RESONADORES(CODIGO_PRESENCIA_GENERADORES_RUIDO),
	NO_CONTEMPLADA(-999);
	;
	
	
	
	
	
	
	private static final Map<Integer,CausalesRechazoPruebasEmisionesContaminantes> mapaEnteroConstanteDefectosGases = new HashMap<Integer, CausalesRechazoPruebasEmisionesContaminantes>();
	private static final Map<String,CausalesRechazoPruebasEmisionesContaminantes> mapaClaveNombreCausaRechazo = new HashMap<String,CausalesRechazoPruebasEmisionesContaminantes>();
	static{
		for(CausalesRechazoPruebasEmisionesContaminantes constante :CausalesRechazoPruebasEmisionesContaminantes.values()){
			mapaEnteroConstanteDefectosGases.put(constante.getCodigoDefecto(), constante);
		}
		for(CausalesRechazoPruebasEmisionesContaminantes constante :CausalesRechazoPruebasEmisionesContaminantes.values()){
			mapaClaveNombreCausaRechazo.put(constante.name(), constante);
		}
	}
	
	
	
	public static CausalesRechazoPruebasEmisionesContaminantes constantePorCodigoDefecto(Integer codigoDefecto){
		CausalesRechazoPruebasEmisionesContaminantes causa = mapaEnteroConstanteDefectosGases.get(codigoDefecto);
		if(causa == null)
			causa = NO_CONTEMPLADA;
		return causa;
	}
	
	public static CausalesRechazoPruebasEmisionesContaminantes constantePorClaveCausa(String clave){
		CausalesRechazoPruebasEmisionesContaminantes causa = mapaClaveNombreCausaRechazo.get(clave);
		if(causa == null)
			causa = NO_CONTEMPLADA;
		return causa;
	}
	
	private static final CausalesRechazoPruebasEmisionesContaminantes[] causasRechazoMotocicletas = new CausalesRechazoPruebasEmisionesContaminantes[]{AUSENCIA_TAPA_ACEITE_MOTO,FUGAS_TUBO_ESCAPE_MOTO,SALIDAS_ADICIONALES_MOTO};
	
	private static final CausalesRechazoPruebasEmisionesContaminantes[] causalesRechazoVehiculosInspeccionPrevia = new CausalesRechazoPruebasEmisionesContaminantes[]{
		AUSENCIA_TAPA_ACEITE_OTTO,AUSENCIA_TAPA_COMBUSTIBLE_OTTO,FUGAS_TUBO_SILENCIADOR_OTTO,SALIDAS_ADICIONALES_OTTO,
		ACCESORIOS_IMPIDEN_OTTO,FALLA_SISTEMA_AD_AIRE_OTTO,DESCONEXION_PCV,INCORRECTA_OP_REFRIGERACION_OTTO
	};
	
	private static final CausalesRechazoPruebasEmisionesContaminantes[] causalesRechazoInspeccionPreviaDiesel = new CausalesRechazoPruebasEmisionesContaminantes[]{
		AUSENCIA_TAPA_ACEITE_DIESEL,AUSENCIA_TAPA_COMBUSTIBLE_DIESEL,FUGAS_TUBO_ESCAPE_DIESEL,SALIDAS_ADICIONALES_DISENIO_DIESEL,
		ACCESORIOS_DEFORMACIONES_IMPIDEN,AUSENCIA_O_INCORRECTA_OPERACION_FILTRO_AIRE_DIESEL,FALLA_SISTEMA_REFRIGERACION_DIESEL,
		OP_INADECUADA_DISPOSITIVOS_IMPIDEN_ACELERAR,INCUMPLIMIENTO_DECRETO_1552,FALLA_SISTEMA_SILENCIADOR,PRESENCIA_RESONADORES
	};
	
	public static Integer[] obtenerCausalesRechazoMotocicletas(){
		
		Integer[] codigosDefecto = new Integer[causasRechazoMotocicletas.length];
		for(int i = 0;i < causasRechazoMotocicletas.length;i++){
			codigosDefecto[i] = causasRechazoMotocicletas[i].getCodigoDefecto();
		}
		return codigosDefecto;
	}
	
	public static Integer[] obtenerCausalesRechazoVehiculosInspeccionPrevia(){
		Integer[] codigosDefecto = new Integer[causalesRechazoVehiculosInspeccionPrevia.length];
		for(int i=0; i < codigosDefecto.length;i++){
			codigosDefecto[i] = causalesRechazoVehiculosInspeccionPrevia[i].getCodigoDefecto();
		}
		return codigosDefecto;
	}
	
	public static Integer[] obtenerCausalesRechazoInspeccionPreviaDiesel(){
		Integer[] codigosDefecto = new Integer[causalesRechazoInspeccionPreviaDiesel.length];
		for(int i=0; i < codigosDefecto.length;i++){
			codigosDefecto[i] = causalesRechazoInspeccionPreviaDiesel[i].getCodigoDefecto();
		}
		return codigosDefecto;
	}
	
	//VEHICULOS CICLO OTTO
	
//	public static int AUSENCIA_TAPA_ACEITE;
//	public static int AUSENCIA_TAPA_COMBUSTIBLE;
//	public static int FUGAS_SILENCIADOR;
//	public static int FUGAS_TUBO_ESCAPE;
//	public static int SALIDAS_ADICIONALES_DISENIO;
//	
//	public static int HUMO_NEGRO_O_AZUL;
//	public static int REVOLUCIONES_FUERA_RANGO;
	
	
	//VEHICULOS DIESEL
	
	public static int OPERACION_INADECUADA_DIFERENCIA_TEMPERATURA;
	public static int INCORRECTA_OPERACION_AUSENCIA_SISTEMA_CONTROL_GIRO;
	public static int SISTEMA_INYECCION_COMBUSTIBLE_NO_LIMITA;
	public static int NO_SE_ALCANZA_VELOCIDAD_5S;
	public static int FALLA_SUBITA_MOTOR;
	public static int MAL_FUNCIONAMIENTO_VEHICULO_DIFERENCIA_OPA_CICLOS;
	
	
	private int codigoDefecto;


	private CausalesRechazoPruebasEmisionesContaminantes(int codigoDefecto) {
		this.codigoDefecto = codigoDefecto;
	}


	public int getCodigoDefecto() {
		return codigoDefecto;
	}


	public void setCodigoDefecto(int codigoDefecto) {
		this.codigoDefecto = codigoDefecto;
	}
	
	
	

}
