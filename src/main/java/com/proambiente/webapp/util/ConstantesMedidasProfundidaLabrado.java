package com.proambiente.webapp.util;

public class ConstantesMedidasProfundidaLabrado {
	
	
	public static final int PROFUNDIDAD_LABRADO_EJE1_DER = 5060; 
	public static final int PROFUNDIDAD_LABRADO_EJE1_IZQ = 5061;
	
	public static final int PROFUNDIDAD_LABRADO_EJE2_DER_1 = 5062;
	public static final int PROFUNDIDAD_LABRADO_EJE2_DER_2 = 5072;
	public static final int PROFUNDIDAD_LABRADO_EJE2_DER_3 = 5082;
	public static final int PROFUNDIDAD_LABRADO_EJE2_DER_4 = 5092;
	
	public static final int PROFUNDIDAD_LABRADO_EJE2_IZQ_1 = 5063;
	public static final int PROFUNDIDAD_LABRADO_EJE2_IZQ_2 = 5073;
	public static final int PROFUNDIDAD_LABRADO_EJE2_IZQ_3 = 5083;
	public static final int PROFUNDIDAD_LABRADO_EJE2_IZQ_4 = 5093;

	public static final int PROFUNDIDAD_LABRADO_EJE3_DER_1 = 5066;
	public static final int PROFUNDIDAD_LABRADO_EJE3_DER_2 = 5076;
	public static final int PROFUNDIDAD_LABRADO_EJE3_DER_3 = 5086;
	public static final int PROFUNDIDAD_LABRADO_EJE3_DER_4 = 5096;
	
	
	public static final int PROFUNDIDAD_LABRADO_EJE3_IZQ_1 = 5067;
	public static final int PROFUNDIDAD_LABRADO_EJE3_IZQ_2 = 5077;
	public static final int PROFUNDIDAD_LABRADO_EJE3_IZQ_3 = 5087;
	public static final int PROFUNDIDAD_LABRADO_EJE3_IZQ_4 = 5097;
	
	
	public static final int PROFUNDIDAD_LABRADO_EJE4_DER_1 = 5068;
	public static final int PROFUNDIDAD_LABRADO_EJE4_DER_2 = 5078;
	public static final int PROFUNDIDAD_LABRADO_EJE4_DER_3 = 5088;
	public static final int PROFUNDIDAD_LABRADO_EJE4_DER_4 = 5098;
	
	public static final int PROFUNDIDAD_LABRADO_EJE4_IZQ_1 = 5069;
	public static final int PROFUNDIDAD_LABRADO_EJE4_IZQ_2 = 5079;
	public static final int PROFUNDIDAD_LABRADO_EJE4_IZQ_3 = 5089;
	public static final int PROFUNDIDAD_LABRADO_EJE4_IZQ_4 = 5099;
	
	public static final int PROFUNDIDAD_LABRADO_EJE5_DER_1 = 5070;
	public static final int PROFUNDIDAD_LABRADO_EJE5_DER_2 = 5080;
	public static final int PROFUNDIDAD_LABRADO_EJE5_DER_3 = 5090;
	public static final int PROFUNDIDAD_LABRADO_EJE5_DER_4 = 5100;
	
	public static final int PROFUNDIDAD_LABRADO_EJE5_IZQ_1 = 5071;
	public static final int PROFUNDIDAD_LABRADO_EJE5_IZQ_2 = 5081;
	public static final int PROFUNDIDAD_LABRADO_EJE5_IZQ_3 = 5091;
	public static final int PROFUNDIDAD_LABRADO_EJE5_IZQ_4 = 5101;
	
	public static final int PROFUNDIDAD_LABRADO_REPUESTO_DER = 5064;
	public static final int PROFUNDIDAD_LABRADO_REPUESTO_IZQ = 5074;

}
