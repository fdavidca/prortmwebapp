package com.proambiente.webapp.security;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordValidator implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Valida que el password cumpla las siguientes restricciones:
	 * Longitud minima 6 caracteres, contener numeros y letras,
	 * contener al menos una mayuscula
	 * 
	 * @param password
	 * @return String conteniendo los errores de validacion separados por cambio de linea,
	 * en caso de un password valido, retornala un String vacio
	 */
	
	public String validarPassword(String password){
		
		StringBuilder sb = new StringBuilder();
		
		if(password.length() < 6){
			sb.append("El password debe tener minimo 6 caracteres").append("\n");
			return sb.toString();
		}
		
		//Validar que tenga letras
		Pattern patternLetras = Pattern.compile("[a-zA-z]");
		Matcher matcherLetras = patternLetras.matcher(password);
		if(!matcherLetras.find()){
			sb.append("El password no contiene ninguna letra \n");
		}
		
		
		//Validar que tenga numeros
		Pattern patternNumeros = Pattern.compile("\\d");
		Matcher matcherNumeros = patternNumeros.matcher(password);
		if(!matcherNumeros.find()){
			sb.append("El password no contiene ningun numero \n");
		}
		
		//Validar que al menos tenga una mayuscula
		Pattern patternMayuscula = Pattern.compile("[A-Z]");
		Matcher matcherMayuscula = patternMayuscula.matcher(password);
		if(!matcherMayuscula.find()){
			sb.append("El password no contiene ninguna mayuscula \n");
		}
		
		//validar que no tenga caracteres ordenados
		
		//Encontrar las letras
		Pattern patternSepararLetras = Pattern.compile("[a-zA-z]+");
		Matcher matcherSepararLetras = patternSepararLetras.matcher(password);
		while(matcherSepararLetras.find()){
			String letras = matcherSepararLetras.group();
			char[] caracteres = letras.toCharArray();
                        if(caracteres.length > 1){
                            if(caracteres[1] - caracteres[0] == 1){
                                sb.append("Caracteres consecutivos").append(" ").append(caracteres[0]).append(" ").append(caracteres[1]).append("\n");                                
                            }
                        }
		}
		
                
                Pattern patternSepararNumeros = Pattern.compile("\\d{2,}");
		Matcher matcherSepararNumeros = patternSepararNumeros.matcher(password);
		while(matcherSepararNumeros.find()){
			String numeros = matcherSepararNumeros.group();
			char[] caracteres = numeros.toCharArray();
                        if(caracteres.length > 1){
                            if(caracteres[1] - caracteres[0] == 1){
                                sb.append("Caracteres consecutivos").append(" ").append(caracteres[0]).append(" ").append(caracteres[1]).append("\n");                                
                            }
                        }
		}		
		return sb.toString();
	}
}
