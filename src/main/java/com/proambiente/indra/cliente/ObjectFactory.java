
package com.proambiente.indra.cliente;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.proambiente.indra.cliente package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.proambiente.indra.cliente
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetInfoPin }
     * 
     */
    public GetInfoPin createGetInfoPin() {
        return new GetInfoPin();
    }

    /**
     * Create an instance of {@link SetConexion }
     * 
     */
    public SetConexion createSetConexion() {
        return new SetConexion();
    }

    /**
     * Create an instance of {@link SetConexionResponse }
     * 
     */
    public SetConexionResponse createSetConexionResponse() {
        return new SetConexionResponse();
    }

    /**
     * Create an instance of {@link GetInfoVehiculo }
     * 
     */
    public GetInfoVehiculo createGetInfoVehiculo() {
        return new GetInfoVehiculo();
    }

    /**
     * Create an instance of {@link EnviarEventosSicov }
     * 
     */
    public EnviarEventosSicov createEnviarEventosSicov() {
        return new EnviarEventosSicov();
    }

    /**
     * Create an instance of {@link EnviarFurSicov }
     * 
     */
    public EnviarFurSicov createEnviarFurSicov() {
        return new EnviarFurSicov();
    }

    /**
     * Create an instance of {@link EnviarFurSicovResponse }
     * 
     */
    public EnviarFurSicovResponse createEnviarFurSicovResponse() {
        return new EnviarFurSicovResponse();
    }

    /**
     * Create an instance of {@link FURRespuesta }
     * 
     */
    public FURRespuesta createFURRespuesta() {
        return new FURRespuesta();
    }

    /**
     * Create an instance of {@link GetInfoPinResponse }
     * 
     */
    public GetInfoPinResponse createGetInfoPinResponse() {
        return new GetInfoPinResponse();
    }

    /**
     * Create an instance of {@link PinRespuesta }
     * 
     */
    public PinRespuesta createPinRespuesta() {
        return new PinRespuesta();
    }

    /**
     * Create an instance of {@link EnviarRuntSicov }
     * 
     */
    public EnviarRuntSicov createEnviarRuntSicov() {
        return new EnviarRuntSicov();
    }

    /**
     * Create an instance of {@link EnviarRuntSicovResponse }
     * 
     */
    public EnviarRuntSicovResponse createEnviarRuntSicovResponse() {
        return new EnviarRuntSicovResponse();
    }

    /**
     * Create an instance of {@link EnviarEventosSicovResponse }
     * 
     */
    public EnviarEventosSicovResponse createEnviarEventosSicovResponse() {
        return new EnviarEventosSicovResponse();
    }

    /**
     * Create an instance of {@link EventosRespuesta }
     * 
     */
    public EventosRespuesta createEventosRespuesta() {
        return new EventosRespuesta();
    }

    /**
     * Create an instance of {@link GetInfoVehiculoResponse }
     * 
     */
    public GetInfoVehiculoResponse createGetInfoVehiculoResponse() {
        return new GetInfoVehiculoResponse();
    }

    /**
     * Create an instance of {@link VehiculoRespuesta }
     * 
     */
    public VehiculoRespuesta createVehiculoRespuesta() {
        return new VehiculoRespuesta();
    }

    /**
     * Create an instance of {@link EntityBase }
     * 
     */
    public EntityBase createEntityBase() {
        return new EntityBase();
    }

    /**
     * Create an instance of {@link GetInfoVehiculoResult }
     * 
     */
    public GetInfoVehiculoResult createGetInfoVehiculoResult() {
        return new GetInfoVehiculoResult();
    }

    /**
     * Create an instance of {@link GetInfoPinResult }
     * 
     */
    public GetInfoPinResult createGetInfoPinResult() {
        return new GetInfoPinResult();
    }

}
