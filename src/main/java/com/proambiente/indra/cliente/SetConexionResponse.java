
package com.proambiente.indra.cliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="setConexionResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "setConexionResult"
})
@XmlRootElement(name = "setConexionResponse")
public class SetConexionResponse {

    protected String setConexionResult;

    /**
     * Obtiene el valor de la propiedad setConexionResult.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSetConexionResult() {
        return setConexionResult;
    }

    /**
     * Define el valor de la propiedad setConexionResult.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSetConexionResult(String value) {
        this.setConexionResult = value;
    }

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SetConexionResponse [setConexionResult=");
		builder.append(setConexionResult);
		builder.append("]");
		return builder.toString();
	}
    
    

}
