
package com.proambiente.indra.cliente;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "Sicov", targetNamespace = "http://190.25.205.154:809?/sicov.asmx", wsdlLocation = "http://172.26.124.36:8056/sicov.asmx?wsdl")
public class Sicov
    extends Service
{

    private final static URL SICOV_WSDL_LOCATION;
    private final static WebServiceException SICOV_EXCEPTION;
    private final static QName SICOV_QNAME = new QName("http://190.25.205.154:809?/sicov.asmx", "Sicov");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://172.26.124.36:8056/sicov.asmx?wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        SICOV_WSDL_LOCATION = url;
        SICOV_EXCEPTION = e;
    }

    public Sicov() {
        super(__getWsdlLocation(), SICOV_QNAME);
    }

    public Sicov(WebServiceFeature... features) {
        super(__getWsdlLocation(), SICOV_QNAME, features);
    }

    public Sicov(URL wsdlLocation) {
        super(wsdlLocation, SICOV_QNAME);
    }

    public Sicov(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, SICOV_QNAME, features);
    }

    public Sicov(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public Sicov(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns SicovSoap
     */
    @WebEndpoint(name = "SicovSoap")
    public SicovSoap getSicovSoap() {
        return super.getPort(new QName("http://190.25.205.154:809?/sicov.asmx", "SicovSoap"), SicovSoap.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns SicovSoap
     */
    @WebEndpoint(name = "SicovSoap")
    public SicovSoap getSicovSoap(WebServiceFeature... features) {
        return super.getPort(new QName("http://190.25.205.154:809?/sicov.asmx", "SicovSoap"), SicovSoap.class, features);
    }

    private static URL __getWsdlLocation() {
        if (SICOV_EXCEPTION!= null) {
            throw SICOV_EXCEPTION;
        }
        return SICOV_WSDL_LOCATION;
    }

}
