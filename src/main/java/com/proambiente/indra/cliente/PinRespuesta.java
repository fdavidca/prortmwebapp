
package com.proambiente.indra.cliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para PinRespuesta complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PinRespuesta">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codRespuesta" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="msjRespuesta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pinRespuesta" type="{http://190.25.205.154:809?/sicov.asmx}getInfoPinResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PinRespuesta", propOrder = {
    "codRespuesta",
    "msjRespuesta",
    "pinRespuesta"
})
public class PinRespuesta {

    protected int codRespuesta;
    protected String msjRespuesta;
    protected GetInfoPinResult pinRespuesta;

    /**
     * Obtiene el valor de la propiedad codRespuesta.
     * 
     */
    public int getCodRespuesta() {
        return codRespuesta;
    }

    /**
     * Define el valor de la propiedad codRespuesta.
     * 
     */
    public void setCodRespuesta(int value) {
        this.codRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad msjRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsjRespuesta() {
        return msjRespuesta;
    }

    /**
     * Define el valor de la propiedad msjRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsjRespuesta(String value) {
        this.msjRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad pinRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link GetInfoPinResult }
     *     
     */
    public GetInfoPinResult getPinRespuesta() {
        return pinRespuesta;
    }

    /**
     * Define el valor de la propiedad pinRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link GetInfoPinResult }
     *     
     */
    public void setPinRespuesta(GetInfoPinResult value) {
        this.pinRespuesta = value;
    }

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PinRespuesta [codRespuesta=");
		builder.append(codRespuesta);
		builder.append(", msjRespuesta=");
		builder.append(msjRespuesta);
		builder.append(", pinRespuesta=");
		builder.append(pinRespuesta);
		builder.append("]");
		return builder.toString();
	}

    
}
