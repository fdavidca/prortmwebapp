
package com.proambiente.indra.cliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EnviarEventosSicovResult" type="{http://190.25.205.154:809?/sicov.asmx}EventosRespuesta" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "enviarEventosSicovResult"
})
@XmlRootElement(name = "EnviarEventosSicovResponse")
public class EnviarEventosSicovResponse {

    @XmlElement(name = "EnviarEventosSicovResult")
    protected EventosRespuesta enviarEventosSicovResult;

    /**
     * Obtiene el valor de la propiedad enviarEventosSicovResult.
     * 
     * @return
     *     possible object is
     *     {@link EventosRespuesta }
     *     
     */
    public EventosRespuesta getEnviarEventosSicovResult() {
        return enviarEventosSicovResult;
    }

    /**
     * Define el valor de la propiedad enviarEventosSicovResult.
     * 
     * @param value
     *     allowed object is
     *     {@link EventosRespuesta }
     *     
     */
    public void setEnviarEventosSicovResult(EventosRespuesta value) {
        this.enviarEventosSicovResult = value;
    }

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EnviarEventosSicovResponse [enviarEventosSicovResult=");
		builder.append(enviarEventosSicovResult);
		builder.append("]");
		return builder.toString();
	}

    
}
