
package com.proambiente.indra.cliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getInfoVehiculoResult" type="{http://190.25.205.154:809?/sicov.asmx}VehiculoRespuesta" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getInfoVehiculoResult"
})
@XmlRootElement(name = "getInfoVehiculoResponse")
public class GetInfoVehiculoResponse {

    protected VehiculoRespuesta getInfoVehiculoResult;

    /**
     * Obtiene el valor de la propiedad getInfoVehiculoResult.
     * 
     * @return
     *     possible object is
     *     {@link VehiculoRespuesta }
     *     
     */
    public VehiculoRespuesta getGetInfoVehiculoResult() {
        return getInfoVehiculoResult;
    }

    /**
     * Define el valor de la propiedad getInfoVehiculoResult.
     * 
     * @param value
     *     allowed object is
     *     {@link VehiculoRespuesta }
     *     
     */
    public void setGetInfoVehiculoResult(VehiculoRespuesta value) {
        this.getInfoVehiculoResult = value;
    }

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GetInfoVehiculoResponse [getInfoVehiculoResult=");
		builder.append(getInfoVehiculoResult);
		builder.append("]");
		return builder.toString();
	}
    
    

}
