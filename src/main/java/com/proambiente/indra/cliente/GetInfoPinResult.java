
package com.proambiente.indra.cliente;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para getInfoPinResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="getInfoPinResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://190.25.205.154:809?/sicov.asmx}EntityBase">
 *       &lt;sequence>
 *         &lt;element name="codigoRunt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="placa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="categoriaServicio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechaRevision" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="fechaReinspeccion" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="recaudadorId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="numeroPin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numeroIdentificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valorPin" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="pruebaActiva" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getInfoPinResult", propOrder = {
    "codigoRunt",
    "placa",
    "categoriaServicio",
    "fechaRevision",
    "fechaReinspeccion",
    "recaudadorId",
    "numeroPin",
    "tipoDocumento",
    "numeroIdentificacion",
    "valorPin",
    "pruebaActiva"
})
public class GetInfoPinResult
    extends EntityBase
{

    protected String codigoRunt;
    protected String placa;
    protected String categoriaServicio;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechaRevision;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechaReinspeccion;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer recaudadorId;
    protected String numeroPin;
    protected String tipoDocumento;
    protected String numeroIdentificacion;
    @XmlElement(required = true, nillable = true)
    protected BigDecimal valorPin;
    protected int pruebaActiva;

    /**
     * Obtiene el valor de la propiedad codigoRunt.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoRunt() {
        return codigoRunt;
    }

    /**
     * Define el valor de la propiedad codigoRunt.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoRunt(String value) {
        this.codigoRunt = value;
    }

    /**
     * Obtiene el valor de la propiedad placa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlaca() {
        return placa;
    }

    /**
     * Define el valor de la propiedad placa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlaca(String value) {
        this.placa = value;
    }

    /**
     * Obtiene el valor de la propiedad categoriaServicio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCategoriaServicio() {
        return categoriaServicio;
    }

    /**
     * Define el valor de la propiedad categoriaServicio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCategoriaServicio(String value) {
        this.categoriaServicio = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaRevision.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaRevision() {
        return fechaRevision;
    }

    /**
     * Define el valor de la propiedad fechaRevision.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaRevision(XMLGregorianCalendar value) {
        this.fechaRevision = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaReinspeccion.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaReinspeccion() {
        return fechaReinspeccion;
    }

    /**
     * Define el valor de la propiedad fechaReinspeccion.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaReinspeccion(XMLGregorianCalendar value) {
        this.fechaReinspeccion = value;
    }

    /**
     * Obtiene el valor de la propiedad recaudadorId.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRecaudadorId() {
        return recaudadorId;
    }

    /**
     * Define el valor de la propiedad recaudadorId.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRecaudadorId(Integer value) {
        this.recaudadorId = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroPin.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroPin() {
        return numeroPin;
    }

    /**
     * Define el valor de la propiedad numeroPin.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroPin(String value) {
        this.numeroPin = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDocumento() {
        return tipoDocumento;
    }

    /**
     * Define el valor de la propiedad tipoDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDocumento(String value) {
        this.tipoDocumento = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroIdentificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    /**
     * Define el valor de la propiedad numeroIdentificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroIdentificacion(String value) {
        this.numeroIdentificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad valorPin.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValorPin() {
        return valorPin;
    }

    /**
     * Define el valor de la propiedad valorPin.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValorPin(BigDecimal value) {
        this.valorPin = value;
    }

    /**
     * Obtiene el valor de la propiedad pruebaActiva.
     * 
     */
    public int getPruebaActiva() {
        return pruebaActiva;
    }

    /**
     * Define el valor de la propiedad pruebaActiva.
     * 
     */
    public void setPruebaActiva(int value) {
        this.pruebaActiva = value;
    }

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GetInfoPinResult [codigoRunt=");
		builder.append(codigoRunt);
		builder.append(", placa=");
		builder.append(placa);
		builder.append(", categoriaServicio=");
		builder.append(categoriaServicio);
		builder.append(", fechaRevision=");
		builder.append(fechaRevision);
		builder.append(", fechaReinspeccion=");
		builder.append(fechaReinspeccion);
		builder.append(", recaudadorId=");
		builder.append(recaudadorId);
		builder.append(", numeroPin=");
		builder.append(numeroPin);
		builder.append(", tipoDocumento=");
		builder.append(tipoDocumento);
		builder.append(", numeroIdentificacion=");
		builder.append(numeroIdentificacion);
		builder.append(", valorPin=");
		builder.append(valorPin);
		builder.append(", pruebaActiva=");
		builder.append(pruebaActiva);
		builder.append("]");
		return builder.toString();
	}

    
}
