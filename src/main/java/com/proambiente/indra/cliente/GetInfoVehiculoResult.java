
package com.proambiente.indra.cliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para getInfoVehiculoResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="getInfoVehiculoResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://190.25.205.154:809?/sicov.asmx}EntityBase">
 *       &lt;sequence>
 *         &lt;element name="TipoServicio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Clase" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Licencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Marca" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Linea" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Modelo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CantSillas" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Color" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Serie" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MotorNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Chasis" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VIN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Cilindraje" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Combustible" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FechaMatricula" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="CapacidadCarga" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="PesoBruto" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CapacidadPasajeros" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CantEjes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Blindado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NivelBlindaje" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VidriosPolarizados" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FechaSoat" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="NumeroPoliza" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EntidadAseguradora" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TipoCarroceria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AutoridadTransito" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ClasicoAntiguo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CantPuertas" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CodigoTipoCarroceria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CodigoCombustible" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CodigoTipoServicio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CodigoClase" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CodigoMarca" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CodigoLinea" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CodigoColor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CodigoLineaWs" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getInfoVehiculoResult", propOrder = {
    "tipoServicio",
    "clase",
    "licencia",
    "marca",
    "linea",
    "modelo",
    "cantSillas",
    "color",
    "serie",
    "motorNo",
    "chasis",
    "vin",
    "cilindraje",
    "combustible",
    "fechaMatricula",
    "capacidadCarga",
    "pesoBruto",
    "capacidadPasajeros",
    "cantEjes",
    "blindado",
    "nivelBlindaje",
    "vidriosPolarizados",
    "fechaSoat",
    "numeroPoliza",
    "entidadAseguradora",
    "tipoCarroceria",
    "autoridadTransito",
    "clasicoAntiguo",
    "cantPuertas",
    "codigoTipoCarroceria",
    "codigoCombustible",
    "codigoTipoServicio",
    "codigoClase",
    "codigoMarca",
    "codigoLinea",
    "codigoColor",
    "codigoLineaWs"
})
public class GetInfoVehiculoResult
    extends EntityBase
{

    @XmlElement(name = "TipoServicio")
    protected String tipoServicio;
    @XmlElement(name = "Clase")
    protected String clase;
    @XmlElement(name = "Licencia")
    protected String licencia;
    @XmlElement(name = "Marca")
    protected String marca;
    @XmlElement(name = "Linea")
    protected String linea;
    @XmlElement(name = "Modelo")
    protected String modelo;
    @XmlElement(name = "CantSillas")
    protected int cantSillas;
    @XmlElement(name = "Color")
    protected String color;
    @XmlElement(name = "Serie")
    protected String serie;
    @XmlElement(name = "MotorNo")
    protected String motorNo;
    @XmlElement(name = "Chasis")
    protected String chasis;
    @XmlElement(name = "VIN")
    protected String vin;
    @XmlElement(name = "Cilindraje")
    protected int cilindraje;
    @XmlElement(name = "Combustible")
    protected String combustible;
    @XmlElement(name = "FechaMatricula", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechaMatricula;
    @XmlElement(name = "CapacidadCarga")
    protected int capacidadCarga;
    @XmlElement(name = "PesoBruto")
    protected int pesoBruto;
    @XmlElement(name = "CapacidadPasajeros")
    protected String capacidadPasajeros;
    @XmlElement(name = "CantEjes")
    protected String cantEjes;
    @XmlElement(name = "Blindado")
    protected String blindado;
    @XmlElement(name = "NivelBlindaje")
    protected String nivelBlindaje;
    @XmlElement(name = "VidriosPolarizados")
    protected String vidriosPolarizados;
    @XmlElement(name = "FechaSoat", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechaSoat;
    @XmlElement(name = "NumeroPoliza")
    protected String numeroPoliza;
    @XmlElement(name = "EntidadAseguradora")
    protected String entidadAseguradora;
    @XmlElement(name = "TipoCarroceria")
    protected String tipoCarroceria;
    @XmlElement(name = "AutoridadTransito")
    protected String autoridadTransito;
    @XmlElement(name = "ClasicoAntiguo")
    protected String clasicoAntiguo;
    @XmlElement(name = "CantPuertas")
    protected String cantPuertas;
    @XmlElement(name = "CodigoTipoCarroceria")
    protected String codigoTipoCarroceria;
    @XmlElement(name = "CodigoCombustible")
    protected String codigoCombustible;
    @XmlElement(name = "CodigoTipoServicio")
    protected String codigoTipoServicio;
    @XmlElement(name = "CodigoClase")
    protected String codigoClase;
    @XmlElement(name = "CodigoMarca")
    protected String codigoMarca;
    @XmlElement(name = "CodigoLinea")
    protected String codigoLinea;
    @XmlElement(name = "CodigoColor")
    protected String codigoColor;
    @XmlElement(name = "CodigoLineaWs")
    protected String codigoLineaWs;

    /**
     * Obtiene el valor de la propiedad tipoServicio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoServicio() {
        return tipoServicio;
    }

    /**
     * Define el valor de la propiedad tipoServicio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoServicio(String value) {
        this.tipoServicio = value;
    }

    /**
     * Obtiene el valor de la propiedad clase.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClase() {
        return clase;
    }

    /**
     * Define el valor de la propiedad clase.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClase(String value) {
        this.clase = value;
    }

    /**
     * Obtiene el valor de la propiedad licencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicencia() {
        return licencia;
    }

    /**
     * Define el valor de la propiedad licencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicencia(String value) {
        this.licencia = value;
    }

    /**
     * Obtiene el valor de la propiedad marca.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMarca() {
        return marca;
    }

    /**
     * Define el valor de la propiedad marca.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMarca(String value) {
        this.marca = value;
    }

    /**
     * Obtiene el valor de la propiedad linea.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLinea() {
        return linea;
    }

    /**
     * Define el valor de la propiedad linea.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLinea(String value) {
        this.linea = value;
    }

    /**
     * Obtiene el valor de la propiedad modelo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModelo() {
        return modelo;
    }

    /**
     * Define el valor de la propiedad modelo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModelo(String value) {
        this.modelo = value;
    }

    /**
     * Obtiene el valor de la propiedad cantSillas.
     * 
     */
    public int getCantSillas() {
        return cantSillas;
    }

    /**
     * Define el valor de la propiedad cantSillas.
     * 
     */
    public void setCantSillas(int value) {
        this.cantSillas = value;
    }

    /**
     * Obtiene el valor de la propiedad color.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getColor() {
        return color;
    }

    /**
     * Define el valor de la propiedad color.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setColor(String value) {
        this.color = value;
    }

    /**
     * Obtiene el valor de la propiedad serie.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSerie() {
        return serie;
    }

    /**
     * Define el valor de la propiedad serie.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSerie(String value) {
        this.serie = value;
    }

    /**
     * Obtiene el valor de la propiedad motorNo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMotorNo() {
        return motorNo;
    }

    /**
     * Define el valor de la propiedad motorNo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMotorNo(String value) {
        this.motorNo = value;
    }

    /**
     * Obtiene el valor de la propiedad chasis.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChasis() {
        return chasis;
    }

    /**
     * Define el valor de la propiedad chasis.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChasis(String value) {
        this.chasis = value;
    }

    /**
     * Obtiene el valor de la propiedad vin.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVIN() {
        return vin;
    }

    /**
     * Define el valor de la propiedad vin.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVIN(String value) {
        this.vin = value;
    }

    /**
     * Obtiene el valor de la propiedad cilindraje.
     * 
     */
    public int getCilindraje() {
        return cilindraje;
    }

    /**
     * Define el valor de la propiedad cilindraje.
     * 
     */
    public void setCilindraje(int value) {
        this.cilindraje = value;
    }

    /**
     * Obtiene el valor de la propiedad combustible.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCombustible() {
        return combustible;
    }

    /**
     * Define el valor de la propiedad combustible.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCombustible(String value) {
        this.combustible = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaMatricula.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaMatricula() {
        return fechaMatricula;
    }

    /**
     * Define el valor de la propiedad fechaMatricula.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaMatricula(XMLGregorianCalendar value) {
        this.fechaMatricula = value;
    }

    /**
     * Obtiene el valor de la propiedad capacidadCarga.
     * 
     */
    public int getCapacidadCarga() {
        return capacidadCarga;
    }

    /**
     * Define el valor de la propiedad capacidadCarga.
     * 
     */
    public void setCapacidadCarga(int value) {
        this.capacidadCarga = value;
    }

    /**
     * Obtiene el valor de la propiedad pesoBruto.
     * 
     */
    public int getPesoBruto() {
        return pesoBruto;
    }

    /**
     * Define el valor de la propiedad pesoBruto.
     * 
     */
    public void setPesoBruto(int value) {
        this.pesoBruto = value;
    }

    /**
     * Obtiene el valor de la propiedad capacidadPasajeros.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCapacidadPasajeros() {
        return capacidadPasajeros;
    }

    /**
     * Define el valor de la propiedad capacidadPasajeros.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCapacidadPasajeros(String value) {
        this.capacidadPasajeros = value;
    }

    /**
     * Obtiene el valor de la propiedad cantEjes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantEjes() {
        return cantEjes;
    }

    /**
     * Define el valor de la propiedad cantEjes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantEjes(String value) {
        this.cantEjes = value;
    }

    /**
     * Obtiene el valor de la propiedad blindado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBlindado() {
        return blindado;
    }

    /**
     * Define el valor de la propiedad blindado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBlindado(String value) {
        this.blindado = value;
    }

    /**
     * Obtiene el valor de la propiedad nivelBlindaje.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNivelBlindaje() {
        return nivelBlindaje;
    }

    /**
     * Define el valor de la propiedad nivelBlindaje.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNivelBlindaje(String value) {
        this.nivelBlindaje = value;
    }

    /**
     * Obtiene el valor de la propiedad vidriosPolarizados.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVidriosPolarizados() {
        return vidriosPolarizados;
    }

    /**
     * Define el valor de la propiedad vidriosPolarizados.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVidriosPolarizados(String value) {
        this.vidriosPolarizados = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaSoat.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaSoat() {
        return fechaSoat;
    }

    /**
     * Define el valor de la propiedad fechaSoat.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaSoat(XMLGregorianCalendar value) {
        this.fechaSoat = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroPoliza.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroPoliza() {
        return numeroPoliza;
    }

    /**
     * Define el valor de la propiedad numeroPoliza.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroPoliza(String value) {
        this.numeroPoliza = value;
    }

    /**
     * Obtiene el valor de la propiedad entidadAseguradora.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntidadAseguradora() {
        return entidadAseguradora;
    }

    /**
     * Define el valor de la propiedad entidadAseguradora.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntidadAseguradora(String value) {
        this.entidadAseguradora = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoCarroceria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoCarroceria() {
        return tipoCarroceria;
    }

    /**
     * Define el valor de la propiedad tipoCarroceria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoCarroceria(String value) {
        this.tipoCarroceria = value;
    }

    /**
     * Obtiene el valor de la propiedad autoridadTransito.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAutoridadTransito() {
        return autoridadTransito;
    }

    /**
     * Define el valor de la propiedad autoridadTransito.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAutoridadTransito(String value) {
        this.autoridadTransito = value;
    }

    /**
     * Obtiene el valor de la propiedad clasicoAntiguo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClasicoAntiguo() {
        return clasicoAntiguo;
    }

    /**
     * Define el valor de la propiedad clasicoAntiguo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClasicoAntiguo(String value) {
        this.clasicoAntiguo = value;
    }

    /**
     * Obtiene el valor de la propiedad cantPuertas.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantPuertas() {
        return cantPuertas;
    }

    /**
     * Define el valor de la propiedad cantPuertas.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantPuertas(String value) {
        this.cantPuertas = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoTipoCarroceria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoTipoCarroceria() {
        return codigoTipoCarroceria;
    }

    /**
     * Define el valor de la propiedad codigoTipoCarroceria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoTipoCarroceria(String value) {
        this.codigoTipoCarroceria = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoCombustible.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoCombustible() {
        return codigoCombustible;
    }

    /**
     * Define el valor de la propiedad codigoCombustible.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoCombustible(String value) {
        this.codigoCombustible = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoTipoServicio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoTipoServicio() {
        return codigoTipoServicio;
    }

    /**
     * Define el valor de la propiedad codigoTipoServicio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoTipoServicio(String value) {
        this.codigoTipoServicio = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoClase.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoClase() {
        return codigoClase;
    }

    /**
     * Define el valor de la propiedad codigoClase.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoClase(String value) {
        this.codigoClase = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoMarca.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoMarca() {
        return codigoMarca;
    }

    /**
     * Define el valor de la propiedad codigoMarca.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoMarca(String value) {
        this.codigoMarca = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoLinea.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoLinea() {
        return codigoLinea;
    }

    /**
     * Define el valor de la propiedad codigoLinea.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoLinea(String value) {
        this.codigoLinea = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoColor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoColor() {
        return codigoColor;
    }

    /**
     * Define el valor de la propiedad codigoColor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoColor(String value) {
        this.codigoColor = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoLineaWs.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoLineaWs() {
        return codigoLineaWs;
    }

    /**
     * Define el valor de la propiedad codigoLineaWs.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoLineaWs(String value) {
        this.codigoLineaWs = value;
    }

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GetInfoVehiculoResult [tipoServicio=");
		builder.append(tipoServicio);
		builder.append(", clase=");
		builder.append(clase);
		builder.append(", licencia=");
		builder.append(licencia);
		builder.append(", marca=");
		builder.append(marca);
		builder.append(", linea=");
		builder.append(linea);
		builder.append(", modelo=");
		builder.append(modelo);
		builder.append(", cantSillas=");
		builder.append(cantSillas);
		builder.append(", color=");
		builder.append(color);
		builder.append(", serie=");
		builder.append(serie);
		builder.append(", motorNo=");
		builder.append(motorNo);
		builder.append(", chasis=");
		builder.append(chasis);
		builder.append(", vin=");
		builder.append(vin);
		builder.append(", cilindraje=");
		builder.append(cilindraje);
		builder.append(", combustible=");
		builder.append(combustible);
		builder.append(", fechaMatricula=");
		builder.append(fechaMatricula);
		builder.append(", capacidadCarga=");
		builder.append(capacidadCarga);
		builder.append(", pesoBruto=");
		builder.append(pesoBruto);
		builder.append(", capacidadPasajeros=");
		builder.append(capacidadPasajeros);
		builder.append(", cantEjes=");
		builder.append(cantEjes);
		builder.append(", blindado=");
		builder.append(blindado);
		builder.append(", nivelBlindaje=");
		builder.append(nivelBlindaje);
		builder.append(", vidriosPolarizados=");
		builder.append(vidriosPolarizados);
		builder.append(", fechaSoat=");
		builder.append(fechaSoat);
		builder.append(", numeroPoliza=");
		builder.append(numeroPoliza);
		builder.append(", entidadAseguradora=");
		builder.append(entidadAseguradora);
		builder.append(", tipoCarroceria=");
		builder.append(tipoCarroceria);
		builder.append(", autoridadTransito=");
		builder.append(autoridadTransito);
		builder.append(", clasicoAntiguo=");
		builder.append(clasicoAntiguo);
		builder.append(", cantPuertas=");
		builder.append(cantPuertas);
		builder.append(", codigoTipoCarroceria=");
		builder.append(codigoTipoCarroceria);
		builder.append(", codigoCombustible=");
		builder.append(codigoCombustible);
		builder.append(", codigoTipoServicio=");
		builder.append(codigoTipoServicio);
		builder.append(", codigoClase=");
		builder.append(codigoClase);
		builder.append(", codigoMarca=");
		builder.append(codigoMarca);
		builder.append(", codigoLinea=");
		builder.append(codigoLinea);
		builder.append(", codigoColor=");
		builder.append(codigoColor);
		builder.append(", codigoLineaWs=");
		builder.append(codigoLineaWs);
		builder.append("]");
		return builder.toString();
	}

    
}
