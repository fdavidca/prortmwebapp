
package com.proambiente.indra.cliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="setFURResult" type="{http://190.25.205.154:809?/sicov.asmx}FURRespuesta" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "setFURResult"
})
@XmlRootElement(name = "setFURResponse")
public class SetFURResponse {

    protected FURRespuesta setFURResult;

    /**
     * Obtiene el valor de la propiedad setFURResult.
     * 
     * @return
     *     possible object is
     *     {@link FURRespuesta }
     *     
     */
    public FURRespuesta getSetFURResult() {
        return setFURResult;
    }

    /**
     * Define el valor de la propiedad setFURResult.
     * 
     * @param value
     *     allowed object is
     *     {@link FURRespuesta }
     *     
     */
    public void setSetFURResult(FURRespuesta value) {
        this.setFURResult = value;
    }

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SetFURResponse [setFURResult=");
		builder.append(setFURResult);
		builder.append("]");
		return builder.toString();
	}

    
}
