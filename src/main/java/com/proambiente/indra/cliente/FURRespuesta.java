
package com.proambiente.indra.cliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para FURRespuesta complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="FURRespuesta">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="furData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codRespuesta" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="msjRespuesta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FURRespuesta", propOrder = {
    "furData",
    "codRespuesta",
    "msjRespuesta"
})
public class FURRespuesta {

    protected String furData;
    protected int codRespuesta;
    protected String msjRespuesta;

    /**
     * Obtiene el valor de la propiedad furData.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFurData() {
        return furData;
    }

    /**
     * Define el valor de la propiedad furData.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFurData(String value) {
        this.furData = value;
    }

    /**
     * Obtiene el valor de la propiedad codRespuesta.
     * 
     */
    public int getCodRespuesta() {
        return codRespuesta;
    }

    /**
     * Define el valor de la propiedad codRespuesta.
     * 
     */
    public void setCodRespuesta(int value) {
        this.codRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad msjRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsjRespuesta() {
        return msjRespuesta;
    }

    /**
     * Define el valor de la propiedad msjRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsjRespuesta(String value) {
        this.msjRespuesta = value;
    }

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FURRespuesta [furData=");
		builder.append(furData);
		builder.append(", codRespuesta=");
		builder.append(codRespuesta);
		builder.append(", msjRespuesta=");
		builder.append(msjRespuesta);
		builder.append("]");
		return builder.toString();
	}
    
    

}
