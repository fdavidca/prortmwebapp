
package com.proambiente.indra.cliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nombreEmpleado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numeroIdentificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="placa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="extranjero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="consecutivoRUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IdRunt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="direccionIpEquipo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "nombreEmpleado",
    "numeroIdentificacion",
    "placa",
    "extranjero",
    "consecutivoRUNT",
    "idRunt",
    "direccionIpEquipo"
})
@XmlRootElement(name = "EnviarRuntSicov")
public class EnviarRuntSicov {

    protected String nombreEmpleado;
    protected String numeroIdentificacion;
    protected String placa;
    protected String extranjero;
    protected String consecutivoRUNT;
    @XmlElement(name = "IdRunt")
    protected String idRunt;
    protected String direccionIpEquipo;

    /**
     * Obtiene el valor de la propiedad nombreEmpleado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreEmpleado() {
        return nombreEmpleado;
    }

    /**
     * Define el valor de la propiedad nombreEmpleado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreEmpleado(String value) {
        this.nombreEmpleado = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroIdentificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    /**
     * Define el valor de la propiedad numeroIdentificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroIdentificacion(String value) {
        this.numeroIdentificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad placa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlaca() {
        return placa;
    }

    /**
     * Define el valor de la propiedad placa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlaca(String value) {
        this.placa = value;
    }

    /**
     * Obtiene el valor de la propiedad extranjero.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtranjero() {
        return extranjero;
    }

    /**
     * Define el valor de la propiedad extranjero.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtranjero(String value) {
        this.extranjero = value;
    }

    /**
     * Obtiene el valor de la propiedad consecutivoRUNT.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsecutivoRUNT() {
        return consecutivoRUNT;
    }

    /**
     * Define el valor de la propiedad consecutivoRUNT.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsecutivoRUNT(String value) {
        this.consecutivoRUNT = value;
    }

    /**
     * Obtiene el valor de la propiedad idRunt.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRunt() {
        return idRunt;
    }

    /**
     * Define el valor de la propiedad idRunt.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRunt(String value) {
        this.idRunt = value;
    }

    /**
     * Obtiene el valor de la propiedad direccionIpEquipo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDireccionIpEquipo() {
        return direccionIpEquipo;
    }

    /**
     * Define el valor de la propiedad direccionIpEquipo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDireccionIpEquipo(String value) {
        this.direccionIpEquipo = value;
    }

}
