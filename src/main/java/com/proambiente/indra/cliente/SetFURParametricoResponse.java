
package com.proambiente.indra.cliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="_setFURParametricoResult" type="{http://190.25.205.154:809?/sicov.asmx}FURRespuesta" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "setFURParametricoResult"
})
@XmlRootElement(name = "_setFURParametricoResponse")
public class SetFURParametricoResponse {

    @XmlElement(name = "_setFURParametricoResult")
    protected FURRespuesta setFURParametricoResult;

    /**
     * Gets the value of the setFURParametricoResult property.
     * 
     * @return
     *     possible object is
     *     {@link FURRespuesta }
     *     
     */
    public FURRespuesta getSetFURParametricoResult() {
        return setFURParametricoResult;
    }

    /**
     * Sets the value of the setFURParametricoResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link FURRespuesta }
     *     
     */
    public void setSetFURParametricoResult(FURRespuesta value) {
        this.setFURParametricoResult = value;
    }

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SetFURParametricoResponse [setFURParametricoResult=");
		builder.append(setFURParametricoResult);
		builder.append("]");
		return builder.toString();
	}

    
}
