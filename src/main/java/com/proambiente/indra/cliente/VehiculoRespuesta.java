
package com.proambiente.indra.cliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para VehiculoRespuesta complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="VehiculoRespuesta">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codRespuesta" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="msjRespuesta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vehRespuesta" type="{http://190.25.205.154:809?/sicov.asmx}getInfoVehiculoResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VehiculoRespuesta", propOrder = {
    "codRespuesta",
    "msjRespuesta",
    "vehRespuesta"
})
public class VehiculoRespuesta {

    protected int codRespuesta;
    protected String msjRespuesta;
    protected GetInfoVehiculoResult vehRespuesta;

    /**
     * Obtiene el valor de la propiedad codRespuesta.
     * 
     */
    public int getCodRespuesta() {
        return codRespuesta;
    }

    /**
     * Define el valor de la propiedad codRespuesta.
     * 
     */
    public void setCodRespuesta(int value) {
        this.codRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad msjRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsjRespuesta() {
        return msjRespuesta;
    }

    /**
     * Define el valor de la propiedad msjRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsjRespuesta(String value) {
        this.msjRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad vehRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link GetInfoVehiculoResult }
     *     
     */
    public GetInfoVehiculoResult getVehRespuesta() {
        return vehRespuesta;
    }

    /**
     * Define el valor de la propiedad vehRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link GetInfoVehiculoResult }
     *     
     */
    public void setVehRespuesta(GetInfoVehiculoResult value) {
        this.vehRespuesta = value;
    }

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("VehiculoRespuesta [codRespuesta=");
		builder.append(codRespuesta);
		builder.append(", msjRespuesta=");
		builder.append(msjRespuesta);
		builder.append(", vehRespuesta=");
		builder.append(vehRespuesta);
		builder.append("]");
		return builder.toString();
	}

    
}
