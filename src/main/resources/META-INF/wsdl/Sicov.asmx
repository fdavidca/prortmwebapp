<?xml version="1.0" encoding="utf-8"?>
<wsdl:definitions xmlns:s="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://schemas.xmlsoap.org/wsdl/soap12/" xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/" xmlns:tns="http://190.25.205.154:809?/sicov.asmx" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:tm="http://microsoft.com/wsdl/mime/textMatching/" xmlns:http="http://schemas.xmlsoap.org/wsdl/http/" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" targetNamespace="http://190.25.205.154:809?/sicov.asmx" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/">
  <wsdl:types>
    <s:schema elementFormDefault="qualified" targetNamespace="http://190.25.205.154:809?/sicov.asmx">
      <s:element name="setFUR">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="cadena" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="setFURResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="setFURResult" type="tns:FURRespuesta" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="FURRespuesta">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="1" name="furData" type="s:string" />
          <s:element minOccurs="1" maxOccurs="1" name="codRespuesta" type="s:int" />
          <s:element minOccurs="0" maxOccurs="1" name="msjRespuesta" type="s:string" />
        </s:sequence>
      </s:complexType>
      <s:element name="setEventsRTMEC">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="cadena" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="setEventsRTMECResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="setEventsRTMECResult" type="tns:FURRespuesta" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="setConexion">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="cadena" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="setConexionResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="setConexionResult" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="_setFURParametrico">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="cadena" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="_setFURParametricoResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="_setFURParametricoResult" type="tns:FURRespuesta" />
          </s:sequence>
        </s:complexType>
      </s:element>
    </s:schema>
  </wsdl:types>
  <wsdl:message name="setFURSoapIn">
    <wsdl:part name="parameters" element="tns:setFUR" />
  </wsdl:message>
  <wsdl:message name="setFURSoapOut">
    <wsdl:part name="parameters" element="tns:setFURResponse" />
  </wsdl:message>
  <wsdl:message name="setEventsRTMECSoapIn">
    <wsdl:part name="parameters" element="tns:setEventsRTMEC" />
  </wsdl:message>
  <wsdl:message name="setEventsRTMECSoapOut">
    <wsdl:part name="parameters" element="tns:setEventsRTMECResponse" />
  </wsdl:message>
  <wsdl:message name="setConexionSoapIn">
    <wsdl:part name="parameters" element="tns:setConexion" />
  </wsdl:message>
  <wsdl:message name="setConexionSoapOut">
    <wsdl:part name="parameters" element="tns:setConexionResponse" />
  </wsdl:message>
  <wsdl:message name="_setFURParametricoSoapIn">
    <wsdl:part name="parameters" element="tns:_setFURParametrico" />
  </wsdl:message>
  <wsdl:message name="_setFURParametricoSoapOut">
    <wsdl:part name="parameters" element="tns:_setFURParametricoResponse" />
  </wsdl:message>
  <wsdl:portType name="SicovSoap">
    <wsdl:operation name="setFUR">
      <wsdl:input message="tns:setFURSoapIn" />
      <wsdl:output message="tns:setFURSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="setEventsRTMEC">
      <wsdl:input message="tns:setEventsRTMECSoapIn" />
      <wsdl:output message="tns:setEventsRTMECSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="setConexion">
      <wsdl:input message="tns:setConexionSoapIn" />
      <wsdl:output message="tns:setConexionSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="_setFURParametrico">
      <wsdl:input message="tns:_setFURParametricoSoapIn" />
      <wsdl:output message="tns:_setFURParametricoSoapOut" />
    </wsdl:operation>
  </wsdl:portType>
  <wsdl:binding name="SicovSoap" type="tns:SicovSoap">
    <soap:binding transport="http://schemas.xmlsoap.org/soap/http" />
    <wsdl:operation name="setFUR">
      <soap:operation soapAction="http://190.25.205.154:809?/sicov.asmx/setFUR" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="setEventsRTMEC">
      <soap:operation soapAction="http://190.25.205.154:809?/sicov.asmx/setEventsRTMEC" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="setConexion">
      <soap:operation soapAction="http://190.25.205.154:809?/sicov.asmx/setConexion" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="_setFURParametrico">
      <soap:operation soapAction="http://190.25.205.154:809?/sicov.asmx/_setFURParametrico" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
  </wsdl:binding>
  <wsdl:binding name="SicovSoap12" type="tns:SicovSoap">
    <soap12:binding transport="http://schemas.xmlsoap.org/soap/http" />
    <wsdl:operation name="setFUR">
      <soap12:operation soapAction="http://190.25.205.154:809?/sicov.asmx/setFUR" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="setEventsRTMEC">
      <soap12:operation soapAction="http://190.25.205.154:809?/sicov.asmx/setEventsRTMEC" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="setConexion">
      <soap12:operation soapAction="http://190.25.205.154:809?/sicov.asmx/setConexion" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="_setFURParametrico">
      <soap12:operation soapAction="http://190.25.205.154:809?/sicov.asmx/_setFURParametrico" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
  </wsdl:binding>
  <wsdl:service name="Sicov">
    <wsdl:port name="SicovSoap" binding="tns:SicovSoap">
      <soap:address location="http://186.116.13.228:9094/Sicov.asmx" />
    </wsdl:port>
    <wsdl:port name="SicovSoap12" binding="tns:SicovSoap12">
      <soap12:address location="http://186.116.13.228:9094/Sicov.asmx" />
    </wsdl:port>
  </wsdl:service>
</wsdl:definitions>